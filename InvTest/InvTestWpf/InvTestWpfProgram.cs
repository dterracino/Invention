﻿/*! 20 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace InvTest
{
  public class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.DeviceEmulation = Inv.WpfDeviceEmulation.iPad_Mini;
        Inv.WpfShell.DeviceEmulationRotated = false;
        Inv.WpfShell.FullScreenMode = false;
        Inv.WpfShell.DefaultWindowWidth = 1920;
        Inv.WpfShell.DefaultWindowHeight = 1080;

        Inv.WpfShell.Run(A => InvTest.Shell.Install(A));
      });
    }
  }
}
