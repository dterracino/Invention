﻿/*! 203 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using Inv.Support;

public static class Invention_Playground
{
  public static void Install(Inv.Application Application)
  {
    InvTest.Shell.Install(Application);
  }
}

namespace InvTest
{
  public static class Shell
  {
    public static void Install(Inv.Application Application)
    {
      new Application(Application);
    }
  }

  public sealed class Application : Inv.Mimic<Inv.Material.Application>
  {
    internal Application(Inv.Application Application)
    {
      this.LogoImage = Resources.Images.PhoenixLogo960x540;

      this.BookmarkFile = Application.Directory.RootFolder.NewFile("Bookmark.txt");

      this.Base = new Inv.Material.Application(Application, new Inv.Material.Theme(Inv.Colour.HotPink));
      Base.Title = "Invention Test";
      Base.HandleExceptionEvent += (Exception) =>
      {
        Debug.WriteLine(Exception.StackTrace);
      };
      Base.StartEvent += () => Compile(About, LoadBoomark());
      Base.StopEvent += () => SaveBookmark();
      Base.SuspendEvent += () => SaveBookmark();
      Base.ExitQuery += () =>
      {
        var LastSurface = Base.Window.ActiveSurface;

        var CloseSurface = Base.Window.NewSurface();
        CloseSurface.Background.Colour = Inv.Colour.Black;

        var CloseDock = CloseSurface.NewVerticalDock();
        CloseSurface.Content = CloseDock;
        CloseDock.Alignment.Center();

        var CloseButton = CloseSurface.NewButton();
        CloseDock.AddHeader(CloseButton);
        CloseButton.Background.Colour = Inv.Colour.Red;
        CloseButton.Padding.Set(20);
        CloseButton.Margin.Set(20);
        CloseButton.SingleTapEvent += () => Base.Exit();

        var CloseLabel = CloseSurface.NewLabel();
        CloseButton.Content = CloseLabel;
        CloseLabel.Font.Size = 40;
        CloseLabel.Font.Colour = Inv.Colour.White;
        CloseLabel.JustifyCenter();
        CloseLabel.Text = "CLOSE";

        var CancelButton = CloseSurface.NewButton();
        CloseDock.AddHeader(CancelButton);
        CancelButton.Background.Colour = Inv.Colour.WhiteSmoke;
        CancelButton.Padding.Set(20);
        CancelButton.Margin.Set(20);
        CancelButton.SingleTapEvent += () => Base.Window.Transition(LastSurface).Fade();

        var CancelLabel = CloseSurface.NewLabel();
        CancelButton.Content = CancelLabel;
        CancelLabel.Font.Size = 40;
        CancelLabel.Font.Colour = Inv.Colour.Black;
        CancelLabel.JustifyCenter();
        CancelLabel.Text = "CANCEL";

        Base.Window.Transition(CloseSurface).Fade();

        return false;
      };
    }

    private Inv.Panel About(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);

      var Dock = Surface.NewVerticalDock();
      Overlay.AddPanel(Dock);
      Dock.Alignment.Center();

      var ButtonDock = Surface.NewHorizontalDock();
      Dock.AddFooter(ButtonDock);

      var Scroll = Surface.NewVerticalScroll();
      Dock.AddClient(Scroll);
      Scroll.Alignment.Center();

      var Stack = Surface.NewVerticalStack();
      Scroll.Content = Stack;

      var TitleLabel = Surface.NewLabel();
      Dock.AddHeader(TitleLabel);
      TitleLabel.Padding.Set(16);
      TitleLabel.Font.Colour = Inv.Colour.White;
      TitleLabel.Font.Light();
      TitleLabel.Font.Size = 36;
      TitleLabel.Text = "About!";

      var EmailButton = new AboutButton(Surface);
      Stack.AddPanel(EmailButton);

      EmailButton.LogoImage = Resources.Images.Water128x128;
      EmailButton.TitleText = "Send me an email";
      EmailButton.ActionText = "hodgskin.callan@gmail.com";
      EmailButton.SingleTapEvent += () =>
      {
        var EmailMessage = Surface.Window.Application.Email.NewMessage();
        EmailMessage.To("Callan Hodgskin", "hodgskin.callan@gmail.com");
        EmailMessage.Subject = "Testing";
        // TODO: file attachment.
        EmailMessage.Send();
      };

      if (Surface.Window.Width > 380)
        Scroll.Size.SetWidth(380);
      else
        Scroll.Size.AutoWidth();

      return Overlay;
    }
    private Inv.Panel ActionFlyout(Inv.Surface Surface)
    {
      var Flyout = Surface.NewOverlay();

      var ActionPanel = new ActionPanel(Surface);
      Flyout.AddPanel(ActionPanel);
      ActionPanel.Alignment.BottomStretch();

      var LeftButton = ActionPanel.AddHeaderButton();
      LeftButton.Caption = "LEFTY";

      var RightButton = ActionPanel.AddFooterButton();
      RightButton.Caption = "RIGHTY";

      return Flyout;
    }
    private Inv.Panel Animations(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.Black;

      var StartButton = Surface.NewButton();
      Overlay.AddPanel(StartButton);
      StartButton.Alignment.BottomLeft();
      StartButton.Background.Colour = Inv.Colour.DarkGreen;
      StartButton.Padding.Set(20);

      var StartLabel = Surface.NewLabel();
      StartButton.Content = StartLabel;
      StartLabel.Font.Size = 30;
      StartLabel.JustifyCenter();
      StartLabel.Font.Colour = Inv.Colour.White;
      StartLabel.Text = "START\nANIMATION";

      var StopButton = Surface.NewButton();
      Overlay.AddPanel(StopButton);
      StopButton.Alignment.BottomRight();
      StopButton.Background.Colour = Inv.Colour.DarkRed;
      StopButton.Padding.Set(20);

      var StopLabel = Surface.NewLabel();
      StopButton.Content = StopLabel;
      StopLabel.Font.Size = 30;
      StopLabel.Font.Colour = Inv.Colour.White;
      StopLabel.JustifyCenter();
      StopLabel.Text = "STOP\nANIMATION";

      var RestartButton = Surface.NewButton();
      Overlay.AddPanel(RestartButton);
      RestartButton.Alignment.BottomCenter();
      RestartButton.Background.Colour = Inv.Colour.Purple;
      RestartButton.Padding.Set(20);

      var RestartLabel = Surface.NewLabel();
      RestartButton.Content = RestartLabel;
      RestartLabel.Font.Size = 30;
      RestartLabel.Font.Colour = Inv.Colour.White;
      RestartLabel.JustifyCenter();
      RestartLabel.Text = "RESTART\nANIMATION";

      var TargetButton = Surface.NewButton();
      Overlay.AddPanel(TargetButton);
      TargetButton.Alignment.Center();
      TargetButton.Background.Colour = Inv.Colour.LightBlue;
      TargetButton.Padding.Set(20);

      var TargetLabel = Surface.NewLabel();
      TargetButton.Content = TargetLabel;
      TargetLabel.Font.Size = 60;
      TargetLabel.JustifyCenter();
      TargetLabel.Text = "ANIMATION TARGET";

      var Animation = (Inv.Animation)null;

      StartButton.SingleTapEvent += () =>
      {
        Animation = Surface.NewAnimation();

        var Target = Animation.AddTarget(TargetButton);
        Target.FadeOpacity(1.0F, 0.0F, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2));

        Animation.Start();
      };

      StopButton.SingleTapEvent += () =>
      {
        if (Animation != null)
        {
          Animation.Stop();
          Animation = null;
        }
      };

      RestartButton.SingleTapEvent += () =>
      {
        StopButton.SingleTap();
        StartButton.SingleTap();
      };

      TargetButton.SingleTapEvent += () =>
      {
        if (Animation.IsActive)
        {
          Animation.Stop();
        }
        else
        {
          Animation.RemoveTargets();
          var Target = Animation.AddTarget(TargetButton);
          var Opacity = TargetLabel.Opacity.Get();

          if (Opacity < 1.0F)
            Target.FadeOpacity(Opacity, 1.0F, TimeSpan.FromSeconds(1.0F * (1.0F - Opacity)));
          else
            Target.FadeOpacity(Opacity, 0.0F, TimeSpan.FromSeconds(1.0F * Opacity));

          Animation.Start();
        }
      };
      return Overlay;
    }
    private Inv.Panel BasicFlow(Inv.Surface Surface)
    {
      var ItemCount = 1;

      var Stack = Surface.NewVerticalStack();
      Stack.Background.Colour = Inv.Colour.WhiteSmoke;

      var Button = Surface.NewButton();
      Stack.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.SteelBlue;
      Button.Padding.Set(10);
      Button.Margin.Set(10);

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Text = "Blah";

      var Flow = Surface.NewFlow();
      Stack.AddPanel(Flow);
      Flow.Background.Colour = Inv.Colour.HotPink;

      var Section = Flow.AddBatchedSection<int>();
      Section.RequestBatch += (StartIndex, Count, Token, Callback) =>
      {
        //Surface.Window.NewTask(Thread =>
        //{
        //  Thread.Post(() =>
        //  {
        //  });
        //});
        Callback(Enumerable.Range(StartIndex, Count));
      };
      Section.ItemQuery += (Item) =>
      {
        var ItemLabel = Surface.NewLabel();
        ItemLabel.Text = string.Format("{0:N0} - {1:T}", Item, DateTimeOffset.Now);
        ItemLabel.Background.Colour = Inv.Colour.LightSteelBlue;
        return ItemLabel;
      };
      Section.SetItemCount(ItemCount);

      Button.SingleTapEvent += () =>
      {
        ItemCount = (ItemCount + 1) % 3;
        //ItemCount += 5;
        Section.SetItemCount(ItemCount);
        Section.Reload();
      };

      return Stack;
    }
    private Inv.Panel Blank(Inv.Surface Surface)
    {
      return Surface.NewVerticalDock();
    }
    private Inv.Panel BrowseFile(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.Black;

      var TempFolder = Surface.Window.Application.Directory.NewFolder("Temp");

      var PDFButton = Surface.NewButton();
      Overlay.AddPanel(PDFButton);
      PDFButton.Alignment.BottomLeft();
      PDFButton.Background.Colour = Inv.Colour.DarkGreen;
      PDFButton.Padding.Set(20);

      var PDFLabel = Surface.NewLabel();
      PDFButton.Content = PDFLabel;
      PDFLabel.Font.Size = 30;
      PDFLabel.JustifyCenter();
      PDFLabel.Font.Colour = Inv.Colour.White;
      PDFLabel.Text = "BROWSE PDF";

      PDFButton.SingleTapEvent += () =>
      {
        var TempFile = TempFolder.NewFile("TempFile.pdf");
        TempFile.WriteAllBytes(Resources.Documents.PhoenixPlan.GetBuffer());

        //TempFile.SetLastWriteTimeUtc(new DateTime(2016, 01, 01));
        //PDFLabel.Text = TempFile.GetLastWriteTimeUtc().ToString();

        Surface.Window.Browse(TempFile);
      };

      var DOCButton = Surface.NewButton();
      Overlay.AddPanel(DOCButton);
      DOCButton.Alignment.BottomRight();
      DOCButton.Background.Colour = Inv.Colour.Purple;
      DOCButton.Padding.Set(20);
      DOCButton.SingleTapEvent += () =>
      {
        var TempFile = TempFolder.NewFile("TempFile.docx");
        TempFile.WriteAllBytes(Resources.Documents.PhoenixLogoDoc.GetBuffer());

        Surface.Window.Browse(TempFile);
      };

      var DOCLabel = Surface.NewLabel();
      DOCButton.Content = DOCLabel;
      DOCLabel.Font.Size = 30;
      DOCLabel.JustifyCenter();
      DOCLabel.Font.Colour = Inv.Colour.White;
      DOCLabel.Text = "BROWSE DOC";
      return Overlay;
    }
    private Inv.Panel ButtonGrid(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var CustomQuery = new CustomQuery(Surface);
      Overlay.AddPanel(CustomQuery);
      CustomQuery.Alignment.StretchCenter();

      var ActionPanel = new ActionPanel(Surface);
      CustomQuery.Content = ActionPanel;
      ActionPanel.Caption = "ACTION PANEL";
      ActionPanel.Alignment.BottomStretch();

      var OneButton = ActionPanel.AddHeaderButton(); // o
      OneButton.Caption = "ONE";
      OneButton.IsVisible = true;
      OneButton.SingleTapEvent += () => ActionPanel.Compose();
      OneButton.PressEvent += () => CustomQuery.ShadeColour = Inv.Colour.SteelBlue;
      OneButton.ReleaseEvent += () => CustomQuery.ShadeColour = Inv.Colour.RosyBrown;

      var TwoButton = ActionPanel.AddHeaderButton(); // c
      TwoButton.Caption = "TWO";
      TwoButton.IsVisible = false;

      var ThreeButton = ActionPanel.AddHeaderButton(); // l
      ThreeButton.Caption = "THREE";
      ThreeButton.IsVisible = false;

      var SevenButton = ActionPanel.AddHeaderButton(); // u
      SevenButton.Caption = "SEVEN";
      SevenButton.IsVisible = false;

      var FourButton = ActionPanel.AddFooterButton(); // k
      FourButton.Caption = "FOUR";
      FourButton.IsEnabled = false;

      var FiveButton = ActionPanel.AddFooterButton(); // d
      FiveButton.Caption = "FIVE";
      FiveButton.IsEnabled = false;

      var SixButton = ActionPanel.AddFooterButton(); // h
      SixButton.Caption = "SIX";

      ActionPanel.Compose();

      CustomQuery.Size.SetMinimumWidth(Surface.Window.Width);
      CustomQuery.Show();

      return Overlay;
    }
    private Inv.Panel CenterInScroll(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();
      
      var Label = Surface.NewLabel();
      Scroll.Content = Label;
      Label.Alignment.Center();
      Label.Font.Colour = Inv.Colour.DimGray;
      Label.Text = "SHOULD BE HORIZONTALLY AND VERTICALLY CENTRED";

      return Scroll;
    }
    private Inv.Panel CrashParenting(Inv.Surface Surface)
    {
      var Frame = Surface.NewFrame();

      var ParentStack = Surface.NewVerticalStack();
      Frame.Content = ParentStack;

      var Button = Surface.NewButton();
      ParentStack.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.SteelBlue;
      Button.Padding.Set(50);

      var ChildStack = Surface.NewHorizontalStack();
      ParentStack.AddPanel(ChildStack);

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Font.Colour = Inv.Colour.White;
      Label.Text = "HELLO";

      Button.SingleTapEvent += () =>
      {
        if (Label.Text == "HELLO")
        {
          Label.Text = "WORLD";
          ChildStack.Margin.Set(50);

          ParentStack.RemovePanel(Button);
          ChildStack.AddPanel(Button);
        }
        else
        {
          Label.Text = "HELLO";
          ChildStack.Margin.Clear();

          ChildStack.RemovePanel(Button);
          ParentStack.AddPanel(Button);
        }
      };

      return Frame;
    }
    private Inv.Panel CustomCanvas(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.WhiteSmoke;

      var PressPoint = (Inv.Point?)null;

      var Canvas = Surface.NewCanvas();
      Overlay.AddPanel(Canvas);
      Canvas.SingleTapEvent += (Point) =>
      {
        Overlay.Background.Colour = Inv.Colour.Yellow;
        Debug.WriteLine("single");
      };
      Canvas.DoubleTapEvent += (Point) =>
      {
        Overlay.Background.Colour = Inv.Colour.Purple;
        Debug.WriteLine("double");
      };
      Canvas.ContextTapEvent += (Point) =>
      {
        Overlay.Background.Colour = Inv.Colour.SteelBlue;
        Debug.WriteLine("context");
      };
      Canvas.PressEvent += (Point) =>
      {
        PressPoint = Point;
      };
      Canvas.MoveEvent += (Point) =>
      {
        PressPoint = Point;
      };
      Canvas.ReleaseEvent += (Point) =>
      {
        PressPoint = null;
      };

      var ZoomFactor = 0;
      var ZoomPoint = (Inv.Point?)null;

      Canvas.ZoomEvent += (Point, Delta) =>
      {
        ZoomPoint = Point;
        ZoomFactor += Delta > 0 ? +10 : -10;
      };

      var Position = 0;

      var Image1Rect = new Inv.Rect(0, 100, 320, 180);
      var Image2Rect = new Inv.Rect(0, 500, 320, 180);
      var Image3Rect = new Inv.Rect(400, 300, 320, 180);
      var Image4Rect = new Inv.Rect(400, 500, 320, 180);
      var Image5Rect = new Inv.Rect(400, 40, 320, 180);

      Canvas.DrawEvent += (DC) =>
      {
        var DCWidth = DC.Width;
        var DCHeight = DC.Height;

        DC.DrawText(Surface.Window.DisplayRate.PerSecond.ToString() + " fps", "", 20, Inv.FontWeight.Regular, Inv.Colour.Black, new Inv.Point(DCWidth - 2, DCHeight - 2), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Bottom);

        var Opacity = Position++ / 1000.0F;
        if (Opacity > 1.0F)
          Opacity = 1.0F;

        var ZoomedSize = Math.Max(10, 100 + ZoomFactor);

        DC.DrawRectangle(Inv.Colour.DarkGreen, Inv.Colour.Red, 3, new Inv.Rect(280, 280, ZoomedSize, ZoomedSize));
        DC.DrawRectangle(Inv.Colour.DarkGreen, Inv.Colour.Blue, 3, new Inv.Rect(280, 380 + ZoomFactor, ZoomedSize, ZoomedSize));
        DC.DrawRectangle(Inv.Colour.DarkGreen, Inv.Colour.Purple, 3, new Inv.Rect(Math.Max(0, 380 + ZoomFactor), Math.Max(0, 280 + ZoomFactor), ZoomedSize, ZoomedSize));
        DC.DrawRectangle(Inv.Colour.DarkGreen, Inv.Colour.Orange, 3, new Inv.Rect(Math.Max(0, 380 + ZoomFactor), Math.Max(0, 380 + ZoomFactor), ZoomedSize, ZoomedSize));

        DC.DrawRectangle(Inv.Colour.Red.Opacity(Opacity), Inv.Colour.Blue.Opacity(Opacity), 1, new Inv.Rect(80, 80, 100, 100));
        DC.DrawEllipse(Inv.Colour.Green.Opacity(Opacity), Inv.Colour.Pink.Opacity(Opacity), 10, new Inv.Point(180, 180), new Inv.Point(50, 50));

        var TintOpacity = 0.50F;

        DC.DrawImage(LogoImage, Image1Rect, Opacity, Inv.Colour.Red.Opacity(TintOpacity), Inv.Mirror.Horizontal);
        DC.DrawImage(LogoImage, Image2Rect, Opacity, Inv.Colour.Green.Opacity(TintOpacity), Inv.Mirror.Vertical);
        DC.DrawImage(LogoImage, Image3Rect, Opacity, Inv.Colour.Yellow.Opacity(TintOpacity));
        DC.DrawImage(LogoImage, Image4Rect, Opacity, Inv.Colour.Blue.Opacity(TintOpacity));
        DC.DrawImage(LogoImage, Image5Rect, Opacity, Inv.Colour.Purple.Opacity(TintOpacity));

        DC.DrawText("Hello World", "", 20, Inv.FontWeight.Regular, Inv.Colour.Purple.Opacity(Opacity), new Inv.Point(100, 300), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);

        var LabelBrush = Inv.Colour.DarkRed.Opacity(0.75F);
        var LabelPen = Inv.Colour.DarkRed.Darken(0.25F);
        var LabelRect = new Inv.Rect(600, 600, 300, 35);
        DC.DrawRectangle(LabelBrush, LabelPen, 1, LabelRect);
        DC.DrawText("AbcdefghijklmnopqrstuvwxyZ", "", 20, Inv.FontWeight.Regular, Inv.Colour.Black, new Inv.Point(LabelRect.Left + LabelRect.Width / 2, LabelRect.Top + (LabelRect.Height / 2)), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);

        DC.DrawLine(Inv.Colour.Black, 5, new Inv.Point(50, 50), new Inv.Point(100, 100), new Inv.Point(50, 100), new Inv.Point(100, 50), new Inv.Point(50, 50));

        DC.DrawArc(Inv.Colour.Red, Inv.Colour.Blue, 5, new Inv.Point(200, 100), new Inv.Point(50, 50), 0, 90);

        //RC.DrawEllipse(Inv.Colour.Red, Inv.Colour.Blue, 5, new Inv.Point(200, 100), new Inv.Point(5, 5));

        if (PressPoint != null)
          DC.DrawEllipse(Inv.Colour.DarkOrange.Opacity(0.50F), Inv.Colour.Orange.Opacity(0.50F), 5, PressPoint.Value, new Inv.Point(25, 25));

        if (ZoomPoint != null)
          DC.DrawEllipse(Inv.Colour.Pink.Opacity(0.50F), Inv.Colour.HotPink.Opacity(0.50F), 5, ZoomPoint.Value, new Inv.Point(5, 5));
      };

      Canvas.Draw();
      Surface.ComposeEvent += () => Canvas.Draw();

      var Button = Surface.NewButton();
      Overlay.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.Orange;
      Button.Padding.Set(20);
      Button.Alignment.TopStretch();
      Button.SingleTapEvent += () =>
      {
        var TransitionSurface = Base.Window.NewSurface();
        Base.Window.Transition(TransitionSurface);

        var TransitionPoint = Inv.Point.Zero;

        var TransitionCanvas = TransitionSurface.NewCanvas();
        TransitionSurface.Content = TransitionCanvas;
        TransitionCanvas.Background.Colour = Inv.Colour.WhiteSmoke;
        TransitionCanvas.DrawEvent += (DC) =>
        {
          var DCWidth = DC.Width;
          var DCHeight = DC.Height;

          DC.DrawText(string.Format("{0}, {1}", TransitionPoint.X, TransitionPoint.Y), "", 20, Inv.FontWeight.Regular, Inv.Colour.Blue, new Inv.Point(DCWidth / 2, DCHeight / 2), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);
          //RC.DrawText(string.Format("{0}, {1}", Application.Window.Width, Application.Window.Height), "", 20, Inv.Colour.Red, new Point(RC.Width / 2, RC.Height / 2 + 50), HorizontalPosition.Left, VerticalPosition.Top);

          DC.DrawText("TOP LEFT", "", 30, Inv.FontWeight.Thin, Inv.Colour.Red, new Inv.Point(0, 0), Inv.HorizontalPosition.Left, Inv.VerticalPosition.Top);
          DC.DrawText("TOP MIDDLE", "", 30, Inv.FontWeight.Light, Inv.Colour.Green, new Inv.Point(DCWidth / 2, 0), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Top);
          DC.DrawText("TOP RIGHT", "", 30, Inv.FontWeight.Regular, Inv.Colour.Blue, new Inv.Point(DCWidth, 0), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Top);

          DC.DrawText("BOT LEFT", "", 30, Inv.FontWeight.Medium, Inv.Colour.Red, new Inv.Point(0, DCHeight), Inv.HorizontalPosition.Left, Inv.VerticalPosition.Bottom);
          DC.DrawText("BOT MIDDLE", "", 30, Inv.FontWeight.Bold, Inv.Colour.Green, new Inv.Point(DCWidth / 2, DCHeight), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Bottom);
          DC.DrawText("BOT RIGHT", "", 30, Inv.FontWeight.Heavy, Inv.Colour.Blue, new Inv.Point(DCWidth, DCHeight), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Bottom);
        };
        TransitionCanvas.SingleTapEvent += (Point) =>
        {
          Debug.WriteLine(string.Format("{0}, {1}", Point.X, Point.Y));

          TransitionPoint = Point;

          TransitionCanvas.Draw();
        };
      };
      return Overlay;
    }
    private Inv.Panel CustomStroke(Inv.Surface Surface)
    {
      var Canvas = Surface.NewCanvas();
      Canvas.Background.Colour = Inv.Colour.Black;

      var BorderWidth = 0;

      Canvas.DrawEvent += (RC) =>
      {
        if (BorderWidth == 3)
          throw new Exception("failing inside draw event.");

        var BackBrush = Inv.Colour.Yellow;
        var BackRect = new Inv.Rect(100, 100, 200, 200);
        RC.DrawRectangle(BackBrush, null, 0, BackRect);

        var TopLeftCornerRect = new Inv.Rect(80, 80, 20, 20);
        RC.DrawRectangle(BackBrush, null, 0, TopLeftCornerRect);

        var TopRightCornerRect = new Inv.Rect(300, 80, 20, 20);
        RC.DrawRectangle(BackBrush, null, 0, TopRightCornerRect);

        var BottomLeftCornerRect = new Inv.Rect(80, 300, 20, 20);
        RC.DrawRectangle(BackBrush, null, 0, BottomLeftCornerRect);

        var BottomRightCornerRect = new Inv.Rect(300, 300, 20, 20);
        RC.DrawRectangle(BackBrush, null, 0, BottomRightCornerRect);

        var LeftBrush = Inv.Colour.DarkRed;
        var LeftRect = new Inv.Rect(100, 100, 30, 200);
        RC.DrawRectangle(LeftBrush, Inv.Colour.Red, BorderWidth, LeftRect);

        var RightBrush = Inv.Colour.DarkBlue;
        var RightRect = new Inv.Rect(270, 100, 30, 200);
        RC.DrawRectangle(RightBrush, Inv.Colour.Blue, BorderWidth, RightRect);
      };

      Canvas.SingleTapEvent += (Point) =>
      {
        BorderWidth++;

        Canvas.Draw();
      };

      Canvas.SingleTap(new Inv.Point(0, 0));

      return Canvas;
    }
    private Inv.Panel DockMargins(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.Yellow;

      var VerticalDock = Surface.NewVerticalDock();
      Overlay.AddPanel(VerticalDock);

      var HorizontalDock = Surface.NewHorizontalDock();
      VerticalDock.AddHeader(HorizontalDock);
      HorizontalDock.Margin.Set(4);
      HorizontalDock.Background.Colour = Inv.Colour.SteelBlue;

      var Button = Surface.NewButton();
      HorizontalDock.AddHeader(Button);
      Button.Size.Set(64, 64);
      Button.Background.Colour = Inv.Colour.Red;

      var Bottom = Surface.NewFrame();
      Overlay.AddPanel(Bottom);
      Bottom.Border.Set(1);
      Bottom.Border.Colour = Inv.Colour.White;
      Bottom.Background.Colour = Inv.Colour.Green;
      Bottom.Margin.Set(0, 72, 0, 0);

      return Overlay;
    }
    private Inv.Panel DockOnOverlay(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Scroll = Surface.NewVerticalScroll();
      Overlay.AddPanel(Scroll);

      var Stack = Surface.NewVerticalStack();
      Scroll.Content = Stack;

      for (var Index = 0; Index < 1; Index++)
      {
        var IndexButton = Surface.NewButton();
        Stack.AddPanel(IndexButton);
        IndexButton.Background.Colour = Inv.Colour.SteelBlue;
        IndexButton.Padding.Set(10);
        IndexButton.Margin.Set(5);

        var IndexLabel = Surface.NewLabel();
        IndexButton.Content = IndexLabel;
        IndexLabel.Alignment.Center();
        IndexLabel.Text = "INDEX " + Index;
        IndexLabel.Size.SetHeight(1000);
      }

      var LeftButton = Surface.NewButton();
      Overlay.AddPanel(LeftButton);
      LeftButton.Alignment.TopLeft();
      LeftButton.Background.Colour = Inv.Colour.Orange;
      LeftButton.Size.Set(200, 100);

      var Dock = Surface.NewVerticalDock();
      Overlay.AddPanel(Dock);
      Dock.Alignment.StretchLeft();

      //var TopButton = Surface.NewButton();
      //Dock.AddClient(TopButton);
      //TopButton.Background.Colour = Inv.Colour.Pink;

      var RightButton = Surface.NewButton();
      Dock.AddFooter(RightButton);
      RightButton.Background.Colour = Inv.Colour.Purple;
      RightButton.Size.Set(200, 100);

      return Overlay;
    }
    private Inv.Panel EmptyLabel(Inv.Surface Surface)
    {
      var Stack = Surface.NewVerticalStack();
      Stack.Alignment.Center();

      var L1 = Surface.NewLabel();
      Stack.AddPanel(L1);
      L1.Text = "This is the first line";

      var L2 = Surface.NewLabel();
      Stack.AddPanel(L2);
      L2.Text = "This is the second line";

      var L3 = Surface.NewLabel();
      Stack.AddPanel(L3);
      L3.Text = "The next line is blank:";

      var L4 = Surface.NewLabel();
      Stack.AddPanel(L4);
      L4.Text = "";

      var L5 = Surface.NewLabel();
      Stack.AddPanel(L5);
      L5.Text = "Did you see a blank line above here?";

      return Stack;
    }
    private Inv.Panel HelpFlyout(Inv.Surface Surface)
    {
      var Flyout = Surface.NewOverlay();

      var Memo = Surface.NewMemo();
      Flyout.AddPanel(Memo);
      Memo.Background.Colour = Inv.Colour.Black;
      Memo.Alignment.TopLeft();
      Memo.Margin.Set(50);
      Memo.Padding.Set(50);
      Memo.Font.Colour = Inv.Colour.White;
      Memo.Font.Size = 20;
      Memo.IsReadOnly = true;
      Memo.Text = "1. This is some help\n2. It is multi line\n3. The rest should be obvious\n4. Read this all again and it makes sense\n5. Last line";

      var Button = Surface.NewButton();
      Flyout.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.Blue;
      Button.Size.SetHeight(200);
      Button.Alignment.BottomStretch();
      Button.SingleTapEvent += () =>
      {
        Surface.Rearrange();

        var FadeOutAnimation = Surface.NewAnimation();
        FadeOutAnimation.AddTarget(Memo).FadeOpacityOut(TimeSpan.FromSeconds(1));
        FadeOutAnimation.Start();
      };

      var FadeInAnimation = Surface.NewAnimation();
      FadeInAnimation.AddTarget(Memo).FadeOpacityIn(TimeSpan.FromSeconds(1));
      FadeInAnimation.Start();
      return Flyout;
    }
    private Inv.Panel HiddenOverlays(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Button = new ComplexButton(Surface);
      Overlay.AddPanel(Button);
      Button.Alignment.Center();

      var Label = Surface.NewButton();
      Overlay.AddPanel(Label);
      Label.Background.Colour = Inv.Colour.Black;
      Label.Visibility.Collapse();

      return Overlay;
    }
    private Inv.Panel CallButton(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      //var ContactScroll = Surface.NewVerticalScroll();
      //Overlay.AddElement(ContactScroll);
      //ContactScroll.Alignment.Center();

      var ContactStack = Surface.NewVerticalStack();
      //ContactScroll.Content = ContactStack;
      Overlay.AddPanel(ContactStack);

      var EmailButton = Surface.NewButton();
      ContactStack.AddPanel(EmailButton);
      EmailButton.Background.Colour = Inv.Colour.DarkOrange;
      EmailButton.SingleTapEvent += () =>
      {
        EmailButton.Background.Colour = Inv.Colour.Red;
        EmailButton.Visibility.Collapse();

        var Shade = Surface.NewButton();
        Overlay.AddPanel(Shade);
        Shade.Background.Colour = Inv.Colour.Black.Opacity(0.50F);
        Shade.SingleTapEvent += () => Overlay.RemovePanel(Shade);
        //Application.Email.NewMessage().Send();
      };

      var EmailLabel = Surface.NewLabel();
      EmailButton.Content = EmailLabel;
      EmailLabel.Alignment.Center();
      EmailLabel.Font.Size = 18;
      EmailLabel.Font.Colour = Inv.Colour.White;
      EmailLabel.Padding.Set(15);
      EmailLabel.Text = "john.carey@kestral.com.au";

      var CallButton = Surface.NewButton();
      ContactStack.AddPanel(CallButton);
      CallButton.Background.Colour = Inv.Colour.DarkGreen;
      CallButton.SingleTapEvent += () =>
      {
        CallButton.Visibility.Collapse();
        /*
        if (CallButton.Margin.IsSet)
          CallButton.Margin.Reset();
        else
          CallButton.Margin.Set(20);*/
      };

      var CallLabel = Surface.NewLabel();
      CallButton.Content = CallLabel;
      //CallLabel.Alignment.Center();
      CallLabel.Font.Size = 30;
      CallLabel.Font.Colour = Inv.Colour.White;
      CallLabel.Padding.Set(30);
      CallLabel.Text = "CALL\n+61412171734";

      var SMSButton = Surface.NewButton();
      ContactStack.AddPanel(SMSButton);
      SMSButton.Background.Colour = Inv.Colour.DarkGreen;
      SMSButton.IsEnabled = false;
      SMSButton.SingleTapEvent += () =>
      {
        SMSButton.Visibility.Collapse();
        /*
        if (SMSButton.Padding.IsSet)
          SMSButton.Padding.Reset();
        else
          SMSButton.Padding.Set(20);*/
      };

      var SMSLabel = Surface.NewLabel();
      SMSButton.Content = SMSLabel;
      SMSLabel.Alignment.Center();
      SMSLabel.Font.Size = 30;
      SMSLabel.Font.Colour = Inv.Colour.White;
      SMSLabel.Padding.Set(30);
      SMSLabel.Text = "SMS\n+61412171734";

      var ResetButton = Surface.NewButton();
      ContactStack.AddPanel(ResetButton);
      ResetButton.Background.Colour = Inv.Colour.DarkGreen;
      ResetButton.SingleTapEvent += () =>
      {
        EmailButton.Visibility.Show();
        CallButton.Visibility.Show();
        SMSButton.Visibility.Show();
      };

      var ResetLabel = Surface.NewLabel();
      ResetButton.Content = ResetLabel;
      //ResetLabel.Alignment.Center();
      ResetLabel.Font.Size = 30;
      ResetLabel.Font.Colour = Inv.Colour.White;
      ResetLabel.Padding.Set(30);
      ResetLabel.Text = "RESET";

      return Overlay;
    }
    private Inv.Panel CenteredScrollingStack(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();
      Scroll.Background.Colour = Inv.Colour.Yellow;
      Scroll.Alignment.Center();

      var Stack = Surface.NewVerticalStack();
      Scroll.Content = Stack;
      Stack.Background.Colour = Inv.Colour.Green;
      Stack.Padding.Set(10);

      var Button = Surface.NewButton();
      Stack.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.DarkGray;
      //Button.Size.SetHeight(100);

      var HelloLabel = Surface.NewLabel();
      Stack.AddPanel(HelloLabel);
      HelloLabel.Text = "Hello";
      HelloLabel.Font.Size = 30;
      HelloLabel.Background.Colour = Inv.Colour.Orange;

      var WorldLabel = Surface.NewLabel();
      Stack.AddPanel(WorldLabel);
      WorldLabel.Text = "World";
      WorldLabel.Font.Size = 30;
      WorldLabel.Background.Colour = Inv.Colour.Pink;

      return Scroll;
    }
    private Inv.Panel ClippedCanvas(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();

      var Button = Surface.NewButton();
      Dock.AddHeader(Button);
      Button.Background.Colour = Inv.Colour.White;
      Button.Border.Colour = Inv.Colour.DarkGray;
      Button.Border.Set(1);
      Button.Margin.Set(10);

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Text = "click to draw";
      Label.Font.Size = 30;
      Label.Font.Light();
      Label.Margin.Set(10);
      Label.Alignment.Center();

      var Frame = Surface.NewFrame();
      Dock.AddClient(Frame);
      Frame.Size.Set(200, 100);
      Frame.Alignment.Center();
      Frame.Background.Colour = Inv.Colour.White;

      var Canvas = Surface.NewCanvas();
      Frame.Content = Canvas;

      Canvas.DrawEvent += (DC) => DC.DrawEllipse(Inv.Colour.Pink, Inv.Colour.Transparent, 0, new Inv.Point(100, 50), new Inv.Point(100, 100));

      Button.SingleTapEvent += () => Canvas.Draw();

      return Dock;
    }
    private Inv.Panel ComplexLayout(Inv.Surface Surface)
    {
      var D1 = Surface.NewVerticalDock();

      var D2 = Surface.NewHorizontalDock();
      D1.AddHeader(D2);

      D2.AddHeader(NewLabel(Surface, "D2.H1", Inv.Colour.Blue));
      D2.AddClient(NewLabel(Surface, "D2.C1", Inv.Colour.Green));
      D2.AddFooter(NewLabel(Surface, "D2.F1", Inv.Colour.Red));

      var Scroll = Surface.NewVerticalScroll();
      D1.AddClient(Scroll);

      var S1 = Surface.NewVerticalStack();
      Scroll.Content = S1;
      S1.AddPanel(NewLabel(Surface, "S1.E1", Inv.Colour.Yellow));
      S1.AddPanel(NewLabel(Surface, "S1.E2", Inv.Colour.Gray));
      S1.AddPanel(NewLabel(Surface, "S1.E3", Inv.Colour.Turquoise));
      S1.AddPanel(NewLabel(Surface, "S1.E4", Inv.Colour.Violet));

      var S2 = Surface.NewHorizontalStack();
      D1.AddFooter(S2);
      S2.AddPanel(NewLabel(Surface, "S2.E1", Inv.Colour.Orange));
      S2.AddPanel(NewLabel(Surface, "S2.E2", Inv.Colour.OldLace));
      S2.AddPanel(NewLabel(Surface, "S2.E3", Inv.Colour.Peru));

      return D1;
    }
    private Inv.Panel ComplexMarginAndPadding(Inv.Surface Surface)
    {
      var Stack = Surface.NewVerticalStack();

      var Test1Button = new ComplexButton(Surface);
      Stack.AddPanel(Test1Button);
      Test1Button.Margin.Set(20);

      var Test2Button = new ComplexButton(Surface);
      Stack.AddPanel(Test2Button);
      Test2Button.Margin.Set(20);

      return Stack;
    }
    private Inv.Panel CornerGraphic(Inv.Surface Surface)
    {
      var Frame = Surface.NewFrame();
      Frame.Corner.Set(40);
      Frame.Size.Set(80, 80);
      Frame.Background.Colour = Inv.Colour.Red;
      Frame.Elevation.Set(5);

      var Graphic = Surface.NewGraphic();
      Frame.Content = Graphic;
      Graphic.Corner.Set(40);
      Graphic.Image = Resources.Images.Water128x128;

      return Frame;
    }
    private Inv.Panel DashboardTable(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();
      
      var Table = Surface.NewTable();
      Scroll.Content = Table;

      Table.Background.Colour = Inv.Colour.LightGray;
      Table.Padding.Set(0, 0, 10, 10);

      const int ContentWidth = 400;
      const int ContentHeight = 400;

      var TableWidth = Math.Max(Surface.Window.Width / ContentWidth, 1);
      var TableHeight = (int)Math.Ceiling(6.0 / TableWidth);

      Table.Adjust(TableWidth, TableHeight);

      foreach (var Column in Table.GetColumns())
        Column.Star();

      foreach (var Row in Table.GetRows())
        Row.Auto();

      foreach (var Cell in Table.GetCells())
      {
        var Button = Surface.NewButton();
        Cell.Content = Button;
        Button.Margin.Set(10, 10, 0, 0);
        Button.Background.Colour = Inv.Colour.White;

        var Dock = Surface.NewVerticalDock();
        Button.Content = Dock;

        var HeadingLabel = Surface.NewLabel();
        Dock.AddHeader(HeadingLabel);
        HeadingLabel.Padding.Set(10, 10, 0, 0);
        HeadingLabel.Font.Size = 24;
        HeadingLabel.Font.Medium();
        HeadingLabel.Text = "Title " + Cell.X + ":" + Cell.Y;

        var SubheadingLabel = Surface.NewLabel();
        Dock.AddHeader(SubheadingLabel);
        SubheadingLabel.Padding.Set(10, 0, 10, 10);
        SubheadingLabel.Font.Size = 16;
        SubheadingLabel.Font.Regular();
        SubheadingLabel.Font.Colour = Inv.Colour.DimGray;
        SubheadingLabel.Text = "SUBHEADING";

        var ContentFrame = Surface.NewFrame();
        Dock.AddClient(ContentFrame);
        ContentFrame.Border.Set(0, 1, 0, 0);
        ContentFrame.Border.Colour = Inv.Colour.LightGray;

        var ContentCanvas = Surface.NewCanvas();
        ContentFrame.Content = ContentCanvas;
        ContentCanvas.Size.Set(400, 400);
        ContentCanvas.DrawEvent += (RC) =>
        {
          RC.DrawRectangle(Inv.Colour.White, null, 0, new Inv.Rect(0, 0, ContentWidth, ContentHeight));

          var BarHeight = 50;
          var BarGap = 10;
          var BarY = BarGap;

          for (var Bar = 0; Bar < 6; Bar++)
          {
            RC.DrawRectangle(Inv.Colour.SteelBlue, Inv.Colour.AliceBlue, 1, new Inv.Rect(50, BarY, 250, BarHeight));

            BarY += BarHeight + BarGap;
          }
        };
        ContentCanvas.Draw();
      }

      return Scroll;
    }
    private Inv.Panel DefaultBackground(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      Dock.Padding.Set(10);
      Dock.Background.Colour = Inv.Colour.Yellow;

      Dock.AddClient(Surface.NewBoard());
      Dock.AddClient(Surface.NewButton());

      // WPF Browser background is white.
      // Android Browser is white and incorrectly stretches to the full height.
      // iOS Browser background is white.
      // Uwa Browser seems to be the only one that is correct.
      //var Browser = Surface.NewBrowser();
      //Dock.AddClient(Browser);
      //Browser.Background.Colour = Inv.Colour.Red;

      Dock.AddClient(Surface.NewCanvas());
      Dock.AddClient(Surface.NewFlow());
      Dock.AddClient(Surface.NewFrame());
      Dock.AddClient(Surface.NewGraphic());
      Dock.AddClient(Surface.NewHorizontalDock());
      Dock.AddClient(Surface.NewTextEdit());
      Dock.AddClient(Surface.NewLabel());
      Dock.AddClient(Surface.NewMemo());
      Dock.AddClient(Surface.NewOverlay());
      Dock.AddClient(Surface.NewHorizontalScroll());
      Dock.AddClient(Surface.NewHorizontalStack());
      Dock.AddClient(Surface.NewTable());

      return Dock;
    }
    private Inv.Panel ElevationButton(Inv.Surface Surface)
    {
      var Frame = Surface.NewFrame();
      Frame.Background.Colour = Inv.Colour.DimGray;

      var Button = Surface.NewButton();
      Frame.Content = Button;

      Button.Background.Colour = Inv.Colour.SteelBlue;
      Button.Alignment.Center();
      Button.Size.Set(200, 200);
      Button.Elevation.Set(5);
      Button.SingleTapEvent += () =>
      {
        if (Frame.Alignment.Get() == Inv.Placement.Stretch)
        {
          Frame.Alignment.Center();
        }
        else
        {
          if (Frame.Background.Colour == Inv.Colour.Black)
            Frame.Background.Colour = Inv.Colour.DimGray;
          else
            Frame.Background.Colour = Inv.Colour.Black;
          Frame.Alignment.Stretch();
        }
      };

      return Frame;
    }
    private Inv.Panel MaterialExperiment(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      
      var ListFrame = Surface.NewFrame();
      Overlay.AddPanel(ListFrame);
      ListFrame.Alignment.Center();
      ListFrame.Size.SetWidth(400);

      var List = new Inv.Material.List(Base, Surface);
      ListFrame.Content = List;
      List.AddItem(Resources.Images.PhoenixLogo960x540, "Phoenix Team").SingleTapEvent += () => Overlay.RemovePanel(ListFrame);
      List.AddDivider();
      List.AddSubheading("Staff members");
      List.AddItem(Resources.Images.Waves, "Callan Hodgskin").AddIcon(Inv.Material.Resources.Images.StarsBlack);
      List.AddDivider();
      List.AddItem(Resources.Images.Waves, "Kyle Vermaes").AddIcon(Inv.Material.Resources.Images.StarsBlack);
      List.AddDivider();
      List.AddItem(Resources.Images.Waves, "Simon Williams").AddIcon(Inv.Material.Resources.Images.StarsBlack);

      var FavouriteFrame = Surface.NewFrame();
      Overlay.AddPanel(FavouriteFrame);
      FavouriteFrame.Alignment.TopRight();
      FavouriteFrame.Margin.Set(100);

      var FavouriteButton = new Inv.Material.FloatingActionButton(Base, Surface);
      FavouriteFrame.Content = FavouriteButton;
      FavouriteButton.Image = Inv.Material.Resources.Images.ViewModuleWhite;
      FavouriteButton.SingleTapEvent += () =>
      {
        var Dialog = new Inv.Material.Dialog(Surface);
        Overlay.AddPanel(Dialog);
        Dialog.HideEvent += () => Overlay.RemovePanel(Dialog);

        Dialog.Title = "Use Google's location service?";

        Dialog.SetContentText("Let Google help apps determine location.  This means sending anonymous location data to Google, even when no apps are running.");

        var DisagreeAction = Dialog.Strip.AddAffirmitiveAction();
        DisagreeAction.Caption = "DISAGREE";
        DisagreeAction.Colour = Inv.Colour.DodgerBlue;

        var AgreeAction = Dialog.Strip.AddAffirmitiveAction();
        AgreeAction.Caption = "AGREE";
        AgreeAction.Colour = Inv.Colour.DodgerBlue;
      };

      var RaisedFrame = Surface.NewFrame();
      Overlay.AddPanel(RaisedFrame);
      RaisedFrame.Alignment.CenterRight();
      RaisedFrame.Margin.Set(100);

      var RaisedButton = new Inv.Material.RaisedButton(Surface);
      RaisedFrame.Content = RaisedButton;
      RaisedButton.Caption = "Hello Button World";
      RaisedButton.Colour = Inv.Colour.Purple;

      var ChipStack = Surface.NewHorizontalStack();
      Overlay.AddPanel(ChipStack);
      ChipStack.Alignment.TopLeft();
      ChipStack.Margin.Set(100);

      var CallanChip = new Inv.Material.Chip(Surface);
      ChipStack.AddPanel(CallanChip);
      CallanChip.Icon = Resources.Images.Waves;
      CallanChip.Text = "Callan Hodgskin";
      CallanChip.RemoveEvent += () => { };

      var PhoenixChip = new Inv.Material.Chip(Surface);
      ChipStack.AddPanel(PhoenixChip);
      PhoenixChip.Icon = Resources.Images.PhoenixLogo960x540;
      PhoenixChip.Text = "Phoenix Team";
      PhoenixChip.RemoveEvent += () => { };

      var Snackbar = new Inv.Material.Snackbar(Surface);
      Overlay.AddPanel(Snackbar);
      Snackbar.Text = "Archived";
      Snackbar.Button.Caption = "UNDO";
      Snackbar.Button.Colour = Inv.Colour.Yellow;

      var PopupFrame = Surface.NewFrame();
      Overlay.AddPanel(PopupFrame);
      PopupFrame.Margin.Set(100);
      PopupFrame.Alignment.BottomRight();

      var Popup = new Inv.Material.Popup(Surface);
      PopupFrame.Content = Popup;

      Popup.AddItem(Inv.Material.Resources.Images.LoopBlack, "Cut", "Ctrl+X");
      Popup.AddItem(Inv.Material.Resources.Images.HelpBlack, "Copy", "Ctrl+C");
      Popup.AddItem(Inv.Material.Resources.Images.HelpBlack, "Paste", "Ctrl+V");
      Popup.AddDivider();
      Popup.AddItem(Inv.Material.Resources.Images.LoopBlack, "Refresh", "F5");
      Popup.AddItem(Inv.Material.Resources.Images.HelpBlack, "Help & feedback", "F1");
      Popup.AddItem(Inv.Material.Resources.Images.SettingsApplicationsBlack, "Settings", "F10");
      Popup.AddItem("Sign out").SingleTapEvent += () => Overlay.RemovePanel(PopupFrame);
      
      var CardFrame = Surface.NewFrame();
      Overlay.AddPanel(CardFrame);
      CardFrame.Margin.Set(100);
      CardFrame.Alignment.BottomLeft();
      CardFrame.Size.SetWidth(400);

      var Card = new Inv.Material.Card(Surface);
      CardFrame.Content = Card;

      Card.Header.Avatar = Inv.Material.Resources.Images.BookmarkWhite;
      Card.Header.Title = "Title goes here";
      Card.Header.Subtitle = "subheading";
      Card.Header.BackgroundColour = Inv.Colour.DarkGoldenrod;
      Card.Header.ForegroundColour = Inv.Colour.White;

      Card.AddMedia(Resources.Images.Waves);
      Card.AddSubject("Card subject");
      Card.AddSupport("This is a long line of supporting text that should wordwrap. If you need any more text you should add extra lines. What a long story you are reading.  I don't know why you haven't stopped yet.");
      
      Card.Strip.AddAffirmitiveAction("SHARE").Colour = Inv.Colour.DarkGoldenrod;
      Card.Strip.AddAffirmitiveAction("EXPLORE").Colour = Inv.Colour.DarkGoldenrod;
      
      Card.Strip.AddDismissiveIcon(Inv.Material.Resources.Images.AnnouncementBlack).SingleTapEvent += () => Overlay.RemovePanel(CardFrame);
      Card.Strip.AddDismissiveIcon(Inv.Material.Resources.Images.AccountBoxBlack);
      
      return Overlay;
    }
    private Inv.Panel DeepNested(Inv.Surface Surface)
    {
      var Button = Surface.NewButton();

      var LastFrame = (Inv.Frame)null;
      var Index = 0;

      Button.SingleTapEvent += () =>
      {
        // NOTE: testing for Android 4.x

        var Frame = Surface.NewFrame();
        Frame.Background.Colour = Index++ % 2 == 0 ? Inv.Colour.Red : Inv.Colour.Blue;
        Frame.Padding.Set(1);

        if (LastFrame == null)
          Button.Content = Frame;
        else
          LastFrame.Content = Frame;

        LastFrame = Frame;
      };

      Button.SingleTap();
      Button.SingleTap();
      Button.SingleTap();
      Button.SingleTap();

      return Button;
    }
    private Inv.Panel KeyModifiers(Inv.Surface Surface)
    {
      var Result = Surface.NewLabel();
      Result.Alignment.Center();
      Result.Font.Size = 30;
      Result.Text = "Press and hold ctrl, alt or shift keys.";

      Surface.Window.KeyModifierEvent += () =>
      {
        Result.Text = Surface.Window.KeyModifier.ToString();
      };

      return Result;
    }
    private Inv.Panel BasicMarginAndPadding(Inv.Surface Surface)
    {
      var Button = Surface.NewButton();
      Button.Alignment.Center();
      Button.Background.Colour = Inv.Colour.Red;
      Button.Size.Set(100, 100);
      Button.Margin.Set(10);
      Button.Padding.Set(20, 0, 0, 0);
      Button.SingleTapEvent += () => Surface.Rearrange();

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Padding.Set(10);
      Label.Alignment.Center();
      Label.Background.Colour = Inv.Colour.Green;
      Label.Font.Colour = Inv.Colour.White;
      Label.LineWrapping = true;
      Label.Text = "Hello Mr Wordwrap";
      return Button;
    }
    private Inv.Panel ComplexButton(Inv.Surface Surface)
    {
      var Button = new ComplexButton(Surface);
      Button.Alignment.TopStretch();

      return Button;
    }
    private Inv.Panel ExchangeItems(Inv.Surface Surface)
    {
      var LayoutScroll = Surface.NewVerticalScroll();
      LayoutScroll.Alignment.TopStretch();
      LayoutScroll.Size.SetHeight(305);
      LayoutScroll.Background.Colour = Inv.Colour.SteelBlue;

      var LayoutStack = Surface.NewVerticalStack();
      LayoutScroll.Content = LayoutStack;
      LayoutStack.Margin.Set(10);

      for (var i = 0; i < 10; i++)
      {
        var ItemButton = new CaptionButton(Surface);
        LayoutStack.AddPanel(ItemButton);
        ItemButton.Margin.Set(5);
        ItemButton.Padding.Set(5);
        ItemButton.Text = "Item#" + i;
      }

      return LayoutScroll;
      
      /*
      var ItemPanel = new TilePanel(Surface);
      ItemPanel.Caption = "EXCHANGE";
      ItemPanel.Alignment.BottomStretch();
      ItemPanel.Size.SetHeight(305);
      for (var i = 0; i < 10; i++)
        ItemPanel.AddButton().Set(null, "Item #" + i, null);
      return ItemPanel;
      */
      /*
      var ExchangePanel = new ExchangePanel(Surface);

      ExchangePanel.LeftPanel.Caption = "LEFT";
      ExchangePanel.RightPanel.Caption = "RIGHT";
      ExchangePanel.CommandButton.Text = "SWAP";
      ExchangePanel.LeftButton.Text = "INCLUDE";
      ExchangePanel.RightButton.Text = "EXCLUDE";
      ExchangePanel.Compose(400);
      ExchangePanel.Alignment.BottomStretch();

      for (var i = 0; i < 10; i++)
      {
        ExchangePanel.LeftPanel.AddButton().Set(null, "Lefty #" + i, null);
        ExchangePanel.RightPanel.AddButton().Set(null, "Right #" + i, null);
      }

      return ExchangePanel;*/
    }
    private Inv.Panel LayoutBoard(Inv.Surface Surface)
    {
      var Board = Surface.NewBoard();
      Board.Background.Colour = Inv.Colour.Black;

      var Header1Label = Surface.NewLabel();
      Board.AddPanel(Header1Label, new Inv.Rect(100, 100, 200, 200));
      Header1Label.Background.Colour = Inv.Colour.Blue;
      Header1Label.JustifyCenter();
      Header1Label.Font.Colour = Inv.Colour.Red;
      Header1Label.Font.Size = 30;
      Header1Label.Padding.Set(20);
      Header1Label.Margin.Set(10);
      Header1Label.Text = "Header1";
      return Board;
    }
    private Inv.Panel LayoutDock(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      //Dock.Alignment.Center();
      //Dock.Padding.Set(1);

      const int HeadingFontSize = 30;

      var Header1Label = Surface.NewLabel();
      Dock.AddHeader(Header1Label);
      //Header1Label.Alignment.Center();
      Header1Label.Background.Colour = Inv.Colour.Blue;
      Header1Label.JustifyCenter();
      Header1Label.Font.Colour = Inv.Colour.Red;
      Header1Label.Font.Size = HeadingFontSize;
      Header1Label.Padding.Set(20);
      Header1Label.Margin.Set(10);
      Header1Label.Text = "Header1";
      //Header1Label.Visibility.Collapse();

      var Header2Label = Surface.NewLabel();
      Dock.AddHeader(Header2Label);
      Header2Label.Background.Colour = Inv.Colour.SteelBlue;
      Header2Label.JustifyCenter();
      Header2Label.Font.Colour = Inv.Colour.DarkBlue;
      Header2Label.Font.Size = HeadingFontSize;
      Header2Label.Padding.Set(10);
      Header2Label.Text = "Header2";

      var Client1Label = Surface.NewLabel();
      Dock.AddClient(Client1Label);
      Client1Label.Background.Colour = Inv.Colour.Pink;
      Client1Label.JustifyCenter();
      Client1Label.Font.Colour = Inv.Colour.DarkRed;
      Client1Label.Font.Size = HeadingFontSize;
      Client1Label.Text = "Client1";

      var Client2Label = Surface.NewLabel();
      Dock.AddClient(Client2Label);
      Client2Label.Background.Colour = Inv.Colour.WhiteSmoke;
      Client2Label.JustifyCenter();
      Client2Label.Font.Colour = Inv.Colour.BlueViolet;
      Client2Label.Font.Size = HeadingFontSize;
      Client2Label.Text = "Client2";

      var Footer1Label = Surface.NewLabel();
      Dock.AddFooter(Footer1Label);
      Footer1Label.Background.Colour = Inv.Colour.Yellow;
      Footer1Label.JustifyCenter();
      Footer1Label.Font.Colour = Inv.Colour.Purple;
      Footer1Label.Font.Size = HeadingFontSize;
      Footer1Label.Padding.Set(10);
      Footer1Label.Text = "Footer1";
      //Footer1Label.Visibility.Collapse();

      var Footer2Label = Surface.NewLabel();
      Dock.AddFooter(Footer2Label);
      Footer2Label.Background.Colour = Inv.Colour.DarkGreen;
      Footer2Label.JustifyCenter();
      Footer2Label.Font.Colour = Inv.Colour.White;
      Footer2Label.Font.Size = HeadingFontSize;
      Footer2Label.Padding.Set(20);
      Footer2Label.Text = "Footer2";

      return Dock;
    }
    private Inv.Panel LabelDockLayout(Inv.Surface Surface)
    {
      var HorizontalDock = Surface.NewHorizontalDock();
      HorizontalDock.Background.Colour = Inv.Colour.DimGray;
      HorizontalDock.Alignment.TopStretch();

      var IconGraphic = Surface.NewGraphic();
      HorizontalDock.AddHeader(IconGraphic);
      IconGraphic.Margin.Set(0, 0, 4, 0);

      var VerticalStack = Surface.NewVerticalStack();
      HorizontalDock.AddClient(VerticalStack);
      VerticalStack.Alignment.CenterStretch();

      var DescriptionLabel = Surface.NewLabel();
      VerticalStack.AddPanel(DescriptionLabel);
      DescriptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      DescriptionLabel.Text = "This is a long trailer text that should wrap over to the next line. Let's see what happens when we add even more text to this paragraph. Perhaps a third line? Or a fourth?";

      return HorizontalDock;
    }
    private Inv.Panel LabelWordwrap(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      Dock.Background.Colour = Inv.Colour.White;
      Dock.Padding.Set(0, 24, 0, 0);
      Dock.Alignment.Center();
      Dock.Elevation.Set(24);
      //Dock.Size.SetWidth(300);

      var TitleLabel = Surface.NewLabel();
      Dock.AddHeader(TitleLabel);
      TitleLabel.Padding.Set(24, 0, 24, 20);
      TitleLabel.JustifyLeft();
      TitleLabel.Font.Colour = Inv.Colour.Black;
      TitleLabel.Font.Size = 20;
      TitleLabel.Font.Medium();
      TitleLabel.Text = "Use Google's location service?";

      var ContentFrame = Surface.NewFrame();
      Dock.AddClient(ContentFrame);

      var ContentLabel = Surface.NewLabel();
      ContentFrame.Content = ContentLabel;
      ContentLabel.JustifyLeft();
      ContentLabel.Font.Colour = Inv.Colour.DimGray;
      ContentLabel.Font.Size = 16;
      ContentLabel.Padding.Set(24, 0, 24, 24);
      ContentLabel.Text = "Let Google help apps determine location.  This means sending anonymous location data to Google, even when no apps are running.";

      var ActionDock = Surface.NewHorizontalDock();
      Dock.AddFooter(ActionDock);
      ActionDock.Margin.Set(24, 4, 4, 4);

      return Dock;
    }
    private Inv.Panel LayeredCanvas(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Position = 0;
      var Reverse = false;

      var BackgroundCanvas = Surface.NewCanvas();
      Overlay.AddPanel(BackgroundCanvas);
      BackgroundCanvas.DrawEvent += (DC) =>
      {
        var DCWidth = DC.Width;
        var DCHeight = DC.Height;

        if (Position <= 0)
          Reverse = false;
        else if (Position >= DCWidth)
          Reverse = true;

        if (Reverse)
          Position--;
        else
          Position++;

        var Opacity = Position / 100.0F;
        if (Opacity > 1.0F)
          Opacity = 1.0F;

        DC.DrawEllipse(Inv.Colour.Pink.Opacity(Opacity), Inv.Colour.Transparent, 0, new Inv.Point(Position, 150), new Inv.Point(100, 100));
        DC.DrawText(Surface.Window.DisplayRate.PerSecond.ToString() + " fps", "", 20, Inv.FontWeight.Regular, Inv.Colour.Black, new Inv.Point(DCWidth - 2, DCHeight - 2), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Bottom);
      };

      var ForegroundCanvas = Surface.NewCanvas();
      Overlay.AddPanel(ForegroundCanvas);
      ForegroundCanvas.DrawEvent += (DC) =>
      {
        DC.DrawEllipse(Inv.Colour.Purple.Opacity(0.50F), Inv.Colour.Transparent, 0, new Inv.Point(300, 200), new Inv.Point(100, 100));
      };
      ForegroundCanvas.Draw();

      Surface.ComposeEvent += () =>
      {
        BackgroundCanvas.Draw();
      };

      return Overlay;
    }
    private Inv.Panel Login(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.White;

      var TitleLabel = Surface.NewLabel();
      Overlay.AddPanel(TitleLabel);
      TitleLabel.Alignment.TopLeft();
      TitleLabel.Margin.Set(10);
      TitleLabel.Font.Thin();
      TitleLabel.Font.Size = 60;
      TitleLabel.Text = "HQ";

      var VersionTextBlock = Surface.NewLabel();
      Overlay.AddPanel(VersionTextBlock);
      VersionTextBlock.Alignment.TopRight();
      VersionTextBlock.Font.Colour = Inv.Colour.DimGray;
      VersionTextBlock.Text = "1.0.0.1";

      var CopyrightTextBlock = Surface.NewLabel();
      Overlay.AddPanel(CopyrightTextBlock);
      CopyrightTextBlock.Alignment.BottomCenter();
      CopyrightTextBlock.Font.Colour = Inv.Colour.DimGray;
      CopyrightTextBlock.Text = "Copyright © " + DateTime.Today.Year + " Kestral. All rights reserved.";

      var Scroll = Surface.NewVerticalScroll();
      Overlay.AddPanel(Scroll);

      var Dock = Surface.NewVerticalDock();
      Scroll.Content = Dock;
      Dock.Margin.Set(10);
      Dock.Alignment.Center();
      Dock.Size.SetWidth(300);

      var DomainLabel = Surface.NewLabel();
      Dock.AddHeader(DomainLabel);
      DomainLabel.Font.Colour = Inv.Colour.DimGray;
      DomainLabel.Text = "domain";

      var DomainEdit = Surface.NewTextEdit();
      Dock.AddHeader(DomainEdit);
      DomainEdit.Background.Colour = Inv.Colour.WhiteSmoke;
      DomainEdit.Border.Set(2);
      DomainEdit.Border.Colour = Inv.Colour.Black;
      DomainEdit.Padding.Set(5);
      DomainEdit.Margin.Set(5, 0, 5, 5);
      DomainEdit.Font.Size = 30;
      DomainEdit.Text = "kestral";
      //DomainEdit.ChangeEvent += () => Refresh();

      var UsernameLabel = Surface.NewLabel();
      Dock.AddHeader(UsernameLabel);
      UsernameLabel.Font.Colour = Inv.Colour.DimGray;
      UsernameLabel.Text = "username";

      var UsernameEdit = Surface.NewTextEdit();
      Dock.AddHeader(UsernameEdit);
      UsernameEdit.Background.Colour = Inv.Colour.WhiteSmoke;
      UsernameEdit.Border.Set(2);
      UsernameEdit.Border.Colour = Inv.Colour.Black;
      UsernameEdit.Padding.Set(5);
      UsernameEdit.Margin.Set(5, 0, 5, 5);
      UsernameEdit.Font.Size = 30;
      UsernameEdit.Text = "";
      //UsernameEdit.ChangeEvent += () => Refresh();

      var PasswordLabel = Surface.NewLabel();
      Dock.AddHeader(PasswordLabel);
      PasswordLabel.Font.Colour = Inv.Colour.DimGray;
      PasswordLabel.Text = "password";

      var PasswordEdit = Surface.NewPasswordEdit();
      Dock.AddHeader(PasswordEdit);
      PasswordEdit.Background.Colour = Inv.Colour.WhiteSmoke;
      PasswordEdit.Border.Set(2);
      PasswordEdit.Border.Colour = Inv.Colour.Black;
      PasswordEdit.Padding.Set(5);
      PasswordEdit.Margin.Set(5, 0, 5, 5);
      PasswordEdit.Font.Size = 30;
      PasswordEdit.Text = "";
      //PasswordEdit.ChangeEvent += () => Refresh();

      var ConnectButton = Surface.NewButton();
      Dock.AddFooter(ConnectButton);
      ConnectButton.Margin.Set(5, 15, 5, 5);
      ConnectButton.Padding.Set(5);
      //ConnectButton.SingleTapEvent += () => Connect();

      var ConnectLabel = Surface.NewLabel();
      ConnectButton.Content = ConnectLabel;
      ConnectLabel.Font.Size = 30;
      ConnectLabel.Font.Colour = Inv.Colour.White;
      ConnectLabel.JustifyCenter();
      ConnectLabel.Text = "CONNECT";

      var MessageLabel = Surface.NewLabel();
      Dock.AddFooter(MessageLabel);
      MessageLabel.Font.Colour = Inv.Colour.DarkRed;
      MessageLabel.Font.Size = 16;
      MessageLabel.LineWrapping = true;

      return Overlay;
    }
    private Inv.Panel LongScrollingForm(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();

      var Stack = Surface.NewVerticalStack();
      Scroll.Content = Stack;
      Stack.Margin.Set(10);

      var LastEdit = (Inv.Edit)null;

      for (var I = 0; I < 30; I++)
      {
        var Label = Surface.NewLabel();
        Stack.AddPanel(Label);
        Label.Font.Colour = Inv.Colour.DimGray;
        Label.Text = string.Format("field {0}", I + 1);

        var Edit = Surface.NewTextEdit();
        Stack.AddPanel(Edit);
        Edit.Background.Colour = Inv.Colour.WhiteSmoke;
        Edit.Border.Set(2);
        Edit.Border.Colour = Inv.Colour.Black;
        Edit.Padding.Set(5);
        Edit.Margin.Set(5, 0, 5, 5);
        Edit.Font.Size = 30;

        if (LastEdit != null)
          LastEdit.ReturnEvent += () => Surface.SetFocus(Edit);

        LastEdit = Edit;
      }

      return Scroll;
    }
    private Inv.Panel LongMemo(Inv.Surface Surface)
    {
      var Memo = Surface.NewMemo();
      Memo.IsReadOnly = true;
      Memo.Background.Colour = Inv.Colour.LightGray;

      var StringBuilder = new StringBuilder();
      for (var Index = 0; Index < 24; Index++)
        StringBuilder.AppendLine("LINE " + Index);

      Memo.Text = StringBuilder.ToString();

      return Memo;
    }
    private Inv.Panel EditAndMemo(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      Dock.Background.Colour = Inv.Colour.White;

      var Button = Surface.NewButton();
      Dock.AddHeader(Button);
      Button.Background.Colour = Inv.Colour.Green;
      Button.Size.SetHeight(100);

      var SearchEdit = Surface.NewSearchEdit();
      Dock.AddHeader(SearchEdit);
      SearchEdit.Margin.Set(10);
      //SearchEdit.Padding.Set(10);
      SearchEdit.Background.Colour = Inv.Colour.LightGray;
      SearchEdit.Font.Colour = Inv.Colour.Black;
      SearchEdit.Font.Size = 18;
      SearchEdit.Text = "";

      var TextEditLabel = Surface.NewLabel();
      Dock.AddHeader(TextEditLabel);
      TextEditLabel.Background.Colour = Inv.Colour.Black;
      TextEditLabel.Font.Colour = Inv.Colour.White;
      TextEditLabel.Font.Size = 30;

      SearchEdit.ChangeEvent += () => TextEditLabel.Text = SearchEdit.Text;

      var PasswordEdit = Surface.NewPasswordEdit();
      Dock.AddHeader(PasswordEdit);
      PasswordEdit.Background.Colour = Inv.Colour.WhiteSmoke;
      PasswordEdit.Font.Colour = Inv.Colour.Black;
      PasswordEdit.Font.Size = 30;

      var PasswordEditLabel = Surface.NewLabel();
      Dock.AddHeader(PasswordEditLabel);
      PasswordEditLabel.Background.Colour = Inv.Colour.Black;
      PasswordEditLabel.Font.Colour = Inv.Colour.White;
      PasswordEditLabel.Font.Size = 30;

      PasswordEdit.ChangeEvent += () => PasswordEditLabel.Text = PasswordEdit.Text;

      var PhoneEdit = Surface.NewPhoneEdit();
      Dock.AddHeader(PhoneEdit);
      PhoneEdit.Margin.Set(10);
      //PhoneEdit.Padding.Set(10);
      PhoneEdit.Background.Colour = Inv.Colour.Pink;
      PhoneEdit.Font.Colour = Inv.Colour.Black;
      PhoneEdit.Font.Size = 18;
      PhoneEdit.Text = "+61 407 141 102";

      var PhoneEditLabel = Surface.NewLabel();
      Dock.AddHeader(PhoneEditLabel);
      PhoneEditLabel.Background.Colour = Inv.Colour.Black;
      PhoneEditLabel.Font.Colour = Inv.Colour.White;
      PhoneEditLabel.Font.Size = 30;

      PhoneEdit.ChangeEvent += () => PhoneEditLabel.Text = PhoneEdit.Text;

      var Memo = Surface.NewMemo();
      Dock.AddClient(Memo);
      Memo.Background.Colour = Inv.Colour.LightGray;

      var MemoLabel = Surface.NewLabel();
      Dock.AddFooter(MemoLabel);
      MemoLabel.Background.Colour = Inv.Colour.Black;
      MemoLabel.Font.Colour = Inv.Colour.White;
      MemoLabel.Font.Size = 30;

      Memo.ChangeEvent += () => MemoLabel.Text = Memo.Text;

      Button.SingleTapEvent += () => Surface.SetFocus(SearchEdit);

      SearchEdit.ReturnEvent += () => Surface.SetFocus(PasswordEdit);

      Surface.SetFocus(SearchEdit);

      return Dock;
    }
    private Inv.Panel FileIO(Inv.Surface Surface)
    {
      var TilePanel = new TilePanel(Surface);
      TilePanel.Caption = "DEVICE NAME: " + Surface.Window.Application.Device.Name + Environment.NewLine + Surface.Window.Application.Directory.Installation + Environment.NewLine + "PROCESS ID: " + Surface.Window.Application.Process.Id;
      TilePanel.Footer = "DEVICE MODEL/SYSTEM: " + Surface.Window.Application.Device.Model + " ... " + Surface.Window.Application.Device.System;

      var Folder = Base.Directory.NewFolder("Data");
      var File = Folder.NewFile("Myfile.dat");

      var CreateButton = TilePanel.AddButton();
      CreateButton.Set(null, "Create file", null);
      CreateButton.SingleTapEvent += () =>
      {
        File.AsText().WriteAll("Hello World");
      };

      var ExistsButton = TilePanel.AddButton();
      ExistsButton.Set(null, "Exists file", null);
      ExistsButton.SingleTapEvent += () =>
      {
        ExistsButton.Set(null, "Exists file", File.Exists() ? "YES" : "NO");
      };

      var ReadButton = TilePanel.AddButton();
      ReadButton.Set(null, "Read file", null);
      ReadButton.SingleTapEvent += () =>
      {
        ReadButton.Set(null, "Read file", File.AsText().ReadAll());
      };

      var DeleteButton = TilePanel.AddButton();
      DeleteButton.Set(null, "Delete file", null);
      DeleteButton.SingleTapEvent += () =>
      {
        File.Delete();
      };

      var CopyButton = TilePanel.AddButton();
      CopyButton.Set(null, "Copy file", null);
      CopyButton.SingleTapEvent += () =>
      {
        var TargetFile = Folder.NewFile("Mycopy.dat");

        File.CopyReplace(TargetFile);
      };

      var MoveButton = TilePanel.AddButton();
      MoveButton.Set(null, "Move file", null);
      MoveButton.SingleTapEvent += () =>
      {
        var TargetFile = Folder.NewFile("Mymove.dat");

        File.MoveReplace(TargetFile);
      };

      return TilePanel;
    }
    private Inv.Panel NumberedList(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Table = Surface.NewTable();
      Overlay.AddPanel(Table);
      Table.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);

      var NumberColumn = Table.AddColumn();
      NumberColumn.Auto();

      var TextColumn = Table.AddColumn();
      TextColumn.Star();

      var Row = Table.AddRow();

      var NumberLabel = Surface.NewLabel();
      Table.GetCell(NumberColumn, Row).Content = NumberLabel;
      NumberLabel.Alignment.TopLeft();
      NumberLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      NumberLabel.Font.Size = 20;
      NumberLabel.Font.Weight = Inv.FontWeight.Light;
      NumberLabel.Text = (Row.Index + 1) + ".";

      var TextLabel = Surface.NewLabel();
      Table.GetCell(TextColumn, Row).Content = TextLabel;
      TextLabel.Margin.Set(0, 0, 10, 0);
      TextLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      TextLabel.Font.Size = 20;
      TextLabel.Font.Weight = Inv.FontWeight.Light;
      TextLabel.LineWrapping = true;
      TextLabel.Text = "Select template 'Class Library' (Portable)";

      var TextEdit = Surface.NewTextEdit();
      Overlay.AddPanel(TextEdit);
      TextEdit.Alignment.BottomStretch();
      TextEdit.Font.Size = 20;
      TextEdit.Font.Colour = Inv.Colour.WhiteSmoke;
      TextEdit.Text = TextLabel.Text;
      TextEdit.ChangeEvent += () => TextLabel.Text = TextEdit.Text;

      var AndroidButton = Surface.NewButton();
      Overlay.AddPanel(AndroidButton);
      AndroidButton.Alignment.Center();
      AndroidButton.Size.Set(80, 80);
      AndroidButton.Background.Colour = Inv.Colour.ForestGreen;
      AndroidButton.SingleTapEvent += () =>
      {
        TextEdit.Text = "Select template 'Class Library' (Portable) but the it gun yfghh yfghh buying ytyghhh gas";
        TextLabel.Text = TextEdit.Text;
      };

      return Overlay;
    }
    private Inv.Panel OverlayAlignments(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      foreach (var Placement in Inv.Support.EnumHelper.GetEnumerable<Inv.Placement>())
      {
        var Label = Surface.NewLabel();
        Label.Font.Colour = Inv.Colour.Black;
        Label.Alignment.Set(Placement);

        var LabelPlacement = Label.Alignment.Get();

        if (IsVerticalStretch(LabelPlacement) && IsHorizontalStretch(LabelPlacement))
        {
          Label.Background.Colour = Inv.Colour.Green.Opacity(0.50F);
          Label.Margin.Set(100);
        }
        else if (IsVerticalStretch(LabelPlacement))
        {
          Label.Background.Colour = Inv.Colour.Red.Opacity(0.50F);
          Label.Size.SetWidth(50);
        }
        else if (IsHorizontalStretch(LabelPlacement))
        {
          Label.Background.Colour = Inv.Colour.Blue.Opacity(0.50F);
          Label.Size.SetHeight(50);
        }
        else
        {
          Label.Text = Placement.ToString().Strip(C => char.IsLower(C));
        }

        Overlay.AddPanel(Label);
      }
      return Overlay;
    }
    private Inv.Panel OverlayAndMargins(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var LeftButton = Surface.NewButton();
      Overlay.AddPanel(LeftButton);
      LeftButton.Alignment.BottomLeft();
      LeftButton.Background.Colour = Inv.Colour.Orange;
      LeftButton.Size.Set(150, 50);

      var RightButton = Surface.NewButton();
      Overlay.AddPanel(RightButton);
      RightButton.Alignment.BottomRight();
      RightButton.Background.Colour = Inv.Colour.Purple;
      RightButton.Size.Set(150, 50);
      RightButton.Margin.Set(0, 0, 50, 0);

      return Overlay;
    }
    private Inv.Panel OverlayButtons(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var LeftButton = Surface.NewButton();
      Overlay.AddPanel(LeftButton);
      LeftButton.Alignment.Center();
      LeftButton.Background.Colour = Inv.Colour.Green;
      LeftButton.Border.Colour = Inv.Colour.Orange.Opacity(0.50F);
      LeftButton.Border.Set(10, 10, 10, 0);
      LeftButton.Size.Set(200, 200);
      LeftButton.SingleTapEvent += () =>
      {
        LeftButton.Background.Colour = Inv.Colour.Red;
        LeftButton.Border.Set(LeftButton.Border.Left + 1);
      };

      var RightButton = Surface.NewButton();
      Overlay.AddPanel(RightButton);
      RightButton.Alignment.Center();
      RightButton.Background.Colour = Inv.Colour.Purple;
      RightButton.Size.Set(100, 100);
      RightButton.SingleTapEvent += () => RightButton.Background.Colour = Inv.Colour.Blue;

      return Overlay;
    }
    private Inv.Panel DockLabelAlignments(Inv.Surface Surface)
    {
      var Dock = Surface.NewHorizontalDock();
      Dock.Background.Colour = Inv.Colour.Black.Opacity(0.75F);

      var CaptionLabel = Surface.NewLabel();
      Dock.AddHeader(CaptionLabel);
      CaptionLabel.Text = "UPPER";
      CaptionLabel.Font.Size = 24;
      CaptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      CaptionLabel.Padding.Set(10, 10, 0, 0);
      CaptionLabel.Alignment.TopLeft();

      var DescriptionLabel = Surface.NewLabel();
      Dock.AddClient(DescriptionLabel);
      DescriptionLabel.Text = "description";
      DescriptionLabel.Font.Size = 24;
      DescriptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      DescriptionLabel.Padding.Set(0, 10, 10, 0);
      DescriptionLabel.Alignment.TopRight();

      return Dock;
    }
    private Inv.Panel OverlayRegions(Inv.Surface Surface)
    {
      var Base = Surface.NewOverlay();
      Base.Alignment.BottomCenter();
      Base.Size.SetHeight(Surface.Window.Height);

      var Button = Surface.NewButton();
      Base.AddPanel(Button);
      Button.Alignment.Stretch();
      Button.Background.Colour = Inv.Colour.Red.Opacity(0.50F);
      Button.SingleTapEvent += () => Base.Visibility.Collapse();

      var Label = Surface.NewLabel();
      Base.AddPanel(Label);
      Label.Background.Colour = Inv.Colour.SteelBlue;
      Label.Text = "HELLO";
      Label.Alignment.BottomStretch();

      return Base;
    }
    private Inv.Panel Pickers(Inv.Surface Surface)
    {
      var Stack = Surface.NewVerticalStack();
      Stack.Background.Colour = Inv.Colour.WhiteSmoke;

      var PickedDateTime = DateTime.Now;

      var DateTimeButton = Surface.NewButton();
      Stack.AddPanel(DateTimeButton);

      var DateTimeLabel = Surface.NewLabel();
      DateTimeButton.Content = DateTimeLabel;
      DateTimeLabel.Alignment.Center();
      DateTimeLabel.Font.Size = 20;
      DateTimeLabel.Font.Colour = Inv.Colour.White;
      DateTimeLabel.Text = "DATE TIME";

      DateTimeButton.Background.Colour = Inv.Colour.SteelBlue;
      DateTimeButton.Margin.Set(20);
      DateTimeButton.Padding.Set(20);
      DateTimeButton.Size.SetWidth(300);
      DateTimeButton.Border.Colour = Inv.Colour.Blue;
      DateTimeButton.Alignment.Center();
      DateTimeButton.Elevation.Set(10);
      DateTimeButton.SingleTapEvent += () =>
      {
        var DatePicker = Surface.Window.Application.Calendar.NewDateTimePicker();
        DatePicker.Value = PickedDateTime;
        DatePicker.SelectEvent += () =>
        {
          PickedDateTime = DatePicker.Value;
          DateTimeLabel.Text = PickedDateTime.ToString();
        };
        DatePicker.Show();
      };

      var PickedDate = DateTime.Now.Date;

      var DateButton = Surface.NewButton();
      Stack.AddPanel(DateButton);

      var DateLabel = Surface.NewLabel();
      DateButton.Content = DateLabel;
      DateLabel.Alignment.Center();
      DateLabel.Font.Size = 20;
      DateLabel.Font.Colour = Inv.Colour.White;
      DateLabel.Text = "DATE";

      DateButton.Background.Colour = Inv.Colour.SteelBlue;
      DateButton.Margin.Set(20);
      DateButton.Padding.Set(20);
      DateButton.Size.SetWidth(300);
      DateButton.Border.Colour = Inv.Colour.Blue;
      DateButton.Alignment.Center();
      DateButton.Elevation.Set(10);
      DateButton.SingleTapEvent += () =>
      {
        var DatePicker = Surface.Window.Application.Calendar.NewDatePicker();
        DatePicker.Value = PickedDate;
        DatePicker.SelectEvent += () =>
        {
          PickedDate = DatePicker.Value;
          DateLabel.Text = PickedDate.ToString("dd/MM/yyyy");
        };
        DatePicker.Show();
      };

      var PickedTime = DateTime.MinValue + DateTime.Now.TimeOfDay;

      var TimeButton = Surface.NewButton();
      Stack.AddPanel(TimeButton);

      var TimeLabel = Surface.NewLabel();
      TimeButton.Content = TimeLabel;
      TimeLabel.Alignment.Center();
      TimeLabel.Font.Size = 20;
      TimeLabel.Font.Colour = Inv.Colour.White;
      TimeLabel.Text = "TIME";

      TimeButton.Background.Colour = Inv.Colour.SteelBlue;
      TimeButton.Margin.Set(20);
      TimeButton.Padding.Set(20);
      TimeButton.Size.SetWidth(300);
      TimeButton.Border.Colour = Inv.Colour.Blue;
      TimeButton.Alignment.Center();
      TimeButton.Elevation.Set(10);
      TimeButton.SingleTapEvent += () =>
      {
        var TimePicker = Surface.Window.Application.Calendar.NewTimePicker();
        TimePicker.Value = PickedTime;
        TimePicker.SelectEvent += () =>
        {
          PickedTime = TimePicker.Value;
          TimeLabel.Text = PickedTime.ToString("HH:mm");
        };
        TimePicker.Show();
      };

      var ImageFileButton = Surface.NewButton();
      Stack.AddPanel(ImageFileButton);

      var ImageFileLabel = Surface.NewLabel();
      ImageFileButton.Content = ImageFileLabel;
      ImageFileLabel.Alignment.Center();
      ImageFileLabel.Font.Size = 20;
      ImageFileLabel.Font.Colour = Inv.Colour.White;
      ImageFileLabel.Text = "IMAGE FILE";

      ImageFileButton.Background.Colour = Inv.Colour.SteelBlue;
      ImageFileButton.Margin.Set(20);
      ImageFileButton.Padding.Set(20);
      ImageFileButton.Size.SetWidth(300);
      ImageFileButton.Border.Colour = Inv.Colour.Blue;
      ImageFileButton.Alignment.Center();
      ImageFileButton.Elevation.Set(10);
      ImageFileButton.SingleTapEvent += () =>
      {
        var ImageFilePicker = Surface.Window.Application.Directory.NewImageFilePicker();
        ImageFilePicker.Title = "Select Tile";
        ImageFilePicker.SelectEvent += (Image) =>
        {
          var FileGraphic = Surface.NewGraphic();
          ImageFileButton.Content = FileGraphic;
          FileGraphic.Image = Image;
        };
        ImageFilePicker.Show();
      };
      return Stack;
    }
    private Inv.Panel Portal(Inv.Surface Surface)
    {
      var TilePanel = new TilePanel(Surface);

      for (var Index = 1; Index <= 20; Index++)
      {
        var TileButton = TilePanel.AddButton();
        TileButton.Set(LogoImage, Index + ". TITLE", "description" + Index);
        TileButton.SingleTapEvent += () =>
        {
          TilePanel.RemoveButton(TileButton);
          Surface.Rearrange();
        };
      }

      return TilePanel;
    }
    private Inv.Panel Records(Inv.Surface Surface)
    {
      var NavigateDock = Surface.NewVerticalDock();
      
      NavigateDock.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);

      var NavigateLabel = Surface.NewLabel();
      NavigateDock.AddHeader(NavigateLabel);
      NavigateLabel.Font.Colour = Inv.Colour.White;
      NavigateLabel.Font.Size = 24;
      NavigateLabel.Text = "Records";
      NavigateLabel.Margin.Set(10);

      var RecordFrame = Surface.NewFrame();
      NavigateDock.AddClient(RecordFrame);

      var RecordPanel = new RecordPanel(Surface);
      RecordFrame.Content = RecordPanel;
      RecordPanel.Alignment.Center();

      for (var Index = 0; Index < 20; Index++)
      {
        var RecordButton = RecordPanel.AddButton();
        RecordButton.Rank = Index;
        RecordButton.Identity = "Identity" + Index;
        RecordButton.Summary = "long summary text";
        RecordButton.Fame = (10 - Index + 1) * 1000;
        RecordButton.AddImage(LogoImage);
      }

      var BackButton = new CaptionButton(Surface);
      NavigateDock.AddFooter(BackButton);
      BackButton.Colour = Inv.Colour.DarkGreen;
      BackButton.Text = "BACK";
      BackButton.Margin.Set(10);
      BackButton.Size.SetHeight(60);

      return NavigateDock;
    }
    private Inv.Panel Reclamation(Inv.Surface Surface)
    {
      var Button = Surface.NewButton();
      Button.Background.Colour = Inv.Colour.DarkRed;
      Button.Margin.Set(20);
      Button.Padding.Set(20);
      Button.Size.Set(300, 300);
      Button.Alignment.Center();
      Button.SingleTapEvent += () => Surface.Window.Application.Process.MemoryReclamation();

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Text = "RECLAMATION";
      Label.Font.Size = 24;
      Label.Font.Colour = Inv.Colour.WhiteSmoke;
      Label.Padding.Set(0, 10, 10, 0);
      Label.JustifyCenter();

      return Button;
    }
    private Inv.Panel RoundButton(Inv.Surface Surface)
    {
      var Button = Surface.NewButton();
      Button.Background.Colour = Inv.Colour.FromArgb(0x80, 0xFF, 0x00, 0x00);
      Button.Margin.Set(20);
      Button.Padding.Set(20);
      Button.Size.Set(200, 200);
      Button.Alignment.Center();
      Button.Border.Colour = Inv.Colour.FromArgb(0x80, 0x00, 0xFF, 0x00);

      var InnerBox = Surface.NewFrame();
      InnerBox.Size.AutoMaximum();
      InnerBox.Background.Colour = Inv.Colour.FromArgb(0x80, 0xFF, 0xFF, 0xFF);
      Button.Content = InnerBox;

      var Configurations = new Action[]
      {
        () =>
        {
          Button.Border.Set(20, 10, 5, 0);
          Button.Corner.Set(100, 100, 100, 0);
        },
        () =>
        {
          Button.Border.Set(0);
          Button.Corner.Set(100, 100, 100, 0);
        },
        () =>
        {
          Button.Border.Set(0);
          Button.Corner.Set(0);
        },
        () =>
        {
          // TODO: Just changing the thickness like this does not trigger an update
          Button.Border.Set(100, 0, 50, 10);
          Button.Corner.Set(0);
        },
        () =>
        {
          Button.Border.Set(50, 10, 5, 0);
          Button.Corner.Set(100, 400, 100, 0);
        },
        () =>
        {
          Button.Border.Set(50, 10, 5, 5);
          Button.Corner.Set(800, 800, 100, 800);
        },
        () =>
        {
          Button.Border.Set(50, 10, 5, 5);
          Button.Corner.Set(50);
        },
        () =>
        {
          Button.Border.Set(10);
          Button.Corner.Set(50);
        }
      };

      var Index = 0;
      Action SwitchButton = () => Configurations[Index++ % Configurations.Length]();

      Button.SingleTapEvent += SwitchButton;
      SwitchButton();
      
      return Button;
    }
    private Inv.Panel FrameFeatures(Inv.Surface Surface)
    {
      var Frame = Surface.NewFrame();
      Frame.Alignment.Center();
      Frame.Size.Set(200, 200);
      Frame.Margin.Set(10);
      Frame.Padding.Set(10);
      Frame.Corner.Set(10);
      Frame.Elevation.Set(10);
      Frame.Border.Colour = Inv.Colour.Red;
      Frame.Border.Set(10);
      Frame.Background.Colour = Inv.Colour.Green;

      var Label = Surface.NewLabel();
      Frame.Content = Label;
      Label.Alignment.Stretch();
      Label.Background.Colour = Inv.Colour.Purple;

      return Frame;
    }
    private Inv.Panel ServerCase(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Frame = Surface.NewButton();
      Overlay.AddPanel(Frame);
      Frame.Background.Colour = Inv.Colour.HotPink;
      Frame.Size.Set(88, 88);
      Frame.Alignment.TopLeft();

      var Button = Surface.NewButton();
      Overlay.AddPanel(Button);

      Button.Background.Colour = Inv.Colour.SteelBlue;
      Button.Alignment.Center();
      Button.Size.Set(200, 200);
      Button.Elevation.Set(5);
      Button.SingleTapEvent += () =>
      {
        if (Button.IsEnabled)
        {
          Button.IsEnabled = false;
          Button.Background.Colour = Inv.Colour.Blue;

          Surface.Window.Sleep(TimeSpan.FromMilliseconds(1000));
        }
        else
        {
          // should not be possible.
          Button.Background.Colour = Inv.Colour.Red;
        }
      };

      Frame.SingleTapEvent += () =>
      {
        Button.IsEnabled = true;
        Button.Background.Colour = Inv.Colour.SteelBlue;
      };

      return Overlay;
    }
    private Inv.Panel SettingsFlyout(Inv.Surface Surface)
    {
      var Flyout = Surface.NewOverlay();

      var Scroll = Surface.NewVerticalScroll();
      Flyout.AddPanel(Scroll);
      Scroll.Alignment.StretchLeft();
      Scroll.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);
      Scroll.Size.SetWidth(250);
      Scroll.Padding.Set(20);

      var Dock = Surface.NewVerticalDock();
      //Flyout.AddElement(Dock);
      Scroll.Content = Dock;
      //Dock.Alignment.StretchLeft();

      var ButtonHeight = 100;
      var ButtonMargin = 10;

      var TitleLabel = Surface.NewLabel();
      Dock.AddFooter(TitleLabel);
      TitleLabel.Alignment.Center();
      TitleLabel.Margin.Set(ButtonMargin);
      TitleLabel.Font.Colour = Inv.Colour.White;
      TitleLabel.Font.Size = 30;
      TitleLabel.Text = "SETTINGS";

      var LanguageStack = Surface.NewVerticalStack();
      Dock.AddFooter(LanguageStack);
      LanguageStack.Margin.Set(ButtonMargin);

      var LanguageArray = new[]
        {
          new
          {
            Code = "en",
            Name = "English"
          },
          new
          {
            Code = "fr",
            Name = "Français"
          }
        };

      var LanguageButtonList = new Inv.DistinctList<CaptionButton>(LanguageArray.Length);

      foreach (var Language in LanguageArray)
      {
        var LanguageButton = new CaptionButton(Surface);
        LanguageStack.AddPanel(LanguageButton);
        LanguageButton.Padding.Set(20);
        LanguageButton.SingleTapEvent += () =>
        {
          foreach (var EachButton in LanguageButtonList)
            EachButton.Colour = LanguageButton == EachButton ? Inv.Colour.Purple : Inv.Colour.DimGray;
        };
        LanguageButton.Colour = Language.Code == System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName ? Inv.Colour.Purple : Inv.Colour.DimGray;
        LanguageButton.Text = Language.Name;

        LanguageButtonList.Add(LanguageButton);
      }

      var SoundButton = Surface.NewButton();
      Dock.AddFooter(SoundButton);
      SoundButton.Alignment.Center();
      SoundButton.Size.Set(100, 100);
      SoundButton.Corner.Set(25);
      SoundButton.Margin.Set(0, 10, 0, 10);

      var SoundGraphic = Surface.NewGraphic();
      SoundButton.Content = SoundGraphic;
      SoundGraphic.Size.Set(64, 64);

      var Muted = false;

      SoundButton.SingleTapEvent += () =>
      {
        Muted = !Muted;

        SoundButton.Background.Colour = Muted ? Inv.Colour.DimGray : Inv.Colour.DimGray;
        SoundGraphic.Image = Muted ? Resources.Images.SoundOff256x256 : Resources.Images.SoundOn256x256;
      };

      SoundButton.Background.Colour = Muted ? Inv.Colour.DimGray : Inv.Colour.DimGray;
      SoundGraphic.Image = Muted ? Resources.Images.SoundOff256x256 : Resources.Images.SoundOn256x256;

      var DiagnosticsButton = new CaptionButton(Surface);
      Dock.AddFooter(DiagnosticsButton);
      DiagnosticsButton.Colour = Inv.Colour.DimGray;
      DiagnosticsButton.Size.SetHeight(ButtonHeight / 2);
      DiagnosticsButton.Margin.Set(ButtonMargin);
      DiagnosticsButton.Text = "DIAGNOSTICS";
      DiagnosticsButton.SingleTapEvent += () =>
      {
      };

      return Flyout;
    }
    private Inv.Panel SideFlyout(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = null;

      var SideDock = Surface.NewVerticalDock();
      Overlay.AddPanel(SideDock);
      SideDock.Alignment.Stretch();
      SideDock.Background.Colour = Inv.Colour.Black;
      SideDock.Size.SetWidth(420);

      var FooterDock = Surface.NewVerticalDock();
      SideDock.AddClient(FooterDock);
      FooterDock.Background.Colour = Inv.Colour.Green;
      FooterDock.Alignment.BottomStretch();

      var TrailerLabel = Surface.NewLabel();
      SideDock.AddFooter(TrailerLabel);
      TrailerLabel.Background.Colour = Inv.Colour.Pink;
      TrailerLabel.Font.Size = 20;
      TrailerLabel.Text = "TRAILER";

      var FooterTitle = Surface.NewLabel();
      FooterDock.AddHeader(FooterTitle);
      FooterTitle.Font.Colour = Inv.Colour.White;
      FooterTitle.Font.Size = 30;
      FooterTitle.Text = "FOOTER";

      //var FooterScroll = Surface.NewVerticalScroll();
      //FooterDock.AddClient(FooterScroll);

      var FooterStack = Surface.NewVerticalStack();
      FooterDock.AddClient(FooterStack);
      //FooterScroll.Content = FooterStack;

      var SummaryButton = Surface.NewButton();
      FooterStack.AddPanel(SummaryButton);

      var SummaryDock = Surface.NewHorizontalDock();
      SummaryButton.Content = SummaryDock;

      var SummaryGraphic = Surface.NewGraphic();
      SummaryDock.AddHeader(SummaryGraphic);
      SummaryGraphic.Size.Set(64, 64);
      SummaryGraphic.Image = LogoImage;

      var SummaryStack = Surface.NewVerticalStack();
      SummaryDock.AddClient(SummaryStack);
      SummaryStack.Background.Colour = Inv.Colour.Orange;
      SummaryStack.Padding.Set(10);

      var SummaryTitle = Surface.NewLabel();
      SummaryStack.AddPanel(SummaryTitle);
      SummaryTitle.Background.Colour = Inv.Colour.Blue;
      SummaryTitle.Font.Colour = Inv.Colour.White;
      SummaryTitle.Font.Size = 30;
      SummaryTitle.Text = "TITLE";

      var SummaryDescription = Surface.NewLabel();
      SummaryStack.AddPanel(SummaryDescription);
      SummaryDescription.Background.Colour = Inv.Colour.Red;
      SummaryDescription.Font.Colour = Inv.Colour.White;
      SummaryDescription.Font.Size = 30;
      SummaryDescription.Text = "description";

      return Overlay;
    }
    private Inv.Panel SplitScreen(Inv.Surface Surface)
    {
      var Dock = Surface.NewHorizontalDock();

      var LeftSelected = false;

      var LeftCanvas = Surface.NewCanvas();
      Dock.AddClient(LeftCanvas);
      LeftCanvas.Border.Set(0, 0, 1, 0);
      LeftCanvas.Border.Colour = Inv.Colour.Black;
      LeftCanvas.PressEvent += (Point) =>
      {
        LeftSelected = true;
        LeftCanvas.Draw();
      };
      LeftCanvas.ReleaseEvent += (Point) =>
      {
        LeftSelected = false;
        LeftCanvas.Draw();
      };
      LeftCanvas.DrawEvent += (DC) => DC.DrawEllipse(LeftSelected ? Inv.Colour.Purple : Inv.Colour.Pink, Inv.Colour.Transparent, 0, new Inv.Point(100, 100), new Inv.Point(100, 100));
      LeftCanvas.Draw();

      var RightSelected = false;

      var RightCanvas = Surface.NewCanvas();
      Dock.AddClient(RightCanvas);
      RightCanvas.Border.Set(1, 0, 0, 0);
      RightCanvas.Border.Colour = Inv.Colour.Black;
      RightCanvas.PressEvent += (Point) =>
      {
        RightSelected = true;
        RightCanvas.Draw();
      };
      RightCanvas.ReleaseEvent += (Point) =>
      {
        RightSelected = false;
        RightCanvas.Draw();
      };
      RightCanvas.DrawEvent += (DC) => DC.DrawEllipse(RightSelected ? Inv.Colour.DarkOliveGreen : Inv.Colour.Orange, Inv.Colour.Transparent, 0, new Inv.Point(100, 100), new Inv.Point(100, 100));
      RightCanvas.Draw();

      return Dock;
    }
    private Inv.Panel SpriteLoadTesting(Inv.Surface Surface)
    {
      var Canvas = Surface.NewCanvas();

      Canvas.DrawEvent += (RC) =>
      {
        var RCWidth = RC.Width;
        var RCHeight = RC.Height;
        if (RCWidth == 0 || RCHeight == 0)
          return;

        RC.DrawRectangle(Inv.Colour.Black, null, 0, new Inv.Rect(0, 0, RCWidth, RCHeight));

        var TileSize = 16;
        var BoardWidth = RCWidth / TileSize;
        var BoardHeight = RCHeight / TileSize;

        var CellY = 0;

        for (var Y = 0; Y <= BoardHeight; Y++)
        {
          var CellX = 0;

          for (var X = 0; X <= BoardWidth; X++)
          {
            var Rect = new Inv.Rect(CellX, CellY, TileSize, TileSize);

            RC.DrawImage(Resources.Images.Water128x128, Rect);

            CellX += TileSize;
          }

          CellY += TileSize;
        }

        RC.DrawText("Inv (" + BoardWidth + " by " + BoardHeight + " @ " + TileSize + "x" + TileSize + ") " + Surface.Window.DisplayRate.PerSecond + " fps", "", 20, Inv.FontWeight.Regular, Inv.Colour.White, new Inv.Point(2, RCHeight - 2), Inv.HorizontalPosition.Left, Inv.VerticalPosition.Bottom);
      };

      Surface.ComposeEvent += () => Canvas.Draw();

      return Canvas;
    }
    private Inv.Panel StackAlignmentHorizontal(Inv.Surface Surface)
    {
      var VerticalStack = Surface.NewVerticalStack();
      VerticalStack.Background.Colour = Inv.Colour.Black;

      var H1 = Surface.NewHorizontalStack();
      VerticalStack.AddPanel(H1);
      H1.Background.Colour = Inv.Colour.WhiteSmoke;
      H1.Margin.Set(5);
      H1.Alignment.StretchLeft();

      var B1 = Surface.NewButton();
      H1.AddPanel(B1);
      B1.Background.Colour = Inv.Colour.Blue;
      B1.Size.Set(140, 140);
      B1.Margin.Set(5);

      var B2 = Surface.NewButton();
      H1.AddPanel(B2);
      B2.Background.Colour = Inv.Colour.Green;
      B2.Size.Set(140, 140);
      B2.Margin.Set(5);

      var H2 = Surface.NewHorizontalStack();
      VerticalStack.AddPanel(H2);
      H2.Background.Colour = Inv.Colour.Wheat;
      H1.Margin.Set(5);
      H2.Alignment.StretchRight();

      var B3 = Surface.NewButton();
      H2.AddPanel(B3);
      B3.Background.Colour = Inv.Colour.Red;
      B3.Size.Set(140, 140);
      B3.Margin.Set(5);

      var B4 = Surface.NewButton();
      H2.AddPanel(B4);
      B4.Background.Colour = Inv.Colour.Yellow;
      B4.Size.Set(140, 140);
      B4.Margin.Set(5);

      return VerticalStack;
    }
    private Inv.Panel StackAlignmentVertical(Inv.Surface Surface)
    {
      var HorizontalStack = Surface.NewHorizontalStack();
      HorizontalStack.Background.Colour = Inv.Colour.Black;

      var V1 = Surface.NewVerticalStack();
      HorizontalStack.AddPanel(V1);
      V1.Background.Colour = Inv.Colour.WhiteSmoke;
      V1.Margin.Set(5);
      V1.Alignment.TopStretch();

      var B1 = Surface.NewButton();
      V1.AddPanel(B1);
      B1.Background.Colour = Inv.Colour.Blue;
      B1.Size.Set(140, 140);
      B1.Margin.Set(5);

      var B2 = Surface.NewButton();
      V1.AddPanel(B2);
      B2.Background.Colour = Inv.Colour.Green;
      B2.Size.Set(140, 140);
      B2.Margin.Set(5);

      var V2 = Surface.NewVerticalStack();
      HorizontalStack.AddPanel(V2);
      V2.Background.Colour = Inv.Colour.Wheat;
      V1.Margin.Set(5);
      V2.Alignment.BottomStretch();

      var B3 = Surface.NewButton();
      V2.AddPanel(B3);
      B3.Background.Colour = Inv.Colour.Red;
      B3.Size.Set(140, 140);
      B3.Margin.Set(5);

      var B4 = Surface.NewButton();
      V2.AddPanel(B4);
      B4.Background.Colour = Inv.Colour.Yellow;
      B4.Size.Set(140, 140);
      B4.Margin.Set(5);

      return HorizontalStack;
    }
    private Inv.Panel StackInStack(Inv.Surface Surface)
    {
      var S1 = Surface.NewHorizontalStack();
      S1.Background.Colour = Inv.Colour.Blue;
      S1.Alignment.TopStretch();
      S1.Margin.Set(10);
      S1.Padding.Set(10);

      var OneLabel = Surface.NewLabel();
      S1.AddPanel(OneLabel);
      OneLabel.Background.Colour = Inv.Colour.White;
      OneLabel.Text = "One";
      OneLabel.Padding.Set(10);
      OneLabel.Margin.Set(10);

      var TwoLabel = Surface.NewLabel();
      S1.AddPanel(TwoLabel);
      TwoLabel.Background.Colour = Inv.Colour.White;
      TwoLabel.LineWrapping = true;
      TwoLabel.Text = "Two\nwith a second\nline...";
      TwoLabel.Padding.Set(10);
      TwoLabel.Margin.Set(10);

      var S2 = Surface.NewVerticalStack();
      S1.AddPanel(S2);
      S2.Background.Colour = Inv.Colour.Orange;
      S2.Padding.Set(10);
      S2.Margin.Set(10);

      var ALabel = Surface.NewLabel();
      S2.AddPanel(ALabel);
      ALabel.Background.Colour = Inv.Colour.White;
      ALabel.Text = "A";
      ALabel.Padding.Set(10);
      ALabel.Margin.Set(10);

      var BLabel = Surface.NewLabel();
      S2.AddPanel(BLabel);
      BLabel.Background.Colour = Inv.Colour.White;
      BLabel.Text = "B";
      BLabel.Padding.Set(10);
      BLabel.Margin.Set(10);

      return S1;
    }
    private Inv.Panel TopStretchStack(Inv.Surface Surface)
    {
      var S1 = Surface.NewHorizontalStack();
      S1.Background.Colour = Inv.Colour.Orange;
      S1.Alignment.TopStretch();
      S1.Padding.Set(7, 7, 7, 0);

      var Button = Surface.NewButton();
      S1.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.Red;
      Button.Size.Set(48, 48);

      var Graphic = Surface.NewGraphic();
      //S1.AddElement(Graphic);
      Graphic.Background.Colour = Inv.Colour.Red;
      Graphic.Size.Set(48, 48);
      Graphic.Image = LogoImage;

      var DescriptionLabel = Surface.NewLabel();
      //S1.AddElement(DescriptionLabel);
      DescriptionLabel.Alignment.CenterLeft();
      DescriptionLabel.Background.Colour = Inv.Colour.Blue;
      DescriptionLabel.Font.Size = 20;
      //DescriptionLabel.Margin.Set(5, 0, 5, 0);
      DescriptionLabel.Text = "Description goes here";
      return S1;
    }
    private Inv.Panel TileFlow(Inv.Surface Surface)
    {
      var Frame = Surface.NewFrame();
      //Frame.Background.Colour = Inv.Colour.White;
      //Frame.Background.Colour = Inv.Colour.LightGray;
      Frame.Background.Colour = Inv.Colour.Black;

      var Flow = Surface.NewFlow();
      Frame.Content = Flow;
      Flow.RefreshEvent += (Refresh) =>
      {
        Surface.Window.RunTask((Thread) =>
        {
          Thread.Yield(TimeSpan.FromSeconds(2));
          Thread.Post(() =>
          {
            Refresh.Complete();
          });
        });
      };

      var EmptySection = Flow.AddSection();
      {
        var Label = Surface.NewLabel();
        Label.Text = "Empty section";
        Label.Font.Bold();
        Label.JustifyRight();
        Label.Font.Colour = Inv.Colour.White;
        Label.Background.Colour = Inv.Colour.SteelBlue;
        Label.Alignment.Stretch();
        EmptySection.SetHeader(Label);
      }

      var MySection = Flow.AddSection();
      {
        var Label = Surface.NewLabel();
        Label.Text = "My section";
        Label.Font.Bold();
        Label.JustifyRight();
        Label.Font.Colour = Inv.Colour.White;
        Label.Background.Colour = Inv.Colour.Orange;
        Label.Alignment.Stretch();
        MySection.SetHeader(Label);
      }
      
      {
        var Label = Surface.NewLabel();
        Label.Text = "Total items: " + MySection.ItemCount;
        Label.Font.Bold();
        Label.JustifyLeft();
        Label.Font.Colour = Inv.Colour.White;
        Label.Background.Colour = Inv.Colour.Orange;
        Label.Alignment.Stretch();
        MySection.SetFooter(Label);
      }
      
      MySection.ItemQuery += (Item) =>
      {
        var Button = Surface.NewButton();
        Button.Background.Colour = Item % 2 == 0 ? Inv.Colour.DimGray : Inv.Colour.DarkGray;

        var Label = Surface.NewLabel();
        Button.Content = Label;
        Label.Text = Item.ToString();
        Label.Font.Colour = Inv.Colour.White;
        Label.Font.Size = 6 + Item;

        return Button;
      };
      MySection.SetItemCount(200);

      var LastSection = Flow.AddSection();
      {
        var Label = Surface.NewLabel();
        Label.Text = "Footer only section";
        Label.Font.Bold();
        Label.JustifyRight();
        Label.Font.Colour = Inv.Colour.White;
        Label.Background.Colour = Inv.Colour.HotPink;
        Label.Alignment.Stretch();
        LastSection.SetFooter(Label);
      }
      LastSection.ItemQuery += (Item) =>
      {
        var Button = Surface.NewButton();
        Button.Background.Colour = Inv.Colour.DimGray;

        var Label = Surface.NewLabel();
        Button.Content = Label;
        Label.Text = Item.ToString();
        Label.Font.Colour = Inv.Colour.White;

        return Button;
      };
      LastSection.SetItemCount(1);

      return Frame;
    }
    private Inv.Panel TimerCountdown(Inv.Surface Surface)
    {
      var Countdown = 10;

      var Label = Surface.NewLabel();
      Label.Alignment.Center();
      Label.Background.Colour = Inv.Colour.Green;
      Label.Size.Set(100, 100);
      Label.Corner.Set(50);
      Label.JustifyCenter();
      Label.Elevation.Set(5);
      Label.Font.Colour = Inv.Colour.White;
      Label.Font.Size = 50;
      Label.Text = Countdown.ToString();

      var Timer = Base.Window.NewTimer();
      Timer.IntervalTime = TimeSpan.FromSeconds(0.5);
      Timer.IntervalEvent += () =>
      {
        Countdown--;

        if (Countdown <= 0)
          Timer.Stop();

        Label.Text = Countdown.ToString();
      };
      Timer.Start();

      return Label;
    }
    private Inv.Panel TestPattern(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.DarkGray;

      var LogicalSizeLabel = Surface.NewLabel();
      Overlay.AddPanel(LogicalSizeLabel);
      LogicalSizeLabel.Alignment.BottomLeft();
      LogicalSizeLabel.Font.Size = 18;
      LogicalSizeLabel.Text = Base.Window.Width + " x " + Base.Window.Height;

      var FrameRateLabel = Surface.NewLabel();
      Overlay.AddPanel(FrameRateLabel);
      FrameRateLabel.Alignment.BottomRight();
      FrameRateLabel.Font.Size = 18;

      var LocationLabel = Surface.NewLabel();
      Overlay.AddPanel(LocationLabel);
      LocationLabel.Alignment.TopCenter();
      LocationLabel.Font.Size = 18;
      LocationLabel.Text = "LOCATION!!!ABC";
      LocationLabel.Margin.Set(40);

      var Graphic = Surface.NewGraphic();
      Overlay.AddPanel(Graphic);
      Graphic.Alignment.BottomCenter();
      Graphic.Image = LogoImage;
      Graphic.Padding.Set(20);

      var Button = Surface.NewButton();
      Overlay.AddPanel(Button);
      //Button.Visibility.Collapse();
      Button.Alignment.Center();
      //Button.Size.Set(320, 320);
      Button.Padding.Set(50);
      Button.Background.Colour = Inv.Colour.Green;
      Button.SingleTapEvent += () =>
      {
        Base.Window.Application.Audio.Play(Resources.Sounds.Clang, 0.1F);
        Debug.WriteLine("left click");
      };
      Button.ContextTapEvent += () =>
      {
        Base.Window.Application.Audio.Play(Resources.Sounds.Clang);
        Debug.WriteLine("right click");
      };

      var Stack = Surface.NewVerticalStack();
      Button.Content = Stack;
      Stack.Alignment.Center();

      var HeadingFontSize = 72;

      var FirstLabel = Surface.NewLabel();
      Stack.AddPanel(FirstLabel);
      FirstLabel.Background.Colour = Inv.Colour.Blue;
      FirstLabel.JustifyCenter();
      FirstLabel.Font.Colour = Inv.Colour.Red;
      FirstLabel.Font.Size = HeadingFontSize;
      FirstLabel.Text = "HW";

      var SecondLabel = Surface.NewLabel();
      Stack.AddPanel(SecondLabel);
      SecondLabel.Background.Colour = Inv.Colour.Yellow;
      SecondLabel.JustifyCenter();
      SecondLabel.Font.Colour = Inv.Colour.Purple;
      SecondLabel.Font.Size = HeadingFontSize;
      SecondLabel.Text = "GW";

      Base.Location.ChangeEvent += (Coordinate) =>
      {
        LocationLabel.Text = Coordinate.Latitude + ", " + Coordinate.Longitude + ", " + Coordinate.Altitude;
        /*
        var PlacemarkList = Application.Location.Lookup(Coordinate).GetPlacemarks().ToDistinctList();

        MobileBroker.Call(C => C.UpdateLocationCoordinate(MobileContext, new Kestral.HQ.Mobile.LocationCoordinate()
        {
          Latitude = Coordinate.Latitude,
          Longitude = Coordinate.Longitude,
          Description = PlacemarkList.Count > 0 ? PlacemarkList[0].Name : null
        }));*/
      };

      Surface.ComposeEvent += () =>
      {
        FrameRateLabel.Text = string.Format("{0} FPS | {1} PC | {2:F1} MB", Base.Window.DisplayRate.PerSecond, Surface.GetPanelCount(), Base.Process.GetMemoryUsage());
      };

      return Overlay;
    }
    /*private Inv.Panel Transitions(Inv.Surface Surface)
    {
      var FirstSurface = Base.Window.NewCustomSurface();
      var SecondSurface = Base.Window.NewCustomSurface();
      FirstSurface.ArrangeEvent += () =>
      {
        FirstSurface.Background.Colour = Inv.Colour.Black;

        var FirstButton = FirstSurface.NewButton();
        FirstSurface.Content = FirstButton;
        FirstButton.SingleTapEvent += () => Base.Window.Transition(SecondSurface).CarouselNext();

        var FirstLabel = FirstSurface.NewLabel();
        FirstButton.Content = FirstLabel;
        FirstLabel.Background.Colour = Inv.Colour.LightBlue;
        FirstLabel.Font.Size = 60;
        FirstLabel.JustifyCenter();
        FirstLabel.Text = "FIRST SURFACE";
      };
      SecondSurface.ArrangeEvent += () =>
      {
        SecondSurface.Background.Colour = Inv.Colour.Black;

        var SecondButton = SecondSurface.NewButton();
        SecondSurface.Content = SecondButton;
        SecondButton.SingleTapEvent += () => Base.Window.Transition(FirstSurface).CarouselBack();

        var SecondLabel = SecondSurface.NewLabel();
        SecondButton.Content = SecondLabel;
        SecondLabel.Background.Colour = Inv.Colour.LightPink;
        SecondLabel.Font.Size = 60;
        SecondLabel.JustifyCenter();
        SecondLabel.Text = "SECOND SURFACE";
      };

      return FirstSurface;
    }*/
    private Inv.Panel TableAdjustments(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();
      Scroll.Background.Colour = Inv.Colour.White;

      var Dock = Surface.NewVerticalDock();
      Scroll.Content = Dock;

      var TableScroll = Surface.NewHorizontalScroll();
      Dock.AddClient(TableScroll);

      var Table = Surface.NewTable();
      TableScroll.Content = Table;
      Table.Margin.Set(4);
      Table.Adjust(3, 3);

      Action<Inv.TableColumn, Inv.TableRow> ResetCells = (HighlightColumn, HighlightRow) =>
      {
        foreach (var Cell in Table.GetCells())
        {
          var Label = Surface.NewLabel();
          Cell.Content = Label;
          Label.Text = string.Format("({0}, {1})", Cell.X, Cell.Y);
          Label.Font.Size = 20;
          Label.Padding.Set(10);

          if (Cell.Column == HighlightColumn || Cell.Row == HighlightRow)
          {
            Label.Font.Colour = Inv.Colour.White;
            Label.Background.Colour = Inv.Colour.Red;
          }
        }
      };

      ResetCells(null, null);

      var Rand = new Random();

      for (var I = 0; I < 4; I++)
      {
        var Button = Surface.NewButton();
        Dock.AddHeader(Button);
        Button.Alignment.TopStretch();
        Button.Margin.Set(4, 4, 4, 0);
        Button.Background.Colour = Inv.Colour.SkyBlue;
        Button.Padding.Set(10);

        var ButtonLabel = Surface.NewLabel();
        Button.Content = ButtonLabel;
        ButtonLabel.Alignment.Center();
        ButtonLabel.Font.Colour = Inv.Colour.White;
        ButtonLabel.Font.Size = 14;

        switch (I)
        {
          case 0:
            ButtonLabel.Text = "Insert Row";
            Button.SingleTapEvent += () =>
            {
              var Row = Table.InsertRow(Rand.Next(Table.RowCount + 1));
              ResetCells(null, Row);
            };
            break;

          case 1:
            ButtonLabel.Text = "Remove Row";
            Button.SingleTapEvent += () =>
            {
              if (Table.RowCount > 0)
              {
                Table.RemoveRow(Table.GetRow(Rand.Next(Table.RowCount)));
                ResetCells(null, null);
              }
            };
            break;

          case 2:
            ButtonLabel.Text = "Insert Column";
            Button.SingleTapEvent += () =>
            {
              var Column = Table.InsertColumn(Rand.Next(Table.ColumnCount + 1));
              ResetCells(Column, null);
            };
            break;

          case 3:
            ButtonLabel.Text = "Remove Column";
            Button.SingleTapEvent += () =>
            {
              if (Table.ColumnCount > 0)
              {
                Table.RemoveColumn(Table.GetColumn(Rand.Next(Table.ColumnCount)));
                ResetCells(null, null);
              }
            };
            break;
        }
      }

      return Scroll;
    }
    private Inv.Panel TableArrangement(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();

      var TableCount = 3;
      var ClientCount = 2;
      var TableSize = 3;

      for (var Index = 0; Index < TableCount; Index++)
      {
        var Table = Surface.NewTable();
        switch (Index % 3)
        {
          case 0:
            Table.Background.Colour = Inv.Colour.Red;
            break;
          case 1:
            Table.Background.Colour = Inv.Colour.Green;
            break;
          case 2:
            Table.Background.Colour = Inv.Colour.Blue;
            break;
        }
        Table.Margin.Set(5);
        Table.Adjust(TableSize, TableSize);

        foreach (var Cell in Table.GetCells())
        {
          var Label = Surface.NewLabel();
          Cell.Content = Label;
          Label.Text = string.Join("\n", Enumerable.Repeat(new string('x', Cell.Y + 1), Cell.X + 1));
          Label.Font.Size = 20;
          Label.Font.Colour = Inv.Colour.White;
          Label.Margin.Set(10);
        }

        if (Index >= TableCount - ClientCount)
          Dock.AddClient(Table);
        else
          Dock.AddHeader(Table);
      }
      
      return Dock;
    }
    private Inv.Panel UniformTable(Inv.Surface Surface)
    {
      var Table = Surface.NewTable();
      Table.Background.Colour = Inv.Colour.Black;
      Table.Margin.Set(5);
      Table.Adjust(9, 9);

      Table.GetColumn(0).Star();
      Table.GetColumn(8).Star();

      var ColumnFrame = Surface.NewFrame();
      Table.GetColumn(1).Content = ColumnFrame;
      ColumnFrame.Background.Colour = Inv.Colour.Pink.Opacity(0.50F);

      var RowFrame = Surface.NewFrame();
      Table.GetRow(1).Content = RowFrame;
      RowFrame.Background.Colour = Inv.Colour.Orange.Opacity(0.50F);

      Table.GetColumn(4).Fixed(150);
      Table.GetRow(4).Fixed(150);

      Table.GetRow(0).Star();
      Table.GetRow(8).Star();

      Table.GetCells().ForEach(Cell =>
      {
        var Button = Surface.NewButton();
        Button.Background.Colour = Inv.Colour.DimGray.Opacity(0.50F);
        Button.Margin.Set(5);
        Button.SingleTapEvent += () => Button.Background.Colour = Inv.Colour.Green;

        var Label = Surface.NewLabel();
        Button.Content = Label;
        Label.Alignment.Center();
        Label.Font.Colour = Inv.Colour.White;
        Label.Font.Size = 20;
        Label.Text = "[" + Cell.X + ", " + Cell.Y + "]";

        Cell.Content = Button;
      });

      return Table;
    }
    private Inv.Panel UnhandledException(Inv.Surface Surface)
    {
      var Button = Surface.NewButton();
      Button.Background.Colour = Inv.Colour.DarkGray;
      Button.Padding.Set(100);

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Text = "Press for an exception";

      var Memo = Surface.NewMemo();
      Memo.IsReadOnly = true;

      Button.SingleTapEvent += () =>
      {
        if (Button.Content == Label)
        {
          Button.Content = Memo;

          try
          {
            throw new Exception("i'm an exception");
          }
          catch (Exception Ex)
          {
            Memo.Text = Ex.StackTrace;

            throw Ex;
          }
        }
        else
        {
          var FaultLogFile = Surface.Window.Application.Directory.NewFolder("Logs").NewFile("Fault.log");
          FaultLogFile.AsText().WriteAll(Memo.Text);

          var EmailMessage = Surface.Window.Application.Email.NewMessage();
          EmailMessage.Subject = "Fault";
          EmailMessage.Attach("Fault.log", FaultLogFile);
          EmailMessage.Send();
        }
      };

      return Button;
    }
    private Inv.Panel Vault(Inv.Surface Surface)
    {
      var Button = Surface.NewButton();
      Button.Background.Colour = Inv.Colour.DarkGray;
      Button.Padding.Set(100);

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Text = "Save secret";

      Button.SingleTapEvent += () =>
      {
        var Secret = Surface.Window.Application.Vault.NewSecret("Credentials");

        //Secret.Delete();

        Secret.Load();
        
        Secret.Properties["Domain"] = "kestral";
        Secret.Properties["Username"] = "phxdemo";
        Secret.Properties["Password"] = "lartsek137!";
        
        Secret.Save();
      };

      return Button;
    }
    private Inv.Panel WebBrowser(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();

      var Browser = Surface.NewBrowser();
      Dock.AddClient(Browser);
      Browser.Background.Colour = Inv.Colour.DarkGray;

      var Button = Surface.NewButton();
      Dock.AddHeader(Button);
      Button.Background.Colour = Inv.Colour.SteelBlue;
      Button.Size.SetHeight(100);
      //Button.SingleTapEvent += () => Browser.LoadUri(new Uri("http://www.kestral.com.au/")); // NOTE: kestral.com.au is bogus with something failing and causing it not to load?
      Button.SingleTapEvent += () => Browser.LoadUri(new Uri("http://m.bom.gov.au/wa/perth/"));

      //Browser.LoadUri(new Uri("http://www.kestral.com.au/"));
      Browser.LoadUri(new Uri("http://www.google.com/"));

      //var Buffer = Resources.Documents.Guide.GetBuffer();
      //Browser.LoadHtml(System.Text.Encoding.UTF8.GetString(Buffer, 0, Buffer.Length));

      return Dock;
    }
    private Inv.Panel StarredTableOnOverlay(Inv.Surface Surface)
    {
      var Graphic = Surface.NewGraphic();
      Graphic.Image = Resources.Images.Waves;
      Graphic.Border.Set(1);
      Graphic.Border.Colour = Inv.Colour.Black;

      var Overlay = Surface.NewOverlay();
      Overlay.Alignment.Center();
      Overlay.Background.Colour = Inv.Colour.Pink;
      Overlay.Margin.Set(10);

      var Frame = Surface.NewFrame();
      Overlay.AddPanel(Graphic);

      var Table = Surface.NewTable();
      Overlay.AddPanel(Table);

      var Row = Table.AddRow();
      Row.Star();

      var Column = Table.AddColumn();
      Column.Star();

      return Overlay;
    }

    private string LoadBoomark()
    {
      try
      {
        return (BookmarkFile.Exists() ? BookmarkFile.AsText().ReadAll() : "").Trim().EmptyAsNull();
      }
      catch
      {
        return null;
      }
    }
    private void SaveBookmark()
    {
      try
      {
        BookmarkFile.AsText().WriteAll(MethodItemList[MethodIndex].Caption);
      }
      catch
      {
        // NOTE: nothing to do if we fail to save the bookmark.
      }
    }
    private void Direct(Func<Inv.Surface, Inv.Panel> Default)
    {
      var DirectSurface = Base.Window.NewSurface();

      DirectSurface.Content = Default(DirectSurface);

      Base.Window.Transition(DirectSurface);
    }
    private void Compile(Func<Inv.Surface, Inv.Panel> Default, string Bookmark)
    {
      var DefaultMethodInfo = Default != null ? Default.GetReflectionInfo() : null;

      var TypeInfo = typeof(Application).GetReflectionInfo();

      var MethodList = TypeInfo.GetReflectionMethods().Where(M => M.GetParameters().Length == 1 && M.GetParameters()[0].ParameterType == typeof(Inv.Surface) && M.ReturnType == typeof(Inv.Panel)).ToDistinctList();
      MethodList.Sort((A, B) => string.Compare(A.Name, B.Name));

      this.MethodIndex = -1;
      this.MethodItemList = new Inv.DistinctList<Inv.Material.DrawerItem>(MethodList.Count);

      var NavigationSurface = Base.NewNavigationSurface();

      var ReflectionSurface = (Inv.Surface)NavigationSurface;

      NavigationSurface.Title = "Invention";
      NavigationSurface.Toolbar.Title = "Material";
      //NavigationSurface.Toolbar.AddTrailingIcon(Inv.Material.Resources.Images.SearchWhite);
      NavigationSurface.KeystrokeEvent += (Keystroke) =>
      {
        switch (Keystroke.Key)
        {
          case Inv.Key.F12:
            var MethodItem = MethodItemList[MethodIndex];
            MethodItem.SingleTap();

            NavigationSurface.Rearrange();  
            break;

          case Inv.Key.Up:
            NavigationSurface.GestureBackward();
            break;

          case Inv.Key.Down:
            NavigationSurface.GestureForward();
            break;

          case Inv.Key.Period:
            NavigationSurface.Drawer.ScrollItem(MethodItemList[MethodIndex]);
            break;
        }

        Debug.WriteLine("KEYSTROKE: " + Keystroke.ToString());
      };
      NavigationSurface.GestureBackwardEvent += () =>
      {
        MethodIndex--;
        if (MethodIndex < 0)
          MethodIndex = MethodList.Count - 1;

        var MethodItem = MethodItemList[MethodIndex];
        MethodItem.SingleTap();
      };
      NavigationSurface.GestureForwardEvent += () =>
      {
        MethodIndex++;
        if (MethodIndex >= MethodItemList.Count)
          MethodIndex = 0;

        var MethodItem = MethodItemList[MethodIndex];
        MethodItem.SingleTap();
      };

      var FirstFrame = true;
      var FirstSW = new Stopwatch();
      NavigationSurface.ArrangeEvent += () =>
      {
        var MethodInfo = MethodList[MethodIndex];

        Debug.WriteLine(MethodInfo.Name + ": loading...");
        FirstFrame = true;
      };
      NavigationSurface.ComposeEvent += () =>
      {
        var MethodInfo = MethodList[MethodIndex];

        if (FirstFrame)
        {
          FirstFrame = false;
          FirstSW.Reset();
          FirstSW.Start();
        }
        else if (FirstSW.IsRunning)
        {
          // SecondFrame.
          FirstSW.Stop();
          Debug.WriteLine(MethodInfo.Name + ": " + FirstSW.ElapsedMilliseconds + " ms");
        }
      };

      foreach (var MethodInfo in MethodList)
      {
        var MethodItem = NavigationSurface.Drawer.AddItem(MethodInfo.Name.PascalCaseToTitleCase());
        MethodItem.SingleTapEvent += () =>
        {
          var MethodPanel = (Inv.Panel)MethodInfo.Invoke(this, new object[] { ReflectionSurface });

          NavigationSurface.Toolbar.Title = MethodInfo.Name.PascalCaseToTitleCase();
          NavigationSurface.SetClient(MethodPanel);
          NavigationSurface.Rearrange();

          foreach (var OtherItem in MethodItemList)
            OtherItem.IsHighlighted = OtherItem == MethodItem;

          MethodIndex = MethodItemList.IndexOf(MethodItem);
        };

        if (Bookmark != null && MethodItem.Caption == Bookmark)
          MethodIndex = MethodItemList.Count; // always go with the bookmark if possible.
        else if (Default != null && Default.GetReflectionInfo() == MethodInfo && MethodIndex < 0)
          MethodIndex = MethodItemList.Count; // otherwise fallback to the default method.

        MethodItemList.Add(MethodItem);
      }

      if (MethodIndex < 0)
        MethodIndex = 0;

      var DefaultItem = MethodItemList[MethodIndex];
      DefaultItem.SingleTap();

      NavigationSurface.Drawer.ScrollItem(DefaultItem);

      Base.Window.Transition(NavigationSurface);
    }
    private Inv.Label NewLabel(Inv.Surface Surface, string Text, Inv.Colour Colour)
    {
      var Result = Surface.NewLabel();
      Result.Text = Text;
      Result.Font.Size = 20;
      Result.Background.Colour = Colour;
      return Result;
    }
    private bool IsHorizontalStretch(Inv.Placement Placement)
    {
      return Placement == Inv.Placement.Stretch || Placement == Inv.Placement.TopStretch || Placement == Inv.Placement.CenterStretch || Placement == Inv.Placement.BottomStretch;
    }
    private bool IsVerticalStretch(Inv.Placement Placement)
    {
      return Placement == Inv.Placement.Stretch || Placement == Inv.Placement.StretchLeft || Placement == Inv.Placement.StretchCenter || Placement == Inv.Placement.StretchRight;
    }

    private Inv.Image LogoImage;
    private Inv.DistinctList<Inv.Material.DrawerItem> MethodItemList;
    private int MethodIndex;
    private Inv.File BookmarkFile;
  }

  internal sealed class ComplexButton : Inv.Mimic<Inv.Button>
  {
    public ComplexButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Padding.Set(10);
      Base.Background.Colour = Inv.Colour.WhiteSmoke;
      Base.SingleTapEvent += () =>
      {
        Debug.WriteLine("test");
      };
      
      var TestDock = Surface.NewHorizontalDock();
      Base.Content = TestDock;

      var TestFrame = Surface.NewFrame();
      TestDock.AddHeader(TestFrame);
      TestFrame.Background.Colour = Inv.Colour.DarkOrange;
      TestFrame.Corner.Set(10);
      TestFrame.Size.Set(44, 44);
      TestFrame.Margin.Set(10);

      var TypeLabel = Surface.NewLabel();
      TestFrame.Content = TypeLabel;
      TypeLabel.Font.Size = 25;
      TypeLabel.Font.Colour = Inv.Colour.White;
      TypeLabel.Alignment.Center();
      TypeLabel.Text = "A";

      var StaffLabel = Surface.NewLabel();
      TestDock.AddClient(StaffLabel);
      StaffLabel.Font.Size = 22;
      StaffLabel.Text = "Callan Hodgskin";
      StaffLabel.Alignment.CenterLeft();
    }

    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
  }

  internal sealed class CustomPanel : Inv.Mimic<Inv.Dock>
  {
    public CustomPanel(Inv.Surface Surface)
    {
      this.Base = Surface.NewVerticalDock();
      Base.Background.Colour = Inv.Colour.Black.Opacity(0.75F);

      var HeaderDock = Surface.NewHorizontalDock();
      Base.AddHeader(HeaderDock);

      this.CaptionLabel = Surface.NewLabel();
      HeaderDock.AddHeader(CaptionLabel);
      CaptionLabel.Visibility.Collapse();
      CaptionLabel.Font.Size = 24;
      CaptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      CaptionLabel.Padding.Set(10, 10, 0, 0);
      CaptionLabel.Alignment.TopLeft();

      this.DescriptionLabel = Surface.NewLabel();
      HeaderDock.AddClient(DescriptionLabel);
      DescriptionLabel.Visibility.Collapse();
      DescriptionLabel.Font.Size = 24;
      DescriptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      DescriptionLabel.Padding.Set(0, 10, 10, 0);
      DescriptionLabel.Alignment.TopRight();

      this.FooterLabel = Surface.NewLabel();
      Base.AddFooter(FooterLabel);
      FooterLabel.Margin.Set(10);
      FooterLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      FooterLabel.Font.Size = 14;
      FooterLabel.Visibility.Collapse();
    }

    public string Caption
    {
      get { return CaptionLabel.Text; }
      set
      {
        CaptionLabel.Text = value;
        CaptionLabel.Visibility.Set(value != null);
      }
    }
    public string Description
    {
      get { return DescriptionLabel.Text; }
      set
      {
        DescriptionLabel.Text = value;
        DescriptionLabel.Visibility.Set(value != null);
      }
    }
    public string Footer
    {
      set
      {
        FooterLabel.Text = value;
        FooterLabel.Visibility.Set(value != null);
      }
    }
    public Inv.Background Background
    {
      get { return Base.Background; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Panel Content
    {
      get { return ContentPanel; }
      set
      {
        if (ContentPanel != value)
        {
          if (ContentPanel != null)
            Base.RemoveClient(ContentPanel);

          this.ContentPanel = value;

          if (ContentPanel != null)
            Base.AddClient(ContentPanel);
        }
      }
    }

    private Inv.Label CaptionLabel;
    private Inv.Label DescriptionLabel;
    private Inv.Label FooterLabel;
    private Inv.Panel ContentPanel;
  }

  public sealed class CustomButton : Inv.Mimic<Inv.Button>
  {
    public CustomButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.SingleTapEvent += () =>
      {
        if (IsClickable && SingleTapEvent != null)
          SingleTapEvent();
      };
      Base.ContextTapEvent += () =>
      {
        if (IsClickable && ContextTapEvent != null)
          ContextTapEvent();
      };
      Base.PressEvent += () =>
      {
        if (IsClickable && PressEvent != null)
          PressEvent();
      };
      Base.ReleaseEvent += () =>
      {
        if (IsClickable && ReleaseEvent != null)
          ReleaseEvent();
      };

      this.IsClickable = true;
    }

    public Inv.Panel Content
    {
      get { return Base.Content; }
      set { Base.Content = value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public bool IsFocused
    {
      get { return Base.IsFocusable; }
      set { Base.IsFocusable = value; }
    }
    public bool IsClickable
    {
      get { return IsClickableField; }
      set
      {
        if (IsClickableField != value)
        {
          this.IsClickableField = value;

          Base.Background.Colour = IsClickableField ? Inv.Colour.DimGray : Inv.Colour.Transparent;
        }
      }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Padding Padding
    {
      get { return Base.Padding; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Corner Corner
    {
      get { return Base.Corner; }
    }
    public Inv.Colour BorderColour
    {
      get { return Base.Border.Colour; }
      set { Base.Border.Colour = value; }
    }
    public Inv.Border BorderThickness
    {
      get { return Base.Border; }
    }
    public Inv.Colour BackgroundColour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public event Action SingleTapEvent;
    public event Action ContextTapEvent;
    public event Action PressEvent;
    public event Action ReleaseEvent;

    public void SingleTap()
    {
      Base.SingleTap();
    }

    private bool IsClickableField;
  }

  internal sealed class CustomQuery : Inv.Mimic<Inv.Overlay>
  {
    public CustomQuery(Inv.Surface Surface)
    {
      this.Base = Surface.NewOverlay();
      Base.Visibility.Collapse();
      Base.Visibility.ChangeEvent += () =>
      {
        if (Base.Visibility.Get())
        {
          if (ShowEvent != null)
            ShowEvent();
        }
        else
        {
          if (HideEvent != null)
            HideEvent();
        }
      };

      this.Button = new ShadeButton(Surface);
      Base.AddPanel(Button);
      Button.ShadeColour = Inv.Colour.Red.Opacity(0.50F);
      Button.SingleTapEvent += () => Hide();
      Button.ContextTapEvent += () => Hide();
    }

    public event Action ShowEvent;
    public event Action HideEvent;
    public Inv.Colour ShadeColour
    {
      // NOTE: don't use Button.ShadeColour because it will give the shade a hover effect.
      set { Base.Background.Colour = value; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Panel Content
    {
      get { return ContentPanel; }
      set
      {
        if (ContentPanel != value)
        {
          if (ContentPanel != null)
            Base.RemovePanel(ContentPanel);

          this.ContentPanel = value;

          if (ContentPanel != null)
            Base.AddPanel(ContentPanel);
        }
      }
    }

    public void Show()
    {
      Base.Visibility.Show();
    }
    public void Hide()
    {
      Base.Visibility.Collapse();
    }

    private ShadeButton Button;
    private Inv.Panel ContentPanel;
  }

  internal sealed class ShadeButton : Inv.Mimic<Inv.Button>
  {
    public ShadeButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Alignment.Stretch();

      ShadeColour = Inv.Colour.Transparent;
    }

    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public float Opacity
    {
      get { return Base.Opacity.Get(); }
      set { Base.Opacity.Set(value); }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Border Border
    {
      get { return Base.Border; }
    }
    public Inv.Colour ShadeColour
    {
      set { Base.Background.Colour = value; }
    }
    public Inv.Panel Content
    {
      get { return Base.Content; }
      set { Base.Content = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }
  }

  internal sealed class RecordPanel : Inv.Mimic<Inv.Button>
  {
    public RecordPanel(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = new ShadeButton(Surface);
      Base.Border.Set(2);
      Base.Border.Colour = Inv.Colour.Transparent;
      Base.SingleTapEvent += () =>
      {
        if (ClickEvent != null)
          ClickEvent();
      };

      this.LayoutScroll = Surface.NewVerticalScroll();
      Base.Content = LayoutScroll;

      this.LayoutStack = Surface.NewVerticalStack();
      LayoutScroll.Content = LayoutStack;

      this.ButtonList = new Inv.DistinctList<RecordButton>();
    }

    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Padding
    {
      get { return LayoutScroll.Margin; }
    }
    public event Action ClickEvent;

    public bool HasButtons()
    {
      return ButtonList.Count > 0;
    }
    public void RemoveButtons()
    {
      ButtonList.Clear();
      LayoutStack.RemovePanels();
    }
    public RecordButton AddButton()
    {
      var Result = new RecordButton(this);

      LayoutStack.AddPanel(Result);
      ButtonList.Add(Result);

      return Result;
    }

    internal Inv.Surface Surface { get; private set; }

    private Inv.Stack LayoutStack;
    private Inv.Scroll LayoutScroll;
    private Inv.DistinctList<RecordButton> ButtonList;
  }

  internal sealed class RecordButton : Inv.Mimic<Inv.Dock>
  {
    internal RecordButton(RecordPanel Panel)
    {
      this.Panel = Panel;

      this.Base = Panel.Surface.NewHorizontalDock();
      Base.Margin.Set(0, 0, 0, 10);

      this.RankLabel = Panel.Surface.NewLabel();
      Base.AddHeader(RankLabel);
      RankLabel.Font.Colour = Inv.Colour.LightGray;
      RankLabel.Alignment.Center();
      RankLabel.Corner.Set(25);
      RankLabel.Background.Colour = Inv.Colour.Transparent;
      RankLabel.Border.Set(2);
      RankLabel.Border.Colour = Inv.Colour.LightGray;
      RankLabel.JustifyCenter();
      RankLabel.Size.Set(44, 44);

      this.FameLabel = Panel.Surface.NewLabel();
      Base.AddFooter(FameLabel);
      FameLabel.Alignment.Center();
      FameLabel.Font.Colour = Inv.Colour.White;
      FameLabel.Margin.Set(0, 0, 5, 0);

      var IdentityStack = Panel.Surface.NewVerticalStack();
      Base.AddClient(IdentityStack);
      IdentityStack.Alignment.CenterStretch();

      this.IdentityLabel = Panel.Surface.NewLabel();
      IdentityStack.AddPanel(IdentityLabel);
      IdentityLabel.LineWrapping = true;
      IdentityLabel.Font.Colour = Inv.Colour.White;
      IdentityLabel.Margin.Set(10, 0, 10, 0);

      this.SummaryLabel = Panel.Surface.NewLabel();
      IdentityStack.AddPanel(SummaryLabel);
      SummaryLabel.LineWrapping = true;
      SummaryLabel.Margin.Set(10, 0, 10, 0);
      SummaryLabel.Font.Colour = Inv.Colour.DarkGray;
      SummaryLabel.Visibility.Collapse();
    }

    public bool IsHighlighted
    {
      set { Base.Background.Colour = value ? Inv.Colour.Purple : Inv.Colour.Transparent; }
    }
    public int Rank
    {
      set { RankLabel.Text = value.ToString("N0"); }
    }
    public int Fame
    {
      set { FameLabel.Text = value.ToString("N0"); }
    }
    public string Identity
    {
      set { IdentityLabel.Text = value; }
    }
    public string Summary
    {
      set
      {
        SummaryLabel.Text = value;
        SummaryLabel.Visibility.Set(value != null);
      }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }

    public void SetFontSize(bool IsNarrow)
    {
      RankLabel.Font.Size = IsNarrow ? 20 : 28;
      FameLabel.Font.Size = IsNarrow ? 20 : 28;
      IdentityLabel.Font.Size = IsNarrow ? 16 : 24;
      SummaryLabel.Font.Size = IsNarrow ? 10 : 14;
    }
    public void AddImage(Inv.Image Image)
    {
      var Graphic = Panel.Surface.NewGraphic();
      Base.AddFooter(Graphic);
      Graphic.Size.Set(48, 48);
      Graphic.Image = Image;
    }

    private Inv.Label RankLabel;
    private Inv.Label IdentityLabel;
    private Inv.Label FameLabel;
    private Inv.Label SummaryLabel;
    private RecordPanel Panel;
  }

  internal sealed class TilePanel
  {
    public TilePanel(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = new CustomPanel(Surface);

      this.LayoutScroll = Surface.NewVerticalScroll();
      Base.Content = LayoutScroll;

      this.LayoutStack = Surface.NewVerticalStack();
      LayoutScroll.Content = LayoutStack;
      LayoutStack.Margin.Set(10, 0, 10, 10);

      this.ItemList = new Inv.DistinctList<TileItem>();
    }

    public string Caption
    {
      set { Base.Caption = value; }
    }
    public string Description
    {
      set { Base.Description = value; }
    }
    public string Footer
    {
      set { Base.Footer = value; }
    }
    public Inv.Background Background
    {
      get { return Base.Background; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Margin Padding
    {
      get { return LayoutScroll.Margin; }
    }

    public void Compose(Inv.DistinctList<TileItem> ItemList)
    {
      if (!this.ItemList.ShallowEqualTo(ItemList))
      {
        LayoutStack.RemovePanels();
        foreach (var Item in ItemList)
          LayoutStack.AddPanel(Item.Base);

        this.ItemList = ItemList;
      }
    }
    public bool HasItems()
    {
      return ItemList.Count > 0;
    }
    public void RemoveItems()
    {
      ItemList.Clear();
      LayoutStack.RemovePanels();
    }
    public void RemoveButton(TileButton Button)
    {
      LayoutStack.RemovePanel(Button);
      ItemList.Remove(Button);
    }
    public TileButton AddButton()
    {
      var Result = new TileButton(this);

      LayoutStack.AddPanel(Result);
      ItemList.Add(Result);

      return Result;
    }
    public TileOption AddOption()
    {
      var Result = new TileOption(this);

      LayoutStack.AddPanel(Result);
      ItemList.Add(Result);

      return Result;
    }
    public TileTable AddTable()
    {
      var Result = new TileTable(this);

      LayoutStack.AddPanel(Result);
      ItemList.Add(Result);

      return Result;
    }
    public void AddButton(TileButton Button)
    {
      LayoutStack.AddPanel(Button);
      ItemList.Add(Button);
    }
    public void AddSeparator(string Title)
    {
      var Result = new TileSeparator(this);

      Result.Text = Title;
      LayoutStack.AddPanel(Result);
      ItemList.Add(Result);
    }

    public static implicit operator Inv.Panel(TilePanel Panel)
    {
      return Panel != null ? Panel.Base : null;
    }

    internal Inv.Surface Surface { get; private set; }

    private CustomPanel Base;
    private Inv.Stack LayoutStack;
    private Inv.Scroll LayoutScroll;
    private Inv.DistinctList<TileItem> ItemList;
  }

  public interface TileItem
  {
    Inv.Panel Base { get; }
  }

  internal sealed class TileTable : TileItem
  {
    public TileTable(TilePanel Panel)
    {
      this.Base = Panel.Surface.NewHorizontalDock();
    }

    public TileColumn AddColumn()
    {
      var Result = new TileColumn(Base.Surface);

      Base.AddClient(Result);

      return Result;
    }

    public static implicit operator Inv.Panel(TileTable Item)
    {
      return Item != null ? Item.Base : null;
    }

    Inv.Panel TileItem.Base
    {
      get { return Base; }
    }

    private Inv.Dock Base;
  }

  internal sealed class TileColumn
  {
    public TileColumn(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = new CustomButton(Surface);
      Base.Margin.Set(1);

      var Stack = Surface.NewVerticalStack();
      Base.Content = Stack;

      this.TitleLabel = Surface.NewLabel();
      Stack.AddPanel(TitleLabel);
      TitleLabel.Font.Colour = Inv.Colour.Yellow;
      TitleLabel.JustifyCenter();

      this.ContentLabel = Surface.NewLabel();
      Stack.AddPanel(ContentLabel);
      ContentLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      ContentLabel.JustifyCenter();
    }

    public bool IsClickable
    {
      get { return Base.IsClickable; }
      set { Base.IsClickable = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public void Set(string Title, string Content)
    {
      var IsSmall = Surface.Window.Width < 768;

      TitleLabel.Font.Size = IsSmall ? 16 : 18;
      TitleLabel.Text = Title;

      ContentLabel.Font.Size = IsSmall ? 16 : 18;
      ContentLabel.Text = Content;
    }

    public static implicit operator Inv.Panel(TileColumn Column)
    {
      return Column != null ? Column.Base : null;
    }

    private Inv.Surface Surface;
    private CustomButton Base;
    private Inv.Label TitleLabel;
    private Inv.Label ContentLabel;
  }

  internal sealed class TileSeparator : TileItem
  {
    public TileSeparator(TilePanel Panel)
    {
      this.Base = Panel.Surface.NewLabel();
      Base.Margin.Set(0, 0, 5, 0);
      Base.JustifyRight();
      Base.Font.Colour = Inv.Colour.White;
      Base.Font.Size = 12;
    }

    public string Text
    {
      get { return Base.Text; }
      set { Base.Text = value; }
    }

    public static implicit operator Inv.Panel(TileSeparator Item)
    {
      return Item != null ? Item.Base : null;
    }

    Inv.Panel TileItem.Base
    {
      get { return Base; }
    }

    private Inv.Label Base;
  }

  internal sealed class TileOption : TileItem
  {
    public TileOption(TilePanel Panel)
    {
      this.Panel = Panel;

      this.Base = new CustomButton(Panel.Surface);
      Base.Margin.Set(1);

      this.HorizontalDock = Panel.Surface.NewHorizontalDock();
      Base.Content = HorizontalDock;

      this.IconGraphic = Panel.Surface.NewGraphic();
      HorizontalDock.AddHeader(IconGraphic);
      IconGraphic.Margin.Set(0, 0, 4, 0);

      this.DescriptionLabel = Panel.Surface.NewLabel();
      HorizontalDock.AddClient(DescriptionLabel);
      DescriptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;

      this.TrailerLabel = Panel.Surface.NewLabel();
      HorizontalDock.AddFooter(TrailerLabel);
      TrailerLabel.Font.Colour = Inv.Colour.Yellow;
      TrailerLabel.Margin.Set(0, 0, 4, 0);
    }

    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Colour Colour
    {
      set
      {
        if (this.ColourField != value)
        {
          this.ColourField = value;

          Base.BackgroundColour = IsCheckedField ? ColourField : IsClickable ? Inv.Colour.DimGray : Inv.Colour.Transparent;
        }
      }
    }
    public bool IsChecked
    {
      get { return IsCheckedField; }
      set
      {
        this.IsCheckedField = value;

        Base.BackgroundColour = IsCheckedField ? ColourField : IsClickable ? Inv.Colour.DimGray : Inv.Colour.Transparent;
      }
    }
    public bool IsVisible
    {
      get { return Base.Visibility.Get(); }
      set { Base.Visibility.Set(value); }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public bool IsClickable
    {
      get { return Base.IsClickable; }
      set { Base.IsClickable = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public void Set(Inv.Image Icon, string Description, string Trailer)
    {
      var GlyphSize = Panel.Surface.Window.Width < 768 ? 32 : 48;

      IconGraphic.Visibility.Set(Icon != null);

      if (Icon != null)
      {
        IconGraphic.Size.Set(GlyphSize, GlyphSize);
        IconGraphic.Image = Icon;
      }

      DescriptionLabel.Visibility.Set(Description != null);
      DescriptionLabel.Padding.Set(Icon == null ? 5 : 0);

      if (Description != null)
      {
        DescriptionLabel.Font.Size = GlyphSize < 48 ? 14 : 16;
        DescriptionLabel.Text = Description;
      }

      TrailerLabel.Visibility.Set(Trailer != null);

      if (Trailer != null)
      {
        TrailerLabel.Font.Size = GlyphSize < 48 ? 16 : 18;
        TrailerLabel.Text = Trailer;
      }
    }
    public void SingleTap()
    {
      Base.SingleTap();
    }

    public static implicit operator Inv.Panel(TileOption Option)
    {
      return Option != null ? Option.Base : null;
    }

    Inv.Panel TileItem.Base
    {
      get { return Base; }
    }

    private TilePanel Panel;
    private CustomButton Base;
    private Inv.Graphic IconGraphic;
    private Inv.Label DescriptionLabel;
    private bool IsCheckedField;
    private Inv.Colour ColourField;
    private Inv.Dock HorizontalDock;
    private Inv.Label TrailerLabel;
  }

  internal sealed class TileButton : TileItem
  {
    public TileButton(TilePanel Panel)
    {
      this.Panel = Panel;

      this.Base = new CustomButton(Panel.Surface);
      Base.Margin.Set(1);

      this.HorizontalDock = Panel.Surface.NewHorizontalDock();
      Base.Content = HorizontalDock;

      this.IconGraphic = Panel.Surface.NewGraphic();
      HorizontalDock.AddHeader(IconGraphic);
      IconGraphic.Margin.Set(0, 0, 4, 0);

      this.VerticalStack = Panel.Surface.NewVerticalStack();
      HorizontalDock.AddClient(VerticalStack);
      VerticalStack.Alignment.CenterStretch();

      this.TitleLabel = Panel.Surface.NewLabel();
      VerticalStack.AddPanel(TitleLabel);
      TitleLabel.Font.Colour = Inv.Colour.Yellow;

      this.DescriptionLabel = Panel.Surface.NewLabel();
      VerticalStack.AddPanel(DescriptionLabel);
      DescriptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
    }

    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Colour Colour
    {
      set
      {
        if (this.ColourField != value)
        {
          this.ColourField = value;

          Base.BackgroundColour = IsCheckedField ? ColourField : IsClickable ? Inv.Colour.DimGray : Inv.Colour.Transparent;
        }
      }
    }
    public bool IsChecked
    {
      get { return IsCheckedField; }
      set
      {
        this.IsCheckedField = value;

        Base.BackgroundColour = IsCheckedField ? ColourField : IsClickable ? Inv.Colour.DimGray : Inv.Colour.Transparent;
      }
    }
    public bool IsVisible
    {
      get { return Base.Visibility.Get(); }
      set { Base.Visibility.Set(value); }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public bool IsClickable
    {
      get { return Base.IsClickable; }
      set { Base.IsClickable = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public void SetTrailer(string Text)
    {
      if (Text == null)
      {
        if (TrailerLabel != null)
        {
          HorizontalDock.RemoveFooter(TrailerLabel);
          this.TrailerLabel = null;
        }
      }
      else
      {
        if (TrailerLabel == null)
        {
          this.TrailerLabel = Panel.Surface.NewLabel();
          HorizontalDock.AddFooter(TrailerLabel);
          TrailerLabel.Font.Colour = Inv.Colour.White;
          TrailerLabel.LineWrapping = true;
          TrailerLabel.Margin.Set(0, 0, 4, 0);
        }

        TrailerLabel.Font.Size = Panel.Surface.Window.Width < 768 ? 16 : 18;
        TrailerLabel.Text = Text;
      }
    }
    public void Set(Inv.Image Icon, string Title, string Description)
    {
      var GlyphSize = Panel.Surface.Window.Width < 768 ? 32 : 48;

      IconGraphic.Visibility.Set(Icon != null);

      if (Icon != null)
      {
        IconGraphic.Size.Set(GlyphSize, GlyphSize);
        IconGraphic.Image = Icon;
      }

      TitleLabel.Visibility.Set(Title != null);

      if (Title != null)
      {
        TitleLabel.Font.Size = GlyphSize < 48 ? 16 : 18;
        TitleLabel.Text = Title;
      }

      DescriptionLabel.Visibility.Set(Description != null);

      if (Description != null)
      {
        DescriptionLabel.Font.Size = GlyphSize < 48 ? 14 : 16;
        DescriptionLabel.Text = Description;
      }

      VerticalStack.Padding.Set(Icon == null ? 5 : 0);
    }
    public void SingleTap()
    {
      Base.SingleTap();
    }

    public static implicit operator Inv.Panel(TileButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    Inv.Panel TileItem.Base
    {
      get { return Base; }
    }

    private TilePanel Panel;
    private CustomButton Base;
    private Inv.Graphic IconGraphic;
    private Inv.Label TitleLabel;
    private Inv.Label DescriptionLabel;
    private bool IsCheckedField;
    private Inv.Colour ColourField;
    private Inv.Dock HorizontalDock;
    private Inv.Stack VerticalStack;
    private Inv.Label TrailerLabel;
  }

  public sealed class MenuPanel : Inv.Mimic<Inv.Dock>
  {
    public MenuPanel(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = Surface.NewVerticalDock();
      Base.Background.Colour = null;

      this.ButtonStack = Surface.NewHorizontalStack();
      Base.AddFooter(ButtonStack);
    }

    public Inv.Colour BackgroundColour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public Inv.Panel Content
    {
      get { return ContentPanel; }
      set
      {
        if (ContentPanel != value)
        {
          if (ContentPanel != null)
            Base.RemoveClient(ContentPanel);

          this.ContentPanel = value;

          if (ContentPanel != null)
            Base.AddClient(ContentPanel);
        }
      }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }

    public bool HasButtons()
    {
      return ButtonStack.HasPanels();
    }
    public void RemoveButtons()
    {
      ButtonStack.RemovePanels();
    }
    public MenuButton AddButton()
    {
      var Result = new MenuButton(this);

      ButtonStack.AddPanel(Result);

      return Result;
    }

    internal Inv.Surface Surface { get; private set; }

    private Inv.Stack ButtonStack;
    private Inv.Panel ContentPanel;
  }

  public sealed class MenuButton
  {
    public MenuButton(MenuPanel Panel)
    {
      var ButtonSize = (Panel.Size.Width.Value / 4) - 10;
      var ImageSize = ButtonSize / 2;

      this.Base = new CustomButton(Panel.Surface);
      Base.Size.Set(ButtonSize, ButtonSize);
      Base.Corner.Set(ImageSize);
      Base.BackgroundColour = Inv.Colour.DimGray;
      Base.Margin.Set(8, 8, 0, 8);

      this.Graphic = Panel.Surface.NewGraphic();
      Base.Content = Graphic;
      Graphic.Size.Set(ImageSize, ImageSize);
      Graphic.Alignment.Center();
      Graphic.Visibility.Collapse();
    }

    public Inv.Image Image
    {
      set
      {
        Graphic.Image = value;
        Graphic.Visibility.Set(value != null);
      }
    }
    public bool IsChecked
    {
      get { return IsCheckedField; }
      set
      {
        if (IsCheckedField != value)
        {
          this.IsCheckedField = value;

          Base.BackgroundColour = value ? Inv.Colour.DarkGreen : Inv.Colour.DimGray;
        }
      }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }

    public void LinkPanel(Inv.Panel Panel)
    {
      Panel.Visibility.ChangeEvent += () =>
      {
        IsChecked = Panel.Visibility.Get();
      };
    }

    public static implicit operator Inv.Panel(MenuButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    private CustomButton Base;
    private bool IsCheckedField;
    private Inv.Graphic Graphic;
  }

  internal sealed class ActionPanel
  {
    public ActionPanel(Inv.Surface Surface)
    {
      this.Surface = Surface;
      this.Base = new CustomPanel(Surface);
      Base.Background.Colour = Inv.Colour.Black;

      this.Graphic = Surface.NewGraphic();
      Graphic.Alignment.Center();
      Graphic.Size.Set(128, 128);

      this.HeaderStack = Surface.NewHorizontalStack();
      HeaderStack.Alignment.StretchLeft();

      this.FooterStack = Surface.NewHorizontalStack();
      FooterStack.Alignment.StretchRight();

      this.HeaderButtonList = new Inv.DistinctList<ActionButton>();
      this.FooterButtonList = new Inv.DistinctList<ActionButton>();

      // NOTE: this is unnecessary if you remember to call Compose before it is shown (which is necessary for it to arrange correctly).
      //       however, it might alleviate some bugs where you forget to call Compose.
      Compose();
    }

    public string Caption
    {
      get { return Base.Caption; }
      set { Base.Caption = value; }
    }
    public string Description
    {
      get { return Base.Description; }
      set { Base.Description = value; }
    }
    public Inv.Colour BackgroundColour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Image Image
    {
      set { Graphic.Image = value; }
    }

    public ActionButton AddHeaderButton()
    {
      var Result = new ActionButton(this);

      HeaderButtonList.Add(Result);

      HeaderStack.AddPanel(Result);

      return Result;
    }
    public ActionButton AddFooterButton()
    {
      var Result = new ActionButton(this);

      FooterButtonList.Add(Result);

      FooterStack.AddPanel(Result);

      return Result;
    }
    public void Compose()
    {
      var IsWide = Surface.Window.Width >= 1280;
      var IsReduced = Surface.Window.Width < 1024;
      var IsNarrow = Surface.Window.Width < 768;
      var IsPortrait = Surface.Window.Width < Surface.Window.Height;
      var IsStacked = IsNarrow && IsPortrait;
      var IsConverting = LayoutDock == null || (IsStacked && ButtonStack == null) || (!IsStacked && ButtonStack != null);
      var HeaderCount = HeaderButtonList.Count(B => B.IsVisible);
      var FooterCount = FooterButtonList.Count(B => B.IsVisible);
      var ButtonCount = IsNarrow && IsPortrait ? Math.Max(HeaderCount, Math.Max(FooterCount, 3)) : Math.Max(HeaderCount + FooterCount, 6);

      this.ButtonMargin = IsReduced || IsNarrow ? 8 : 16;
      this.ButtonSize = Math.Min(((Surface.Window.Width - ButtonMargin - (IsReduced ? 0 : 180) - (ButtonCount * ButtonMargin)) / ButtonCount), Math.Min(Surface.Window.Height / 3, 160));

      if (IsConverting && LayoutDock != null)
      {
        LayoutDock.RemovePanels();
        this.LayoutDock = null;
      }

      if (IsConverting && ButtonStack != null)
      {
        ButtonStack.RemovePanels();
        this.ButtonStack = null;
      }

      if (IsConverting)
      {
        Debug.Assert(LayoutDock == null);
        Debug.Assert(ButtonStack == null);

        this.LayoutDock = Surface.NewHorizontalDock();
        Base.Content = LayoutDock;
        LayoutDock.AddHeader(Graphic);

        if (IsStacked)
        {
          this.ButtonStack = Surface.NewVerticalStack();
          LayoutDock.AddClient(ButtonStack);
          ButtonStack.AddPanel(HeaderStack);
          ButtonStack.AddPanel(FooterStack);
        }
        else
        {
          LayoutDock.AddHeader(HeaderStack);
          LayoutDock.AddFooter(FooterStack);
        }
      }

      LayoutDock.Margin.Set(ButtonMargin, ButtonMargin, 0, 0);

      Graphic.Visibility.Set(!IsReduced);
      Graphic.Margin.Set(0, 0, ButtonMargin, 0);

      this.LabelSize = IsWide ? 24 : IsNarrow ? 14 : IsReduced ? 16 : 18;

      foreach (var HeaderButton in HeaderButtonList)
        HeaderButton.Compose();

      foreach (var FooterButton in FooterButtonList)
        FooterButton.Compose();

      // focus.
      var FocusButton = HeaderButtonList.Find(H => H.IsVisible) ?? FooterButtonList.Find(H => H.IsVisible);
      if (FocusButton != null)
        Surface.SetFocus(FocusButton);
    }

    public static implicit operator Inv.Panel(ActionPanel Panel)
    {
      return Panel != null ? Panel.Base : null;
    }

    internal Inv.Surface Surface { get; private set; }
    internal int ButtonSize { get; private set; }
    internal int ButtonMargin { get; private set; }
    internal int LabelSize { get; private set; }

    private CustomPanel Base;
    private Inv.Graphic Graphic;
    private Inv.Stack ButtonStack;
    private Inv.Stack HeaderStack;
    private Inv.Stack FooterStack;
    private Inv.Dock LayoutDock;
    private Inv.DistinctList<ActionButton> HeaderButtonList;
    private Inv.DistinctList<ActionButton> FooterButtonList;
  }

  internal sealed class ActionButton
  {
    public ActionButton(ActionPanel Panel)
    {
      this.Panel = Panel;

      this.Base = new CustomButton(Panel.Surface);
      Base.IsFocused = true;

      this.CaptionLabel = Panel.Surface.NewLabel();
      Base.Content = CaptionLabel;
      CaptionLabel.Alignment.Stretch();
      CaptionLabel.JustifyCenter();
      CaptionLabel.Font.Colour = Inv.Colour.White;
      CaptionLabel.LineWrapping = true;

      Compose(); // see NOTE in ActionPanel constructor.
    }

    public string Caption
    {
      get { return CaptionLabel.Text; }
      set { CaptionLabel.Text = value; }
    }
    public Inv.Colour Colour
    {
      get { return Base.BackgroundColour; }
      set { Base.BackgroundColour = value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public bool IsVisible
    {
      get { return Base.Visibility.Get(); }
      set { Base.Visibility.Set(value); }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }
    public event Action PressEvent
    {
      add { Base.PressEvent += value; }
      remove { Base.PressEvent -= value; }
    }
    public event Action ReleaseEvent
    {
      add { Base.ReleaseEvent += value; }
      remove { Base.ReleaseEvent -= value; }
    }

    internal void Compose()
    {
      Base.Margin.Set(0, 0, Panel.ButtonMargin, Panel.ButtonMargin);
      Base.Size.Set(Panel.ButtonSize, Panel.ButtonSize);
      CaptionLabel.Font.Size = Panel.LabelSize;
    }

    public static implicit operator Inv.Panel(ActionButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    private ActionPanel Panel;
    private CustomButton Base;
    private Inv.Label CaptionLabel;
  }

  public sealed class CaptionButton
  {
    public CaptionButton(Inv.Surface Surface)
    {
      this.Base = new CustomButton(Surface);

      this.Label = Surface.NewLabel();
      Base.Content = Label;
      Label.JustifyCenter();
      Label.Font.Size = 18;
      Label.Font.Colour = Inv.Colour.White;
      Label.LineWrapping = true;
    }

    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Padding Padding
    {
      get { return Base.Padding; }
    }
    public Inv.Colour Colour
    {
      set
      {
        if (this.ColourField != value)
        {
          this.ColourField = value;

          Base.BackgroundColour = value;
        }
      }
    }
    public string Text
    {
      set { Label.Text = value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public static implicit operator Inv.Panel(CaptionButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    private CustomButton Base;
    private Inv.Label Label;
    private Inv.Colour ColourField;
  }

  internal sealed class AboutButton : Inv.Mimic<Inv.Button>
  {
    public AboutButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Background.Colour = Inv.Colour.Yellow;
      Base.Border.Set(1);
      Base.Border.Colour = Inv.Colour.Red;
      //Base.Margin.Set(4, 0, 4, 4);

      var Dock = Surface.NewHorizontalDock();
      Base.Content = Dock;

      this.Graphic = Surface.NewGraphic();
      Dock.AddHeader(Graphic);
      Graphic.Padding.Set(4);
      Graphic.Size.Set(64, 64);
      Graphic.Alignment.TopLeft();

      var Stack = Surface.NewVerticalStack();
      Dock.AddClient(Stack);
      Stack.Alignment.CenterStretch();

      this.TitleLabel = Surface.NewLabel();
      Stack.AddPanel(TitleLabel);
      TitleLabel.Font.Colour = Inv.Colour.Black;
      TitleLabel.Font.Light();
      TitleLabel.Font.Size = 22;

      this.ActionLabel = Surface.NewLabel();
      Stack.AddPanel(ActionLabel);
      ActionLabel.Font.Colour = Inv.Colour.Black;
      ActionLabel.Font.Medium();
      ActionLabel.Font.Size = 14;

      this.SupportingLabel = Surface.NewLabel();
      Stack.AddPanel(SupportingLabel);
      SupportingLabel.Padding.Set(0, 4, 0, 0);
      SupportingLabel.Font.Colour = Inv.Colour.Black;
      SupportingLabel.Font.Size = 14;
      SupportingLabel.Visibility.Collapse();
    }

    public Inv.Colour Colour
    {
      set { Base.Background.Colour = value; }
    }
    public Inv.Image LogoImage
    {
      set { Graphic.Image = value; }
    }
    public string TitleText
    {
      set { TitleLabel.Text = value; }
    }
    public string ActionText
    {
      set { ActionLabel.Text = value; }
    }
    public string SupportingText
    {
      set
      {
        SupportingLabel.Text = value;
        SupportingLabel.Visibility.Set(value != null);
      }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    private Inv.Graphic Graphic;
    private Inv.Label TitleLabel;
    private Inv.Label ActionLabel;
    private Inv.Label SupportingLabel;
  }

  internal sealed class ExchangePanel : Inv.Mimic<Inv.Dock>
  {
    public ExchangePanel(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.LeftPanel = new TilePanel(Surface);
      this.RightPanel = new TilePanel(Surface);
      this.LeftButton = new CaptionButton(Surface);
      this.CommandButton = new CaptionButton(Surface);
      this.RightButton = new CaptionButton(Surface);
    }

    public TilePanel LeftPanel { get; private set; }
    public TilePanel RightPanel { get; private set; }
    public CaptionButton LeftButton { get; private set; }
    public CaptionButton RightButton { get; private set; }
    public CaptionButton CommandButton { get; private set; }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public event Action CloseEvent;

    public void Compose(int EdgeWidth)
    {
      if (Base != null)
        Base.RemovePanels();

      if (ButtonDock != null)
        ButtonDock.RemovePanels();

      var IsPortrait = Surface.Window.Width < Surface.Window.Height;
      var IsNarrow = Surface.Window.Width < 1024;
      var LayoutMargin = IsNarrow ? 5 : 10;
      var IsSingle = IsNarrow && IsPortrait;

      var CommandSize = IsNarrow ? 150 : 200;
      int PanelSize;
      if (IsSingle)
      {
        PanelSize = EdgeWidth - (CommandSize / 2) - (LayoutMargin * 4);

        var Gap = Surface.Window.Height - (PanelSize + CommandSize - 50);

        if (Gap > 0 && Gap < 40)
          PanelSize -= (40 - Gap) / 2;
      }
      else
      {
        PanelSize = Math.Min(400, (Surface.Window.Width - CommandSize - (LayoutMargin * 4)) / 2);
      }

      this.Base = IsSingle ? Surface.NewVerticalDock() : Surface.NewHorizontalDock();
      Base.Alignment.BottomStretch();
      Base.Background.Colour = Inv.Colour.Black;

      if (IsSingle)
      {
        Base.Size.SetMaximumWidth(Math.Min(420, Surface.Window.Width));
        Base.Size.AutoHeight();

        var CloseButton = new CaptionButton(Surface);
        Base.AddHeader(CloseButton);
        CloseButton.Colour = Inv.Colour.DimGray;
        CloseButton.Text = "CLOSE";
        CloseButton.Size.SetHeight(40);
        CloseButton.SingleTapEvent += () =>
        {
          if (CloseEvent != null)
            CloseEvent();
        };
      }
      else
      {
        Base.Size.AutoMaximumWidth();
        Base.Size.SetHeight(Math.Min(Surface.Window.Height - 40, Math.Max(EdgeWidth, (CommandSize * 2) + (LayoutMargin * 4))));
      }

      Base.AddHeader(LeftPanel);
      if (IsSingle)
      {
        LeftPanel.Size.AutoWidth();
        LeftPanel.Size.SetHeight(PanelSize);
      }
      else
      {
        LeftPanel.Size.SetWidth(PanelSize);
        LeftPanel.Size.AutoHeight();
      }
      LeftPanel.Background.Colour = Inv.Colour.Transparent;

      Base.AddFooter(RightPanel);
      if (IsSingle)
      {
        RightPanel.Size.AutoWidth();
        RightPanel.Size.SetHeight(PanelSize);
      }
      else
      {
        RightPanel.Size.SetWidth(PanelSize);
        RightPanel.Size.AutoHeight();
      }
      RightPanel.Background.Colour = Inv.Colour.Transparent;

      this.ButtonDock = IsSingle ? Surface.NewHorizontalDock() : Surface.NewVerticalDock();
      Base.AddClient(ButtonDock);
      if (IsSingle)
      {
        ButtonDock.Size.AutoWidth();
        ButtonDock.Size.SetHeight(CommandSize - 50);
      }
      else
      {
        ButtonDock.Size.SetWidth(CommandSize);
        ButtonDock.Size.AutoHeight();
      }

      ButtonDock.AddHeader(LeftButton);
      LeftButton.Margin.Set(LayoutMargin);
      LeftButton.Padding.Set(LayoutMargin);
      if (IsSingle)
      {
        LeftButton.Size.SetWidth(CommandSize / 2);
        LeftButton.Size.AutoHeight();
      }
      else
      {
        LeftButton.Size.AutoWidth();
        LeftButton.Size.SetHeight(CommandSize / 2);
      }

      ButtonDock.AddClient(CommandButton);
      CommandButton.Padding.Set(LayoutMargin);
      if (IsSingle)
      {
        CommandButton.Margin.Set(0, LayoutMargin, LayoutMargin, LayoutMargin);
        //CommandButton.Size.SetWidth(CommandSize);
      }
      else
      {
        CommandButton.Margin.Set(LayoutMargin, 0, LayoutMargin, LayoutMargin);
        //CommandButton.Size.SetHeight(CommandSize);
      }

      ButtonDock.AddFooter(RightButton);
      RightButton.Padding.Set(LayoutMargin);
      if (IsSingle)
      {
        RightButton.Margin.Set(0, LayoutMargin, LayoutMargin, LayoutMargin);
        RightButton.Size.SetWidth(CommandSize / 2);
        RightButton.Size.AutoHeight();
      }
      else
      {
        RightButton.Margin.Set(LayoutMargin, 0, LayoutMargin, LayoutMargin);
        RightButton.Size.AutoWidth();
        RightButton.Size.SetHeight(CommandSize / 2);
      }
    }

    private Inv.Surface Surface;
    private Inv.Dock ButtonDock;
  }
}