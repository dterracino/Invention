﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.DataProtection;

namespace Inv
{
  internal sealed class UwaVault
  {
    public UwaVault()
    {
      this.LocalSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
      this.DataProtectionProvider = new DataProtectionProvider("LOCAL=user");
    }

    public void Save(Secret Secret)
    {
      var Serialized = Secret.Serialize();
      var Bytes = Encoding.UTF8.GetBytes(Serialized);
      var Protected = Protect(Bytes);
      var Encoded = Convert.ToBase64String(Protected);

      LocalSettings.Values[Secret.Name] = Encoded;
    }
    public void Load(Secret Secret)
    {
      var Element = LocalSettings.Values[Secret.Name];

      if (Element != null)
      {
        var Encoded = (string)Element;
        var Protected = Convert.FromBase64String(Encoded);
        var Bytes = Unprotect(Protected);
        var Serialized = Encoding.UTF8.GetString(Bytes, 0, Bytes.Length);

        Secret.Deserialize(Serialized);
      }
      else
      {
        Secret.Properties.Clear();
      }
    }
    public void Delete(Secret Secret)
    {
      LocalSettings.Values.Remove(Secret.Name);
    }

    private byte[] Protect(byte[] data)
    {
      var Buffer = CryptographicBuffer.CreateFromByteArray(data);

      var ProtectTask = DataProtectionProvider.ProtectAsync(Buffer).AsTask();
      ProtectTask.Wait();

      byte[] Result;
      CryptographicBuffer.CopyToByteArray(ProtectTask.Result, out Result);

      return Result;
    }
    private byte[] Unprotect(byte[] data)
    {
      var Buffer = CryptographicBuffer.CreateFromByteArray(data);

      var ProtectTask = DataProtectionProvider.UnprotectAsync(Buffer).AsTask();
      ProtectTask.Wait();

      byte[] Result;
      CryptographicBuffer.CopyToByteArray(ProtectTask.Result, out Result);

      return Result;
    }

    private Windows.Storage.ApplicationDataContainer LocalSettings;
    private DataProtectionProvider DataProtectionProvider;
  }
}