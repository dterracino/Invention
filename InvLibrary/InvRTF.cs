﻿/*! 3 !*/
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Inv
{
  public class RTFWriter 
  {
    public RTFWriter(Stream stream)
    {
      this.StreamWriter = new StreamWriter(stream, Encoding.GetEncoding("ASCII"));
    }

    public int LineLimit { get; set; }

    public void StartGroup()
    {
      StreamWriter.Write('{');
      lineCount++;
      wasControl = true;
    }
    public void CloseGroup()
    {
      wasControl = false;
      StreamWriter.Write('}');
      lineCount++;
      CheckNewLine();
    }
    public void Break(string word)
    {
      wasControl = true;
      StreamWriter.Write(word);
      lineCount += word.Length;
    }
    public void Control(string word)
    {
      wasControl = true;
      StreamWriter.Write('\\' + word);
      lineCount += word.Length + 1;
    }
    public void ControlBoolean(string word, bool on)
    {
      if (on)
        Control(word);
      else
        Control(word + '0');
    }
    public void Text(string text)
    {
      if (!string.IsNullOrEmpty(text))
      {
        if (wasControl)
        {
          StreamWriter.Write(' ');
          lineCount++;
          CheckNewLine();
        }

        foreach (var c in text)
        {
          if (c == '\\' || c == '{' || c == '}')
          {
            StreamWriter.Write('\\' + c);
            lineCount += 2;
          }
          else if (c > 'ÿ')
          {
              StreamWriter.Write("\\u"+((int) c).ToString()+" ?");
              lineCount += 3+((int) c).ToString().Length;
          }
          else
          {
              StreamWriter.Write(c);
              lineCount++;
          }
        }

        wasControl = false;
      }
    }
    public void CheckNewLine()
    {
      if ((LineLimit != 0) && (lineCount > LineLimit))
      {
        StreamWriter.Write("\r\n");
        lineCount = 0;
      }
    }
    public void Write(byte[] c)
    {
      var p = new string(Encoding.UTF8.GetChars(c));
      StreamWriter.Write(p);
    }
    public void Close()
    {
      StreamWriter.Flush();
      StreamWriter.Dispose();
    }

    private bool wasControl;
    private int lineCount;
    private StreamWriter StreamWriter;
  }

  public class RtfReader : StreamReader
  {
    public RtfReader(Stream stream)
      : base(stream, Encoding.GetEncoding("ASCII"))
    {
      IsBreak = false;
      Control = null;
      Cache = "";
    }

    public bool PeekIsGroupStart()
    {
      return !IsBreak && (PeekChar(0) == '{');
    }
    public bool PeekIsGroupClose()
    {
      return !IsBreak && (PeekChar(0) == '}');
    }
    public bool PeekIsControl()
    {
      return !IsBreak && (PeekChar(0) == '\\') && (PeekChar(1) != '\\');
    }
    public bool PeekIsBreak()
    {
      return IsBreak;
    }
    public void ConsumeGroupStart()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(PeekChar(0) == '{', "ConsumeGroupStart, Found \"{0}\" consuming {{", PeekChar(0));
      ReadChar();
      IsBreak = false;
    }
    public void ConsumeGroupClose()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(PeekChar(0) == '}', "ConsumeGroupClose, Found \"{0}\" consuming }}", PeekChar(0));
      ReadChar();
      IsBreak = !EndOfStream && (PeekChar(0) == ';');
      ConsumeSpace();
    }
    public string ConsumeControl()
    {
      var res = ConsumeControlInner();
      IsBreak = PeekChar(0) == ';';
      return res;
    }
    public void ConsumeBreak()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IsBreak && PeekChar(0) == ';', "ConsumeGroupClose, Found \"{0}\" consuming ;", PeekChar(0));
      ReadChar();
      IsBreak = false;
      ConsumeSpace();
    }
    public string ConsumeText()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!(IsBreak || (PeekChar(0) == '\\' && PeekChar(1) != '\\') || CharInSet(PeekChar(0), new char[] { '{', '}' })), "ConsumeText', 'not at text consuming text");
      var res = ConsumeUntilCharacterSet(new char[] { '\\', '{', '}', '\r', '\n' });
      while ((PeekChar(0) == '\\' && PeekChar(1) == '\\'))
      {
        ConsumeCharacterCount(2);
        res = res + "\\" + ConsumeUntilCharacterSet(new char[] {'\\', '{', '}', '\r', '\n'});
      }
      EatEoln();
      IsBreak = false;
      return res;
    }
    public string PeekControl()
    {
      var res = ConsumeControlInner();
      Control = res;
      return res;
    }
    public bool PeekIsNumericControl(string name)
    {
      var val = PeekControl();
      int dummy;
      return val.Length >= name.Length && val.Substring(0, name.Length) == name && val.Length > name.Length && Int32.TryParse(val.Substring(name.Length), out dummy);
    }
    public int ConsumeNumericControl(string name)
    {
      var val = ConsumeControl();
      int dummy;
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(val.Length >= name.Length && val.Substring(0, name.Length) == name && val.Length > name.Length && Int32.TryParse(val.Substring(name.Length), out dummy), "ConsumeNumericControl, Found '{0}' trying to read '{1}'.", val, name);
      return Int32.Parse(val.Substring(name.Length));
    }
    public bool PeekIsBooleanControl(string name)
    {
      var val = PeekControl();
      return val.Length >= name.Length && val.Substring(0, name.Length) == name && (val.Length == name.Length || val.Substring(name.Length) == "0");
    }
    public bool ConsumeBooleanControl(string name)
    {
      var val = ConsumeControl();
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(val.Length >= name.Length && val.Substring(0, name.Length) == name && (val.Length == name.Length || val.Substring(name.Length) == "0"), "ConsumeNumericControl, Found '{0}' trying to read '{1}'.", val, name);
      return val.Length == name.Length;
    }

    private char PeekChar(int offset)
    {
      while (Cache.Length <= offset)
        Cache = Cache + (char)Read();
      return Cache[offset];
    }
    private char ReadChar()
    {
      if (Cache.Length == 0)
        return (char)Read();
      else
      {
        var c = Cache[0];
        Cache = Cache.Length == 1 ? "" : Cache.Substring(1);
        return c;
      }
    }
    private string ConsumeControlInner()
    {
      var res = "";
      if (Control != null)
        res = Control;
      else
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(PeekChar(0) == '\\', "ConsumeGroupClose, Found \"{0}\" consuming control (\\)", PeekChar(0));
        ReadChar(); // '\'
        res += ReadChar();
        // work around an apparent Word Bug: \~ does not have a following space
        if (res != "~")
        {
          if (res == "\'")
            res += ConsumeCharacterCount(2);
          //else if ((res == "\\") || (res == "{") || (res == "}"))
          //  res = res;    // escape special characters
          else
          {
            // todo: does a control code ever include a '\'? (in which case, we'd need to check with a '\' is actually a termination or not)
            res = res + ConsumeUntilCharacterSet(new char[] { ' ', '\\', '{', '}', ';', '\r', '\n' });
            ConsumeSpace();
          }
        }
        EatEoln();
      }
      Control = null;
      return res;
    }
    private void ConsumeSpace()
    {
      if (!EndOfStream && (PeekChar(0) == ' '))
        ReadChar();

      while (!EndOfStream && CharInSet(PeekChar(0), SetVertical()))
        ReadChar();
    }
    private void EatEoln()
    {
      while (!EndOfStream && CharInSet(PeekChar(0), SetVertical()))
        ReadChar();
    }
    private string ConsumeUntilCharacterSet(char[] tokens)
    {
      var b = new StringBuilder();
      while (!EndOfStream && !CharInSet(PeekChar(0), tokens))
        b.Append(ReadChar());
      return b.ToString();
    }
    private string ConsumeCharacterCount(int count)
    {
      var b = new StringBuilder();
      for (var i = 0; i < count; i++)
        b.Append(ReadChar());
      return b.ToString();
    }
    private bool CharInSet(char p, char[] set)
    {
      var res = false;
      foreach (var c in set)
        res = res || c == p;
      return res;
    }
    private char[] SetVertical()
    {
      return new char[] { '\r', '\n', '\t' };
    }

    private bool IsBreak;
    private string Control;
    private string Cache;
  }
}

