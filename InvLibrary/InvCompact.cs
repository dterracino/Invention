﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public sealed class CompactWriter : IDisposable
  {
    public CompactWriter(Stream Stream)
    {
      this.Stream = Stream;
    }
    public void Dispose()
    {
      Stream.Dispose();
    }

    public void WriteStream(Action<Stream> Action)
    {
      Action(Stream);
    }
    public void WriteBoolean(bool Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteCharacter(char Value)
    {
      var Buffer = BitConverter.GetBytes(Value);
      WriteInt32(Buffer.Length);
      WriteBuffer(Buffer);
    }
    public void WriteDateTime(DateTime Value)
    {
      WriteInt64(Value.ToBinary());
    }
    public void WriteDateTimeOffset(DateTimeOffset Value)
    {
      WriteInt64(Value.DateTime.ToBinary());
      WriteInt64(Value.Offset.Ticks);
    }
    public void WriteDateTimeOffsetNullable(DateTimeOffset? Value)
    {
      WriteBoolean(Value == null);

      if (Value != null)
        WriteDateTimeOffset(Value.Value);
    }
    public void WriteTimeSpan(TimeSpan Duration)
    {
      WriteInt64(Duration.Ticks);
    }
    public void WriteGuid(Guid Value)
    {
      WriteBuffer(Value.ToByteArray());
    }
    public void WriteUInt8(byte Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteUInt16(ushort Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteUInt32(uint Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteInt16(short Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteInt32(int Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteByte(byte Value)
    {
      WriteBuffer(new byte[1] { Value });
    }
    public void WriteInt32Nullable(int? Value)
    {
      WriteBoolean(Value == null);

      if (Value != null)
        WriteInt32(Value.Value);
    }
    public void WriteInt64(long Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteInt64Nullable(long? Value)
    {
      WriteBoolean(Value == null);

      if (Value != null)
        WriteInt64(Value.Value);
    }
    public void WriteDecimal(decimal Value)
    {
      // NOTE: there is no BitConverter for decimal.
      var Bits = Decimal.GetBits(Value);

      //Debug.Assert(Bits.Length == 4);

      var Buffer = new byte[16];
      var lo = Bits[0];
      var mid = Bits[1];
      var hi = Bits[2];
      var flags = Bits[3];

      Buffer[0] = (byte)lo;
      Buffer[1] = (byte)(lo >> 8);
      Buffer[2] = (byte)(lo >> 16);
      Buffer[3] = (byte)(lo >> 24);

      Buffer[4] = (byte)mid;
      Buffer[5] = (byte)(mid >> 8);
      Buffer[6] = (byte)(mid >> 16);
      Buffer[7] = (byte)(mid >> 24);

      Buffer[8] = (byte)hi;
      Buffer[9] = (byte)(hi >> 8);
      Buffer[10] = (byte)(hi >> 16);
      Buffer[11] = (byte)(hi >> 24);

      Buffer[12] = (byte)flags;
      Buffer[13] = (byte)(flags >> 8);
      Buffer[14] = (byte)(flags >> 16);
      Buffer[15] = (byte)(flags >> 24);

      WriteBuffer(Buffer);
    }
    public void WriteFloat(float Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteDouble(double Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteString(string Value)
    {
      if (Value == null)
      {
        WriteInt32(0); // length
        WriteByte(0); // null
      }
      else
      {
        var Buffer = Encoding.UTF8.GetBytes(Value);
        var Length = Buffer.Length;

        WriteInt32(Length);

        if (Length == 0)
          WriteByte(1); // non-null
        else
          WriteBuffer(Buffer);
      }
    }
    public void WriteBinary(Inv.Binary Value)
    {
      var Buffer = Value.GetBuffer();

      WriteString(Value.GetFormat());
      WriteInt32(Buffer.Length);
      WriteBuffer(Buffer);
    }
    public void WriteImage(Inv.Image Value)
    {
      var Buffer = Value.GetBuffer();

      WriteString(Value.GetFormat());
      WriteInt32(Buffer.Length);
      WriteBuffer(Buffer);
    }
    public void WriteSound(Inv.Sound Value)
    {
      var Buffer = Value.GetBuffer();

      WriteString(Value.GetFormat());
      WriteInt32(Buffer.Length);
      WriteBuffer(Buffer);
    }
    public void WriteByteArray(byte[] Value)
    {
      WriteInt32(Value.Length);
      WriteBuffer(Value);
    }
    public void WriteInt32Array(int[] Value)
    {
      WriteInt32(Value.Length);

      for (var Index = 0; Index < Value.Length; Index++)
        WriteInt32(Value[Index]);
    }
    public void WriteInt64Array(long[] Value)
    {
      WriteInt32(Value.Length);

      for (var Index = 0; Index < Value.Length; Index++)
        WriteInt64(Value[Index]);
    }
    public void WriteStringArray(string[] Value)
    {
      WriteInt32(Value.Length);

      for (var Index = 0; Index < Value.Length; Index++)
        WriteString(Value[Index]);
    }
    public void WriteColour(Inv.Colour Value)
    {
      if (Value == null)
      {
        WriteInt32(0);
        WriteByte(0);
      }
      else
      {
        WriteInt32(Value.RawValue);

        if (Value.RawValue == 0)
          WriteByte(1);
      }
    }
    public void WriteUri(Uri Uri)
    {
      WriteString(Uri != null ? Uri.OriginalString : null);
    }

    private void WriteBuffer(byte[] Buffer)
    {
      Stream.Write(Buffer, 0, Buffer.Length);
    }

    private Stream Stream;
  }

  public sealed class CompactReader : IDisposable
  {
    public CompactReader(Stream Stream)
    {
      this.Stream = Stream;
    }
    public void Dispose()
    {
      Stream.Dispose();
    }

    public void ReadStream(Action<Stream> Action)
    {
      Action(Stream);
    }
    public bool ReadBoolean()
    {
      return BitConverter.ToBoolean(ReadBuffer(1), 0);
    }
    public char ReadCharacter()
    {
      var Length = ReadInt32();
      var Buffer = ReadBuffer(Length);

      return BitConverter.ToChar(Buffer, 0);
    }
    public DateTime ReadDateTime()
    {
      var DateTimeBinary = ReadInt64();

      return DateTime.FromBinary(DateTimeBinary);
    }
    public DateTimeOffset ReadDateTimeOffset()
    {
      var DateTimeBinary = ReadInt64();
      var OffsetTicks = ReadInt64();

      return new DateTimeOffset(DateTime.FromBinary(DateTimeBinary), TimeSpan.FromTicks(OffsetTicks));
    }
    public DateTimeOffset? ReadDateTimeOffsetNullable()
    {
      if (ReadBoolean())
        return null;
      else
        return ReadDateTimeOffset();
    }
    public TimeSpan ReadTimeSpan()
    {
      return new TimeSpan(ReadInt64());
    }
    public byte ReadUInt8()
    {
      return ReadBuffer(1)[0];
    }
    public ushort ReadUInt16()
    {
      return BitConverter.ToUInt16(ReadBuffer(2), 0);
    }
    public uint ReadUInt32()
    {
      return BitConverter.ToUInt32(ReadBuffer(4), 0);
    }
    public byte ReadByte()
    {
      return ReadBuffer(1)[0];
    }
    public short ReadInt16()
    {
      return BitConverter.ToInt16(ReadBuffer(2), 0);
    }
    public int ReadInt32()
    {
      return BitConverter.ToInt32(ReadBuffer(4), 0);
    }
    public int? ReadInt32Nullable()
    {
      if (ReadBoolean())
        return null;
      else
        return ReadInt32();
    }
    public long ReadInt64()
    {
      return BitConverter.ToInt64(ReadBuffer(8), 0);
    }
    public long? ReadInt64Nullable()
    {
      if (ReadBoolean())
        return null;
      else
        return ReadInt64();
    }
    public decimal ReadDecimal()
    {
      var Buffer = ReadBuffer(16);

      var lo = ((int)Buffer[0]) | ((int)Buffer[1] << 8) | ((int)Buffer[2] << 16) | ((int)Buffer[3] << 24);
      var mid = ((int)Buffer[4]) | ((int)Buffer[5] << 8) | ((int)Buffer[6] << 16) | ((int)Buffer[7] << 24);
      var hi = ((int)Buffer[8]) | ((int)Buffer[9] << 8) | ((int)Buffer[10] << 16) | ((int)Buffer[11] << 24);
      var flags = ((int)Buffer[12]) | ((int)Buffer[13] << 8) | ((int)Buffer[14] << 16) | ((int)Buffer[15] << 24);

      return new Decimal(new int[] { lo, mid, hi, flags });
    }
    public float ReadFloat()
    {
      return BitConverter.ToSingle(ReadBuffer(4), 0);
    }
    public double ReadDouble()
    {
      return BitConverter.ToDouble(ReadBuffer(4), 0);
    }
    public string ReadString()
    {
      var Length = ReadInt32();

      if (Length == 0)
      {
        if (ReadByte() == 0)
          return null;
      }

      var Buffer = ReadBuffer(Length);

      return Encoding.UTF8.GetString(Buffer, 0, Length);
    }
    public Guid ReadGuid()
    {
      return new Guid(ReadBuffer(16));
    }
    public Inv.Colour ReadColour()
    {
      var RawValue = ReadInt32();

      if (RawValue == 0)
      {
        if (ReadByte() == 0)
          return null;
      }

      return Inv.Colour.FromArgb(RawValue);
    }
    public Inv.Binary ReadBinary()
    {
      var Format = ReadString();
      var Count = ReadInt32();
      var Bytes = ReadBuffer(Count);

      return new Inv.Binary(Bytes, Format);
    }
    public Inv.Image ReadImage()
    {
      var Format = ReadString();
      var Count = ReadInt32();
      var Bytes = ReadBuffer(Count);

      return new Inv.Image(Bytes, Format);
    }
    public Inv.Sound ReadSound()
    {
      var Format = ReadString();
      var Count = ReadInt32();
      var Bytes = ReadBuffer(Count);

      return new Inv.Sound(Bytes, Format);
    }
    public byte[] ReadByteArray()
    {
      var Count = ReadInt32();
      var Bytes = ReadBuffer(Count);

      return Bytes;
    }
    public int[] ReadInt32Array()
    {
      var Count = ReadInt32();

      var Result = new int[Count];

      for (var Index = 0; Index < Count; Index++)
        Result[Index] = ReadInt32();

      return Result;
    }
    public long[] ReadInt64Array()
    {
      var Count = ReadInt32();

      var Result = new long[Count];

      for (var Index = 0; Index < Count; Index++)
        Result[Index] = ReadInt64();

      return Result;
    }
    public string[] ReadStringArray()
    {
      var Count = ReadInt32();

      var Result = new string[Count];

      for (var Index = 0; Index < Count; Index++)
        Result[Index] = ReadString();

      return Result;
    }
    public Uri ReadUri()
    {
      var Result = ReadString();

      return Result != null ? new Uri(Result) : null;
    }

    private byte[] ReadBuffer(int Length)
    {
      var Result = new byte[Length];

      Stream.Read(Result, 0, Length);

      return Result;
    }

    private Stream Stream;
  }
}