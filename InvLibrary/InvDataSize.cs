﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Globalization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Inv.Support;

namespace Inv
{
  [XmlSchemaProvider("MySchema")]
  public struct DataSize : IComparable<DataSize>, IEquatable<DataSize>, IComparable, IXmlSerializable
  {
    private DataSize(long TotalBytes)
    {
      TotalBytesField = TotalBytes;
    }

    public const long BytesPerTerabyte = 1L << 40;
    public const long BytesPerGigabyte = 1L << 30;
    public const long BytesPerMegabyte = 1L << 20;
    public const long BytesPerKilobyte = 1L << 10;

    public static readonly DataSize MaxValue = new DataSize(long.MaxValue);
    public static readonly DataSize MinValue = new DataSize(long.MinValue);
    public static readonly DataSize Zero = new DataSize(0L);

    public static DataSize FromTerabytes(long Terabytes)
    {
      return new DataSize(Terabytes * BytesPerTerabyte);
    }
    public static DataSize FromGigabytes(long Gigabytes)
    {
      return new DataSize(Gigabytes * BytesPerGigabyte);
    }
    public static DataSize FromMegabytes(long Megabytes)
    {
      return new DataSize(Megabytes * BytesPerMegabyte);
    }
    public static DataSize FromKilobytes(long Kilobytes)
    {
      return new DataSize(Kilobytes * BytesPerKilobyte);
    }
    public static DataSize FromBytes(long Bytes)
    {
      return new DataSize(Bytes);
    }
    public static DataSize operator -(DataSize t)
    {
      return new DataSize(-t.TotalBytes);
    }
    public static DataSize operator -(DataSize t1, DataSize t2)
    {
      return new DataSize(t1.TotalBytes - t2.TotalBytes);
    }
    public static bool operator !=(DataSize t1, DataSize t2)
    {
      return t1.TotalBytes != t2.TotalBytes;
    }
    public static DataSize operator +(DataSize t)
    {
      return new DataSize(+t.TotalBytes);
    }
    public static DataSize operator +(DataSize t1, DataSize t2)
    {
      return new DataSize(t1.TotalBytes + t2.TotalBytes);
    }
    public static DataSize operator *(DataSize t1, double t2)
    {
      return new DataSize((long)Math.Round(t1.TotalBytes * t2));
    }
    public static DataSize operator *(DataSize t1, DataSize t2)
    {
      return new DataSize(t1.TotalBytes * t2.TotalBytes);
    }
    public static float operator / (DataSize t1, DataSize t2)
    {
      return (float) t1.TotalBytes / t2.TotalBytes;
    }
    public static bool operator <(DataSize t1, DataSize t2)
    {
      return t1.TotalBytes < t2.TotalBytes;
    }
    public static bool operator <=(DataSize t1, DataSize t2)
    {
      return t1.TotalBytes <= t2.TotalBytes;
    }
    public static bool operator ==(DataSize t1, DataSize t2)
    {
      return t1.TotalBytes == t2.TotalBytes;
    }
    public static bool operator >(DataSize t1, DataSize t2)
    {
      return t1.TotalBytes > t2.TotalBytes;
    }
    public static bool operator >=(DataSize t1, DataSize t2)
    {
      return t1.TotalBytes >= t2.TotalBytes;
    }
    public static bool TryParse(string Input, out DataSize SizeSpan)
    {
      var Number = Input.Trim();
      var Multiplier = 1L;

      if (Number.Length > 1 && Number[Number.Length - 1] == 'B')
      {
        switch (Number[Number.Length - 2])
        {
          case 'K':
            Number = Number.Remove(Number.Length - 2);
            Multiplier = BytesPerKilobyte;
            break;

          case 'M':
            Number = Number.Remove(Number.Length - 2);
            Multiplier = BytesPerMegabyte;
            break;

          case 'G':
            Number = Number.Remove(Number.Length - 2);
            Multiplier = BytesPerGigabyte;
            break;

          case 'T':
            Number = Number.Remove(Number.Length - 2);
            Multiplier = BytesPerTerabyte;
            break;

          default:
            Number = Number.Remove(Number.Length - 1);
            break;
        }

        Number = Number.Trim();
      }

      long Value;
      var Result = long.TryParse(Number, out Value);

      if (Result)
        SizeSpan = new DataSize(Value * Multiplier);
      else
        SizeSpan = DataSize.Zero;

      return Result;
    }
    public static DataSize Parse(string Input)
    {
      DataSize Result;

      if (!TryParse(Input, out Result))
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Unable to parse '{0}' into a data size.", Input));

      return Result;
    }
    public static DataSize? ParseOrDefault(string Input, DataSize? Default = null)
    {
      DataSize Result;

      if (TryParse(Input, out Result))
        return Result;
      else
        return Default;
    }

    public long TotalBytes
    {
      get { return TotalBytesField; }
    }
    public double TotalKilobytes
    {
      get { return (double)TotalBytesField / BytesPerKilobyte; }
    }
    public double TotalMegabytes
    {
      get { return (double)TotalBytesField / BytesPerMegabyte; }
    }
    public double TotalGigabytes
    {
      get { return (double)TotalBytesField / BytesPerGigabyte; }
    }
    public double TotalTerabytes
    {
      get { return (double)TotalBytesField / BytesPerTerabyte; }
    }

    public override bool Equals(object value)
    {
      var Source = value as Inv.DataSize?;

      return Source != null && EqualTo(Source.Value);
    }
    public override int GetHashCode()
    {
      return TotalBytesField.GetHashCode();
    }
    public override string ToString()
    {
      string Result;

      var InBytes = TotalBytesField;

      var IsNegative = InBytes < 0;
      if (IsNegative)
        InBytes = -InBytes;

      if (InBytes < BytesPerKilobyte)
        Result = InBytes + " bytes";
      else if (InBytes < BytesPerMegabyte)
        Result = Math.Round(((double)InBytes / BytesPerKilobyte), 2) + " KB";
      else if (InBytes < BytesPerGigabyte)
        Result = Math.Round(((double)InBytes / BytesPerMegabyte), 2) + " MB";
      else if (InBytes < BytesPerTerabyte)
        Result = Math.Round(((double)InBytes / BytesPerGigabyte), 2) + " GB";
      else
        Result = Math.Round(((double)InBytes / BytesPerTerabyte), 2) + " TB";

      if (IsNegative)
        Result = "-" + Result;

      return Result;
    }
    public string ToBriefString()
    {
      string Result;

      var InBytes = TotalBytesField;

      var IsNegative = InBytes < 0;
      if (IsNegative)
        InBytes = -InBytes;

      if (InBytes < BytesPerKilobyte)
        Result = TotalBytes + " B";
      else if (InBytes < Inv.DataSize.BytesPerMegabyte)
        Result = string.Format("{0:0} KB", TotalKilobytes.RoundAwayFromZero());
      else if (InBytes < Inv.DataSize.BytesPerGigabyte)
        Result = string.Format("{0:0} MB", TotalMegabytes.RoundAwayFromZero());
      else if (InBytes < Inv.DataSize.BytesPerTerabyte)
        Result = string.Format("{0:0} GB", TotalGigabytes.RoundAwayFromZero());
      else
        Result = string.Format("{0:0} TB", TotalTerabytes.RoundAwayFromZero());

      if (IsNegative)
        Result = "-" + Result;

      return Result;
    }

    public int CompareTo(DataSize value)
    {
      if (value == this)
        return 0;
      else
        return TotalBytes.CompareTo(value.TotalBytes);
    }
    public bool EqualTo(DataSize value)
    {
      return CompareTo(value) == 0;
    }

    int IComparable<DataSize>.CompareTo(DataSize other)
    {
      return TotalBytes.CompareTo(other.TotalBytes);
    }
    bool IEquatable<DataSize>.Equals(DataSize other)
    {
      return TotalBytes == other.TotalBytes;
    }
    int IComparable.CompareTo(object obj)
    {
      return CompareTo((DataSize)obj);
    }

    private static XmlQualifiedName MySchema(object xs)
    {
      return new XmlQualifiedName("long", "http://www.w3.org/2001/XMLSchema");
    }
    System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
    {
      return null; // NOTE: this is required to be null.
    }
    void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
    {
      TotalBytesField = XmlConvert.ToInt64(reader.ReadInnerXml());
    }
    void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
    {
      writer.WriteRaw(XmlConvert.ToString(TotalBytesField));
    }

    private /*readonly*/ long TotalBytesField;
  }
}
