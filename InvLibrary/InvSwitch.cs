﻿/*! 3 !*/
  using System;
  using System.Collections.Generic;
  using System.Diagnostics;
  using System.Linq;
  using System.Reflection;
  using System.Text;
  using System.Threading.Tasks;
  using Inv.Support;

namespace Inv
{
  public sealed class TypeSwitch<TContext>
  {
    public TypeSwitch()
    {
      this.ContextType = typeof(TContext);
      this.ActionDictionary = new Dictionary<Type, Action<TContext, object>>();
    }

    public void Compile<TRecord>(object Owner)
    {
      var BaseType = typeof(TRecord);
      var OwnerType = Owner.GetType().GetReflectionInfo();
      var MethodInfoArray = OwnerType.GetReflectionMethods().Where(M => !M.IsPublic && !M.IsStatic).ToArray();

      var AddMethod = this.GetType().GetReflectionMethod("Add");

      foreach (var MethodInfo in MethodInfoArray)
      {
        var ParameterInfoArray = MethodInfo.GetParameters();

        if (ParameterInfoArray.Length == 2 && ParameterInfoArray[0].ParameterType == ContextType && ParameterInfoArray[1].ParameterType.GetReflectionInfo().BaseType == BaseType)
        {
          var RecordType = ParameterInfoArray[1].ParameterType;

          var MethodDelegate = MethodInfo.CreateDelegate(typeof(Action<,>).MakeGenericType(ContextType, RecordType), Owner);
          AddMethod.MakeGenericMethod(RecordType).Invoke(this, new object[] { MethodDelegate });
        }
      }

#if DEBUG
      var SubtypeArray = BaseType.GetReflectionInfo().Assembly.GetReflectionTypes().Where(t => t.GetReflectionInfo().BaseType == BaseType);

      var MissingTypeArray = SubtypeArray.Except(ActionDictionary.Keys).ToArray();

      if (MissingTypeArray.Length > 0)
        throw new Exception("Compiled route must declare methods for missing subtypes:\n" + MissingTypeArray.Select(T => T.FullName).AsSeparatedText("\n"));
#endif
    }
    public void Compile<TRecord>(Action<TContext, TRecord> Action)
    {
      var BaseType = typeof(TRecord);
      var SubtypeArray = BaseType.GetReflectionInfo().Assembly.GetReflectionTypes().Where(t => t.GetReflectionInfo().BaseType == BaseType).ToArray();

      Action<TContext, object> CompileAction = (C, R) => Action(C, (TRecord)R);

      if (SubtypeArray.Length == 0)
      {
        ActionDictionary.Add(BaseType, CompileAction);
      }
      else
      {
        foreach (var Subtype in SubtypeArray)
          ActionDictionary.Add(Subtype, CompileAction);
      }
    }
    public void Execute(TContext Context, object Record)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Record, "Record");

      if (Record != null)
      {
        var RegisterType = Record.GetType();
        var Action = ActionDictionary.GetValueOrDefault(RegisterType);

        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Action != null, "Type not registered: {0}", RegisterType.FullName);

        if (Action != null)
          Action(Context, Record);
      }
    }

    private void Add<TRecord>(Action<TContext, TRecord> Action)
    {
      ActionDictionary.Add(typeof(TRecord), (C, R) => Action(C, (TRecord)R));
    }

    private Dictionary<Type, Action<TContext, object>> ActionDictionary;
    private Type ContextType;
  }
}
  