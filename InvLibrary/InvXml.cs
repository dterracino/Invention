﻿/*! 4 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using Inv.Support;

namespace Inv
{
  [DataContract]
  public struct Xml : IComparable
  {
    public static Xml Empty()
    {
      return new Xml(""); 
    }
    public static Xml Hollow(long Length)
    {
      return new Xml(null, Length);
    }

    public Xml(string Text)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Text, "Text");

      this.Text = Text;
      this.Length = Text.Length;
    }
    internal Xml(string Text, long Length)
    {
      this.Text = Text;
      this.Length = Length;
    }
    internal Xml(Xml Xml)
    {
      this.Text = Xml.Text;
      this.Length = Xml.Length;
    }

    public string GetText()
    {
      var Result = this.Text;

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Result != null, "Cannot use GetText() on a hollow Xml.");

      return Result;
    }
    public long GetLength()
    {
      return Length;
    }
    public Inv.DataSize GetSize()
    {
      return Inv.DataSize.FromBytes(Length);
    }
    public bool IsEmpty()
    {
      return Length == 0;
    }
    public bool IsHollow()
    {
      return Text == null;
    }
    public bool EqualTo(Xml Xml)
    {
      if (this.IsHollow() || Xml.IsHollow())
        return this.Length == Xml.Length;
      else
        return this.Text.Equals(Xml.Text, StringComparison.Ordinal);
    }
    public int CompareTo(Xml Xml)
    {
      if (this.IsHollow() || Xml.IsHollow())
        return this.Length.CompareTo(Xml.Length);
      else
        return this.Text.CompareTo(Xml.Text);
    }

    public override string ToString()
    {
      return "Xml[" + Length.ToString(CultureInfo.InvariantCulture) + "]";
    }
    public override bool Equals(object obj)
    {
      var Source = obj as Xml?;

      return Source != null && EqualTo(Source.Value);
    }
    public override int GetHashCode()
    {
      return this.Text.GetHashCode();
    }

    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Xml)obj);
    }

    [DataMember]
    private readonly string Text;
    [DataMember]
    private readonly long Length;
  }

  public sealed class XmlDocument
  {
    public XmlDocument()
    {
    }

    public XmlElement RootElement { get; private set; }

    public XmlElement AccessRoot(string LocalName, string Namespace)
    {
      if (RootElement == null)
        throw new Exception("Expected root element was not installed.");

      if (RootElement.LocalName != LocalName)
        throw new Exception(string.Format("Expected root element to be named '{0}' but was named '{1}'.", LocalName, RootElement.LocalName));

      if (RootElement.Namespace != Namespace)
        throw new Exception(string.Format("Expected root element to be namespaced '{0}' but was namespaced '{1}'.", Namespace, RootElement.Namespace));

      return RootElement;
    }
    public XmlElement InstallRoot(string LocalName, string Namespace)
    {
      return InstallRoot(null, LocalName, Namespace);
    }
    public XmlElement InstallRoot(string Prefix, string LocalName, string Namespace)
    {
      this.RootElement = new XmlElement(Prefix, LocalName, Namespace);
      return RootElement;
    }
    public void WriteToStream(System.IO.Stream Stream, Encoding Encoding = null)
    {
      XmlDocumentStreamWriteEngine.Execute
      (
        XmlDocument: this,
        Stream: Stream,
        Encoding: Encoding
      );
    }
    public void ReadFromStream(System.IO.Stream Stream)
    {
      XmlDocumentStreamReadEngine.Execute
      (
        Stream: Stream,
        XmlDocument: this
      );
    }
    public string WriteToString()
    {
      string Result;
      XmlDocumentStringWriteEngine.Execute
      (
        XmlDocument: this,
        Text: out Result
      );
      return Result;
    }
    public void ReadFromString(string Text)
    {
      using (var TextReader = new System.IO.StringReader(Text))
      {
        XmlDocumentTextReadEngine.Execute
        (
          TextReader: TextReader,
          XmlDocument: this
        );
      }
    }
    public Inv.Binary WriteToBinary(Encoding Encoding = null, string Extension = ".xml")
    {
      using (var MemoryStream = new MemoryStream())
      {
        XmlDocumentStreamWriteEngine.Execute
        (
          XmlDocument: this,
          Stream: MemoryStream,
          Encoding: Encoding
        );

        return new Inv.Binary(MemoryStream.ToArray(), Extension);
      }
    }
    public void ReadFromBinary(Inv.Binary Binary)
    {
      using (var MemoryStream = new MemoryStream(Binary.GetBuffer()))
      {
        XmlDocumentStreamReadEngine.Execute
        (
          Stream: MemoryStream,
          XmlDocument: this
        );
      }
    }
  }

  internal enum XmlNodeType
  {
    Element,
    Content,
    Comment
  }

  internal sealed class XmlNode
  {
    internal static XmlNode AsElement(XmlElement Element)
    {
      return new XmlNode()
      {
        Type = XmlNodeType.Element,
        Element = Element
      };
    }
    internal static XmlNode AsContent(string Content)
    {
      return new XmlNode()
      {
        Type = XmlNodeType.Content,
        Content = Content
      };
    }
    internal static XmlNode AsComment(string Comment)
    {
      return new XmlNode()
      {
        Type = XmlNodeType.Comment,
        Comment = Comment
      };
    }

    private XmlNode()
    {
    }

    public XmlNodeType Type { get; private set; }
    public XmlElement Element { get; private set; }
    public string Content { get; internal set; }
    public string Comment { get; internal set; }
  }

  public sealed class XmlElement
  {
    internal XmlElement(string Prefix, string LocalName, string Namespace)
    {
      this.Prefix = Prefix;
      this.LocalName = LocalName;
      this.Namespace = Namespace;
      this.AttributeList = new Inv.DistinctList<XmlAttribute>();
      this.NodeList = new Inv.DistinctList<XmlNode>();
    }

    public string Prefix { get; internal set; }
    public string LocalName { get; internal set; }
    public string Namespace { get; internal set; }
    public bool HasContent
    {
      get { return NodeList.Count == 1 && NodeList[0].Type == XmlNodeType.Content; }
    }
    public string Content
    {
      get
      {
        if (HasContent)
          return NodeList[0].Content;
        else
          return null;
      }
      set
      {
        if (value == null)
        {
          NodeList.Clear();
        }
        else if (NodeList.Count == 0)
        {
          NodeList.Add(XmlNode.AsContent(value));
        }
        else if (NodeList.Count > 1)
        {
          throw new Exception("Cannot set content when there is more than one child node.");
        }
        else if (NodeList[0].Type != XmlNodeType.Content)
        {
          throw new Exception("Cannot set content when the child node is not of type content.");
        }
        else
        {
          NodeList[0].Content = value;
        }
      }
    }

    public void AddContent(string Text)
    {
      NodeList.Add(XmlNode.AsContent(Text));
    }
    public void AddComment(string Text)
    {
      NodeList.Add(XmlNode.AsComment(Text));
    }
    public XmlElement AddChildElement(string Prefix, string LocalName, string Namespace)
    {
      var Result = new XmlElement(Prefix, LocalName, Namespace);

      NodeList.Add(XmlNode.AsElement(Result));

      return Result;
    }
    public XmlElement AddChildElement(string LocalName)
    {
      return AddChildElement(null, LocalName, null);
    }
    public XmlElement InsertChildElement(int Index, string LocalName)
    {
      return InsertChildElement(Index, null, LocalName, null);
    }
    public XmlElement InsertChildElement(int Index, string Prefix, string LocalName, string Namespace)
    {
      var Result = new XmlElement(Prefix, LocalName, Namespace);

      NodeList.Insert(Index, XmlNode.AsElement(Result));

      return Result;
    }
    public void ImportAsChildElement(Inv.Xml ImportXml)
    {
      var ImportXmlDocument = new XmlDocument();

      ImportXmlDocument.ReadFromString(ImportXml.GetText());

      NodeList.Add(XmlNode.AsElement(ImportXmlDocument.RootElement));
    }
    public void AddAttribute(string Prefix, string LocalName, string Namespace, string Value)
    {
      AttributeList.Add(new XmlAttribute()
      {
        Prefix = Prefix,
        LocalName = LocalName,
        Namespace = Namespace,
        Value = Value
      });
    }
    public void AddAttribute(string LocalName, string Namespace, string Value)
    {
      AddAttribute(null, LocalName, Namespace, Value);
    }
    public void AddAttribute(string LocalName, string Value)
    {
      AddAttribute(null, LocalName, null, Value);
    }
    public bool HasAttribute(string LocalName)
    {
      return AttributeList.Exists(Attribute => Attribute.LocalName == LocalName);
    }
    public string AccessAttribute(string LocalName)
    {
      var Result = AttributeList.Find(Attribute => Attribute.LocalName == LocalName);

      if (Result == null)
        throw new Exception("Unable to access attribute: " + LocalName);

      return Result.Value;
    }
    public bool HasAttributes()
    {
      return AttributeList.Count > 0;
    }
    public IEnumerable<XmlAttribute> GetAttributes()
    {
      return AttributeList;
    }
    public void AddNamespace(string Identifier, string Namespace)
    {
      AddAttribute("xmlns", Identifier, null, Namespace);
    }
    public bool HasChildElements()
    {
      return NodeList.Any(N => N.Type == XmlNodeType.Element);
    }
    public IEnumerable<XmlElement> AccessChildElement(string LocalName)
    {
      return GetChildElements().Where(Element => Element.LocalName == LocalName);
    }
    public string AccessChildContent(string LocalName)
    {
      var XmlElement = AccessChildElement(LocalName).SingleOrDefault();

      if (XmlElement != null && XmlElement.HasContent)
        return XmlElement.Content;
      else
        return null;
    }
    public IEnumerable<XmlElement> GetChildElements()
    {
      foreach (var Node in NodeList)
      {
        if (Node.Type == XmlNodeType.Element)
          yield return Node.Element;
      }
    }

    internal IEnumerable<XmlNode> GetNodes()
    {
      return NodeList;
    }

    private Inv.DistinctList<XmlAttribute> AttributeList;
    private Inv.DistinctList<XmlNode> NodeList;
  }

  public sealed class XmlAttribute
  {
    internal XmlAttribute()
    {
    }

    public string Prefix { get; internal set; }
    public string LocalName { get; internal set; }
    public string Namespace { get; internal set; }
    public string Value { get; internal set; }
  }

  internal static class XmlElementWriterEngine
  {
    public static void Execute
    (
      System.Xml.XmlWriter XmlWriter,
      Inv.XmlElement XmlElement
    )
    {
      if (XmlElement.Namespace == null)
        XmlWriter.WriteStartElement(XmlElement.LocalName);
      else if (XmlElement.Prefix == null)
        XmlWriter.WriteStartElement(XmlElement.LocalName, XmlElement.Namespace);
      else
        XmlWriter.WriteStartElement(XmlElement.Prefix, XmlElement.LocalName, XmlElement.Namespace);

      foreach (var XmlAttribute in XmlElement.GetAttributes())
      {
        if (XmlAttribute.Prefix == null && XmlAttribute.Namespace == null)
          XmlWriter.WriteAttributeString(XmlAttribute.LocalName, XmlAttribute.Value);
        else if (XmlAttribute.Prefix == null && XmlAttribute.Namespace != null)
          XmlWriter.WriteAttributeString(XmlAttribute.LocalName, XmlAttribute.Namespace, XmlAttribute.Value);
        else
          XmlWriter.WriteAttributeString(XmlAttribute.Prefix, XmlAttribute.LocalName, XmlAttribute.Namespace, XmlAttribute.Value);
      }

      foreach (var Node in XmlElement.GetNodes())
      {
        switch (Node.Type)
        {
          case Inv.XmlNodeType.Element:
            Inv.XmlElementWriterEngine.Execute
            (
              XmlWriter: XmlWriter,
              XmlElement: Node.Element
            );
            break;

          case Inv.XmlNodeType.Content:
            XmlWriter.WriteString(Node.Content);
            break;

          case Inv.XmlNodeType.Comment:
            XmlWriter.WriteComment(Node.Comment);
            break;

          default:
            throw new Exception("Inv.XmlNodeType not handled.");
        }
      }

      XmlWriter.WriteEndElement();
    }
  }

  internal static class XmlElementReaderEngine
  {
    public static void Execute
    (
      System.Xml.XmlReader XmlReader,
      Inv.XmlElement XmlElement
    )
    {
      if (XmlReader.NodeType != System.Xml.XmlNodeType.Element)
        throw new Exception("Xml must be an element.");

      XmlElement.LocalName = XmlReader.LocalName;
      XmlElement.Namespace = XmlReader.NamespaceURI;
      XmlElement.Prefix = XmlReader.Prefix;

      if (XmlReader.MoveToFirstAttribute())
      {
        do
        {
          // TODO: what about prefix/namespace?
          XmlElement.AddAttribute(null, XmlReader.Name, null, XmlReader.Value);
        }
        while (XmlReader.MoveToNextAttribute());

        XmlReader.MoveToElement();
      }

      if (XmlReader.IsEmptyElement)
      {
        XmlReader.Read();
      }
      else
      {
        XmlReader.ReadStartElement();

        while (XmlReader.NodeType != System.Xml.XmlNodeType.EndElement)
        {
          switch (XmlReader.NodeType)
          {
            case System.Xml.XmlNodeType.Element:
              Inv.XmlElementReaderEngine.Execute
              (
                XmlReader: XmlReader,
                XmlElement: XmlElement.AddChildElement(null, null, null)
              );
              break;

            case System.Xml.XmlNodeType.Text:
              XmlElement.AddContent(XmlReader.ReadContentAsString());
              break;

            case System.Xml.XmlNodeType.Comment:
              XmlElement.AddComment(XmlReader.Value);
              XmlReader.Read();
              break;

            default:
              throw new Exception("XmlNodeType not handled.");
          }
        }

        XmlReader.ReadEndElement();
      }
    }
  }

  internal static class XmlDocumentStreamWriteEngine
  {
    public static void Execute
    (
      Stream Stream,
      Inv.XmlDocument XmlDocument,
      System.Text.Encoding Encoding = null
    )
    {
      using (var XmlWriter = System.Xml.XmlWriter.Create(Stream, Encoding != null ? new System.Xml.XmlWriterSettings() { Indent = true, Encoding = Encoding } : new System.Xml.XmlWriterSettings() { Indent = true }))
      {
        XmlWriter.WriteStartDocument();

        Inv.XmlElementWriterEngine.Execute
        (
          XmlWriter: XmlWriter,
          XmlElement: XmlDocument.RootElement
        );

        XmlWriter.WriteEndDocument();
      }
    }
  }

  internal static class XmlDocumentStringReadEngine
  {
    public static void Execute
    (
      string Text,
      Inv.XmlDocument XmlDocument
    )
    {
      using (var StringReader = new StringReader(Text))
      {
        Inv.XmlDocumentTextReadEngine.Execute
        (
          TextReader: StringReader,
          XmlDocument: XmlDocument
        );
      }
    }
  }

  internal static class XmlDocumentStringWriteEngine
  {
    public static void Execute
    (
      Inv.XmlDocument XmlDocument,
      out string Text
    )
    {
      var StringBuilder = new StringBuilder();

      using (var XmlWriter = System.Xml.XmlWriter.Create(StringBuilder, new System.Xml.XmlWriterSettings() { Indent = true }))
      {
        XmlWriter.WriteStartDocument();

        Inv.XmlElementWriterEngine.Execute
        (
          XmlWriter: XmlWriter,
          XmlElement: XmlDocument.RootElement
        );

        XmlWriter.WriteEndDocument();
      }

      Text = StringBuilder.ToString();
    }
  }

  internal static class XmlDocumentStreamReadEngine
  {
    public static void Execute
    (
      Stream Stream,
      Inv.XmlDocument XmlDocument
    )
    {
      using (var StreamReader = new StreamReader(Stream))
      {
        Inv.XmlDocumentTextReadEngine.Execute
        (
          TextReader: StreamReader,
          XmlDocument: XmlDocument
        );
      }
    }
  }

  internal static class XmlDocumentTextReadEngine
  {
    public static void Execute
    (
      TextReader TextReader,
      Inv.XmlDocument XmlDocument
    )
    {
      using (var XmlReader = System.Xml.XmlReader.Create(TextReader, new System.Xml.XmlReaderSettings() { IgnoreWhitespace = true }))
      {
        var RootElement = XmlDocument.InstallRoot(null, null, null);

        XmlReader.Read();

        if (XmlReader.NodeType != System.Xml.XmlNodeType.XmlDeclaration)
          throw new Exception("Xml document must have a declaration node type.");

        XmlReader.Read();

        Inv.XmlElementReaderEngine.Execute
        (
          XmlReader: XmlReader,
          XmlElement: RootElement
        );
      }
    }
  }
}
