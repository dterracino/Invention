﻿/*! 11 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// A serializable class representing a 32-bit ARGB colour.
  /// </summary>
  [XmlSchemaProvider("MySchema")]
  public sealed class Colour : IComparable, IEquatable<Colour>, IXmlSerializable
  {
    public static Inv.Colour FromArgb(byte Alpha, byte Red, byte Green, byte Blue)
    {
      return ColourFactory.Resolve(new ColourARGBRecord()
      {
        A = Alpha,
        R = Red,
        G = Green,
        B = Blue
      }.Argb);
    }
    public static Inv.Colour FromArgb(ColourARGBRecord Argb)
    {
      return ColourFactory.Resolve(Argb.Argb);
    }
    public static Inv.Colour FromArgb(uint Argb)
    {
      return ColourFactory.Resolve((int)Argb);
    }
    public static Inv.Colour FromArgb(int Argb)
    {
      return ColourFactory.Resolve(Argb);
    }
    public static Inv.Colour FromHSV(double hue, double saturation, double value)
    {
      int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
      double f = hue / 60 - Math.Floor(hue / 60);

      value = value * 255;
      var v = Convert.ToByte(value);
      var p = Convert.ToByte(value * (1 - saturation));
      var q = Convert.ToByte(value * (1 - f * saturation));
      var t = Convert.ToByte(value * (1 - (1 - f) * saturation));

      if (hi == 0)
        return Inv.Colour.FromArgb(255, v, t, p);
      else if (hi == 1)
        return Inv.Colour.FromArgb(255, q, v, p);
      else if (hi == 2)
        return Inv.Colour.FromArgb(255, p, v, t);
      else if (hi == 3)
        return Inv.Colour.FromArgb(255, p, q, v);
      else if (hi == 4)
        return Inv.Colour.FromArgb(255, t, p, v);
      else
        return Inv.Colour.FromArgb(255, v, p, q);
    }
    public static Inv.Colour FromName(string Name)
    {
      return NameDictionary.GetByRightOrDefault(Name, Inv.Colour.Transparent);
    }
    public static Inv.Colour FromHexadecimalColour(string Value)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Value != null && Value.Length >= 6 && Value[0] != '#', "Hex string must be valid: {0}", Value);

      if (Value.Length == 6)
        return Inv.Colour.FromArgb(int.Parse("FF" + Value, System.Globalization.NumberStyles.AllowHexSpecifier));
      else
        return Inv.Colour.FromArgb(int.Parse(Value, System.Globalization.NumberStyles.AllowHexSpecifier));
    }
    public static Inv.Colour FromHtmlColour(string Value)
    {
      if (Value.Length == 7 && Value[0] == '#')
        return Inv.Colour.FromHexadecimalColour("FF" + Value.Substring(1));
      else
        return null;
    }

    internal Colour(int Argb)
    {
      // NOTE: only used by ColourTree.
      this.RawValue = Argb;
    }

    public string Name
    {
      get { return NameDictionary.GetByLeftOrDefault(this, Hex); }
    }
    public string Hex
    {
      get { return RawValue.ToString("X8").Batch(2).AsSeparatedText(" "); }
    }
    public int RawValue { get; private set; }
    public bool IsOpaque
    {
      get { return GetARGBRecord().A == 0xFF; }
    }

    public bool IsTransparent()
    {
      return GetARGBRecord().A == 0x00;
    }
    public ColourARGBRecord GetARGBRecord()
    {
      var Result = new ColourARGBRecord();

      Result.Argb = RawValue;

      return Result;
    }
    public ColourHSLRecord GetHSLRecord()
    {
      var Record = GetARGBRecord();

      var Result = new ColourHSLRecord();

      // NOTE: lifted from System.Drawing.Color methods GetHue(), GetSaturation(), GetBrightness()

      // HUE.
      {
        if ((int)Record.R == (int)Record.G && (int)Record.G == (int)Record.B)
        {
          Result.H = 0.0f;
        }
        else
        {
          var num1 = (float)Record.R / (float)byte.MaxValue;
          var num2 = (float)Record.G / (float)byte.MaxValue;
          var num3 = (float)Record.B / (float)byte.MaxValue;
          var num4 = 0.0f;
          var num5 = num1;
          var num6 = num1;
          if ((double)num2 > (double)num5)
            num5 = num2;
          if ((double)num3 > (double)num5)
            num5 = num3;
          if ((double)num2 < (double)num6)
            num6 = num2;
          if ((double)num3 < (double)num6)
            num6 = num3;
          float num7 = num5 - num6;
          if ((double)num1 == (double)num5)
            num4 = (num2 - num3) / num7;
          else if ((double)num2 == (double)num5)
            num4 = (float)(2.0 + ((double)num3 - (double)num1) / (double)num7);
          else if ((double)num3 == (double)num5)
            num4 = (float)(4.0 + ((double)num1 - (double)num2) / (double)num7);
          var num8 = num4 * 60f;
          if ((double)num8 < 0.0)
            num8 += 360f;
          Result.H = num8;
        }
      }

      // SATURATION.
      {
        var num1 = (double)Record.R / (double)byte.MaxValue;
        var num2 = (float)Record.G / (float)byte.MaxValue;
        var num3 = (float)Record.B / (float)byte.MaxValue;
        var num4 = 0.0f;
        var num5 = (float)num1;
        var num6 = (float)num1;
        if ((double)num2 > (double)num5)
          num5 = num2;
        if ((double)num3 > (double)num5)
          num5 = num3;
        if ((double)num2 < (double)num6)
          num6 = num2;
        if ((double)num3 < (double)num6)
          num6 = num3;
        if ((double)num5 != (double)num6)
          num4 = ((double)num5 + (double)num6) / 2.0 > 0.5 ? (float)(((double)num5 - (double)num6) / (2.0 - (double)num5 - (double)num6)) : (float)(((double)num5 - (double)num6) / ((double)num5 + (double)num6));
        Result.S = num4;
      }

      // BRIGHTNESS.
      {
        var num1 = (double)Record.R / (double)byte.MaxValue;
        var num2 = (float)Record.G / (float)byte.MaxValue;
        var num3 = (float)Record.B / (float)byte.MaxValue;
        var num4 = (float)num1;
        var num5 = (float)num1;
        if ((double)num2 > (double)num4)
          num4 = num2;
        if ((double)num3 > (double)num4)
          num4 = num3;
        if ((double)num2 < (double)num5)
          num5 = num2;
        if ((double)num3 < (double)num5)
          num5 = num3;

        Result.L = (float)(((double)num4 + (double)num5) / 2.0);
      }

      return Result;
    }
    public int GetArgb()
    {
      return RawValue;
    }
    public string ToHtmlColour()
    {
      return "#" + (RawValue & 0xFFFFFF).ToString("X6");
    }

    public int CompareTo(Colour Colour)
    {
      return this.RawValue.CompareTo(Colour.RawValue);
    }
    public bool EqualTo(Colour Colour)
    {
      return this.RawValue == Colour.RawValue;
    }
    public override int GetHashCode()
    {
      return RawValue;
    }
    public override string ToString()
    {
      return Name;
    }

    // not a data member.
    internal object Node { get; set; }

    bool IEquatable<Colour>.Equals(Inv.Colour Colour)
    {
      return CompareTo(Colour) == 0;
    }
    int IComparable.CompareTo(object Object)
    {
      return CompareTo((Colour)Object);
    }

    private static XmlQualifiedName MySchema(object xs)
    {
      return new XmlQualifiedName("string", "http://www.w3.org/2001/XMLSchema");
    }
    System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
    {
      return null;  // NOTE: this is required to be null.
    }
    void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
    {
      RawValue = Int32.Parse(reader.ReadInnerXml(), System.Globalization.NumberStyles.HexNumber);
    }
    void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
    {
      writer.WriteRaw(RawValue.ToString("X8"));
    }

    static Colour()
    {
      List = new Inv.DistinctList<Inv.Colour>();
      NameDictionary = new Bidictionary<Inv.Colour, string>();

      var ColourType = typeof(Inv.Colour);
      var ColourInfo = ColourType.GetReflectionInfo();

      foreach (var Field in ColourInfo.GetReflectionFields().Where(F => F.IsStatic && F.IsPublic && F.FieldType == ColourType))
      {
        var Colour = (Inv.Colour)Field.GetValue(null);
        List.Add(Colour);

        NameDictionary.Add(Colour, Field.Name);
      }
    }

    internal static Inv.Colour ConvertHSLToColor(byte Alpha, float Hue, float Saturation, float Lighting)
    {
#if DEBUG
      if (0 > Alpha || 255 < Alpha)
        throw new ArgumentOutOfRangeException("alpha");
      if (0f > Hue || 360f < Hue)
        throw new ArgumentOutOfRangeException("hue");
      if (0f > Saturation || 1f < Saturation)
        throw new ArgumentOutOfRangeException("saturation");
      if (0f > Lighting || 1f < Lighting)
        throw new ArgumentOutOfRangeException("lighting");
#endif
      if (Saturation == 0)
        return Inv.Colour.FromArgb(Alpha, Convert.ToByte(Lighting * 255), Convert.ToByte(Lighting * 255), Convert.ToByte(Lighting * 255));

      float fMax, fMid, fMin;

      if (0.5 < Lighting)
      {
        fMax = Lighting - (Lighting * Saturation) + Saturation;
        fMin = Lighting + (Lighting * Saturation) - Saturation;
      }
      else
      {
        fMax = Lighting + (Lighting * Saturation);
        fMin = Lighting - (Lighting * Saturation);
      }

      var iSextant = (int)Math.Floor(Hue / 60f);

      if (300f <= Hue)
        Hue -= 360f;

      Hue /= 60f;
      Hue -= 2f * (float)Math.Floor(((iSextant + 1f) % 6f) / 2f);

      if (iSextant % 2 == 0)
        fMid = Hue * (fMax - fMin) + fMin;
      else
        fMid = fMin - Hue * (fMax - fMin);

      var iMax = Convert.ToByte(fMax * 255);
      var iMid = Convert.ToByte(fMid * 255);
      var iMin = Convert.ToByte(fMin * 255);

      switch (iSextant)
      {
        case 1:
          return Inv.Colour.FromArgb(Alpha, iMid, iMax, iMin);
        case 2:
          return Inv.Colour.FromArgb(Alpha, iMin, iMax, iMid);
        case 3:
          return Inv.Colour.FromArgb(Alpha, iMin, iMid, iMax);
        case 4:
          return Inv.Colour.FromArgb(Alpha, iMid, iMin, iMax);
        case 5:
          return Inv.Colour.FromArgb(Alpha, iMax, iMin, iMid);
        default:
          return Inv.Colour.FromArgb(Alpha, iMax, iMid, iMin);
      }
    }

    public static readonly Inv.Colour AliceBlue = FromArgb(0xFFF0F8FF);
    public static readonly Inv.Colour AntiqueWhite = FromArgb(0xFFFAEBD7);
    //public static readonly Inv.Colour Aqua = FromArgb(0xFF00FFFF); // NOTE: duplicate of Cyan
    public static readonly Inv.Colour Aquamarine = FromArgb(0xFF7FFFD4);
    public static readonly Inv.Colour Azure = FromArgb(0xFFF0FFFF);
    public static readonly Inv.Colour Beige = FromArgb(0xFFF5F5DC);
    public static readonly Inv.Colour Bisque = FromArgb(0xFFFFE4C4);
    public static readonly Inv.Colour Black = FromArgb(0xFF000000);
    public static readonly Inv.Colour BlanchedAlmond = FromArgb(0xFFFFEBCD);
    public static readonly Inv.Colour Blue = FromArgb(0xFF0000FF);
    public static readonly Inv.Colour BlueViolet = FromArgb(0xFF8A2BE2);
    public static readonly Inv.Colour Brown = FromArgb(0xFFA52A2A);
    public static readonly Inv.Colour BurlyWood = FromArgb(0xFFDEB887);
    public static readonly Inv.Colour CadetBlue = FromArgb(0xFF5F9EA0);
    public static readonly Inv.Colour Chartreuse = FromArgb(0xFF7FFF00);
    public static readonly Inv.Colour Chocolate = FromArgb(0xFFD2691E);
    public static readonly Inv.Colour Coral = FromArgb(0xFFFF7F50);
    public static readonly Inv.Colour CornflowerBlue = FromArgb(0xFF6495ED);
    public static readonly Inv.Colour Cornsilk = FromArgb(0xFFFFF8DC);
    public static readonly Inv.Colour Crimson = FromArgb(0xFFDC143C);
    public static readonly Inv.Colour Cyan = FromArgb(0xFF00FFFF);
    public static readonly Inv.Colour DarkBlue = FromArgb(0xFF00008B);
    public static readonly Inv.Colour DarkCyan = FromArgb(0xFF008B8B);
    public static readonly Inv.Colour DarkGoldenrod = FromArgb(0xFFB8860B);
    public static readonly Inv.Colour DarkGray = FromArgb(0xFFA9A9A9);
    public static readonly Inv.Colour DarkGreen = FromArgb(0xFF006400);
    public static readonly Inv.Colour DarkKhaki = FromArgb(0xFFBDB76B);
    public static readonly Inv.Colour DarkMagenta = FromArgb(0xFF8B008B);
    public static readonly Inv.Colour DarkOliveGreen = FromArgb(0xFF556B2F);
    public static readonly Inv.Colour DarkOrange = FromArgb(0xFFFF8C00);
    public static readonly Inv.Colour DarkOrchid = FromArgb(0xFF9932CC);
    public static readonly Inv.Colour DarkRed = FromArgb(0xFF8B0000);
    public static readonly Inv.Colour DarkSalmon = FromArgb(0xFFE9967A);
    public static readonly Inv.Colour DarkSeaGreen = FromArgb(0xFF8FBC8F);
    public static readonly Inv.Colour DarkSlateBlue = FromArgb(0xFF483D8B);
    public static readonly Inv.Colour DarkSlateGray = FromArgb(0xFF2F4F4F);
    public static readonly Inv.Colour DarkTurquoise = FromArgb(0xFF00CED1);
    public static readonly Inv.Colour DarkViolet = FromArgb(0xFF9400D3);
    public static readonly Inv.Colour DarkYellow = FromArgb(0xFFCCCC00);
    public static readonly Inv.Colour DeepPink = FromArgb(0xFFFF1493);
    public static readonly Inv.Colour DeepSkyBlue = FromArgb(0xFF00BFFF);
    public static readonly Inv.Colour DimGray = FromArgb(0xFF696969);
    public static readonly Inv.Colour DodgerBlue = FromArgb(0xFF1E90FF);
    public static readonly Inv.Colour Firebrick = FromArgb(0xFFB22222);
    public static readonly Inv.Colour FloralWhite = FromArgb(0xFFFFFAF0);
    public static readonly Inv.Colour ForestGreen = FromArgb(0xFF228B22);
    //public static readonly Inv.Colour Fuchsia = FromArgb(0xFFFF00FF); // NOTE: duplicate of Magenta
    public static readonly Inv.Colour Gainsboro = FromArgb(0xFFDCDCDC);
    public static readonly Inv.Colour GhostWhite = FromArgb(0xFFF8F8FF);
    public static readonly Inv.Colour Gold = FromArgb(0xFFFFD700);
    public static readonly Inv.Colour Goldenrod = FromArgb(0xFFDAA520);
    public static readonly Inv.Colour Gray = FromArgb(0xFF808080);
    public static readonly Inv.Colour Green = FromArgb(0xFF008000);
    public static readonly Inv.Colour GreenYellow = FromArgb(0xFFADFF2F);
    public static readonly Inv.Colour Honeydew = FromArgb(0xFFF0FFF0);
    public static readonly Inv.Colour HotPink = FromArgb(0xFFFF69B4);
    public static readonly Inv.Colour IndianRed = FromArgb(0xFFCD5C5C);
    public static readonly Inv.Colour Indigo = FromArgb(0xFF4B0082);
    public static readonly Inv.Colour Ivory = FromArgb(0xFFFFFFF0);
    public static readonly Inv.Colour Khaki = FromArgb(0xFFF0E68C);
    public static readonly Inv.Colour Lavender = FromArgb(0xFFE6E6FA);
    public static readonly Inv.Colour LavenderBlush = FromArgb(0xFFFFF0F5);
    public static readonly Inv.Colour LawnGreen = FromArgb(0xFF7CFC00);
    public static readonly Inv.Colour LemonChiffon = FromArgb(0xFFFFFACD);
    public static readonly Inv.Colour LightBlue = FromArgb(0xFFADD8E6);
    public static readonly Inv.Colour LightCoral = FromArgb(0xFFF08080);
    public static readonly Inv.Colour LightCyan = FromArgb(0xFFE0FFFF);
    public static readonly Inv.Colour LightGoldenrodYellow = FromArgb(0xFFFAFAD2);
    public static readonly Inv.Colour LightGray = FromArgb(0xFFD3D3D3);
    public static readonly Inv.Colour LightGreen = FromArgb(0xFF90EE90);
    public static readonly Inv.Colour LightPink = FromArgb(0xFFFFB6C1);
    public static readonly Inv.Colour LightSalmon = FromArgb(0xFFFFA07A);
    public static readonly Inv.Colour LightSeaGreen = FromArgb(0xFF20B2AA);
    public static readonly Inv.Colour LightSkyBlue = FromArgb(0xFF87CEFA);
    public static readonly Inv.Colour LightSlateGray = FromArgb(0xFF778899);
    public static readonly Inv.Colour LightSteelBlue = FromArgb(0xFFB0C4DE);
    public static readonly Inv.Colour LightYellow = FromArgb(0xFFFFFFE0);
    public static readonly Inv.Colour Lime = FromArgb(0xFF00FF00);
    public static readonly Inv.Colour LimeGreen = FromArgb(0xFF32CD32);
    public static readonly Inv.Colour Linen = FromArgb(0xFFFAF0E6);
    public static readonly Inv.Colour Magenta = FromArgb(0xFFFF00FF);
    public static readonly Inv.Colour Maroon = FromArgb(0xFF800000);
    public static readonly Inv.Colour MediumAquamarine = FromArgb(0xFF66CDAA);
    public static readonly Inv.Colour MediumBlue = FromArgb(0xFF0000CD);
    public static readonly Inv.Colour MediumOrchid = FromArgb(0xFFBA55D3);
    public static readonly Inv.Colour MediumPurple = FromArgb(0xFF9370DB);
    public static readonly Inv.Colour MediumSeaGreen = FromArgb(0xFF3CB371);
    public static readonly Inv.Colour MediumSlateBlue = FromArgb(0xFF7B68EE);
    public static readonly Inv.Colour MediumSpringGreen = FromArgb(0xFF00FA9A);
    public static readonly Inv.Colour MediumTurquoise = FromArgb(0xFF48D1CC);
    public static readonly Inv.Colour MediumVioletRed = FromArgb(0xFFC71585);
    public static readonly Inv.Colour MidnightBlue = FromArgb(0xFF191970);
    public static readonly Inv.Colour MintCream = FromArgb(0xFFF5FFFA);
    public static readonly Inv.Colour MistyRose = FromArgb(0xFFFFE4E1);
    public static readonly Inv.Colour Moccasin = FromArgb(0xFFFFE4B5);
    public static readonly Inv.Colour NavajoWhite = FromArgb(0xFFFFDEAD);
    public static readonly Inv.Colour Navy = FromArgb(0xFF000080);
    public static readonly Inv.Colour OldLace = FromArgb(0xFFFDF5E6);
    public static readonly Inv.Colour Olive = FromArgb(0xFF808000);
    public static readonly Inv.Colour OliveDrab = FromArgb(0xFF6B8E23);
    public static readonly Inv.Colour Orange = FromArgb(0xFFFFA500);
    public static readonly Inv.Colour OrangeRed = FromArgb(0xFFFF4500);
    public static readonly Inv.Colour Orchid = FromArgb(0xFFDA70D6);
    public static readonly Inv.Colour PaleGoldenrod = FromArgb(0xFFEEE8AA);
    public static readonly Inv.Colour PaleGreen = FromArgb(0xFF98FB98);
    public static readonly Inv.Colour PaleTurquoise = FromArgb(0xFFAFEEEE);
    public static readonly Inv.Colour PaleVioletRed = FromArgb(0xFFDB7093);
    public static readonly Inv.Colour PapayaWhip = FromArgb(0xFFFFEFD5);
    public static readonly Inv.Colour PeachPuff = FromArgb(0xFFFFDAB9);
    public static readonly Inv.Colour Peru = FromArgb(0xFFCD853F);
    public static readonly Inv.Colour Pink = FromArgb(0xFFFFC0CB);
    public static readonly Inv.Colour Plum = FromArgb(0xFFDDA0DD);
    public static readonly Inv.Colour PowderBlue = FromArgb(0xFFB0E0E6);
    public static readonly Inv.Colour Purple = FromArgb(0xFF800080);
    public static readonly Inv.Colour Red = FromArgb(0xFFFF0000);
    public static readonly Inv.Colour RosyBrown = FromArgb(0xFFBC8F8F);
    public static readonly Inv.Colour RoyalBlue = FromArgb(0xFF4169E1);
    public static readonly Inv.Colour SaddleBrown = FromArgb(0xFF8B4513);
    public static readonly Inv.Colour Salmon = FromArgb(0xFFFA8072);
    public static readonly Inv.Colour SandyBrown = FromArgb(0xFFF4A460);
    public static readonly Inv.Colour SeaGreen = FromArgb(0xFF2E8B57);
    public static readonly Inv.Colour SeaShell = FromArgb(0xFFFFF5EE);
    public static readonly Inv.Colour Sienna = FromArgb(0xFFA0522D);
    public static readonly Inv.Colour Silver = FromArgb(0xFFC0C0C0);
    public static readonly Inv.Colour SkyBlue = FromArgb(0xFF87CEEB);
    public static readonly Inv.Colour SlateBlue = FromArgb(0xFF6A5ACD);
    public static readonly Inv.Colour SlateGray = FromArgb(0xFF708090);
    public static readonly Inv.Colour Snow = FromArgb(0xFFFFFAFA);
    public static readonly Inv.Colour SpringGreen = FromArgb(0xFF00FF7F);
    public static readonly Inv.Colour SteelBlue = FromArgb(0xFF4682B4);
    public static readonly Inv.Colour Tan = FromArgb(0xFFD2B48C);
    public static readonly Inv.Colour Teal = FromArgb(0xFF008080);
    public static readonly Inv.Colour Thistle = FromArgb(0xFFD8BFD8);
    public static readonly Inv.Colour Tomato = FromArgb(0xFFFF6347);
    public static readonly Inv.Colour Transparent = FromArgb(0x00FFFFFF);
    public static readonly Inv.Colour Turquoise = FromArgb(0xFF40E0D0);
    public static readonly Inv.Colour Violet = FromArgb(0xFFEE82EE);
    public static readonly Inv.Colour Wheat = FromArgb(0xFFF5DEB3);
    public static readonly Inv.Colour White = FromArgb(0xFFFFFFFF);
    public static readonly Inv.Colour WhiteSmoke = FromArgb(0xFFF5F5F5);
    public static readonly Inv.Colour Yellow = FromArgb(0xFFFFFF00);
    public static readonly Inv.Colour YellowGreen = FromArgb(0xFF9ACD32);
    public static readonly Inv.DistinctList<Inv.Colour> List;

    private static Bidictionary<Inv.Colour, string> NameDictionary;
  }

  public struct ColourHSLRecord
  {
    public float H; // Hue.
    public float S; // Saturation.
    public float L; // Lightness.
  }

  [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Explicit)]
  public struct ColourARGBRecord
  {
    [System.Runtime.InteropServices.FieldOffset(0)]
    public Int32 Argb;
    [System.Runtime.InteropServices.FieldOffset(3)]
    public byte A;
    [System.Runtime.InteropServices.FieldOffset(2)]
    public byte R;
    [System.Runtime.InteropServices.FieldOffset(1)]
    public byte G;
    [System.Runtime.InteropServices.FieldOffset(0)]
    public byte B;
  }

  // NOTE: this has to be a separate class to Inv.Colour due to the static constructed properties.
  internal static class ColourFactory
  {
    static ColourFactory()
    {
      Dictionary = new Dictionary<int, Colour>();
    }

    public static long MemorySize()
    {
      lock (Dictionary)
        return Dictionary.Count * 16;
    }

    public static Inv.Colour Resolve(int Argb)
    {
      lock (Dictionary)
        return Dictionary.GetOrAdd(Argb, K => new Inv.Colour(K));
    }

    private static Dictionary<int, Inv.Colour> Dictionary;
  }
}