﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using Inv.Support;
using System.Threading.Tasks;

namespace Inv
{
  // Asynchronous but sequential message processing via a delegate.

  public sealed class TaskGate<T> : IDisposable
  {
    public TaskGate(string Name, Action<T> ProcessDelegate, int Limit = 1000)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Name != null, "Name must be specified.");
        Inv.Assert.Check(ProcessDelegate != null, "ProcessDelegate must be specified.");
        Inv.Assert.Check(Limit > 0, "Limit must be greater than 0.");
      }

      this.Name = Name;
      this.IsActive = true;
      this.MessageQueue = new Queue<T>();
      this.PostWaitHandle = new EventWaitHandle(true, EventResetMode.AutoReset);
      this.FlushWaitHandle = new EventWaitHandle(true, EventResetMode.ManualReset);
      this.ProcessWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
      this.ProcessDelegate = ProcessDelegate;
      this.Limit = Limit;
      this.ProcessTask = Inv.TaskGovernor.RunActivity("AdvTaskGate-" + Name, ProcessMethod);
    }
    public void Dispose()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IsActive, "Gate must be active.");

      if (IsActive)
      {
        IsActive = false;
        ProcessWaitHandle.Set();
        ProcessTask.Wait();

        // NOTE: this can happen if you dispose while the queue is not empty - interrupting the processing seems like correct behaviour.
        //Inv.Assert.Check(MessageQueue.Count == 0, "Message queue must be empty after the process thread finishes.");

        ProcessWaitHandle.Dispose();
        PostWaitHandle.Dispose();
        FlushWaitHandle.Dispose();
      }
    }

    public string Name { get; private set; }
    public int Limit { get; private set; }

    public void Post(params T[] MessageArray)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IsActive, "Gate must be active.");

      if (IsActive)
      {
        foreach (var Message in MessageArray)
        {
          if (PostWaitHandle.WaitOne())
          {
            if (ProcessException != null)
              throw ProcessException;

            lock (MessageQueue)
            {
              MessageQueue.Enqueue(Message);

              if (MessageQueue.Count == 1)
              {
                ProcessWaitHandle.Set();
                FlushWaitHandle.Reset();
              }

              if (MessageQueue.Count < Limit)
                PostWaitHandle.Set();
            }
          }
        }
      }
    }
    public void Flush()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IsActive, "Gate must be active.");

      if (IsActive)
      {
        FlushWaitHandle.WaitOne();

        if (ProcessException != null)
          throw ProcessException;
      }
    }

    private void ProcessMethod()
    {
      do
      {
        if (ProcessWaitHandle.WaitOne())
        {
          T Message;

          while (IsActive && TryDequeue(out Message))
          {
            try
            {
              ProcessDelegate(Message);
            }
            catch (Exception Exception)
            {
              ProcessException = Exception;

              FlushWaitHandle.Set();
              PostWaitHandle.Set();

              return; // Unhandled process exception cause the gate to stop.
            }

            lock (MessageQueue)
            {
              if (MessageQueue.Count == 0)
                FlushWaitHandle.Set();
            }
          }
        }
      }
      while (IsActive);
    }
    private bool TryDequeue(out T Record)
    {
      lock (MessageQueue)
      {
        if (MessageQueue.Count == 0)
        {
          Record = default(T);
          return false;
        }
        else
        {
          if (MessageQueue.Count == Limit)
            PostWaitHandle.Set();

          Record = MessageQueue.Dequeue();

          return true;
        }
      }
    }

    private Action<T> ProcessDelegate;
    private Exception ProcessException;
    private bool IsActive;
    private Inv.TaskActivity ProcessTask;
    private EventWaitHandle ProcessWaitHandle;
    private EventWaitHandle PostWaitHandle;
    private EventWaitHandle FlushWaitHandle;
    private Queue<T> MessageQueue;
  }
}
