﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Globalization;
using System.Reflection;
using Inv.Support;

namespace Inv
{
  [DataContract, KnownType(typeof(DateTimeOffset))]
  public struct Choice<TBase, TEnumeration> where TEnumeration : struct
  {
    public Choice(params Type[] InputTypeArray)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(InputTypeArray.Length == Enum.GetNames(typeof(TEnumeration)).Length, "Choice expected correct number of arguments for this enumeration type.");

      this.CurrentIndex = null;
      this.CurrentOption = default(TBase);
      this.TypeArrayField = InputTypeArray;
    }
    public Type[] TypeArray
    {
      get { return TypeArrayField; }
      set { TypeArrayField = value; }
    }
    public TEnumeration? Index
    {
      get { return CurrentIndex; }
      set
      {
        if (value == null)
        {
          CurrentOption = default(TBase);
          CurrentIndex = null;
        }
        else 
        {
          var ObjectType = TypeArrayField[(int)(object)value];

          if (ObjectType == null)
          {
            CurrentOption = default(TBase);
          }
          else
          {
            // NOTE: this is useful for detecting a class of bugs, but it also prevents the programmer from replacing a choice, like you would a normal field.
            //Inv.Assert.Check(!CurrentIndex.Equals(value), "Cannot set the choice to the same type.");

            CurrentOption = (TBase)Activator.CreateInstance(ObjectType);
          }

          CurrentIndex = value;
        }
      }
    }
    public TBase Option
    {
      get { return CurrentOption; }
    }

    public TObject Retrieve<TObject>(TEnumeration RetrieveIndex) where TObject : TBase
    {
      #if DEBUG
      if (CurrentOption == null)
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Cannot retrieve [{0}] when nothing has been stored.", RetrieveIndex));

      if (!CurrentIndex.Equals(RetrieveIndex))
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Cannot retrieve [{0}] when [{1}] has been stored.", RetrieveIndex, CurrentIndex));
      #endif

      return (TObject)CurrentOption;
    }
    public void Store<TObject>(TEnumeration StoreIndex, TObject Object) where TObject : TBase
    {
      #if DEBUG
      if (Object == null)
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Cannot store [{0}] with a null value", StoreIndex));

      /* // NOTE: this is useful for detecting a class of bugs, but it also prevents the programmer from replacing a choice, like you would a normal field.
      if (CurrentIndex != null)
      {
        if (CurrentIndex.Equals(StoreIndex))
          throw new Exception(string.Format(CultureInfo.InvariantCulture, "Cannot store [{0}] when it is already stored.", StoreIndex.ToString()));
        else
          throw new Exception(string.Format(CultureInfo.InvariantCulture, "Cannot store [{0}] when [{1}] is already stored.", StoreIndex.ToString(), CurrentIndex.ToString()));
      }*/

      var ObjectType = TypeArrayField[(int)(object)StoreIndex];

      if (Object.GetType() != ObjectType && !ObjectType.IsInstanceOfType(Object))
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Cannot store [{0}] with object {1} when {2} is expected.", StoreIndex, Object.GetType(), ObjectType));
      #endif

      CurrentIndex = StoreIndex;
      CurrentOption = Object;
    }
    public bool EqualTo(Inv.Choice<TBase, TEnumeration> Value)
    {
      if (Value.CurrentIndex == null && this.CurrentIndex == null)
        return true;
      
      if (Value.CurrentIndex == null || this.CurrentIndex == null)
        return false;

      if (Value.CurrentOption == null && this.CurrentOption == null)
        return true;

      if (Value.CurrentOption == null || this.CurrentOption == null)
        return false;

      return Value.CurrentIndex.Equals(this.CurrentIndex) && Value.CurrentOption.Equals(this.CurrentOption);
    }

    public override bool Equals(object value)
    {
      var Source = value as Inv.Choice<TBase, TEnumeration>?;

      return Source != null && EqualTo(Source.Value);
    }
    public override int GetHashCode()
    {
      if (CurrentIndex == null && CurrentOption == null)
        return base.GetHashCode();
      else if (CurrentOption == null)
        return CurrentIndex.GetHashCode();
      else if (CurrentIndex == null)
        return CurrentOption.GetHashCode();
      else
        return CurrentIndex.GetHashCode() ^ CurrentOption.GetHashCode();
    }

    private Type[] TypeArrayField; 
    [DataMember]
    private TEnumeration? CurrentIndex;
    [DataMember]
    private TBase CurrentOption;
  }
}
