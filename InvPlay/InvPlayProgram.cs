﻿/*! 34 !*/
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using System.Diagnostics;
using Inv.Support;

namespace Inv
{
  public class PlayProgram
  {
    [STAThread]
    static void Main(string[] args)
    {
      var PlaySpec = args.Length > 0 ? new PlaySpec(args[0]) : null;

#if DEBUG
      if (PlaySpec == null)
        PlaySpec = new PlaySpec(@"C:\Development\Forge\Inv\InvManual\InvManual\InvManual.csproj");
      //PlaySpec = new PlaySpec(@"C:\Development\Forge\Inv\InvTest\InvTest\InvTest.csproj");
      //PlaySpec = new PlaySpec(@"C:\Development\Forge\Karisma\KarismaMobile\KarismaMobile.csproj");
#endif

      PlayShell.Run(PlaySpec);
    }
  }

  public static class PlayShell
  {
    public static void Run(PlaySpec PlaySpec)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.PreventDeviceEmulation = false;
        Inv.WpfShell.DeviceEmulation = Inv.WpfDeviceEmulation.iPad_Mini;
        Inv.WpfShell.DeviceEmulationArray = new[] { Inv.WpfDeviceEmulation.iPhone5, Inv.WpfDeviceEmulation.iPhone6_7, Inv.WpfDeviceEmulation.iPhone6_7Plus, Inv.WpfDeviceEmulation.iPad_Mini, Inv.WpfDeviceEmulation.iPadPro };
        Inv.WpfShell.DeviceEmulationRotated = true;
        Inv.WpfShell.DefaultWindowWidth = 1280;
        Inv.WpfShell.DefaultWindowHeight = 1024;
        Inv.WpfShell.FullScreenMode = false;

        var MissingSpec = PlaySpec == null || PlaySpec.ProjectFilePath == null;
        var IncorrectSpec = !MissingSpec && !string.Equals(System.IO.Path.GetExtension(PlaySpec.ProjectFilePath), ".csproj", StringComparison.InvariantCultureIgnoreCase);
        var MissingFile = !MissingSpec && !IncorrectSpec && !System.IO.File.Exists(PlaySpec.ProjectFilePath);

        if (MissingSpec || IncorrectSpec || MissingFile)
        {
          Inv.WpfShell.Run(A =>
          {
            var ErrorSurface = A.Window.NewSurface();

            string ErrorCause;
            if (MissingSpec)
              ErrorCause = ".csproj file path was not provided.";
            else if (IncorrectSpec)
              ErrorCause = "Provided file path must have a .csproj extension.";
            else if (MissingFile)
              ErrorCause = ".csproj file path does not exist:";
            else
              throw new Exception("Error case not handled.");

            var ErrorMessage = 
              "InvPlay requires a single command line parameter which is the full path of your Invention .csproj" + Environment.NewLine + Environment.NewLine +
              "E.g." + Environment.NewLine +
             @"    InvPlay.exe ""C:\Dev\ExampleProject.csproj""" + Environment.NewLine + Environment.NewLine +
             ErrorCause + (!MissingSpec ? Environment.NewLine + Environment.NewLine + PlaySpec.ProjectFilePath : "");

            A.TransitionError(ErrorMessage, Inv.Colour.DarkRed);
          });
        }
        else
        {
          Inv.WpfShell.MainFolderPath = Path.Combine(Inv.WpfShell.MainFolderPath, Path.GetFileNameWithoutExtension(PlaySpec.ProjectFilePath));
          System.IO.Directory.CreateDirectory(Inv.WpfShell.MainFolderPath);

          new PlayApplication(PlaySpec).Run();
        }
      });
    }

    internal static void TransitionError(this Inv.Application Application, string Message, Inv.Colour Colour)
    {
      var Surface = Application.Window.NewSurface();
      Surface.Background.Colour = Colour;

      var Memo = Surface.NewMemo();
      Surface.Content = Memo;
      Memo.Font.Name = Surface.Window.Application.Device.MonospacedFontName;
      Memo.Font.Colour = Inv.Colour.White;
      Memo.Font.Size = 18;
      Memo.Alignment.Center();
      Memo.IsReadOnly = true;
      Memo.Text = Message;

      Surface.Transition();
    }
  }

  public sealed class PlaySpec
  {
    public PlaySpec(string ProjectFilePath)
    {
      this.ProjectFilePath = ProjectFilePath;
    }

    public string ProjectFilePath { get; private set; }
  }

  public sealed class PlayApplication
  {
    internal PlayApplication(PlaySpec PlaySpec)
    {
      this.PlaySpec = PlaySpec;
      this.WpfEngine = new WpfEngine();

      this.WatchSet = new HashSet<string>();
      this.CodeFilePathList = new Inv.DistinctList<string>();

      this.InvPlaygroundProjectId = ProjectId.CreateNewId(); // InvPlayProject.Id;

      AppDomain.CurrentDomain.AssemblyResolve += (Sender, Event) =>
      {
        if (Event.RequestingAssembly == null)
          return null; // inside visual studio extensions?

        if (OutputPath == null)
          return null; // this is when starting up.

        var fields = Event.Name.Split(',');
        var name = fields[0];
        var culture = fields[2];
        // failing to ignore queries for satellite resource assemblies or using [assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)] 
        // in AssemblyInfo.cs will crash the program on non en-US based system cultures.
        if (name.EndsWith(".resources") && !culture.EndsWith("neutral"))
          return null;

        var ResolvePath = Path.Combine(OutputPath, name + ".dll");
        return Assembly.LoadFile(ResolvePath);
      };
    }

    public void Run()
    {
      ExecutePlayground(true);

      using (var WatchThrottle = new Inv.TaskThrottle())
      using (var Watcher = new FileSystemWatcher(Path.GetDirectoryName(PlaySpec.ProjectFilePath)))
      {
        var ReloadProject = false;

        WatchThrottle.ThresholdTime = TimeSpan.FromMilliseconds(50);
        WatchThrottle.FireEvent += () =>
        {
          WpfEngine.Asynchronise(() =>
          {
            WpfEngine.Unload();
            try
            {
              ExecutePlayground(ReloadProject);
            }
            finally
            {
              WpfEngine.Reload();
            }

            if (ReloadProject)
              ReloadProject = false;
          });
        };

        Watcher.Changed += (Sender, Event) =>
        {
          // TODO: WatchSet is not threadsafe.
          if (WatchSet.Contains(Event.FullPath))
          {
            if (string.Equals(Event.FullPath, PlaySpec.ProjectFilePath, StringComparison.InvariantCultureIgnoreCase))
              ReloadProject = true;

            WatchThrottle.Fire();
          }
        };
        Watcher.NotifyFilter |= NotifyFilters.Attributes;
        Watcher.NotifyFilter |= NotifyFilters.CreationTime;
        Watcher.NotifyFilter |= NotifyFilters.DirectoryName;
        Watcher.NotifyFilter |= NotifyFilters.FileName;
        Watcher.NotifyFilter |= NotifyFilters.LastAccess;
        Watcher.NotifyFilter |= NotifyFilters.LastWrite;
        Watcher.NotifyFilter |= NotifyFilters.Security;
        Watcher.NotifyFilter |= NotifyFilters.Size;
        Watcher.EnableRaisingEvents = true;
        Watcher.IncludeSubdirectories = true;

        WpfEngine.Run();
      }
    }

    private void ExecutePlayground(bool Reload)
    {
      var Application = new Inv.Application();
      WpfEngine.Install(Application);

      var Delegate = LoadPlayground(Reload);
      try
      {
        Delegate(Application);
      }
      catch (Exception Exception)
      {
        Application.TransitionError(Exception.AsReport(), Inv.Colour.DarkRed);
      }
    }
    private Action<Inv.Application> LoadPlayground(bool ReloadProject)
    {
      var ProjectFilePath = PlaySpec.ProjectFilePath;

      var SW = new Stopwatch();

      string BuildPath = null;

      if (Solution == null || ReloadProject)
      {
        var BuildWorkspace = SW.Measure<Microsoft.CodeAnalysis.MSBuild.MSBuildWorkspace>("Create workspace", () => Microsoft.CodeAnalysis.MSBuild.MSBuildWorkspace.Create(new Dictionary<string, string> { { "CheckForSystemRuntimeDependency", "true" } }));
        var BuildProject = SW.Measure<Project>("Load project", () =>
        {
          return BuildWorkspace.OpenProjectAsync(ProjectFilePath).Result;
        });

        BuildPath = BuildProject.OutputFilePath;

        this.Solution = SW.Measure<Solution>("Compile playground", () =>
        {
          var InvPlaygroundSolution = new AdhocWorkspace().CurrentSolution;

          InvPlaygroundSolution = InvPlaygroundSolution.AddProject(InvPlaygroundProjectId, BuildProject.Name, BuildProject.AssemblyName, LanguageNames.CSharp);
          InvPlaygroundSolution = InvPlaygroundSolution.AddMetadataReferences(InvPlaygroundProjectId, BuildProject.MetadataReferences);

          foreach (var ProjectReference in BuildProject.ProjectReferences)
          {
            var ReferenceProject = BuildWorkspace.CurrentSolution.GetProject(ProjectReference.ProjectId);

            // TODO: something like this to include more than one .csproj 'in memory'.
            /*if (ReferenceProject.Name == "KarismaPortable")
            {
              var PortableProjectId = ProjectId.CreateNewId(); // ReferenceProject.Id;

              InvPlaygroundSolution = InvPlaygroundSolution.AddProject(PortableProjectId, ReferenceProject.Name, ReferenceProject.AssemblyName, LanguageNames.CSharp);
              InvPlaygroundSolution = InvPlaygroundSolution.AddMetadataReferences(PortableProjectId, ReferenceProject.MetadataReferences);

              CompileProject(ref InvPlaygroundSolution, PortableProjectId, ReferenceProject.FilePath, ResourceDescriptionList);

              foreach (var NestedReference in ReferenceProject.ProjectReferences)
              {
                var NestedProject = WS.CurrentSolution.GetProject(NestedReference.ProjectId);
                InvPlaygroundSolution = InvPlaygroundSolution.AddMetadataReference(PortableProjectId, MetadataReference.CreateFromFile(NestedProject.OutputFilePath));
              }

              InvPlaygroundSolution = InvPlaygroundSolution.AddProjectReference(InvPlaygroundProjectId, new ProjectReference(PortableProjectId));
            }
            else*/
            {
              //InvPlaygroundSolution = InvPlaygroundSolution.AddMetadataReference(InvPlaygroundProjectId, MetadataReference.CreateFromFile(ReferenceProject.OutputFilePath));
              InvPlaygroundSolution = InvPlaygroundSolution.AddMetadataReference(InvPlaygroundProjectId, MetadataReference.CreateFromFile(Path.Combine(Path.GetDirectoryName(BuildProject.OutputFilePath), Path.GetFileName(ReferenceProject.OutputFilePath))));
            }
          }

          return InvPlaygroundSolution.WithProjectCompilationOptions(InvPlaygroundProjectId, new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));
          //return InvPlaygroundSolution.WithProjectCompilationOptions(InvPlaygroundProjectId, new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary).WithMetadataReferenceResolver(BuildProject.CompilationOptions.MetadataReferenceResolver));
        });

        var ProjectFolderPath = Path.GetDirectoryName(ProjectFilePath);

        var CSharpProject = new Inv.CSharpProject();
        CSharpProject.LoadFromFile(ProjectFilePath);

        this.CodeFilePathList.Clear();
        foreach (var CSharpIncludeReference in CSharpProject.IncludeReference)
        {
          var CodeFilePath = CSharpIncludeReference.File.Path;
          if (string.Equals(Path.GetExtension(CodeFilePath), ".cs", StringComparison.InvariantCultureIgnoreCase) || string.Equals(Path.GetExtension(CodeFilePath), ".rs", StringComparison.InvariantCultureIgnoreCase))
            CodeFilePathList.Add(Path.Combine(ProjectFolderPath, CSharpIncludeReference.File.Path));
        }

        WatchSet.Clear();
        WatchSet.Add(ProjectFilePath); // watch the .csproj itself.
        WatchSet.AddRange(CodeFilePathList);
      }

      var ResourceDescriptionList = new Inv.DistinctList<ResourceDescription>();

      var Compilation = SW.Measure("Prepare Playground", () =>
      {
        var CompileSolution = Solution;
        CompileProject(ref CompileSolution, InvPlaygroundProjectId, ProjectFilePath, ResourceDescriptionList);
        var Project = CompileSolution.GetProject(InvPlaygroundProjectId);
        var Result = Project.GetCompilationAsync().Result;

        // needs to be after all the Roslyn assemblies have been resolved.
        if (BuildPath != null)
          this.OutputPath = Path.GetDirectoryName(BuildPath);

        return Result;
      });

      EmitResult EmitResult = null;
      Assembly InvPlaygroundAssembly = null;

      SW.Measure("Emit Playground", () =>
      {
        using (var AssemblyStream = new MemoryStream())
        using (var SymbolStream = new MemoryStream())
        {
          EmitResult = Compilation.Emit(AssemblyStream, SymbolStream, manifestResources: ResourceDescriptionList.ToArray());

          if (EmitResult.Success)
          {
            var AssemblyBytes = AssemblyStream.GetBuffer();
            var SymbolBytes = SymbolStream.GetBuffer();

            InvPlaygroundAssembly = Assembly.Load(AssemblyBytes, SymbolBytes);
          }
        }
      });

      var ResultMessage = EmitResult.Diagnostics.Length == 0 ? string.Empty : EmitResult.Diagnostics.Select(D => D.ToString()).AsSeparatedText("\n");
      var ResultColour = Inv.Colour.Black;

      Action<Inv.Application> ResultAction = null;

      if (EmitResult.Success)
      {
        ResultAction = SW.Measure<Action<Inv.Application>>("Run Playground", () =>
        {
          try
          {
            foreach (var CandidateType in InvPlaygroundAssembly.ExportedTypes)
            {
              // ignore non-static classes and the special 'Resources' class.
              if (!CandidateType.IsAbstract || CandidateType.Name == "Resources")
                continue;

              foreach (var RuntimeMethod in CandidateType.GetRuntimeMethods())
              {
                if (RuntimeMethod.ReturnType == typeof(void) && RuntimeMethod.GetParameters().Length == 1 && RuntimeMethod.GetParameters()[0].ParameterType == typeof(Inv.Application))
                {
                  var AssetFolderPathField = CandidateType.GetField("AssetFolderPath");
                  if (AssetFolderPathField != null)
                    Inv.WpfShell.OverrideAssetFolderPath = (string)AssetFolderPathField.GetValue(null);
                  else
                    Inv.WpfShell.OverrideAssetFolderPath = null;

                  return (Application) => RuntimeMethod.Invoke(null, new object[] { Application });
                }
              }
            }

            ResultColour = Inv.Colour.DarkSlateGray;
            ResultMessage =
@"Invention main method was not found in the project.

There must be a public static class with a public static void method that takes an Inv.Application parameter. For example:

namespace MyProject
{
  public static class Shell
  {
    public static void Main(Inv.Application Application)
    {
      // application logic goes here.
    }
  }
}";
            return null;
          }
          catch (Exception Exception)
          {
            ResultMessage = Exception.AsReport();
            return null;
          }
        });
      }

      if (ResultAction == null)
        ResultAction = (Application) => Application.TransitionError(ResultMessage, ResultColour);

      return ResultAction;
    }

    private void CompileProject(ref Solution InvPlaygroundSolution, ProjectId InvPlaygroundProjectId, string ProjectFilePath, DistinctList<ResourceDescription> ResourceDescriptionList)
    {
      foreach (var CodeFilePath in CodeFilePathList)
      {
        if (string.Equals(Path.GetExtension(CodeFilePath), ".cs", StringComparison.InvariantCultureIgnoreCase))
        {
          using (var FileStream = new FileStream(CodeFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
//          using (var StreamReader = new StreamReader(FileStream, System.Text.Encoding.UTF8))
            InvPlaygroundSolution = InvPlaygroundSolution.AddDocument(DocumentId.CreateNewId(InvPlaygroundProjectId), Path.GetFileName(CodeFilePath), Microsoft.CodeAnalysis.Text.SourceText.From(FileStream, System.Text.Encoding.UTF8), null);
        }
        else if (string.Equals(Path.GetExtension(CodeFilePath), ".rs", StringComparison.InvariantCultureIgnoreCase))
        {
          ResourceDescriptionList.Add(new ResourceDescription("Resources." + Path.GetFileName(CodeFilePath), () => System.IO.File.Open(CodeFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite), true));
        }
        else
        {
          Debug.Assert(false, CodeFilePath);
        }
      }
    }

    private WpfEngine WpfEngine;
    private PlaySpec PlaySpec;
    private HashSet<string> WatchSet;
    private Solution Solution;
    private string OutputPath;
    private ProjectId InvPlaygroundProjectId;
    private DistinctList<string> CodeFilePathList;
  }
}