/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

#if DEBUG
namespace Inv
{
  public abstract class AndroidDebugRenderActivity : Android.App.Activity
  {
    protected override void OnCreate(Android.OS.Bundle bundle)
    {
      base.OnCreate(bundle);
      RequestWindowFeature(Android.Views.WindowFeatures.NoTitle);
      Window.AddFlags(Android.Views.WindowManagerFlags.Fullscreen | Android.Views.WindowManagerFlags.HardwareAccelerated);
      Window.SetSoftInputMode(Android.Views.SoftInput.StateHidden);

      var UiFlags = Android.Views.SystemUiFlags.Fullscreen | Android.Views.SystemUiFlags.HideNavigation | Android.Views.SystemUiFlags.ImmersiveSticky;
      Window.DecorView.SystemUiVisibility = (Android.Views.StatusBarVisibility)UiFlags;

      this.Choreographer = Android.Views.Choreographer.Instance;
      this.FrameCallback = new RenderFrameCallback(Process);

      this.TileSize = 128;

      this.FrameView = new RenderFrameView(this, Canvas =>
      {
        //if (this.Library == RenderLibrary.SkiaSharp)
        //  SkiaRender(Canvas);
        //else if (this.Library == RenderLibrary.AndroidGraphics)
          GraphicsRender(Canvas);
      });
      FrameView.TapEvent += () =>
      {
        if (this.Library == RenderLibrary.SkiaSharp)
          this.Library = RenderLibrary.AndroidGraphics;
        //else if (this.Library == RenderLibrary.AndroidGraphics)
        //  this.Library = RenderLibrary.UrhoSharp;
        else
          this.Library = RenderLibrary.SkiaSharp;

        Refresh();
      };
      FrameView.ZoomEvent += (ZoomIn) =>
      {
        if (ZoomIn)
          TileSize++;
        else
          TileSize--;

        if (TileSize > 256)
          TileSize = 256;
        else if (TileSize < 16)
          TileSize = 16;
      };

      var Assembly = typeof(AndroidDebugRenderActivity).GetTypeInfo().Assembly;

      /*
      // skia.
      this.SkiaWaterBitmap = SkiaSharp.SKBitmap.Decode(new SkiaSharp.SKManagedStream(Assembly.GetManifestResourceStream("InvTest.Water.png")));

      this.SkiaFpsTextPaint = new SkiaSharp.SKPaint()
      {
        IsAntialias = true,
        TextAlign = SkiaSharp.SKTextAlign.Right,
        TextSize = 40,
        Color = SkiaSharp.SKColors.White
      };

      this.SkiaRect = new SkiaSharp.SKRect();
      */

      // graphics.
      this.GraphicsWaterBitmap = Android.Graphics.BitmapFactory.DecodeStream(Assembly.GetManifestResourceStream("InvTest.Water.png"));

      this.GraphicsFpsTextPaint = new Android.Graphics.Paint()
      {
        AntiAlias = true,
        Color = Android.Graphics.Color.White,
        TextSize = 40,
        TextAlign = Android.Graphics.Paint.Align.Right
      };

      this.GraphicsBackgroundPaint = new Android.Graphics.Paint()
      {
        AntiAlias = true,
        Color = Android.Graphics.Color.Black
      };

      this.GraphicsRect = new Android.Graphics.Rect();

      // urho.
      //this.UrhoView = Urho.Droid.UrhoSurface.CreateSurface<RenderUrhoApplication>(this);

      this.Library = RenderLibrary.SkiaSharp;
      Refresh();

      // start the frame loop.
      Process();
    }

    private void Refresh()
    {
      //if (Library == RenderLibrary.UrhoSharp)
      //  SetContentView(UrhoView);
      //else
        SetContentView(FrameView);
    }
    private void Process()
    {
      FrameView.Invalidate();

      var CurrentTick = System.Environment.TickCount;
      var Difference = CurrentTick - FPSLastTick;

      if (Difference < 0 || Difference >= 1000)
      {
        this.FPSLastCount = FPSCurrentCount;
        this.FPSCurrentCount = 0;
        this.FPSLastTick = System.Environment.TickCount;
      }

      FPSCurrentCount++;

      Choreographer.PostFrameCallback(FrameCallback);
    }

    /*
    private void SkiaRender(Android.Graphics.Canvas Canvas)
    {
      var CanvasWidth = Canvas.Width;
      var CanvasHeight = Canvas.Height;

      if (Backbuffer != null && (Backbuffer.Width != CanvasWidth || Backbuffer.Height != CanvasHeight))
      {
        Backbuffer.Dispose();
        this.Backbuffer = null;
      }

      if (Backbuffer == null)
        this.Backbuffer = Android.Graphics.Bitmap.CreateBitmap(CanvasWidth, CanvasHeight, Android.Graphics.Bitmap.Config.Argb8888);

      var TileWidth = CanvasWidth / TileSize;
      var TileHeight = CanvasHeight / TileSize;

      var Pixels = Backbuffer.LockPixels();
      try
      {
        using (var SKSurface = SkiaSharp.SKSurface.Create(CanvasWidth, CanvasHeight, SkiaSharp.SKColorType.Rgba_8888, SkiaSharp.SKAlphaType.Premul, Pixels, CanvasWidth * 4))
        {
          var SKCanvas = SKSurface.Canvas;

          // test case logic.
          SKCanvas.Clear(SkiaSharp.SKColors.Black);

          SkiaRect.Top = 0;
          SkiaRect.Bottom = TileSize;

          for (var Y = 0; Y <= TileHeight; Y++)
          {
            SkiaRect.Left = 0;
            SkiaRect.Right = TileSize;

            for (var X = 0; X <= TileWidth; X++)
            {
              SKCanvas.DrawBitmap(SkiaWaterBitmap, SkiaRect, null);

              SkiaRect.Left += TileSize;
              SkiaRect.Right += TileSize;
            }

            SkiaRect.Top += TileSize;
            SkiaRect.Bottom += TileSize;
          }

          SKCanvas.DrawText("(tap to toggle to Android.Graphics, pinch-and-zoom to change tile size)", CanvasWidth - 8, SkiaFpsTextPaint.TextSize, SkiaFpsTextPaint);
          SKCanvas.DrawText("SkiaSharp (" + TileWidth + " by " + TileHeight + " @ " + TileSize + "x" + TileSize + ") " + FPSLastCount + " fps", CanvasWidth - 8, CanvasHeight - 8, SkiaFpsTextPaint);
        }
      }
      finally
      {
        Backbuffer.UnlockPixels();
      }

      Canvas.DrawBitmap(Backbuffer, 0, 0, null);
    }
    */
    private void GraphicsRender(Android.Graphics.Canvas Canvas)
    {
      // test case logic.
      var CanvasWidth = Canvas.Width;
      var CanvasHeight = Canvas.Height;

      Canvas.DrawRect(0, 0, CanvasWidth, CanvasHeight, GraphicsBackgroundPaint);

      var TileWidth = CanvasWidth / TileSize;
      var TileHeight = CanvasHeight / TileSize;

      GraphicsRect.Top = 0;
      GraphicsRect.Bottom = TileSize;

      for (var Y = 0; Y <= TileHeight; Y++)
      {
        GraphicsRect.Left = 0;
        GraphicsRect.Right = TileSize;

        for (var X = 0; X <= TileWidth; X++)
        {
          Canvas.DrawBitmap(GraphicsWaterBitmap, null, GraphicsRect, null);

          GraphicsRect.Left += TileSize;
          GraphicsRect.Right += TileSize;
        }

        GraphicsRect.Top += TileSize;
        GraphicsRect.Bottom += TileSize;
      }

      Canvas.DrawText("(tap to toggle to SkiaSharp, pinch-and-zoom to change tile size)", CanvasWidth - 8, GraphicsFpsTextPaint.TextSize, GraphicsFpsTextPaint);
      Canvas.DrawText("Android.Graphics (" + TileWidth + " by " + TileHeight + " @ " + TileSize + "x" + TileSize +") " + FPSLastCount + " fps", CanvasWidth - 8, CanvasHeight - 8, GraphicsFpsTextPaint);
    } 

    private Choreographer Choreographer;
    private RenderFrameCallback FrameCallback;
    private RenderFrameView FrameView;
    private int FPSLastTick;
    private int FPSLastCount;
    private int FPSCurrentCount;
    private RenderLibrary Library;
    private int TileSize;

    private Android.Graphics.Bitmap GraphicsWaterBitmap;
    private Android.Graphics.Rect GraphicsRect;
    private Android.Graphics.Paint GraphicsFpsTextPaint;
    private Android.Graphics.Paint GraphicsBackgroundPaint;

    //private SkiaSharp.SKBitmap SkiaWaterBitmap;
    //private SkiaSharp.SKRect SkiaRect;
    //private SkiaSharp.SKPaint SkiaFpsTextPaint;
    //private Android.Graphics.Bitmap Backbuffer;
    //private Org.Libsdl.App.SDLSurface UrhoView;
  }

  enum RenderLibrary
  {
    SkiaSharp,
    AndroidGraphics,
    UrhoSharp
  }

  class RenderFrameView : Android.Views.View, ScaleGestureDetector.IOnScaleGestureListener
  {
    public RenderFrameView(Context Context, Action<Android.Graphics.Canvas> RenderAction)
      : base(Context)
    {
      this.RenderAction = RenderAction;
      this.ScaleDetector = new ScaleGestureDetector(Context, this);
    }

    public event Action TapEvent;
    public event Action<bool> ZoomEvent;

    public override void Draw(Android.Graphics.Canvas Canvas)
    {
      base.Draw(Canvas);

      RenderAction(Canvas);
    }
    public override bool OnTouchEvent(MotionEvent Event)
    {
      ScaleDetector.OnTouchEvent(Event);

      switch (Event.Action & MotionEventActions.Mask)
      {
        case MotionEventActions.Down:
          this.IsPressed = true;
          break;

        case MotionEventActions.Up:
          if (IsPressed)
          {
            this.IsPressed = false;

            if (TapEvent != null)
              TapEvent();
          }
          break;
      }

      return true;
    }

    bool ScaleGestureDetector.IOnScaleGestureListener.OnScale(ScaleGestureDetector detector)
    {
      this.IsPressed = false;

      if (ZoomEvent != null)
        ZoomEvent((int)(detector.ScaleFactor) > 0 ? true : false);

      return true;
    }
    bool ScaleGestureDetector.IOnScaleGestureListener.OnScaleBegin(ScaleGestureDetector detector)
    {
      return true;
    }
    void ScaleGestureDetector.IOnScaleGestureListener.OnScaleEnd(ScaleGestureDetector detector)
    {
    }

    private Action<Android.Graphics.Canvas> RenderAction;
    private ScaleGestureDetector ScaleDetector;
    private bool IsPressed;
  }

  class RenderFrameCallback : Java.Lang.Object, Android.Views.Choreographer.IFrameCallback
  {
    public RenderFrameCallback(Action Action)
    {
      this.Action = Action;
    }

    void Android.Views.Choreographer.IFrameCallback.DoFrame(long frameTimeNanos)
    {
      Action();
    }

    private Action Action;
  }
  /*
  class RenderUrhoApplication : Urho.Application
  {
    private Urho.Scene Scene;
    private Urho.Node CameraNode;

    protected override void Start()
    {
      base.Start();

      this.Scene = new Urho.Scene();
      Scene.CreateComponent<Urho.Octree>();

      this.CameraNode = Scene.CreateChild("Camera");
      CameraNode.Position = new Urho.Vector3(0.0f, 0.0f, -10.0f);

      var camera = CameraNode.CreateComponent<Urho.Camera>();
      camera.Orthographic = true;
      camera.OrthoSize = Graphics.Height * 1;
      camera.Zoom = 1.5f * Math.Min(Graphics.Width / 1280.0f, Graphics.Height / 800.0f); // Set zoom according to user's resolution to ensure full visibility (initial zoom (1.5) is set for full visibility at 1280x800 resolution)

      //spriteNode = scene.CreateChild("SpriterAnimation");

      Renderer.SetViewport(0, new Urho.Viewport(Context, Scene, CameraNode.GetComponent<Urho.Camera>(), null));
    }
  }*/
}
#endif