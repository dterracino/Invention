/*! 14 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using Inv.Support;

namespace Inv
{
  public abstract class AndroidActivity : Android.App.Activity, Android.Locations.ILocationListener
  {
    public abstract void Install(Inv.Application Application);

    public override void OnLowMemory()
    {
#if DEBUG
      Engine.Reclamation();
#endif

      base.OnLowMemory();
    }
    public override bool OnKeyDown(Android.Views.Keycode Keycode, Android.Views.KeyEvent Event)
    {
      switch (Keycode)
      {
        case Android.Views.Keycode.VolumeUp:
          AudioManager.AdjustStreamVolume(Android.Media.Stream.Music, Android.Media.Adjust.Raise, Android.Media.VolumeNotificationFlags.ShowUi);
          return true;

        case Android.Views.Keycode.VolumeDown:
          AudioManager.AdjustStreamVolume(Android.Media.Stream.Music, Android.Media.Adjust.Lower, Android.Media.VolumeNotificationFlags.ShowUi);
          return true;

        default:
          break;
      }

      if (!IsTransitioning)
      {
        Engine.Guard(() =>
        {
          var InvSurface = Engine.InvApplication.Window.ActiveSurface;
          if (InvSurface != null)
          {
            var InvKey = Engine.TranslateKey(Keycode);
            if (InvKey != null)
            {
              InvSurface.KeystrokeInvoke(new Keystroke()
              {
                Key = InvKey.Value,
                Modifier = new KeyModifier()
                {
                  IsLeftAlt = Event.IsAltPressed,
                  IsRightAlt = Event.IsAltPressed,
                  IsLeftCtrl = Event.IsCtrlPressed,
                  IsRightCtrl = Event.IsCtrlPressed,
                  IsLeftShift = Event.IsShiftPressed,
                  IsRightShift = Event.IsShiftPressed,
                }
              });
            }
          }
        });
      }

      return base.OnKeyDown(Keycode, Event);
    }
    public override void OnBackPressed()
    {
      Engine.Guard(() =>
      {
        var InvSurface = Engine.InvApplication.Window.ActiveSurface;

        if (InvSurface != null && InvSurface.GestureBackwardQuery())
        {
          InvSurface.GestureBackwardInvoke();
        }
        else if (Engine.InvApplication.ExitQueryInvoke())
        {
          // TODO: this returns to the splash activity which effectively ends this activity.
          base.OnBackPressed(); 
        }
      });
    }
    public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
    {
      base.OnConfigurationChanged(newConfig);

      Engine.Guard(() =>
      {
        Engine.Resize();

        var InvSurface = Engine.InvApplication.Window.ActiveSurface;
        if (InvSurface != null)
          InvSurface.ArrangeInvoke();
      });
    }
    public override void AddContentView(Android.Views.View view, Android.Views.ViewGroup.LayoutParams @params)
    {
      base.AddContentView(view, @params);

      ContentViewList.Add(view);
    }
    public override bool DispatchTouchEvent(Android.Views.MotionEvent ev)
    {
      if (IsTransitioning)
      {
        LeftEdgeSwipe = false;
        RightEdgeSwipe = false;

        return true; // ignore interaction while transitioning.
      }
      else if (ev.Action == Android.Views.MotionEventActions.Down && ev.RawX <= 10)
      {
        LeftEdgeSwipe = true;
        RightEdgeSwipe = false;

        return true;
      }
      else if (ev.Action == Android.Views.MotionEventActions.Down && ev.RawX >= Engine.DisplayMetrics.WidthPixels - 10)
      {
        LeftEdgeSwipe = false;
        RightEdgeSwipe = true;

        return true;
      }
      else if (ev.Action == Android.Views.MotionEventActions.Move && (LeftEdgeSwipe || RightEdgeSwipe))
      {
        var InvSurface = Engine.InvApplication.Window.ActiveSurface;

        if (InvSurface != null)
        {
          if (LeftEdgeSwipe && ev.RawX >= 0)
          {
            if (ev.RawX >= 20)
            {
              Engine.Guard(() => InvSurface.GestureBackwardInvoke());
              LeftEdgeSwipe = false;
            }
          }
          else if (RightEdgeSwipe && ev.RawX <= Engine.DisplayMetrics.WidthPixels)
          {
            if (ev.RawX <= Engine.DisplayMetrics.WidthPixels - 20)
            {
              Engine.Guard(() => InvSurface.GestureForwardInvoke());
              RightEdgeSwipe = false;
            }
          }
          else
          {
            LeftEdgeSwipe = false;
            RightEdgeSwipe = false;
          }
        }
        else
        {
          LeftEdgeSwipe = false;
          RightEdgeSwipe = false;
        }

        return true;
      }
      else
      {
        LeftEdgeSwipe = false;
        RightEdgeSwipe = false;

        return base.DispatchTouchEvent(ev);
      }
    }
    public void OnLocationChanged(Android.Locations.Location location)
    {
      Engine.Guard(() => Engine.InvApplication.Location.ChangeInvoke(new Inv.Coordinate(location.Latitude, location.Longitude, location.Altitude)));
    }
    public void OnProviderDisabled(string provider)
    {
    }
    public void OnProviderEnabled(string provider)
    {
    }
    public void OnStatusChanged(string provider, Android.Locations.Availability status, Android.OS.Bundle extras)
    {
    }
    public void RemoveContentView(Android.Views.View view)
    {
      ((Android.Views.ViewGroup)view.Parent).RemoveView(view);

      ContentViewList.Remove(view);
    }
    public bool HasContentView(Android.Views.View view)
    {
      return ContentViewList.Contains(view);
    }
    public Android.Views.View GetContentView()
    {
      return ContentViewList.Count > 0 ? ContentViewList[0] : null;
    }

    internal const int PickFileId = 1000;

    protected override void OnCreate(Android.OS.Bundle bundle)
    {
      // check the required attribute parameters are set.
#if DEBUG
      var AndroidActivityAttribute = GetType().CustomAttributes.Find(A => A.AttributeType == typeof(Android.App.ActivityAttribute));

      var LabelParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "Label");
      Debug.Assert(LabelParameter != null && LabelParameter.TypedValue.Value != null, "[Activity] attribute must include Label parameter");

      var MainLauncherParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "MainLauncher");
      Debug.Assert(MainLauncherParameter != null && MainLauncherParameter.TypedValue.Value != null, "[Activity] attribute must include MainLauncher parameter");

      var ThemeParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "Theme");
      Debug.Assert(ThemeParameter != null && ThemeParameter.TypedValue.Value != null, "[Activity] attribute must include Theme parameter");

      var IconParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "Icon");
      Debug.Assert(IconParameter != null && IconParameter.TypedValue.Value != null, "[Activity] attribute must include Icon parameter");

      var ConfigurationChangesParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "ConfigurationChanges");
      Debug.Assert(ConfigurationChangesParameter != null && (Android.Content.PM.ConfigChanges)(ConfigurationChangesParameter.TypedValue.Value) == (Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize), "[Activity] attribute must specify ConfigurationChanges parameter = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize");
#endif

      // NOTE: any exceptions in the constructor will terminate the application without any trace.

      RequestWindowFeature(Android.Views.WindowFeatures.NoTitle);
      Window.AddFlags(Android.Views.WindowManagerFlags.Fullscreen | Android.Views.WindowManagerFlags.HardwareAccelerated);

      Window.SetSoftInputMode(Android.Views.SoftInput.StateHidden);

      base.OnCreate(bundle);

      this.ContentViewList = new DistinctList<Android.Views.View>();

      var Application = new Inv.Application();

      this.Engine = new Inv.AndroidEngine(Application, this);

      Install(Application);

      Engine.Start();

      Window.DecorView.SystemUiVisibilityChange += (Sender, Event) =>
      {
        if (((Android.Views.SystemUiFlags)Event.Visibility & Android.Views.SystemUiFlags.Fullscreen) == 0)
          Engine.Immersive();
      };

      // TODO: this is not re-entrant.
      AppDomain.CurrentDomain.UnhandledException += (Sender, Event) =>
      {
        var Exception = Event.ExceptionObject as Exception;
        if (Exception != null)
          Engine.InvApplication.HandleExceptionInvoke(Exception);
      };

      // TODO: this is not re-entrant.
      Android.Runtime.AndroidEnvironment.UnhandledExceptionRaiser += (Sender, Event) =>
      {
        var Exception = Event.Exception;
        if (Exception != null)
          Engine.InvApplication.HandleExceptionInvoke(Exception);
        Event.Handled = true;
      };

      this.AudioManager = (Android.Media.AudioManager)GetSystemService(Android.Content.Context.AudioService);
    }
    protected override void OnDestroy()
    {
      // NOTE: destroy is not guaranteed to run.
      Engine.Stop();

      base.OnDestroy();
    }
    protected override void OnStop()
    {
      // NOTE: stop seems to be called all the time...

      base.OnStop();
    }
    protected override void OnPause()
    {
      base.OnPause();

      Engine.Pause();
    }
    protected override void OnResume()
    {
      base.OnResume();

      Engine.Resume();
    }
    protected override void OnActivityResult(int requestCode, Android.App.Result resultCode, Android.Content.Intent data)
    {
      try
      {
        if (requestCode == PickFileId && data != null)
        {
          if (ActiveFilePicker != null)
          {
            if (resultCode == Android.App.Result.Ok)
            {
              using (var ContentStream = ContentResolver.OpenInputStream(data.Data))
              using (var MemoryStream = new System.IO.MemoryStream())
              {
                ContentStream.CopyTo(MemoryStream);

                MemoryStream.Flush();

                var MemoryBinary = new Inv.Binary(MemoryStream.ToArray(), data.Type);

                Engine.Guard(() => ActiveFilePicker.SelectInvoke(MemoryBinary));
              }
            }
            else if (resultCode == Android.App.Result.Canceled)
            {
              Engine.Guard(() => ActiveFilePicker.CancelInvoke());
            }
            else
            {
              // what is Android.App.Result.FirstUser?
              return;
            }

            this.ActiveFilePicker = null;
          }
        }
      }
      catch (Exception Exception)
      {
        Engine.Guard(() =>
        {
          Engine.InvApplication.HandleExceptionInvoke(Exception);

          ActiveFilePicker.CancelInvoke();
        });
      }
    }

    internal bool IsTransitioning
    {
      get { return IsTransitioningField; }
      set
      {
        this.IsTransitioningField = value;

        if (value)
          Window.SetFlags(Android.Views.WindowManagerFlags.NotTouchable, Android.Views.WindowManagerFlags.NotTouchable);
        else
          Window.ClearFlags(Android.Views.WindowManagerFlags.NotTouchable);
      }
    }
    internal DirectoryFilePicker ActiveFilePicker { get; set; }

    private Inv.AndroidEngine Engine;
    private Inv.DistinctList<Android.Views.View> ContentViewList;
    private Android.Media.AudioManager AudioManager;
    private bool LeftEdgeSwipe;
    private bool RightEdgeSwipe;
    private bool IsTransitioningField;
  }

  internal sealed class AndroidPlatform : Inv.Platform
  {
    public AndroidPlatform(AndroidEngine Engine)
    {
      this.Engine = Engine;
      this.PersonalPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
      this.DownloadsPath = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).AbsolutePath;
      this.CurrentProcess = System.Diagnostics.Process.GetCurrentProcess();
      this.Vault = new AndroidVault(Engine.AndroidActivity);
      this.Handler = new Android.OS.Handler(Android.OS.Looper.MainLooper);
    }

    int Platform.ThreadAffinity()
    {
      return System.Threading.Thread.CurrentThread.ManagedThreadId;
    }
    string Platform.CalendarTimeZoneName()
    {
      return TimeZoneInfo.Local.DisplayName;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      if (CalendarPicker.SetDate && CalendarPicker.SetTime)
      {
#if ANDROID_SUPPORT
        var Fragment = new AndroidDateTimePickerFragment();
        Fragment.SelectedDate = CalendarPicker.Value;
        Fragment.SelectEvent += () =>
        {
          Engine.Guard(() =>
          {
            CalendarPicker.Value = Fragment.SelectedDate;
            CalendarPicker.SelectInvoke();
          });
        };
        Fragment.CancelEvent += () =>
        {
          Engine.Guard(() => CalendarPicker.CancelInvoke());
        };
        Fragment.Show(Engine.AndroidActivity.FragmentManager, AndroidDateTimePickerFragment.TAG);
#else
        throw new NotImplementedException();
#endif
      }
      else if (CalendarPicker.SetDate)
      {
        var Fragment = new AndroidDatePickerFragment();
        Fragment.SelectedDate = CalendarPicker.Value;
        Fragment.SelectEvent += () =>
        {
          Engine.Guard(() =>
          {
            CalendarPicker.Value = Fragment.SelectedDate;
            CalendarPicker.SelectInvoke();
          });
        };
        Fragment.CancelEvent += () =>
        {
          Engine.Guard(() => CalendarPicker.CancelInvoke());
        };
        Fragment.Show(Engine.AndroidActivity.FragmentManager, AndroidDatePickerFragment.TAG);
      }
      else if (CalendarPicker.SetTime)
      {
        var Fragment = new AndroidTimePickerFragment();
        Fragment.SelectedTime = CalendarPicker.Value;
        Fragment.SelectEvent += () =>
        {
          Engine.Guard(() =>
          {
            CalendarPicker.Value = Fragment.SelectedTime;
            CalendarPicker.SelectInvoke();
          });
        };
        Fragment.CancelEvent += () =>
        {
          Engine.Guard(() => CalendarPicker.CancelInvoke());
        };
        Fragment.Show(Engine.AndroidActivity.FragmentManager, AndroidTimePickerFragment.TAG);
      }
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      var EmailIntent = new Android.Content.Intent(Android.Content.Intent.ActionSendMultiple);
      //EmailIntent.SetType("text/plain");
      EmailIntent.SetType("message/rfc822");
      EmailIntent.PutExtra(Android.Content.Intent.ExtraEmail, EmailMessage.GetTos().Select(T => T.Address).ToArray());
      EmailIntent.PutExtra(Android.Content.Intent.ExtraSubject, EmailMessage.Subject);
      EmailIntent.PutExtra(Android.Content.Intent.ExtraText, EmailMessage.Body);

      var AttachmentUris = new List<Android.OS.IParcelable>();

      // NOTE: we have to copy all attachment files from the internal storage to external storage so the email app can actually access the file.
      foreach (var Attachment in EmailMessage.GetAttachments())
      {
        var AttachmentFile = System.IO.Path.Combine(DownloadsPath, Attachment.Name);
        System.IO.File.Copy(SelectFilePath(Attachment.File), AttachmentFile, true);

        var JavaFile = new Java.IO.File(AttachmentFile);
        AttachmentUris.Add(Android.Net.Uri.FromFile(JavaFile));
      }

      if (AttachmentUris.Count > 0)
        EmailIntent.PutParcelableArrayListExtra(Android.Content.Intent.ExtraStream, AttachmentUris);

      if (!IsIntentSupported(EmailIntent))
        return false;

      Engine.AndroidActivity.StartActivity(EmailIntent);
      return true;
    }
    bool Platform.PhoneIsSupported
    {
      get { return IsIntentSupported(new Android.Content.Intent(Android.Content.Intent.ActionDial)); }
    }
    void Platform.PhoneDial(string PhoneNumber)
    {
      var DialIntent = new Android.Content.Intent(Android.Content.Intent.ActionDial, Android.Net.Uri.Parse("tel:" + PhoneNumber));
      if (!IsIntentSupported(DialIntent))
        Android.Widget.Toast.MakeText(Engine.AndroidActivity, "Unable to dial on this device", Android.Widget.ToastLength.Short).Show();
      else
        Engine.AndroidActivity.StartActivity(DialIntent);
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
      var SmsIntent = new Android.Content.Intent(Android.Content.Intent.ActionSendto, Android.Net.Uri.Parse("smsto:" + PhoneNumber));
      if (!IsIntentSupported(SmsIntent))
        Android.Widget.Toast.MakeText(Engine.AndroidActivity, "Unable to SMS on this device", Android.Widget.ToastLength.Short).Show();
      else
        Engine.AndroidActivity.StartActivity(SmsIntent);
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).LastWriteTimeUtc;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      System.IO.File.SetLastWriteTimeUtc(SelectFilePath(File), Timestamp);
    }
    System.IO.Stream Platform.DirectoryCreateFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.Read, 65536);
    }
    System.IO.Stream Platform.DirectoryAppendFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), System.IO.FileMode.Append, System.IO.FileAccess.Write, System.IO.FileShare.Read, 65536);
    }
    System.IO.Stream Platform.DirectoryOpenFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read, 65536);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return System.IO.File.Exists(SelectFilePath(File));
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      System.IO.File.Delete(SelectFilePath(File));
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Copy(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Move(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return new System.IO.DirectoryInfo(SelectFolderPath(Folder)).GetFiles(FileMask).Select(F => Folder.NewFile(F.Name));
    }
    System.IO.Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return new System.IO.BufferedStream(Engine.AndroidActivity.Assets.Open(Asset.Name));
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      return Engine.AndroidActivity.Assets.List("").Contains(Asset.Name);
    }
    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      var PickTitle = FilePicker.Title ?? "Select " + FilePicker.Type.ToString().ToUpper() + " File";

      var PickIntent = new Android.Content.Intent(Android.Content.Intent.ActionGetContent);

      switch (FilePicker.Type)
      {
        case FileType.Image:
          PickIntent.SetType("image/*");
          break;

        case FileType.Any:
          PickIntent.SetType("*/*");
          break;

        default:
          throw new Exception("FileType not handled: " + FilePicker.Type);
      }

      Engine.AndroidActivity.ActiveFilePicker = FilePicker;
      Engine.AndroidActivity.StartActivityForResult(Android.Content.Intent.CreateChooser(PickIntent, PickTitle), AndroidActivity.PickFileId);
    }
    bool Platform.LocationIsSupported
    {
      get { return Engine.AndroidLocationProvider != null; }
    }
    void Platform.LocationLookup(LocationLookup LocationLookup)
    {
      var AndroidGeocoder = new Android.Locations.Geocoder(Engine.AndroidActivity);
      AndroidGeocoder.GetFromLocationAsync(LocationLookup.Coordinate.Latitude, LocationLookup.Coordinate.Longitude, 10).ContinueWith(Task =>
      {
        Debug.WriteLine(Task.Result.ToArray().Select(R => R.ToString()).AsSeparatedText(Environment.NewLine));

        LocationLookup.SetPlacemarks(Task.Result.Select(P => new Inv.Placemark
        (
          Name: P.Premises ?? P.GetAddressLine(0),
          Locality: P.Locality,
          SubLocality: P.SubLocality,
          PostalCode: P.PostalCode,
          AdministrativeArea: P.AdminArea,
          SubAdministrativeArea: P.SubAdminArea,
          CountryName: P.CountryName,
          CountryCode : P.CountryCode,
          Latitude: P.HasLatitude ? P.Latitude : LocationLookup.Coordinate.Latitude,
          Longitude: P.HasLongitude ? P.Longitude : LocationLookup.Coordinate.Longitude
        )));
      });
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume, float Rate)
    {
      Engine.PlaySound(Sound, Volume, Rate, false, PersonalPath);
    }
    void Platform.AudioPlayClip(AudioClip Clip)
    {
      Clip.Node = Engine.PlaySound(Clip.Sound, Clip.Volume, Clip.Rate, Clip.Loop, PersonalPath);
    }
    void Platform.AudioStopClip(AudioClip Clip)
    {
      var AndroidStreamId = Clip.Node as int?;
      if (AndroidStreamId != null)
      {
        Clip.Node = null;
        Engine.SoundPool.Stop(AndroidStreamId.Value);
      }
    }
    void Platform.WindowBrowse(Inv.File File)
    {
      // NOTE: can only browse files that are in ExternalStorage.

      var FilePath = SelectFilePath(File);

      var BrowseFile = System.IO.Path.Combine(DownloadsPath, File.Name);
      System.IO.File.Copy(SelectFilePath(File), BrowseFile, true);

      var JavaFile = new Java.IO.File(BrowseFile);
      JavaFile.SetReadable(true);

      var BrowseIntent = new Android.Content.Intent(Android.Content.Intent.ActionView);
      BrowseIntent.SetDataAndType(Android.Net.Uri.FromFile(JavaFile), "application/" + File.Extension.Trim('.'));
      //BrowseIntent.SetFlags(Android.Content.ActivityFlags.ClearTop);
      //BrowseIntent.SetFlags(Android.Content.ActivityFlags.GrantReadUriPermission);
      //BrowseIntent.SetFlags(Android.Content.ActivityFlags.NewTask);
      //BrowseIntent.SetFlags(Android.Content.ActivityFlags.ClearWhenTaskReset); 

      var AppList = Engine.AndroidActivity.PackageManager.QueryIntentActivities(BrowseIntent, Android.Content.PM.PackageInfoFlags.MatchDefaultOnly);

      if (AppList.Count == 0)
        Android.Widget.Toast.MakeText(Engine.AndroidActivity, "There is no installed application to view " + File.Name, Android.Widget.ToastLength.Short).Show();
      else
        Engine.AndroidActivity.StartActivity(BrowseIntent);
    }
    void Platform.WindowPost(Action Action)
    {
      // NOTE: don't use RunOnUiThread because we always want to post to the end of the message queue.
      Handler.Post(Action);
    }
    void Platform.WindowCall(Action Action)
    {
      // TODO: Android.OS.Looper.MainLooper.IsCurrentThread is API Level 23.
      var Runnable = new Java.Lang.Runnable(Action);
      Engine.AndroidActivity.RunOnUiThread(Runnable); // NOTE: this will run immediately if we already on the UIThread (as desired).
      Runnable.Wait();
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      // NOTE: CurrentProcess.Refresh() doesn't seem to be required here on Android.
      return CurrentProcess.PrivateMemorySize64;
    }
    void Platform.ProcessMemoryReclamation()
    {
      Engine.Reclamation();
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      var TcpClient = new Inv.Tcp.Client(WebClient.Host, WebClient.Port, WebClient.CertHash);
      TcpClient.Connect();

      WebClient.Node = TcpClient;
      WebClient.SetStreams(TcpClient.Stream, TcpClient.Stream);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
      var TcpClient = (Inv.Tcp.Client)WebClient.Node;
      if (TcpClient != null)
      {
        WebClient.Node = null;
        WebClient.SetStreams(null, null);

        TcpClient.Disconnect();
      }
    }
    void Platform.WebServerConnect(WebServer WebServer)
    {
      var TcpServer = new Inv.Tcp.Server(WebServer.Host, WebServer.Port, WebServer.CertHash);
      TcpServer.AcceptEvent += (TcpChannel) => WebServer.AcceptChannel(TcpChannel, TcpChannel.Stream, TcpChannel.Stream);
      TcpServer.RejectEvent += (TcpChannel) => WebServer.RejectChannel(TcpChannel);

      WebServer.Node = TcpServer;
      WebServer.DropDelegate = (Node) => ((Inv.Tcp.Channel)Node).Drop();

      TcpServer.Connect();
    }
    void Platform.WebServerDisconnect(WebServer WebServer)
    {
      var TcpServer = (Inv.Tcp.Server)WebServer.Node;
      if (TcpServer != null)
      {
        TcpServer.Disconnect();

        WebServer.Node = null;
        WebServer.DropDelegate = null;
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      var BrowserIntent = new Android.Content.Intent(Android.Content.Intent.ActionView, Android.Net.Uri.Parse(Uri.AbsoluteUri));
      Engine.AndroidActivity.StartActivity(BrowserIntent);
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      var MarketIntent = new Android.Content.Intent(Android.Content.Intent.ActionView, Android.Net.Uri.Parse("market://details?id=" + GooglePlayID));
      MarketIntent.AddFlags(Android.Content.ActivityFlags.NoHistory | Android.Content.ActivityFlags.ClearWhenTaskReset | Android.Content.ActivityFlags.MultipleTask | Android.Content.ActivityFlags.NewTask);
      Engine.AndroidActivity.StartActivity(MarketIntent);
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      Vault.Load(Secret);
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      Vault.Save(Secret);
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      Vault.Delete(Secret);
    }

    private bool IsIntentSupported(Android.Content.Intent AndroidIntent)
    {
      var AvailableActivities = Engine.AndroidActivity.PackageManager.QueryIntentActivities(AndroidIntent, Android.Content.PM.PackageInfoFlags.MatchDefaultOnly);

      return AvailableActivities != null && AvailableActivities.Count > 0;
    }
    private string SelectFilePath(File File)
    {
      return System.IO.Path.Combine(SelectFolderPath(File.Folder), File.Name);
    }
    private string SelectFolderPath(Folder Folder)
    {
      string Result;

      if (Folder.Name != null)
        Result = System.IO.Path.Combine(PersonalPath, Folder.Name);
      else
        Result = PersonalPath;

      System.IO.Directory.CreateDirectory(Result);

      return Result;
    }

    private AndroidEngine Engine;
    private AndroidVault Vault;
    private string PersonalPath;
    private string DownloadsPath;
    private System.Diagnostics.Process CurrentProcess;
    private Android.OS.Handler Handler;
  }

  internal sealed class AndroidFrameCallback : Android.Views.View, Android.Views.Choreographer.IFrameCallback
  {
    public AndroidFrameCallback(Android.Content.Context Context, Action Action)
      : base(Context)
    {
      this.Action = Action;
    }

    void Android.Views.Choreographer.IFrameCallback.DoFrame(long frameTimeNanos)
    {
      Action();
    }

    private Action Action;
  }

  internal sealed class AndroidEngine 
  {
    public AndroidEngine(Inv.Application InvApplication, AndroidActivity AndroidActivity)
    {
      this.InvApplication = InvApplication;
      this.AndroidActivity = AndroidActivity;
      this.ImageList = new Inv.DistinctList<Inv.Image>(16384); // 64KB memory.

      this.FillPaint = new Android.Graphics.Paint()
      {
        AntiAlias = false
      };
      FillPaint.SetStyle(Android.Graphics.Paint.Style.Fill);

      this.FillAndStrokePaint = new Android.Graphics.Paint()
      {
        AntiAlias = false
      };
      FillAndStrokePaint.SetStyle(Android.Graphics.Paint.Style.FillAndStroke);

      this.StrokePaint = new Android.Graphics.Paint()
      {
        AntiAlias = false,
        StrokeCap = Android.Graphics.Paint.Cap.Round
      };
      StrokePaint.SetStyle(Android.Graphics.Paint.Style.Stroke);

      this.ImagePaint = new Android.Graphics.Paint()
      {
        AntiAlias = true
      };

      this.TextPaint = new Android.Graphics.Paint()
      {
        AntiAlias = true
      };

      this.SoundPool = new Android.Media.SoundPool(20, Android.Media.Stream.Music, 0);
      this.SoundList = new Inv.DistinctList<Inv.Sound>(1024);

      this.RouteArray = new Inv.EnumArray<Inv.PanelType, Func<Inv.Panel, AndroidNode>>()
      {
        { Inv.PanelType.Browser, TranslateBrowser },
        { Inv.PanelType.Button, TranslateButton },
        { Inv.PanelType.Board, TranslateBoard },
        { Inv.PanelType.Dock, TranslateDock },
        { Inv.PanelType.Edit, TranslateEdit },
        { Inv.PanelType.Graphic, TranslateGraphic },
        { Inv.PanelType.Label, TranslateLabel },
        { Inv.PanelType.Memo, TranslateMemo },
        { Inv.PanelType.Overlay, TranslateOverlay },
        { Inv.PanelType.Canvas, TranslateCanvas },
        { Inv.PanelType.Scroll, TranslateScroll },
        { Inv.PanelType.Frame, TranslateFrame },
        { Inv.PanelType.Stack, TranslateStack },
        { Inv.PanelType.Table, TranslateTable },
        { Inv.PanelType.Flow, TranslateFlow },
      };

      #region Key mapping.
      this.KeyDictionary = new Dictionary<Android.Views.Keycode, Inv.Key>()
      {
        { Android.Views.Keycode.Num0, Key.n0 },
        { Android.Views.Keycode.Num1, Key.n1 },
        { Android.Views.Keycode.Num2, Key.n2 },
        { Android.Views.Keycode.Num3, Key.n3 },
        { Android.Views.Keycode.Num4, Key.n4 },
        { Android.Views.Keycode.Num5, Key.n5 },
        { Android.Views.Keycode.Num6, Key.n6 },
        { Android.Views.Keycode.Num7, Key.n7 },
        { Android.Views.Keycode.Num8, Key.n8 },
        { Android.Views.Keycode.Num9, Key.n9 },

        { Android.Views.Keycode.A, Key.A },
        { Android.Views.Keycode.B, Key.B },
        { Android.Views.Keycode.C, Key.C },
        { Android.Views.Keycode.D, Key.D },
        { Android.Views.Keycode.E, Key.E },
        { Android.Views.Keycode.F, Key.F },
        { Android.Views.Keycode.G, Key.G },
        { Android.Views.Keycode.H, Key.H },
        { Android.Views.Keycode.I, Key.I },
        { Android.Views.Keycode.J, Key.J },
        { Android.Views.Keycode.K, Key.K },
        { Android.Views.Keycode.L, Key.L },
        { Android.Views.Keycode.M, Key.M },
        { Android.Views.Keycode.N, Key.N },
        { Android.Views.Keycode.O, Key.O },
        { Android.Views.Keycode.P, Key.P },
        { Android.Views.Keycode.Q, Key.Q },
        { Android.Views.Keycode.R, Key.R },
        { Android.Views.Keycode.S, Key.S },
        { Android.Views.Keycode.T, Key.T },
        { Android.Views.Keycode.U, Key.U },
        { Android.Views.Keycode.V, Key.V },
        { Android.Views.Keycode.W, Key.W },
        { Android.Views.Keycode.X, Key.X },
        { Android.Views.Keycode.Y, Key.Y },
        { Android.Views.Keycode.Z, Key.Z },
        { Android.Views.Keycode.F1, Key.F1 },
        { Android.Views.Keycode.F2, Key.F2 },
        { Android.Views.Keycode.F3, Key.F3 },
        { Android.Views.Keycode.F4, Key.F4 },
        { Android.Views.Keycode.F5, Key.F5 },
        { Android.Views.Keycode.F6, Key.F6 },
        { Android.Views.Keycode.F7, Key.F7 },
        { Android.Views.Keycode.F8, Key.F8 },
        { Android.Views.Keycode.F9, Key.F9 },
        { Android.Views.Keycode.F10, Key.F10 },
        { Android.Views.Keycode.F11, Key.F11 },
        { Android.Views.Keycode.F12, Key.F12 },
        { Android.Views.Keycode.Escape, Key.Escape },
        { Android.Views.Keycode.Enter, Key.Enter },
        { Android.Views.Keycode.Tab, Key.Tab },
        { Android.Views.Keycode.Space, Key.Space },
        { Android.Views.Keycode.Period, Key.Period },
        { Android.Views.Keycode.Comma, Key.Comma },
        { Android.Views.Keycode.Grave, Key.BackQuote },
        { Android.Views.Keycode.DpadUp, Key.Up },
        { Android.Views.Keycode.DpadDown, Key.Down },
        { Android.Views.Keycode.DpadLeft, Key.Left },
        { Android.Views.Keycode.DpadRight, Key.Right },
        { Android.Views.Keycode.MoveHome, Key.Home },
        { Android.Views.Keycode.MoveEnd, Key.End },
        { Android.Views.Keycode.PageUp, Key.PageUp },
        { Android.Views.Keycode.PageDown, Key.PageDown },
        { Android.Views.Keycode.Clear, Key.Clear },
        { Android.Views.Keycode.Insert, Key.Insert },
        { Android.Views.Keycode.Del, Key.Delete },
        { Android.Views.Keycode.Backslash, Key.Backslash },
        { Android.Views.Keycode.Slash, Key.Slash },
        { Android.Views.Keycode.Back, Key.Backspace },
        { Android.Views.Keycode.ShiftLeft, Key.LeftShift },
        { Android.Views.Keycode.ShiftRight, Key.RightShift },
        { Android.Views.Keycode.CtrlLeft, Key.LeftCtrl },
        { Android.Views.Keycode.CtrlRight, Key.RightCtrl },
        { Android.Views.Keycode.AltLeft, Key.LeftAlt },
        { Android.Views.Keycode.AltRight, Key.RightAlt },
      };
      #endregion

      InvApplication.Directory.Installation = AndroidActivity.ApplicationContext.PackageName;
      InvApplication.SetPlatform(new AndroidPlatform(this));

      InvApplication.Device.Name = Environment.MachineName;
      InvApplication.Device.Model = GetDeviceModel();
      InvApplication.Device.System = "Android " + Android.OS.Build.VERSION.Release + " (" + Android.OS.Build.VERSION.Sdk + ")";
      InvApplication.Device.Keyboard = IsHardwareKeyboardAvailable();
      InvApplication.Device.Mouse = false; // TODO: is this a thing?  you can use mouse in the emulator.
      InvApplication.Device.Touch = true;
      InvApplication.Device.ProportionalFontName = "sans-serif";
      InvApplication.Device.MonospacedFontName = "monospace";

      InvApplication.Process.Id = Android.OS.Process.MyPid(); 

      this.Choreographer = Android.Views.Choreographer.Instance;
      this.FrameCallback = new AndroidFrameCallback(AndroidActivity, Process);

      this.AndroidSdkVersion = (int)Android.OS.Build.VERSION.SdkInt;
#if DEBUG
      //this.AndroidSdkVersion = 19; // for compatibility testing.
#endif

      this.AndroidSurfaceLayoutParams = new Android.Widget.FrameLayout.LayoutParams(Android.Widget.FrameLayout.LayoutParams.MatchParent, Android.Widget.FrameLayout.LayoutParams.MatchParent);

      this.FontFamilyArray = new Inv.EnumArray<FontWeight, string>()
      {
        { FontWeight.Thin, "-thin" },
        { FontWeight.Light, "-light" },
        { FontWeight.Regular, "" },
        { FontWeight.Medium, "-medium" },
        { FontWeight.Bold, "" }, // regular + bold style.
        { FontWeight.Heavy, "-black" },
      };

      this.AndroidFontArray = new Inv.EnumArray<FontWeight, Android.Graphics.Typeface>();
      AndroidFontArray.Fill(W => TranslateGraphicsTypeface("sans-serif", W));
      this.InputManager = (Android.Views.InputMethods.InputMethodManager)AndroidActivity.GetSystemService(Android.Content.Context.InputMethodService);

      Resize();
    }

    public Inv.Application InvApplication { get; private set; }

    public void Start()
    {
      try
      {
        InvApplication.StartInvoke();

        AndroidActivity.Title = InvApplication.Title;
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception);

        // NOTE: can't let the application start if the StartEvent failed (it will hang on a black screen) so have the application crash instead.
        throw Exception;
      }

      Process();
    }
    public void Stop()
    {
      Guard(() => 
      {
        InvApplication.StopInvoke();
      });
    }
    public void Resume()
    {
      Immersive();

      if (IsProcessPaused)
      {
        this.IsProcessPaused = false;

        Guard(() =>
        {
          if (InvApplication.Location.IsRequired)
          {
            this.AndroidLocationManager = AndroidActivity.GetSystemService(Android.Content.Context.LocationService) as Android.Locations.LocationManager;
            
            var LocationCriteria = new Android.Locations.Criteria()
            {
              Accuracy = Android.Locations.Accuracy.Fine,
              PowerRequirement = Android.Locations.Power.NoRequirement
            };

            AndroidLocationProvider = AndroidLocationManager != null ? AndroidLocationManager.GetBestProvider(LocationCriteria, true) : null;

            if (AndroidLocationProvider != null)
            {
              // 2000L = the minimum time between updates (in seconds), 
              // 1.0F = the minimum distance the user needs to move to generate an update (in meters),

              AndroidLocationManager.RequestLocationUpdates(AndroidLocationProvider, 2000L, 1.0F, AndroidActivity);
            }
          }
        });

        Guard(() =>
        {
          InvApplication.ResumeInvoke();
        });

        if (IsProcessSuspended)
        {
          // if the last callback wasn't actually posted, lets start now.
          IsProcessSuspended = false;
          Process();
        }
      }
    }
    public void Pause()
    {
      if (!IsProcessPaused)
      {
        this.IsProcessPaused = true;

        Guard(() =>
        {
          InvApplication.SuspendInvoke();
        });

        Guard(() =>
        {
          if (AndroidLocationManager != null)
          {
            AndroidLocationManager.RemoveUpdates(AndroidActivity);
            AndroidLocationManager = null;
          }
        });
      }
    }

    internal AndroidActivity AndroidActivity { get; private set; }
    internal string AndroidLocationProvider { get; set; }
    internal Android.Util.DisplayMetrics DisplayMetrics { get; private set; }
    internal Android.Media.SoundPool SoundPool { get; private set; }

    internal Inv.Key? TranslateKey(Android.Views.Keycode AndroidKey)
    {
      Inv.Key Result;
      if (KeyDictionary.TryGetValue(AndroidKey, out Result))
        return Result;
      else
        return null;
    }
    internal int PlaySound(Inv.Sound InvSound, float Volume, float Rate, bool Loop, string PersonalPath)
    {
      if (InvSound == null)
        return 0;

      // NOTE: android will inconsistently play a sound with a volume greater than 1.0F.
      //       this requirement is asserted in the Inv API, but this is code is to ensure sound is played in RELEASE, despite a mistake with the volume.
      if (Volume > 1.0F)
        Volume = 1.0F;
      else if (Volume < 0.0F)
        Volume = 0.0F;

      // NOTE: Android docs say that 0.5 to 2.0 is the support range for playback rate.
      if (Rate > 2.0F)
        Rate = 2.0F;
      else if (Rate < 0.5F)
        Rate = 0.5F;

      var AndroidSoundId = InvSound.Node as int?;

      if (AndroidSoundId == null)
      {
        var Buffer = InvSound.GetBuffer();

        var FilePath = System.IO.Path.Combine(PersonalPath, SoundList.Count + InvSound.GetFormat());

        using (var FileStream = new System.IO.FileStream(FilePath, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.Read, 65536))
          FileStream.Write(Buffer, 0, Buffer.Length);

        AndroidSoundId = SoundPool.Load(FilePath, 1);

        // just in case it is already in the list with a non-android sound id.
        if (InvSound.Node == null || !SoundList.Contains(InvSound))
          SoundList.Add(InvSound);

        InvSound.Node = AndroidSoundId;
      }

      // NOTE: this is required because the first time a sound is loaded, it doesn't work straight away.
      int StreamID;
      var Retry = 0;
      do
      {
        StreamID = SoundPool.Play(AndroidSoundId.Value, Volume, Volume, 1, Loop ? -1 : 0, Rate);
        if (StreamID == 0)
          System.Threading.Thread.Sleep(10);
      }
      while (StreamID == 0 && Retry++ < 100);

      if (StreamID == 0)
        Debug.WriteLine("Play sound failed");

      return StreamID;
    }
    internal void Resize()
    {
      var Display = AndroidActivity.WindowManager.DefaultDisplay;

      this.DisplayMetrics = new Android.Util.DisplayMetrics();
      Display.GetRealMetrics(DisplayMetrics);
      this.ConvertPtToPxFactor = DisplayMetrics.Density;

      InvApplication.Window.Width = (int)(DisplayMetrics.WidthPixels / ConvertPtToPxFactor);
      InvApplication.Window.Height = (int)(DisplayMetrics.HeightPixels / ConvertPtToPxFactor);
    }
    internal void Immersive()
    {
      // NOTE: the typecast a deliberate workaround, we need to use a different enum and cast it to get the desired behaviour.
      // NOTE: needs to happen 'OnResume' so it re-applies when your return from another activity.
      var UiFlags = Android.Views.SystemUiFlags.Fullscreen | Android.Views.SystemUiFlags.HideNavigation | Android.Views.SystemUiFlags.ImmersiveSticky;// | Android.Views.SystemUiFlags.LayoutStable | Android.Views.SystemUiFlags.LayoutHideNavigation | Android.Views.SystemUiFlags.HideNavigation;
      AndroidActivity.Window.DecorView.SystemUiVisibility = (Android.Views.StatusBarVisibility)UiFlags;
    }
    internal void Reclamation()
    {
      // dispose bitmaps.
      foreach (var Image in ImageList)
      {
        var Bitmap = Image.Node as Android.Graphics.Bitmap;

        if (Bitmap != null)
          Bitmap.Dispose();

        Image.Node = null;
      }
      ImageList.Clear();

      // unload sounds.
      /*
      foreach (var Sound in SoundList)
      {
        var SoundId = (int?)Sound.Node;

        if (SoundId != null)
          SoundPool.Unload(SoundId.Value);

        Sound.Node = null;
      }
      SoundList.Clear();
      */
    }
    internal void Guard(Action Action)
    {
      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception);
      }
    }
    internal Android.Graphics.Bitmap TranslateGraphicsBitmap(Inv.Image Image)
    {
      if (Image == null)
      {
        return null;
      }
      else 
      {
        var Result = Image.Node as Android.Graphics.Bitmap;

        if (Result == null)
        {
          var ImageBuffer = Image.GetBuffer();

          Result = Android.Graphics.BitmapFactory.DecodeByteArray(ImageBuffer, 0, ImageBuffer.Length);

          if (Image.Node == null || !ImageList.Contains(Image))
            ImageList.Add(Image);

          Image.Node = Result;
        }

        return Result;
      }
    }
    internal Android.Graphics.Paint TranslateGraphicsFillAndStrokePaint(Colour InvColour, int StrokePixels)
    {
      if (InvColour == null)
        return null;

      FillAndStrokePaint.Color = TranslateGraphicsColor(InvColour);
      FillAndStrokePaint.StrokeWidth = StrokePixels;

      return FillAndStrokePaint;
    }
    internal Android.Graphics.Paint TranslateGraphicsFillPaint(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return null;

      FillPaint.Color = TranslateGraphicsColor(InvColour);

      return FillPaint;
    }
    internal Android.Graphics.Paint TranslateGraphicsStrokePaint(Inv.Colour InvColour, int StrokePixels)
    {
      if (InvColour == null || StrokePixels <= 0)
        return null;

      StrokePaint.Color = TranslateGraphicsColor(InvColour);
      StrokePaint.StrokeWidth = StrokePixels;

      return StrokePaint;
    }
    internal Android.Graphics.Paint TranslateGraphicsImagePaint(float Opacity, Inv.Colour Tint)
    {
      if (Opacity == 1.0F && Tint == null)
        return null;

      if (Opacity == 1.0F)
        ImagePaint.Alpha = 255;
      else
        ImagePaint.Alpha = (int)(Opacity * 255);

      if (Tint == null)
        ImagePaint.SetColorFilter(null);
      else
        ImagePaint.SetColorFilter(TranslateGraphicsColorFilter(Tint));

      return ImagePaint;
    }
    internal Android.Graphics.Paint TranslateGraphicsTextPaint(string FontName, int FontSizePixels, Inv.FontWeight FontWeight, Inv.Colour FontColour)
    {
      // NOTE: a null textpaint causes the downstream code to fail.
      //if (FontColour == null || FontSize == 0)
      //  return null;

      TextPaint.Color = TranslateGraphicsColor(FontColour);
      TextPaint.TextSize = FontSizePixels;
      TextPaint.SetTypeface(TranslateGraphicsTypeface(FontName, FontWeight));

      return TextPaint;
    }
    internal int PtToPx(int Points)
    {
      return (int)(Points * ConvertPtToPxFactor + 0.5F);
    }
    internal int PxToPt(int Pixels)
    {
      return (int)(Pixels / ConvertPtToPxFactor + 0.5F);
    }

    private void Process()
    {
      if (InvApplication.IsExit)
      {
        AndroidActivity.Finish();
        // NOTE: this won't actually end the application, that's the job of the Android OS (apparently).

        // TODO: there doesn't appear to be a way to actually terminate the process in code.
        //Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        //System.Environment.Exit(0);
      }
      else
      {
        Guard(() =>
        {
          var InvWindow = InvApplication.Window;

          InvWindow.ProcessInvoke();

          if (InvWindow.ActiveTimerSet.Count > 0)
          {
            foreach (var InvTimer in InvWindow.ActiveTimerSet)
            {
              var AndroidTimer = AccessTimer(InvTimer, S =>
              {
                var Result = new System.Timers.Timer();
                Result.Elapsed += (Sender, Event) => InvWindow.Application.Platform.WindowPost(() => Guard(() => InvTimer.IntervalInvoke()));
                return Result;
              });

              if (InvTimer.IsRestarting)
              {
                InvTimer.IsRestarting = false;
                AndroidTimer.Stop();
              }

              if (AndroidTimer.Interval != InvTimer.IntervalTime.TotalMilliseconds)
                AndroidTimer.Interval = InvTimer.IntervalTime.TotalMilliseconds;

              if (InvTimer.IsEnabled && !AndroidTimer.Enabled)
                AndroidTimer.Start();
              else if (!InvTimer.IsEnabled && AndroidTimer.Enabled)
                AndroidTimer.Stop();
            }

            InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
          }

          var InvSurface = InvWindow.ActiveSurface;

          if (InvSurface != null)
          {
            var AndroidSurface = AccessSurface(InvSurface, S =>
            {
              var Result = new AndroidLayoutSurface(AndroidActivity);
              return Result;
            });

            if (!AndroidActivity.HasContentView(AndroidSurface) && InvWindow.ActiveTransition != null)
              InvSurface.ArrangeInvoke();

            ProcessTransition(AndroidSurface);

            InvSurface.ComposeInvoke();

            UpdateSurface(InvSurface, AndroidSurface);

            if (InvSurface.Focus != null)
            {
              var AndroidFocus = InvSurface.Focus.Node as AndroidNode;

              InvSurface.Focus = null;

              if (AndroidFocus != null && AndroidFocus.Element.RequestFocus())
                AndroidFocus.Element.PostDelayed(() => InputManager.ShowSoftInput(AndroidFocus.Element, Android.Views.InputMethods.ShowFlags.Implicit), 0);
            }
          }

          if (InvWindow.Render())
            AndroidActivity.Window.DecorView.SetBackgroundColor(TranslateGraphicsColor(InvWindow.Background.Colour ?? Inv.Colour.Black));

          if (InvSurface != null)
            ProcessAnimation(InvSurface);

          InvWindow.DisplayRate.Calculate();
        });

        if (IsProcessPaused)
          IsProcessSuspended = true;
        else
          Choreographer.PostFrameCallback(FrameCallback);
      }
    }
    private void UpdateSurface(Surface InvSurface, AndroidLayoutSurface AndroidSurface)
    {
      if (InvSurface.Render())
      {
        AndroidSurface.SetBackgroundColor(TranslateGraphicsColor(InvSurface.Background.Colour));

        var AndroidPanel = TranslatePanel(InvSurface.Content);
        AndroidSurface.SetContentElement(AndroidPanel);
      }

      InvSurface.ProcessChanges(P => TranslatePanel(P));
    }
    private void ProcessTransition(AndroidLayoutSurface AndroidSurface)
    {
      var InvWindow = InvApplication.Window;
      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
      }
      else if (!AndroidActivity.IsTransitioning)
      {
        var AndroidContent = AndroidActivity.GetContentView();

        if (AndroidSurface == AndroidContent)
        {
          Debug.WriteLine("Transition to self");
        }
        else
        {
          // give the previous surface a chance to process before it is animated away.
          if (InvTransition.FromSurface != null && AndroidContent != null)
            UpdateSurface(InvTransition.FromSurface, (AndroidLayoutSurface)AndroidContent);

          var AnimateDuration = (long)InvTransition.Duration.TotalMilliseconds;

          switch (InvTransition.Animation)
          {
            case TransitionAnimation.None:
              if (AndroidContent != null)
                AndroidActivity.RemoveContentView(AndroidContent);

              AndroidActivity.AddContentView(AndroidSurface, AndroidSurfaceLayoutParams);
              break;

            case TransitionAnimation.Fade:
              AndroidActivity.IsTransitioning = true;

              if (AndroidContent != null)
              {
                var HalfTime = AnimateDuration / 2;

                var FadeOut = new Android.Views.Animations.AlphaAnimation(1, 0);
                FadeOut.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
                FadeOut.Duration = HalfTime;
                FadeOut.AnimationEnd += (Sender, Event) =>
                {
                  AndroidActivity.RemoveContentView(AndroidContent);
                  AndroidContent.Alpha = 1.0F;
                  AndroidActivity.AddContentView(AndroidSurface, AndroidSurfaceLayoutParams);

                  var FadeIn = new Android.Views.Animations.AlphaAnimation(0, 1);
                  FadeIn.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
                  FadeIn.Duration = HalfTime;
                  FadeIn.AnimationEnd += (S, E) => AndroidActivity.IsTransitioning = false;
                  AndroidSurface.StartAnimation(FadeIn);
                };
                AndroidContent.StartAnimation(FadeOut);
              }
              else
              {
                AndroidActivity.AddContentView(AndroidSurface, AndroidSurfaceLayoutParams);

                var FadeIn = new Android.Views.Animations.AlphaAnimation(0, 1);
                FadeIn.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
                FadeIn.Duration = AnimateDuration;
                FadeIn.AnimationEnd += (Sender, Event) => AndroidActivity.IsTransitioning = false;
                AndroidSurface.StartAnimation(FadeIn);
              }
              break;

            case TransitionAnimation.CarouselNext:
              AndroidActivity.IsTransitioning = true;

              if (AndroidContent != null)
              {
                var NextSlideOut = new Android.Views.Animations.TranslateAnimation(0.0F, -DisplayMetrics.WidthPixels, 0.0F, 0.0F);
                NextSlideOut.Duration = AnimateDuration;
                NextSlideOut.AnimationEnd += (Sender, Event) => AndroidActivity.RemoveContentView(AndroidContent);
                AndroidContent.StartAnimation(NextSlideOut);
              }

              AndroidActivity.AddContentView(AndroidSurface, AndroidSurfaceLayoutParams);

              var NextSlideIn = new Android.Views.Animations.TranslateAnimation(DisplayMetrics.WidthPixels, 0.00F, 0.0F, 0.0F);
              NextSlideIn.Duration = AnimateDuration;
              NextSlideIn.AnimationEnd += (Sender, Event) => AndroidActivity.IsTransitioning = false;
              AndroidSurface.StartAnimation(NextSlideIn);
              break;

            case TransitionAnimation.CarouselBack:
              AndroidActivity.IsTransitioning = true;

              if (AndroidContent != null)
              {
                var BackSlideOut = new Android.Views.Animations.TranslateAnimation(0.00F, DisplayMetrics.WidthPixels, 0.0F, 0.0F);
                BackSlideOut.Duration = AnimateDuration;
                BackSlideOut.AnimationEnd += (Sender, Event) => AndroidActivity.RemoveContentView(AndroidContent);
                AndroidContent.StartAnimation(BackSlideOut);
              }

              AndroidActivity.AddContentView(AndroidSurface, AndroidSurfaceLayoutParams);

              var BackSlideIn = new Android.Views.Animations.TranslateAnimation(-DisplayMetrics.WidthPixels, 0.00F, 0.0F, 0.0F);
              BackSlideIn.Duration = AnimateDuration;
              BackSlideIn.AnimationEnd += (Sender, Event) => AndroidActivity.IsTransitioning = false;
              AndroidSurface.StartAnimation(BackSlideIn);
              break;

            default:
              throw new Exception("TransitionAnimation not handled: " + InvTransition.Animation);
          }
        }

        InvWindow.ActiveTransition = null;
      }
    }
    private void ProcessAnimation(Inv.Surface InvSurfaceActive)
    {
      if (InvSurfaceActive.StopAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StopAnimationSet)
        {
          if (InvAnimation.Node != null)
          {
            var AndroidAnimationGroupList = (Inv.DistinctList<AnimationGroup>)InvAnimation.Node;
            InvAnimation.Node = null;

            foreach (var AndroidAnimationGroup in AndroidAnimationGroupList)
            {
              AndroidAnimationGroup.AnimationSet.Cancel();
              AndroidAnimationGroup.AndroidPanel.ClearAnimation();
            }
          }
        }
        InvSurfaceActive.StopAnimationSet.Clear();
      }
      else if (InvSurfaceActive.StartAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StartAnimationSet)
        {
          var AndroidAnimationGroupList = new Inv.DistinctList<AnimationGroup>();
          InvAnimation.Node = AndroidAnimationGroupList;

          var InvLastTarget = InvAnimation.GetTargets().LastOrDefault();

          foreach (var InvTarget in InvAnimation.GetTargets())
          {
            var InvPanel = InvTarget.Panel;
            var AndroidPanel = TranslatePanel(InvPanel);

            // TODO: switch to ValueAnimator?

            // NOTE: AnimationSet does not support animations on different views.
            var AndroidAnimationSet = new Android.Views.Animations.AnimationSet(false);

            var InvLastCommand = InvTarget.GetCommands().LastOrDefault();
            
            foreach (var InvCommand in InvTarget.GetCommands())
            {
              var InvOpacityCommand = (Inv.AnimationOpacityCommand)InvCommand;

              var AndroidAlphaAnimation = new Android.Views.Animations.AlphaAnimation(InvOpacityCommand.From, InvOpacityCommand.To);

              if (InvOpacityCommand.Offset != null)
                AndroidAlphaAnimation.StartOffset = (long)InvOpacityCommand.Offset.Value.TotalMilliseconds;

              AndroidAlphaAnimation.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
              AndroidAlphaAnimation.Duration = (long)InvOpacityCommand.Duration.TotalMilliseconds;
              AndroidAlphaAnimation.AnimationStart += (S, E) =>
              {
                if (InvAnimation.IsActive)
                {
                  if (InvOpacityCommand.From <= InvOpacityCommand.To)
                  {
                    AndroidPanel.Alpha = InvOpacityCommand.To;
                    InvPanel.Opacity.BypassSet(InvOpacityCommand.To);
                  }
                  else
                  {
                    AndroidPanel.Alpha = InvOpacityCommand.From;
                    InvPanel.Opacity.BypassSet(InvOpacityCommand.From);
                  }
                }
              };
              AndroidAlphaAnimation.AnimationEnd += (S, E) =>
              {
                if (InvAnimation.IsActive)
                {
                  AndroidPanel.Alpha = InvOpacityCommand.To;
                  InvPanel.Opacity.BypassSet(InvOpacityCommand.To);

                  InvCommand.Complete();

                  if (InvTarget == InvLastTarget && InvCommand == InvLastCommand)
                    InvAnimation.Complete();
                }
              };

              AndroidAnimationSet.AddAnimation(AndroidAlphaAnimation);
            }

            AndroidPanel.StartAnimation(AndroidAnimationSet);

            var AndroidAnimationGroup = new AnimationGroup(AndroidPanel, AndroidAnimationSet);
            AndroidAnimationGroupList.Add(AndroidAnimationGroup);
          }
        }
        InvSurfaceActive.StartAnimationSet.Clear();
      }
    }

    private AndroidLayoutContainer TranslatePanel(Inv.Panel InvPanel)
    {
      if (InvPanel == null)
        return null;
      else
        return RouteArray[InvPanel.PanelType](InvPanel).Container;
    }
    private AndroidNode TranslateBoard(Inv.Panel InvPanel)
    {
      var InvBoard = (Inv.Board)InvPanel;

      var AndroidNode = AccessPanel(InvBoard, () =>
      {
        var AndroidBoard = new AndroidLayoutBoard(AndroidActivity);
        return AndroidBoard;
      });

      RenderPanel(InvBoard, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidBoard = AndroidNode.Element;

        TranslateLayout(InvBoard, AndroidLayout, AndroidBoard);
        TranslateBackground(InvBoard, AndroidBoard.Background);

        if (InvBoard.PinCollection.Render())
        {
          AndroidBoard.RemoveAllViews();

          foreach (var InvPin in InvBoard.PinCollection)
          {
            var AndroidPanel = TranslatePanel(InvPin.Panel);
            AndroidBoard.AddElement(AndroidPanel, PtToPx(InvPin.Rect.Left), PtToPx(InvPin.Rect.Top), PtToPx(InvPin.Rect.Right), PtToPx(InvPin.Rect.Bottom));
          }
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateBrowser(Inv.Panel InvPanel)
    {
      var InvBrowser = (Inv.Browser)InvPanel;

      var AndroidNode = AccessPanel(InvPanel, () =>
      {
        var AndroidBrowser = new AndroidLayoutBrowser(AndroidActivity);
        return AndroidBrowser;
      });

      RenderPanel(InvBrowser, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidBrowser = AndroidNode.Element;

        TranslateLayout(InvBrowser, AndroidLayout, AndroidBrowser);
        TranslateBackground(InvBrowser, AndroidBrowser.Background);

        var Navigate = InvBrowser.UriSingleton.Render();
        if (InvBrowser.HtmlSingleton.Render())
          Navigate = true;

        if (Navigate)
          AndroidBrowser.Navigate(InvBrowser.UriSingleton.Data, InvBrowser.HtmlSingleton.Data);
      });

      return AndroidNode;
    }
    private AndroidNode TranslateButton(Inv.Panel InvPanel)
    {
      var InvButton = (Inv.Button)InvPanel;

      var AndroidNode = AccessPanel(InvPanel, () =>
      {
        var AndroidButton = new AndroidLayoutButton(AndroidActivity);
        AndroidButton.Click += (Sender, Event) =>
        {
          Guard(() =>
          {
            // Down -> Up -> Click.
            //Debug.WriteLine("Click");

            if (InvApplication.Window.IsActiveSurface(InvButton.Surface) && ActivatedButton == AndroidButton)
            {
              this.ActivatedButton = null;
              InvButton.SingleTapInvoke();
            }
          });
        };
        AndroidButton.LongClick += (Sender, Event) =>
        {
          Guard(() =>
          {
            // Down -> LongClick -> Up
            //Debug.WriteLine("LongClick");

            if (InvApplication.Window.IsActiveSurface(InvButton.Surface) && ExclusiveButton == AndroidButton)
            {
              this.ActivatedButton = null;
              InvButton.ContextTapInvoke();
            }
          });
        };
        AndroidButton.Touch += (Sender, Event) =>
        {
          Guard(() =>
          {
            if (InvApplication.Window.IsActiveSurface(InvButton.Surface))
            {
              switch (Event.Event.Action)
              {
                case Android.Views.MotionEventActions.Down:
                case Android.Views.MotionEventActions.PointerDown:
                  //Debug.WriteLine("Down");

                  if (ExclusiveButton == null || ExclusiveButton == AndroidButton)
                  {
                    this.ExclusiveButton = AndroidButton;
                    this.ActivatedButton = null;

                    TranslateBackground(TranslatePressColour(InvButton.Background.Colour), AndroidButton.Background);

                    InvButton.PressInvoke();
                  }
                  break;

                case Android.Views.MotionEventActions.Up:
                case Android.Views.MotionEventActions.PointerUp:
                  //Debug.WriteLine("Up");

                  if (ExclusiveButton == AndroidButton)
                  {
                    this.ExclusiveButton = null;
                    this.ActivatedButton = AndroidButton;

                    if (AndroidButton.Hovered)
                      TranslateBackground(TranslateHoverColour(InvButton.Background.Colour), AndroidButton.Background);
                    else
                      TranslateBackground(InvButton.Background.Colour, AndroidButton.Background);

                    InvButton.ReleaseInvoke();
                  }
                  break;

                case Android.Views.MotionEventActions.Cancel:
                  //Debug.WriteLine("Cancel");

                  if (ExclusiveButton == AndroidButton)
                  {
                    this.ExclusiveButton = null;

                    TranslateBackground(InvButton.Background.Colour, AndroidButton.Background);
                  }
                  break;

                default:
                  break;
              }
            }

            Event.Handled = false;
          });
        };
        AndroidButton.Hover += (Sender, Event) =>
        {
          Guard(() =>
          {
            if (InvApplication.Window.IsActiveSurface(InvButton.Surface))
            {
              switch (Event.Event.Action)
              {
                case Android.Views.MotionEventActions.HoverEnter:
                  TranslateBackground(TranslateHoverColour(InvButton.Background.Colour), AndroidButton.Background);
                  break;

                case Android.Views.MotionEventActions.HoverExit:
                  TranslateBackground(InvButton.Background.Colour, AndroidButton.Background);
                  break;

                default:
                  break;
              }
            }
          });
        };
        return AndroidButton;
      });

      RenderPanel(InvButton, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidButton = AndroidNode.Element;

        AndroidLayout.Alpha = InvButton.IsEnabled ? 1.0F : 0.5F;

        AndroidButton.Enabled = InvButton.IsEnabled;
        AndroidButton.Focusable = InvButton.IsFocusable;
        TranslateLayout(InvButton, AndroidLayout, AndroidButton);
        TranslateBackground(InvButton, AndroidButton.Background);

        if (InvButton.ContentSingleton.Render())
        {
          AndroidButton.SetContentElement(null); // detach previous content in case it has moved.
          var AndroidContent = TranslatePanel(InvButton.Content);
          AndroidButton.SetContentElement(AndroidContent);
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateCanvas(Inv.Panel InvPanel)
    {
      var InvCanvas = (Inv.Canvas)InvPanel;

      var AndroidNode = AccessPanel(InvCanvas, () =>
      {
        var Result = new AndroidLayoutCanvas(AndroidActivity, this);
        Result.PressEvent += (TouchX, TouchY) => Guard(() => InvCanvas.PressInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.MoveEvent += (TouchX, TouchY) => Guard(() => InvCanvas.MoveInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.ReleaseEvent += (TouchX, TouchY) => Guard(() => InvCanvas.ReleaseInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.SingleTapEvent += (TouchX, TouchY) => Guard(() => InvCanvas.SingleTapInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.DoubleTapEvent += (TouchX, TouchY) => Guard(() => InvCanvas.DoubleTapInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.LongPressEvent += (TouchX, TouchY) => Guard(() => InvCanvas.ContextTapInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.ZoomEvent += (TouchX, TouchY, Delta) => Guard(() => InvCanvas.ZoomInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY)), Delta));
        Result.GraphicsDrawAction = () => Guard(() => InvCanvas.DrawInvoke(Result));
        return Result;
      });

      RenderPanel(InvCanvas, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidCanvas = AndroidNode.Element;

        TranslateLayout(InvCanvas, AndroidLayout, AndroidCanvas);
        TranslateBackground(InvCanvas, AndroidCanvas.Background);

        if (InvCanvas.Redrawing)
          AndroidCanvas.Invalidate();
      });

      return AndroidNode;
    }
    private AndroidNode TranslateDock(Inv.Panel InvPanel)
    {
      var InvDock = (Inv.Dock)InvPanel;

      var IsHorizontal = InvDock.Orientation == DockOrientation.Horizontal;

      var AndroidNode = AccessPanel(InvDock, () =>
      {
        var AndroidDock = new AndroidLayoutDock(AndroidActivity);
        AndroidDock.Orientation = IsHorizontal ? Android.Widget.Orientation.Horizontal : Android.Widget.Orientation.Vertical;
        return AndroidDock;
      });

      RenderPanel(InvDock, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidDock = AndroidNode.Element;

        TranslateLayout(InvDock, AndroidLayout, AndroidDock);
        TranslateBackground(InvDock, AndroidDock.Background);

        if (InvDock.CollectionRender())
          AndroidDock.ComposeElements(InvDock.HeaderCollection.Select(H => TranslatePanel(H)), InvDock.ClientCollection.Select(C => TranslatePanel(C)), InvDock.FooterCollection.Reverse().Select(F => TranslatePanel(F)));
      });

      return AndroidNode;
    }
    private AndroidNode TranslateEdit(Inv.Panel InvPanel)
    {
      var InvEdit = (Inv.Edit)InvPanel;

      var IsSearch = InvEdit.Input == EditInput.Search;

      var AndroidNode = AccessPanel(InvEdit, () =>
      {
        if (IsSearch)
        {
          var AndroidSearch = new AndroidLayoutSearch(AndroidActivity);

          AndroidSearch.FocusableInTouchMode = true;
          AndroidSearch.QueryTextChange += (Sender, Event) => Guard(() => InvEdit.ChangeText(AndroidSearch.Query));
          AndroidSearch.QueryTextSubmit += (Sender, Event) =>
          {
            Guard(() =>
            {
              //AndroidEdit.HideKeyboard();
              InvEdit.Return();
            });
          };

          return (Android.Views.View)AndroidSearch;
        }
        else
        {
          var AndroidEdit = new AndroidLayoutEdit(AndroidActivity);

          switch (InvEdit.Input)
          {
            case EditInput.Number:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassNumber;
              break;

            case EditInput.Integer:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassNumber | Android.Text.InputTypes.NumberFlagSigned;
              break;

            case EditInput.Decimal:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassNumber | Android.Text.InputTypes.NumberFlagSigned | Android.Text.InputTypes.NumberFlagDecimal;
              break;

            case EditInput.Password:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationPassword;
              break;

            case EditInput.Name:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationPersonName | Android.Text.InputTypes.TextFlagCapWords;
              break;

            case EditInput.Email:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationEmailAddress;
              break;

            case EditInput.Phone:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassPhone;
              break;

            case EditInput.Search:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText;
              break;

            case EditInput.Text:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextFlagAutoCorrect | Android.Text.InputTypes.TextFlagAutoComplete;
              break;

            case EditInput.Uri:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationUri;
              break;

            default:
              throw new Exception("EditInput not handled: " + InvEdit.Input);
          }

          AndroidEdit.FocusableInTouchMode = true;
          AndroidEdit.AfterTextChanged += (Sender, Event) => Guard(() => InvEdit.ChangeText(AndroidEdit.Text));
          AndroidEdit.KeyPress += (Sender, Event) =>
          {
            Guard(() =>
            {
              if (Event.Event.Action == Android.Views.KeyEventActions.Down && Event.KeyCode == Android.Views.Keycode.Enter)
              {
                AndroidEdit.HideKeyboard();
                InvEdit.Return();
              }
              else
              {
                Event.Handled = false;
              }
            });
          };
          AndroidEdit.EditorAction += (Sender, Event) =>
          {
            Guard(() =>
            {
              if (Event.ActionId == Android.Views.InputMethods.ImeAction.Done)
              {
                AndroidEdit.HideKeyboard();
                InvEdit.Return();
              }
              else
              {
                Event.Handled = false;
              }
            });
          };

          return (Android.Views.View)AndroidEdit;
        }
      });

      RenderPanel(InvEdit, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;

        if (IsSearch)
        {
          var AndroidSearch = (AndroidLayoutSearch)AndroidNode.Element;
          TranslateLayout(InvEdit, AndroidLayout, AndroidSearch);
          TranslateBackground(InvEdit, AndroidSearch.Background);
          
          TranslateFont(InvEdit.Font, (FontTypeface, FontColor, FontSize) => AndroidSearch.SetFont(FontTypeface, FontColor, FontSize));
          
          AndroidSearch.Enabled = !InvEdit.IsReadOnly;

          if (InvEdit.Text != AndroidSearch.Query)
          {
            // NOTE: this works, but doesn't seem to display when it is first run.
            AndroidSearch.SetQuery(InvEdit.Text, false);
          }
        }
        else
        {
          var AndroidEdit = (AndroidLayoutEdit)AndroidNode.Element;

          TranslateLayout(InvEdit, AndroidLayout, AndroidEdit);
          TranslateBackground(InvEdit, AndroidEdit.Background);

          TranslateFont(InvEdit.Font, (FontTypeface, FontColor, FontSize) =>
          {
            AndroidEdit.SetTypeface(FontTypeface, Android.Graphics.TypefaceStyle.Normal);
            AndroidEdit.SetTextColor(FontColor);
            AndroidEdit.SetTextSize(Android.Util.ComplexUnitType.Sp, FontSize);
          });

          AndroidEdit.Enabled = !InvEdit.IsReadOnly;
          AndroidEdit.Text = InvEdit.Text;
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateGraphic(Inv.Panel InvPanel)
    {
      var InvGraphic = (Inv.Graphic)InvPanel;

      var AndroidNode = AccessPanel(InvGraphic, () =>
      {
        var AndroidGraphic = new AndroidLayoutGraphic(AndroidActivity);
        return AndroidGraphic;
      });

      RenderPanel(InvGraphic, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidGraphic = AndroidNode.Element;

        TranslateLayout(InvGraphic, AndroidLayout, AndroidGraphic);
        TranslateBackground(InvGraphic, AndroidGraphic.Background);

        if (InvGraphic.ImageSingleton.Render())
        {
          var Bitmap = TranslateGraphicsBitmap(InvGraphic.Image);
          AndroidGraphic.SetImageBitmap(Bitmap);
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateLabel(Inv.Panel InvPanel)
    {
      var InvLabel = (Inv.Label)InvPanel;

      var AndroidNode = AccessPanel(InvLabel, () =>
      {
        var AndroidLabel = new AndroidLayoutLabel(AndroidActivity);
        return AndroidLabel;
      });

      RenderPanel(InvLabel, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidLabel = AndroidNode.Element;

        TranslateLayout(InvLabel, AndroidLayout, AndroidLabel);
        TranslateBackground(InvLabel, AndroidLabel.Background);
        TranslateFont(InvLabel.Font, (FontTypeface, FontColor, FontSize) =>
        {
          AndroidLabel.SetTypeface(FontTypeface, Android.Graphics.TypefaceStyle.Normal);
          AndroidLabel.SetTextColor(FontColor);
          AndroidLabel.SetTextSize(Android.Util.ComplexUnitType.Sp, FontSize);
        });

        AndroidLabel.Gravity = (InvLabel.Justification == Justification.Center ? Android.Views.GravityFlags.CenterHorizontal : InvLabel.Justification == Justification.Left ? Android.Views.GravityFlags.Left : Android.Views.GravityFlags.Right) | Android.Views.GravityFlags.CenterVertical;
        AndroidLabel.SetSingleLine(!InvLabel.LineWrapping);
        AndroidLabel.Text = InvLabel.Text;
      });

      return AndroidNode;
    }
    private AndroidNode TranslateMemo(Inv.Panel InvPanel)
    {
      var InvMemo = (Inv.Memo)InvPanel;

      var AndroidNode = AccessPanel(InvMemo, () =>
      {
        var AndroidMemo = new AndroidLayoutMemo(AndroidActivity);
        AndroidMemo.AfterTextChanged += (Sender, Event) => Guard(() => InvMemo.ChangeText(AndroidMemo.Text));
        return AndroidMemo;
      });

      RenderPanel(InvMemo, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidMemo = AndroidNode.Element;

        TranslateLayout(InvMemo, AndroidLayout, AndroidMemo);
        TranslateBackground(InvMemo, AndroidMemo.Background);

        TranslateFont(InvMemo.Font, (FontTypeface, FontColor, FontSize) =>
        {
          AndroidMemo.SetTypeface(FontTypeface, Android.Graphics.TypefaceStyle.Normal);
          AndroidMemo.SetTextColor(FontColor);
          AndroidMemo.SetTextSize(Android.Util.ComplexUnitType.Sp, FontSize);
        });

        AndroidMemo.IsReadOnly = InvMemo.IsReadOnly;
        AndroidMemo.Text = InvMemo.Text;
      });

      return AndroidNode;
    }
    private AndroidNode TranslateOverlay(Inv.Panel InvPanel)
    {
      var InvOverlay = (Inv.Overlay)InvPanel;

      var AndroidNode = AccessPanel(InvOverlay, () =>
      {
        var AndroidOverlay = new AndroidLayoutOverlay(AndroidActivity);
        return AndroidOverlay;
      });

      RenderPanel(InvOverlay, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidOverlay = AndroidNode.Element;

        TranslateLayout(InvOverlay, AndroidLayout, AndroidOverlay);
        TranslateBackground(InvOverlay, AndroidOverlay.Background);

        if (InvOverlay.PanelCollection.Render())
          AndroidOverlay.ComposeElements(InvOverlay.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return AndroidNode;
    }
    private AndroidNode TranslateScroll(Inv.Panel InvPanel)
    {
      var InvScroll = (Inv.Scroll)InvPanel;

      var IsHorizontal = InvScroll.Orientation == ScrollOrientation.Horizontal;

      var AndroidNode = AccessPanel(InvScroll, () =>
      {
        var Result = new AndroidLayoutScroll(AndroidActivity, IsHorizontal);
        return Result;
      });

      RenderPanel(InvScroll, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidScroll = AndroidNode.Element;

        TranslateLayout(InvScroll, AndroidLayout, AndroidScroll);
        TranslateBackground(InvScroll, AndroidScroll.Background);

        if (InvScroll.ContentSingleton.Render())
        {
          var AndroidContent = TranslatePanel(InvScroll.Content);
          AndroidScroll.SetContentElement(AndroidContent);
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateFrame(Inv.Panel InvPanel)
    {
      var InvFrame = (Inv.Frame)InvPanel;

      var AndroidNode = AccessPanel(InvPanel, () =>
      {
        var Result = new AndroidLayoutFrame(AndroidActivity);
        return Result;
      });

      RenderPanel(InvFrame, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidFrame = AndroidNode.Element;

        TranslateLayout(InvFrame, AndroidLayout, AndroidFrame);
        TranslateBackground(InvFrame, AndroidFrame.Background);

        if (InvFrame.ContentSingleton.Render())
        {
          AndroidFrame.SetContentElement(null); // detach previous content in case it has moved.
          var AndroidPanel = TranslatePanel(InvFrame.Content);
          AndroidFrame.SetContentElement(AndroidPanel);
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateStack(Inv.Panel InvPanel)
    {
      var InvStack = (Inv.Stack)InvPanel;

      var IsVertical = InvStack.Orientation == StackOrientation.Vertical;

      var AndroidNode = AccessPanel(InvStack, () =>
      {
        var AndroidStack = new AndroidLayoutStack(AndroidActivity);
        AndroidStack.Orientation = IsVertical ? Android.Widget.Orientation.Vertical : Android.Widget.Orientation.Horizontal;
        return AndroidStack;
      });

      RenderPanel(InvStack, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidStack = AndroidNode.Element;

        TranslateLayout(InvStack, AndroidLayout, AndroidStack);
        TranslateBackground(InvStack, AndroidStack.Background);

        if (InvStack.PanelCollection.Render())
          AndroidStack.ComposeElements(InvStack.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return AndroidNode;
    }
    private AndroidNode TranslateTable(Inv.Panel InvPanel)
    {
      var InvTable = (Inv.Table)InvPanel;

      var AndroidNode = AccessPanel(InvTable, () =>
      {
        var AndroidTable = new AndroidLayoutTable(AndroidActivity);
        return AndroidTable;
      });

      RenderPanel(InvTable, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidTable = AndroidNode.Element;

        TranslateLayout(InvTable, AndroidLayout, AndroidTable);
        TranslateBackground(InvTable, AndroidTable.Background);

        if (InvTable.CollectionRender())
        {
          var AndroidRowList = new Inv.DistinctList<AndroidLayoutTableRow>(InvTable.RowCollection.Count);

          foreach (var InvRow in InvTable.RowCollection)
          {
            var AndroidRow = new AndroidLayoutTableRow(AndroidTable, TranslatePanel(InvRow.Content), InvRow.LengthType == TableAxisLength.Star, InvRow.LengthType == TableAxisLength.Fixed ? InvRow.LengthValue : InvRow.LengthType == TableAxisLength.Star ? InvRow.LengthValue : (int?)null);
            AndroidRowList.Add(AndroidRow);
          }

          var AndroidColumnList = new Inv.DistinctList<AndroidLayoutTableColumn>(InvTable.ColumnCollection.Count);

          foreach (var InvColumn in InvTable.ColumnCollection)
          {
            var AndroidColumn = new AndroidLayoutTableColumn(AndroidTable, TranslatePanel(InvColumn.Content), InvColumn.LengthType == TableAxisLength.Star, InvColumn.LengthType == TableAxisLength.Fixed ? InvColumn.LengthValue : InvColumn.LengthType == TableAxisLength.Star ? InvColumn.LengthValue : (int?)null);
            AndroidColumnList.Add(AndroidColumn);
          }

          var AndroidCellList = new Inv.DistinctList<AndroidLayoutTableCell>(InvTable.CellCollection.Count);

          foreach (var InvCell in InvTable.CellCollection)
          {
            var AndroidCell = new AndroidLayoutTableCell(AndroidColumnList[InvCell.Column.Index], AndroidRowList[InvCell.Row.Index], TranslatePanel(InvCell.Content));
            AndroidCellList.Add(AndroidCell);
          }

          AndroidTable.ComposeElements(AndroidRowList, AndroidColumnList, AndroidCellList);
        }
      });

      return AndroidNode;
    }
    private AndroidNode TranslateFlow(Inv.Panel InvPanel)
    {
      var InvFlow = (Inv.Flow)InvPanel;
      
      var AndroidNode = AccessPanel(InvFlow, () =>
      {
        var AndroidFlow = new AndroidLayoutFlow(AndroidActivity, new AndroidLayoutFlow.Configuration
        {
          SectionCountQuery = () => InvFlow.SectionList.Count,
          ItemCountQuery = (Section) => InvFlow.SectionList[Section].ItemCount,
          ItemContentQuery = (Section, Item) => TranslatePanel(InvFlow.SectionList[Section].ItemInvoke(Item)),
          HeaderContentQuery = (Section) => TranslatePanel(InvFlow.SectionList[Section].Header),
          FooterContentQuery = (Section) => TranslatePanel(InvFlow.SectionList[Section].Footer)
        });
        AndroidFlow.RefreshEvent += () => InvFlow.RefreshInvoke(new FlowRefresh(InvFlow, AndroidFlow.CompleteRefresh));
        return AndroidFlow;
      });

      RenderPanel(InvFlow, AndroidNode, () =>
      {
        var AndroidLayout = AndroidNode.Container;
        var AndroidFlow = AndroidNode.Element;

        TranslateLayout(InvFlow, AndroidLayout, AndroidFlow);
        TranslateBackground(InvFlow, AndroidFlow.Background);

        AndroidFlow.IsRefreshable = InvFlow.IsRefreshable;

        if (InvFlow.IsRefresh)
        {
          InvFlow.IsRefresh = false;

          AndroidFlow.NewRefresh();
        }

        if (InvFlow.IsReload || InvFlow.ReloadSectionList.Count > 0 || InvFlow.ReloadItemList.Count > 0)
        {
          InvFlow.IsReload = false;
          InvFlow.ReloadSectionList.Clear(); // not supported.
          InvFlow.ReloadItemList.Clear(); // not supported.

          AndroidFlow.Reload();
        }

        if (InvFlow.ScrollSection != null || InvFlow.ScrollIndex != null)
        {
          if (InvFlow.ScrollSection != null && InvFlow.ScrollIndex != null)
            AndroidFlow.ScrollToItem(InvFlow.ScrollSection.Value, InvFlow.ScrollIndex.Value);

          InvFlow.ScrollSection = null;
          InvFlow.ScrollIndex = null;
        }
      });

      return AndroidNode;
    }

    private AndroidLayoutSurface AccessSurface(Inv.Surface InvSurface, Func<Inv.Surface, AndroidLayoutSurface> BuildFunction)
    {
      if (InvSurface.Node == null)
      {
        var Result = BuildFunction(InvSurface);

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (AndroidLayoutSurface)InvSurface.Node;
      }
    }
    private System.Timers.Timer AccessTimer(Inv.Timer InvTimer, Func<Inv.Timer, System.Timers.Timer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (System.Timers.Timer)InvTimer.Node;
      }
    }
    private AndroidNode<T> AccessPanel<T>(Panel InvPanel, Func<T> BuildFunction)
      where T : Android.Views.View
    {
      if (InvPanel.Node == null)
      {
        var Result = new AndroidNode<T>();

        Result.Container = new AndroidLayoutContainer(AndroidActivity);
        Result.Element = BuildFunction();

        Result.Container.SetContentElement(Result.Element);

        InvPanel.Node = Result;

        return Result;
      }
      else
      {
        return (AndroidNode<T>)InvPanel.Node;
      }
    }
    private void RenderPanel(Inv.Panel InvPanel, AndroidNode AndroidNode, Action Action)
    {
      if (InvPanel.Render())
        Action();
    }
    private void TranslateBackground(Inv.Panel InvPanel, AndroidLayoutBackground AndroidBackground)
    {
      if (InvPanel.Background.Render())
        AndroidBackground.SetColor(TranslateGraphicsColor(InvPanel.Background.Colour));

      if (InvPanel.Corner.Render())
        AndroidBackground.SetCornerRadius(PtToPx(InvPanel.Corner.TopLeft), PtToPx(InvPanel.Corner.TopRight), PtToPx(InvPanel.Corner.BottomRight), PtToPx(InvPanel.Corner.BottomLeft));

      if (InvPanel.Border.Render())
        AndroidBackground.SetBorderStroke(TranslateGraphicsColor(InvPanel.Border.Colour), PtToPx(InvPanel.Border.Left), PtToPx(InvPanel.Border.Top), PtToPx(InvPanel.Border.Right), PtToPx(InvPanel.Border.Bottom));
    }
    private void TranslateBackground(Inv.Colour InvColour, AndroidLayoutBackground AndroidBackground)
    {
      AndroidBackground.SetColor(TranslateGraphicsColor(InvColour));
    }
    private void TranslateLayout(Inv.Panel InvPanel, AndroidLayoutContainer AndroidContainer, Android.Views.View AndroidElement)
    {
      if (InvPanel.Opacity.Render())
        AndroidElement.Alpha = InvPanel.Opacity.Get();

      var InvVisibility = InvPanel.Visibility;
      if (InvVisibility.Render())
        AndroidContainer.SetContentVisiblity(InvVisibility.Get());

      var InvPadding = InvPanel.Padding;
      var InvBorder = InvPanel.Border;
      if (InvPadding.Render() || InvBorder.IsChanged)
        AndroidElement.SetPadding(PtToPx(InvPadding.Left + InvBorder.Left), PtToPx(InvPadding.Top + InvBorder.Top), PtToPx(InvPadding.Right + InvBorder.Right), PtToPx(InvPadding.Bottom + InvBorder.Bottom));

      TranslateAlignment(InvPanel.Alignment, AndroidContainer);
      TranslateMargin(InvPanel.Margin, AndroidContainer);
      TranslateSize(InvPanel.Size, AndroidContainer);

      // ClipToOutline, OutlineProvider, Elevation introduced in API 21 (TranslateElevation needs to be a separate method to avoid breaking the runtime).
      if (AndroidSdkVersion >= 21)
        TranslateElevation(AndroidContainer, AndroidElement, InvPanel);
    }
    private void TranslateElevation(AndroidLayoutContainer AndroidContainer, Android.Views.View AndroidPanel, Panel InvPanel)
    {
      var InvElevation = InvPanel.Elevation;
      if (InvElevation.Render())
      {
        // TODO: custom shadows so we can select the elevation colour?

        var Depth = InvElevation.Get();

        if (Depth > 0)
        {
          Debug.Assert(AndroidPanel.EnumerateParents().All(G => !G.ClipChildren), "All ViewGroup parents of an elevated panel must have .SetClipChildren(false) called on them to ensure elevation shadows won't be clipped.");

          AndroidContainer.SetClipToPadding(false);
          
          AndroidPanel.OutlineProvider = new AndroidPanelOutlineProvider(this, InvPanel);
          AndroidPanel.Elevation = PtToPx(Depth);
        }
        else
        {
          AndroidPanel.Elevation = 0;
        }
      }
    }
    private void TranslateMargin(Inv.Edge InvMargin, AndroidLayoutContainer AndroidContainer)
    {
      if (InvMargin.Render())
      {
        if (InvMargin.IsSet)
          AndroidContainer.SetContentMargins(PtToPx(InvMargin.Left), PtToPx(InvMargin.Top), PtToPx(InvMargin.Right), PtToPx(InvMargin.Bottom));
        else
          AndroidContainer.SetContentMargins(0, 0, 0, 0);
      }
    }
    private void TranslateAlignment(Inv.Alignment InvAlignment, AndroidLayoutContainer AndroidContainer)
    {
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Placement.BottomStretch:
            AndroidContainer.SetContentVerticalBottom();
            AndroidContainer.SetContentHorizontalStretch();
            break;

          case Placement.BottomLeft:
            AndroidContainer.SetContentVerticalBottom();
            AndroidContainer.SetContentHorizontalLeft();
            break;

          case Placement.BottomCenter:
            AndroidContainer.SetContentVerticalBottom();
            AndroidContainer.SetContentHorizontalCenter();
            break;

          case Placement.BottomRight:
            AndroidContainer.SetContentVerticalBottom();
            AndroidContainer.SetContentHorizontalRight();
            break;

          case Placement.TopStretch:
            AndroidContainer.SetContentVerticalTop();
            AndroidContainer.SetContentHorizontalStretch();
            break;

          case Placement.TopLeft:
            AndroidContainer.SetContentVerticalTop();
            AndroidContainer.SetContentHorizontalLeft();
            break;

          case Placement.TopCenter:
            AndroidContainer.SetContentVerticalTop();
            AndroidContainer.SetContentHorizontalCenter();
            break;

          case Placement.TopRight:
            AndroidContainer.SetContentVerticalTop();
            AndroidContainer.SetContentHorizontalRight();
            break;

          case Placement.CenterStretch:
            AndroidContainer.SetContentVerticalCenter();
            AndroidContainer.SetContentHorizontalStretch();
            break;

          case Placement.CenterLeft:
            AndroidContainer.SetContentVerticalCenter();
            AndroidContainer.SetContentHorizontalLeft();
            break;

          case Placement.Center:
            AndroidContainer.SetContentVerticalCenter();
            AndroidContainer.SetContentHorizontalCenter();
            break;

          case Placement.CenterRight:
            AndroidContainer.SetContentVerticalCenter();
            AndroidContainer.SetContentHorizontalRight();
            break;

          case Placement.Stretch:
            AndroidContainer.SetContentVerticalStretch();
            AndroidContainer.SetContentHorizontalStretch();
            break;

          case Placement.StretchLeft:
            AndroidContainer.SetContentVerticalStretch();
            AndroidContainer.SetContentHorizontalLeft();
            break;

          case Placement.StretchCenter:
            AndroidContainer.SetContentVerticalStretch();
            AndroidContainer.SetContentHorizontalCenter();
            break;

          case Placement.StretchRight:
            AndroidContainer.SetContentVerticalStretch();
            AndroidContainer.SetContentHorizontalRight();
            break;

          default:
            throw new ApplicationException("Placement not handled: " + InvAlignment.Get());
        }
      }
    }
    private void TranslateSize(Inv.Size InvSize, AndroidLayoutContainer AndroidContainer)
    {
      if (InvSize.Render())
      {
        if (InvSize.Width != null)
          AndroidContainer.SetContentWidth(PtToPx(InvSize.Width.Value));
        else
          AndroidContainer.ClearContentWidth();

        if (InvSize.Height != null)
          AndroidContainer.SetContentHeight(PtToPx(InvSize.Height.Value));
        else
          AndroidContainer.ClearContentHeight();

        if (InvSize.MinimumWidth != null)
          AndroidContainer.SetContentMinimumWidth(PtToPx(InvSize.MinimumWidth.Value));
        else
          AndroidContainer.ClearContentMinimumWidth();

        if (InvSize.MinimumHeight != null)
          AndroidContainer.SetContentMinimumHeight(PtToPx(InvSize.MinimumHeight.Value));
        else
          AndroidContainer.ClearContentMinimumHeight();

        if (InvSize.MaximumWidth != null)
          AndroidContainer.SetContentMaximumWidth(PtToPx(InvSize.MaximumWidth.Value));
        else
          AndroidContainer.ClearContentMaximumWidth();

        if (InvSize.MaximumHeight != null)
          AndroidContainer.SetContentMaximumHeight(PtToPx(InvSize.MaximumHeight.Value));
        else
          AndroidContainer.ClearContentMaximumHeight();
      }
    }
    private void TranslateFont(Inv.Font InvFont, Action<Android.Graphics.Typeface, Android.Graphics.Color, int> Action)
    {
      if (InvFont.Render())
      {
        var FontTypeface = TranslateGraphicsTypeface(InvFont.Name, InvFont.Weight);
        var FontColor = TranslateGraphicsColor(InvFont.Colour ?? Inv.Colour.Black);
        var FontSize = InvFont.Size ?? 14; // TODO: what is the Inv default font size?

        Action(FontTypeface, FontColor, FontSize);
      }
    }

    private Android.Graphics.Typeface TranslateGraphicsTypeface(string FontName, Inv.FontWeight InvFontWeight)
    {
      if (FontName != null)
        return Android.Graphics.Typeface.Create(FontName + FontFamilyArray[InvFontWeight], InvFontWeight == FontWeight.Bold ? Android.Graphics.TypefaceStyle.Bold : Android.Graphics.TypefaceStyle.Normal);
      else
        return AndroidFontArray[InvFontWeight];
    }
    private Android.Graphics.Color TranslateGraphicsColor(Inv.Colour InvColour)
    {
      if (InvColour == null)
      {
        return Android.Graphics.Color.Transparent;
      }
      else if (InvColour.Node == null)
      {
        var Result = new Android.Graphics.Color(InvColour.RawValue);

        InvColour.Node = new AndroidColourNode()
        {
          Color = Result,
          Filter = null
        };

        return Result;
      }
      else
      {
        return ((AndroidColourNode)InvColour.Node).Color;
      }
    }
    private Android.Graphics.ColorFilter TranslateGraphicsColorFilter(Inv.Colour InvColour)
    {
      if (InvColour == null)
      {
        return null;
      }
      else if (InvColour.Node == null)
      {
        var Color = new Android.Graphics.Color(InvColour.RawValue);
        var Result = new Android.Graphics.PorterDuffColorFilter(Color, Android.Graphics.PorterDuff.Mode.SrcAtop);

        InvColour.Node = new AndroidColourNode()
        {
          Color = Color,
          Filter = Result
        };

        return Result;
      }
      else
      {
        var Node = (AndroidColourNode)InvColour.Node;

        if (Node.Filter == null)
          Node.Filter = new Android.Graphics.PorterDuffColorFilter(Node.Color, Android.Graphics.PorterDuff.Mode.SrcAtop);

        return Node.Filter;
      }
    }
    private Inv.Colour TranslateHoverColour(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return null;
      else
        return InvColour.Lighten(0.25F);
    }
    private Inv.Colour TranslatePressColour(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return null;
      else
        return InvColour.Darken(0.25F);
    }

    private int CalculateInSampleSize(Android.Graphics.BitmapFactory.Options options, int reqWidth, int reqHeight)
    {
      // Raw height and width of image
      var height = options.OutHeight;
      var width = options.OutWidth;
      var inSampleSize = 1D;

      if (height > reqHeight || width > reqWidth)
      {
        var halfHeight = (int)(height / 2);
        var halfWidth = (int)(width / 2);

        // Calculate a inSampleSize that is a power of 2 - the decoder will use a value that is a power of two anyway.
        while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth)
          inSampleSize *= 2;
      }

      return (int)inSampleSize;
    }
    private string GetDeviceModel()
    {
      var manufacturer = Android.OS.Build.Manufacturer;
      var model = Android.OS.Build.Model;
      if (model.StartsWith(manufacturer))
        return Capitalize(model);
      else
        return Capitalize(manufacturer) + " " + model;
    }
    private string Capitalize(string s)
    {
      if (s == null || s.Length == 0)
        return "";

      var first = s[0];
      if (char.IsUpper(first))
        return s;
      else
        return char.ToUpper(first) + s.Substring(1);
    }
    private bool IsHardwareKeyboardAvailable()
    {
      return AndroidActivity.Resources.Configuration.Keyboard != Android.Content.Res.KeyboardType.Nokeys;
    }

    private int AndroidSdkVersion;
    private float ConvertPtToPxFactor;
    private bool IsProcessPaused;
    private AndroidFrameCallback FrameCallback;
    private Android.Graphics.Paint FillPaint;
    private Android.Graphics.Paint FillAndStrokePaint;
    private Android.Graphics.Paint StrokePaint;
    private Android.Graphics.Paint ImagePaint;
    private Android.Graphics.Paint TextPaint;
    private Android.Views.Choreographer Choreographer;
    private Android.Locations.LocationManager AndroidLocationManager;
    private Android.Views.ViewGroup.LayoutParams AndroidSurfaceLayoutParams;
    private Android.Views.InputMethods.InputMethodManager InputManager;
    private Inv.EnumArray<Inv.PanelType, Func<Inv.Panel, AndroidNode>> RouteArray;
    private Dictionary<Android.Views.Keycode, Inv.Key> KeyDictionary;
    private Inv.EnumArray<Inv.FontWeight, Android.Graphics.Typeface> AndroidFontArray;
    private Inv.DistinctList<Inv.Image> ImageList;
    private Inv.DistinctList<Inv.Sound> SoundList;
    private bool IsProcessSuspended;
    private AndroidLayoutButton ExclusiveButton;
    private AndroidLayoutButton ActivatedButton;
    private EnumArray<FontWeight, string> FontFamilyArray;

    private struct AndroidColourNode
    {
      public Android.Graphics.Color Color;
      public Android.Graphics.ColorFilter Filter;
    }
    
    private sealed class AndroidPanelOutlineProvider : Android.Views.ViewOutlineProvider
    {
      public AndroidPanelOutlineProvider(AndroidEngine Engine, Inv.Panel Panel)
      {
        this.Engine = Engine;
        this.Panel = Panel;
      }

      public override void GetOutline(Android.Views.View View, Android.Graphics.Outline Outline)
      {
        if (Panel.Corner.IsUniform)
        {
          Outline.SetRoundRect(0, 0, View.Width, View.Height, Engine.PtToPx(Panel.Corner.TopLeft));
        }
        else
        {
          var PathBuilder = new AndroidLayoutBezierPathBuilder();
          var Size = new AndroidLayoutBezierPathBuilder.Size(View.Width, View.Height);
          var CornerRadius = new AndroidLayoutBezierPathBuilder.CornerRadius(Engine.PtToPx(Panel.Corner.TopLeft), Engine.PtToPx(Panel.Corner.TopRight), Engine.PtToPx(Panel.Corner.BottomRight), Engine.PtToPx(Panel.Corner.BottomLeft));
          var BorderThickness = new AndroidLayoutBezierPathBuilder.BorderThickness();

          PathBuilder.AddRoundedRect(Size, CornerRadius, BorderThickness);

          Outline.SetConvexPath(PathBuilder.Render());
        }
      }

      private Panel Panel;
      private AndroidEngine Engine;
    }

    private struct AnimationGroup
    {
      public AnimationGroup(Android.Views.View AndroidPanel, Android.Views.Animations.AnimationSet AnimationSet)
      {
        this.AndroidPanel = AndroidPanel;
        this.AnimationSet = AnimationSet;
      }

      public readonly Android.Views.View AndroidPanel;
      public readonly Android.Views.Animations.AnimationSet AnimationSet;
    }
  }

  internal abstract class AndroidNode
  {
    public AndroidLayoutContainer Container { get; set; }
    public Android.Views.View Element { get; set; }
  }

  internal sealed class AndroidNode<T> : AndroidNode
    where T : Android.Views.View
  {
    public new T Element
    {
      get { return (T)base.Element; }
      set { base.Element = value; }
    }
  }
}