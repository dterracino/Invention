/*! 8 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Inv.Support;

namespace Inv
{
  internal interface ITouchable
  {
  }

  internal sealed class AndroidLayoutBackground : Android.Graphics.Drawables.Drawable
  {
    public AndroidLayoutBackground()
    {
      this.Paint = new Paint();
      this.BorderPaint = new Paint();
    }

    public Android.Graphics.Color? GetColor()
    {
      return CurrentState.BackgroundColour;
    }
    public void SetColor(Android.Graphics.Color Color)
    {
      CurrentState.BackgroundColour = Color;

      InvalidateSelf();
    }
    public void SetCornerRadius(int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      CurrentState.TopLeftCornerRadius = TopLeft;
      CurrentState.TopRightCornerRadius = TopRight;
      CurrentState.BottomRightCornerRadius = BottomRight;
      CurrentState.BottomLeftCornerRadius = BottomLeft;

      InvalidateSelf();
    }
    public void SetBorderStroke(Android.Graphics.Color Color, int Left, int Top, int Right, int Bottom)
    {
      CurrentState.BorderColour = Color;

      CurrentState.LeftBorderThickness = Left;
      CurrentState.TopBorderThickness = Top;
      CurrentState.RightBorderThickness = Right;
      CurrentState.BottomBorderThickness = Bottom;

      InvalidateSelf();
    }

    public override void Draw(Android.Graphics.Canvas Canvas)
    {
      PreviousState = CurrentState;

      Paint.Color = CurrentState.BackgroundColour ?? Android.Graphics.Color.Transparent;

      ClipPath = null;
      Shape = null;
      BorderShape = null;

      var BorderThicknessSet = CurrentState.IsBorderThicknessSet;
      if (BorderThicknessSet || CurrentState.IsCornerRadiusSet)
      {
        var Width = Bounds.Width();
        var Height = Bounds.Height();

        var Size = new AndroidLayoutBezierPathBuilder.Size(Width, Height);
        var CornerRadius = new AndroidLayoutBezierPathBuilder.CornerRadius(CurrentState.TopLeftCornerRadius, CurrentState.TopRightCornerRadius, CurrentState.BottomRightCornerRadius, CurrentState.BottomLeftCornerRadius);
        var BorderThickness = new AndroidLayoutBezierPathBuilder.BorderThickness(CurrentState.LeftBorderThickness, CurrentState.TopBorderThickness, CurrentState.RightBorderThickness, CurrentState.BottomBorderThickness);

        // Clipping is required on android to resolve situation where inner border edges can extend beyond outer edges
        var ClipBuilder = new AndroidLayoutBezierPathBuilder();
        ClipBuilder.AddRoundedRect(Size, CornerRadius, BorderThickness);
        ClipPath = ClipBuilder.Render();

        var InnerBuilder = new AndroidLayoutBezierPathBuilder();
        InnerBuilder.AddRoundedRectMinusBorder(Size, CornerRadius, BorderThickness);
        var InnerPath = InnerBuilder.Render();

        Shape = new Android.Graphics.Drawables.Shapes.PathShape(InnerPath, Width, Height);
        Shape.Resize(Width, Height);

        if (BorderThicknessSet)
        {
          BorderPaint.Color = CurrentState.BorderColour ?? Android.Graphics.Color.Transparent;

          var BorderBuilder = new AndroidLayoutBezierPathBuilder();
          BorderBuilder.AddRoundedRectBorder(Size, CornerRadius, BorderThickness);
          var BorderPath = BorderBuilder.Render();

          BorderShape = new Android.Graphics.Drawables.Shapes.PathShape(BorderPath, Width, Height);
          BorderShape.Resize(Width, Height);
        }
      }

      if (ClipPath != null)
        Canvas.ClipPath(ClipPath);

      if (Shape == null)
      {
        Canvas.DrawPaint(Paint);
      }
      else
      {
        Shape.Draw(Canvas, Paint);

        if (BorderShape != null)
          BorderShape.Draw(Canvas, BorderPaint);
      }
    }
    public override void SetAlpha(int Alpha)
    {
      this.Alpha = Alpha;
    }
    public override void SetColorFilter(ColorFilter ColorFilter)
    {
      throw new NotImplementedException();
    }

    public override int Opacity
    {
      get { return 1; }
    }

    private Path ClipPath;
    private Android.Graphics.Drawables.Shapes.Shape Shape;
    private Android.Graphics.Drawables.Shapes.Shape BorderShape;
    private Android.Graphics.Paint Paint;
    private Android.Graphics.Paint BorderPaint;
    private State CurrentState;
    private State PreviousState;

    private struct State : IEquatable<State>
    {
      public int TopLeftCornerRadius { get; set; }
      public int TopRightCornerRadius { get; set; }
      public int BottomRightCornerRadius { get; set; }
      public int BottomLeftCornerRadius { get; set; }

      public int LeftBorderThickness { get; set; }
      public int TopBorderThickness { get; set; }
      public int RightBorderThickness { get; set; }
      public int BottomBorderThickness { get; set; }

      public Android.Graphics.Color? BackgroundColour { get; set; }
      public Android.Graphics.Color? BorderColour { get; set; }

      public bool IsCornerRadiusSet
      {
        get { return TopLeftCornerRadius != 0 || TopRightCornerRadius != 0 || BottomRightCornerRadius != 0 || BottomLeftCornerRadius != 0; }
      }
      public bool IsBorderThicknessSet
      {
        get { return LeftBorderThickness != 0 || TopBorderThickness != 0 || RightBorderThickness != 0 || BottomBorderThickness != 0; }
      }
      public bool IsCornerRadiusUniform
      {
        get { return TopLeftCornerRadius == TopRightCornerRadius && TopRightCornerRadius == BottomRightCornerRadius && BottomRightCornerRadius == BottomLeftCornerRadius; }
      }
      public bool IsBorderThicknessUniform
      {
        get { return LeftBorderThickness == TopBorderThickness && TopBorderThickness == RightBorderThickness && RightBorderThickness == BottomBorderThickness; }
      }

      public bool Equals(State Other)
      {
        return TopLeftCornerRadius == Other.TopLeftCornerRadius && TopRightCornerRadius == Other.TopRightCornerRadius
          && BottomRightCornerRadius == Other.BottomRightCornerRadius && BottomLeftCornerRadius == Other.BottomLeftCornerRadius
          && LeftBorderThickness == Other.LeftBorderThickness && TopBorderThickness == Other.TopBorderThickness
          && RightBorderThickness == Other.RightBorderThickness && BottomBorderThickness == Other.BottomBorderThickness
          && BackgroundColour == Other.BackgroundColour && BorderColour == Other.BorderColour;
      }
      public override bool Equals(object Other)
      {
        if (Other is State)
          return Equals((State)Other);

        return false;
      }
      public override int GetHashCode()
      {
        return Tuple.Create(
          Tuple.Create(TopLeftCornerRadius, TopRightCornerRadius, BottomRightCornerRadius, BottomLeftCornerRadius),
          Tuple.Create(LeftBorderThickness, TopBorderThickness, RightBorderThickness, BottomBorderThickness),
          BackgroundColour,
          BorderColour).GetHashCode();
      }
    }
  }

  internal sealed class AndroidLayoutBezierPathBuilder : Inv.BezierPathBuilder<Path>
  {
    public AndroidLayoutBezierPathBuilder()
    {
      this.Path = new Path();
    }

    public override void MoveTo(Point Point)
    {
      Path.MoveTo(Point.X, Point.Y);
    }
    public override void AddLineTo(Point Point)
    {
      Path.LineTo(Point.X, Point.Y);
    }
    public override void AddCurveTo(Point EndPoint, Point ControlPoint1, Point ControlPoint2)
    {
      Path.CubicTo(ControlPoint1.X, ControlPoint1.Y, ControlPoint2.X, ControlPoint2.Y, EndPoint.X, EndPoint.Y);
    }
    public override void Close()
    {
      Path.Close();
    }
    public override Path Render()
    {
      return Path;
    }

    private Path Path;
  }

  internal abstract class AndroidLayoutGroup : Android.Views.ViewGroup
  {
    public AndroidLayoutGroup(Android.Content.Context context)
      : base(context)
    {
      this.SetClipChildren(false);
      
      base.Background = new AndroidLayoutBackground();

      //this.MotionEventSplittingEnabled = false;
    }

    public new AndroidLayoutBackground Background
    {
      get { return (AndroidLayoutBackground)base.Background; }
    }

    public override bool OnTouchEvent(MotionEvent Event)
    {
      if ((Background.GetColor() ?? Android.Graphics.Color.Transparent) == Android.Graphics.Color.Transparent)
        return base.OnTouchEvent(Event);
      else
        return this.PassthroughTouchEvent();
    }
  }

  internal sealed class AndroidLayoutSurface : Android.Views.ViewGroup
  {
    public AndroidLayoutSurface(Android.Content.Context context)
      : base(context)
    {
      this.SetClipChildren(false);

      base.Background = new AndroidLayoutBackground();

      //this.MotionEventSplittingEnabled = false;
    }

    public new AndroidLayoutBackground Background
    {
      get { return (AndroidLayoutBackground)base.Background; }
    }

    public void SetContentElement(Android.Views.View Element)
    {
      if (this.ContentElement != Element)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement);

        this.ContentElement = Element;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement);

        this.Arrange();
      }
    }
    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      int RequestedWidth, RequestedHeight;
      this.MeasureOnlyChild(ContentElement, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      this.LayoutOnlyChild(ContentElement, Left, Top, Right, Bottom);
    }

    private Android.Views.View ContentElement;
  }

  internal sealed class AndroidLayoutButton : Android.Views.ViewGroup, ITouchable
  {
    public AndroidLayoutButton(Android.Content.Context context)
      : base(context)
    {
      this.SetClipChildren(false);

      base.Background = new AndroidLayoutBackground();

      this.Clickable = true;

      //this.MotionEventSplittingEnabled = false;
    }

    public new AndroidLayoutBackground Background
    {
      get { return (AndroidLayoutBackground)base.Background; }
    }

    public void SetContentElement(Android.Views.View Element)
    {
      if (this.ContentElement != Element)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement);

        this.ContentElement = Element;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement);

        this.Arrange();
      }
    }
    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      int RequestedWidth, RequestedHeight;
      this.MeasureOnlyChild(ContentElement, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      this.LayoutOnlyChild(ContentElement, Left, Top, Right, Bottom);
    }

    private Android.Views.View ContentElement;
  }

  internal sealed class AndroidLayoutEdit : Android.Widget.EditText
  {
    public AndroidLayoutEdit(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidLayoutBackground();

      SetSingleLine();
    }

    public new AndroidLayoutBackground Background
    {
      get { return (AndroidLayoutBackground)base.Background; }
    }
  }

  internal sealed class AndroidLayoutSearch : Android.Widget.SearchView
  {
    public AndroidLayoutSearch(Android.Content.Context context)
      : base(context)
    {
      //Find(this);

      base.Background = new AndroidLayoutBackground();

      base.SetPadding(-16, -16, -16, -16);

      this.EditText = (AutoCompleteTextView)FindViewById(Context.Resources.GetIdentifier("android:id/search_src_text", null, null));
      EditText.SetPadding(0, 0, 0, 0);
      //EditText.SetHintTextColor(Color.Black);
      //EditText.Gravity = GravityFlags.Bottom;

      this.PlateView = FindViewById(Context.Resources.GetIdentifier("android:id/search_plate", null, null));
      PlateView.SetBackgroundColor(Color.Transparent);
      PlateView.SetPadding(0, 0, 0, 0);

      this.SearchEditFrame = FindViewById(Context.Resources.GetIdentifier("android:id/search_edit_frame", null, null));
      SearchEditFrame.SetPadding(0, 0, 0, 0);
      //SearchEditFrame.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent, 1);

      this.SubmitArea = FindViewById(Context.Resources.GetIdentifier("android:id/submit_area", null, null));
      SubmitArea.SetPadding(0, 0, 0, 0);

      this.SubmitButton = FindViewById(Context.Resources.GetIdentifier("android:id/search_go_btn", null, null));
      SubmitButton.SetPadding(0, 0, 0, 0);

      this.CloseButton = (ImageView)FindViewById(Context.Resources.GetIdentifier("android:id/search_close_btn", null, null));
      CloseButton.SetPadding(0, 0, 0, 0);

      this.VoiceButton = FindViewById(Context.Resources.GetIdentifier("android:id/search_voice_btn", null, null));
      VoiceButton.SetPadding(0, 0, 0, 0);

      this.SearchHintIcon = (ImageView)FindViewById(Context.Resources.GetIdentifier("android:id/search_mag_icon", null, null));
      SearchHintIcon.SetPadding(0, 0, 0, 0);

      // TODO: not sure about this - it fixes a height problem (collapsed is +8px taller than it should be, but expanded is correct).
      OnActionViewExpanded();
    }

    [Conditional("DEBUG")]
    private void Find(ViewGroup Self)
    {
      for (var i = 0; i < Self.ChildCount; i++)
      {
        var v = Self.GetChildAt(i);

        System.Diagnostics.Debug.WriteLine(v.GetType().Name);
        System.Diagnostics.Debug.WriteLine("PADDING: " + v.PaddingLeft + ", " + v.PaddingTop + ", " + v.PaddingRight + ", " + v.PaddingBottom);
        v.SetPadding(0, 0, 0, 0);

        var LLP = v.LayoutParameters as LinearLayout.LayoutParams;
        System.Diagnostics.Debug.WriteLine("MARGIN: " + LLP.LeftMargin + ", " + LLP.TopMargin + ", " + LLP.RightMargin + ", " + LLP.BottomMargin);
        //v.LayoutParameters = new LinearLayout.LayoutParams(LLP.Width, LLP.Height, LLP.Weight);
        //v.RequestLayout();

        if (v is ViewGroup)
          Find(v as ViewGroup);
      }
    }

    public new AndroidLayoutBackground Background
    {
      get { return (AndroidLayoutBackground)base.Background; }
    }

    public override void SetPadding(int left, int top, int right, int bottom)
    {
      // SearchView has a default 16px padding that we need to remove.
      base.SetPadding(left - 16, top - 16, right - 16, bottom - 16);
    }
    public void SetFont(Typeface FontTypeface, Color FontColor, int FontSize)
    {
      if (EditText != null)
      {
        EditText.SetTypeface(FontTypeface, Android.Graphics.TypefaceStyle.Normal);
        EditText.SetTextColor(FontColor);
        EditText.SetTextSize(Android.Util.ComplexUnitType.Sp, FontSize);
      }

      // TODO: the magnifying glass is not changing its colour.
      if (SearchHintIcon != null)
        SearchHintIcon.SetColorFilter(FontColor, PorterDuff.Mode.SrcAtop);

      // NOTE: this does change the 'cancel search' X.
      if (CloseButton != null)
        CloseButton.SetColorFilter(FontColor, PorterDuff.Mode.SrcAtop);
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      //SetMeasuredDimension(MeasuredWidth - 32, MeasuredHeight - 32);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      base.OnLayout(Changed, Left, Top, Right, Bottom);
    }

    private AutoCompleteTextView EditText;
    private View PlateView;
    private View SearchEditFrame;
    private View SubmitArea;
    private View SubmitButton;
    private ImageView CloseButton;
    private View VoiceButton;
    private ImageView SearchHintIcon;
  }

  internal sealed class AndroidLayoutLabel : Android.Widget.TextView
  {
    public AndroidLayoutLabel(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidLayoutBackground();

      Ellipsize = Android.Text.TextUtils.TruncateAt.End;
      Gravity = GravityFlags.CenterVertical;
    }

    public new AndroidLayoutBackground Background
    {
      get { return (AndroidLayoutBackground)base.Background; }
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      base.OnLayout(Changed, Left, Top, Right, Bottom);
    }
  }

  internal sealed class AndroidLayoutMemo : AndroidLayoutGroup
  {
    public AndroidLayoutMemo(Android.Content.Context context)
      : base(context)
    {
      SetClipChildren(true);

      this.VerticalScroll = new AndroidLayoutVerticalScroll(context);
      AddView(VerticalScroll);

      this.EditText = new Android.Widget.EditText(context);
      VerticalScroll.SetContentElement(EditText);
      EditText.Background = null;
      EditText.Gravity = GravityFlags.Top;
    }

    public new AndroidLayoutBackground Background
    {
      get { return (AndroidLayoutBackground)base.Background; }
    }

    public string Text
    {
      get { return EditText.Text; }
      set { EditText.Text = value; }
    }
    public bool IsReadOnly
    {
      get { return EditText.KeyListener == null; }
      set
      {
        if (IsReadOnly != value)
        {
          var Swap = this.ReadOnlyKeyListener;
          this.ReadOnlyKeyListener = EditText.KeyListener;
          EditText.KeyListener = Swap;
        }
      }
    }
    public event EventHandler<Android.Text.AfterTextChangedEventArgs> AfterTextChanged
    {
      add { EditText.AfterTextChanged += value; }
      remove { EditText.AfterTextChanged -= value; }
    }

    public void SetTypeface(Typeface tf, TypefaceStyle style)
    {
      EditText.SetTypeface(tf, style);
    }
    public void SetTextColor(Color color)
    {
      EditText.SetTextColor(color);
    }
    public void SetTextSize(Android.Util.ComplexUnitType unit, float size)
    {
      EditText.SetTextSize(unit, size);
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      int RequestedWidth, RequestedHeight;
      this.MeasureOnlyChild(VerticalScroll, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      this.LayoutOnlyChild(VerticalScroll, Left, Top, Right, Bottom);
    }

    private AndroidLayoutVerticalScroll VerticalScroll;
    private Android.Widget.EditText EditText;
    private Android.Text.Method.IKeyListener ReadOnlyKeyListener;
  }

  internal sealed class AndroidLayoutDock : AndroidLayoutGroup
  {
    public AndroidLayoutDock(Context Context)
      : base(Context)
    {
      this.ElementList = new DistinctList<Android.Views.View>();
      this.HeaderList = new DistinctList<Android.Views.View>();
      this.ClientList = new DistinctList<Android.Views.View>();
      this.FooterList = new DistinctList<Android.Views.View>();
    }

    public Orientation Orientation { get; set; }

    public void ComposeElements(IEnumerable<Android.Views.View> Headers, IEnumerable<Android.Views.View> Clients, IEnumerable<Android.Views.View> Footers)
    {
      this.HeaderList.Clear();
      HeaderList.AddRange(Headers);

      this.ClientList.Clear();
      ClientList.AddRange(Clients);

      this.FooterList.Clear();
      FooterList.AddRange(Footers);

      var PreviousElementList = ElementList;
      this.ElementList = new DistinctList<Android.Views.View>(HeaderList.Count + ClientList.Count + FooterList.Count);
      ElementList.AddRange(HeaderList);
      ElementList.AddRange(ClientList);
      ElementList.AddRange(FooterList);

      foreach (var Element in ElementList.Except(PreviousElementList))
        this.SafeAddView(Element);

      foreach (var Header in PreviousElementList.Except(ElementList))
        this.SafeRemoveView(Header);

      this.Arrange();
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      if (!ElementList.Any())
      {
        int RequestedWidth;
        int RequestedHeight;
        this.MeasureOnlyChild(null, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);
        SetMeasuredDimension(RequestedWidth, RequestedHeight);
      }
      else
      {
        var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
        var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);
        var HorizontalPadding = PaddingLeft + PaddingRight;
        var VerticalPadding = PaddingTop + PaddingBottom;

        switch (Orientation)
        {
          case Orientation.Horizontal:
            var ElementHeightSpec = MeasureSpec.MakeMeasureSpec(Math.Max(0, MeasuredHeight - VerticalPadding), HeightMode);

            switch (WidthMode)
            {
              case MeasureSpecMode.AtMost:
              case MeasureSpecMode.Exactly:
                var RemainingWidth = Math.Max(0, MeasuredWidth - HorizontalPadding);

                foreach (var Element in HeaderList.Union(FooterList))
                {
                  Element.Measure(MeasureSpec.MakeMeasureSpec(RemainingWidth, MeasureSpecMode.AtMost), ElementHeightSpec);
                  RemainingWidth -= Element.MeasuredWidth;
                  if (RemainingWidth < 0)
                    RemainingWidth = 0;
                }

                if (ClientList.Any())
                {
                  var ClientWidthSpec = MeasureSpec.MakeMeasureSpec(RemainingWidth / ClientList.Count, WidthMode);

                  foreach (var Client in ClientList)
                    Client.Measure(ClientWidthSpec, ElementHeightSpec);
                }

                var ToMeasureList = new DistinctList<MeasureableElement>();

                if (HeightMode != MeasureSpecMode.Exactly)
                {
                  var ElementHeight = ElementList.Max(E => E.MeasuredHeight);
                  ElementHeightSpec = MeasureSpec.MakeMeasureSpec(ElementHeight, MeasureSpecMode.Exactly);

                  ToMeasureList.AddRange(ElementList.Where(E => E.MeasuredHeight != ElementHeight).Select(E => new MeasureableElement
                  {
                    Element = E,
                    HeightMeasureSpec = ElementHeightSpec
                  }));
                }

                if (WidthMode == MeasureSpecMode.AtMost && ClientList.Count > 1)
                {
                  var ClientWidth = ClientList.Max(C => C.MeasuredWidth);
                  var ClientWidthSpec = MeasureSpec.MakeMeasureSpec(ClientWidth, MeasureSpecMode.Exactly);
                  foreach (var Client in ClientList.Where(C => C.MeasuredWidth != ClientWidth))
                  {
                    var ExistingItem = ToMeasureList.FirstOrDefault(R => R.Element == Client);

                    if (ExistingItem == null)
                      ToMeasureList.Add(new MeasureableElement { Element = Client, WidthMeasureSpec = ClientWidthSpec, HeightMeasureSpec = ElementHeightSpec });
                    else
                      ExistingItem.WidthMeasureSpec = ClientWidthSpec;
                  }
                }

                foreach (var Element in ToMeasureList)
                  Element.Measure();
                break;

              case MeasureSpecMode.Unspecified:
                foreach (var Element in ElementList)
                  Element.Measure(WidthMeasureSpec, ElementHeightSpec);

                if (ClientList.Count > 1)
                {
                  var ClientWidth = ClientList.Max(C => C.MeasuredWidth);
                  var ClientWidthSpec = MeasureSpec.MakeMeasureSpec(ClientWidth, MeasureSpecMode.Exactly);

                  foreach (var Client in ClientList.Where(C => C.MeasuredWidth != ClientWidth))
                    Client.Measure(ClientWidthSpec, ElementHeightSpec);
                }
                break;

              default:
                throw new UnhandledEnumCaseException<MeasureSpecMode>();
            }

            var HorizontalSummedWidth = ElementList.Sum(E => E.MeasuredWidth) + HorizontalPadding;
            var HorizontalMaxedHeight = ElementList.Max(E => E.MeasuredHeight) + VerticalPadding;

            var HorizontalRequestedWidth = WidthMode == MeasureSpecMode.Exactly ? MeasuredWidth : WidthMode == MeasureSpecMode.AtMost ? Math.Min(HorizontalSummedWidth, MeasuredWidth) : HorizontalSummedWidth;
            var HorizontalRequestedHeight = HeightMode == MeasureSpecMode.Exactly ? MeasuredHeight : HeightMode == MeasureSpecMode.AtMost ? Math.Min(HorizontalMaxedHeight, MeasuredHeight) : HorizontalMaxedHeight;
            SetMeasuredDimension(HorizontalRequestedWidth, HorizontalRequestedHeight);
            break;

          case Orientation.Vertical:
            var ElementWidthSpec = MeasureSpec.MakeMeasureSpec(Math.Max(0, MeasuredWidth - HorizontalPadding), WidthMode);

            switch (HeightMode)
            {
              case MeasureSpecMode.AtMost:
              case MeasureSpecMode.Exactly:
                var RemainingHeight = Math.Max(0, MeasuredHeight - VerticalPadding);

                foreach (var Element in HeaderList.Union(FooterList))
                {
                  Element.Measure(ElementWidthSpec, MeasureSpec.MakeMeasureSpec(RemainingHeight, MeasureSpecMode.AtMost));
                  RemainingHeight -= Element.MeasuredHeight;
                  if (RemainingHeight < 0)
                    RemainingHeight = 0;
                }

                if (ClientList.Any())
                {
                  var ClientHeightSpec = MeasureSpec.MakeMeasureSpec(RemainingHeight / ClientList.Count, HeightMode);

                  foreach (var Client in ClientList)
                    Client.Measure(ElementWidthSpec, ClientHeightSpec);
                }

                var ToMeasureList = new DistinctList<MeasureableElement>();

                if (WidthMode != MeasureSpecMode.Exactly)
                {
                  var ElementWidth = ElementList.Max(E => E.MeasuredWidth);
                  ElementWidthSpec = MeasureSpec.MakeMeasureSpec(ElementWidth, MeasureSpecMode.Exactly);

                  ToMeasureList.AddRange(ElementList.Where(E => E.MeasuredWidth != ElementWidth).Select(E => new MeasureableElement
                  {
                    Element = E,
                    WidthMeasureSpec = ElementWidthSpec
                  }));
                }

                if (HeightMode == MeasureSpecMode.AtMost && ClientList.Count > 1)
                {
                  var ClientHeight = ClientList.Max(C => C.MeasuredHeight);
                  var ClientHeightSpec = MeasureSpec.MakeMeasureSpec(ClientHeight, MeasureSpecMode.Exactly);
                  foreach (var Client in ClientList.Where(C => C.MeasuredHeight != ClientHeight))
                  {
                    var ExistingItem = ToMeasureList.FirstOrDefault(R => R.Element == Client);

                    if (ExistingItem == null)
                      ToMeasureList.Add(new MeasureableElement { Element = Client, WidthMeasureSpec = ElementWidthSpec, HeightMeasureSpec = ClientHeightSpec });
                    else
                      ExistingItem.HeightMeasureSpec = ClientHeightSpec;
                  }
                }

                foreach (var Element in ToMeasureList)
                  Element.Measure();
                break;

              case MeasureSpecMode.Unspecified:
                foreach (var Element in ElementList)
                  Element.Measure(ElementWidthSpec, HeightMeasureSpec);

                if (ClientList.Count > 1)
                {
                  var ClientHeight = ClientList.Max(C => C.MeasuredHeight);
                  var ClientHeightSpec = MeasureSpec.MakeMeasureSpec(ClientHeight, MeasureSpecMode.Exactly);

                  foreach (var Client in ClientList.Where(C => C.MeasuredHeight != ClientHeight))
                    Client.Measure(ElementWidthSpec, ClientHeightSpec);
                }
                break;

              default:
                throw new UnhandledEnumCaseException<MeasureSpecMode>();
            }

            var VerticalMaxedWidth = ElementList.Max(E => E.MeasuredWidth) + HorizontalPadding;
            var VerticalSummedHeight = ElementList.Sum(E => E.MeasuredHeight) + VerticalPadding;

            var VerticalRequestedWidth = WidthMode == MeasureSpecMode.Exactly ? MeasuredWidth : WidthMode == MeasureSpecMode.AtMost ? Math.Min(VerticalMaxedWidth, MeasuredWidth) : VerticalMaxedWidth;
            var VerticalRequestedHeight = HeightMode == MeasureSpecMode.Exactly ? MeasuredHeight : HeightMode == MeasureSpecMode.AtMost ? Math.Min(VerticalSummedHeight, MeasuredHeight) : VerticalSummedHeight;
            SetMeasuredDimension(VerticalRequestedWidth, VerticalRequestedHeight);
            break;

          default:
            throw new UnhandledEnumCaseException<Orientation>();
        }
      }
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      var DockWidth = Right - Left;
      var DockHeight = Bottom - Top;

      switch (Orientation)
      {
        case Orientation.Horizontal:
          var PanelLeft = PaddingLeft;
          var HeaderWidth = 0;

          var TopEdge = PaddingTop;
          if (TopEdge > DockHeight)
            TopEdge = DockHeight;

          var BottomEdge = DockHeight - PaddingBottom;
          if (BottomEdge < 0)
            BottomEdge = DockHeight;

          foreach (var Header in HeaderList)
          {
            Header.LayoutChild(PanelLeft, TopEdge, PanelLeft + Header.MeasuredWidth, BottomEdge);
            PanelLeft += Header.MeasuredWidth;
            HeaderWidth += Header.MeasuredWidth;
          }

          var FooterWidth = FooterList.Sum(F => F.MeasuredWidth);
          var RemainderWidth = DockWidth - HeaderWidth - FooterWidth - PaddingLeft - PaddingRight;

          if (ClientList.Count > 0)
          {
            var SharedWidth = RemainderWidth < 0 ? 0 : RemainderWidth / ClientList.Count;

            var LastClient = ClientList.Last();

            foreach (var Client in ClientList)
            {
              var FrameWidth = SharedWidth;
              if (RemainderWidth > 0 && Client == LastClient)
                FrameWidth += RemainderWidth - (SharedWidth * ClientList.Count);

              Client.LayoutChild(PanelLeft, TopEdge, PanelLeft + FrameWidth, BottomEdge);
              PanelLeft += FrameWidth;
            }
          }

          var PanelRight = (RemainderWidth < 0 ? DockWidth - RemainderWidth : DockWidth) - PaddingRight;

          foreach (var Footer in FooterList)
          {
            PanelRight -= Footer.MeasuredWidth;
            Footer.LayoutChild(PanelRight, TopEdge, PanelRight + Footer.MeasuredWidth, BottomEdge);
          }
          break;

        case Orientation.Vertical:
          var PanelTop = PaddingTop;
          var HeaderHeight = 0;

          var LeftEdge = PaddingLeft;
          if (LeftEdge > DockWidth)
            LeftEdge = DockWidth;

          var RightEdge = DockWidth - PaddingRight;
          if (RightEdge < 0)
            RightEdge = DockWidth;

          foreach (var Header in HeaderList)
          {
            Header.LayoutChild(LeftEdge, PanelTop, RightEdge, PanelTop + Header.MeasuredHeight);
            PanelTop += Header.MeasuredHeight;
            HeaderHeight += Header.MeasuredHeight;
          }

          var FooterHeight = FooterList.Sum(F => F.MeasuredHeight);
          var RemainderHeight = DockHeight - HeaderHeight - FooterHeight - PaddingTop - PaddingBottom;

          if (ClientList.Count > 0)
          {
            var SharedHeight = RemainderHeight < 0 ? 0 : RemainderHeight / ClientList.Count;
            var LastClient = ClientList.Last();

            foreach (var Client in ClientList)
            {
              var FrameHeight = SharedHeight;
              if (RemainderHeight > 0 && Client == LastClient)
                FrameHeight += RemainderHeight - (SharedHeight * ClientList.Count);

              Client.LayoutChild(LeftEdge, PanelTop, RightEdge, PanelTop + FrameHeight);
              PanelTop += FrameHeight;
            }
          }

          var PanelBottom = (RemainderHeight < 0 ? DockHeight - RemainderHeight : DockHeight) - PaddingBottom;

          foreach (var Footer in FooterList)
          {
            PanelBottom -= Footer.MeasuredHeight;
            Footer.LayoutChild(LeftEdge, PanelBottom, RightEdge, PanelBottom + Footer.MeasuredHeight);
          }
          break;

        default:
          throw new UnhandledEnumCaseException<Orientation>();
      }
    }

    private Inv.DistinctList<Android.Views.View> ElementList;
    private Inv.DistinctList<Android.Views.View> HeaderList;
    private Inv.DistinctList<Android.Views.View> ClientList;
    private Inv.DistinctList<Android.Views.View> FooterList;

    private sealed class MeasureableElement
    {
      public void Measure()
      {
        Element.Measure(WidthMeasureSpec ?? MeasureSpec.MakeMeasureSpec(Element.MeasuredWidth, MeasureSpecMode.Exactly), HeightMeasureSpec ?? MeasureSpec.MakeMeasureSpec(Element.MeasuredHeight, MeasureSpecMode.Exactly));
      }

      public Android.Views.View Element { get; set; }
      public int? WidthMeasureSpec { get; set; }
      public int? HeightMeasureSpec { get; set; }
    }
  }

  internal sealed class AndroidLayoutStack : AndroidLayoutGroup
  {
    public AndroidLayoutStack(Android.Content.Context context)
      : base(context)
    {
      this.ElementArray = new Android.Views.View[] { };
    }

    public Orientation Orientation { get; set; }

    internal void ComposeElements(IEnumerable<Android.Views.View> Elements)
    {
      var PreviousArray = ElementArray;
      this.ElementArray = Elements.ToArray();

      foreach (var Element in ElementArray.Except(PreviousArray))
        this.SafeAddView(Element);

      foreach (var Element in PreviousArray.Except(ElementArray))
        this.SafeRemoveView(Element);

      this.Arrange();
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      if (ElementArray.Length == 0)
      {
        int RequestedWidth;
        int RequestedHeight;
        this.MeasureOnlyChild(null, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);
        SetMeasuredDimension(RequestedWidth, RequestedHeight);
      }
      else
      {
        var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
        var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);
        var HorizontalPadding = PaddingLeft + PaddingRight;
        var VerticalPadding = PaddingTop + PaddingBottom;

        switch (Orientation)
        {
          case Orientation.Horizontal:
            var ElementHeightSpec = MeasureSpec.MakeMeasureSpec(Math.Max(0, MeasuredHeight - VerticalPadding), HeightMode);

            switch (WidthMode)
            {
              case MeasureSpecMode.AtMost:
              case MeasureSpecMode.Exactly:
                var RemainingWidth = Math.Max(0, MeasuredWidth - HorizontalPadding);

                foreach (var Element in ElementArray)
                {
                  Element.Measure(MeasureSpec.MakeMeasureSpec(RemainingWidth, MeasureSpecMode.AtMost), ElementHeightSpec);
                  RemainingWidth -= Element.MeasuredWidth;
                  if (RemainingWidth < 0)
                    RemainingWidth = 0;
                }
                break;

              case MeasureSpecMode.Unspecified:
                var WidthSpec = MeasureSpec.MakeMeasureSpec(Math.Max(0, MeasuredWidth - HorizontalPadding), WidthMode);
                foreach (var Element in ElementArray)
                  Element.Measure(WidthSpec, ElementHeightSpec);
                break;

              default:
                throw new UnhandledEnumCaseException<MeasureSpecMode>();
            }

            if (HeightMode != MeasureSpecMode.Exactly)
            {
              var ElementHeight = ElementArray.Max(E => E.MeasuredHeight);
              ElementHeightSpec = MeasureSpec.MakeMeasureSpec(ElementHeight, MeasureSpecMode.Exactly);

              foreach (var Element in ElementArray.Where(E => E.MeasuredHeight != ElementHeight))
                Element.Measure(MeasureSpec.MakeMeasureSpec(Element.MeasuredWidth, MeasureSpecMode.Exactly), ElementHeightSpec);
            }

            var HorizontalSummedWidth = ElementArray.Sum(E => E.MeasuredWidth) + HorizontalPadding;
            var HorizontalMaxedHeight = ElementArray.Max(E => E.MeasuredHeight) + VerticalPadding;

            var HorizontalRequestedWidth = WidthMode == MeasureSpecMode.Exactly ? MeasuredWidth : WidthMode == MeasureSpecMode.AtMost ? Math.Min(HorizontalSummedWidth, MeasuredWidth) : HorizontalSummedWidth;
            var HorizontalRequestedHeight = HeightMode == MeasureSpecMode.Exactly ? MeasuredHeight : HeightMode == MeasureSpecMode.AtMost ? Math.Min(HorizontalMaxedHeight, MeasuredHeight) : HorizontalMaxedHeight;
            SetMeasuredDimension(HorizontalRequestedWidth, HorizontalRequestedHeight);
            break;

          case Orientation.Vertical:
            var ElementWidthSpec = MeasureSpec.MakeMeasureSpec(Math.Max(0, MeasuredWidth - HorizontalPadding), WidthMode);

            switch (HeightMode)
            {
              case MeasureSpecMode.AtMost:
              case MeasureSpecMode.Exactly:
                var RemainingHeight = Math.Max(0, MeasuredHeight - VerticalPadding);

                foreach (var Element in ElementArray)
                {
                  Element.Measure(ElementWidthSpec, MeasureSpec.MakeMeasureSpec(RemainingHeight, MeasureSpecMode.AtMost));
                  RemainingHeight -= Element.MeasuredHeight;
                  if (RemainingHeight < 0)
                    RemainingHeight = 0;
                }
                break;

              case MeasureSpecMode.Unspecified:
                var HeightSpec = MeasureSpec.MakeMeasureSpec(Math.Max(0, MeasuredHeight - VerticalPadding), HeightMode);
                foreach (var Element in ElementArray)
                  Element.Measure(ElementWidthSpec, HeightSpec);
                break;

              default:
                throw new UnhandledEnumCaseException<MeasureSpecMode>();
            }

            if (WidthMode != MeasureSpecMode.Exactly)
            {
              var ElementWidth = ElementArray.Max(E => E.MeasuredWidth);
              ElementWidthSpec = MeasureSpec.MakeMeasureSpec(ElementWidth, MeasureSpecMode.Exactly);

              foreach (var Element in ElementArray.Where(E => E.MeasuredWidth != ElementWidth))
                Element.Measure(ElementWidthSpec, MeasureSpec.MakeMeasureSpec(Element.MeasuredHeight, MeasureSpecMode.Exactly));
            }

            var VerticalMaxedWidth = ElementArray.Max(E => E.MeasuredWidth) + HorizontalPadding;
            var VerticalSummedHeight = ElementArray.Sum(E => E.MeasuredHeight) + VerticalPadding;

            var VerticalRequestedWidth = WidthMode == MeasureSpecMode.Exactly ? MeasuredWidth : WidthMode == MeasureSpecMode.AtMost ? Math.Min(VerticalMaxedWidth, MeasuredWidth) : VerticalMaxedWidth;
            var VerticalRequestedHeight = HeightMode == MeasureSpecMode.Exactly ? MeasuredHeight : HeightMode == MeasureSpecMode.AtMost ? Math.Min(VerticalSummedHeight, MeasuredHeight) : VerticalSummedHeight;
            SetMeasuredDimension(VerticalRequestedWidth, VerticalRequestedHeight);
            break;

          default:
            throw new UnhandledEnumCaseException<Orientation>();
        }
      }
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      switch (Orientation)
      {
        case Orientation.Horizontal:
          var StackHeight = Bottom - Top;

          var EdgeTop = PaddingTop;
          if (EdgeTop > StackHeight)
            EdgeTop = StackHeight;

          var EdgeBottom = StackHeight - PaddingBottom;
          if (EdgeBottom < 0)
            EdgeBottom = StackHeight;

          var ElementLeft = PaddingLeft;
          foreach (var Element in ElementArray)
          {
            Element.LayoutChild(ElementLeft, EdgeTop, ElementLeft + Element.MeasuredWidth, EdgeBottom);
            ElementLeft += Element.MeasuredWidth;
          }
          break;

        case Orientation.Vertical:
          var StackWidth = Right - Left;

          var EdgeLeft = PaddingLeft;
          if (EdgeLeft > StackWidth)
            EdgeLeft = StackWidth;

          var EdgeRight = StackWidth - PaddingRight;
          if (EdgeRight < 0)
            EdgeRight = StackWidth;

          var ElementTop = PaddingTop;
          foreach (var Element in ElementArray)
          {
            Element.LayoutChild(EdgeLeft, ElementTop, EdgeRight, ElementTop + Element.MeasuredHeight);
            ElementTop += Element.MeasuredHeight;
          }
          break;

        default:
          throw new UnhandledEnumCaseException<Orientation>();
      }

      //Border.Layout();
    }

    private Android.Views.View[] ElementArray;
  }

  internal sealed class AndroidLayoutTable : AndroidLayoutGroup
  {
    public AndroidLayoutTable(Android.Content.Context context)
      : base(context)
    {
      this.RowList = new DistinctList<AndroidLayoutTableRow>();
      this.ColumnList = new DistinctList<AndroidLayoutTableColumn>();
      this.CellList = new DistinctList<AndroidLayoutTableCell>();
      this.ElementList = new DistinctList<Android.Views.View>();
    }

    public void ComposeElements(IEnumerable<AndroidLayoutTableRow> Rows, IEnumerable<AndroidLayoutTableColumn> Columns, IEnumerable<AndroidLayoutTableCell> Cells)
    {
      this.RowList.Clear();
      RowList.AddRange(Rows);

      this.ColumnList.Clear();
      ColumnList.AddRange(Columns);

      this.CellList.Clear();
      CellList.AddRange(Cells);

      var PreviousElementList = ElementList;
      this.ElementList = new DistinctList<Android.Views.View>(RowList.Count + ColumnList.Count + CellList.Count);
      ElementList.AddRange(RowList.Select(R => R.Element).ExceptNull());
      ElementList.AddRange(ColumnList.Select(R => R.Element).ExceptNull());
      ElementList.AddRange(CellList.Select(R => R.Element).ExceptNull());

      foreach (var Element in ElementList.Except(PreviousElementList))
        this.SafeAddView(Element);

      foreach (var Header in PreviousElementList.Except(ElementList))
        this.SafeRemoveView(Header);

      this.Arrange();
    }
    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
      var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);
      var WidthSize = MeasureSpec.GetSize(WidthMeasureSpec);
      var HeightSize = MeasureSpec.GetSize(HeightMeasureSpec);

      var AvailableWidth = MeasuredWidth - PaddingLeft - PaddingRight;
      var AvailableHeight = MeasuredHeight - PaddingTop - PaddingBottom;

      var WidestRowWidth = AvailableWidth;
      var TallestColumnHeight = AvailableHeight;

      var DynamicRowList = RowList.Where(R => R.Length.IsStar).ToDistinctList();
      var DynamicColumnList = ColumnList.Where(C => C.Length.IsStar).ToDistinctList();

      var DynamicRowWeight = DynamicRowList.Sum(R => R.Length.Size ?? 1);
      var DynamicColumnWeight = DynamicColumnList.Sum(C => C.Length.Size ?? 1);

      // Fixed lengths
      foreach (var Row in RowList)
      {
        Row.TempLength = -1;

        if (Row.Length.IsFixed)
        {
          Row.TempLength = Row.Length.Size ?? 0;

          if (Row.Element != null)
          {
            Row.Element.Measure(MeasureSpec.MakeMeasureSpec(WidestRowWidth, WidthMode), MeasureSpec.MakeMeasureSpec(Row.TempLength, MeasureSpecMode.Exactly));
            if (Row.Element.MeasuredWidth > WidestRowWidth)
              WidestRowWidth = Row.Element.MeasuredWidth;
          }
        }
      }

      foreach (var Column in ColumnList)
      {
        Column.TempLength = -1;

        if (Column.Length.IsFixed)
        {
          Column.TempLength = Column.Length.Size ?? 0;

          if (Column.Element != null)
          {
            Column.Element.Measure(MeasureSpec.MakeMeasureSpec(Column.TempLength, MeasureSpecMode.Exactly), MeasureSpec.MakeMeasureSpec(TallestColumnHeight, HeightMode));
            if (Column.Element.MeasuredHeight > TallestColumnHeight)
              TallestColumnHeight = Column.Element.MeasuredHeight;
          }
        }
      }

      var RemainingWidth = AvailableWidth - ColumnList.Where(C => C.TempLength != -1).Sum(C => C.TempLength);
      var RemainingHeight = AvailableHeight - RowList.Where(R => R.TempLength != -1).Sum(R => R.TempLength);

      // Auto lengths
      foreach (var Row in RowList.Where(R => R.Length.IsAuto))
      {
        var RowWidth = 0;
        var RowHeight = 0;

        var RowHeightMode = HeightMode == MeasureSpecMode.Exactly ? MeasureSpecMode.AtMost : HeightMode;
        var DefaultWidthMode = WidthMode == MeasureSpecMode.Exactly ? MeasureSpecMode.AtMost : WidthMode;

        if (Row.Element != null)
        {
          Row.Element.Measure(MeasureSpec.MakeMeasureSpec(WidestRowWidth, WidthMode), MeasureSpec.MakeMeasureSpec(RowHeight == 0 ? RemainingHeight : RowHeight, RowHeightMode));
          RowWidth = Row.Element.MeasuredWidth;
          RowHeight = Row.Element.MeasuredHeight;
        }

        var RowCellList = CellList.Where(C => C.Row == Row && C.Element != null).ToDistinctList();
        var UsedWidth = 0;

        foreach (var Cell in RowCellList)
        {
          Cell.Element.Measure(
            Cell.Column.TempLength == -1 ? MeasureSpec.MakeMeasureSpec(Math.Max(RemainingWidth - UsedWidth, 0), DefaultWidthMode) : MeasureSpec.MakeMeasureSpec(Cell.Column.TempLength, MeasureSpecMode.Exactly),
            MeasureSpec.MakeMeasureSpec(RemainingHeight - RowHeight, RowHeightMode)
          );
          UsedWidth += Cell.Element.MeasuredWidth;
          if (Cell.Element.MeasuredHeight > RowHeight)
            RowHeight = Cell.Element.MeasuredHeight;
        }

        RowWidth = Math.Max(RowWidth, RowCellList.Sum(C => C.Element.MeasuredWidth));
        var RowHeightMeasureSpec = MeasureSpec.MakeMeasureSpec(RowHeight, MeasureSpecMode.Exactly);

        if (Row.Element != null && Row.Element.MeasuredWidth < RowWidth)
          Row.Element.Measure(MeasureSpec.MakeMeasureSpec(RowWidth, MeasureSpecMode.Exactly), RowHeightMeasureSpec);

        foreach (var Cell in RowCellList.Where(C => C.Element.MeasuredHeight < RowHeight))
          Cell.Element.Measure(MeasureSpec.MakeMeasureSpec(Cell.Element.MeasuredWidth, MeasureSpecMode.Exactly), RowHeightMeasureSpec);

        if (RowWidth > WidestRowWidth)
          WidestRowWidth = RowWidth;

        Row.TempLength = RowHeight;
        RemainingHeight -= RowHeight;
      }

      foreach (var Column in ColumnList.Where(C => C.Length.IsAuto))
      {
        var ColumnWidth = 0;
        var ColumnHeight = 0;

        var ColumnWidthMode = WidthMode == MeasureSpecMode.Exactly ? MeasureSpecMode.AtMost : WidthMode;
        var DefaultHeightMode = HeightMode == MeasureSpecMode.Exactly ? MeasureSpecMode.AtMost : HeightMode;

        if (Column.Element != null)
        {
          Column.Element.Measure(MeasureSpec.MakeMeasureSpec(ColumnWidth == 0 ? RemainingWidth : ColumnWidth, ColumnWidthMode), MeasureSpec.MakeMeasureSpec(TallestColumnHeight, HeightMode));
          ColumnWidth = Column.Element.MeasuredWidth;
          ColumnHeight = Column.Element.MeasuredHeight;
        }

        var ColumnCellList = CellList.Where(C => C.Column == Column && C.Element != null).ToDistinctList();
        var UsedHeight = 0;

        foreach (var Cell in ColumnCellList)
        {
          Cell.Element.Measure(
            MeasureSpec.MakeMeasureSpec(RemainingWidth - ColumnWidth, ColumnWidthMode),
            Cell.Row.TempLength == -1 ? MeasureSpec.MakeMeasureSpec(Math.Max(RemainingHeight - UsedHeight, 0), DefaultHeightMode) : MeasureSpec.MakeMeasureSpec(Cell.Row.TempLength, MeasureSpecMode.Exactly)
          );
          UsedHeight += Cell.Element.MeasuredHeight;
          if (Cell.Element.MeasuredWidth > ColumnWidth)
            ColumnWidth = Cell.Element.MeasuredWidth;
        }

        ColumnHeight = Math.Max(ColumnHeight, ColumnCellList.Sum(C => C.Element.MeasuredHeight));
        var ColumnWidthMeasureSpec = MeasureSpec.MakeMeasureSpec(ColumnWidth, MeasureSpecMode.Exactly);

        if (Column.Element != null && Column.Element.MeasuredHeight < ColumnHeight)
          Column.Element.Measure(ColumnWidthMeasureSpec, MeasureSpec.MakeMeasureSpec(ColumnHeight, MeasureSpecMode.Exactly));

        foreach (var Cell in ColumnCellList.Where(C => C.Element.MeasuredWidth < ColumnWidth))
          Cell.Element.Measure(ColumnWidthMeasureSpec, MeasureSpec.MakeMeasureSpec(Cell.Element.MeasuredHeight, MeasureSpecMode.Exactly));

        if (ColumnHeight > TallestColumnHeight)
          TallestColumnHeight = ColumnHeight;

        Column.TempLength = ColumnWidth;
        RemainingWidth -= ColumnWidth;
      }

      var DynamicRowFactor = DynamicRowWeight == 0 ? 0f : RemainingHeight / (float)DynamicRowWeight;
      var DynamicColumnFactor = DynamicColumnWeight == 0 ? 0f : RemainingWidth / (float)DynamicColumnWeight;

      // Star lengths
      foreach (var Row in DynamicRowList)
      {
        var RowWidth = 0;
        var RowHeight = (int)(DynamicRowFactor * (Row.Length.Size ?? 1));
        var RowHeightMode = HeightMode == MeasureSpecMode.Exactly ? MeasureSpecMode.AtMost : HeightMode;
        var DefaultRowWidthMode = WidthMode == MeasureSpecMode.Exactly ? MeasureSpecMode.AtMost : WidthMode;

        if (Row.Element != null)
        {
          Row.Element.Measure(MeasureSpec.MakeMeasureSpec(WidestRowWidth, WidthMode), MeasureSpec.MakeMeasureSpec(RowHeight, RowHeightMode));
          RowWidth = Row.Element.MeasuredWidth;
          RowHeight = Row.Element.MeasuredHeight;
        }

        var RowCellList = CellList.Where(C => C.Row == Row && C.Element != null).ToDistinctList();
        var UsedWidth = 0;

        foreach (var Cell in RowCellList)
        {
          Cell.Element.Measure(
            Cell.Column.TempLength == -1 ? MeasureSpec.MakeMeasureSpec(Math.Max(RemainingWidth - UsedWidth, 0), DefaultRowWidthMode) : MeasureSpec.MakeMeasureSpec(Cell.Column.TempLength, MeasureSpecMode.Exactly),
            MeasureSpec.MakeMeasureSpec(RowHeight, RowHeightMode)
          );
          UsedWidth += Cell.Element.MeasuredWidth;
          if (Cell.Element.MeasuredHeight > RowHeight)
            RowHeight = Cell.Element.MeasuredHeight;
        }

        RowWidth = Math.Max(RowWidth, RowCellList.Sum(C => C.Element.MeasuredWidth));
        var RowHeightMeasureSpec = MeasureSpec.MakeMeasureSpec(RowHeight, MeasureSpecMode.Exactly);

        if (Row.Element != null && Row.Element.MeasuredWidth < RowWidth)
          Row.Element.Measure(MeasureSpec.MakeMeasureSpec(RowWidth, MeasureSpecMode.Exactly), RowHeightMeasureSpec);

        foreach (var Cell in RowCellList.Where(C => C.Element.MeasuredHeight < RowHeight))
          Cell.Element.Measure(MeasureSpec.MakeMeasureSpec(Cell.Element.MeasuredWidth, MeasureSpecMode.Exactly), RowHeightMeasureSpec);

        if (RowWidth > WidestRowWidth)
          WidestRowWidth = RowWidth;

        Row.TempLength = RowHeight;
        RemainingHeight -= RowHeight;
      }

      foreach (var Column in DynamicColumnList)
      {
        var ColumnWidth = (int)(DynamicColumnFactor * (Column.Length.Size ?? 1));
        var ColumnHeight = 0;
        var ColumnWidthMode = WidthMode == MeasureSpecMode.Exactly ? MeasureSpecMode.AtMost : WidthMode;

        if (Column.Element != null)
        {
          Column.Element.Measure(MeasureSpec.MakeMeasureSpec(ColumnWidth, ColumnWidthMode), MeasureSpec.MakeMeasureSpec(TallestColumnHeight, HeightMode));
          ColumnWidth = Column.Element.MeasuredWidth;
          ColumnHeight = Column.Element.MeasuredHeight;
        }

        var ColumnCellList = CellList.Where(C => C.Column == Column && C.Element != null).ToDistinctList();

        foreach (var Cell in ColumnCellList)
        {
          Cell.Element.Measure(
            MeasureSpec.MakeMeasureSpec(ColumnWidth, ColumnWidthMode),
            MeasureSpec.MakeMeasureSpec(Cell.Row.TempLength, MeasureSpecMode.Exactly)
          );
          if (Cell.Element.MeasuredWidth > ColumnWidth)
            ColumnWidth = Cell.Element.MeasuredWidth;
        }

        ColumnHeight = Math.Max(ColumnHeight, ColumnCellList.Sum(C => C.Element.MeasuredHeight));
        var ColumnWidthMeasureSpec = MeasureSpec.MakeMeasureSpec(ColumnWidth, MeasureSpecMode.Exactly);

        if (Column.Element != null && Column.Element.MeasuredHeight < ColumnHeight)
          Column.Element.Measure(ColumnWidthMeasureSpec, MeasureSpec.MakeMeasureSpec(ColumnHeight, MeasureSpecMode.Exactly));

        foreach (var Cell in ColumnCellList.Where(C => C.Element.MeasuredWidth < ColumnWidth))
          Cell.Element.Measure(ColumnWidthMeasureSpec, MeasureSpec.MakeMeasureSpec(Cell.Element.MeasuredHeight, MeasureSpecMode.Exactly));

        if (ColumnHeight > TallestColumnHeight)
          TallestColumnHeight = ColumnHeight;

        Column.TempLength = ColumnWidth;
      }

      var FinalWidth = ColumnList.Sum(C => C.TempLength);
      var FinalHeight = RowList.Sum(R => R.TempLength);

      var Offset = 0;
      foreach (var Row in RowList)
      {
        Row.TempStart = Offset;
        Offset += Row.TempLength;
        if (Row.Element != null && (Row.Element.MeasuredWidth != FinalWidth || Row.Element.MeasuredHeight != Row.TempLength))
          Row.Element.Measure(MeasureSpec.MakeMeasureSpec(FinalWidth, MeasureSpecMode.Exactly), MeasureSpec.MakeMeasureSpec(Row.TempLength, MeasureSpecMode.Exactly));
      }

      Offset = 0;
      foreach (var Column in ColumnList)
      {
        Column.TempStart = Offset;
        Offset += Column.TempLength;
        if (Column.Element != null && (Column.Element.MeasuredWidth != Column.TempLength || Column.Element.MeasuredHeight != FinalHeight))
          Column.Element.Measure(MeasureSpec.MakeMeasureSpec(Column.TempLength, MeasureSpecMode.Exactly), MeasureSpec.MakeMeasureSpec(FinalHeight, MeasureSpecMode.Exactly));
      }

      foreach (var Cell in CellList)
        if (Cell.Element != null && (Cell.Element.MeasuredWidth != Cell.Column.TempLength || Cell.Element.MeasuredHeight != Cell.Row.TempLength))
          Cell.Element.Measure(MeasureSpec.MakeMeasureSpec(Cell.Column.TempLength, MeasureSpecMode.Exactly), MeasureSpec.MakeMeasureSpec(Cell.Row.TempLength, MeasureSpecMode.Exactly));

      if (WidthMode == MeasureSpecMode.Exactly && FinalWidth != WidthSize)
        FinalWidth = WidthSize;

      if (HeightMode == MeasureSpecMode.Exactly && FinalHeight != HeightSize)
        FinalHeight = HeightSize;

      if (FinalWidth != MeasuredWidth || FinalHeight != MeasuredHeight)
        SetMeasuredDimension(FinalWidth, FinalHeight);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      var ColumnTop = PaddingTop;
      var ColumnBottom = Bottom - Top - PaddingBottom;
      var RowLeft = PaddingLeft;
      var RowRight = Right - Left - PaddingRight;

      foreach (var Column in ColumnList)
      {
        if (Column.Element != null)
          Column.Element.LayoutChild(Column.TempStart, ColumnTop, Column.TempStart + Column.TempLength, ColumnBottom);
      }

      foreach (var Row in RowList)
      {
        if (Row.Element != null)
          Row.Element.LayoutChild(RowLeft, Row.TempStart, RowRight, Row.TempStart + Row.TempLength);
      }

      foreach (var Cell in CellList)
      {
        if (Cell.Element != null)
          Cell.Element.LayoutChild(Cell.Column.TempStart, Cell.Row.TempStart, Cell.Column.TempStart + Cell.Column.TempLength, Cell.Row.TempStart + Cell.Row.TempLength);
      }
    }

    internal Inv.DistinctList<AndroidLayoutTableRow> RowList { get; private set; }
    internal Inv.DistinctList<AndroidLayoutTableColumn> ColumnList { get; private set; }
    internal Inv.DistinctList<AndroidLayoutTableCell> CellList { get; private set; }

    private DistinctList<Android.Views.View> ElementList;
  }

  internal sealed class AndroidLayoutTableLength
  {
    public AndroidLayoutTableLength(bool Fill, int? Value)
    {
      this.Fill = Fill;
      this.Size = Value;
    }

    public bool Fill { get; private set; }
    public int? Size { get; private set; }
    public bool IsStar
    {
      get { return Fill; }
    }
    public bool IsAuto
    {
      get { return !Fill && Size == null; }
    }
    public bool IsFixed
    {
      get { return !Fill && Size != null; }
    }
  }

  internal sealed class AndroidLayoutTableColumn
  {
    public AndroidLayoutTableColumn(AndroidLayoutTable Table, Android.Views.View Element, bool Fill, int? Value)
    {
      this.Table = Table;
      this.Element = Element;
      this.Length = new AndroidLayoutTableLength(Fill, Value);
    }

    public Android.Views.View Element { get; private set; }
    public AndroidLayoutTableLength Length { get; private set; }

    internal int TempStart;
    internal int TempLength;

    private AndroidLayoutTable Table;
  }

  internal sealed class AndroidLayoutTableRow
  {
    public AndroidLayoutTableRow(AndroidLayoutTable Table, Android.Views.View Element, bool Fill, int? Value)
    {
      this.Table = Table;
      this.Element = Element;
      this.Length = new AndroidLayoutTableLength(Fill, Value);
    }

    public Android.Views.View Element { get; private set; }
    public AndroidLayoutTableLength Length { get; private set; }

    internal int TempStart;
    internal int TempLength;

    private AndroidLayoutTable Table;
  }

  internal sealed class AndroidLayoutTableCell
  {
    public AndroidLayoutTableCell(AndroidLayoutTableColumn Column, AndroidLayoutTableRow Row, Android.Views.View Element)
    {
      this.Column = Column;
      this.Row = Row;
      this.Element = Element;
    }

    public AndroidLayoutTableColumn Column { get; private set; }
    public AndroidLayoutTableRow Row { get; private set; }
    public Android.Views.View Element { get; private set; }
  }

  internal sealed class AndroidLayoutBoard : AndroidLayoutGroup
  {
    public AndroidLayoutBoard(Android.Content.Context context)
      : base(context)
    {
      this.RecordList = new DistinctList<AndroidLayoutRecord>();
    }

    public void RemoveElements()
    {
      if (RecordList.Count > 0)
      {
        foreach (var Record in RecordList)
          RemoveView(Record.Element);
        RecordList.Clear();

        this.Arrange();
      }
    }
    public void AddElement(Android.Views.View Element, int Left, int Top, int Right, int Bottom)
    {
      RecordList.Add(new AndroidLayoutRecord()
      {
        Element = Element,
        Left = Left,
        Top = Top,
        Right = Right,
        Bottom = Bottom
      });
      this.SafeAddView(Element);

      this.Arrange();
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      if (!RecordList.Any())
      {
        int RequestedWidth;
        int RequestedHeight;
        this.MeasureOnlyChild(null, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);
        SetMeasuredDimension(RequestedWidth, RequestedHeight);
      }
      else
      {
        foreach (var Record in RecordList)
          Record.Element.Measure(MeasureSpec.MakeMeasureSpec(Record.Right - Record.Left, MeasureSpecMode.Exactly), MeasureSpec.MakeMeasureSpec(Record.Bottom - Record.Top, MeasureSpecMode.Exactly));

        var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
        var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);

        var MaxedRight = RecordList.Max(R => R.Right);
        var MaxedBottom = RecordList.Max(R => R.Bottom);

        var IdealWidth = Math.Max(SuggestedMinimumWidth, PaddingLeft + MaxedRight + PaddingRight);
        var IdealHeight = Math.Max(SuggestedMinimumHeight, PaddingTop + MaxedBottom + PaddingBottom);

        switch (WidthMode)
        {
          case MeasureSpecMode.AtMost:
            IdealWidth = Math.Min(IdealWidth, MeasuredWidth);
            break;

          case MeasureSpecMode.Exactly:
            IdealWidth = MeasuredWidth;
            break;

          case MeasureSpecMode.Unspecified:
            break;

          default:
            throw new UnhandledEnumCaseException<MeasureSpecMode>();
        }

        switch (HeightMode)
        {
          case MeasureSpecMode.AtMost:
            IdealHeight = Math.Min(IdealHeight, MeasuredHeight);
            break;

          case MeasureSpecMode.Exactly:
            IdealHeight = MeasuredHeight;
            break;

          case MeasureSpecMode.Unspecified:
            break;

          default:
            throw new UnhandledEnumCaseException<MeasureSpecMode>();
        }

        SetMeasuredDimension(IdealWidth, IdealHeight);
      }
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      foreach (var Record in RecordList)
        Record.Element.LayoutChild(PaddingLeft + Record.Left, PaddingTop + Record.Top, Record.Right - PaddingRight, Record.Bottom - PaddingBottom);
    }

    private Inv.DistinctList<AndroidLayoutRecord> RecordList;

    private sealed class AndroidLayoutRecord
    {
      public Android.Views.View Element;
      public int Left;
      public int Top;
      public int Right;
      public int Bottom;
    }
  }

/* // TODO: attempt to solve the corner radius clipping.
  internal sealed class AndroidLayoutGraphic : AndroidLayoutGroup
  {
    public AndroidLayoutGraphic(Android.Content.Context context)
      : base(context)
    {
      this.SetClipChildren(true); // TODO: necessary to clip the inner imageview?

      this.ImageView = new Android.Widget.ImageView(context);
      AddView(ImageView);
      // NOTE: both of these seem to work together to emulate the WPF 'uniform' scaling.
      ImageView.SetAdjustViewBounds(true);
      ImageView.SetScaleType(Android.Widget.ImageView.ScaleType.FitCenter);
    }

    public void SetImageBitmap(Bitmap Bitmap)
    {
      ImageView.SetImageBitmap(Bitmap);
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      int RequestedWidth, RequestedHeight;
      this.MeasureOnlyChild(ImageView, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      this.LayoutOnlyChild(ImageView, Left, Top, Right, Bottom);
    }

    private Android.Widget.ImageView ImageView;
  }
*/
  
  internal sealed class AndroidLayoutGraphic : Android.Widget.ImageView
  {
    public AndroidLayoutGraphic(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidLayoutBackground();

      // NOTE: both of these seem to work together to emulate the WPF 'uniform' scaling.
      SetAdjustViewBounds(true);
      SetScaleType(Android.Widget.ImageView.ScaleType.FitCenter);
    }

    public new AndroidLayoutBackground Background
    {
      get { return (AndroidLayoutBackground)base.Background; }
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      base.OnLayout(Changed, Left, Top, Right, Bottom);
    }
  }

  internal sealed class AndroidLayoutOverlay : AndroidLayoutGroup
  {
    public AndroidLayoutOverlay(Android.Content.Context context)
      : base(context)
    {
      this.ElementList = new DistinctList<Android.Views.View>();
    }

    public void ComposeElements(IEnumerable<Android.Views.View> Elements)
    {
      var PreviousList = ElementList;
      this.ElementList = Elements.ToDistinctList();

      foreach (var Element in ElementList.Except(PreviousList))
        this.SafeAddView(Element);

      foreach (var Element in PreviousList.Except(ElementList))
        this.SafeRemoveView(Element);

      this.Arrange();
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      var RequestedWidth = 0;
      var RequestedHeight = 0;

      if (ElementList.Any())
      {
        var FinalWidth = RequestedWidth;
        var FinalHeight = RequestedHeight;

        foreach (var Element in ElementList)
        {
          this.MeasureOnlyChild(Element, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);

          if (RequestedWidth > FinalWidth)
            FinalWidth = RequestedWidth;

          if (RequestedHeight > FinalHeight)
            FinalHeight = RequestedHeight;
        }

        // now we know the max width/height, we need to remeasure everyone.
        var RemeasureWidthSpec = MeasureSpec.MakeMeasureSpec(FinalWidth, MeasureSpecMode.Exactly);
        var RemeasureHeightSpec = MeasureSpec.MakeMeasureSpec(FinalHeight, MeasureSpecMode.Exactly);
        foreach (var Element in ElementList)
          this.MeasureOnlyChild(Element, RemeasureWidthSpec, RemeasureHeightSpec, out RequestedWidth, out RequestedHeight);

        RequestedWidth = FinalWidth;
        RequestedHeight = FinalHeight;
      }
      else
      {
        this.MeasureOnlyChild(null, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);
      }

      SetMeasuredDimension(RequestedWidth, RequestedHeight);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      foreach (var Element in ElementList)
        this.LayoutOnlyChild(Element, Left, Top, Right, Bottom);
    }

    private Inv.DistinctList<Android.Views.View> ElementList;
  }

  internal sealed class AndroidLayoutFrame : AndroidLayoutGroup
  {
    public AndroidLayoutFrame(Android.Content.Context context)
      : base(context)
    {
    }

    public void SetContentElement(Android.Views.View Element)
    {
      if (this.ContentElement != Element)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement);

        this.ContentElement = Element;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement);

        this.Arrange();
      }
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      int RequestedWidth, RequestedHeight;
      this.MeasureOnlyChild(ContentElement, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      this.LayoutOnlyChild(ContentElement, Left, Top, Right, Bottom);
    }

    private Android.Views.View ContentElement;
  }

  internal sealed class AndroidLayoutFlow : AndroidLayoutGroup
  {
    public AndroidLayoutFlow(Android.Content.Context Context, Configuration Configuration)
      : base(Context)
    {
      this.SetClipChildren(true);

      this.Adapter = new FlowAdapter(Configuration);
      
      this.ListView = new Android.Widget.ListView(Context);
      ListView.Adapter = Adapter;
      ListView.LayoutParameters = new LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);
      ListView.Divider = null;
      ListView.DividerHeight = 0;
      ListView.SetSelector(Android.Resource.Color.Transparent);
      AddView(ListView);
      
      this.Arrange();
    }

    public bool IsRefreshable
    {
      get 
      { 
#if ANDROID_SUPPORT
        return RefreshLayout != null; 
#else
        return false;
#endif
      }
      set
      {
        #if ANDROID_SUPPORT
        if (value != IsRefreshable)
        {
          if (value)
          {
            RemoveView(ListView);

            this.RefreshLayout = new Android.Support.V4.Widget.SwipeRefreshLayout(Context);
            RefreshLayout.LayoutParameters = new LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);
            RefreshLayout.Refresh += (Sender, Event) => RefreshInvoke();
            RefreshLayout.AddView(ListView);
            AddView(RefreshLayout);
          }
          else
          {
            RefreshLayout.RemoveView(ListView);
            RemoveView(RefreshLayout);
            RefreshLayout = null;

            AddView(ListView);
          }

          if (!IsInLayout)
            RequestLayout();
        }
#endif
      }
    }
    public event Action RefreshEvent;
    
    public void NewRefresh()
    {
#if ANDROID_SUPPORT
      if (RefreshLayout != null)
      {
        RefreshLayout.Refreshing = true;
        RefreshInvoke();
      }
#endif
    }
    public void CompleteRefresh()
    {
#if ANDROID_SUPPORT
      if (RefreshLayout != null)
        RefreshLayout.Refreshing = false;
#endif
    }
    public void Reload()
    {
      Adapter.NotifyDataSetChanged();
    }
    public void ScrollToItem(int Section, int Index)
    {
      ListView.SetSelection(Adapter.GetAbsoluteIndex(Section, Index));
    }
    
    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      ChildView.Measure(MeasureSpec.MakeMeasureSpec(MeasuredWidth, MeasureSpecMode.Exactly), MeasureSpec.MakeMeasureSpec(MeasuredHeight, MeasureSpecMode.Exactly));
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      ChildView.LayoutChild(0, 0, Right - Left, Bottom - Top);
    }
    protected override void OnDetachedFromWindow()
    {
      SelectionIndex = ListView.FirstVisiblePosition;
      var FirstView = ListView.GetChildAt(0);
      SelectionY = FirstView == null ? 0 : FirstView.Top - ListView.PaddingTop;

      base.OnDetachedFromWindow();
    }
    protected override void OnAttachedToWindow()
    {
      base.OnAttachedToWindow();

      ListView.SetSelectionFromTop(SelectionIndex, SelectionY);
    }

    private void RefreshInvoke()
    {
      if (RefreshEvent != null)
        RefreshEvent();
    }

#if ANDROID_SUPPORT
    private Android.Support.V4.Widget.SwipeRefreshLayout RefreshLayout;
#endif
    private Android.Widget.ListView ListView;
    private FlowAdapter Adapter;
    private int SelectionIndex;
    private int SelectionY;
    private View ChildView
    {
      get
      {
#if ANDROID_SUPPORT
        if (RefreshLayout == null)
          return ListView;
        else
          return RefreshLayout;
#else
        return ListView;
#endif
      }
    }

    internal sealed class Configuration
    {
      public Func<int> SectionCountQuery;
      public Func<int, int> ItemCountQuery;
      public Func<int, int, View> ItemContentQuery;
      public Func<int, View> HeaderContentQuery;
      public Func<int, View> FooterContentQuery;
    }

    private sealed class FlowItemContainerView : ViewGroup
    {
      public FlowItemContainerView(Context Context)
        : base(Context)
      {
        this.SetClipChildren(false);
      }

      public int FixedHeight
      {
        get { return FixedHeightField; }
        set
        {
          FixedHeightField = value;
          RequestLayout();
        }
      }

      public void SetContent(View Content)
      {
        if (this.Content != Content)
        {
          if (this.Content != null)
            RemoveAllViews();

          this.Content = Content;

          if (this.Content != null)
          {
            // Since header and footer views aren't recreated, they need to be detached from their parents
            if (this.Content.Parent is ViewGroup)
              (this.Content.Parent as ViewGroup).RemoveView(this.Content);
            AddView(this.Content);
          }

          RequestLayout();
        }
      }

      protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
      {
        base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

        int RequestedWidth, RequestedHeight;
        this.MeasureOnlyChild(Content, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);

        SetMeasuredDimension(RequestedWidth, RequestedHeight);
      }
      protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
      {
        this.LayoutOnlyChild(Content, Left, Top, Right, Bottom);
      }

      private View Content;
      private int FixedHeightField;
    }

    private sealed class FlowAdapter : BaseAdapter
    {
      public FlowAdapter(Configuration Configuration)
        : base()
      {
        this.Configuration = Configuration;
      }

      public override int Count
      {
        get { return Enumerable.Range(0, Configuration.SectionCountQuery()).Sum(Section => (Configuration.HeaderContentQuery(Section) == null ? 0 : 1) + Configuration.ItemCountQuery(Section) + (Configuration.FooterContentQuery(Section) == null ? 0 : 1)); }
      }

      public override Java.Lang.Object GetItem(int Position)
      {
        return null;
      }
      public override long GetItemId(int Position)
      {
        return Position;
      }
      public override View GetView(int Position, View ConvertView, ViewGroup Parent)
      {
        // NOTE: Ignoring the ConvertView has been observed to cause issues with flow item buttons not working, so it is used to hold a simple container view.
        var RecycledContainer = ConvertView as FlowItemContainerView;
        if (RecycledContainer == null)
          RecycledContainer = new FlowItemContainerView(Parent.Context);

        var Index = 0;
        var Section = 0;

        while (Index <= Position)
        {
          var SectionHeader = Configuration.HeaderContentQuery(Section);
          if (SectionHeader != null)
          {
            if (Index == Position)
            {
              RecycledContainer.SetContent(SectionHeader);
              return RecycledContainer;
            }

            Index++;
          }

          var SectionItemCount = Configuration.ItemCountQuery(Section);
          if (Position <= Index + SectionItemCount - 1)
          {
            RecycledContainer.SetContent(Configuration.ItemContentQuery(Section, Position - Index));
            return RecycledContainer;
          }

          Index += SectionItemCount;

          var SectionFooter = Configuration.FooterContentQuery(Section);
          if (SectionFooter != null)
          {
            if (Index == Position)
            {
              RecycledContainer.SetContent(SectionFooter);
              return RecycledContainer;
            }

            Index++;
          }

          Section++;
        }
        
        return null;
      }

      public int GetAbsoluteIndex(int Section, int Index)
      {
        var Position = 0;

        for (var SectionIndex = 0; SectionIndex <= Section; SectionIndex++)
        {
          if (Configuration.HeaderContentQuery(SectionIndex) != null)
            Position++;

          if (SectionIndex == Section)
          {
            Position += Index;
            break;
          }

          Position += Configuration.ItemCountQuery(SectionIndex);

          if (Configuration.FooterContentQuery(SectionIndex) != null)
            Position++;
        }

        return Position;
      }

      private Configuration Configuration;
    }
  }

  internal sealed class AndroidLayoutScroll : AndroidLayoutGroup
  {
    public AndroidLayoutScroll(Android.Content.Context context, bool IsHorizontal)
      : base(context)
    {
      this.SetClipChildren(true); // necessary to clip the inner scroll views properly.

      if (IsHorizontal)
      {
        this.HorizontalScroll = new AndroidLayoutHorizontalScroll(context);
        this.ScrollView = HorizontalScroll;
      }
      else
      {
        this.VerticalScroll = new AndroidLayoutVerticalScroll(context);
        this.ScrollView = VerticalScroll;
      }

      AddView(ScrollView);
    }

    public void SetContentElement(Android.Views.View Element)
    {
      if (VerticalScroll != null)
        VerticalScroll.SetContentElement(Element);
      else
        HorizontalScroll.SetContentElement(Element);
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      int RequestedWidth, RequestedHeight;
      this.MeasureOnlyChild(ScrollView, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      this.LayoutOnlyChild(ScrollView, Left, Top, Right, Bottom);
    }

    private Android.Views.View ScrollView;
    private AndroidLayoutVerticalScroll VerticalScroll;
    private AndroidLayoutHorizontalScroll HorizontalScroll;
  }

  internal sealed class AndroidLayoutHorizontalScroll : Android.Widget.HorizontalScrollView
  {
    public AndroidLayoutHorizontalScroll(Android.Content.Context context)
      : base(context)
    {
      this.FillViewport = true;
    }

    public void SetContentElement(Android.Views.View Element)
    {
      if (this.ContentElement != Element)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement);

        this.ContentElement = Element;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement);

        this.Arrange();
      }
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      if (ContentElement != null)
      {
        var IdealWidth = MeasuredWidth;

        var WidthSize = IdealWidth - PaddingLeft - PaddingRight;
        var HeightSize = MeasureSpec.GetSize(HeightMeasureSpec);
        var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);
        var ContentHeightSpec = MeasureSpec.MakeMeasureSpec(HeightSize, HeightMode);

        ContentElement.Measure(MeasureSpec.MakeMeasureSpec(WidthSize, MeasureSpecMode.Unspecified), ContentHeightSpec);

        if (ContentElement.MeasuredWidth < WidthSize)
          ContentElement.Measure(MeasureSpec.MakeMeasureSpec(WidthSize, MeasureSpecMode.Exactly), ContentHeightSpec);

        var IdealHeight = PaddingTop + ContentElement.MeasuredHeight + PaddingBottom;

        switch (HeightMode)
        {
          case MeasureSpecMode.AtMost:
            IdealHeight = Math.Min(IdealHeight, HeightSize);
            break;

          case MeasureSpecMode.Exactly:
            IdealHeight = HeightSize;
            break;

          case MeasureSpecMode.Unspecified:
            break;

          default:
            throw new UnhandledEnumCaseException<MeasureSpecMode>();
        }

        SetMeasuredDimension(IdealWidth, IdealHeight);
      }
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      this.LayoutOnlyChild(ContentElement, Left, Top, Right, Bottom);
    }

    private Android.Views.View ContentElement;
  }
  
  internal sealed class AndroidLayoutVerticalScroll : Android.Widget.ScrollView
  {
    public AndroidLayoutVerticalScroll(Android.Content.Context context)
      : base(context)
    {
      this.FillViewport = true;
    }

    public void SetContentElement(Android.Views.View Element)
    {
      if (this.ContentElement != Element)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement);

        this.ContentElement = Element;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement);

        this.Arrange();
      }
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      if (ContentElement != null)
      {
        var IdealHeight = MeasuredHeight; // this is valid because we called base.OnMeasure.

        var WidthSize = MeasureSpec.GetSize(WidthMeasureSpec);
        var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
        var HeightSize = IdealHeight - PaddingTop - PaddingBottom;
        var ContentWidthSpec = MeasureSpec.MakeMeasureSpec(WidthSize - PaddingLeft - PaddingRight, WidthMode);

        // we allow the content to measure its height as tall as it can be.
        ContentElement.Measure(ContentWidthSpec, MeasureSpec.MakeMeasureSpec(HeightSize, MeasureSpecMode.Unspecified));

        // but if the measured height is less than the scroll viewport, we force it to match the viewport.
        if (ContentElement.MeasuredHeight < HeightSize)
          ContentElement.Measure(ContentWidthSpec, MeasureSpec.MakeMeasureSpec(HeightSize, MeasureSpecMode.Exactly));

        var IdealWidth = PaddingLeft + ContentElement.MeasuredWidth + PaddingRight;

        switch (WidthMode)
        {
          case MeasureSpecMode.AtMost:
            IdealWidth = Math.Min(IdealWidth, WidthSize);
            break;

          case MeasureSpecMode.Exactly:
            IdealWidth = WidthSize;
            break;

          case MeasureSpecMode.Unspecified:
            break;

          default:
            throw new UnhandledEnumCaseException<MeasureSpecMode>();
        }

        SetMeasuredDimension(IdealWidth, IdealHeight);
      }
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      this.LayoutOnlyChild(ContentElement, Left, Top, Right, Bottom);

      // HACK: this forces the scrollview to make sure the scrolled content is actually in the viewport.
      if (Changed)
        ScrollTo(ScrollX, ScrollY);
    }

    private Android.Views.View ContentElement;
  }

  internal sealed class AndroidLayoutBrowser : Android.Webkit.WebView
  {
    public AndroidLayoutBrowser(Android.Content.Context context)
      : base(context)
    {
      //this.SetClipToPadding(false);
      //this.SetClipChildren(false);

      base.Background = new AndroidLayoutBackground();

      Settings.JavaScriptEnabled = true;
      //Settings.DomStorageEnabled = true;

      SetWebViewClient(new SSLTolerentWebViewClient()); // https://www.google.com/ requires this for some reason.
    }

    public new AndroidLayoutBackground Background
    {
      get { return (AndroidLayoutBackground)base.Background; }
    }

    public void Navigate(Uri Uri, string Html)
    {
      if (Html != null)
        LoadData(Html, "text/html", "UTF-8");
      else if (Uri != null)
        LoadUrl(Uri.AbsoluteUri);
      else
        LoadUrl("about:blank");
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      // TODO: doesn't share space correctly when used in Dock.AddClient.

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      base.OnLayout(Changed, Left, Top, Right, Bottom);
    }

    private class SSLTolerentWebViewClient : Android.Webkit.WebViewClient
    {
      public override void OnReceivedSslError(Android.Webkit.WebView view, Android.Webkit.SslErrorHandler handler, Android.Net.Http.SslError error)
      {
        base.OnReceivedSslError(view, handler, error);
        //handler.Proceed();
      }
    }
  }

  //internal sealed class AndroidLayoutCanvas : Android.Views.SurfaceView, ISurfaceHolderCallback, Android.Views.GestureDetector.IOnGestureListener, ScaleGestureDetector.IOnScaleGestureListener
  internal sealed class AndroidLayoutCanvas : Android.Views.View, Android.Views.GestureDetector.IOnGestureListener, ScaleGestureDetector.IOnScaleGestureListener, Inv.DrawContract
  {
    public AndroidLayoutCanvas(Android.Content.Context context, AndroidEngine AndroidEngine)
      : base(context)
    {
      //this.Clickable = true; // NOTE: does not seem to be required when you are directly handling touch events.

      //this.ClipToOutline = true;

      this.ScaleDetector = new ScaleGestureDetector(context, this);

      base.Background = new AndroidLayoutBackground();

      this.AndroidEngine = AndroidEngine;

      this.AndroidArcRect = new Android.Graphics.RectF();
      this.AndroidEllipseRect = new Android.Graphics.RectF();
      this.AndroidBitmapRect = new Android.Graphics.Rect();
      this.AndroidTextRect = new Android.Graphics.Rect();
    }

    public new AndroidLayoutBackground Background
    {
      get { return (AndroidLayoutBackground)base.Background; }
    }

    public event Action<int, int> PressEvent;
    public event Action<int, int> ReleaseEvent;
    public event Action<int, int> MoveEvent;
    public event Action<int, int> SingleTapEvent;
    public event Action<int, int> DoubleTapEvent;
    public event Action<int, int> LongPressEvent;
    public event Action<int, int, int> ZoomEvent;
    public Action GraphicsDrawAction;

    protected override void OnDraw(Android.Graphics.Canvas Canvas)
    {
      //base.OnDraw(Canvas);
      this.AndroidCanvas = Canvas;
      GraphicsDrawAction();
    }
    public override bool OnTouchEvent(MotionEvent Event)
    {
      //Debug.WriteLine(Event.Action.ToString());

      ScaleDetector.OnTouchEvent(Event);
      //GestureDetector.OnTouchEvent(Event);

      switch (Event.Action & MotionEventActions.Mask)
      {
        case MotionEventActions.Down:
          if (Handler != null)
            Handler.RemoveCallbacks(LongPressedInvoke);

          this.LastX = (int)Event.GetX();
          this.LastY = (int)Event.GetY();

          var CurrentTime = System.Environment.TickCount;
          var IsDoubleTap = CurrentTime - LastTime < Android.Views.ViewConfiguration.DoubleTapTimeout;
          if (IsDoubleTap)
          {
            if (IsPressed)
            {
              this.IsPressed = false;

              if (Handler != null)
                Handler.RemoveCallbacks(LongPressedInvoke);
            }

            if (DoubleTapEvent != null)
              DoubleTapEvent(LastX, LastY);
          }
          else
          {
            if (IsPressed && Handler != null)
              Handler.RemoveCallbacks(LongPressedInvoke);
            else
              this.IsPressed = true;

            if (Handler != null)
              Handler.PostDelayed(LongPressedInvoke, Android.Views.ViewConfiguration.LongPressTimeout);

            if (PressEvent != null)
              PressEvent(LastX, LastY);
          }
          this.LastTime = CurrentTime;
          break;

        case MotionEventActions.Move:
          var MoveX = Event.GetX();
          var MoveY = Event.GetY();

          if (MoveEvent != null)
            MoveEvent((int)MoveX, (int)MoveY);

          if (IsPressed)
          {
            var Distance = ((MoveY - LastY) * (MoveY - LastY)) + ((MoveX - LastX) * (MoveX - LastX));
            if (Distance > 400) // sqrt(400) == 20
            {
              this.IsPressed = false;

              if (Handler != null)
                Handler.RemoveCallbacks(LongPressedInvoke);
            }
          }
          break;

        case MotionEventActions.Up:
          var ReleaseX = (int)Event.GetX();
          var ReleaseY = (int)Event.GetY();

          if (IsPressed)
          {
            this.IsPressed = false;

            if (SingleTapEvent != null)
              SingleTapEvent(ReleaseX, ReleaseY);

            if (Handler != null)
              Handler.RemoveCallbacks(LongPressedInvoke);
          }

          if (ReleaseEvent != null)
            ReleaseEvent(ReleaseX, ReleaseY);
          break;

        case MotionEventActions.Cancel:
          if (IsPressed)
          {
            this.IsPressed = false;

            if (Handler != null)
              Handler.RemoveCallbacks(LongPressedInvoke);
          }
          break;
      }

      return true;
    }

    private void SingleTapInvoke()
    {
    }
    private void LongPressedInvoke()
    {
      this.IsPressed = false;

      if (LongPressEvent != null)
        LongPressEvent(LastX, LastY);
    }

    bool GestureDetector.IOnGestureListener.OnDown(MotionEvent Event)
    {
      this.LastX = (int)Event.GetX();
      this.LastY = (int)Event.GetY();
      SingleTapInvoke();

      return true;
    }
    bool GestureDetector.IOnGestureListener.OnFling(MotionEvent Event1, MotionEvent Event2, float velocityX, float velocityY)
    {
      return true;
    }
    void GestureDetector.IOnGestureListener.OnLongPress(MotionEvent Event)
    {
      this.LastX = (int)Event.GetX();
      this.LastY = (int)Event.GetY();
      LongPressedInvoke();
    }
    bool GestureDetector.IOnGestureListener.OnScroll(MotionEvent Event1, MotionEvent Event2, float distanceX, float distanceY)
    {
      return true;
    }
    void GestureDetector.IOnGestureListener.OnShowPress(MotionEvent Event)
    {
    }
    bool GestureDetector.IOnGestureListener.OnSingleTapUp(MotionEvent Event)
    {
      return true;
    }
    bool ScaleGestureDetector.IOnScaleGestureListener.OnScale(ScaleGestureDetector detector)
    {
      if (ZoomEvent != null)
        ZoomEvent((int)detector.FocusX, (int)detector.FocusY, (int)(detector.ScaleFactor) > 0 ? +1 : -1);

      return true;
    }
    bool ScaleGestureDetector.IOnScaleGestureListener.OnScaleBegin(ScaleGestureDetector detector)
    {
      return true;
    }
    void ScaleGestureDetector.IOnScaleGestureListener.OnScaleEnd(ScaleGestureDetector detector)
    {
    }

    int DrawContract.Width
    {
      get { return AndroidEngine.PxToPt(Width); }
    }
    int DrawContract.Height
    {
      get { return AndroidEngine.PxToPt(Height); }
    }
    void DrawContract.DrawText(string TextFragment, string TextFontName, int TextFontSize, FontWeight TextFontWeight, Colour TextFontColour, Point TextPoint, HorizontalPosition TextHorizontal, VerticalPosition TextVertical)
    {
      var TextSizePixels = AndroidEngine.PtToPx(TextFontSize);
      var TextPaint = AndroidEngine.TranslateGraphicsTextPaint(TextFontName.EmptyAsNull(), TextSizePixels, TextFontWeight, TextFontColour);
      var TextPointX = AndroidEngine.PtToPx(TextPoint.X);
      var TextPointY = AndroidEngine.PtToPx(TextPoint.Y);
      var TextString = TextFragment ?? ""; // avoid null-ops.

      TextPaint.GetTextBounds(TextString, 0, TextString.Length, AndroidTextRect);

      if (TextHorizontal != HorizontalPosition.Left)
      {
        var TextWidth = AndroidTextRect.Width();

        if (TextHorizontal == HorizontalPosition.Right)
          TextPointX -= TextWidth;
        else if (TextHorizontal == HorizontalPosition.Center)
          TextPointX -= TextWidth / 2;
      }

      if (TextVertical != VerticalPosition.Bottom)
      {
        var TextHeight = AndroidTextRect.Height();

        if (TextVertical == VerticalPosition.Top)
          TextPointY += TextHeight;
        else if (TextVertical == VerticalPosition.Center)
          TextPointY += TextHeight / 2;
      }

      AndroidCanvas.DrawText(TextString, TextPointX, TextPointY, TextPaint);
    }
    void DrawContract.DrawLine(Inv.Colour LineStrokeColour, int LineStrokeThickness, Inv.Point LineSourcePoint, Inv.Point LineTargetPoint, params Inv.Point[] LineExtraPointArray)
    {
      if (LineStrokeThickness <= 0 || LineStrokeColour == Inv.Colour.Transparent)
        return;

      var LineStrokePixels = AndroidEngine.PtToPx(LineStrokeThickness);

      var LinePaint = AndroidEngine.TranslateGraphicsStrokePaint(LineStrokeColour, LineStrokePixels);

      if (LineExtraPointArray.Length == 0)
      {
        AndroidCanvas.DrawLine(
          AndroidEngine.PtToPx(LineSourcePoint.X), AndroidEngine.PtToPx(LineSourcePoint.Y), 
          AndroidEngine.PtToPx(LineTargetPoint.X), AndroidEngine.PtToPx(LineTargetPoint.Y), LinePaint);
      }
      else
      {
        var PositionArray = new float[(LineExtraPointArray.Length * 4) + 4];

        PositionArray[0] = AndroidEngine.PtToPx(LineSourcePoint.X);
        PositionArray[1] = AndroidEngine.PtToPx(LineSourcePoint.Y);
        PositionArray[2] = AndroidEngine.PtToPx(LineTargetPoint.X);
        PositionArray[3] = AndroidEngine.PtToPx(LineTargetPoint.Y);

        var CurrentPointX = PositionArray[2];
        var CurrentPointY = PositionArray[3];

        var PositionIndex = 4;
        for (var Index = 0; Index < LineExtraPointArray.Length; Index++)
        {
          var TargetPoint = LineExtraPointArray[Index];
          var TargetPointX = AndroidEngine.PtToPx(TargetPoint.X);
          var TargetPointY = AndroidEngine.PtToPx(TargetPoint.Y);

          PositionArray[PositionIndex++] = CurrentPointX;
          PositionArray[PositionIndex++] = CurrentPointY;
          PositionArray[PositionIndex++] = TargetPointX;
          PositionArray[PositionIndex++] = TargetPointY;

          CurrentPointX = TargetPointX;
          CurrentPointY = TargetPointY;
        }

        AndroidCanvas.DrawLines(PositionArray, LinePaint);
      }
    }
    void DrawContract.DrawRectangle(Colour RectangleFillColour, Colour RectangleStrokeColour, int RectangleStrokeThickness, Rect RectangleRect)
    {
      var RectangleStrokePixels = AndroidEngine.PtToPx(RectangleStrokeThickness);
      var RectangleLeft = AndroidEngine.PtToPx(RectangleRect.Left);
      var RectangleTop = AndroidEngine.PtToPx(RectangleRect.Top);
      var RectangleRight = AndroidEngine.PtToPx(RectangleRect.Right + 1);
      var RectangleBottom = AndroidEngine.PtToPx(RectangleRect.Bottom + 1);

      if (RectangleFillColour != null)
        AndroidCanvas.DrawRect(RectangleLeft, RectangleTop, RectangleRight, RectangleBottom, AndroidEngine.TranslateGraphicsFillPaint(RectangleFillColour));

      if (RectangleStrokeColour != null && RectangleStrokePixels > 0)
      {
        var HalfThickness = RectangleStrokePixels / 2;

        RectangleLeft += HalfThickness;
        RectangleTop += HalfThickness;

        if (RectangleLeft + HalfThickness < RectangleRight)
          RectangleRight -= HalfThickness;

        if (RectangleTop + HalfThickness < RectangleBottom)
          RectangleBottom -= HalfThickness;

        AndroidCanvas.DrawRect(RectangleLeft, RectangleTop, RectangleRight, RectangleBottom, AndroidEngine.TranslateGraphicsStrokePaint(RectangleStrokeColour, RectangleStrokePixels));
      }
    }
    void DrawContract.DrawArc(Inv.Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Inv.Point ArcCenter, Inv.Point ArcRadius, float StartAngle, float SweepAngle)
    {
      var ArcStrokePixels = AndroidEngine.PtToPx(ArcStrokeThickness);
      AndroidArcRect.Left = AndroidEngine.PtToPx(ArcCenter.X - ArcRadius.X);
      AndroidArcRect.Top = AndroidEngine.PtToPx(ArcCenter.Y - ArcRadius.Y);
      AndroidArcRect.Right = AndroidEngine.PtToPx(ArcCenter.X + ArcRadius.X);
      AndroidArcRect.Bottom = AndroidEngine.PtToPx(ArcCenter.Y + ArcRadius.Y);

      // NOTE: StartAngle-90 because Android starts at 3 o'clock for some reason.

      if (ArcFillColour != null)
        AndroidCanvas.DrawArc(AndroidArcRect, StartAngle - 90, SweepAngle - StartAngle, true, AndroidEngine.TranslateGraphicsFillPaint(ArcFillColour));

      if (ArcStrokeColour != null && ArcStrokeThickness > 0)
        AndroidCanvas.DrawArc(AndroidArcRect, StartAngle - 90, SweepAngle - StartAngle, true, AndroidEngine.TranslateGraphicsStrokePaint(ArcStrokeColour, ArcStrokePixels));
    }
    void DrawContract.DrawEllipse(Colour EllipseFillColour, Colour EllipseStrokeColour, int EllipseStrokeThickness, Point EllipseCenter, Point EllipseRadius)
    {
      var EllipseStrokePixels = AndroidEngine.PtToPx(EllipseStrokeThickness);
      AndroidEllipseRect.Left = AndroidEngine.PtToPx(EllipseCenter.X - EllipseRadius.X);
      AndroidEllipseRect.Top = AndroidEngine.PtToPx(EllipseCenter.Y - EllipseRadius.Y);
      AndroidEllipseRect.Right = AndroidEngine.PtToPx(EllipseCenter.X + EllipseRadius.X);
      AndroidEllipseRect.Bottom = AndroidEngine.PtToPx(EllipseCenter.Y + EllipseRadius.Y);

      if (EllipseFillColour != null)
        AndroidCanvas.DrawOval(AndroidEllipseRect, AndroidEngine.TranslateGraphicsFillPaint(EllipseFillColour));

      if (EllipseStrokeColour != null && EllipseStrokeThickness > 0)
        AndroidCanvas.DrawOval(AndroidEllipseRect, AndroidEngine.TranslateGraphicsStrokePaint(EllipseStrokeColour, EllipseStrokePixels));

      // NOTE: had a problem with a 5.0 emulator, so decided to just not use this supposed 21+ API call.
      //if (IsAPILevel21) 
      //{
      //  if (EllipseFillColour != null)
      //    AndroidCanvas.DrawOval(EllipseLeft, EllipseTop, EllipseRight, EllipseBottom, TranslateGraphicsFillPaint(EllipseFillColour));
      //
      //  if (EllipseStrokeColour != null && EllipseStrokeThickness > 0)
      //    AndroidCanvas.DrawOval(EllipseLeft, EllipseTop, EllipseRight, EllipseBottom, TranslateGraphicsStrokePaint(EllipseStrokeColour, EllipseStrokePixels));
      //}
    }
    void DrawContract.DrawImage(Image ImageSource, Rect ImageRect, float ImageOpacity, Colour ImageTint, Mirror? ImageMirror)
    {
      var ImageBitmap = AndroidEngine.TranslateGraphicsBitmap(ImageSource);

      if (ImageBitmap != null)
      {
        AndroidBitmapRect.Left = AndroidEngine.PtToPx(ImageRect.Left);
        AndroidBitmapRect.Top = AndroidEngine.PtToPx(ImageRect.Top);
        AndroidBitmapRect.Right = AndroidEngine.PtToPx(ImageRect.Right + 1);
        AndroidBitmapRect.Bottom = AndroidEngine.PtToPx(ImageRect.Bottom + 1);

        var ImagePaint = AndroidEngine.TranslateGraphicsImagePaint(ImageOpacity, ImageTint);

        if (ImageMirror == null)
        {
          AndroidCanvas.DrawBitmap(ImageBitmap, null, AndroidBitmapRect, ImagePaint);
        }
        else
        {
          var Matrix = new Android.Graphics.Matrix();

          var BitmapWidth = AndroidBitmapRect.Width();
          var BitmapHeight = AndroidBitmapRect.Height();
          var ScaleX = (float)BitmapWidth / (float)ImageBitmap.Width;
          var ScaleY = (float)BitmapHeight / (float)ImageBitmap.Height;

          if (ImageMirror.Value == Mirror.Horizontal)
          {
            Matrix.SetScale(-ScaleX, +ScaleY);
            Matrix.PostTranslate(AndroidBitmapRect.Left + BitmapWidth, AndroidBitmapRect.Top);
          }
          else if (ImageMirror.Value == Mirror.Vertical)
          {
            Matrix.SetScale(+ScaleX, -ScaleY);
            Matrix.PostTranslate(AndroidBitmapRect.Left, AndroidBitmapRect.Top + BitmapHeight);
          }

          AndroidCanvas.DrawBitmap(ImageBitmap, Matrix, ImagePaint);
        }
      }
    }
    void DrawContract.DrawPolygon(Colour FillColour, Colour StrokeColour, int StrokeThickness, LineJoin LineJoin, Point StartPoint, params Point[] PointArray)
    {
      var Path = new Path();
      Path.MoveTo(AndroidEngine.PtToPx(StartPoint.X), AndroidEngine.PtToPx(StartPoint.Y));

      foreach (var Point in PointArray)
        Path.LineTo(AndroidEngine.PtToPx(Point.X), AndroidEngine.PtToPx(Point.Y));

      var PolygonStrokePixels = AndroidEngine.PtToPx(StrokeThickness);

      AndroidCanvas.DrawPath(Path, AndroidEngine.TranslateGraphicsFillAndStrokePaint(FillColour, PolygonStrokePixels));
    }

    private int LastTime;
    private int LastX;
    private int LastY;
    private bool IsPressed;
    private ScaleGestureDetector ScaleDetector;
    private AndroidEngine AndroidEngine;
    private RectF AndroidEllipseRect;
    private RectF AndroidArcRect;
    private Android.Graphics.Canvas AndroidCanvas;
    private Android.Graphics.Rect AndroidBitmapRect;
    private Android.Graphics.Rect AndroidTextRect;
  }

  internal enum AndroidLayoutHorizontal
  {
    Stretch,
    Center,
    Left,
    Right
  }

  internal enum AndroidLayoutVertical
  {
    Stretch,
    Center,
    Top,
    Bottom
  }

  internal sealed class AndroidLayoutContainer : Android.Views.ViewGroup
  {
    public AndroidLayoutContainer(Android.Content.Context context)
      : base(context)
    {
      this.SetClipChildren(false);

      this.ContentWidth = -1;
      this.ContentHeight = -1;
      this.ContentMinimumWidth = -1;
      this.ContentMinimumHeight = -1;
      this.ContentMaximumWidth = -1;
      this.ContentMaximumHeight = -1;
      this.ContentVisible = true;

      //this.MotionEventSplittingEnabled = false;
    }

    public void SetContentVisiblity(bool ContentVisible)
    {
      if (ContentVisible != this.ContentVisible)
      {
        this.ContentVisible = ContentVisible;

        this.Arrange();
      }
    }
    public void SetContentHorizontal(AndroidLayoutHorizontal ContentHorizontal)
    {
      if (ContentHorizontal != this.ContentHorizontal)
      {
        this.ContentHorizontal = ContentHorizontal;

        this.Arrange();
      }
    }
    public void SetContentVertical(AndroidLayoutVertical ContentVertical)
    {
      if (ContentVertical != this.ContentVertical)
      {
        this.ContentVertical = ContentVertical;

        this.Arrange();
      }
    }
    public void SetContentElement(Android.Views.View ContentElement)
    {
      if (ContentElement != this.ContentElement)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement);

        this.ContentElement = ContentElement;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement);

        this.Arrange();
      }
    }
    public void SetContentWidth(int ContentWidth)
    {
      if (ContentWidth != this.ContentWidth)
      {
        this.ContentWidth = ContentWidth;

        this.Arrange();
      }
    }
    public void SetContentHeight(int ContentHeight)
    {
      if (ContentHeight != this.ContentHeight)
      {
        this.ContentHeight = ContentHeight;

        this.Arrange();
      }
    }
    public void SetContentMinimumWidth(int ContentMinimumWidth)
    {
      if (ContentMinimumWidth != this.ContentMinimumWidth)
      {
        this.ContentMinimumWidth = ContentMinimumWidth;

        this.Arrange();
      }
    }
    public void SetContentMaximumWidth(int ContentMaximumWidth)
    {
      if (ContentMaximumWidth != this.ContentMaximumWidth)
      {
        this.ContentMaximumWidth = ContentMaximumWidth;

        this.Arrange();
      }
    }
    public void SetContentMinimumHeight(int ContentMinimumHeight)
    {
      if (ContentMinimumHeight != this.ContentMinimumHeight)
      {
        this.ContentMinimumHeight = ContentMinimumHeight;

        this.Arrange();
      }
    }
    public void SetContentMaximumHeight(int ContentMaximumHeight)
    {
      if (ContentMaximumHeight != this.ContentMaximumHeight)
      {
        this.ContentMaximumHeight = ContentMaximumHeight;

        this.Arrange();
      }
    }
    public void ClearContentWidth()
    {
      SetContentWidth(-1);
    }
    public void ClearContentHeight()
    {
      SetContentHeight(-1);
    }
    public void ClearContentMinimumWidth()
    {
      SetContentMinimumWidth(-1);
    }
    public void ClearContentMinimumHeight()
    {
      SetContentMinimumHeight(-1);
    }
    public void ClearContentMaximumWidth()
    {
      SetContentMaximumWidth(-1);
    }
    public void ClearContentMaximumHeight()
    {
      SetContentMaximumHeight(-1);
    }
    public void SetContentVerticalCenter()
    {
      SetContentVertical(AndroidLayoutVertical.Center);
    }
    public void SetContentHorizontalCenter()
    {
      SetContentHorizontal(AndroidLayoutHorizontal.Center);
    }
    public void SetContentVerticalTop()
    {
      SetContentVertical(AndroidLayoutVertical.Top);
    }
    public void SetContentVerticalBottom()
    {
      SetContentVertical(AndroidLayoutVertical.Bottom);
    }
    public void SetContentHorizontalLeft()
    {
      SetContentHorizontal(AndroidLayoutHorizontal.Left);
    }
    public void SetContentHorizontalRight()
    {
      SetContentHorizontal(AndroidLayoutHorizontal.Right);
    }
    public void SetContentHorizontalStretch()
    {
      SetContentHorizontal(AndroidLayoutHorizontal.Stretch);
    }
    public void SetContentVerticalStretch()
    {
      SetContentVertical(AndroidLayoutVertical.Stretch);
    }
    public void SetContentMargins(int Left, int Top, int Right, int Bottom)
    {
      SetPadding(Left, Top, Right, Bottom);
    }
    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
      var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);
      var HorizontalPadding = PaddingLeft + PaddingRight;
      var VerticalPadding = PaddingTop + PaddingBottom;

      if (ContentElement != null)
      {
        if (ContentVisible)
        {
          var ChildWidthSize = MeasuredWidth - HorizontalPadding;
          var ChildWidthMode = WidthMode;
          var ChildHeightSize = MeasuredHeight - VerticalPadding;
          var ChildHeightMode = HeightMode;

          if (WidthMode == MeasureSpecMode.Exactly && ContentHorizontal != AndroidLayoutHorizontal.Stretch)
            ChildWidthMode = MeasureSpecMode.AtMost;

          if (HeightMode == MeasureSpecMode.Exactly && ContentVertical != AndroidLayoutVertical.Stretch)
            ChildHeightMode = MeasureSpecMode.AtMost;

          if (ContentWidth >= 0)
          {
            if (ContentWidth <= ChildWidthSize || ChildWidthMode == MeasureSpecMode.Unspecified)
              ChildWidthSize = ContentWidth;// -HorizontalPadding;

            ChildWidthMode = MeasureSpecMode.Exactly;
          }
          else if (ContentMaximumWidth >= 0)
          {
            if (ChildWidthSize >= ContentMaximumWidth || ChildWidthMode == MeasureSpecMode.Unspecified)
            {
              ChildWidthSize = Math.Min(ContentMaximumWidth, ChildWidthSize);// - HorizontalPadding;
              ChildWidthMode = MeasureSpecMode.AtMost;
            }
          }

          if (ContentHeight >= 0)
          {
            if (ContentHeight <= ChildHeightSize || ChildHeightMode == MeasureSpecMode.Unspecified)
              ChildHeightSize = ContentHeight;// - VerticalPadding;

            ChildHeightMode = MeasureSpecMode.Exactly;
          }
          else if (ContentMaximumHeight >= 0)
          {
            if (ChildHeightSize >= ContentMaximumHeight || ChildHeightMode == MeasureSpecMode.Unspecified)
            {
              ChildHeightSize = Math.Min(ContentMaximumHeight, ChildHeightSize);// - VerticalPadding;
              ChildHeightMode = MeasureSpecMode.AtMost;
            }
          }

          ContentElement.Measure(MeasureSpec.MakeMeasureSpec(ChildWidthSize, ChildWidthMode), MeasureSpec.MakeMeasureSpec(ChildHeightSize, ChildHeightMode));

          var RequireSecondMeasure = false;

          if (ContentElement.MeasuredWidth < ContentMinimumWidth && ChildWidthMode != MeasureSpecMode.Exactly)
          {
            ChildWidthSize = Math.Min(ContentMinimumWidth, ChildWidthSize);// - HorizontalPadding;
            ChildWidthMode = MeasureSpecMode.Exactly;
            RequireSecondMeasure = true;
          }

          if (ContentElement.MeasuredHeight < ContentMinimumHeight && ChildHeightMode != MeasureSpecMode.Exactly)
          {
            ChildHeightSize = Math.Min(ContentMinimumHeight, ChildHeightSize);// - VerticalPadding;
            ChildHeightMode = MeasureSpecMode.Exactly;
            RequireSecondMeasure = true;
          }

          if (RequireSecondMeasure)
            ContentElement.Measure(MeasureSpec.MakeMeasureSpec(ChildWidthSize, ChildWidthMode), MeasureSpec.MakeMeasureSpec(ChildHeightSize, ChildHeightMode));

          var ContainerWidth = MeasuredWidth;
          var ContainerHeight = MeasuredHeight;

          var TotalContentWidth = ContentElement.MeasuredWidth + HorizontalPadding;
          if ((WidthMode == MeasureSpecMode.Unspecified && TotalContentWidth > MeasuredWidth) || (WidthMode == MeasureSpecMode.AtMost && TotalContentWidth < MeasuredWidth))
            ContainerWidth = TotalContentWidth;

          var TotalContentHeight = ContentElement.MeasuredHeight + VerticalPadding;
          if ((HeightMode == MeasureSpecMode.Unspecified && TotalContentHeight > MeasuredHeight) || (HeightMode == MeasureSpecMode.AtMost && TotalContentHeight < MeasuredHeight))
            ContainerHeight = TotalContentHeight;

#if DEBUG
          //if (ContainerWidth < 0 || ContainerHeight < 0 )
          //  System.Diagnostics.Debug.WriteLine("Container: " + ContainerWidth + ", " + ContainerHeight);
#endif

          SetMeasuredDimension(ContainerWidth, ContainerHeight);
        }
        else
        {
          ContentElement.Measure(MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Exactly), MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Exactly));
          SetMeasuredDimension(WidthMode == MeasureSpecMode.Exactly ? MeasuredWidth : 0, HeightMode == MeasureSpecMode.Exactly ? MeasuredHeight : 0);
        }
      }
      else
      {
        var ResultWidth = WidthMode == MeasureSpecMode.Exactly ? MeasuredWidth : HorizontalPadding;
        var ResultHeight = HeightMode == MeasureSpecMode.Exactly ? MeasuredHeight : VerticalPadding;
        SetMeasuredDimension(ResultWidth, ResultHeight);
      }
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      if (ContentElement != null)
      {
        if (!ContentVisible)
        {
          ContentElement.Visibility = ViewStates.Gone;
          ContentElement.LayoutChild(0, 0, 0, 0);
        }
        else
        {
          ContentElement.Visibility = ViewStates.Visible;

          var ElementLeft = 0;
          var ElementTop = 0;
          var ElementWidth = ContentElement.MeasuredWidth;
          var ElementHeight = ContentElement.MeasuredHeight;
          var BoundsWidth = Right - Left;
          var BoundsHeight = Bottom - Top;

          switch (ContentHorizontal)
          {
            case AndroidLayoutHorizontal.Left:
              ElementLeft = PaddingLeft;
              break;

            case AndroidLayoutHorizontal.Center:
            case AndroidLayoutHorizontal.Stretch:
              ElementLeft = PaddingLeft + (BoundsWidth - PaddingLeft - PaddingRight - ElementWidth) / 2;
              break;

            case AndroidLayoutHorizontal.Right:
              ElementLeft = BoundsWidth - PaddingRight - ElementWidth;
              break;

            default:
              throw new UnhandledEnumCaseException<AndroidLayoutHorizontal>();
          }

          switch (ContentVertical)
          {
            case AndroidLayoutVertical.Top:
              ElementTop = PaddingTop;
              break;

            case AndroidLayoutVertical.Center:
            case AndroidLayoutVertical.Stretch:
              ElementTop = PaddingTop + (BoundsHeight - PaddingTop - PaddingBottom - ElementHeight) / 2;
              break;

            case AndroidLayoutVertical.Bottom:
              ElementTop = BoundsHeight - PaddingBottom - ElementHeight;
              break;

            default:
              throw new UnhandledEnumCaseException<AndroidLayoutVertical>();
          }

#if DEBUG
          //if (ElementWidth > BoundsWidth || ElementHeight > BoundsHeight)
          //  System.Diagnostics.Debug.WriteLine("Element Size: (" + ElementWidth + ", " + ElementHeight + ") vs (" + BoundsWidth + ", " + BoundsHeight + ")");
#endif

          ContentElement.LayoutChild(ElementLeft, ElementTop, ElementLeft + ElementWidth, ElementTop + ElementHeight);
        }
      }
    }

    private Android.Views.View ContentElement;
    private AndroidLayoutVertical ContentVertical;
    private AndroidLayoutHorizontal ContentHorizontal;
    private bool ContentVisible;
    private int ContentWidth;
    private int ContentHeight;
    private int ContentMinimumWidth;
    private int ContentMinimumHeight;
    private int ContentMaximumWidth;
    private int ContentMaximumHeight;
  }

  internal static class AndroidLayoutFoundation
  {
    public static void SafeAddView(this ViewGroup AndroidViewGroup, View ElementView)
    {
      if (ElementView.Parent == null)
      {
        AndroidViewGroup.AddView(ElementView);
      }
      else
      {
        var ViewGroup = ElementView.Parent as ViewGroup;

        if (ViewGroup != AndroidViewGroup)
        {
          ViewGroup.RemoveView(ElementView);
          AndroidViewGroup.AddView(ElementView);
        }
      }
    }
    public static void SafeRemoveView(this ViewGroup AndroidViewGroup, View ElementView)
    {
      if (ElementView.Parent == AndroidViewGroup)
        AndroidViewGroup.RemoveView(ElementView);
    }
    public static void Arrange(this View AndroidView)
    {
      // NOTE: doesn't seem to need to request layout all the way up the parent views.
      AndroidView.RequestLayout();

      //var AndroidCurrent = AndroidView;
      //while (AndroidCurrent != null)
      //{
      //  AndroidCurrent.RequestLayout();
      //  //AndroidCurrent.PostInvalidate();
      //  //AndroidCurrent.Invalidate();
      //  //AndroidCurrent.ForceLayout();
      //
      //  AndroidCurrent = AndroidCurrent.Parent as View;
      //}
    }
    public static void LayoutChild(this View AndroidView, int l, int t, int r, int b)
    {
#if DEBUG
      //if (l < 0 || t < 0 || l > r || t > b)
      //  System.Diagnostics.Debug.WriteLine("Layout incorrect (" + l + ", " + t + ", " + r + ", " + b + ")");
#endif

      AndroidView.Layout(l, t, r, b);
    }
    public static void MeasureOnlyChild(this View AndroidView, Android.Views.View ContentElement, int WidthMeasureSpec, int HeightMeasureSpec, out int MeasuredWidth, out int MeasuredHeight)
    {
      var IdealWidth = AndroidView.PaddingLeft + AndroidView.PaddingRight;
      var IdealHeight = AndroidView.PaddingTop + AndroidView.PaddingBottom;

      var WidthSize = View.MeasureSpec.GetSize(WidthMeasureSpec);
      var WidthMode = View.MeasureSpec.GetMode(WidthMeasureSpec);
      var HeightSize = View.MeasureSpec.GetSize(HeightMeasureSpec);
      var HeightMode = View.MeasureSpec.GetMode(HeightMeasureSpec);

      if (ContentElement != null)
      {
        var ChildWidthSize = Math.Max(0, WidthSize - AndroidView.PaddingLeft - AndroidView.PaddingRight);
        var ChildWidthSpec = View.MeasureSpec.MakeMeasureSpec(ChildWidthSize, WidthMode);
        var ChildHeightSize = Math.Max(0, HeightSize - AndroidView.PaddingTop - AndroidView.PaddingBottom);
        var ChildHeightSpec = View.MeasureSpec.MakeMeasureSpec(ChildHeightSize, HeightMode);

        ContentElement.Measure(ChildWidthSpec, ChildHeightSpec);

        IdealWidth += ContentElement.MeasuredWidth;
        IdealHeight += ContentElement.MeasuredHeight;
      }

      switch (WidthMode)
      {
        case MeasureSpecMode.AtMost:
          MeasuredWidth = Math.Min(IdealWidth, WidthSize);
          break;

        case MeasureSpecMode.Exactly:
          MeasuredWidth = WidthSize;
          break;

        case MeasureSpecMode.Unspecified:
          MeasuredWidth = IdealWidth;
          break;

        default:
          throw new UnhandledEnumCaseException<MeasureSpecMode>();
      }

      switch (HeightMode)
      {
        case MeasureSpecMode.AtMost:
          MeasuredHeight = Math.Min(IdealHeight, HeightSize);
          break;

        case MeasureSpecMode.Exactly:
          MeasuredHeight = HeightSize;
          break;

        case MeasureSpecMode.Unspecified:
          MeasuredHeight = IdealHeight;
          break;

        default:
          throw new UnhandledEnumCaseException<MeasureSpecMode>();
      }
    }
    public static void LayoutOnlyChild(this View AndroidView, Android.Views.View ContentElement, int Left, int Top, int Right, int Bottom)
    {
      if (ContentElement != null)
      {
        // NOTE: child should always match the measurement of the parent (as it is a layout container).
        //System.Diagnostics.Debug.Assert(Right - Left == ContentElement.MeasuredWidth - AndroidView.PaddingLeft - AndroidView.PaddingRight);
        //System.Diagnostics.Debug.Assert(Bottom - Top == ContentElement.MeasuredHeight - AndroidView.PaddingTop - AndroidView.PaddingBottom);

        ContentElement.LayoutChild(AndroidView.PaddingLeft, AndroidView.PaddingTop, AndroidView.PaddingLeft + ContentElement.MeasuredWidth, AndroidView.PaddingTop + ContentElement.MeasuredHeight);
      }
    }
    public static void HideKeyboard(this View View)
    {
      var imm = (Android.Views.InputMethods.InputMethodManager)View.Context.GetSystemService(Android.Content.Context.InputMethodService);
      imm.HideSoftInputFromWindow(View.WindowToken, 0);
    }
    public static IEnumerable<Android.Views.ViewGroup> EnumerateParents(this View AndroidView)
    {
      while (AndroidView.Parent != null && AndroidView.Parent is Android.Views.ViewGroup)
      {
        var Parent = AndroidView.Parent as Android.Views.ViewGroup;
        AndroidView = Parent;
        yield return Parent;
      }
    }
    public static void RecurseClipChildren(this View View, bool Value)
    {
      var ViewGroup = View as ViewGroup;

      if (ViewGroup != null)
      {
        ViewGroup.SetClipChildren(Value);

        for (var ViewIndex = 0; ViewIndex < ViewGroup.ChildCount; ViewIndex++)
          ViewGroup.GetChildAt(ViewIndex).RecurseClipChildren(Value);
      }
    }
    public static string ChildHierarchyToString(this Activity Activity)
    {
      var Writer = new HierarchyWriter<View>(GetChildCount, EnumerateChildren, ViewToString);
      return Writer.WriteToString(Activity.Window.DecorView);
    }
    public static string ChildHierarchyToString(this View RootView)
    {
      var Writer = new HierarchyWriter<View>(GetChildCount, EnumerateChildren, ViewToString);
      return Writer.WriteToString(RootView);
    }
    public static bool PassthroughTouchEvent(this View View)
    {
      var Current = View.Parent;

      while (Current != null)
      {
        if (Current is ITouchable)
          return false;

        Current = Current.Parent;
      }

      return true;
    }

    private static int GetChildCount(View View)
    {
      var Group = View as ViewGroup;
      if (Group == null)
        return 0;
      else
        return Group.ChildCount;
    }
    private static IEnumerable<View> EnumerateChildren(View View)
    {
      var Group = View as ViewGroup;

      if (Group != null)
      {
        var Index = 0;
        while (Index < Group.ChildCount)
          yield return Group.GetChildAt(Index++);
      }
    }
    private static string ViewToString(View View)
    {
      var Properties = new Dictionary<string, string>();
      Properties.Add("frame", string.Format("({0} {1}; {2} {3})", View.Left, View.Top, View.Right - View.Left, View.Bottom - View.Top));

      if (View is TextView)
      {
        var TextView = View as TextView;
        Properties.Add("text", "\"" + TextView.Text + "\"");
      }

      return string.Format("<{0}; {1}>", View.GetType().FullName, string.Join("; ", Properties.Select(P => string.Format("{0} = {1}", P.Key, P.Value))));
    }
  }

  internal sealed class UnhandledEnumCaseException<TEnum> : ApplicationException
    where TEnum : struct
  {
    public UnhandledEnumCaseException()
      : base(string.Format("Unhandled enum case for {0}", typeof(TEnum).FullName))
    {
    }
  }
}