﻿/*! 4 !*/
//------------------------------------------------------------------------------
// <copyright file="PlayCommand.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.Design;
using System.Globalization;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System.Diagnostics;
using System.Reflection;
using System.Collections.Generic;

namespace InvExtension
{
  internal sealed class PlayCommand
  {
    public const int CommandId = 0x0100;
    public static readonly Guid CommandSet = new Guid("5546e2e2-c389-447f-a902-a11544c1f5f0");
    public static PlayCommand Instance { get; private set; }

    public static void Initialize(Package package)
    {
      Instance = new PlayCommand(package);
    }

    private PlayCommand(Package package)
    {
      if (package == null)
        throw new ArgumentNullException("package");

      this.package = package;

      var commandService = this.ServiceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
      if (commandService != null)
      {
        var menuCommandID = new CommandID(CommandSet, CommandId);
        var menuItem = new MenuCommand(this.PlayCallback, menuCommandID);
        commandService.AddCommand(menuItem);
      }
    }

    private IServiceProvider ServiceProvider
    {
      get
      {
        return this.package;
      }
    }
    private EnvDTE80.DTE2 GetDTE2()
    {
      return ServiceProvider.GetService(typeof(EnvDTE.DTE)) as EnvDTE80.DTE2;
    }
    private EnvDTE.Project GetSelectedProject()
    {
      var _applicationObject = GetDTE2();
      var uih = _applicationObject.ToolWindows.SolutionExplorer;
      var selectedItems = (Array)uih.SelectedItems;
      if (selectedItems != null)
      {
        foreach (EnvDTE.UIHierarchyItem selItem in selectedItems)
        {
          var Project = selItem.Object as EnvDTE.Project;
          if (Project != null)
            return Project;
        }
      }
      return null;
    }

    private void PlayCallback(object sender, EventArgs e)
    {
      // TODO: find the correct version of InvPlay.exe in ..\packages\ for the selected project.

      var ErrorMessage = (string)null;

      var Project = GetSelectedProject();

      if (Project == null)
      {
        ErrorMessage = "Invention Play must be run on a portable platform project.";
      }
      else
      {
        var VSProject = Project.Object as VSLangProj.VSProject;

        if (VSProject != null)
        {
          foreach (VSLangProj.Reference reference in VSProject.References)
          {
            if (reference.SourceProject == null && reference.Name == "Inv.Platform")
            {
              var HintPath = reference.Path;

              var PlayPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Project.FullName), System.IO.Path.GetDirectoryName(HintPath), @"..\..", "play", "InvPlay.exe");

              if (!System.IO.File.Exists(PlayPath))
              {
                ErrorMessage = "Invention Play was unable to locate the executable file." + Environment.NewLine + Environment.NewLine + PlayPath;
                break;
              }

              // success!
              var Info = Process.Start(PlayPath, "\"" + Project.FullName + "\"");

              var DTE = GetDTE2();

              if (DTE != null)
              {
                foreach (EnvDTE.Process LocalProcess in DTE.Debugger.LocalProcesses)
                {
                  if (LocalProcess.ProcessID == Info.Id)
                  {
                    LocalProcess.Attach();
                    return;
                  }
                }
              }
              return;
            }
          }

          if (ErrorMessage == null)
            ErrorMessage = "Invention Play requires the project to reference the platform package." + Environment.NewLine + Environment.NewLine + "NuGet PM> Install-Package Invention.Platform";
        }
      }

      if (ErrorMessage != null)
      {
        VsShellUtilities.ShowMessageBox(
          this.ServiceProvider,
          ErrorMessage,
          "Invention Play",
          OLEMSGICON.OLEMSGICON_INFO,
          OLEMSGBUTTON.OLEMSGBUTTON_OK,
          OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
      }
    }

    private readonly Package package;
  }
}