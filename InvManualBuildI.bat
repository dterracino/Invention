rem DID YOU REMEMBER TO INCREMENT THE BUILD NUMBER?

pause

rem BUILD **INVENTION MANUAL** iOS

@echo off
attrib -r "C:\Development\Forge\Inv\InvManual\InvManual\Resources\Texts\Version.txt"
Set YEAR=%DATE:~10,4%
Set MONTH=%DATE:~7,2%
Set DAY=%DATE:~4,2%
Set HOUR=%TIME:~0,2%
Set HOUR=%HOUR: =0%
Set MIN=%TIME:~3,2%
Set DTS=%YEAR%.%MONTH%%DAY%.%HOUR%%MIN%
echo echo|set/p="i%DTS%">"C:\Development\Forge\Inv\InvManual\InvManual\Resources\Texts\Version.txt"
@echo on

"C:\Development\Forge\Inv\InvGen\bin\Debug\InvGen.exe" "C:\Development\Forge\Inv\InvManual\InvManual\Resources\InvManual.InvResourcePackage"

@echo off
rem BUILD AT HOME

rem "C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe" C:\Development\Forge\Inv\Inv.sln /p:Configuration=Ad-hoc;Platform="iPhone" /t:Manual\InvManualI:Clean /p:ServerAddress=10.1.1.239 /p:ServerUser=callanh /p:ServerPassword=
rem "C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe" C:\Development\Forge\Inv\Inv.sln /p:Configuration=Ad-hoc;Platform="iPhone" /t:Manual\InvManualI /p:ServerAddress=10.1.1.239 /p:ServerUser=callanh /p:ServerPassword=
@echo on

rem BUILD AT KESTRAL

"C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe" C:\Development\Forge\Inv\Inv.sln /p:Configuration=Ad-hoc;Platform="iPhone" /t:Manual\InvManualI:Clean /p:ServerAddress=10.250.22.65 /p:ServerUser=callanh /p:ServerPassword=
"C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe" C:\Development\Forge\Inv\Inv.sln /p:Configuration=Ad-hoc;Platform="iPhone" /t:Manual\InvManualI /p:ServerAddress=10.250.22.65 /p:ServerUser=callanh /p:ServerPassword=