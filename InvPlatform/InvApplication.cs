﻿/*! 16 !*/
#if DEBUG
//#define TRACE_PANEL_CHANGE
//#define TRACE_PANEL_RENDER
#endif
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  internal interface Platform
  {
    int ThreadAffinity();
    string CalendarTimeZoneName();
    void CalendarShowPicker(CalendarPicker CalendarPicker);
    bool EmailSendMessage(EmailMessage EmailMessage);
    bool PhoneIsSupported { get; }
    void PhoneDial(string PhoneNumber);
    void PhoneSMS(string PhoneNumber);
    long DirectoryGetLengthFile(File File);
    DateTime DirectoryGetLastWriteTimeUtcFile(File File);
    void DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp);
    System.IO.Stream DirectoryCreateFile(File File);
    System.IO.Stream DirectoryAppendFile(File File);
    System.IO.Stream DirectoryOpenFile(File File);
    bool DirectoryExistsFile(File File);
    void DirectoryDeleteFile(File File);
    void DirectoryCopyFile(File SourceFile, File TargetFile);
    void DirectoryMoveFile(File SourceFile, File TargetFile);
    IEnumerable<File> DirectoryGetFolderFiles(Folder Folder, string FileMask);
    System.IO.Stream DirectoryOpenAsset(Asset Asset);
    bool DirectoryExistsAsset(Asset Asset);
    void DirectoryShowFilePicker(DirectoryFilePicker FilePicker);
    bool LocationIsSupported { get; }
    void LocationLookup(LocationLookup LocationLookup);
    void AudioPlaySound(Inv.Sound Sound, float Volume, float Rate);
    void AudioPlayClip(AudioClip Clip);
    void AudioStopClip(AudioClip Clip);
    void WindowBrowse(File File);
    void WindowPost(Action Action);
    void WindowCall(Action Action);
    long ProcessMemoryUsedBytes();
    void ProcessMemoryReclamation();
    void WebClientConnect(WebClient Client);
    void WebClientDisconnect(WebClient Client);
    void WebServerConnect(WebServer Server);
    void WebServerDisconnect(WebServer Server);
    void WebLaunchUri(Uri Uri);
    void MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID);
    void VaultLoadSecret(Secret Secret);
    void VaultSaveSecret(Secret Secret);
    void VaultDeleteSecret(Secret Secret);
  }

  public sealed class Application
  {
    public Application()
    {
      this.Device = new Device(this);
      this.Process = new Process(this);
      this.Window = new Window(this);
      this.Directory = new Directory(this);
      this.Audio = new Audio(this);
      this.Location = new Location(this);
      this.Calendar = new Calendar(this);
      this.Market = new Market(this);
      this.Vault = new Vault(this);
      this.Web = new Web(this);
      this.Email = new Email(this);
      this.Phone = new Phone(this);
    }

    public string Title { get; set; }
    public bool IsExit { get; private set; }
    public Device Device { get; private set; }
    public Process Process { get; private set; }
    public Window Window { get; private set; }
    public Directory Directory { get; private set; }
    public Audio Audio { get; private set; }
    public Location Location { get; private set; }
    public Calendar Calendar { get; private set; }
    public Market Market { get; private set; }
    public Vault Vault { get; private set; }
    public Web Web { get; private set; }
    public Email Email { get; private set; }
    public Phone Phone { get; private set; }
    public event Action StartEvent;
    public event Action StopEvent;
    public event Action SuspendEvent;
    public event Action ResumeEvent;
    public event Func<bool> ExitQuery;
    public event Action<Exception> HandleExceptionEvent;

    public void Exit()
    {
      CheckThreadAffinity();

      this.IsExit = true;
    }
    public void CheckThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
      {
        if (Platform == null)
          throw new Exception("Platform is not yet ready - please move your code into the Application.StartEvent.");

        if (ThreadAffinity != Platform.ThreadAffinity())
          throw new Exception("Thread affinity mismatch.");
      }
    }

    internal Platform Platform { get; private set; }
    internal int ThreadAffinity { get; private set; }

    internal void StartInvoke()
    {
      CheckThreadAffinity();

      if (StartEvent != null)
        StartEvent();
    }
    internal void StopInvoke()
    {
      CheckThreadAffinity();

      if (StopEvent != null)
        StopEvent();
    }
    internal void SuspendInvoke()
    {
      CheckThreadAffinity();

      if (SuspendEvent != null)
        SuspendEvent();
    }
    internal void ResumeInvoke()
    {
      CheckThreadAffinity();

      if (ResumeEvent != null)
        ResumeEvent();
    }
    internal bool ExitQueryInvoke()
    {
      CheckThreadAffinity();

      if (ExitQuery != null)
        return ExitQuery();
      else
        return true;
    }
    internal void HandleExceptionInvoke(Exception Exception)
    {
      CheckThreadAffinity();

      try
      {
        if (HandleExceptionEvent != null)
          HandleExceptionEvent(Exception);
      }
      catch
      {
        // NOTE: prevent further exceptions.
      }
    }

    internal void SetPlatform(Platform Platform)
    {
      this.Platform = Platform;
      this.IsExit = false;
      this.ThreadAffinity = Platform.ThreadAffinity();
    }
  }

  public sealed class Process
  {
    internal Process(Application Application)
    {
      this.Application = Application;
    }

    public int Id { get; internal set; }

    public Inv.DataSize GetMemoryUsage()
    {
      // NOTE: allowed from any thread.
      //Application.CheckThreadAffinity();

      return Inv.DataSize.FromBytes(Application.Platform.ProcessMemoryUsedBytes());
    }
    public void MemoryReclamation()
    {
      Application.CheckThreadAffinity();

      Application.Platform.ProcessMemoryReclamation();
    }

    private Application Application;
  }

  public sealed class Device
  {
    internal Device(Application Application)
    {
      this.Application = Application;
    }

    public string Name { get; internal set; }
    public string Model { get; internal set; }
    public string System { get; internal set; }
    public bool Keyboard { get; internal set; }
    public bool Mouse { get; internal set; }
    public bool Touch { get; internal set; }
    public string ProportionalFontName { get; internal set; }
    public string MonospacedFontName { get; internal set; }

    internal Application Application { get; private set; }
  }

  public sealed class Calendar
  {
    internal Calendar(Application Application)
    {
      this.Application = Application;
    }

    public string GetTimeZoneName()
    {
      return Application.Platform.CalendarTimeZoneName();
    }
    public CalendarPicker NewDateTimePicker()
    {
      return NewPicker(true, true);
    }
    public CalendarPicker NewDatePicker()
    {
      return NewPicker(true, false);
    }
    public CalendarPicker NewTimePicker()
    {
      return NewPicker(false, true);
    }
    /*
    public void CreateReminder(string Title, string Description, DateTime Time, TimeSpan Duration)
    {
      Application.Platform.CalendarCreateReminder(Title, Description, Time, Duration);
    }
    */

    internal Application Application { get; private set; }

    internal CalendarPicker NewPicker(bool SetDate, bool SetTime)
    {
      return new CalendarPicker(this, SetDate, SetTime);
    }
  }

  public abstract class Picker
  {
    internal object Node { get; set; }
  }

  public sealed class CalendarPicker : Picker
  {
    internal CalendarPicker(Calendar Calendar, bool SetDate, bool SetTime)
    {
      this.Calendar = Calendar;
      this.SetDate = SetDate;
      this.SetTime = SetTime;
    }

    public bool SetDate { get; private set; }
    public bool SetTime { get; private set; }
    public DateTime Value { get; set; }
    public event Action SelectEvent;
    public event Action CancelEvent;

    public void Show()
    {
      Calendar.Application.Platform.CalendarShowPicker(this);
    }

    internal void SelectInvoke()
    {
      if (SelectEvent != null)
        SelectEvent();
    }
    internal void CancelInvoke()
    {
      if (CancelEvent != null)
        CancelEvent();
    }

    private Calendar Calendar;
  }

  public sealed class Market
  {
    internal Market(Application Application)
    {
      this.Application = Application;
    }

    public void Browse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      Application.Platform.MarketBrowse(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }

    internal Application Application { get; private set; }
  }

  public sealed class Email
  {
    internal Email(Application Application)
    {
      this.Application = Application;
    }

    public EmailMessage NewMessage()
    {
      return new EmailMessage(this);
    }

    internal Application Application { get; private set; }
  }

  public sealed class Phone
  {
    internal Phone(Application Application)
    {
      this.Application = Application;
    }

    public bool IsSupported
    {
      get { return Application.Platform.PhoneIsSupported; }
    }

    public void Dial(string Number)
    {
      Application.Platform.PhoneDial(Number);
    }
    public void SMS(string Number)
    {
      Application.Platform.PhoneSMS(Number);
    }

    private Application Application;
  }

  public sealed class EmailMessage
  {
    internal EmailMessage(Email Email)
    {
      this.Email = Email;
      this.ToList = new Inv.DistinctList<EmailRecipient>();
      this.AttachmentList = new Inv.DistinctList<EmailAttachment>();
    }

    public string Subject { get; set; }
    public string Body { get; set; }

    public void To(string RecipientName, string RecipientAddress)
    {
      ToList.Add(new EmailRecipient(RecipientName, RecipientAddress));
    }
    public void Attach(string DisplayName, File File)
    {
      AttachmentList.Add(new EmailAttachment(DisplayName, File));
    }

    public bool Send()
    {
      AttachmentList.RemoveAll(A => !A.File.Exists());

      return Email.Application.Platform.EmailSendMessage(this);
    }

    internal IEnumerable<EmailRecipient> GetTos()
    {
      return ToList;
    }
    internal IEnumerable<EmailAttachment> GetAttachments()
    {
      return AttachmentList;
    }
    internal bool HasAttachments()
    {
      return AttachmentList.Count > 0;
    }

    private Email Email;
    private Inv.DistinctList<EmailRecipient> ToList;
    private Inv.DistinctList<EmailAttachment> AttachmentList;
  }

  public sealed class EmailRecipient
  {
    internal EmailRecipient(string Name, string Address)
    {
      this.Name = Name;
      this.Address = Address;
    }

    public string Name { get; private set; }
    public string Address { get; private set; }
  }

  public sealed class EmailAttachment
  {
    internal EmailAttachment(string Name, File File)
    {
      this.Name = Name;
      this.File = File;
    }

    public string Name { get; private set; }
    public File File { get; private set; }
  }

  public sealed class Audio
  {
    internal Audio(Application Application)
    {
      this.Application = Application;
      this.IsRateControlled = true;
      this.IsVolumeControlled = true;
    }

    public void SetMuted(bool IsMuted)
    {
      this.IsMuted = IsMuted;
    }
    public void SetRateControl(bool IsRateControlled)
    {
      this.IsRateControlled = IsRateControlled;
    }
    public void SetVolumeControl(bool IsVolumeControlled)
    {
      this.IsVolumeControlled = IsVolumeControlled;
    }
    public void Play(Inv.Sound Sound, float Volume = 1.0F, float Rate = 1.0F)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Volume >= 0.0F && Volume <= 1.0F, "Play volume must be between 0.0F and 1.0F.");
        Inv.Assert.Check(Rate >= 0.5F && Rate <= 2.0F, "Play rate must be between 0.5F and 2.0F.");
      }

      if (!IsMuted)
        Application.Platform.AudioPlaySound(Sound, IsVolumeControlled ? Volume : 1.0F, IsRateControlled ? Rate : 1.0F);
    }
    public AudioClip NewClip(Inv.Sound Sound, float Volume = 1.0F, float Rate = 1.0F, bool Loop = false)
    {
      return new AudioClip(this, Sound, Volume, Rate, Loop);
    }

    internal Application Application { get; private set; }

    private bool IsMuted;
    private bool IsRateControlled;
    private bool IsVolumeControlled;
  }

  public sealed class AudioClip
  {
    internal AudioClip(Audio Audio, Inv.Sound Sound, float Volume, float Rate, bool Loop)
    {
      this.Audio = Audio;
      this.Sound = Sound;
      this.Volume = Volume;
      this.Rate = Rate;
      this.Loop = Loop;
    }

    public Sound Sound { get; private set; }
    public float Volume { get; private set; }
    public float Rate { get; private set; }
    public bool Loop { get; private set; }

    public void Play()
    {
      Audio.Application.Platform.AudioPlayClip(this);
    }
    /*
    public void Pause()
    {
      Audio.Application.Platform.AudioPauseClip(this);
    }
    public void Resume()
    {
      Audio.Application.Platform.AudioResumeClip(this);
    }*/
    public void Stop()
    {
      Audio.Application.Platform.AudioStopClip(this);
    }

    internal object Node { get; set; }

    private Audio Audio;
  }

  public sealed class Directory
  {
    internal Directory(Application Application)
    {
      this.Application = Application;
      this.RootFolder = new Folder(this, null);
    }

    public string Installation { get; internal set; }
    public Folder RootFolder { get; private set; }

    public Folder NewFolder(string Name)
    {
      return new Folder(this, Name);
    }
    public Asset NewAsset(string Name)
    {
      return new Asset(this, Name);
    }
    public DirectoryImagePicker NewImageFilePicker()
    {
      return new DirectoryImagePicker(this, FileType.Image);
    }
    public DirectoryFilePicker NewFilePicker()
    {
      return new DirectoryFilePicker(this, FileType.Any);
    }

    internal DirectoryFilePicker NewFilePicker(FileType FileType)
    {
      return new DirectoryFilePicker(this, FileType);
    }

    internal Application Application { get; private set; }
  }

  public enum FileType
  {
    Image,
    Any
  }

  public sealed class DirectoryFilePicker : Picker
  {
    internal DirectoryFilePicker(Directory Directory, FileType Type)
    {
      this.Directory = Directory;
      this.Type = Type;
    }

    public FileType Type { get; private set; }
    public string Title { get; set; }
    public event Action<Inv.Binary> SelectEvent;
    public event Action CancelEvent;

    public void Show()
    {
      Directory.Application.Platform.DirectoryShowFilePicker(this);
    }

    internal void SelectInvoke(Inv.Binary Binary)
    {
      if (SelectEvent != null)
        SelectEvent(Binary);
    }
    internal void CancelInvoke()
    {
      if (CancelEvent != null)
        CancelEvent();
    }

    private Directory Directory;
  }

  public sealed class DirectoryImagePicker
  {
    public DirectoryImagePicker(Directory Directory, FileType Type)
    {
      this.Base = new DirectoryFilePicker(Directory, Type);

      Base.SelectEvent += (Binary) =>
      {
        if (SelectEvent != null)
          SelectEvent(new Inv.Image(Binary.GetBuffer(), Binary.GetFormat()));
      };
    }

    public string Title
    {
      get { return Base.Title; }
      set { Base.Title = value; }
    }
    public event Action<Inv.Image> SelectEvent;
    public event Action CancelEvent
    {
      add { Base.CancelEvent += value; }
      remove { Base.CancelEvent -= value; }
    }

    public void Show()
    {
      Base.Show();
    }

    private DirectoryFilePicker Base;
  }

  public sealed class Folder
  {
    internal Folder(Directory Directory, string Name)
    {
      this.Directory = Directory;
      this.Name = Name;
    }

    public string Name { get; private set; }

    public File NewFile(string Name)
    {
      return new File(this, Name);
    }
    public IEnumerable<File> GetFiles(string Mask)
    {
      return Directory.Application.Platform.DirectoryGetFolderFiles(this, Mask);
    }

    internal Directory Directory { get; private set; }
  }

  public sealed class Asset
  {
    internal Asset(Directory Directory, string Name)
    {
      this.Directory = Directory;
      this.Name = Name;
    }

    public Directory Directory { get; private set; }
    public string Name { get; private set; }
    public string Title
    {
      get { return System.IO.Path.GetFileNameWithoutExtension(Name); }
    }
    public string Extension
    {
      get { return System.IO.Path.GetExtension(Name); }
    }

    public bool Exists()
    {
      return Directory.Application.Platform.DirectoryExistsAsset(this);
    }
    public System.IO.Stream Open()
    {
      return Directory.Application.Platform.DirectoryOpenAsset(this);
    }
  }

  public sealed class File
  {
    internal File(Folder Folder, string Name)
    {
      this.Folder = Folder;
      this.Name = Name;
    }

    public Folder Folder { get; private set; }
    public string Name { get; private set; }
    public string Title
    {
      get { return System.IO.Path.GetFileNameWithoutExtension(Name); }
    }
    public string Extension
    {
      get { return System.IO.Path.GetExtension(Name); }
    }

    public long GetLength()
    {
      return Folder.Directory.Application.Platform.DirectoryGetLengthFile(this);
    }
    public DateTime GetLastWriteTimeUtc()
    {
      return Folder.Directory.Application.Platform.DirectoryGetLastWriteTimeUtcFile(this);
    }
    public void SetLastWriteTimeUtc(DateTime Timestamp)
    {
      Folder.Directory.Application.Platform.DirectorySetLastWriteTimeUtcFile(this, Timestamp);
    }
    public System.IO.Stream Create()
    {
      return Folder.Directory.Application.Platform.DirectoryCreateFile(this);
    }
    public System.IO.Stream Open()
    {
      return Folder.Directory.Application.Platform.DirectoryOpenFile(this);
    }
    public System.IO.Stream Append()
    {
      return Folder.Directory.Application.Platform.DirectoryAppendFile(this);
    }
    public bool Exists()
    {
      return Folder.Directory.Application.Platform.DirectoryExistsFile(this);
    }
    public void Delete()
    {
      Folder.Directory.Application.Platform.DirectoryDeleteFile(this);
    }
    public void Copy(Inv.File CopyFile)
    {
      Folder.Directory.Application.Platform.DirectoryCopyFile(this, CopyFile);
    }
    public void CopyReplace(Inv.File CopyFile)
    {
      if (CopyFile.Exists())
        CopyFile.Delete();

      Folder.Directory.Application.Platform.DirectoryCopyFile(this, CopyFile);
    }
    public void Move(Inv.File MoveFile)
    {
      Folder.Directory.Application.Platform.DirectoryMoveFile(this, MoveFile);
    }
    public void MoveReplace(Inv.File MoveFile)
    {
      if (MoveFile.Exists())
        MoveFile.Delete();

      Folder.Directory.Application.Platform.DirectoryMoveFile(this, MoveFile);
    }
    public byte[] ReadAllBytes()
    {
      var Result = new byte[GetLength()];

      using (var Stream = Open())
        Stream.Read(Result, 0, Result.Length);

      return Result;
    }
    public void WriteAllBytes(byte[] Buffer)
    {
      using (var Stream = Create())
        Stream.Write(Buffer, 0, Buffer.Length);
    }

    public CsvFile AsCsv()
    {
      return new CsvFile(this);
    }
    public IniFile AsIni()
    {
      return new IniFile(this);
    }
    public TextFile AsText()
    {
      return new TextFile(this);
    }
    public CompactFile AsCompact()
    {
      return new CompactFile(this);
    }
  }

  public sealed class CompactFile
  {
    internal CompactFile(File Base)
    {
      this.Base = Base;
    }

    public Inv.CompactWriter Create()
    {
      return new Inv.CompactWriter(Base.Create());
    }
    public Inv.CompactReader Open()
    {
      return new Inv.CompactReader(Base.Open());
    }
    public Inv.CompactWriter Append()
    {
      return new Inv.CompactWriter(Base.Append());
    }

    private File Base;
  }

  public sealed class CsvFile
  {
    internal CsvFile(File Base)
    {
      this.Base = Base;
    }

    public Inv.CsvWriter Create()
    {
      return new Inv.CsvWriter(Base.Create());
    }
    public Inv.CsvReader Open()
    {
      return new Inv.CsvReader(Base.Open());
    }
    public Inv.CsvWriter Append()
    {
      return new Inv.CsvWriter(Base.Append());
    }

    private File Base;
  }

  public sealed class IniFile
  {
    internal IniFile(File Base)
    {
      this.Base = Base;
    }

    public Inv.IniWriter Create()
    {
      return new Inv.IniWriter(Base.Create());
    }
    public Inv.IniReader Open()
    {
      return new Inv.IniReader(Base.Open());
    }
    public Inv.IniWriter Append()
    {
      return new Inv.IniWriter(Base.Append());
    }

    private File Base;
  }

  public sealed class TextFile
  {
    internal TextFile(File Base)
    {
      this.Base = Base;
    }

    public System.IO.StreamWriter Create()
    {
      return new System.IO.StreamWriter(Base.Create());
    }
    public System.IO.StreamReader Open()
    {
      return new System.IO.StreamReader(Base.Open());
    }
    public System.IO.StreamWriter Append()
    {
      return new System.IO.StreamWriter(Base.Append());
    }
    public IEnumerable<string> ReadLines()
    {
      using (var TextReader = Open())
      {
        var FileLine = TextReader.ReadLine();

        while (FileLine != null)
        {
          yield return FileLine;

          FileLine = TextReader.ReadLine();
        }
      }
    }
    public string ReadAll()
    {
      if (Base.Exists())
      {
        using (var StreamReader = Open())
          return StreamReader.ReadToEnd();
      }
      else
      {
        return null;
      }
    }
    public void WriteAll(string Text)
    {
      using (var StreamWriter = Create())
        StreamWriter.Write(Text);
    }
    public TextSyntaxFile AsSyntax(Inv.Syntax.Grammar Grammar)
    {
      return new TextSyntaxFile(this, Grammar);
    }

    private File Base;
  }

  public sealed class TextSyntaxFile
  {
    internal TextSyntaxFile(TextFile Base, Inv.Syntax.Grammar Grammar)
    {
      this.Base = Base;
      this.Grammar = Grammar;
    }

    public void Write(Action<Inv.Syntax.Formatter> FormatAction)
    {
      using (var Stream = Base.Create())
        Inv.Syntax.Foundation.WriteTextStream(Grammar, Stream, FormatAction);
    }
    public void Read(Action<Inv.Syntax.Extractor> ExtractAction)
    {
      using (var Stream = Base.Open())
        Inv.Syntax.Foundation.ReadTextStream(Grammar, Stream, ExtractAction);
    }

    private TextFile Base;
    private Syntax.Grammar Grammar;
  }

  public enum TransitionAnimation
  {
    None,
    Fade,
    CarouselBack,
    CarouselNext
  }

  public sealed class Transition
  {
    internal Transition(Window Window)
    {
      this.Window = Window;
      this.FromSurface = Window.ActiveSurface;
      this.Animation = TransitionAnimation.None;
      this.Duration = Window.DefaultTransitionDuration;
    }

    public TransitionAnimation Animation { get; private set; }
    public TimeSpan Duration { get; set; }

    public void SetAnimation(TransitionAnimation Animation)
    {
      CheckThreadAffinity();

      this.Animation = Animation;
    }
    public void None()
    {
      SetAnimation(TransitionAnimation.None);
    }
    public void Fade()
    {
      SetAnimation(TransitionAnimation.Fade);
    }
    public void CarouselBack()
    {
      SetAnimation(TransitionAnimation.CarouselBack);
    }
    public void CarouselNext()
    {
      SetAnimation(TransitionAnimation.CarouselNext);
    }

    internal Surface FromSurface { get; private set; }

    private void CheckThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Window.CheckThreadAffinity();
    }

    private Window Window;
  }

  public sealed class Window
  {
    internal Window(Application Application)
    {
      this.Application = Application;
      this.ActiveTimerSet = new HashSet<Timer>();
      this.Background = new Background(this);
      this.DisplayRate = new WindowDisplayRate(this);
      this.DefaultTransitionDuration = TimeSpan.FromMilliseconds(300);
      this.KeyModifier = new KeyModifier();
      this.IsChanged = true;
    }

    public Application Application { get; private set; }
    public Background Background { get; private set; }
    public TimeSpan DefaultTransitionDuration { get; set; }
    public KeyModifier KeyModifier { get; private set; }
    public WindowDisplayRate DisplayRate { get; private set; }
    public Surface ActiveSurface { get; private set; }
    public int Width { get; internal set; }
    public int Height { get; internal set; }
    public event Action ProcessEvent;
    public event Action KeyModifierEvent;

    public bool IsActiveSurface(Inv.Surface Surface)
    {
      CheckThreadAffinity();

      return ActiveSurface == Surface;
    }
    public Surface NewSurface()
    {
      CheckThreadAffinity();

      return new Surface(this);
    }
    public Transition Transition(Surface Surface)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Surface.Window == this, "Surface must belong to this window.");

      if (ActiveTransition == null || ActiveSurface != Surface)
      {
        this.ActiveTransition = new Transition(this);
        this.ActiveSurface = Surface;
      }
 
      return ActiveTransition;
    }
    public Timer NewTimer()
    {
      CheckThreadAffinity();

      return new Timer(this);
    }
    public WindowTask NewTask(Action<WindowThread> Action)
    {
      CheckThreadAffinity();

      return new WindowTask(this, Action);
    }
    public WindowTask RunTask(Action<WindowThread> Action)
    {
      CheckThreadAffinity();

      var Result = NewTask(Action);
      
      Result.Run();

      return Result;
    }
    public void Browse(File File)
    {
      Application.Platform.WindowBrowse(File);
    }
    public void Post(Action Action)
    {
      Application.Platform.WindowPost(Action);
    }
    public void Call(Action Action)
    {
      Application.Platform.WindowCall(Action);
    }
    public void Sleep(TimeSpan Duration)
    {
      using (var WaitHandle = new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.ManualReset))
        WaitHandle.WaitOne(Duration);
    }

    internal Transition ActiveTransition { get; set; }
    internal HashSet<Timer> ActiveTimerSet { get; set; }

    internal bool Render()
    {
      CheckThreadAffinity();

      if (IsChanged)
      {
        this.IsChanged = false;
        return true;
      }

      return false;
    }
    internal void Change()
    {
      CheckThreadAffinity();

      this.IsChanged = true;
    }
    internal void ProcessInvoke()
    {
      if (ProcessEvent != null)
        ProcessEvent();
    }
    internal void CheckModifier(KeyModifier UpdateModifier)
    {
      if (!KeyModifier.IsEqual(UpdateModifier))
      {
        KeyModifier.SetFlags(UpdateModifier.GetFlags());

        if (KeyModifierEvent != null)
          KeyModifierEvent();
      }
    }

    internal void CheckThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Application.CheckThreadAffinity();
    }

    private bool IsChanged;
  }

  public sealed class WindowDisplayRate
  {
    internal WindowDisplayRate(Window Window)
    {
      this.Window = Window;
    }

    public int PerSecond
    {
      get { return LastCount; }
    }

    internal void Calculate()
    {
      Window.CheckThreadAffinity();

      var CurrentTick = System.Environment.TickCount;
      var Difference = CurrentTick - LastTick;

      if (Difference < 0 || Difference >= 1000)
      {
        this.LastCount = CurrentCount;
        this.CurrentCount = 0;
        this.LastTick = System.Environment.TickCount;
      }

      CurrentCount++;
    }

    private Window Window;
    private int LastTick;
    private int LastCount;
    private int CurrentCount;
  }

  public sealed class WindowTask
  {
    internal WindowTask(Window Window, Action<WindowThread> Action)
    {
      this.Window = Window;
      this.Base = new System.Threading.Tasks.Task(() =>
      {
        try
        {
          this.SW = Stopwatch.StartNew();
          try
          {
            Action(new WindowThread(this));
          }
          finally
          {
            SW.Stop();
          }
        }
        catch (Exception Exception)
        {
          Window.Application.HandleExceptionInvoke(Exception);

          // TODO: keep this exception to throw on Wait() ?
        }
      });
    }

    public TimeSpan Elapsed
    {
      get { return SW.Elapsed; }
    }
    public bool IsCompleted
    {
      get { return Base.IsCompleted; }
    }

    public void Run()
    {
      Window.CheckThreadAffinity();

      Base.Start();
    }
    public void Wait()
    {
      Window.CheckThreadAffinity();

      if (Base.Status == TaskStatus.Running)
        Base.Wait();
    }

    internal Window Window { get; private set; }
    internal Stopwatch SW { get; private set; }

    private Task Base;
  }

  public sealed class WindowThread
  {
    internal WindowThread(WindowTask Task)
    {
      this.Task = Task;
    }

    /// <summary>
    /// Sleep the thread so the task will run to the minimum duration.
    /// If the task has exceeded the minimum duration then no sleep is performed.
    /// </summary>
    /// <param name="MinimumDuration"></param>
    public void Yield(TimeSpan MinimumDuration)
    {
      // TODO: check the task's thread affinity.

      var Result = MinimumDuration - Task.SW.Elapsed;

      if (Result > TimeSpan.Zero)
        Sleep(Result);
    }
    public void Sleep(TimeSpan Duration)
    {
      using (var WaitHandle = new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.ManualReset))
        WaitHandle.WaitOne(Duration);
      //using (var WaitHandle = new System.Threading.ManualResetEventSlim(false))
      //  WaitHandle.Wait(Duration);
    }
    /// <summary>
    /// Post a delegate back to the main window thread.
    /// </summary>
    /// <param name="Action"></param>
    public void Post(Action Action)
    {
      Task.Window.Application.Platform.WindowPost(Action);
    }

    private WindowTask Task;
  }

  public enum Key
  {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    n0,
    n1,
    n2,
    n3,
    n4,
    n5,
    n6,
    n7,
    n8,
    n9,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    Period,
    Tilde,
    BackQuote,
    Asterisk,
    Comma,
    Escape,
    Enter,
    Tab,
    Space,
    Insert,
    Delete,
    Up,
    Down,
    Left,
    Right,
    Home,
    PageUp,
    End,
    PageDown,
    Clear,
    Slash,
    Backslash,
    Plus,
    Minus,
    Backspace,
    LeftShift,
    RightShift,
    LeftAlt,
    RightAlt,
    LeftCtrl,
    RightCtrl
  }

  public enum KeyModifierFlags : byte
  {
    None = 0x00,
    LeftShift = 0x01,
    RightShift = 0x02,
    LeftAlt = 0x04,
    RightAlt = 0x08,
    LeftCtrl = 0x10,
    RightCtrl = 0x20
  }

  public sealed class KeyModifier
  {
    internal KeyModifier(KeyModifierFlags Flags)
    {
      this.Flags = Flags;
    }
    internal KeyModifier()
    {
    }

    public bool IsLeftShift
    {
      get { return (Flags & KeyModifierFlags.LeftShift) > 0; }
      set 
      { 
        if (value)
          Flags |= KeyModifierFlags.LeftShift; 
        else
          Flags &= ~KeyModifierFlags.LeftShift; 
      }
    }
    public bool IsRightShift
    {
      get { return (Flags & KeyModifierFlags.RightShift) > 0; }
      set
      {
        if (value)
          Flags |= KeyModifierFlags.RightShift;
        else
          Flags &= ~KeyModifierFlags.RightShift;
      }
    }
    public bool IsShift
    {
      get { return (Flags & (KeyModifierFlags.LeftShift | KeyModifierFlags.RightShift)) > 0; }
    }
    public bool IsLeftAlt
    {
      get { return (Flags & KeyModifierFlags.LeftAlt) > 0; }
      set
      {
        if (value)
          Flags |= KeyModifierFlags.LeftAlt;
        else
          Flags &= ~KeyModifierFlags.LeftAlt;
      }
    }
    public bool IsRightAlt
    {
      get { return (Flags & KeyModifierFlags.RightAlt) > 0; }
      set
      {
        if (value)
          Flags |= KeyModifierFlags.RightAlt;
        else
          Flags &= ~KeyModifierFlags.RightAlt;
      }
    }
    public bool IsAlt
    {
      get { return (Flags & (KeyModifierFlags.LeftAlt | KeyModifierFlags.RightAlt)) > 0; }
    }
    public bool IsLeftCtrl
    {
      get { return (Flags & KeyModifierFlags.LeftCtrl) > 0; }
      set
      {
        if (value)
          Flags |= KeyModifierFlags.LeftCtrl;
        else
          Flags &= ~KeyModifierFlags.LeftCtrl;
      }
    }
    public bool IsRightCtrl
    {
      get { return (Flags & KeyModifierFlags.RightCtrl) > 0; }
      set
      {
        if (value)
          Flags |= KeyModifierFlags.RightCtrl;
        else
          Flags &= ~KeyModifierFlags.RightCtrl;
      }
    }
    public bool IsCtrl
    {
      get { return (Flags & (KeyModifierFlags.LeftCtrl | KeyModifierFlags.RightCtrl)) > 0; }
    }

    public override string ToString()
    {
      var Builder = new StringBuilder();

      if (IsLeftShift && IsRightShift)
        Builder.Append("shift + ");
      else if (IsLeftShift)
        Builder.Append("left shift + ");
      else if (IsRightShift)
        Builder.Append("right shift + ");

      if (IsLeftCtrl && IsRightCtrl)
        Builder.Append("ctrl + ");
      else if (IsLeftCtrl)
        Builder.Append("left ctrl + ");
      else if (IsRightCtrl)
        Builder.Append("right ctrl + ");

      if (IsLeftAlt && IsRightAlt)
        Builder.Append("alt + ");
      else if (IsLeftAlt)
        Builder.Append("left alt + ");
      else if (IsRightAlt)
        Builder.Append("right alt + ");

      if (Builder.Length == 0)
        return "";
      else
        return Builder.ToString().Substring(0, Builder.Length - 3);
    }

    public bool IsEqual(KeyModifier KeyModifier)
    {
      return Flags == KeyModifier.Flags;
    }

    internal KeyModifierFlags GetFlags()
    {
      return Flags;
    }
    internal void SetFlags(KeyModifierFlags Flags)
    {
      this.Flags = Flags;
    }

    private KeyModifierFlags Flags;
  }

  public sealed class Keystroke
  {
    internal Keystroke()
    {
    }

    public Key Key { get; internal set; }
    public KeyModifier Modifier { get; internal set; }

    public override string ToString()
    {
      var Result = Key.ToString();

      if (Modifier.IsCtrl)
        Result = "ctrl + " + Result;

      if (Modifier.IsAlt)
        Result = "alt + " + Result;

      if (Modifier.IsShift)
        Result = "shift + " + Result;

      return Result;
    }
  }

  public sealed class Surface 
  {
    internal Surface(Window Window)
    {
      this.Window = Window;
      this.PanelSet = new HashSet<Panel>();
      this.ChangedPanelList = new DistinctList<Panel>();
      this.StartAnimationSet = new HashSet<Animation>();
      this.StopAnimationSet = new HashSet<Animation>();
      this.Background = new Background(this);
      this.IsChanged = true;
    }

    public Window Window { get; private set; }
    public Background Background { get; private set; }
    public Panel Content
    {
      get { return ContentField; }
      set
      {
        if (ContentField != value)
        {
          if (ContentField != null)
            Detach(null, ContentField);

          this.ContentField = value;

          if (ContentField != null)
            Attach(null, ContentField);

          Change();
        }
      }
    }
    public event Action GestureBackwardEvent;
    public event Action GestureForwardEvent;
    public event Action<Keystroke> KeystrokeEvent;
    public event Action ArrangeEvent;
    public event Action ComposeEvent;

    public Animation NewAnimation()
    {
      CheckThreadAffinity();

      return new Animation(this);
    }
    public Board NewBoard()
    {
      CheckThreadAffinity();

      return new Board(this);
    }
    public Button NewButton()
    {
      CheckThreadAffinity();

      return new Button(this);
    }
    public Canvas NewCanvas()
    {
      CheckThreadAffinity();

      return new Canvas(this);
    }
    public Dock NewDock(DockOrientation Orientation)
    {
      CheckThreadAffinity();

      return new Dock(this, Orientation);
    }
    public Dock NewVerticalDock()
    {
      return NewDock(DockOrientation.Vertical);
    }
    public Dock NewHorizontalDock()
    {
      return NewDock(DockOrientation.Horizontal);
    }
    public Edit NewEdit(EditInput Input)
    {
      CheckThreadAffinity();

      return new Edit(this, Input);
    }
    public Edit NewTextEdit()
    {
      return NewEdit(EditInput.Text);
    }
    public Edit NewNumberEdit()
    {
      return NewEdit(EditInput.Number);
    }
    public Edit NewIntegerEdit()
    {
      return NewEdit(EditInput.Integer);
    }
    public Edit NewDecimalEdit()
    {
      return NewEdit(EditInput.Decimal);
    }
    public Edit NewNameEdit()
    {
      return NewEdit(EditInput.Name);
    }
    public Edit NewEmailEdit()
    {
      return NewEdit(EditInput.Email);
    }
    public Edit NewPhoneEdit()
    {
      return NewEdit(EditInput.Phone);
    }
    public Edit NewUriEdit()
    {
      return NewEdit(EditInput.Uri);
    }
    public Edit NewPasswordEdit()
    {
      return NewEdit(EditInput.Password);
    }
    public Edit NewSearchEdit()
    {
      return NewEdit(EditInput.Search);
    }
    public Graphic NewGraphic()
    {
      CheckThreadAffinity();

      return new Graphic(this);
    }
    public Label NewLabel()
    {
      CheckThreadAffinity();

      return new Label(this);
    }
    public Memo NewMemo()
    {
      CheckThreadAffinity();

      return new Memo(this);
    }
    public Overlay NewOverlay()
    {
      CheckThreadAffinity();

      return new Overlay(this);
    }
    public Browser NewBrowser()
    {
      CheckThreadAffinity();

      return new Browser(this);
    }
    public Scroll NewScroll(ScrollOrientation Orientation)
    {
      CheckThreadAffinity();

      return new Scroll(this, Orientation);
    }
    public Scroll NewVerticalScroll()
    {
      return NewScroll(ScrollOrientation.Vertical);
    }
    public Scroll NewHorizontalScroll()
    {
      return NewScroll(ScrollOrientation.Horizontal);
    }
    public Frame NewFrame()
    {
      CheckThreadAffinity();

      return new Frame(this);
    }
    public Stack NewStack(StackOrientation Orientation)
    {
      CheckThreadAffinity();

      return new Stack(this, Orientation);
    }
    public Stack NewHorizontalStack()
    {
      return NewStack(StackOrientation.Horizontal);
    }
    public Stack NewVerticalStack()
    {
      return NewStack(StackOrientation.Vertical);
    }
    public Table NewTable()
    {
      CheckThreadAffinity();

      return new Table(this);
    }
    public Flow NewFlow()
    {
      CheckThreadAffinity();

      return new Flow(this);
    }
    public void SetFocus(Panel Panel)
    {
      CheckThreadAffinity();

      this.Focus = Panel;
    }
    public void GestureBackward()
    {
      CheckThreadAffinity();

      GestureBackwardInvoke();
    }
    public void GestureForward()
    {
      CheckThreadAffinity();

      GestureForwardInvoke();
    }
    public int GetPanelCount()
    {
      CheckThreadAffinity();

      return PanelSet.Count;
    }
    public int GetPanelDepth()
    {
      CheckThreadAffinity();

      if (Content == null)
        return 0;
      else
        return MaxVisibleDepth(Content, 0);
    }
    public string GetPanelDisplay()
    {
      CheckThreadAffinity();

      var StringBuilder = new StringBuilder();

      StringBuilder.AppendLine("total panel count: " + GetPanelCount());
      StringBuilder.AppendLine("maximum visible panel depth: " + GetPanelDepth());
      StringBuilder.AppendLine();

      if (Content != null && Content.Visibility.Get())
        PanelDisplay(Content, StringBuilder, 0);
      else
        StringBuilder.AppendLine("empty");
      
      return StringBuilder.ToString();
    }
    public void Rearrange()
    {
      CheckThreadAffinity();

      ArrangeInvoke();
    }
    public Transition Transition()
    {
      return Window.Transition(this);
    }

    internal Panel Focus { get; set; }
    internal object Node { get; set; }
    internal HashSet<Panel> PanelSet { get; private set; }
    internal HashSet<Animation> StartAnimationSet { get; private set; }
    internal HashSet<Animation> StopAnimationSet { get; private set; }

    internal void KeystrokeInvoke(Keystroke Keystroke)
    {
      CheckThreadAffinity();

      if (KeystrokeEvent != null)
        KeystrokeEvent(Keystroke);
    }
    internal void GestureBackwardInvoke()
    {
      CheckThreadAffinity();

      if (GestureBackwardEvent != null)
        GestureBackwardEvent();
    }
    internal bool GestureBackwardQuery()
    {
      CheckThreadAffinity();

      return GestureBackwardEvent != null;
    }
    internal void GestureForwardInvoke()
    {
      CheckThreadAffinity();

      if (GestureForwardEvent != null)
        GestureForwardEvent();
    }
    internal bool GestureForwardQuery()
    {
      CheckThreadAffinity();

      return GestureForwardEvent != null;
    }
    internal void ComposeInvoke()
    {
      CheckThreadAffinity();

      if (ComposeEvent != null)
        ComposeEvent();
    }
    internal void ArrangeInvoke()
    {
      CheckThreadAffinity();

      if (ArrangeEvent != null)
        ArrangeEvent();
    }
    internal bool Render()
    {
      CheckThreadAffinity();

      if (IsChanged)
      {
        this.IsChanged = false;
        return true;
      }

      return false;
    }
    internal void Change()
    {
      CheckThreadAffinity();

      this.IsChanged = true;
    }
    internal void Attach(Inv.Panel Parent, Inv.Panel Child)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Parent == null || Parent.Surface == this, "Parent does not belong to this surface");
        Inv.Assert.Check(Child.Surface == this, "Child does not belong to this surface");
      }

      if (Parent == null || PanelSet.Contains(Parent))
        Attach(Child);
    }
    internal void Detach(Inv.Panel Parent, Inv.Panel Child)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Parent == null || Parent.Surface == this, "Parent does not belong to this surface");
        Inv.Assert.Check(Child.Surface == this, "Child does not belong to this surface");
      }

      if (Parent == null || PanelSet.Contains(Parent))
        Detach(Child);
    }
    internal void StartAnimation(Animation Animation)
    {
      CheckThreadAffinity();

      StopAnimationSet.Remove(Animation);
      StartAnimationSet.Add(Animation);
    }
    internal void StopAnimation(Animation Animation)
    {
      CheckThreadAffinity();

      StartAnimationSet.Remove(Animation);
      StopAnimationSet.Add(Animation);
    }
    internal void ProcessChanges(Action<Inv.Panel> Action)
    {
      CheckThreadAffinity();

      var ChangedPanelArray = ChangedPanelList.ToArray();
      ChangedPanelList.Clear();

      foreach (var ChangedPanel in ChangedPanelArray)
      {
        // some panels will exercise sub-panels which means we don't need to check them again.
        if (ChangedPanel.IsChanged)
          Action(ChangedPanel);
      }
    }
    internal void ChangePanel(Panel Panel)
    {
      CheckThreadAffinity();

      ChangedPanelList.Add(Panel);
    }
    internal void RenderPanel(Panel Panel)
    {
      CheckThreadAffinity();

      ChangedPanelList.Remove(Panel);
    }

    private void Attach(Panel Panel)
    {
      PanelSet.Add(Panel);
      foreach (var Child in Panel.GetChilds())
        Attach(Child);
    }
    private void Detach(Panel Panel)
    {
      PanelSet.Remove(Panel);
      foreach (var Child in Panel.GetChilds())
        Detach(Child);
    }
    private void PanelDisplay(Panel Panel, StringBuilder Builder, int Level)
    {
      var Indent = new string(' ', Level * 2);

      var VisibleChildrenArray = Panel.GetChilds().Where(C => C.Visibility.Get()).ToArray();

      if (VisibleChildrenArray.Length > 0)
      {
        Builder.AppendLine(Indent + Panel.DisplayType);

        foreach (var Child in VisibleChildrenArray)
          PanelDisplay(Child, Builder, Level + 1);
      }
      else
      {
        Builder.AppendLine(Indent + Panel.DisplayType + ";");
      }
    }
    private int MaxVisibleDepth(Panel Panel, int Depth)
    {
      var Result = Depth;

      if (Panel.Visibility.Get())
      {
        foreach (var Child in Panel.GetChilds())
        {
          var ChildDepth = MaxVisibleDepth(Child, Depth + 1);

          if (Result < ChildDepth)
            Result = ChildDepth;
        }
      }

      return Result;
    }

    internal void CheckThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Window.CheckThreadAffinity();
    }
    
    private Panel ContentField;
    private Inv.DistinctList<Panel> ChangedPanelList;
    private bool IsChanged;
  }

  public sealed class Animation
  {
    internal Animation(Surface Surface)
    {
      this.Surface = Surface;
      this.TargetList = new DistinctList<AnimationTarget>();
    }

    public bool IsActive { get; private set; }
    public event Action CompleteEvent;

    public void RemoveTargets()
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!IsActive, "Animation must not be active.");

      TargetList.Clear();
    }
    public AnimationTarget AddTarget(Panel Panel)
    {
      CheckThreadAffinity();

      var Result = new AnimationTarget(Panel);

      TargetList.Add(Result);

      return Result;
    }
    public void Start()
    {
      CheckThreadAffinity();

      if (!IsActive)
      {
        this.IsActive = true;
        Surface.StartAnimation(this);
      }
    }
    public void Stop()
    {
      CheckThreadAffinity();

      if (IsActive)
      {
        this.IsActive = false;
        Surface.StopAnimation(this);
      }
    }

    internal object Node { get; set; }
    internal int TargetCount
    {
      get { return TargetList.Count; }
    }

    internal IEnumerable<AnimationTarget> GetTargets()
    {
      CheckThreadAffinity();

      return TargetList;
    }
    internal void Complete()
    {
      CheckThreadAffinity();

      this.IsActive = false;

      if (CompleteEvent != null)
        CompleteEvent();
    }

    private void CheckThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Surface.CheckThreadAffinity();
    }

    private Inv.DistinctList<AnimationTarget> TargetList;
    private Surface Surface;
  }

  public sealed class AnimationTarget
  {
    internal AnimationTarget(Panel Panel)
    {
      this.Panel = Panel;
      this.CommandList = new Inv.DistinctList<AnimationCommand>();
    }

    internal Panel Panel { get; private set; }

    public AnimationCommand FadeOpacityOut(TimeSpan Duration, TimeSpan? Offset = null)
    {
      return FadeOpacity(1.0F, 0.0F, Duration, Offset);
    }
    public AnimationCommand FadeOpacityIn(TimeSpan Duration, TimeSpan? Offset = null)
    {
      return FadeOpacity(0.0F, 1.0F, Duration, Offset);
    }
    public AnimationCommand FadeOpacity(float From, float To, TimeSpan Duration, TimeSpan? Offset = null)
    {
      var Result = new AnimationOpacityCommand(this)
      {
        Duration = Duration,
        Offset = Offset,
        From = From,
        To = To
      };

      CommandList.Add(Result);

      return Result;
    }

    internal int CommandCount
    {
      get { return CommandList.Count; }
    }

    internal IEnumerable<AnimationCommand> GetCommands()
    {
      return CommandList;
    }

    private Inv.DistinctList<AnimationCommand> CommandList;
  }

  public enum AnimationType
  {
    Opacity
  }

  public abstract class AnimationCommand
  {
    internal AnimationCommand(AnimationTarget Target)
    {
      this.Target = Target;
    }

    //public event Action CompleteEvent;

    internal AnimationTarget Target { get; private set; }
    internal abstract AnimationType Type { get; }

    internal void Complete()
    {
      // TODO: do we need really this?

      //if (CompleteEvent != null)
      //  CompleteEvent();
    }
  }

  internal sealed class AnimationOpacityCommand : AnimationCommand
  {
    public AnimationOpacityCommand(AnimationTarget Target)
      : base(Target)
    {
    }

    public TimeSpan? Offset { get; internal set; }
    public TimeSpan Duration { get; internal set; }
    public float From { get; internal set; }
    public float To { get; internal set; }

    internal override AnimationType Type
    {
      get { return AnimationType.Opacity; }
    }
  }

  public enum PanelType
  {
    Board,
    Browser,
    Button,
    Canvas,
    Dock,
    Edit,
    Flow,
    Frame,
    Graphic,
    Label,
    Memo,
    Overlay,
    Scroll,
    Stack,
    Table
  }

  public abstract class Panel
  {
    internal Panel(Surface Surface)
    {
      this.Surface = Surface;
      this.Opacity = new Opacity(this);
      this.Background = new Background(this);
      this.Corner = new Corner(this);
      this.Border = new Border(this);
      this.Alignment = new Alignment(this);
      this.Visibility = new Visibility(this);
      this.Size = new Size(this);
      this.Margin = new Margin(this);
      this.Padding = new Padding(this);
      this.Elevation = new Elevation(this);
      this.Parent = null;
      this.ChildSet = null;
      //this.IsChanged = true; // NOTE: this assumes all platforms give correct defaults.
    }

    public Surface Surface { get; private set; }
    public Opacity Opacity { get; private set; }
    public Background Background { get; private set; }
    public Corner Corner { get; private set; }
    public Border Border { get; private set; }
    public Alignment Alignment { get; private set; }
    public Visibility Visibility { get; private set; }
    public Size Size { get; private set; }
    public Margin Margin { get; private set; }
    public Padding Padding { get; private set; }
    public Elevation Elevation { get; private set; }

    internal Panel Parent { get; private set; }
    internal object Node { get; set; }
    internal bool IsChanged { get; set; }
    internal abstract PanelType PanelType { get; }
    internal virtual string DisplayType
    {
      get { return PanelType.ToString().ToLower(); }
    }

    internal void Change()
    {
      CheckThreadAffinity();

      if (!IsChanged)
      {
        this.IsChanged = true;
        Surface.ChangePanel(this);

#if TRACE_PANEL_CHANGE
        if (GetType() != typeof(Render))
          Debug.WriteLine("CHANGE " + DisplayType);
#endif
      }
    }
    internal bool Render()
    {
      CheckThreadAffinity();

      if (IsChanged)
      {
        this.IsChanged = false;
        Surface.RenderPanel(this);

#if TRACE_PANEL_RENDER
        if (GetType() != typeof(Render))
          Debug.WriteLine("RENDER " + DisplayType);
#endif

        return true;
      }

      return false;
    }
    internal void AddChild(Panel Child)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(Child, "Child");
        Inv.Assert.Check(Child != this, "Parent must not add itself as a child.");
        Inv.Assert.Check(Child.Surface == Surface, "Child must belong to the owning surface.");
        Inv.Assert.Check(Child.Parent == null, "Child must not already have a parent.");
      }

      Child.Parent = this;

      if (ChildSet == null)
        ChildSet = new HashSet<Inv.Panel>();

      ChildSet.Add(Child);

      Surface.Attach(this, Child);
    }
    internal void RemoveChild(Panel Child)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(Child, "Child");
        Inv.Assert.Check(Child != this, "Parent must not remove itself as a child.");
        Inv.Assert.Check(Child.Surface == Surface, "Child must belong to the owning surface.");
        Inv.Assert.Check(Child.Parent == this, "Child must belong to this parent.");
      }

      Child.Parent = null;

      if (ChildSet != null)
        ChildSet.Remove(Child);

      Surface.Detach(this, Child);
    }
    internal bool HasChilds()
    {
      return ChildSet != null && ChildSet.Count > 0;
    }
    internal IEnumerable<Panel> GetChilds()
    {
      if (ChildSet != null)
        return ChildSet;
      else
        return new Panel[] { };
    }
    internal void Unparent()
    {
      Debug.Assert(Parent != null);

      if (Parent != null)
      {
        switch (Parent.PanelType)
        {
          case Inv.PanelType.Button:
            var Button = (Button)Parent;
            Debug.Assert(Button.Content == this, "Button content must match expected parent.");
            Button.Content = null;
            break;

          case Inv.PanelType.Board:
            var Board = (Board)Parent;
            Board.RemovePanel(this);
            break;

          case Inv.PanelType.Dock:
            var Dock = (Dock)Parent;
            Dock.RemovePanel(this);
            break;

          case Inv.PanelType.Overlay:
            var Overlay = (Overlay)Parent;
            Overlay.RemovePanel(this);
            break;

          case Inv.PanelType.Scroll:
            var Scroll = (Scroll)Parent;
            Scroll.Content = null;
            break;

          case Inv.PanelType.Table:
            var Table = (Table)Parent;
            Table.RemovePanel(this);
            break;

          case Inv.PanelType.Stack:
            var Stack = (Stack)Parent;
            Stack.RemovePanel(this);
            break;
            
          default:
            throw new Exception("PanelType not handled: " + Parent.PanelType);
        }
      }

      Debug.Assert(Parent == null);
    }

    protected void CheckThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Surface.CheckThreadAffinity();
    }

    private HashSet<Panel> ChildSet;
  }

  public sealed class Overlay : Panel
  {
    internal Overlay(Surface Surface)
      : base(Surface)
    {
      this.PanelCollection = new Inv.Collection<Panel>(this);
    }

    public void AddPanel(Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      PanelCollection.Add(Panel);
    }
    public void InsertPanel(int Index, Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      PanelCollection.Insert(Index, Panel);
    }
    public void InsertPanelBefore(Inv.Panel Before, Inv.Panel Panel)
    {
      CheckThreadAffinity();

      var PanelIndex = PanelCollection.IndexOf(Before);
      if (PanelIndex < 0)
        PanelIndex = 0;

      InsertPanel(PanelIndex, Panel);
    }
    public void InsertPanelAfter(Inv.Panel After, Inv.Panel Panel)
    {
      CheckThreadAffinity();

      var PanelIndex = PanelCollection.IndexOf(After) + 1;
      if (PanelIndex <= 0)
        PanelIndex = PanelCollection.Count;

      InsertPanel(PanelIndex, Panel);
    }
    public void RemovePanel(Panel Panel)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Panel, "Panel");

      if (Panel != null)
      {
        RemoveChild(Panel);
        PanelCollection.Remove(Panel);
      }
    }
    public void RemovePanels()
    {
      CheckThreadAffinity();

      if (PanelCollection.Count > 0)
      {
        foreach (var Element in PanelCollection)
          RemoveChild(Element);
        PanelCollection.Clear();
      }
    }
    public bool HasPanel(Panel Panel)
    {
      CheckThreadAffinity();

      return PanelCollection.Contains(Panel);
    }
    public IEnumerable<Panel> GetPanels()
    {
      CheckThreadAffinity();

      return PanelCollection;
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Overlay; }
    }
    internal Collection<Panel> PanelCollection { get; private set; }
  }

  public sealed class Browser : Panel
  {
    internal Browser(Surface Surface)
      : base(Surface)
    {
      this.UriSingleton = new Singleton<Uri>(this);
      this.HtmlSingleton = new Singleton<string>(this);
    }

    public Uri Uri
    {
      get { return UriSingleton.Data; }
    }
    public string Html
    {
      get { return HtmlSingleton.Data; }
    }

    public void LoadUri(Uri Uri)
    {
      CheckThreadAffinity();

      UriSingleton.Data = Uri;
      HtmlSingleton.Data = null;
    }
    public void LoadHtml(string Html)
    {
      CheckThreadAffinity();

      UriSingleton.Data = null;
      HtmlSingleton.Data = Html;
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Browser; }
    }
    internal Singleton<Uri> UriSingleton { get; private set; }
    internal Singleton<string> HtmlSingleton { get; private set; }
  }

  public sealed class Canvas : Panel
  {
    internal Canvas(Surface Surface)
      : base(Surface)
    {
      this.Redrawing = false;
    }

    public event Action<DrawContract> DrawEvent;
    public event Action<Point> SingleTapEvent;
    public event Action<Point> DoubleTapEvent;
    public event Action<Point> ContextTapEvent;
    public event Action<Point> PressEvent;
    public event Action<Point> ReleaseEvent;
    public event Action<Point> MoveEvent;
    public event Action<Point, int> ZoomEvent;

    public void Draw()
    {
      CheckThreadAffinity();

      this.Redrawing = true;
      Change();
    }
    public void SingleTap(Point Point)
    {
      SingleTapInvoke(Point);
    }
    public void DoubleTap(Point Point)
    {
      DoubleTapInvoke(Point);
    }
    public void ContextTap(Point Point)
    {
      ContextTapInvoke(Point);
    }
    public void Press(Point Point)
    {
      PressInvoke(Point);
    }
    public void Release(Point Point)
    {
      ReleaseInvoke(Point);
    }
    public void Move(Point Point)
    {
      MoveInvoke(Point);
    }
    public void Zoom(Point Point, int Delta)
    {
      ZoomInvoke(Point, Delta);
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Canvas; }
    }
    internal bool Redrawing { get; private set; }

    internal void PressInvoke(Point Point)
    {
      CheckThreadAffinity();

      if (PressEvent != null)
        PressEvent(Point);
    }
    internal void ReleaseInvoke(Point Point)
    {
      CheckThreadAffinity();

      if (ReleaseEvent != null)
        ReleaseEvent(Point);
    }
    internal void MoveInvoke(Point Point)
    {
      CheckThreadAffinity();

      if (MoveEvent != null)
        MoveEvent(Point);
    }
    internal void SingleTapInvoke(Point Point)
    {
      CheckThreadAffinity();

      if (SingleTapEvent != null)
        SingleTapEvent(Point);
    }
    internal void DoubleTapInvoke(Point Point)
    {
      CheckThreadAffinity();

      if (DoubleTapEvent != null)
        DoubleTapEvent(Point);
    }
    internal void ContextTapInvoke(Point Point)
    {
      CheckThreadAffinity();

      if (ContextTapEvent != null)
        ContextTapEvent(Point);
    }
    internal void ZoomInvoke(Point Point, int Delta)
    {
      CheckThreadAffinity();

      if (ZoomEvent != null)
        ZoomEvent(Point, Delta);
    }
    internal void DrawInvoke(DrawContract DrawContract)
    {
      this.Redrawing = false;

      if (DrawEvent != null)
        DrawEvent(DrawContract);
    }
  }

  public enum VerticalPosition
  {
    Top,
    Center,
    Bottom
  }

  public enum HorizontalPosition
  {
    Left,
    Center,
    Right
  }

  public enum Mirror
  {
    Vertical,
    Horizontal
  }

  public interface DrawContract
  {
    int Width { get; }
    int Height { get; }

    void DrawText(string TextFragment, string TextFontName, int TextFontSize, Inv.FontWeight TextFontWeight, Colour TextFontColour, Inv.Point TextPoint, Inv.HorizontalPosition TextHorizontal, Inv.VerticalPosition TextVertical);
    /// <summary>
    /// Draw one or more connected line segments.
    /// </summary>
    /// <param name="LineStrokeColour">The colour of the drawn lines</param>
    /// <param name="LineStrokeThickness">Thickness of the drawn lines</param>
    /// <param name="LineSourcePoint">Starting point of the first line</param>
    /// <param name="LineTargetPoint">Ending point of the first line</param>
    /// <param name="LineExtraPointArray">The extra point joins from the end of the first line and so on</param>
    void DrawLine(Inv.Colour LineStrokeColour, int LineStrokeThickness, Inv.Point LineSourcePoint, Inv.Point LineTargetPoint, params Inv.Point[] LineExtraPointArray); 
    void DrawRectangle(Inv.Colour RectangleFillColour, Inv.Colour RectangleStrokeColour, int RectangleStrokeThickness, Inv.Rect RectangleRect);
    /// <summary>
    /// Draw a wedge of an ellipse.
    /// </summary>
    /// <param name="ArcFillColour"></param>
    /// <param name="ArcStrokeColour"></param>
    /// <param name="ArcStrokeThickness"></param>
    /// <param name="ArcCenter"></param>
    /// <param name="ArcRadius"></param>
    /// <param name="ArcStartAngle">Starting angle between 0 and 360</param>
    /// <param name="ArcSweepAngle">Ending angle between 0 and 360</param>
    void DrawArc(Inv.Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Inv.Point ArcCenter, Inv.Point ArcRadius, float ArcStartAngle, float ArcSweepAngle);
    void DrawEllipse(Inv.Colour EllipseFillColour, Inv.Colour EllipseStrokeColour, int EllipseStrokeThickness, Inv.Point EllipseCenter, Inv.Point EllipseRadius);
    void DrawImage(Inv.Image ImageSource, Inv.Rect ImageRect, float ImageOpacity = 1.0F, Inv.Colour ImageTint = null, Inv.Mirror? ImageMirror = null);
    void DrawPolygon(Inv.Colour FillColour, Inv.Colour StrokeColour, int StrokeThickness, Inv.LineJoin LineJoin, Inv.Point StartPoint, params Inv.Point[] PointArray);
  }

  public sealed class Board : Panel
  {
    internal Board(Surface Surface)
      : base(Surface)
    {
      this.PinCollection = new Collection<BoardPin>(this);
    }

    public void SetPanel(Panel Panel, Rect Rect)
    {
      CheckThreadAffinity();

      PinCollection.RemoveWhere(E => E.Panel == Panel);
      PinCollection.Add(new BoardPin(Rect, Panel));
    }
    public void AddPanel(Panel Panel, Rect Rect)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      PinCollection.Add(new BoardPin(Rect, Panel));
    }
    public void RemovePanel(Panel Panel)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Panel, "Panel");

      if (Panel != null)
      {
        RemoveChild(Panel);
        PinCollection.RemoveWhere(E => E.Panel == Panel);
      }
    }
    public void RemovePanels()
    {
      CheckThreadAffinity();

      if (PinCollection.Count > 0)
      {
        foreach (var Element in PinCollection)
          RemoveChild(Element.Panel);
        PinCollection.Clear();
      }
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Board; }
    }
    internal Collection<BoardPin> PinCollection { get; private set; }
  }

  internal sealed class BoardPin
  {
    public BoardPin(Rect Rect, Panel Panel)
    {
      this.Rect = Rect;
      this.Panel = Panel;
    }

    public readonly Rect Rect;
    public readonly Panel Panel;
  }

  internal sealed class Singleton<T> : Element
  {
    internal Singleton(Panel Panel)
      : base(Panel)
    {
    }

    public T Data
    {
      get
      {
        CheckThreadAffinity(); 
        
        return DataField; 
      }
      set
      {
        CheckThreadAffinity();

        if (!object.Equals(DataField, value))
        {
          this.DataField = value;
          Change();
        }
      }
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Singleton; }
    }

    private T DataField;
  }

  internal sealed class Collection<T> : Element, IEnumerable<T>
  {
    internal Collection(Panel Panel)
      : base(Panel)
    {
      this.List = new Inv.DistinctList<T>();
    }

    public T this[int Index]
    {
      get 
      {
        CheckThreadAffinity();

        return List[Index]; 
      }
    }
    public int Count
    {
      get 
      {
        CheckThreadAffinity();

        return List.Count; 
      }
    }
    public void Clear()
    {
      CheckThreadAffinity();

      if (List.Count > 0)
      {
        List.Clear();
        Change();
      }
    }
    public void Add(T Item)
    {
      CheckThreadAffinity();

      List.Add(Item);
      Change();
    }
    public void Insert(int Index, T Item)
    {
      CheckThreadAffinity();

      List.Insert(Index, Item);
      Change();
    }
    public void Remove(T Item)
    {
      CheckThreadAffinity();

      if (List.Remove(Item))
        Change();
    }
    public void RemoveAt(int Index)
    {
      List.RemoveAt(Index);
    }
    public void RemoveWhere(Predicate<T> ItemPredicate)
    {
      CheckThreadAffinity();

      if (List.RemoveAll(ItemPredicate) > 0)
        Change();
    }
    public bool Contains(T Item)
    {
      CheckThreadAffinity();

      return List.Contains(Item);
    }
    public int IndexOf(T Item)
    {
      return List.IndexOf(Item);
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Collection; }
    }

    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
      CheckThreadAffinity();

      return List.GetEnumerator();
    }
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      CheckThreadAffinity();

      return List.GetEnumerator();
    }

    private Inv.DistinctList<T> List;
  }

  public enum DockOrientation
  {
    Horizontal,
    Vertical
  }

  public sealed class Dock : Panel
  {
    internal Dock(Surface Surface, DockOrientation Orientation)
      : base(Surface)
    {
      this.Orientation = Orientation;
      this.HeaderCollection = new Inv.Collection<Panel>(this);
      this.ClientCollection = new Inv.Collection<Panel>(this);
      this.FooterCollection = new Inv.Collection<Panel>(this);
    }

    public DockOrientation Orientation { get; private set; }
    public bool HasPanels()
    {
      CheckThreadAffinity();

      return HeaderCollection.Count > 0 || ClientCollection.Count > 0 || FooterCollection.Count > 0;
    }
    public void RemovePanels()
    {
      CheckThreadAffinity();

      RemoveHeaders();
      RemoveClients();
      RemoveFooters();
    }
    public void RemovePanel(Panel Panel)
    {
      CheckThreadAffinity();

      RemoveChild(Panel);
      HeaderCollection.Remove(Panel);
      ClientCollection.Remove(Panel);
      FooterCollection.Remove(Panel);
    }
    public void AddHeader(Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      HeaderCollection.Add(Panel);
    }
    public void InsertHeader(int Index, Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      HeaderCollection.Insert(Index, Panel);
    }
    public void RemoveHeader(Panel Panel)
    {
      CheckThreadAffinity();

      RemoveChild(Panel);
      HeaderCollection.Remove(Panel);
    }
    public Inv.Panel HeaderAt(int Index)
    {
      CheckThreadAffinity();

      return HeaderCollection[Index];
    }
    public bool HasHeader(Panel Panel)
    {
      CheckThreadAffinity();

      return HeaderCollection.Contains(Panel);
    }
    public IEnumerable<Panel> GetHeaders()
    {
      CheckThreadAffinity();

      return HeaderCollection;
    }
    public bool HasHeaders()
    {
      CheckThreadAffinity();

      return HeaderCollection.Count > 0;
    }
    public void RemoveHeaders()
    {
      CheckThreadAffinity();

      if (HeaderCollection.Count > 0)
      {
        foreach (var Header in HeaderCollection)
          RemoveChild(Header);
        HeaderCollection.Clear();
      }
    }
    public void AddClient(Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      ClientCollection.Add(Panel);
    }
    public void InsertClient(int Index, Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      ClientCollection.Insert(Index, Panel);
    }
    public void RemoveClient(Panel Panel)
    {
      CheckThreadAffinity();

      RemoveChild(Panel);
      ClientCollection.Remove(Panel);
    }
    public Inv.Panel ClientAt(int Index)
    {
      CheckThreadAffinity();

      return ClientCollection[Index];
    }
    public bool HasClient(Panel Panel)
    {
      CheckThreadAffinity();

      return ClientCollection.Contains(Panel);
    }
    public IEnumerable<Panel> GetClients()
    {
      CheckThreadAffinity();

      return ClientCollection;
    }
    public bool HasClients()
    {
      CheckThreadAffinity();

      return ClientCollection.Count > 0;
    }
    public void RemoveClients()
    {
      CheckThreadAffinity();

      if (ClientCollection.Count > 0)
      {
        foreach (var Client in ClientCollection)
          RemoveChild(Client);
        ClientCollection.Clear();
      }
    }
    public void AddFooter(Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      FooterCollection.Add(Panel);
    }
    public void InsertFooter(int Index, Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      FooterCollection.Insert(Index, Panel);
    }
    public void RemoveFooter(Panel Panel)
    {
      CheckThreadAffinity();

      RemoveChild(Panel);
      FooterCollection.Remove(Panel);
    }
    public Inv.Panel FooterAt(int Index)
    {
      CheckThreadAffinity();

      return FooterCollection[Index];
    }
    public bool HasFooter(Panel Panel)
    {
      CheckThreadAffinity();

      return FooterCollection.Contains(Panel);
    }
    public IEnumerable<Panel> GetFooters()
    {
      CheckThreadAffinity();

      return FooterCollection;
    }
    public bool HasFooters()
    {
      CheckThreadAffinity();

      return FooterCollection.Count > 0;
    }
    public void RemoveFooters()
    {
      CheckThreadAffinity();

      if (FooterCollection.Count > 0)
      {
        foreach (var Footer in FooterCollection)
          RemoveChild(Footer);
        FooterCollection.Clear();
      }
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Dock; }
    }
    internal override string DisplayType
    {
      get { return Orientation == DockOrientation.Vertical ? "v-dock" : "h-dock"; }
    }
    internal Collection<Panel> HeaderCollection { get; private set; }
    internal Collection<Panel> ClientCollection { get; private set; }
    internal Collection<Panel> FooterCollection { get; private set; }

    internal bool CollectionRender()
    {
      CheckThreadAffinity();

      var Result = false;
      
      if (HeaderCollection.Render())
        Result = true;

      if (ClientCollection.Render())
        Result = true;

      if (FooterCollection.Render())
        Result = true;

      return Result;
    }
  }

  public sealed class Flow : Panel
  {
    internal Flow(Surface Surface)
      : base(Surface)
    {
      this.SectionList = new DistinctList<Inv.FlowSection>();
      this.ReloadSectionList = new Inv.DistinctList<int>();
      this.ReloadItemList = new Inv.DistinctList<IndexPath>();
    }

    public int SectionCount
    {
      get { return SectionList.Count; }
    }
    public event Action<FlowRefresh> RefreshEvent
    {
      add
      {
        CheckThreadAffinity();
        RefreshDelegate += value;
        Change();
      }
      remove
      {
        CheckThreadAffinity();
        RefreshDelegate -= value;
        Change();
      }
    }

    public FlowSection AddSection()
    {
      CheckThreadAffinity();

      var Section = new FlowSection(this);
      SectionList.Add(Section);
      Reload();
      return Section;
    }
    public BatchedFlowSection<TItem> AddBatchedSection<TItem>()
    {
      CheckThreadAffinity();

      var Section = new BatchedFlowSection<TItem>(AddSection());
      Reload();
      return Section;
    }
    public void RemoveSection(FlowSection Section)
    {
      CheckThreadAffinity();

      if (SectionList.Remove(Section))
        Reload();
    }
    public void RemoveSections()
    {
      CheckThreadAffinity();

      if (SectionList.Count > 0)
      {
        SectionList.Clear();
        Reload();
      }
    }
    public IEnumerable<FlowSection> GetSections()
    {
      return SectionList;
    }
    public void Reload()
    {
      CheckThreadAffinity();

      this.IsReload = true;
      Change();
    }
    public void Refresh()
    {
      CheckThreadAffinity();

      this.IsRefresh = true;
      Change();
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Flow; }
    }
    internal bool IsReload { get; set; }
    internal bool IsRefresh { get; set; }
    internal int? ScrollSection { get; set; }
    internal int? ScrollIndex { get; set; }
    internal bool IsRefreshable
    { 
      get
      {
        CheckThreadAffinity();

        return RefreshDelegate != null; 
      }
    }
    internal DistinctList<int> ReloadSectionList { get; private set; }
    internal DistinctList<IndexPath> ReloadItemList { get; private set; }
    internal DistinctList<FlowSection> SectionList { get; private set; }

    internal void RefreshInvoke(FlowRefresh TileRefresh)
    {
      CheckThreadAffinity();

      if (RefreshDelegate != null)
        RefreshDelegate(TileRefresh);
    }
    internal void Reload(FlowSection Section)
    {
      CheckThreadAffinity();

      if (!IsReload)
      {
        var SectionIndex = SectionList.IndexOf(Section);
        if (SectionIndex != -1 && !ReloadSectionList.Contains(SectionIndex))
        {
          ReloadSectionList.Add(SectionIndex);
          Change();
        }
      }
    }
    internal void Reload(FlowSection Section, int StartIndex, int Count)
    {
      CheckThreadAffinity();

      if (!IsReload)
      {
        var SectionIndex = SectionList.IndexOf(Section);
        if (SectionIndex != -1 && !ReloadSectionList.Contains(SectionIndex))
        {
          ReloadItemList.AddRange(Enumerable.Range(StartIndex, Count).Select(Index => new IndexPath { Section = SectionIndex, Index = Index }).Where(Path => !ReloadItemList.Contains(Path)));
          Change();
        }
      }
    }
    internal void ScrollTo(FlowSection Section, int Index)
    {
      CheckThreadAffinity();

      var SectionIndex = SectionList.IndexOf(Section);
      if (SectionIndex != -1)
      {
        ScrollSection = SectionIndex;
        ScrollIndex = Index;
        Change();
      }
    }
    internal FlowSection GetSection(int SectionIndex)
    {
      return SectionList[SectionIndex];
    }
    
    private Action<FlowRefresh> RefreshDelegate;

    internal struct IndexPath
    {
      public int Section;
      public int Index;
    }
  }

  public sealed class FlowRefresh
  {
    internal FlowRefresh(Flow Flow, Action CompleteAction)
    {
      this.Flow = Flow;
      this.CompleteAction = CompleteAction;
    }

    public void Complete()
    {
      Flow.Surface.CheckThreadAffinity();

      CompleteAction();
    }

    private Flow Flow;
    private Action CompleteAction;
  }

  public sealed class FlowSection
  {
    internal FlowSection(Flow Flow)
    {
      this.Flow = Flow;
    }

    public int ItemCount { get; private set; }
    public Panel Header { get; private set; }
    public Panel Footer { get; private set; }
    public event Func<int, Panel> ItemQuery;

    public void SetItemCount(int ItemCount)
    {
      CheckThreadAffinity();

      if (this.ItemCount != ItemCount)
      {
        this.ItemCount = ItemCount;

        Reload();
      }
    }
    public void SetHeader(Panel HeaderPanel)
    {
      CheckThreadAffinity();

      if (HeaderPanel != Header)
      {
        this.Header = HeaderPanel;

        Flow.Reload(this);
      }
    }
    public void SetFooter(Panel FooterPanel)
    {
      CheckThreadAffinity();

      if (FooterPanel != Footer)
      {
        this.Footer = FooterPanel;

        Flow.Reload(this);
      }
    }
    public void Reload()
    {
      Flow.Reload(this);
    }
    public void ScrollToItemAtIndex(int Index)
    {
      Flow.ScrollTo(this, Index);
    }

    internal Flow Flow { get; private set; }

    internal void Reload(int StartIndex, int Count)
    {
      Flow.Reload(this, StartIndex, Count);
    }
    internal void CheckThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Flow.Surface.CheckThreadAffinity();
    }
    internal Panel ItemInvoke(int Item)
    {
      CheckThreadAffinity();

      if (ItemQuery != null)
        return ItemQuery(Item);

      return null;
    }
  }

  public sealed class BatchedFlowSection<TItem>
  {
    internal BatchedFlowSection(FlowSection Base)
    {
      this.Base = Base;
      this.Cache = new Dictionary<int, TItem>();
      this.RequestedBatches = new HashSet<int>();
      this.CancellationSource = new System.Threading.CancellationTokenSource();

      Base.ItemQuery += (Item) =>
      {
        RequestBatchAtIndex(Item);
        RequestBatchAtIndex(Item - BatchSize);
        RequestBatchAtIndex(Item + BatchSize);

        if (Cache.ContainsKey(Item))
        {
          var ItemPanel = ItemQueryInvoke(Cache[Item]);

#if DEBUG
         //var Placeholder = Base.Flow.Surface.NewFrame();
         //Placeholder.Content = ItemPanel;
         //Placeholder.Background.Colour = Inv.Colour.DarkGray;
         //Placeholder.Size.SetHeight(44);
         //return Placeholder;
#endif
          return ItemPanel;
        }
        else
        {
          var Placeholder = Base.Flow.Surface.NewFrame();
#if DEBUG
          Placeholder.Background.Colour = Inv.Colour.DimGray;
#endif
          //Placeholder.Size.SetHeight(44);
          return Placeholder;
        }
      };
    }

    public event Action<int, int, System.Threading.CancellationToken, Action<IEnumerable<TItem>>> RequestBatch;
    public event Func<TItem, Panel> ItemQuery;

    public void SetItemCount(int ItemCount)
    {
      if (ItemCount != Base.ItemCount)
      {
        ClearCache();
        Base.SetItemCount(ItemCount);
      }
    }
    public void SetHeader(Panel HeaderPanel)
    {
      Base.SetHeader(HeaderPanel);
    }
    public void SetFooter(Panel FooterPanel)
    {
      Base.SetFooter(FooterPanel);
    }
    public void Reload()
    {
      ClearCache();
      Base.Reload();
    }

    private void ClearCache()
    {
      CancellationSource.Cancel();
      CancellationSource = new System.Threading.CancellationTokenSource();
      Cache.Clear();
      RequestedBatches.Clear();
    }
    private void RequestBatchAtIndex(int Index)
    {
      if (Index < 0 || Base.ItemCount <= Index)
        return;
      
      var Batch = Index / BatchSize;

      if (!RequestedBatches.Contains(Batch))
      {
        RequestedBatches.Add(Batch);

        if (RequestBatch != null)
        {
          var StartIndex = Batch * BatchSize;
          var Count = Math.Min(Base.ItemCount - StartIndex, BatchSize);
          var Token = CancellationSource.Token;

          RequestBatch(StartIndex, Count, Token, (ResultItems) =>
          {
            Base.CheckThreadAffinity();

            if (Token.IsCancellationRequested)
              return;

            var CurrentIndex = StartIndex;
            foreach (var ResultItem in ResultItems)
              Cache.Add(CurrentIndex++, ResultItem);

            if (Inv.Assert.IsEnabled)
              Inv.Assert.Check(CurrentIndex - StartIndex == Count, "An incorrect number of items was passed to the RequestBatch callback");

            Base.Reload(StartIndex, Count);
          });
        }
      }
    }
    private Panel ItemQueryInvoke(TItem Item)
    {
      if (ItemQuery != null)
        return ItemQuery(Item);
      else
        return null;
    }

    private FlowSection Base;
    private Dictionary<int, TItem> Cache;
    private HashSet<int> RequestedBatches;
    private System.Threading.CancellationTokenSource CancellationSource;
    private const int BatchSize = 20; // TODO: Make this dynamic
    
    public static implicit operator FlowSection(BatchedFlowSection<TItem> Section)
    {
      return Section != null ? Section.Base : null;
    }
  }

  public enum StackOrientation
  {
    Horizontal,
    Vertical
  }

  public sealed class Stack : Panel
  {
    internal Stack(Surface Surface, StackOrientation Orientation)
      : base(Surface)
    {
      this.Orientation = Orientation;
      this.PanelCollection = new Inv.Collection<Panel>(this);
    }

    public StackOrientation Orientation { get; private set; }

    public void AddPanel(Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      PanelCollection.Add(Panel);
    }
    public void InsertPanel(int Index, Inv.Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      PanelCollection.Insert(Index, Panel);
    }
    public void RemovePanels()
    {
      CheckThreadAffinity();

      if (PanelCollection.Count > 0)
      {
        foreach (var Panel in PanelCollection)
          RemoveChild(Panel);
        PanelCollection.Clear();
      }
    }
    public IEnumerable<Panel> GetPanels()
    {
      CheckThreadAffinity();

      return PanelCollection;
    }
    public void RemovePanel(Panel Panel)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Panel, "Panel");

      if (Panel != null)
      {
        RemoveChild(Panel);
        PanelCollection.Remove(Panel);
      }
    }
    public void InsertPanelBefore(Inv.Panel Before, Inv.Panel Panel)
    {
      CheckThreadAffinity();

      var PanelIndex = PanelCollection.IndexOf(Before);
      if (PanelIndex < 0)
        PanelIndex = 0;

      InsertPanel(PanelIndex, Panel);
    }
    public void InsertPanelAfter(Inv.Panel After, Inv.Panel Panel)
    {
      CheckThreadAffinity();

      var PanelIndex = PanelCollection.IndexOf(After) + 1;
      if (PanelIndex <= 0)
        PanelIndex = PanelCollection.Count;

      InsertPanel(PanelIndex, Panel);
    }
    public bool HasPanel(Panel Panel)
    {
      CheckThreadAffinity();

      return PanelCollection.Contains(Panel);
    }
    public Inv.Panel PanelAt(int Index)
    {
      CheckThreadAffinity();

      return PanelCollection[Index];
    }
    public bool HasPanels()
    {
      CheckThreadAffinity();

      return PanelCollection.Count > 0;
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Stack; }
    }
    internal override string DisplayType
    {
      get { return Orientation == StackOrientation.Vertical ? "v-stack" : "h-stack"; }
    }
    internal Inv.Collection<Panel> PanelCollection { get; private set; }
  }

  public sealed class Frame : Panel
  {
    internal Frame(Surface Surface)
      : base(Surface)
    {
      this.ContentSingleton = new Singleton<Panel>(this);
    }

    public Panel Content
    {
      get { return ContentSingleton.Data; }
      set
      {
        CheckThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
            RemoveChild(ContentSingleton.Data);

          ContentSingleton.Data = value;

          if (ContentSingleton.Data != null)
            AddChild(ContentSingleton.Data);
        }
      }
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Frame; }
    }
    internal Singleton<Panel> ContentSingleton { get; private set; }
  }

  public enum ScrollOrientation
  {
    Horizontal,
    Vertical
  }

  public sealed class Scroll : Panel
  {
    internal Scroll(Surface Surface, ScrollOrientation Orientation)
      : base(Surface)
    {
      this.Orientation = Orientation;
      this.ContentSingleton = new Singleton<Panel>(this);
    }

    public ScrollOrientation Orientation { get; private set; }

    public Panel Content
    {
      get { return ContentSingleton.Data; }
      set
      {
        CheckThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
            RemoveChild(ContentSingleton.Data);

          ContentSingleton.Data = value;

          if (ContentSingleton.Data != null)
            AddChild(ContentSingleton.Data);
        }
      }
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Scroll; }
    }
    internal override string DisplayType
    {
      get { return Orientation == ScrollOrientation.Vertical ? "v-scroll" : "h-scroll"; }
    }
    internal Singleton<Panel> ContentSingleton { get; private set; }
  }

  public sealed class Table : Panel
  {
    internal Table(Surface Surface)
      : base(Surface)
    {
      this.RowCollection = new Collection<TableRow>(this);
      this.ColumnCollection = new Collection<TableColumn>(this);
      this.CellCollection = new Collection<TableCell>(this);
      this.CellGrid = new Grid<TableCell>();
    }

    public int ColumnCount
    {
      get { return CellGrid.Width; }
    }
    public int RowCount
    {
      get { return CellGrid.Height; }
    }

    public void RemovePanel(Panel Panel)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Panel, "Panel");

      if (Panel != null)
      {
        foreach (var Row in RowCollection)
        {
          if (Row.Content == Panel)
            Row.Content = null;
        }

        foreach (var Column in ColumnCollection)
        {
          if (Column.Content == Panel)
            Column.Content = null;
        }

        foreach (var Cell in CellCollection)
        {
          if (Cell.Content == Panel)
            Cell.Content = null;
        }
      }
    }
    public void RemovePanels()
    {
      CheckThreadAffinity();

      if (RowCollection.Count > 0)
      {
        foreach (var Row in RowCollection)
        {
          if (Row.Content != null)
            RemoveChild(Row.Content);
        }
        RowCollection.Clear();
      }

      if (ColumnCollection.Count > 0)
      {
        foreach (var Column in ColumnCollection)
        {
          if (Column.Content != null)
            RemoveChild(Column.Content);
        }
        ColumnCollection.Clear();
      }

      if (CellCollection.Count > 0)
      {
        foreach (var Cell in CellCollection)
        {
          if (Cell.Content != null)
            RemoveChild(Cell.Content);
        }
        CellCollection.Clear();
      }

      CellGrid.Resize(0, 0);
    }
    public TableRow AddRow()
    {
      CheckThreadAffinity();

      var Result = new TableRow(this, RowCollection.Count);
      RowCollection.Add(Result);

      CellGrid.Resize(CellGrid.Width, CellGrid.Height + 1);
      foreach (var Column in ColumnCollection)
        CellGrid[Column.Index, Result.Index] = NewCell(Column, Result);

      return Result;
    }
    public TableRow InsertRow(int Index)
    {
      CheckThreadAffinity();

      if (Index < 0)
        Index = 0;
      if (Index >= RowCollection.Count)
        return AddRow();

      foreach (var Row in RowCollection.Where(R => R.Index >= Index))
        Row.Index++;

      var Result = new TableRow(this, Index);
      RowCollection.Insert(Index, Result);

      CellGrid.Resize(CellGrid.Width, CellGrid.Height + 1);

      for (var RowIndex = RowCollection.Count - 2; RowIndex >= Index; RowIndex--)
        foreach (var Column in ColumnCollection)
          CellGrid[Column.Index, RowIndex + 1] = CellGrid[Column.Index, RowIndex];

      foreach (var Column in ColumnCollection)
        CellGrid[Column.Index, Index] = NewCell(Column, Result);

      return Result;
    }
    public void RemoveRow(TableRow Row)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(RowCollection.Contains(Row), "Rows can only be removed from their own table");

      CellCollection.RemoveWhere(Cell => Cell.Row.Index == Row.Index);

      for (var RowIndex = Row.Index; RowIndex < RowCollection.Count - 1; RowIndex++)
      {
        foreach (var Column in ColumnCollection)
          CellGrid[Column.Index, RowIndex] = CellGrid[Column.Index, RowIndex + 1];
      }

      RowCollection.Remove(Row);
      CellGrid.Resize(CellGrid.Width, CellGrid.Height - 1);

      foreach (var MovedRow in RowCollection.Where(R => R.Index > Row.Index))
        MovedRow.Index--;

      Debug.Assert(CellGrid.Width * CellGrid.Height == CellCollection.Count);
    }
    public TableRow GetRow(int Index)
    {
      CheckThreadAffinity();

      return RowCollection[Index];
    }
    public IEnumerable<TableRow> GetRows()
    {
      CheckThreadAffinity();

      return RowCollection;
    }
    public TableColumn AddColumn()
    {
      CheckThreadAffinity();

      var Result = new TableColumn(this, ColumnCollection.Count);
      ColumnCollection.Add(Result);

      CellGrid.Resize(CellGrid.Width + 1, CellGrid.Height);
      foreach (var Row in RowCollection)
        CellGrid[Result.Index, Row.Index] = NewCell(Result, Row);

      return Result;
    }
    public TableColumn InsertColumn(int Index)
    {
      CheckThreadAffinity();

      if (Index < 0)
        Index = 0;
      if (Index >= ColumnCollection.Count)
        return AddColumn();

      foreach (var Column in ColumnCollection.Where(C => C.Index >= Index))
        Column.Index++;

      var Result = new TableColumn(this, Index);
      ColumnCollection.Insert(Index, Result);

      CellGrid.Resize(CellGrid.Width + 1, CellGrid.Height);

      for (var ColumnIndex = ColumnCollection.Count - 2; ColumnIndex >= Index; ColumnIndex--)
        foreach (var Row in RowCollection)
          CellGrid[ColumnIndex + 1, Row.Index] = CellGrid[ColumnIndex, Row.Index];

      foreach (var Row in RowCollection)
        CellGrid[Index, Row.Index] = NewCell(Result, Row);

      return Result;
    }
    public void RemoveColumn(TableColumn Column)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(ColumnCollection.Contains(Column), "Columns can only be removed from their own table");

      CellCollection.RemoveWhere(Cell => Cell.Column.Index == Column.Index);

      for (var ColumnIndex = Column.Index; ColumnIndex < ColumnCollection.Count - 1; ColumnIndex++)
        foreach (var Row in RowCollection)
          CellGrid[ColumnIndex, Row.Index] = CellGrid[ColumnIndex + 1, Row.Index];

      ColumnCollection.Remove(Column);
      CellGrid.Resize(CellGrid.Width - 1, CellGrid.Height);

      foreach (var MovedColumn in ColumnCollection.Where(C => C.Index > Column.Index))
        MovedColumn.Index--;
      
      Debug.Assert(CellGrid.Width * CellGrid.Height == CellCollection.Count);
    }
    public TableColumn GetColumn(int Index)
    {
      CheckThreadAffinity();

      return ColumnCollection[Index];
    }
    public IEnumerable<TableColumn> GetColumns()
    {
      CheckThreadAffinity();

      return ColumnCollection;
    }
    public TableCell GetCell(int ColumnIndex, int RowIndex)
    {
      CheckThreadAffinity();

      return GetCell(GetColumn(ColumnIndex), GetRow(RowIndex));
    }
    public TableCell GetCell(TableColumn Column, TableRow Row)
    {
      CheckThreadAffinity();

      return CellGrid[Column.Index, Row.Index];
    }
    public IEnumerable<TableCell> GetCells()
    {
      CheckThreadAffinity();

      return CellGrid;
    }
    public void Adjust(int Columns, int Rows)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Columns >= 0, "Columns must be zero or more.");
        Inv.Assert.Check(Rows >= 0, "Rows must be zero or more.");
      }

      if (Columns != ColumnCount || Rows != RowCount)
      {
        CellGrid.Resize(Columns, Rows);

        while (ColumnCollection.Count > Columns)
        {
          var Column = ColumnCollection[ColumnCollection.Count - 1];
          CellCollection.RemoveWhere(R => R.Column == Column);
          ColumnCollection.RemoveAt(ColumnCollection.Count - 1);
        }

        while (ColumnCollection.Count < Columns)
          ColumnCollection.Add(new TableColumn(this, ColumnCollection.Count));

        while (RowCollection.Count > Rows)
        {
          var Row = RowCollection[RowCollection.Count - 1];
          CellCollection.RemoveWhere(R => R.Row == Row);
          RowCollection.RemoveAt(RowCollection.Count - 1);
        }

        while (RowCollection.Count < Rows)
          RowCollection.Add(new TableRow(this, RowCollection.Count));

        // TODO: this could be more efficient - only new rows and columns need cells.
        for (var Y = 0; Y < Rows; Y++)
        {
          for (var X = 0; X < Columns; X++)
          {
            if (CellGrid[X, Y] == null)
              CellGrid[X, Y] = NewCell(ColumnCollection[X], RowCollection[Y]);
          }
        }
      }

      Debug.Assert(RowCollection.Count == Rows);
      Debug.Assert(ColumnCollection.Count == Columns);
      Debug.Assert(CellGrid.Width * CellGrid.Height == CellCollection.Count);
    }
    public void Compose(Inv.Panel[,] PanelArray)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(PanelArray.ExceptNull().IsDistinct(), "Table.Compose must be distinct panels without any spanning cells.");

      Adjust(PanelArray.GetLength(1), PanelArray.GetLength(0));

      foreach (var Cell in CellCollection)
        Cell.Content = PanelArray[Cell.Row.Index, Cell.Column.Index];
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Table; }
    }
    internal Inv.Collection<TableRow> RowCollection { get; private set; }
    internal Inv.Collection<TableColumn> ColumnCollection { get; private set; }
    internal Inv.Collection<TableCell> CellCollection { get; private set; }

    internal bool CollectionRender()
    {
      CheckThreadAffinity();

      var Result = false;

      if (RowCollection.Render())
        Result = true;

      if (ColumnCollection.Render())
        Result = true;

      if (CellCollection.Render())
        Result = true;

      if (CellCollection.Any(C => C.IsChanged))
        Result = true;

      return Result;
    }

    private TableCell NewCell(TableColumn Column, TableRow Row)
    {
      var Result = new TableCell(this, Column, Row);

      CellCollection.Add(Result);

      return Result;
    }

    private Inv.Grid<TableCell> CellGrid;
  }

  public sealed class TableRow : TableAxis
  {
    internal TableRow(Table Table, int Index)
      : base(Table, Index)
    {
    }

    public IEnumerable<TableCell> GetCells()
    {
      return Table.CellCollection.Where(C => C.Row == this);
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.TableRow; }
    }
  }

  public sealed class TableColumn : TableAxis
  {
    internal TableColumn(Table Table, int Index)
      : base(Table, Index)
    {
    }

    public IEnumerable<TableCell> GetCells()
    {
      return Table.CellCollection.Where(C => C.Column == this);
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.TableColumn; }
    }
  }

  public sealed class TableCell : Element
  {
    internal TableCell(Panel Panel, TableColumn Column, TableRow Row)
      : base(Panel)
    {
      this.Column = Column;
      this.Row = Row;
      this.ContentSingleton = new Singleton<Inv.Panel>(Panel);
    }

    public int X
    {
      get { return Column.Index; }
    }
    public int Y
    {
      get { return Row.Index; }
    }
    public TableColumn Column { get; private set; }
    public TableRow Row { get; private set; }
    public Inv.Panel Content
    {
      get { return ContentSingleton.Data; }
      set
      {
        CheckThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
            Panel.RemoveChild(ContentSingleton.Data);

          ContentSingleton.Data = value;

          if (ContentSingleton.Data != null)
            Panel.AddChild(ContentSingleton.Data);
        }
      }
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.TableCell; }
    }
    internal Singleton<Inv.Panel> ContentSingleton { get; private set; }
  }

  public enum TableAxisLength
  {
    Star,
    Auto,
    Fixed
  }

  public abstract class TableAxis : Element
  {
    internal TableAxis(Table Table, int Index)
      : base(Table)
    {
      this.Table = Table;
      this.LengthType = TableAxisLength.Auto;
      this.LengthValue = 0;
      this.Index = Index;
      this.ContentSingleton = new Singleton<Inv.Panel>(Table);
    }
    public int Index { get; internal set; }
    public Inv.Panel Content
    {
      get { return ContentSingleton.Data; }
      set
      {
        CheckThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
            Panel.RemoveChild(ContentSingleton.Data);

          ContentSingleton.Data = value;

          if (ContentSingleton.Data != null)
            Panel.AddChild(ContentSingleton.Data);
        }
      }
    }

    public void Auto()
    {
      Set(TableAxisLength.Auto, 0);
    }
    public void Fixed(int Points)
    {
      Set(TableAxisLength.Fixed, Points);
    }
    public void Star(int Units = 1)
    {
      Set(TableAxisLength.Star, Units);
    }

    internal Table Table { get; private set; }
    internal TableAxisLength LengthType { get; private set; }
    internal int LengthValue { get; private set; }
    internal Singleton<Inv.Panel> ContentSingleton { get; private set; }

    internal void Set(TableAxisLength Type, int Value)
    {
      CheckThreadAffinity();

      if (Type != this.LengthType || Value != this.LengthValue)
      {
        this.LengthType = Type;
        this.LengthValue = Value;
        Change();
      }
    }
  }

  public sealed class Button : Panel
  {
    internal Button(Surface Surface)
      : base(Surface)
    {
      this.ContentSingleton = new Singleton<Panel>(this);
      this.IsEnabledField = true;
      this.IsFocusableField = false;
    }

    public Panel Content
    {
      get { return ContentSingleton.Data; }
      set
      {
        CheckThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
            RemoveChild(ContentSingleton.Data);

          ContentSingleton.Data = value;

          if (ContentSingleton.Data != null)
            AddChild(ContentSingleton.Data);
        }
      }
    }
    public bool IsEnabled
    {
      get
      {
        CheckThreadAffinity();

        return IsEnabledField; 
      }
      set
      {
        CheckThreadAffinity();

        if (IsEnabledField != value)
        {          
          this.IsEnabledField = value;
          Change();
        }
      }
    }
    public bool IsFocusable
    {
      get
      {
        CheckThreadAffinity();

        return IsFocusableField; 
      }
      set
      {
        CheckThreadAffinity();

        if (IsFocusableField != value)
        {
          this.IsFocusableField = value;
          Change();
        }
      }
    }
    public event Action SingleTapEvent;
    public event Action ContextTapEvent;
    public event Action PressEvent
    {
      add
      {
        CheckThreadAffinity();
        PressDelegate += value;
        Change();
      }
      remove
      {
        CheckThreadAffinity();
        PressDelegate += value;
        Change();
      }
    }
    public event Action ReleaseEvent
    {
      add
      {
        CheckThreadAffinity();
        ReleaseDelegate += value;
        Change();
      }
      remove
      {
        CheckThreadAffinity();
        ReleaseDelegate += value;
        Change();
      }
    }

    public void SingleTap()
    {
      SingleTapInvoke();
    }
    public void ContextTap()
    {
      ContextTapInvoke();
    }
    public void Press()
    {
      PressInvoke();
    }
    public void Release()
    {
      ReleaseInvoke();
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Button; }
    }
    internal Singleton<Panel> ContentSingleton { get; private set; }
    internal bool HasPress
    {
      get { return PressDelegate != null; }
    }
    internal bool HasRelease
    {
      get { return ReleaseDelegate != null; }
    }

    internal void SingleTapInvoke()
    {
      CheckThreadAffinity();

      if (SingleTapEvent != null)
        SingleTapEvent();
    }
    internal void ContextTapInvoke()
    {
      CheckThreadAffinity();

      if (ContextTapEvent != null)
        ContextTapEvent();
    }
    internal void PressInvoke()
    {
      CheckThreadAffinity();

      if (PressDelegate != null)
        PressDelegate();
    }
    internal void ReleaseInvoke()
    {
      CheckThreadAffinity();

      if (ReleaseDelegate != null)
        ReleaseDelegate();
    }

    private bool IsEnabledField;
    private bool IsFocusableField;
    private Action PressDelegate;
    private Action ReleaseDelegate;
  }

  public enum Justification
  {
    Left,
    Center,
    Right
  }

  public sealed class Timer
  {
    internal Timer(Window Window)
    {
      this.Window = Window;
    }

    public event Action IntervalEvent;
    public TimeSpan IntervalTime { get; set; }
    public bool IsEnabled { get; private set; }

    public void Start()
    {
      CheckThreadAffinity();

      if (!IsEnabled)
      {
        this.IsEnabled = true;
        Window.ActiveTimerSet.Add(this);
      }
    }
    public void Stop()
    {
      CheckThreadAffinity();

      if (IsEnabled)
      {
        this.IsEnabled = false;
        // NOTE: the platform engine must remove the timer from the active set once it has been stopped.
      }
    }
    public void Restart()
    {
      CheckThreadAffinity();

      if (!IsEnabled)
        Start();
      else
        this.IsRestarting = true;
    }

    internal object Node { get; set; }
    internal bool IsRestarting { get; set; }

    internal void IntervalInvoke()
    {
      var IntervalDelegate = IntervalEvent;

      if (IntervalDelegate != null)
        IntervalDelegate();
    }

    private void CheckThreadAffinity()
    {
      Window.CheckThreadAffinity();
    }

    private Window Window;
  }

  public sealed class Label : Panel
  {
    internal Label(Surface Surface)
      : base(Surface)
    {
      this.Font = new Font(this);
      this.LineWrappingField = true;
      this.Justification = Inv.Justification.Left;
    }

    public Font Font { get; private set; }
    public Justification Justification { get; private set; }
    public string Text
    {
      get
      {
        CheckThreadAffinity(); 
        
        return TextField; 
      }
      set
      {
        CheckThreadAffinity();

        if (TextField != value)
        {
          this.TextField = value;
          Change();
        }
      }
    }
    public bool LineWrapping
    {
      get
      {
        CheckThreadAffinity(); 
        
        return LineWrappingField; 
      }
      set
      {
        CheckThreadAffinity();

        if (LineWrappingField != value)
        {
          this.LineWrappingField = value;
          Change();
        }
      }
    }

    public void Justify(Justification Justification)
    {
      CheckThreadAffinity();

      if (this.Justification != Justification)
      {
        this.Justification = Justification;
        Change();
      }
    }
    public void JustifyLeft()
    {
      Justify(Inv.Justification.Left);
    }
    public void JustifyCenter()
    {
      Justify(Inv.Justification.Center);
    }
    public void JustifyRight()
    {
      Justify(Inv.Justification.Right);
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Label; }
    }
    internal override string DisplayType
    {
      get { return "label:" + Text.ConvertToCSharpString(); }
    }

    private string TextField;
    private bool LineWrappingField;
  }

  public sealed class Graphic : Panel
  {
    internal Graphic(Surface Surface)
      : base(Surface)
    {
      this.ImageSingleton = new Singleton<Image>(this);
    }

    public Inv.Image Image
    {
      get { return ImageSingleton.Data;  }
      set { this.ImageSingleton.Data = value; }
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Graphic; }
    }
    internal override string DisplayType
    {
      get { return "graphic" + (Image != null ? ":" + Image.GetBuffer().Length : ""); }
    }
    internal Singleton<Inv.Image> ImageSingleton { get; private set; }
  }

  public enum EditInput
  {
    Decimal,
    Email,
    Integer,
    Name,
    Number,
    Password,
    Phone,
    Search,
    Text,
    Uri
  }

  public sealed class Edit : Panel
  {
    internal Edit(Surface Surface, EditInput Input)
      : base(Surface)
    {
      this.Input = Input;
      this.Font = new Font(this);
    }

    public EditInput Input { get; private set; }
    public Font Font { get; private set; }
    public bool IsReadOnly
    {
      get
      {
        CheckThreadAffinity();

        return IsReadOnlyField; 
      }
      set
      {
        CheckThreadAffinity();

        if (IsReadOnlyField != value)
        {
          this.IsReadOnlyField = value;
          Change();
        }
      }
    }
    public string Text
    {
      get
      {
        CheckThreadAffinity(); 
        
        return TextField; 
      }
      set
      {
        CheckThreadAffinity();

        if (TextField != value)
        {
          this.TextField = value;
          Change();
          ChangeInvoke();
        }
      }
    }
    public event Action ChangeEvent
    {
      add
      {
        CheckThreadAffinity();
        ChangeDelegate += value;
        Change();
      }
      remove
      {
        CheckThreadAffinity();
        ChangeDelegate += value;
        Change();
      }
    }
    public bool HasChange
    {
      get { return ChangeDelegate != null; }
    }
    public event Action ReturnEvent
    {
      add
      {
        CheckThreadAffinity();
        ReturnDelegate += value;
        Change();
      }
      remove
      {
        CheckThreadAffinity();
        ReturnDelegate += value;
        Change();
      }
    }
    public bool HasReturn
    {
      get { return ReturnDelegate != null; }
    }

    public void Return()
    {
      CheckThreadAffinity();

      if (ReturnDelegate != null)
        ReturnDelegate();
    }

    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Edit; }
    }
    internal override string DisplayType
    {
      get { return "edit:" + Text.ConvertToCSharpString(); }
    }

    internal void UpdateText(string Text)
    {
      // update the Text, mark as changed, but don't invoke ChangeDelegate.
      CheckThreadAffinity();

      if (TextField != Text)
      {
        this.TextField = Text;
        Change();
      }
    }
    internal void ChangeText(string Text)
    {
      // update the Text, but don't mark as changed, and invoke ChangeDelegate.
      CheckThreadAffinity();

      if (TextField != Text)
      {
        this.TextField = Text;

        ChangeInvoke();
      }
    }

    private void ChangeInvoke()
    {
      if (ChangeDelegate != null)
        ChangeDelegate();
    }

    private string TextField;
    private bool IsReadOnlyField;
    private Action ChangeDelegate;
    private Action ReturnDelegate;
  }

  public sealed class Memo : Panel
  {
    internal Memo(Surface Surface)
      : base(Surface)
    {
      this.Font = new Font(this);
    }

    public Font Font { get; private set; }
    public string Text
    {
      get
      {
        CheckThreadAffinity(); 
        
        return TextField; 
      }
      set
      {
        CheckThreadAffinity();

        if (TextField != value)
        {
          this.TextField = value;
          Change();
          ChangeInvoke();
        }
      }
    }
    public bool IsReadOnly
    {
      get 
      {
        CheckThreadAffinity(); 
        
        return IsReadOnlyField; 
      }
      set
      {
        CheckThreadAffinity();

        if (IsReadOnlyField != value)
        {
          this.IsReadOnlyField = value;
          Change();
        }
      }
    }
    public event Action ChangeEvent
    {
      add
      {
        CheckThreadAffinity();
        ChangeDelegate += value;
        Change();
      }
      remove
      {
        CheckThreadAffinity();
        ChangeDelegate += value;
        Change();
      }
    }
    public bool HasChange
    {
      get { return ChangeDelegate != null; }
    }
    
    internal override PanelType PanelType
    {
      get { return Inv.PanelType.Memo; }
    }
    internal override string DisplayType
    {
      get { return "memo:" + Text.ConvertToCSharpString(); }
    }

    internal void UpdateText(string Text)
    {
      // update the Text, mark as changed, but don't invoke ChangeDelegate.
      CheckThreadAffinity();

      if (TextField != Text)
      {
        this.TextField = Text;
        Change();
      }
    }
    internal void ChangeText(string Text)
    {
      // update the Text, but don't mark as changed, and invoke ChangeDelegate.
      CheckThreadAffinity();

      if (TextField != Text)
      {
        this.TextField = Text;
        ChangeInvoke();
      }
    }

    private void ChangeInvoke()
    {
      if (ChangeDelegate != null)
        ChangeDelegate();
    }

    private string TextField;
    private bool IsReadOnlyField;
    private Action ChangeDelegate;
  }

  public enum ElementType
  {
    Alignment,
    Background,
    Border,
    Collection,
    Corner,
    Elevation,
    Font,
    Margin,
    Padding,
    Opacity,
    Singleton,
    Size,
    TableRow,
    TableColumn,
    TableCell,
    Visibility
  }

  public abstract class Element
  {
    internal Element(Element Element)
      : this(Element.Change)
    {
      this.Window = Element.Window;
      this.Surface = Element.Surface;
    }
    internal Element(Window Window)
      : this(Window.Change)
    {
      this.Window = Window;
      this.Surface = null;
      this.Panel = null;
    }
    internal Element(Surface Surface)
      : this(Surface.Change)
    {
      this.Window = Surface.Window;
      this.Surface = Surface;
      this.Panel = null;
    }
    internal Element(Panel Panel)
      : this(Panel.Change)
    {
      this.Window = Panel.Surface.Window;
      this.Surface = Panel.Surface;
      this.Panel = Panel;
    }
    private Element(Action ChangeAction)
    {
      this.ChangeAction = ChangeAction;
      //this.IsChanged = true; // NOTE: all platforms must observe the defaults of each element.
    }

    internal abstract ElementType ElementType { get; }
    internal Window Window { get; private set; }
    internal Surface Surface { get; private set; }
    internal Panel Panel { get; private set; }
    internal bool IsChanged { get; private set; }

    internal void Change()
    {
      CheckThreadAffinity();

      if (!IsChanged)
        this.IsChanged = true;

      // TODO: this ensures the parent IsChanged is set (to workaround platform engine bugs where Element.Render() is not called).
      if (ChangeAction != null)
        ChangeAction();
    }
    internal bool Render()
    {
      CheckThreadAffinity();

      if (IsChanged)
      {
        this.IsChanged = false;
        return true;
      }
      else
      {
        return false;
      }
    }

    protected void CheckThreadAffinity()
    {
      Window.CheckThreadAffinity();
    }

    private Action ChangeAction;
  }

  public sealed class Border : Edge
  {
    internal Border(Panel Panel)
      : base(Panel)
    {
    }

    public Inv.Colour Colour
    {
      get 
      {
        CheckThreadAffinity();

        return ColourField; 
      }
      set
      {
        CheckThreadAffinity();

        if (ColourField != value)
        {
          this.ColourField = value;
          Change();
        }
      }
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Border; }
    }

    private Inv.Colour ColourField;
  }

  public enum LineJoin
  {
    // Summary:
    //     Regular angular vertices.
    Miter = 0,
    //
    // Summary:
    //     Beveled vertices.
    Bevel = 1,
    //
    // Summary:
    //     Rounded vertices.
    Round = 2
  }

  public struct Point
  {
    public Point(int X, int Y)
      : this()
    {
      this.X = X;
      this.Y = Y;
    }

    public int X { get; private set; }
    public int Y { get; private set; }

    public static readonly Inv.Point Zero = new Point(0, 0);

    public static Inv.Point operator -(Inv.Point Left, Inv.Point Right)
    {
      return new Inv.Point(Left.X - Right.X, Left.Y - Right.Y);
    }
  }

  public struct Rect
  {
    public Rect(int Left, int Top, int Width, int Height)
      : this()
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Width > 0, "Width must not be zero or negative: {0}", Width);
        Inv.Assert.Check(Height > 0, "Height must not be zero or negative: {0}", Height);
      }

      this.Left = Left;
      this.Top = Top;
      this.Right = Left + Width - 1; // denormalised for use by Android.
      this.Bottom = Top + Height - 1; // denormalised for use by Android.
      this.Width = Width;
      this.Height = Height;
    }

    public int Left { get; private set; }
    public int Top { get; private set; }
    public int Right { get; private set; }
    public int Bottom { get; private set; }
    public int Width { get; private set; }
    public int Height { get; private set; }

    public Inv.Point TopLeft()
    {
      return new Point(Left, Top);
    }
    public Inv.Point TopRight()
    {
      return new Point(Right, Top);
    }
    public Inv.Point BottomLeft()
    {
      return new Point(Left, Bottom);
    }
    public Inv.Point BottomRight()
    {
      return new Point(Right, Bottom);
    }
    public Inv.Point TopCenter()
    {
      return new Point(Left + (Width / 2), Top);
    }
    public Inv.Point BottomCenter()
    {
      return new Point(Left + (Width / 2), Bottom);
    }
    public Inv.Point Center()
    {
      return new Point(Left + (Width / 2), Top + (Height / 2));
    }
    public bool Contains(Inv.Point Point)
    {
      return Point.X >= Left && Point.X <= Right && Point.Y >= Top && Point.Y <= Bottom;
    }
    public Inv.Rect Offset(int X, int Y)
    {
      return new Inv.Rect(Left + X, Top + Y, Width, Height);
    }
    public Inv.Rect Expand(int Size)
    {
      return new Inv.Rect(Left - Size, Top - Size, Width + (Size * 2), Height + (Size * 2));
    }
    public Inv.Rect Reduce(int Size)
    {
      return new Inv.Rect(Left + Size, Top + Size, Width - (Size * 2), Height - (Size * 2));
    }

    public override string ToString()
    {
      return Left + "," + Right + "," + Width + "," + Height;
    }
  }

  public sealed class Size : Element
  {
    internal Size(Panel Panel)
      : base(Panel)
    {
    }

    public int? Width { get; private set; }
    public int? Height { get; private set; }
    public int? MinimumWidth { get; private set; }
    public int? MinimumHeight { get; private set; }
    public int? MaximumWidth { get; private set; }
    public int? MaximumHeight { get; private set; }

    public bool Is(int? Width, int? Height)
    {
      return this.Width == Width && this.Height == Height;
    }
    public void Set(Size Size)
    {
      CheckThreadAffinity();

      if (this.Width != Size.Width || this.Height != Size.Height)
      {
        this.Width = Width;
        this.Height = Height;
        Change();
      }
    }
    public void Set(int? Width, int? Height)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Width == null || Width >= 0, "Width must be null, zero or a positive integer.");
        Inv.Assert.Check(Height == null || Height >= 0, "Height must be null, zero or a positive integer.");
      }

      if (this.Width != Width || this.Height != Height)
      {
        this.Width = Width;
        this.Height = Height;
        Change();
      }
    }
    public void SetWidth(int? Width)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Width == null || Width >= 0, "Width must be null, zero or a positive integer.");

      if (this.Width != Width)
      {
        this.Width = Width;
        Change();
      }
    }
    public void SetHeight(int? Height)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Height == null || Height >= 0, "Height must be null, zero or a positive integer.");

      if (this.Height != Height)
      {
        this.Height = Height;
        Change();
      }
    }
    public void SetMinimum(int? MinimumWidth, int? MinimumHeight)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(MinimumWidth == null || MinimumWidth >= 0, "MinimumWidth must be null, zero or a positive integer.");
        Inv.Assert.Check(MinimumHeight == null || MinimumHeight >= 0, "MinimumHeight must be null, zero or a positive integer.");
      }

      if (this.MinimumWidth != MinimumWidth || this.MinimumHeight != MinimumHeight)
      {
        this.MinimumWidth = MinimumWidth;
        this.MinimumHeight = MinimumHeight;
        Change();
      }
    }
    public void SetMinimumWidth(int? MinimumWidth)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(MinimumWidth == null || MinimumWidth >= 0, "MinimumWidth must be null, zero or a positive integer.");

      if (this.MinimumWidth != MinimumWidth)
      {
        this.MinimumWidth = MinimumWidth;
        Change();
      }
    }
    public void SetMinimumHeight(int? MinimumHeight)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(MinimumHeight == null || MinimumHeight >= 0, "MinimumHeight must be null, zero or a positive integer.");

      if (this.MinimumHeight != MinimumHeight)
      {
        this.MinimumHeight = MinimumHeight;
        Change();
      }
    }
    public void SetMaximum(int? MaximumWidth, int? MaximumHeight)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(MaximumWidth == null || MaximumWidth >= 0, "MaximumWidth must be null, zero or a positive integer.");
        Inv.Assert.Check(MaximumHeight == null || MaximumHeight >= 0, "MaximumHeight must be null, zero or a positive integer.");
      }

      if (this.MaximumWidth != MaximumWidth || this.MaximumHeight != MaximumHeight)
      {
        this.MaximumWidth = MaximumWidth;
        this.MaximumHeight = MaximumHeight;
        Change();
      }
    }
    public void SetMaximumWidth(int? MaximumWidth)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(MaximumWidth == null || MaximumWidth >= 0, "MaximumWidth must be null, zero or a positive integer.");

      if (this.MaximumWidth != MaximumWidth)
      {
        this.MaximumWidth = MaximumWidth;
        Change();
      }
    }
    public void SetMaximumHeight(int? MaximumHeight)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(MaximumHeight == null || MaximumHeight >= 0, "MaximumHeight must be null, zero or a positive integer.");

      if (this.MaximumHeight != MaximumHeight)
      {
        this.MaximumHeight = MaximumHeight;
        Change();
      }
    }

    public void Auto()
    {
      CheckThreadAffinity();

      if (this.Width != null || this.Height != null)
      {
        this.Width = null;
        this.Height = null;
        Change();
      }
    }
    public void AutoWidth()
    {
      CheckThreadAffinity();

      if (Width != null)
      {
        this.Width = null;
        Change();
      }
    }
    public void AutoHeight()
    {
      CheckThreadAffinity();

      if (Height != null)
      {
        this.Height = null;
        Change();
      }
    }
    public void AutoMinimum()
    {
      CheckThreadAffinity();

      if (MinimumWidth != null || MinimumHeight != null)
      {
        this.MinimumWidth = null;
        this.MinimumHeight = null;
        Change();
      }
    }
    public void AutoMinimumWidth()
    {
      CheckThreadAffinity();

      if (MinimumWidth != null)
      {
        this.MinimumWidth = null;
        Change();
      }
    }
    public void AutoMinimumHeight()
    {
      CheckThreadAffinity();

      if (MinimumHeight != null)
      {
        this.MinimumHeight = null;
        Change();
      }
    }
    public void AutoMaximum()
    {
      CheckThreadAffinity();

      if (MaximumWidth != null || MaximumHeight != null)
      {
        this.MaximumWidth = null;
        this.MaximumHeight = null;
        Change();
      }
    }
    public void AutoMaximumWidth()
    {
      CheckThreadAffinity();

      if (MaximumWidth != null)
      {
        this.MaximumWidth = null;
        Change();
      }
    }
    public void AutoMaximumHeight()
    {
      CheckThreadAffinity();

      if (MaximumHeight != null)
      {
        this.MaximumHeight = null;
        Change();
      }
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Size; }
    }
    internal void Set(int? Width, int? Height, int? MinimumWidth, int? MinimumHeight, int? MaximumWidth, int? MaximumHeight)
    {
      CheckThreadAffinity();

      var Changed = false;

      if (Width != this.Width)
      {
        this.Width = Width;
        Changed = true;
      }

      if (Height != this.Height)
      {
        this.Height = Height;
        Changed = true;
      }

      if (MinimumWidth != this.MinimumWidth)
      {
        this.MinimumWidth = MinimumWidth;
        Changed = true;
      }

      if (MinimumHeight != this.MinimumHeight)
      {
        this.MinimumHeight = MinimumHeight;
        Changed = true;
      }

      if (MaximumWidth != this.MaximumWidth)
      {
        this.MaximumWidth = MaximumWidth;
        Changed = true;
      }

      if (MaximumHeight != this.MaximumHeight)
      {
        this.MaximumHeight = MaximumHeight;
        Changed = true;
      }

      if (Changed)
        Change();
    }
  }

  public sealed class Visibility : Element
  {
    internal Visibility(Panel Panel)
      : base(Panel)
    {
      this.IsVisible = true;
    }

    public event Action ChangeEvent;

    public bool Get()
    {
      CheckThreadAffinity();

      return IsVisible;
    }
    public void Set(bool IsVisible)
    {
      CheckThreadAffinity(); 

      if (this.IsVisible != IsVisible)
      {
        this.IsVisible = IsVisible;

        Change();

        ChangeInvoke();
      }
    }
    public void Show()
    {
      Set(true);
    }
    public void Collapse()
    {
      Set(false);
    }
    public void Toggle()
    {
      Set(!Get());
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Visibility; }
    }

    private void ChangeInvoke()
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }

    private bool IsVisible;
  }

  public sealed class Background : Element
  {
    internal Background(Window Window)
      : base(Window)
    {
    }
    internal Background(Surface Surface)
      : base(Surface)
    {
    }
    internal Background(Panel Panel)
      : base(Panel)
    {
    }

    public Inv.Colour Colour
    {
      get
      {
        CheckThreadAffinity(); 
        
        return ColourField; 
      }
      set
      {
        CheckThreadAffinity();

        if (ColourField != value)
        {
          this.ColourField = value;
          Change();
        }
      }
    }

    public Inv.Colour Get()
    {
      return Colour;
    }
    public void Set(Inv.Colour Colour)
    {
      this.Colour = Colour;
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Background; }
    }

    private Inv.Colour ColourField;
  }

  public sealed class Opacity : Element
  {
    internal Opacity(Panel Panel)
      : base(Panel)
    {
      this.Percent = 1.00F;
    }

    public float Get()
    {
      CheckThreadAffinity();

      return this.Percent;
    }
    public void Set(float Percent)
    {
      CheckThreadAffinity();

      if (this.Percent != Percent)
      {
        this.Percent = Percent;
        Change();
      }
    }
    public void Clear()
    {
      Set(0.0F);
    }
    public void Opaque()
    {
      Set(1.0F);
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Opacity; }
    }

    internal void BypassSet(float Percent)
    {
      // set the opacity without notifying the change, useful for animations.
      this.Percent = Percent;
    }

    private float Percent;
  }

  public sealed class Alignment : Element
  {
    internal Alignment(Panel Panel)
      : base(Panel)
    {
      this.Placement = Placement.Stretch;
    }

    public Placement Get()
    {
      CheckThreadAffinity();

      return Placement;
    }
    public void Set(Placement Placement)
    {
      CheckThreadAffinity();

      if (this.Placement != Placement)
      {
        this.Placement = Placement;
        Change();
      }
    }
    public void TopLeft()
    {
      Set(Placement.TopLeft);
    }
    public void TopCenter()
    {
      Set(Placement.TopCenter);
    }
    public void TopRight()
    {
      Set(Placement.TopRight);
    }
    public void TopStretch()
    {
      Set(Inv.Placement.TopStretch);
    }
    public void CenterLeft()
    {
      Set(Placement.CenterLeft);
    }
    public void Center()
    {
      Set(Placement.Center);
    }
    public void CenterRight()
    {
      Set(Placement.CenterRight);
    }
    public void CenterStretch()
    {
      Set(Placement.CenterStretch);
    }
    public void BottomLeft()
    {
      Set(Placement.BottomLeft);
    }
    public void BottomCenter()
    {
      Set(Placement.BottomCenter);
    }
    public void BottomRight()
    {
      Set(Placement.BottomRight);
    }
    public void BottomStretch()
    {
      Set(Placement.BottomStretch);
    }
    public void Stretch()
    {
      Set(Placement.Stretch);
    }
    public void StretchLeft()
    {
      Set(Placement.StretchLeft);
    }
    public void StretchRight()
    {
      Set(Placement.StretchRight);
    }
    public void StretchCenter()
    {
      Set(Placement.StretchCenter);
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Alignment; }
    }

    private Placement Placement;
  }

  public enum Placement
  {
    Stretch,
    StretchLeft,
    StretchCenter,
    StretchRight,
    TopStretch,
    TopLeft,
    TopCenter,
    TopRight,
    CenterStretch,
    CenterLeft,
    Center,
    CenterRight,
    BottomStretch,
    BottomLeft,
    BottomCenter,
    BottomRight
  }

  public sealed class Font : Element
  {
    internal Font(Window Window)
      : base(Window)
    {
      this.WeightField = FontWeight.Regular;
    }
    internal Font(Panel Panel)
      : base(Panel)
    {
      this.WeightField = FontWeight.Regular;
    }

    public string Name
    {
      get
      {
        CheckThreadAffinity(); 
        
        return NameField; 
      }
      set
      {
        CheckThreadAffinity();

        if (NameField != value)
        {
          this.NameField = value;
          Change();
        }
      }
    }
    public int? Size
    {
      get
      {
        CheckThreadAffinity(); 
        
        return SizeField; 
      }
      set
      {
        CheckThreadAffinity();

        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(value != 0, "Font size of zero is not supported.");

        if (SizeField != value)
        {
          this.SizeField = value;
          Change();
        }
      }
    }
    public Inv.Colour Colour
    {
      get
      {
        CheckThreadAffinity(); 
        
        return ColourField; 
      }
      set
      {
        CheckThreadAffinity();

        if (ColourField != value)
        {
          this.ColourField = value;
          Change();
        }
      }
    }
    public Inv.FontWeight Weight
    {
      get
      {
        CheckThreadAffinity();

        return WeightField;
      }
      set
      {
        CheckThreadAffinity();

        if (WeightField != value)
        {
          this.WeightField = value;
          Change();
        }
      }
    }

    public void Thin()
    {
      Weight = FontWeight.Thin;
    }
    public void Light()
    {
      Weight = FontWeight.Light;
    }
    public void Regular()
    {
      Weight = FontWeight.Regular;
    }
    public void Medium()
    {
      Weight = FontWeight.Medium;
    }
    public void Bold()
    {
      Weight = FontWeight.Bold;
    }
    public void Heavy()
    {
      Weight = FontWeight.Heavy;
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Font; }
    }

    private string NameField;
    private int? SizeField;
    private Inv.Colour ColourField;
    private Inv.FontWeight WeightField;
  }

  public enum FontWeight
  {
    Thin,
    Light,
    Regular,
    Medium,
    Bold,
    Heavy
  }

  public sealed class Corner : Element
  {
    internal Corner(Panel Panel)
      : base(Panel)
    {
    }

    public int TopLeft { get; private set; }
    public int TopRight { get; private set; }
    public int BottomRight { get; private set; }
    public int BottomLeft { get; private set; }
    public bool IsSet
    {
      get
      {
        CheckThreadAffinity(); 
        
        return TopLeft != 0 || TopRight != 0 || BottomRight != 0 || BottomLeft != 0; 
      }
    }
    public bool IsUniform
    {
      get
      {
        CheckThreadAffinity();

        return TopLeft == TopRight && TopRight == BottomRight && BottomRight == BottomLeft;
      }
    }

    public void Clear()
    {
      Set(0, 0, 0, 0);
    }
    public void Set(int Value)
    {
      Set(Value, Value, Value, Value);
    }
    public void Set(int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(TopLeft >= 0 && TopRight >= 0 && BottomRight >= 0 && BottomLeft >= 0, "Corners must greater than or equal to zero.");

      if (this.TopLeft != TopLeft || this.TopRight != TopRight || this.BottomRight != BottomRight || this.BottomLeft != BottomLeft)
      {
        this.TopLeft = TopLeft;
        this.TopRight = TopRight;
        this.BottomRight = BottomRight;
        this.BottomLeft = BottomLeft;

        Change();
      }
    }
    public bool Is(int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      CheckThreadAffinity();

      return this.TopLeft == TopLeft && this.TopRight == TopRight && this.BottomRight == BottomRight && this.BottomLeft == BottomLeft;
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Corner; }
    }
  }

  public abstract class Edge : Element // Margin & Padding.
  {
    internal Edge(Element Element)
      : base(Element)
    {
    }
    internal Edge(Panel Panel)
      : base(Panel)
    {
    }

    public int Left { get; private set; }
    public int Top { get; private set; }
    public int Right { get; private set; }
    public int Bottom { get; private set; }
    public bool IsSet
    {
      get
      {
        CheckThreadAffinity();

        return Left != 0 || Top != 0 || Right != 0 || Bottom != 0; 
      }
    }
    public bool IsUniform
    {
      get
      {
        CheckThreadAffinity();

        return Left == Top && Top == Right && Right == Bottom;
      }
    }
    public bool IsHorizontal
    {
      get
      {
        CheckThreadAffinity();

        return Left == Right;
      }
    }
    public bool IsVertical
    {
      get
      {
        CheckThreadAffinity();

        return Top == Bottom;
      }
    }

    public void Clear()
    {
      Set(0, 0, 0, 0);
    }
    public void Set(int Value)
    {
      Set(Value, Value, Value, Value);
    }
    public void Set(int Horizontal, int Vertical)
    {
      Set(Horizontal, Vertical, Horizontal, Vertical);
    }
    public void Set(int Left, int Top, int Right, int Bottom)
    {
      CheckThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Left >= 0 && Top >= 0 && Right >= 0 && Bottom >= 0, "Edges must be greater than or equal to zero.");

      if (this.Left != Left || this.Top != Top || this.Right != Right || this.Bottom != Bottom)
      {
        this.Left = Left;
        this.Top = Top;
        this.Right = Right;
        this.Bottom = Bottom;
        Change();
      }
    }
    public bool Is(int Size)
    {
      CheckThreadAffinity();

      return IsUniform && Size == Left;
    }
    public bool Is(int Horizontal, int Vertical)
    {
      CheckThreadAffinity();

      return this.Left == Horizontal && this.Top == Vertical && this.Right == Horizontal && this.Bottom == Vertical;
    }
    public bool Is(int Left, int Top, int Right, int Bottom)
    {
      CheckThreadAffinity();

      return this.Left == Left && this.Top == Top && this.Right == Right && this.Bottom == Bottom;
    }
  }

  public sealed class Margin : Edge
  {
    internal Margin(Element Element)
      : base(Element)
    {
    }
    internal Margin(Panel Panel)
      : base(Panel)
    {
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Margin; }
    }
  }

  public sealed class Padding : Edge
  {
    internal Padding(Element Element)
      : base(Element)
    {
    }
    internal Padding(Panel Panel)
      : base(Panel)
    {
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Padding; }
    }
  }

  public sealed class Elevation : Element
  {
    internal Elevation(Panel Panel)
      : base(Panel)
    {
    }

    public void Clear()
    {
      Set(0);
    }
    public int Get()
    {
      return Depth;
    }
    public void Set(int Depth)
    {
      CheckThreadAffinity();

      if (this.Depth != Depth)
      {
        this.Depth = Depth;
        Change();
      }
    }

    internal override ElementType ElementType
    {
      get { return Inv.ElementType.Elevation; }
    }

    private int Depth;
  }

  public sealed class Location
  {
    internal Location(Application Application)
    {
      this.Application = Application;
    }

    public bool IsSupported
    {
      get { return Application.Platform.LocationIsSupported; }
    }
    public event Action<Coordinate> ChangeEvent;

    public void Lookup(Inv.Coordinate Coordinate, Action<LocationLookup> ResultAction)
    {
      var Result = new LocationLookup(Coordinate, ResultAction);

      Application.Platform.LocationLookup(Result);
    }

    internal bool IsRequired
    {
      get { return ChangeEvent != null; }
    }

    internal void ChangeInvoke(Coordinate Coordinate)
    {
      if (ChangeEvent != null)
        ChangeEvent(Coordinate);
    }

    private Application Application;
  }

  public sealed class LocationLookup
  {
    internal LocationLookup(Coordinate Coordinate, Action<LocationLookup> ResultAction)
    {
      this.Coordinate = Coordinate;
      this.ResultAction = ResultAction;
    }

    public Coordinate Coordinate { get; private set; }

    public IEnumerable<Placemark> GetPlacemarks()
    {
      return PlacemarkArray;
    }
    public void SetPlacemarks(IEnumerable<Placemark> Placemarks)
    {
      this.PlacemarkArray = Placemarks.ToArray();
      ResultAction(this);
    }

    private Action<LocationLookup> ResultAction;
    private Placemark[] PlacemarkArray;
  }

  public sealed class Placemark
  {
    internal Placemark(string Name, string Locality, string SubLocality, string PostalCode, string AdministrativeArea, string SubAdministrativeArea, string CountryName, string CountryCode, double Latitude, double Longitude)
    {
      this.Name = Name;
      this.Locality = Locality;
      this.SubLocality = SubLocality;
      this.PostalCode = PostalCode;
      this.AdministrativeArea = AdministrativeArea;
      this.SubAdministrativeArea = SubAdministrativeArea;
      this.CountryName = CountryName;
      this.CountryCode = CountryCode;
      this.Latitude = Latitude;
      this.Longitude = Longitude;
    }

    public string Name { get; private set; } // street
    public string Locality { get; private set; } // suburb.
    public string SubLocality { get; private set; } // city.
    public string PostalCode { get; private set; } // postal code
    public string AdministrativeArea { get; private set; } // state
    public string SubAdministrativeArea { get; private set; } // ?
    public string CountryName { get; private set; }
    public string CountryCode { get; private set; }
    public double Latitude { get; private set; }
    public double Longitude { get; private set; }

    public string ToCanonical()
    {
      using (var StringWriter = new System.IO.StringWriter())
      {
        using (var CsvWriter = new Inv.CsvWriter(StringWriter))
          CsvWriter.WriteRecord(Name, Locality, SubLocality, PostalCode, AdministrativeArea, SubAdministrativeArea, CountryName, CountryCode, Latitude.ToString(), Longitude.ToString());

        return StringWriter.ToString();
      }
    }
    public static Placemark FromCanonical(string Text)
    {
      if (Text == null)
        return null;

      using (var StringReader = new System.IO.StringReader(Text))
      {
        using (var CsvReader = new Inv.CsvReader(StringReader))
        {
          var CsvRecord = CsvReader.ReadRecord().ToArray();

          return new Placemark
          (
            Name: CsvRecord.Length > 0 ? CsvRecord[0] : null,
            Locality: CsvRecord.Length > 1 ? CsvRecord[1] : null,
            SubLocality: CsvRecord.Length > 2 ? CsvRecord[2] : null,
            PostalCode: CsvRecord.Length > 3 ? CsvRecord[3] : null,
            AdministrativeArea: CsvRecord.Length > 4 ? CsvRecord[4] : null,
            SubAdministrativeArea: CsvRecord.Length > 5 ? CsvRecord[5] : null,
            CountryName: CsvRecord.Length > 6 ? CsvRecord[6] : null,
            CountryCode : CsvRecord.Length > 7 ? CsvRecord[7] : null,
            Latitude: CsvRecord.Length > 8 ? double.Parse(CsvRecord[8]) : 0.0,
            Longitude: CsvRecord.Length > 9 ? double.Parse(CsvRecord[9]) : 0.0
          );
        }
      }
    }
  }

  public sealed class Coordinate
  {
    internal Coordinate(double Latitude, double Longitude, double Altitude)
    {
      this.Latitude = Latitude;
      this.Longitude = Longitude;
      this.Altitude = Altitude;
    }

    public double Latitude { get; private set; }
    public double Longitude { get; private set; }
    public double Altitude { get; private set; }
  }

  public abstract class BezierPathBuilder<TOutput>
  {
    public abstract void MoveTo(Point Point);
    public abstract void AddLineTo(Point Point);
    public abstract void AddCurveTo(Point EndPoint, Point ControlPoint1, Point ControlPoint2);
    public abstract void Close();
    public abstract TOutput Render();
    
    public void AddRoundedRectMinusBorder(Size Size, CornerRadius CornerRadius, BorderThickness BorderThickness)
    {
      // Inner shape
      AddRoundedRect(Size, CornerRadius, BorderThickness, false, true, false, true);
    }
    public void AddRoundedRectBorder(Size Size, CornerRadius CornerRadius, BorderThickness BorderThickness)
    {
      // Border shape
      AddRoundedRect(Size, CornerRadius, BorderThickness, true, true, false, false);
      AddRoundedRect(Size, CornerRadius, BorderThickness, false, false, true, true);
    }
    public void AddRoundedRect(Size Size, CornerRadius CornerRadius, BorderThickness BorderThickness)
    {
      // Inner shape + Border shape
      AddRoundedRect(Size, CornerRadius, BorderThickness, true, true, false, true);
    }

    private void AddRoundedRect(Size Size, CornerRadius CornerRadius, BorderThickness BorderThickness, bool IsOuterBorder, bool IsClockwise, bool ContinuePath, bool ClosePath)
    {
      var Offset = IsOuterBorder ? Point.Empty : new Point(BorderThickness.Left, BorderThickness.Top);
      var Width = Size.Width - (IsOuterBorder ? 0 : BorderThickness.Left + BorderThickness.Right);
      var Height = Size.Height - (IsOuterBorder ? 0 : BorderThickness.Top + BorderThickness.Bottom);

      var TopLeftStart = new Point(Offset.X, Offset.Y + CornerRadius.TopLeft);
      var TopLeftEnd = new Point(Offset.X + CornerRadius.TopLeft, Offset.Y);

      var TopRightStart = new Point(Offset.X + Width - CornerRadius.TopRight, Offset.Y);
      var TopRightEnd = new Point(Offset.X + Width, Offset.Y + CornerRadius.TopRight);

      var BottomRightStart = new Point(Offset.X + Width, Offset.Y + Height - CornerRadius.BottomRight);
      var BottomRightEnd = new Point(Offset.X + Width - CornerRadius.BottomRight, Offset.Y + Height);

      var BottomLeftStart = new Point(Offset.X + CornerRadius.BottomLeft, Offset.Y + Height);
      var BottomLeftEnd = new Point(Offset.X, Offset.Y + Height - CornerRadius.BottomLeft);
      
      // Adjust points for conflicting radii
      if (TopLeftStart.Y > BottomLeftEnd.Y)
      {
        var LeftY = Offset.Y + Height * CornerRadius.TopLeft / (CornerRadius.TopLeft + CornerRadius.BottomLeft);
        TopLeftStart.Y = LeftY;
        BottomLeftEnd.Y = LeftY;
      }

      if (TopLeftEnd.X > TopRightStart.X)
      {
        var TopX = Offset.X + Width * CornerRadius.TopLeft / (CornerRadius.TopLeft + CornerRadius.TopRight);
        TopLeftEnd.X = TopX;
        TopRightStart.X = TopX;
      }

      if (TopRightEnd.Y > BottomRightStart.Y)
      {
        var RightY = Offset.Y + Height * CornerRadius.TopRight / (CornerRadius.TopRight + CornerRadius.BottomRight);
        TopRightEnd.Y = RightY;
        BottomRightStart.Y = RightY;
      }

      if (BottomRightEnd.X < BottomLeftStart.X)
      {
        var BottomX = Offset.X + Width * CornerRadius.BottomLeft / (CornerRadius.BottomLeft + CornerRadius.BottomRight);
        BottomRightEnd.X = BottomX;
        BottomLeftStart.X = BottomX;
      }

      // Control points
      var K = (float)BezierCircleApproximationFactor;
      var TopLeftPoint1 = new Point(TopLeftStart.X, TopLeftStart.Y - (TopLeftStart.Y - TopLeftEnd.Y) * K);
      var TopLeftPoint2 = new Point(TopLeftEnd.X - (TopLeftEnd.X - TopLeftStart.X) * K, TopLeftEnd.Y);
      var TopRightPoint1 = new Point(TopRightStart.X + (TopRightEnd.X - TopRightStart.X) * K, TopRightStart.Y);
      var TopRightPoint2 = new Point(TopRightEnd.X, TopRightEnd.Y - (TopRightEnd.Y - TopRightStart.Y) * K);
      var BottomRightPoint1 = new Point(BottomRightStart.X, BottomRightStart.Y + (BottomRightEnd.Y - BottomRightStart.Y) * K);
      var BottomRightPoint2 = new Point(BottomRightEnd.X + (BottomRightStart.X - BottomRightEnd.X) * K, BottomRightEnd.Y);
      var BottomLeftPoint1 = new Point(BottomLeftStart.X - (BottomLeftStart.X - BottomLeftEnd.X) * K, BottomLeftStart.Y);
      var BottomLeftPoint2 = new Point(BottomLeftEnd.X, BottomLeftEnd.Y + (BottomLeftStart.Y - BottomLeftEnd.Y) * K);
      
      if (ContinuePath)
        AddLineTo(TopLeftStart);
      else
        MoveTo(TopLeftStart);

      if (IsClockwise)
      {
        if (!TopLeftStart.Equals(TopLeftEnd))
          AddCurveTo(TopLeftEnd, TopLeftPoint1, TopLeftPoint2);
        AddLineTo(TopRightStart);
        if (!TopRightStart.Equals(TopRightEnd))
          AddCurveTo(TopRightEnd, TopRightPoint1, TopRightPoint2);
        AddLineTo(BottomRightStart);
        if (!BottomRightStart.Equals(BottomRightEnd))
          AddCurveTo(BottomRightEnd, BottomRightPoint1, BottomRightPoint2);
        AddLineTo(BottomLeftStart);
        if (!BottomLeftStart.Equals(BottomLeftEnd))
          AddCurveTo(BottomLeftEnd, BottomLeftPoint1, BottomLeftPoint2);
        AddLineTo(TopLeftStart);
      }
      else
      {
        AddLineTo(BottomLeftEnd);
        if (!BottomLeftEnd.Equals(BottomLeftStart))
          AddCurveTo(BottomLeftStart, BottomLeftPoint2, BottomLeftPoint1);
        AddLineTo(BottomRightEnd);
        if (!BottomRightEnd.Equals(BottomRightStart))
          AddCurveTo(BottomRightStart, BottomRightPoint2, BottomRightPoint1);
        AddLineTo(TopRightEnd);
        if (!TopRightEnd.Equals(TopRightStart))
          AddCurveTo(TopRightStart, TopRightPoint2, TopRightPoint1);
        AddLineTo(TopLeftEnd);
        if (!TopLeftEnd.Equals(TopLeftStart))
          AddCurveTo(TopLeftStart, TopLeftPoint2, TopLeftPoint1);
      }

      if (ClosePath)
        Close();
    }

    private const double BezierCircleApproximationFactor = 0.551915024494; // bezier circle approximation constant via http://spencermortensen.com/articles/bezier-circle/

    public struct Point : IEquatable<Point>
    {
      public Point(float X, float Y)
        : this()
      {
        this.X = X;
        this.Y = Y;
      }

      public float X { get; set; }
      public float Y { get; set; }

      public static Point Empty
      {
        get { return new Point(0, 0); }
      }

      public bool Equals(Point Other)
      {
        return X == Other.X && Y == Other.Y;
      }
      public override bool Equals(object Other)
      {
        if (Other is Point)
          return Equals((Point)Other);

        return false;
      }
      public override int GetHashCode()
      {
        return Tuple.Create(X, Y).GetHashCode();
      }
    }

    public struct Size
    {
      public Size(float Width, float Height)
        : this()
      {
        this.Width = Width;
        this.Height = Height;
      }

      public float Width { get; set; }
      public float Height { get; set; }
    }

    public struct CornerRadius
    {
      public CornerRadius(float TopLeft, float TopRight, float BottomRight, float BottomLeft)
        : this()
      {
        this.TopLeft = TopLeft;
        this.TopRight = TopRight;
        this.BottomRight = BottomRight;
        this.BottomLeft = BottomLeft;
      }

      public float TopLeft { get; set; }
      public float TopRight { get; set; }
      public float BottomRight { get; set; }
      public float BottomLeft { get; set; }
    }

    public struct BorderThickness
    {
      public BorderThickness(float Left, float Top, float Right, float Bottom)
        : this()
      {
        this.Left = Left;
        this.Top = Top;
        this.Right = Right;
        this.Bottom = Bottom;
      }

      public float Left { get; set; }
      public float Top { get; set; }
      public float Right { get; set; }
      public float Bottom { get; set; }
    }
  }
}