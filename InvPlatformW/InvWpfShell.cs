﻿/*! 19 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using Inv.Support;
using System.Windows.Media.Imaging;

// for access to WpfEngine.
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("InvPlay, PublicKey=00240000048000009400000006020000002400005253413100040000010001006dbeea36eb5ced1aa13e22b63bffb9abc55372b8801d77a8fd022ce31a8c09a0d395bbcc1cda900768373cfe7b17813192e8a750bc0a681b295ba28d0d66ad5e98993894fd93876fdf2878ca4798ffb9d944f75a5ad0fdfbb4f549a7b6457339d898e21a744b88ac5bdfb878c9b0a9b17f2a910a778cf65b4903aaeef51b6fd2")]

namespace Inv
{
  public static class WpfShell
  {
    static WpfShell()
    {
      FullScreenMode = true;
      DefaultWindowX = null;
      DefaultWindowY = null;
      DefaultWindowWidth = 800;
      DefaultWindowHeight = 600;
      DefaultFontName = "Segoe UI";
      DeviceEmulationArray = WpfDeviceEmulation.Array;
      DeviceEmulationFramed = true;
#if DEBUG
      PreventDeviceEmulation = false;
#else
      PreventDeviceEmulation = true;
#endif

      ThisAssembly = Assembly.GetExecutingAssembly();

      var MainAssembly = Assembly.GetEntryAssembly();

      MainFolderPath = MainAssembly == null ? Path.GetTempPath() : Path.GetDirectoryName(MainAssembly.Location);
    }

    public static bool FullScreenMode { get; set; }
    public static int? DefaultWindowX { get; set; }
    public static int? DefaultWindowY { get; set; }
    public static int DefaultWindowWidth { get; set; }
    public static int DefaultWindowHeight { get; set; }
    public static string DefaultFontName { get; set; }
    public static bool PreventDeviceEmulation { get; set; }
    public static WpfDeviceEmulation DeviceEmulation { get; set; }
    public static WpfDeviceEmulation[] DeviceEmulationArray { get; set; }
    public static bool DeviceEmulationRotated { get; set; }
    public static bool DeviceEmulationFramed { get; set; }
    public static string MainFolderPath { get; set; }
    public static string OverrideAssetFolderPath { get; set; }
    public static int PrimaryScreenWidth
    {
      get { return (int)System.Windows.SystemParameters.PrimaryScreenWidth; }
    }
    public static int PrimaryScreenHeight
    {
      get { return (int)System.Windows.SystemParameters.PrimaryScreenHeight; }
    }

    public static void CheckRequirements(Action Action)
    {
      if (!IsNet45OrNewer())
      {
        System.Windows.MessageBox.Show("Please install the Microsoft .NET 4.5 runtime.", "");
        return;
      }

      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        System.Windows.MessageBox.Show(Exception.AsReport());
      }
    }
    public static void Run(Action<Inv.Application> InvAction)
    {
      var Engine = new WpfEngine();
      
      var InvApplication = new Inv.Application();
      Engine.Install(InvApplication);

      if (InvAction != null)
        InvAction(InvApplication);

      Engine.Run();
    }
    public static void Host(Inv.Application InvApplication, System.Windows.Controls.ContentControl WpfContainer)
    {
      Debug.Assert(InvApplication != null);
      Debug.Assert(WpfContainer != null);

      var Host = new WpfHost(WpfContainer);

      var WpfWindow = WpfContainer as System.Windows.Window;
      if (WpfWindow != null)
        WpfWindow.Closed += (Sender, Event) => InvApplication.Exit();

      Host.ShutdownEvent += () => Host.Stop();
      Host.Install(InvApplication);
      Host.Start();
    }

    internal static Stream GetResourceStream(string Location)
    {
      return ThisAssembly.GetManifestResourceStream(Location);
    }
    internal static System.Windows.ResourceDictionary LoadResourceDictionary(string Name)
    {
      var Assembly = typeof(WpfShell).Assembly;

      var BaseName = "pack://application:,,,/" + Assembly.GetName().Name + ";component/";
      var Dictionary = new System.Windows.ResourceDictionary();

      if (Dictionary.TrySetSource(new Uri(BaseName + "resources/" + Name, UriKind.Absolute)) || Dictionary.TrySetSource(new Uri(BaseName + Name, UriKind.Absolute)))
      {
        return Dictionary;
      }
      else
      {
        Debug.WriteLine("Unable to find resource dictionary: " + Name);
        return null;
      }
    }
    [DebuggerNonUserCode]
    internal static bool TrySetSource(this System.Windows.ResourceDictionary ResourceDictionary, Uri Uri)
    {
      try
      {
        ResourceDictionary.Source = Uri;

        return true;
      }
      catch
      {
        return false;
      }
    }

    private static bool IsNet45OrNewer()
    {
      // Class "ReflectionContext" exists from .NET 4.5 onwards.
      return Type.GetType("System.Reflection.ReflectionContext", false) != null;
    }

    private static Assembly ThisAssembly;
  }

  internal sealed class WpfPlatform : Inv.Platform
  {
    public WpfPlatform(WpfHost Host)
    {
      this.Host = Host;
      this.Vault = new WpfVault();
      this.CurrentProcess = System.Diagnostics.Process.GetCurrentProcess();
    }

    int Platform.ThreadAffinity()
    {
      return Thread.CurrentThread.ManagedThreadId;
    }
    string Platform.CalendarTimeZoneName()
    {
      return TimeZoneInfo.Local.Id;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      Host.ShowCalendarPicker(CalendarPicker);
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      var Mapi = new Inv.Mapi();

      foreach (var To in EmailMessage.GetTos())
        Mapi.AddRecipient(To.Name, To.Address, false);

      foreach (var Attachment in EmailMessage.GetAttachments())
        Mapi.Attach(SelectFilePath(Attachment.File), Attachment.Name);

      var Result = Mapi.Send(EmailMessage.Subject, EmailMessage.Body);

      // NOTE: seems to mess up the full screen mode slightly in the bottom right edge.
      //if (Result)
      //  WpfWindow.WindowState = System.Windows.WindowState.Minimized;

      return Result;
    }
    bool Platform.PhoneIsSupported
    {
      get { return false; }
    }
    void Platform.PhoneDial(string PhoneNumber)
    {
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).LastWriteTimeUtc;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      System.IO.File.SetLastWriteTimeUtc(SelectFilePath(File), Timestamp);
    }
    Stream Platform.DirectoryCreateFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Create, FileAccess.Write, FileShare.Read, 65536);
    }
    Stream Platform.DirectoryAppendFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Append, FileAccess.Write, FileShare.Read, 65536);
    }
    Stream Platform.DirectoryOpenFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Open, FileAccess.Read, FileShare.Read, 65536);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return System.IO.File.Exists(SelectFilePath(File));
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      System.IO.File.Delete(SelectFilePath(File));
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Copy(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Move(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return new System.IO.DirectoryInfo(SelectFolderPath(Folder)).GetFiles(FileMask).Select(F => Folder.NewFile(F.Name));
    }
    Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return new System.IO.FileStream(SelectAssetPath(Asset), FileMode.Open, FileAccess.Read, FileShare.Read, 65536);
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      return System.IO.File.Exists(SelectAssetPath(Asset));
    }
    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      var WpfFileDialog = new WpfFileDialog(FilePicker.Title);

      switch (FilePicker.Type)
      {
        case FileType.Image:
          WpfFileDialog.Filter.AddImageFile();
          break;

        case FileType.Any:
          break;

        default:
          throw new Exception("FileType not handled: " + FilePicker.Type);
      }

      var Result = WpfFileDialog.OpenSingleFile();

      if (Result != null)
        FilePicker.SelectInvoke(new Inv.Binary(System.IO.File.ReadAllBytes(Result), System.IO.Path.GetExtension(Result)));
      else
        FilePicker.CancelInvoke();
    }
    bool Platform.LocationIsSupported
    {
#if DEBUG
      get { return true; }
#else
      get { return false; }
#endif
    }
    void Platform.LocationLookup(LocationLookup LocationLookup)
    {
#if DEBUG
      if (LocationLookup.Coordinate.Latitude == -31.953 && LocationLookup.Coordinate.Longitude == 115.814)
        LocationLookup.SetPlacemarks(new Placemark[] { new Inv.Placemark
        (
          Name: "8 Sadlier St",
          Locality: "Subiaco",
          SubLocality: "Perth",
          PostalCode: "6008",
          AdministrativeArea: "WA",
          SubAdministrativeArea: null,
          CountryName: "Australia",
          CountryCode : "AU",
          Latitude: -31.953,
          Longitude: 115.814
        )});
      else
#endif
        // TODO: implement using Bing web services?
        LocationLookup.SetPlacemarks(new Placemark[] { });
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume, float Rate)
    {
      if (Host.HasSound)
        Host.PlaySound(Sound, Volume, Rate, false);
    }
    void Platform.AudioPlayClip(AudioClip Clip)
    {
      if (Host.HasSound)
        Clip.Node = Host.PlaySound(Clip.Sound, Clip.Volume, Clip.Rate, Clip.Loop);
    }
    void Platform.AudioStopClip(AudioClip Clip)
    {
      var IrrKlangSound = Clip.Node as IrrKlang.ISound;

      if (IrrKlangSound != null)
      {
        Clip.Node = null;
        IrrKlangSound.Stop();
      }
    }
    void Platform.WindowBrowse(File File)
    {
      System.Diagnostics.Process.Start(SelectFilePath(File));
    }
    void Platform.WindowPost(Action Action)
    {
      Host.Post(Action);
    }
    void Platform.WindowCall(Action Action)
    {
      Host.Call(Action);
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      CurrentProcess.Refresh();
      return CurrentProcess.PrivateMemorySize64;
    }
    void Platform.ProcessMemoryReclamation()
    {
      Host.Reclamation();
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      var TcpClient = new Inv.Tcp.Client(WebClient.Host, WebClient.Port, WebClient.CertHash);
      TcpClient.Connect();
      
      WebClient.Node = TcpClient;
      WebClient.SetStreams(TcpClient.Stream, TcpClient.Stream);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
      var TcpClient = (Inv.Tcp.Client)WebClient.Node;
      if (TcpClient != null)
      {
        WebClient.Node = null;
        WebClient.SetStreams(null, null);

        TcpClient.Disconnect();
      }
    }
    void Platform.WebServerConnect(WebServer WebServer)
    {
      var TcpServer = new Inv.Tcp.Server(WebServer.Host, WebServer.Port, WebServer.CertHash);
      TcpServer.AcceptEvent += (TcpChannel) => WebServer.AcceptChannel(TcpChannel, TcpChannel.Stream, TcpChannel.Stream);
      TcpServer.RejectEvent += (TcpChannel) => WebServer.RejectChannel(TcpChannel);

      WebServer.Node = TcpServer;
      WebServer.DropDelegate = (Node) => ((Inv.Tcp.Channel)Node).Drop();

      TcpServer.Connect();
    }
    void Platform.WebServerDisconnect(WebServer WebServer)
    {
      var TcpServer = (Inv.Tcp.Server)WebServer.Node;
      if (TcpServer != null)
      {
        TcpServer.Disconnect();

        WebServer.Node = null;
        WebServer.DropDelegate = null;
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      System.Diagnostics.Process.Start(Uri.AbsoluteUri);
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      Vault.Load(Secret);
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      Vault.Save(Secret);
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      Vault.Delete(Secret);
    }

    private string SelectAssetPath(Asset Asset)
    {
      return System.IO.Path.Combine(Inv.WpfShell.OverrideAssetFolderPath ?? System.IO.Path.Combine(Inv.WpfShell.MainFolderPath, "Assets"), Asset.Name);
    }    
    private string SelectFilePath(File File)
    {
      return System.IO.Path.Combine(SelectFolderPath(File.Folder), File.Name);
    }
    private string SelectFolderPath(Folder Folder)
    {
      string Result;

      if (Folder.Name != null)
        Result = System.IO.Path.Combine(WpfShell.MainFolderPath, Folder.Name);
      else
        Result = WpfShell.MainFolderPath;

      System.IO.Directory.CreateDirectory(Result);

      return Result;
    }

    private WpfHost Host;
    private WpfVault Vault;
    private System.Diagnostics.Process CurrentProcess;
  }

  internal sealed class WpfEngine
  {
    public WpfEngine()
    {
      if (System.Windows.Application.Current == null)
      {
        var WpfApplication = new System.Windows.Application();
        Debug.Assert(System.Windows.Application.Current == WpfApplication);

        // TODO: this is not re-entrant.
        AppDomain.CurrentDomain.UnhandledException += (Sender, Event) =>
        {
          var Exception = Event.ExceptionObject as Exception;

          if (Exception == null && Event.ExceptionObject != null)
            System.Windows.MessageBox.Show(Event.ExceptionObject.GetType().AssemblyQualifiedName + " - " + Event.ExceptionObject.ToString());
          else
            InvApplication.HandleExceptionInvoke(Exception);
        };

        // TODO: this is not re-entrant.
        TaskScheduler.UnobservedTaskException += (Sender, Event) =>
        {
          InvApplication.HandleExceptionInvoke(Event.Exception);

          Event.SetObserved();
        };
      }

      // NOTE: the first window becomes the MainWindow.
      this.WpfWindow = new WpfWindow()
      {
        Owner = null,
        UseLayoutRounding = true,
        SnapsToDevicePixels = true,
        Topmost = false,
        IsTabStop = false,
        //TaskbarItemInfo = new System.Windows.Shell.TaskbarItemInfo(),
        Width = WpfShell.DefaultWindowWidth,
        Height = WpfShell.DefaultWindowHeight,
        FontFamily = new System.Windows.Media.FontFamily(WpfShell.DefaultFontName),
        //SizeToContent = System.Windows.SizeToContent.Manual
      };
      WpfWindow.MouseLeftButtonDown += (Sender, Event) =>
      {
        WpfWindow.DragMove();
      };

      // TODO: custom max/min/restore buttons?
      //var WpfChrome = new WpfChrome(WpfWindow);
      //WpfChrome.Toggle(false);

      if (WpfShell.DefaultWindowX == null && WpfShell.DefaultWindowY == null)
      {
        WpfWindow.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
      }
      else
      {
        WpfWindow.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;

        if (WpfShell.DefaultWindowX != null)
          WpfWindow.Left = WpfShell.DefaultWindowX.Value;

        if (WpfShell.DefaultWindowY != null)
          WpfWindow.Top = WpfShell.DefaultWindowY.Value;
      }

      //System.Windows.Media.TextOptions.SetTextFormattingMode(WpfWindow, System.Windows.Media.TextFormattingMode.Display); // TODO: does this help with anything?
      WpfWindow.Closing += (Sender, Event) =>
      {
        Event.Cancel = !InvApplication.ExitQueryInvoke();
      };
      WpfWindow.Closed += (Sender, Event) =>
      {
        Shutdown();
      };
      WpfWindow.Activated += (Sender, Event) =>
      {
        WpfHost.CheckModifier();
      };
      WpfWindow.PreviewKeyDown += (Sender, Event) =>
      {
        var IsAlt = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftAlt) || System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightAlt);

        if (IsAlt && Event.Key == System.Windows.Input.Key.System && Event.SystemKey == System.Windows.Input.Key.Return)
        {
          WpfShell.FullScreenMode = !WpfShell.FullScreenMode;

          if (WpfShell.FullScreenMode)
            EnterFullScreen();
          else
            ExitFullScreen();

          WpfHost.Rearrange();

          Event.Handled = true;
        }
      };

      if (Debugger.IsAttached && WpfWindow.Icon == null)
      {
        var FileAssembly = Assembly.GetEntryAssembly();

        if (FileAssembly != null)
        {
          using (var FileIcon = System.Drawing.Icon.ExtractAssociatedIcon(FileAssembly.Location))
          {
            var Result = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(FileIcon.Handle, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            Result.Freeze();

            WpfWindow.Icon = Result;
          }
        }
      }

      if (WpfShell.FullScreenMode)
        EnterFullScreen();

      this.WpfHost = new WpfHost(WpfWindow);
      WpfHost.ShutdownEvent += () => Shutdown();
    }

    public void Install(Inv.Application InvApplication)
    {
      this.InvApplication = InvApplication;
      WpfHost.Install(InvApplication);
    }
    public void Unload()
    {
      WpfHost.Stop();
    }
    public void Reload()
    {
      Load();

      WpfHost.Start();
    }
    public void Run()
    {
      using (var SingletonMutex = InvApplication.Title != null ? new SystemMutex(InvApplication.Title) : null)
      {
        var SingletonBound = SingletonMutex == null;

        if (!SingletonBound)
        {
          var SingletonAttempt = 0;
          while (SingletonAttempt < 10)
          {
            SingletonBound = SingletonMutex.Lock(TimeSpan.FromMilliseconds(500));

            if (SingletonBound)
            {
              break; // We control the mutex, can continue as the singleton process.
            }
            else
            {
              SingletonAttempt++;
            }
          }
        }

        if (SingletonBound)
        {
          WpfHost.Start();
          try
          {
            Load();

            if (!WpfWindow.IsVisible)
              WpfWindow.Show();

            this.DispatcherFrame = new System.Windows.Threading.DispatcherFrame();
            DispatcherFrame.Dispatcher.UnhandledException += UnhandledException;
            try
            {
              System.Windows.Threading.Dispatcher.PushFrame(DispatcherFrame);
            }
            finally
            {
              if (DispatcherFrame != null)
              {
                DispatcherFrame.Dispatcher.UnhandledException -= UnhandledException;
                this.DispatcherFrame = null;
              }
            }
          }
          finally
          {
            WpfHost.Stop();
          }
        }
      }
    }
    public void Asynchronise(Action Action)
    {
      WpfHost.Post(Action);
    }

    internal Inv.Application InvApplication { get; private set; }
    
    internal void EnterFullScreen()
    {
      WpfWindow.WindowStyle = System.Windows.WindowStyle.None;
      WpfWindow.ResizeMode = System.Windows.ResizeMode.NoResize;

      this.WpfWindowChrome = System.Windows.Shell.WindowChrome.GetWindowChrome(WpfWindow);
      System.Windows.Shell.WindowChrome.SetWindowChrome(WpfWindow, null);

      WpfWindow.WindowState = System.Windows.WindowState.Maximized;
    }
    internal void ExitFullScreen()
    {
      WpfWindow.WindowStyle = System.Windows.WindowStyle.SingleBorderWindow;
      WpfWindow.ResizeMode = System.Windows.ResizeMode.CanResize;

      System.Windows.Shell.WindowChrome.SetWindowChrome(WpfWindow, WpfWindowChrome);

      WpfWindow.WindowState = System.Windows.WindowState.Normal;
    }

    private void Load()
    {
      var InvWindow = InvApplication.Window;

      WpfWindow.Title = InvApplication.Title ?? "";
    }
    private void Shutdown()
    {
      if (DispatcherFrame != null)
      {
        DispatcherFrame.Continue = false;
        DispatcherFrame.Dispatcher.UnhandledException -= UnhandledException;
        this.DispatcherFrame = null;
      }
    }
    private void UnhandledException(object Sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs Event)
    {
      if (!Event.Handled)
      {
        InvApplication.HandleExceptionInvoke(Event.Exception);

        Event.Handled = true;
      }
    }

    private WpfWindow WpfWindow;
    private WpfHost WpfHost;
    private System.Windows.Shell.WindowChrome WpfWindowChrome;
    private System.Windows.Threading.DispatcherFrame DispatcherFrame;
  }

  internal sealed class WpfHost
  {
    public WpfHost(System.Windows.Controls.ContentControl WpfContainer)
    {
      this.WpfContainer = WpfContainer;

      this.WpfBorder = new System.Windows.Controls.Border();
      WpfContainer.Content = WpfBorder;
      WpfBorder.Background = System.Windows.Media.Brushes.DimGray;

      // NOTE: this may have a positive affect on performance (unproven) but it definitely makes rounded edges ugly.
      //System.Windows.Media.RenderOptions.SetEdgeMode(WpfBorder, System.Windows.Media.EdgeMode.Aliased);

      this.WpfMaster = new WpfGrid();
      WpfMaster.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
      WpfMaster.VerticalAlignment = System.Windows.VerticalAlignment.Center;
      WpfMaster.ClipToBounds = true;
      System.Windows.NameScope.SetNameScope(WpfMaster, new System.Windows.NameScope());
      WpfMaster.MouseLeftButtonDown += (Sender, Event) =>
      {
        Event.Handled = true;
      };

      if (WpfShell.PreventDeviceEmulation)
      {
        WpfBorder.Child = WpfMaster;
      }
      else
      {
        var WpfEmulationGrid = new WpfGrid();
        WpfBorder.Child = WpfEmulationGrid;
        WpfEmulationGrid.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        WpfEmulationGrid.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;

        this.WpfEmulationDeviceGraphic = new WpfGraphic();
        WpfEmulationGrid.Children.Add(WpfEmulationDeviceGraphic);
        //WpfEmulationDeviceGraphic.Margin = new System.Windows.Thickness(3, 9, 0, 0);
        WpfEmulationDeviceGraphic.Background = null;
        WpfEmulationDeviceGraphic.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        WpfEmulationDeviceGraphic.VerticalAlignment = System.Windows.VerticalAlignment.Center;

        this.WpfEmulationApplicationLabel = new WpfLabel();
        WpfEmulationGrid.Children.Add(WpfEmulationApplicationLabel);
        WpfEmulationApplicationLabel.Foreground = System.Windows.Media.Brushes.White;
        WpfEmulationApplicationLabel.Padding = new System.Windows.Thickness(10);
        WpfEmulationApplicationLabel.FontSize = 20;
        WpfEmulationApplicationLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
        WpfEmulationApplicationLabel.VerticalAlignment = System.Windows.VerticalAlignment.Top;

        this.WpfEmulationFrameLabel = new WpfLabel();
        WpfEmulationGrid.Children.Add(WpfEmulationFrameLabel);
        WpfEmulationFrameLabel.Foreground = System.Windows.Media.Brushes.White;
        WpfEmulationFrameLabel.Padding = new System.Windows.Thickness(10);
        WpfEmulationFrameLabel.FontSize = 20;
        WpfEmulationFrameLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
        WpfEmulationFrameLabel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;

        this.WpfEmulationInstructionLabel = new WpfLabel();
        WpfEmulationGrid.Children.Add(WpfEmulationInstructionLabel);
        WpfEmulationInstructionLabel.Foreground = System.Windows.Media.Brushes.White;
        WpfEmulationInstructionLabel.Padding = new System.Windows.Thickness(10);
        WpfEmulationInstructionLabel.FontSize = 20;
        WpfEmulationInstructionLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
        WpfEmulationInstructionLabel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;

        WpfEmulationGrid.Children.Add(WpfMaster);
      }

      var SizeChangedOnceOnly = false;

      var LeftEdgeSwipe = false;
      var RightEdgeSwipe = false;

      WpfMaster.PreviewMouseDown += (Sender, Event) =>
      {
        var MousePoint = Event.GetPosition(WpfMaster);

        LeftEdgeSwipe = MousePoint.X <= 10;
        RightEdgeSwipe = MousePoint.X >= WpfMaster.ActualWidth - 10;
      };
      WpfMaster.PreviewMouseUp += (Sender, Event) =>
      {
        LeftEdgeSwipe = false;
        RightEdgeSwipe = false;
      };
      WpfMaster.PreviewMouseMove += (Sender, Event) =>
      {
        if (LeftEdgeSwipe || RightEdgeSwipe)
        {
          var InvSurface = InvApplication.Window.ActiveSurface;

          if (InvSurface != null)
          {
            var MousePoint = Event.GetPosition(WpfMaster);

            if (LeftEdgeSwipe && MousePoint.X >= 0)
            {
              if (MousePoint.X >= 20)
              {
                InvSurface.GestureBackwardInvoke();
                LeftEdgeSwipe = false;
              }
            }
            else if (RightEdgeSwipe && MousePoint.X <= WpfMaster.ActualWidth)
            {
              if (MousePoint.X <= WpfMaster.ActualWidth - 20)
              {
                InvSurface.GestureForwardInvoke();
                RightEdgeSwipe = false;
              }
            }
            else
            {
              LeftEdgeSwipe = false;
              RightEdgeSwipe = false;
            }
          }
          else
          {
            LeftEdgeSwipe = false;
            RightEdgeSwipe = false;
          }

          Event.Handled = true;
        }
      };
      WpfContainer.SizeChanged += (Sender, Event) =>
      {
        if (!SizeChangedOnceOnly)
        {
          SizeChangedOnceOnly = true;

          WpfContainer.Dispatcher.BeginInvoke((Action)delegate
          {
            SizeChangedOnceOnly = false;

            Rearrange();
          }, System.Windows.Threading.DispatcherPriority.Render);
        }
      };
      WpfContainer.PreviewKeyDown += (Sender, Event) =>
      {
        var InvSurface = InvApplication.Window.ActiveSurface;
        var InvModifier = GetModifier();

        InvApplication.Window.CheckModifier(InvModifier);

        if (!WpfShell.PreventDeviceEmulation)
        {
          if (InvModifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Left || Event.Key == System.Windows.Input.Key.Right))
          {
            WpfShell.DeviceEmulationRotated = !WpfShell.DeviceEmulationRotated;
            Resize();
            /*
            var RotateLogical = InvApplication.Window.Height;
            InvApplication.Window.Height = InvApplication.Window.Width;
            InvApplication.Window.Width = RotateLogical;

            var RotatePhysical = WpfMaster.Height;
            WpfMaster.Height = WpfMaster.Width;
            WpfMaster.Width = RotatePhysical;
            */
            if (InvSurface != null)
              InvSurface.ArrangeInvoke();

            return;
          }
          else if (InvModifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Add || Event.Key == System.Windows.Input.Key.OemPlus))
          {
            WpfShell.DeviceEmulationFramed = true;
            Resize();
          }
          else if (InvModifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Subtract || Event.Key == System.Windows.Input.Key.OemMinus))
          {
            WpfShell.DeviceEmulationFramed = false;
            Resize();
          }
          else if (InvModifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Up || Event.Key == System.Windows.Input.Key.Down))
          {
            var EmulationIndex = WpfShell.DeviceEmulation != null ? WpfShell.DeviceEmulationArray.FindIndex(E => E == WpfShell.DeviceEmulation) : -1;

            if (Event.Key == System.Windows.Input.Key.Down)
              EmulationIndex++;
            else
              EmulationIndex--;

            if (EmulationIndex >= WpfShell.DeviceEmulationArray.Length)
              EmulationIndex = 0;
            else if (EmulationIndex < 0)
              EmulationIndex = WpfShell.DeviceEmulationArray.Length - 1;

            WpfShell.DeviceEmulation = WpfShell.DeviceEmulationArray[EmulationIndex];
            Resize();

            if (InvSurface != null)
              InvSurface.ArrangeInvoke();

            return;
          }
          else if (InvModifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Clear || Event.Key == System.Windows.Input.Key.NumPad5))
          {
            WpfShell.DeviceEmulation = null;
            Resize();

            if (InvSurface != null)
              InvSurface.ArrangeInvoke();

            return;
          }
          else if (InvModifier.IsCtrl && (Event.Key == System.Windows.Input.Key.F8))
          {
            if (InvSurface != null)
            {
              var SnapshotFilePath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), string.Format("{0}_inv-snapshot.txt", InvApplication.Title));

              System.IO.File.WriteAllText(SnapshotFilePath, InvSurface.GetPanelDisplay());

              System.Diagnostics.Process.Start(SnapshotFilePath);
            }

            return;
          }
        }

        if (InvSurface != null)
        {
          if (IsTransitioning)
          {
            // ignore keystrokes while transitioning.
            Event.Handled = true;
          }
          else
          {
            var WpfKey = Event.Key == System.Windows.Input.Key.System ? Event.SystemKey : Event.Key;

            var InvKey = TranslateKey(WpfKey);
            if (InvKey != null)
            {
              var Keystroke = new Keystroke()
              {
                Key = InvKey.Value,
                Modifier = InvModifier
              };

              InvSurface.KeystrokeInvoke(Keystroke);
            }
          }
        }
      };
      WpfContainer.PreviewKeyUp += (Sender, Event) =>
      {
        CheckModifier();
      };
      WpfMaster.PreviewMouseDown += (Sender, Event) =>
      {
        if (Event.ButtonState == System.Windows.Input.MouseButtonState.Pressed)
        {
          var InvSurface = InvApplication.Window.ActiveSurface;

          if (Event.ChangedButton == System.Windows.Input.MouseButton.XButton1 && InvSurface != null)
          {
            InvSurface.GestureBackwardInvoke();

            Event.Handled = true;
          }
          else if (Event.ChangedButton == System.Windows.Input.MouseButton.XButton2 && InvSurface != null)
          {
            InvSurface.GestureForwardInvoke();

            Event.Handled = true;
          }
        }
      };

      Microsoft.Win32.SystemEvents.DisplaySettingsChanged += (Sender, Event) =>
      {
        InvApplication.Platform.WindowPost(() =>
        {
          Resize();

          var InvSurface = InvApplication.Window.ActiveSurface;
          if (InvSurface != null)
            InvSurface.ArrangeInvoke();
        });
      };

      try
      {
        this.SoundPlayer = new WpfSoundPlayer();
      }
      catch (Exception Exception)
      {
        this.SoundException = Exception;
        this.SoundPlayer = null;
      }

      this.PenDictionary = new Dictionary<PenKey, System.Windows.Media.Pen>();
      this.LookupPenKey = new PenKey();
      this.MirrorTransformArray = new Inv.EnumArray<Mirror, System.Windows.Media.ScaleTransform>()
      {
        { Mirror.Horizontal, new System.Windows.Media.ScaleTransform(-1, +1) },
        { Mirror.Vertical, new System.Windows.Media.ScaleTransform(+1, -1) },
      };

      #region Key mapping.
      this.KeyDictionary = new Dictionary<System.Windows.Input.Key, Inv.Key>()
      {
        { System.Windows.Input.Key.D0, Inv.Key.n0 },
        { System.Windows.Input.Key.D1, Inv.Key.n1 },
        { System.Windows.Input.Key.D2, Inv.Key.n2 },
        { System.Windows.Input.Key.D3, Inv.Key.n3 },
        { System.Windows.Input.Key.D4, Inv.Key.n4 },
        { System.Windows.Input.Key.D5, Inv.Key.n5 },
        { System.Windows.Input.Key.D6, Inv.Key.n6 },
        { System.Windows.Input.Key.D7, Inv.Key.n7 },
        { System.Windows.Input.Key.D8, Inv.Key.n8 },
        { System.Windows.Input.Key.D9, Inv.Key.n9 },
        { System.Windows.Input.Key.A, Inv.Key.A },
        { System.Windows.Input.Key.B, Inv.Key.B },
        { System.Windows.Input.Key.C, Inv.Key.C },
        { System.Windows.Input.Key.D, Inv.Key.D },
        { System.Windows.Input.Key.E, Inv.Key.E },
        { System.Windows.Input.Key.F, Inv.Key.F },
        { System.Windows.Input.Key.G, Inv.Key.G },
        { System.Windows.Input.Key.H, Inv.Key.H },
        { System.Windows.Input.Key.I, Inv.Key.I },
        { System.Windows.Input.Key.J, Inv.Key.J },
        { System.Windows.Input.Key.K, Inv.Key.K },
        { System.Windows.Input.Key.L, Inv.Key.L },
        { System.Windows.Input.Key.M, Inv.Key.M },
        { System.Windows.Input.Key.N, Inv.Key.N },
        { System.Windows.Input.Key.O, Inv.Key.O },
        { System.Windows.Input.Key.P, Inv.Key.P },
        { System.Windows.Input.Key.Q, Inv.Key.Q },
        { System.Windows.Input.Key.R, Inv.Key.R },
        { System.Windows.Input.Key.S, Inv.Key.S },
        { System.Windows.Input.Key.T, Inv.Key.T },
        { System.Windows.Input.Key.U, Inv.Key.U },
        { System.Windows.Input.Key.V, Inv.Key.V },
        { System.Windows.Input.Key.W, Inv.Key.W },
        { System.Windows.Input.Key.X, Inv.Key.X },
        { System.Windows.Input.Key.Y, Inv.Key.Y },
        { System.Windows.Input.Key.Z, Inv.Key.Z },
        { System.Windows.Input.Key.F1, Inv.Key.F1 },
        { System.Windows.Input.Key.F2, Inv.Key.F2 },
        { System.Windows.Input.Key.F3, Inv.Key.F3 },
        { System.Windows.Input.Key.F4, Inv.Key.F4 },
        { System.Windows.Input.Key.F5, Inv.Key.F5 },
        { System.Windows.Input.Key.F6, Inv.Key.F6 },
        { System.Windows.Input.Key.F7, Inv.Key.F7 },
        { System.Windows.Input.Key.F8, Inv.Key.F8 },
        { System.Windows.Input.Key.F9, Inv.Key.F9 },
        { System.Windows.Input.Key.F10, Inv.Key.F10 },
        { System.Windows.Input.Key.F11, Inv.Key.F11 },
        { System.Windows.Input.Key.F12, Inv.Key.F12 },
        { System.Windows.Input.Key.Escape, Inv.Key.Escape },
        { System.Windows.Input.Key.Enter, Inv.Key.Enter },
        { System.Windows.Input.Key.Tab, Inv.Key.Tab },
        { System.Windows.Input.Key.Space, Inv.Key.Space },
        { System.Windows.Input.Key.OemPeriod, Inv.Key.Period },
        { System.Windows.Input.Key.OemComma, Inv.Key.Comma },
        { System.Windows.Input.Key.OemTilde, Inv.Key.Tilde },
        //{ System.Windows.Input.Key.OemTilde, Inv.Key.BackQuote }, // TODO: plus shift.
        { System.Windows.Input.Key.OemPlus, Inv.Key.Plus },
        { System.Windows.Input.Key.OemMinus, Inv.Key.Minus },
        { System.Windows.Input.Key.Up, Inv.Key.Up },
        { System.Windows.Input.Key.Down, Inv.Key.Down },
        { System.Windows.Input.Key.Left, Inv.Key.Left },
        { System.Windows.Input.Key.Right, Inv.Key.Right },
        { System.Windows.Input.Key.Home, Inv.Key.Home },
        { System.Windows.Input.Key.End, Inv.Key.End },
        { System.Windows.Input.Key.PageUp, Inv.Key.PageUp },
        { System.Windows.Input.Key.PageDown, Inv.Key.PageDown },
        { System.Windows.Input.Key.Clear, Inv.Key.Clear },
        { System.Windows.Input.Key.Insert, Inv.Key.Insert },
        { System.Windows.Input.Key.Delete, Inv.Key.Delete },
        { System.Windows.Input.Key.Oem2, Inv.Key.Slash },
        { System.Windows.Input.Key.Oem5, Inv.Key.Backslash },
        { System.Windows.Input.Key.Add, Inv.Key.Plus },
        { System.Windows.Input.Key.Subtract, Inv.Key.Minus },
        { System.Windows.Input.Key.Divide, Inv.Key.Slash },
        { System.Windows.Input.Key.Multiply, Inv.Key.Asterisk },
        { System.Windows.Input.Key.OemBackslash, Inv.Key.Backslash },
        { System.Windows.Input.Key.Back, Inv.Key.Backspace },
        { System.Windows.Input.Key.LeftShift, Inv.Key.LeftShift },
        { System.Windows.Input.Key.RightShift, Inv.Key.RightShift },
        { System.Windows.Input.Key.LeftAlt, Inv.Key.LeftAlt },
        { System.Windows.Input.Key.RightAlt, Inv.Key.RightAlt },
        { System.Windows.Input.Key.LeftCtrl, Inv.Key.LeftCtrl },
        { System.Windows.Input.Key.RightCtrl, Inv.Key.RightCtrl },
      };
      #endregion

      this.RouteArray = new Inv.EnumArray<PanelType, Func<Panel, System.Windows.FrameworkElement>>()
      {
        { Inv.PanelType.Browser, TranslateBrowser },
        { Inv.PanelType.Button, TranslateButton },
        { Inv.PanelType.Board, TranslateBoard },
        { Inv.PanelType.Dock, TranslateDock },
        { Inv.PanelType.Edit, TranslateEdit },
        { Inv.PanelType.Flow, TranslateFlow },
        { Inv.PanelType.Frame, TranslateFrame },
        { Inv.PanelType.Graphic, TranslateGraphic },
        { Inv.PanelType.Label, TranslateLabel },
        { Inv.PanelType.Memo, TranslateMemo },
        { Inv.PanelType.Overlay, TranslateOverlay },
        { Inv.PanelType.Canvas, TranslateCanvas },
        { Inv.PanelType.Scroll, TranslateScroll },
        { Inv.PanelType.Stack, TranslateStack },
        { Inv.PanelType.Table, TranslateTable },
      };
    }

    internal event Action ShutdownEvent;
    internal bool HasSound
    {
      get { return SoundException == null; }
    }
    internal bool IsTransitioning { get; private set; }
    internal Inv.Application InvApplication { get; private set; }
    internal Inv.EnumArray<Mirror, System.Windows.Media.ScaleTransform> MirrorTransformArray { get; private set; }

    internal KeyModifier GetModifier()
    {
      return new KeyModifier()
      {
        IsLeftShift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift),
        IsRightShift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift),
        IsLeftAlt = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftAlt),
        IsRightAlt = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightAlt),
        IsLeftCtrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl),
        IsRightCtrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl)
      };
    }
    internal void Install(Inv.Application InvApplication)
    {
      this.InvApplication = InvApplication;
      InvApplication.SetPlatform(new WpfPlatform(this));

      InvApplication.Directory.Installation = WpfShell.MainFolderPath;

      InvApplication.Device.Name = Environment.MachineName;
      InvApplication.Device.Model = "";

      InvApplication.Process.Id = System.Diagnostics.Process.GetCurrentProcess().Id;

      using (var mc = new System.Management.ManagementClass("Win32_ComputerSystem"))
      {
        var moc = mc.GetInstances();
        if (moc.Count != 0)
        {
          foreach (var mo in mc.GetInstances())
            InvApplication.Device.Model += mo["Manufacturer"].ToString() + " " + mo["Model"].ToString();
        }
      }

      InvApplication.Device.System = WpfSystem.Name + " " + WpfSystem.Edition + " " + WpfSystem.Version;

      InvApplication.Device.Keyboard = true; // TODO: System.Windows.Input.Keyboard;
      InvApplication.Device.Mouse = true; // TODO: System.Windows.Input.Mouse;
      InvApplication.Device.Touch = WpfSystem.HasTouchInput();
      InvApplication.Device.ProportionalFontName = WpfShell.DefaultFontName;
      InvApplication.Device.MonospacedFontName = "Consolas";

      Resize();

      if (SoundException != null)
      {
        System.Windows.MessageBox.Show(
          "There was a problem initialising the sound engine which may be caused by missing assemblies or issues with your sound card driver. Please check the installation and sound card driver." + Environment.NewLine + Environment.NewLine +
          "'" + SoundException.Message + "'" + Environment.NewLine + Environment.NewLine +
          "This application will run with all sound effects disabled.");
      }
    }
    internal void Start()
    {
      InvApplication.StartInvoke();

      if (InvApplication.Location.IsRequired)
      {
#if DEBUG
        InvApplication.Location.ChangeInvoke(new Inv.Coordinate(-31.953, 115.814, 0.0));
#endif
      }

      Process();

      System.Windows.Media.CompositionTarget.Rendering += Rendering;
    }
    internal void Stop()
    {
      System.Windows.Media.CompositionTarget.Rendering -= Rendering;

      InvApplication.StopInvoke();
    }
    internal void Rearrange()
    {
      var LastLogicalScreenWidthPoints = InvApplication.Window.Width;
      var LastLogicalScreenHeightPoints = InvApplication.Window.Height;

      Resize();

      if (LastLogicalScreenWidthPoints != InvApplication.Window.Width || LastLogicalScreenHeightPoints != InvApplication.Window.Height)
      {
        var InvSurface = InvApplication.Window.ActiveSurface;
        if (InvSurface != null)
          InvSurface.ArrangeInvoke();
      }
    }
    internal void Reclamation()
    {
      // NOTE: nothing disposable here.
      PenDictionary.Clear();

      if (HasSound)
        SoundPlayer.Reclamation();
    }
    internal void ShowCalendarPicker(CalendarPicker CalendarPicker)
    {
      var WpfOverlay = new WpfGrid();
      WpfMaster.Children.Add(WpfOverlay);

      var WpfShadeBrush = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Black) { Opacity = 0.50 };
      WpfShadeBrush.Freeze();

      var WpfShade = new WpfButton();
      WpfOverlay.Children.Add(WpfShade);
      WpfShade.Background = WpfShadeBrush;
      WpfShade.HoverBackgroundBrush = WpfShadeBrush;
      WpfShade.PressedBackgroundBrush = WpfShadeBrush;
      WpfShade.LeftClick += (Sender, Event) =>
      {
        WpfMaster.Children.Remove(WpfOverlay);
        CalendarPicker.CancelInvoke();
        Event.Handled = true;
      };
      WpfShade.RightClick += (Sender, Event) =>
      {
        WpfMaster.Children.Remove(WpfOverlay);
        CalendarPicker.CancelInvoke();
        Event.Handled = true;
      };

      // TODO: escape key to cancel.

      var WpfFrame = new WpfFrame();
      WpfOverlay.Children.Add(WpfFrame);
      WpfFrame.Background = System.Windows.Media.Brushes.White;
      WpfFrame.Padding = new System.Windows.Thickness(10);
      WpfFrame.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
      WpfFrame.VerticalAlignment = System.Windows.VerticalAlignment.Center;

      var WpfStack = new System.Windows.Controls.StackPanel();
      WpfFrame.Child = WpfStack;
      WpfStack.Orientation = System.Windows.Controls.Orientation.Vertical;

      var TitleLabel = new WpfLabel();
      WpfStack.Children.Add(TitleLabel);
      TitleLabel.Margin = new System.Windows.Thickness(10);
      TitleLabel.Foreground = System.Windows.Media.Brushes.Black;
      TitleLabel.FontSize = 30;
      TitleLabel.TextAlignment = System.Windows.TextAlignment.Center;
      TitleLabel.Text = "Select" + (CalendarPicker.SetDate ? " Date" : "") + (CalendarPicker.SetTime ? " Time" : "");

      var AcceptButton = new WpfButton();

      var SelectedValue = CalendarPicker.Value.Date + CalendarPicker.Value.TimeOfDay.TruncateSeconds();

      if (CalendarPicker.SetDate)
      {
        var WpfCalendar = new WpfCalendar();
        WpfStack.Children.Add(WpfCalendar);
        WpfCalendar.Padding = new System.Windows.Thickness(0);
        WpfCalendar.IsHitTestVisible = true;
        WpfCalendar.SelectionMode = System.Windows.Controls.CalendarSelectionMode.SingleDate;
        WpfCalendar.SelectedDate = SelectedValue;
        WpfCalendar.DisplayDate = SelectedValue;
        WpfCalendar.IsTodayHighlighted = true;
        WpfCalendar.SelectedDatesChanged += (Sender, Event) =>
        {
          if (WpfCalendar.SelectedDate != null)
            SelectedValue = WpfCalendar.SelectedDate.Value.Date + SelectedValue.TimeOfDay.TruncateSeconds();
        };
      }

      if (CalendarPicker.SetTime)
      {
        var WpfTimeEdit = new WpfEdit(false);
        WpfStack.Children.Add(WpfTimeEdit);
        WpfTimeEdit.BorderBrush = System.Windows.Media.Brushes.LightGray;
        WpfTimeEdit.BorderThickness = new System.Windows.Thickness(1);
        WpfTimeEdit.Background = System.Windows.Media.Brushes.WhiteSmoke;
        WpfTimeEdit.Padding = new System.Windows.Thickness(10);
        WpfTimeEdit.GetTextBox().FontSize = 20;
        WpfTimeEdit.GetTextBox().TextAlignment = System.Windows.TextAlignment.Center;
        WpfTimeEdit.IsReadOnly = false;
        WpfTimeEdit.Text = CalendarPicker.Value.ToString("HH:mm");
        WpfTimeEdit.TextChanged += (Sender, Event) =>
        {
          DateTime Time;
          if (DateTime.TryParseExact(WpfTimeEdit.Text, "H:mm", null, DateTimeStyles.AssumeLocal, out Time))
          {
            SelectedValue = SelectedValue.Date + Time.TimeOfDay.TruncateSeconds();

            WpfTimeEdit.Foreground = System.Windows.Media.Brushes.Black;
            AcceptButton.IsEnabled = true;
          }
          else
          {
            WpfTimeEdit.Foreground = System.Windows.Media.Brushes.Red;
            AcceptButton.IsEnabled = false;
          }
        };
      }

      var ButtonGrid = new WpfGrid();
      WpfStack.Children.Add(ButtonGrid);
      ButtonGrid.Margin = new System.Windows.Thickness(0, 10, 0, 0);
      ButtonGrid.ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = new System.Windows.GridLength(1.0, System.Windows.GridUnitType.Star) });
      ButtonGrid.ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = new System.Windows.GridLength(1.0, System.Windows.GridUnitType.Star) });
      ButtonGrid.RowDefinitions.Add(new System.Windows.Controls.RowDefinition() { Height = System.Windows.GridLength.Auto });

      AcceptButton.Margin = new System.Windows.Thickness(0, 0, 5, 0);
      AcceptButton.Background = System.Windows.Media.Brushes.DarkGreen;
      AcceptButton.HoverBackgroundBrush = System.Windows.Media.Brushes.Green;
      AcceptButton.PressedBackgroundBrush = System.Windows.Media.Brushes.DarkGreen;
      AcceptButton.Padding = new System.Windows.Thickness(10);
      AcceptButton.LeftClick += (Sender, Event) =>
      {
        WpfMaster.Children.Remove(WpfOverlay);

        CalendarPicker.Value = SelectedValue;
        CalendarPicker.SelectInvoke();

        Event.Handled = true;
      };

      var AcceptLabel = new WpfLabel();
      AcceptButton.Content = AcceptLabel;
      AcceptLabel.Foreground = System.Windows.Media.Brushes.White;
      AcceptLabel.FontSize = 20;
      AcceptLabel.TextAlignment = System.Windows.TextAlignment.Center;
      AcceptLabel.Text = "ACCEPT";

      var CancelButton = new WpfButton();
      CancelButton.Margin = new System.Windows.Thickness(5, 0, 0, 0);
      CancelButton.Background = System.Windows.Media.Brushes.LightGray;
      CancelButton.HoverBackgroundBrush = System.Windows.Media.Brushes.WhiteSmoke;
      CancelButton.PressedBackgroundBrush = System.Windows.Media.Brushes.LightGray;
      CancelButton.Padding = new System.Windows.Thickness(10);
      CancelButton.LeftClick += (Sender, Event) =>
      {
        WpfMaster.Children.Remove(WpfOverlay);
        CalendarPicker.CancelInvoke();
        Event.Handled = true;
      };

      var CancelLabel = new WpfLabel();
      CancelButton.Content = CancelLabel;
      CancelLabel.Foreground = System.Windows.Media.Brushes.Black;
      CancelLabel.FontSize = 20;
      CancelLabel.TextAlignment = System.Windows.TextAlignment.Center;
      CancelLabel.Text = "CANCEL";

      ButtonGrid.ComposeDock(true, new System.Windows.FrameworkElement[] { }, new System.Windows.FrameworkElement[] { AcceptButton, CancelButton }, new System.Windows.FrameworkElement[] { });

    }
    internal void Post(Action Action)
    {
      WpfContainer.Dispatcher.BeginInvoke(Action);
    }
    internal void Call(Action Action)
    {
      if (System.Windows.Threading.Dispatcher.CurrentDispatcher == WpfContainer.Dispatcher)
        Action();
      else
        WpfContainer.Dispatcher.Invoke(Action);
    }
    internal System.Windows.Media.Brush TranslateBrush(Inv.Colour InvColour)
    {
      if (InvColour == null)
      {
        return null;
      }
      else if (InvColour.Node == null)
      {
        var Result = new System.Windows.Media.SolidColorBrush(TranslateColour(InvColour));
        Result.Freeze();

        InvColour.Node = Result;

        return Result;
      }
      else
      {
        return (System.Windows.Media.SolidColorBrush)InvColour.Node;
      }
    }
    internal System.Windows.Media.Pen TranslatePen(Inv.Colour Colour, int Thickness, System.Windows.Media.PenLineJoin LineJoin = System.Windows.Media.PenLineJoin.Miter)
    {
      if (Colour == null || Thickness <= 0)
        return null;

      LookupPenKey.Colour = Colour;
      LookupPenKey.Thickness = Thickness;

      return PenDictionary.GetOrAdd(LookupPenKey, C =>
      {
        var Result = new System.Windows.Media.Pen(TranslateBrush(Colour), Thickness)
        {
          EndLineCap = System.Windows.Media.PenLineCap.Round, // smooth joins for DrawLine.
          LineJoin = LineJoin
        };
        Result.Freeze();
        return Result;
      });
    }
    internal void TranslateRect(Inv.Rect InvRect, ref System.Windows.Rect WpfRect)
    {
      WpfRect.X = InvRect.Left;
      WpfRect.Y = InvRect.Top;
      WpfRect.Width = InvRect.Width;
      WpfRect.Height = InvRect.Height;
    }
    internal System.Windows.Point TranslatePoint(Inv.Point InvPoint)
    {
      return new System.Windows.Point(InvPoint.X, InvPoint.Y);
    }
    internal System.Windows.FontWeight TranslateFontWeight(FontWeight InvFontWeight)
    {
      switch (InvFontWeight)
      {
        case Inv.FontWeight.Thin:
          return System.Windows.FontWeights.Thin;

        case Inv.FontWeight.Light:
          return System.Windows.FontWeights.Light;

        case Inv.FontWeight.Regular:
          return System.Windows.FontWeights.Regular;

        case Inv.FontWeight.Medium:
          return System.Windows.FontWeights.Medium;

        case Inv.FontWeight.Bold:
          return System.Windows.FontWeights.Bold;

        case Inv.FontWeight.Heavy:
          return System.Windows.FontWeights.Black;

        default:
          throw new ApplicationException("FontWeight not handled: " + InvFontWeight);
      }
    }
    internal System.Windows.Media.Imaging.BitmapSource TranslateImage(Inv.Image Image)
    {
      if (Image == null)
      {
        return null;
      }
      else 
      {
        var Result = Image.Node as System.Windows.Media.Imaging.BitmapSource;

        if (Result == null)
        {
          Result = LoadImage(Image);
          Image.Node = Result;
        }

        return Result;
      }
    }
    internal void Guard(Action Action)
    {
      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception);
      }
    }
    internal void CheckModifier()
    {
      InvApplication.Window.CheckModifier(GetModifier());
    }
    internal object PlaySound(Sound Sound, float Volume, float Rate, bool Loop)
    {
      if (SoundPlayer != null)
        return SoundPlayer.Play(Sound, Volume, Rate, Loop);
      else
        return null;
    }

    private void Resize()
    {
      var OwnerWindow = System.Windows.Window.GetWindow(WpfContainer);
      var FormsScreen = OwnerWindow != null ? System.Windows.Forms.Screen.FromHandle(new System.Windows.Interop.WindowInteropHelper(OwnerWindow).Handle) : null;

      Debug.Assert(OwnerWindow != null || !WpfShell.FullScreenMode, "Must not be fullscreen is there is no owner window");

      var ScreenWidth = FormsScreen != null ? FormsScreen.Bounds.Width : 0;
      var ScreenHeight = FormsScreen != null ? FormsScreen.Bounds.Height : 0;

      var ResolutionWidthPixels = WpfShell.FullScreenMode ? ScreenWidth : (int)WpfContainer.ActualWidth;
      if (ResolutionWidthPixels <= 0 && WpfContainer.Width != double.NaN)
        ResolutionWidthPixels = (int)WpfContainer.Width;

      var ResolutionHeightPixels = WpfShell.FullScreenMode ? ScreenHeight : (int)WpfContainer.ActualHeight;
      if (ResolutionHeightPixels <= 0 && WpfContainer.Height != double.NaN)
        ResolutionHeightPixels = (int)WpfContainer.Height;

      if (!WpfShell.FullScreenMode && WpfContainer is System.Windows.Window)
      {
        var HorizontalBorderHeight = (int)System.Windows.SystemParameters.ResizeFrameHorizontalBorderHeight;
        var VerticalBorderWidth = (int)System.Windows.SystemParameters.ResizeFrameVerticalBorderWidth;
        var CaptionHeight = (int)System.Windows.SystemParameters.CaptionHeight;
      
        ResolutionWidthPixels -= (VerticalBorderWidth * 2) + 8;
        ResolutionHeightPixels -= (HorizontalBorderHeight * 2) + CaptionHeight + 9;
      }

      // NOTE: fallback to resolution pixels, based on 24" monitor.

      if (!WpfShell.PreventDeviceEmulation)
      {
        var IsActiveEmulation = WpfShell.DeviceEmulation != null;

        if (IsActiveEmulation)
        {
          if (WpfShell.DeviceEmulationFramed)
          {
            var ResourceLocation = "Inv.Resources.Frames." + WpfShell.DeviceEmulation.Name.Replace('/', '_') + "." + (WpfShell.DeviceEmulationRotated ? "Landscape" : "Portrait") + ".png";

            if (ResourceLocation != DeviceFrameLocation)
            {
              using (var ResourceStream = WpfShell.GetResourceStream(ResourceLocation))
              {
                if (ResourceStream == null)
                {
                  this.DeviceFrameImage = null;
                }
                else
                {
                  var BitmapImage = new System.Windows.Media.Imaging.BitmapImage();

                  BitmapImage.BeginInit();
                  BitmapImage.CacheOption = System.Windows.Media.Imaging.BitmapCacheOption.OnLoad;
                  BitmapImage.StreamSource = ResourceStream;
                  BitmapImage.EndInit();

                  WpfEmulationDeviceGraphic.Source = BitmapImage;
                  WpfEmulationDeviceGraphic.Width = BitmapImage.PixelWidth / 2;
                  WpfEmulationDeviceGraphic.Height = BitmapImage.PixelHeight / 2;

                  this.DeviceFrameImage = BitmapImage;
                }
              }

              this.DeviceFrameLocation = ResourceLocation;
            }
          }

          WpfEmulationApplicationLabel.Text = IsActiveEmulation ? InvApplication.Title + Environment.NewLine + WpfShell.DeviceEmulation.Name + " Emulation" : null;
          WpfEmulationInstructionLabel.Text = IsActiveEmulation ? "Ctrl + Up/Down to change the device" + Environment.NewLine + "Ctrl + Left/Right to rotate this device" + Environment.NewLine + "Ctrl + Plus/Minus to toggle the frame" : null;
          WpfEmulationDeviceGraphic.Source = IsActiveEmulation && WpfShell.DeviceEmulationFramed ? DeviceFrameImage : null;

          if (WpfShell.DeviceEmulationRotated)
          {
            ResolutionWidthPixels = WpfShell.DeviceEmulation.Height;
            ResolutionHeightPixels = WpfShell.DeviceEmulation.Width;
          }
          else
          {
            ResolutionWidthPixels = WpfShell.DeviceEmulation.Width;
            ResolutionHeightPixels = WpfShell.DeviceEmulation.Height;
          }
        }
      }

      InvApplication.Window.Width = ResolutionWidthPixels;
      InvApplication.Window.Height = ResolutionHeightPixels;

      WpfMaster.Width = ResolutionWidthPixels;
      WpfMaster.Height = ResolutionHeightPixels;
    }
    private void Process()
    {
      if (InvApplication.IsExit)
      {
        if (ShutdownEvent != null)
          ShutdownEvent();
      }
      else
      {
        var InvWindow = InvApplication.Window;

        InvWindow.ProcessInvoke();

        if (InvWindow.ActiveTimerSet.Count > 0)
        {
          foreach (var InvTimer in InvWindow.ActiveTimerSet)
          {
            var WpfTimer = AccessTimer(InvTimer, S =>
            {
              var Result = new System.Windows.Threading.DispatcherTimer();
              Result.Tick += (Sender, Event) => S.IntervalInvoke();
              return Result;
            });

            if (InvTimer.IsRestarting)
            {
              InvTimer.IsRestarting = false;
              WpfTimer.Stop();
            }

            if (WpfTimer.Interval != InvTimer.IntervalTime)
              WpfTimer.Interval = InvTimer.IntervalTime;

            if (InvTimer.IsEnabled && !WpfTimer.IsEnabled)
              WpfTimer.Start();
            else if (!InvTimer.IsEnabled && WpfTimer.IsEnabled)
              WpfTimer.Stop();
          }

          InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
        }

        var InvSurfaceActive = InvWindow.ActiveSurface;

        if (InvSurfaceActive != null)
        {
          var WpfSurface = AccessSurface(InvSurfaceActive, S =>
          {
            var Result = new WpfSurface();
            Result.Name = "Surface" + SurfaceCount++;
            WpfMaster.RegisterName(Result.Name, Result);
            return Result;
          });

          if (!WpfMaster.Children.Contains(WpfSurface))
            InvSurfaceActive.ArrangeInvoke();

          ProcessTransition(WpfSurface);

          InvSurfaceActive.ComposeInvoke();

          UpdateSurface(InvSurfaceActive, WpfSurface);

          if (InvSurfaceActive.Focus != null)
          {
            var WpfFocus = InvSurfaceActive.Focus.Node as System.Windows.FrameworkElement;
            var WpfOverrideFocus = WpfFocus as WpfOverrideFocusContract;

            InvSurfaceActive.Focus = null;

            if (WpfOverrideFocus != null)
              WpfMaster.Dispatcher.BeginInvoke((Action)delegate { WpfOverrideFocus.OverrideFocus(); }, System.Windows.Threading.DispatcherPriority.Input);
            else if (WpfFocus != null)
              WpfMaster.Dispatcher.BeginInvoke((Action)delegate { WpfFocus.Focus(); }, System.Windows.Threading.DispatcherPriority.Input);
          }
        }
        else
        {
          WpfMaster.Children.Clear();
        }

        if (InvSurfaceActive != null)
          ProcessAnimation(InvSurfaceActive);

        if (InvWindow.Render())
        {
          if (InvWindow.Background.Render())
            WpfMaster.Background = TranslateBrush(InvWindow.Background.Colour ?? Inv.Colour.Black);
        }

        InvWindow.DisplayRate.Calculate();

        if (!WpfShell.PreventDeviceEmulation)
        {
          WpfEmulationFrameLabel.Text = WpfShell.DeviceEmulation != null ? string.Format("{0} x {1} | {2} fps | {3} pc | {4:F1} MB",
            InvWindow.Width, InvWindow.Height, InvWindow.DisplayRate.PerSecond, InvWindow.ActiveSurface != null ? InvWindow.ActiveSurface.GetPanelCount() : 0, InvApplication.Process.GetMemoryUsage()) : null;
        }
      }
    }

    private void UpdateSurface(Surface InvSurface, WpfSurface WpfSurface)
    {
      if (InvSurface.Render())
      {
        WpfSurface.Background = TranslateBrush(InvSurface.Background.Colour);
        WpfSurface.Child = TranslatePanel(InvSurface.Content);
      }

      InvSurface.ProcessChanges(P => TranslatePanel(P));
    }
    private void ProcessTransition(WpfSurface WpfSurface)
    {
      var InvWindow = InvApplication.Window;

      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
        Debug.Assert(WpfMaster.Children.Contains(WpfSurface));
      }
      else if (!IsTransitioning) // don't transition while animating a transition.
      {
        var WpfFromSurface = WpfMaster.Children.Count == 0 ? null : (WpfSurface)WpfMaster.Children[0];
        var WpfToSurface = WpfSurface;

        if (WpfFromSurface == WpfToSurface)
        {
          Debug.WriteLine("Transition to self");
        }
        else
        {
          // give the previous surface a chance to process before it is animated away.
          if (InvTransition.FromSurface != null && WpfFromSurface != null)
            UpdateSurface(InvTransition.FromSurface, WpfFromSurface);

          switch (InvTransition.Animation)
          {
            case TransitionAnimation.None:
              WpfMaster.Children.Clear();
              WpfMaster.Children.Add(WpfToSurface);
              break;

            case TransitionAnimation.Fade:
              IsTransitioning = true;

              WpfMaster.Children.Add(WpfToSurface);

              WpfToSurface.IsHitTestVisible = false;
              WpfToSurface.Opacity = 0.0;

              var WpfFadeDuration = new System.Windows.Duration(TimeSpan.FromMilliseconds(InvTransition.Duration.TotalMilliseconds / 2));

              var WpfFadeInAnimation = new System.Windows.Media.Animation.DoubleAnimation()
              {
                From = 0.0,
                To = 1.0,
                Duration = WpfFadeDuration,
                FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop
              };
              WpfFadeInAnimation.Completed += (Sender, Event) =>
              {
                WpfToSurface.IsHitTestVisible = true;
                WpfToSurface.Opacity = 1.0;

                IsTransitioning = false;
              };
              WpfFadeInAnimation.Freeze();

              if (WpfFromSurface == null)
              {
                WpfToSurface.BeginAnimation(System.Windows.UIElement.OpacityProperty, WpfFadeInAnimation);
              }
              else
              {
                WpfFromSurface.IsHitTestVisible = false;

                var WpfFadeOutAnimation = new System.Windows.Media.Animation.DoubleAnimation()
                {
                  From = 1.0,
                  To = 0.0,
                  Duration = WpfFadeDuration,
                  FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop
                };
                WpfFadeOutAnimation.Completed += (Sender, Event) =>
                {
                  WpfFromSurface.IsHitTestVisible = true;
                  WpfMaster.Children.Remove(WpfFromSurface);

                  WpfToSurface.BeginAnimation(System.Windows.UIElement.OpacityProperty, WpfFadeInAnimation);
                };
                WpfFadeOutAnimation.Freeze();

                WpfFromSurface.BeginAnimation(System.Windows.UIElement.OpacityProperty, WpfFadeOutAnimation);
              }
              break;

            case TransitionAnimation.CarouselBack:
            case TransitionAnimation.CarouselNext:
              IsTransitioning = true;

              var CarouselForward = InvTransition.Animation == TransitionAnimation.CarouselNext;

              var WpfCarouselDuration = new System.Windows.Duration(InvTransition.Duration);

              var WpfCarouselStoryboard = new System.Windows.Media.Animation.Storyboard();

              if (WpfFromSurface != null)
              {
                var FromCarouselTransform = new System.Windows.Media.TranslateTransform() { X = 0, Y = 0 };
                WpfFromSurface.RenderTransform = FromCarouselTransform;

                WpfFromSurface.IsHitTestVisible = false;

                var WpfFromAnimation = new System.Windows.Media.Animation.DoubleAnimation()
                {
                  AccelerationRatio = 0.5,
                  DecelerationRatio = 0.5,
                  Duration = WpfCarouselDuration,
                  From = 0,
                  To = CarouselForward ? -WpfMaster.ActualWidth : WpfMaster.ActualWidth
                };
                System.Windows.Media.Animation.Storyboard.SetTarget(WpfFromAnimation, FromCarouselTransform);
                System.Windows.Media.Animation.Storyboard.SetTargetProperty(WpfFromAnimation, new System.Windows.PropertyPath(System.Windows.Media.TranslateTransform.XProperty));
                WpfFromAnimation.Freeze();

                WpfCarouselStoryboard.Children.Add(WpfFromAnimation);
              }

              if (WpfToSurface != null)
              {
                var ToCarouselTransform = new System.Windows.Media.TranslateTransform() { X = 0, Y = 0 };
                WpfToSurface.RenderTransform = ToCarouselTransform;

                WpfMaster.Children.Add(WpfToSurface);

                WpfToSurface.IsHitTestVisible = false;
                var WpfToAnimation = new System.Windows.Media.Animation.DoubleAnimation()
                {
                  AccelerationRatio = 0.5,
                  DecelerationRatio = 0.5,
                  Duration = WpfCarouselDuration,
                  From = CarouselForward ? WpfMaster.ActualWidth : -WpfMaster.ActualWidth,
                  To = 0
                };
                System.Windows.Media.Animation.Storyboard.SetTarget(WpfToAnimation, ToCarouselTransform);
                System.Windows.Media.Animation.Storyboard.SetTargetProperty(WpfToAnimation, new System.Windows.PropertyPath(System.Windows.Media.TranslateTransform.XProperty));
                WpfToAnimation.Freeze();

                WpfCarouselStoryboard.Children.Add(WpfToAnimation);
              }

              WpfCarouselStoryboard.Completed += (Sender, Event) =>
              {
                if (WpfFromSurface != null)
                {
                  WpfFromSurface.IsHitTestVisible = true;
                  WpfMaster.Children.Remove(WpfFromSurface);
                  WpfFromSurface.RenderTransform = null;
                }

                if (WpfToSurface != null)
                {
                  WpfToSurface.IsHitTestVisible = true;
                  WpfToSurface.RenderTransform = null;
                }

                WpfCarouselStoryboard.Remove(WpfMaster);

                IsTransitioning = false;
              };
              WpfCarouselStoryboard.Freeze();

              WpfCarouselStoryboard.Begin(WpfMaster, null);
              break;

            default:
              throw new ApplicationException("TransitionAnimation not handled: " + InvTransition.Animation);
          }
        }

        InvWindow.ActiveTransition = null;
      }
    }
    private void ProcessAnimation(Inv.Surface InvSurfaceActive)
    {
      if (InvSurfaceActive.StopAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StopAnimationSet)
        {
          if (InvAnimation.Node != null)
          {
            var WpfStoryboard = (System.Windows.Media.Animation.Storyboard)InvAnimation.Node;

            foreach (var InvTarget in InvAnimation.GetTargets())
            {
              var WpfPanel = TranslatePanel(InvTarget.Panel);
              if (WpfPanel != null)
              {
                //if (System.Windows.DependencyPropertyHelper.GetValueSource(WpfPanel, System.Windows.UIElement.OpacityProperty).IsAnimated)
                //  WpfPanel.SetValue(System.Windows.UIElement.OpacityProperty, WpfPanel.GetValue(System.Windows.UIElement.OpacityProperty));

                WpfPanel.Opacity = WpfPanel.Opacity;
                InvTarget.Panel.Opacity.BypassSet((float)WpfPanel.Opacity);
              }
            }

            WpfStoryboard.Stop();
            //WpfStoryboard.Remove(); // Remove stuffs things up.
          }
        }
        InvSurfaceActive.StopAnimationSet.Clear();
      }
      else if (InvSurfaceActive.StartAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StartAnimationSet)
        {
          var WpfStoryboard = new System.Windows.Media.Animation.Storyboard();
          InvAnimation.Node = WpfStoryboard;
          WpfStoryboard.Completed += (Sender, Event) =>
          {
            InvAnimation.Complete();
            InvAnimation.Node = null;
          };

          foreach (var InvTarget in InvAnimation.GetTargets())
          {
            var WpfPanel = TranslatePanel(InvTarget.Panel);
            foreach (var InvCommand in InvTarget.GetCommands())
              WpfStoryboard.Children.Add(TranslateAnimationCommand(InvTarget.Panel, WpfPanel, InvCommand));
          }

          WpfStoryboard.Freeze();
          WpfStoryboard.Begin();
        }
        InvSurfaceActive.StartAnimationSet.Clear();
      }
    }
    private System.Windows.Media.Animation.Timeline TranslateAnimationCommand(Inv.Panel InvPanel, System.Windows.FrameworkElement WpfPanel, AnimationCommand InvCommand)
    {
      // TODO: more types of animation commands.
      var Result = TranslateAnimationOpacityCommand(InvPanel, WpfPanel, InvCommand);
      Result.Completed += (Sender, Event) => InvCommand.Complete();
      Result.Freeze();
      return Result;
    }
    private System.Windows.Media.Animation.Timeline TranslateAnimationOpacityCommand(Inv.Panel InvPanel, System.Windows.FrameworkElement WpfPanel, AnimationCommand InvCommand)
    {
      var InvOpacityCommand = (Inv.AnimationOpacityCommand)InvCommand;

      var Result = new System.Windows.Media.Animation.DoubleAnimation()
      {
        From = InvOpacityCommand.From,
        To = InvOpacityCommand.To,
        Duration = InvOpacityCommand.Duration,
        FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop
      };

      // NOTE: if you set BeginTime to null, the animation will not run!
      if (InvOpacityCommand.Offset != null)
        Result.BeginTime = InvOpacityCommand.Offset;

      System.Windows.Media.Animation.Storyboard.SetTarget(Result, WpfPanel);
      System.Windows.Media.Animation.Storyboard.SetTargetProperty(Result, new System.Windows.PropertyPath(System.Windows.UIElement.OpacityProperty));

      WpfPanel.Opacity = InvOpacityCommand.From;
      InvPanel.Opacity.BypassSet(InvOpacityCommand.To);

      Result.RemoveRequested += (Sender, Event) =>
      {
        Debug.WriteLine("Test");
      };
      Result.Completed += (Sender, Event) =>
      {
        WpfPanel.Opacity = InvPanel.Opacity.Get();
      };

      return Result;
    }

    private System.Windows.FrameworkElement TranslateBoard(Inv.Panel InvPanel)
    {
      var InvBoard = (Inv.Board)InvPanel;

      var WpfBoard = AccessPanel(InvBoard, P =>
      {
        return new WpfBoard();
      });

      RenderPanel(InvBoard, WpfBoard, () =>
      {
        TranslateLayout(InvBoard, WpfBoard);

        if (InvBoard.PinCollection.Render())
        {
          foreach (var WpfElement in WpfBoard.GetElements())
            WpfElement.Child = null;

          WpfBoard.RemoveElements();
          foreach (var InvPin in InvBoard.PinCollection)
          {
            var WpfElement = WpfBoard.AddElement();
            System.Windows.Controls.Canvas.SetLeft(WpfElement, InvPin.Rect.Left);
            System.Windows.Controls.Canvas.SetTop(WpfElement, InvPin.Rect.Top);
            WpfElement.Width = InvPin.Rect.Width;
            WpfElement.Height = InvPin.Rect.Height;

            var WpfPanel = TranslatePanel(InvPin.Panel);
            WpfElement.Child = WpfPanel;
          }
        }
      });

      return WpfBoard;
    }
    private System.Windows.FrameworkElement TranslateBrowser(Inv.Panel InvPanel)
    {
      var InvBrowser = (Inv.Browser)InvPanel;

      var WpfBrowser = AccessPanel(InvBrowser, P =>
      {
        var Result = new WpfBrowser();
        return Result;
      });

      RenderPanel(InvBrowser, WpfBrowser, () =>
      {
        TranslateLayout(InvBrowser, WpfBrowser);

        var Navigate = InvBrowser.UriSingleton.Render();
        if (InvBrowser.HtmlSingleton.Render())
          Navigate = true;

        if (Navigate)
          WpfBrowser.Navigate(InvBrowser.UriSingleton.Data, InvBrowser.HtmlSingleton.Data);
      });

      return WpfBrowser;
    }
    private System.Windows.FrameworkElement TranslateButton(Inv.Panel InvPanel)
    {
      var InvButton = (Inv.Button)InvPanel;

      var WpfButton = AccessPanel(InvButton, P =>
      {
        var Result = new WpfButton();
        Result.PressClick += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface))
            P.PressInvoke();
        };
        Result.ReleaseClick += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface))
            P.ReleaseInvoke();
        };
        Result.LeftClick += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface))
            P.SingleTapInvoke();

          Event.Handled = true;
        };
        Result.RightClick += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface))
            P.ContextTapInvoke();

          Event.Handled = true;
        };
        return Result;
      });

      RenderPanel(InvButton, WpfButton, () =>
      {
        TranslateVisibility(InvButton.Visibility, WpfButton);
        TranslateMargin(InvButton.Margin, WpfButton);
        TranslatePadding(InvButton.Padding, WpfButton);
        TranslateOpacity(InvButton.Opacity, WpfButton);
        TranslateAlignment(InvButton.Alignment, WpfButton);
        TranslateSize(InvButton.Size, WpfButton);
        TranslateElevation(InvButton.Elevation, WpfButton);

        if (InvButton.Border.IsChanged)
        {
          TranslateBorder(InvButton.Border, WpfButton);
          WpfButton.HoverBorderThickness = WpfButton.BorderThickness;
          WpfButton.HoverBorderBrush = WpfButton.BorderBrush;
        }

        if (InvButton.Corner.Render())
          WpfButton.CornerRadius = TranslateCorner(InvButton.Corner);

        if (InvButton.Background.Render())
        {
          WpfButton.Background = TranslateBrush(InvButton.Background.Colour);
          WpfButton.HoverBackgroundBrush = TranslateHoverBrush(InvButton.Background.Colour);
          WpfButton.PressedBackgroundBrush = TranslatePressBrush(InvButton.Background.Colour);
        }

        WpfButton.IsEnabled = InvButton.IsEnabled;
        WpfButton.Focusable = InvButton.IsFocusable;

        if (InvButton.ContentSingleton.Render())
        {
          WpfButton.SetContent(null); // TODO: there is a problem in Help > Items if this line is removed.
          WpfButton.SetContent(TranslatePanel(InvButton.ContentSingleton.Data));
        }
      });

      return WpfButton;
    }
    private System.Windows.FrameworkElement TranslateCanvas(Inv.Panel InvPanel)
    {
      var InvCanvas = (Inv.Canvas)InvPanel;

      var WpfCanvas = AccessPanel(InvCanvas, P =>
      {
        var Result = new WpfCanvas(this);

        var IsLeftPressed = false;
        var IsRightPressed = false;
        var MouseDownTimestamp = 0;

        // TODO: IsManipulationEnabled breaks all the mouse events.
        //       which would be fine except I have to reimplement all the useful touch gestures (perhaps learn from Blake.NUI).
        //Result.IsManipulationEnabled = true; 
        /*
        var TouchCount = 0;

        Result.TouchMove += (Sender, Event) =>
        {
          var MovePoint = TranslatePoint(Event.GetTouchPoint(Result).Position);
          P.MoveInvoke(MovePoint);

          Event.Handled = true;
        };
        Result.TouchDown += (Sender, Event) =>
        {
          TouchCount++;
        
          if (TouchCount == 1)
          {
            System.Windows.Input.Mouse.Capture(Result);
            Result.CaptureTouch(Event.TouchDevice);
        
            var PressPoint = TranslatePoint(Event.GetTouchPoint(Result).Position);
            P.PressInvoke(PressPoint);
        
            Event.Handled = true;
          }
        };
        Result.TouchUp += (Sender, Event) =>
        {
          TouchCount--;
        
          if (TouchCount == 0)
          {
            Result.ReleaseTouchCapture(Event.TouchDevice);
            Result.ReleaseMouseCapture();
        
            var ReleasePoint = TranslatePoint(Event.GetTouchPoint(Result).Position);
            P.ReleaseInvoke(ReleasePoint);
        
            Event.Handled = true;
          }
        };
        */
        Result.MouseMove += (Sender, Event) =>
        {
          if (IsLeftPressed || IsRightPressed)
          {
            var MovePoint = TranslatePoint(Event.GetPosition(Result));
            P.MoveInvoke(MovePoint);

            Event.Handled = true;
          }
        };
        Result.MouseDown += (Sender, Event) =>
        {
          System.Windows.Input.Mouse.Capture(Result);

          if (Event.ChangedButton == System.Windows.Input.MouseButton.Left && Event.ClickCount < 2)
          {
            IsLeftPressed = true;
            MouseDownTimestamp = Event.Timestamp;

            var PressPoint = TranslatePoint(Event.GetPosition(Result));
            P.PressInvoke(PressPoint);
          }
          else if (Event.ChangedButton == System.Windows.Input.MouseButton.Right && Event.ClickCount < 2)
          {
            IsRightPressed = true;
          }

          Event.Handled = true;
        };
        Result.MouseUp += (Sender, Event) =>
        {
          Result.ReleaseMouseCapture();

          var ReleasePoint = TranslatePoint(Event.GetPosition(Result));

          if (Event.ChangedButton == System.Windows.Input.MouseButton.Left && Event.ClickCount < 2)
          {
            if (IsLeftPressed)
            {
              IsLeftPressed = false;
              P.ReleaseInvoke(ReleasePoint);

              if (Event.Timestamp - MouseDownTimestamp <= 250)
                P.SingleTapInvoke(ReleasePoint);
            }
          }
          else if (Event.ChangedButton == System.Windows.Input.MouseButton.Right && Event.ClickCount < 2)
          {
            if (IsRightPressed)
            {
              IsRightPressed = false;
              P.ContextTapInvoke(ReleasePoint);
            }
          }

          Event.Handled = true;
        };
        Result.MouseDoubleClick += (Sender, Event) =>
        {
          if (Event.ChangedButton == System.Windows.Input.MouseButton.Left)
            P.DoubleTapInvoke(TranslatePoint(Event.GetPosition(Result)));
        };
        Result.MouseWheel += (Sender, Event) =>
        {
          P.ZoomInvoke(TranslatePoint(Event.GetPosition(Result)), Event.Delta > 0 ? +1 : -1);
        };
        Result.ManipulationDelta += (Sender, Event) =>
        {
          // NOTE: this requires IsManipulationEnabled which is a problem (see comment above).

          if (Event.DeltaManipulation.Scale.X != 1.0 || Event.DeltaManipulation.Scale.Y != 1.0)
            P.ZoomInvoke(TranslatePoint(Event.Manipulators.First().GetPosition(Result)), Event.DeltaManipulation.Scale.X > 1.0 || Event.DeltaManipulation.Scale.Y > 1.0 ? +1 : -1);
        };

        return Result;
      });

      RenderPanel(InvCanvas, WpfCanvas, () =>
      {
        TranslateLayout(InvCanvas, WpfCanvas);

        if (InvCanvas.Redrawing)
        {
          Guard(() =>
          {
            WpfCanvas.StartDrawing();
            try
            {
              InvCanvas.DrawInvoke(WpfCanvas);
            }
            finally
            {
              WpfCanvas.StopDrawing();
            }
          });
        }
      });

      return WpfCanvas;
    }
    private System.Windows.FrameworkElement TranslateDock(Inv.Panel InvPanel)
    {
      var InvDock = (Inv.Dock)InvPanel;

      var IsHorizontal = InvDock.Orientation == DockOrientation.Horizontal;

      var WpfDock = AccessPanel(InvDock, P =>
      {
        var Result = new WpfGrid();
        return Result;
      });

      RenderPanel(InvDock, WpfDock, () =>
      {
        TranslateLayout(InvDock, WpfDock);

        if (InvDock.CollectionRender())
          WpfDock.ComposeDock(IsHorizontal, InvDock.HeaderCollection.Select(H => TranslatePanel(H)), InvDock.ClientCollection.Select(H => TranslatePanel(H)), InvDock.FooterCollection.Select(H => TranslatePanel(H)));
      });

      return WpfDock;
    }
    private System.Windows.FrameworkElement TranslateEdit(Inv.Panel InvPanel)
    {
      var InvEdit = (Inv.Edit)InvPanel;

      var IsPassword = InvEdit.Input == EditInput.Password;
      var IsSearch = InvEdit.Input == EditInput.Search;

      var WpfEdit = AccessPanel(InvEdit, P =>
      {
        if (IsSearch)
        {
          var Result = new WpfSearch();

          Result.ChangeEvent += () => P.ChangeText(Result.Text);
          Result.ReturnEvent += () => P.Return();

          return (WpfClipper)Result;
        }
        else
        {
          var Result = new WpfEdit(IsPassword);

          // TODO: Wpf allowed input.
          switch (InvEdit.Input)
          {
            case EditInput.Decimal:
              break;

            case EditInput.Email:
              break;

            case EditInput.Integer:
              break;

            case EditInput.Name:
              break;

            case EditInput.Number:
              break;

            case EditInput.Password:
              break;

            case EditInput.Phone:
              break;

            case EditInput.Text:
              break;

            case EditInput.Uri:
              break;

            default:
              throw new Exception("EditInput not handled: " + InvEdit.Input);
          }

          Result.TextChanged += (Sender, Event) => P.ChangeText(Result.Text);
          Result.KeyDown += (Sender, Event) =>
          {
            if (Event.Key == System.Windows.Input.Key.Return)
              P.Return();
          };

          return (WpfClipper)Result;
        }
      });

      RenderPanel(InvEdit, WpfEdit, () =>
      {
        TranslateLayout(InvEdit, WpfEdit);

        if (IsSearch)
        {
          var WpfSearchEdit = (WpfSearch)WpfEdit;

          TranslateFont(InvEdit.Font, WpfSearchEdit.TextBox);

          WpfSearchEdit.TextBox.IsReadOnly = InvEdit.IsReadOnly;
          WpfSearchEdit.Text = InvEdit.Text;
        }
        else
        {
          var WpfTextEdit = (WpfEdit)WpfEdit;

          if (IsPassword)
            TranslateFont(InvEdit.Font, WpfTextEdit.GetPasswordBox(), null);
          else
            TranslateFont(InvEdit.Font, WpfTextEdit.GetTextBox());

          WpfTextEdit.IsReadOnly = InvEdit.IsReadOnly;
          WpfTextEdit.Text = InvEdit.Text;
        }
      });

      return WpfEdit;
    }
    private System.Windows.FrameworkElement TranslateFlow(Inv.Panel InvPanel)
    {
      var InvFlow = (Inv.Flow)InvPanel;

      var WpfFlow = AccessPanel(InvFlow, P =>
      {
        var Result = new WpfFlow();
        Result.SectionCountQuery = () => InvFlow.SectionList.Count;
        Result.HeaderContentQuery = (Section) => TranslatePanel(InvFlow.SectionList[Section].Header);
        Result.FooterContentQuery = (Section) => TranslatePanel(InvFlow.SectionList[Section].Footer);
        Result.ItemCountQuery = (Section) => InvFlow.SectionList[Section].ItemCount;
        Result.ItemContentQuery = (Section, Item) => TranslatePanel(InvFlow.SectionList[Section].ItemInvoke(Item));
        return Result;
      });

      RenderPanel(InvFlow, WpfFlow, () =>
      {
        TranslateLayout(InvFlow, WpfFlow);

        if (InvFlow.IsRefresh)
        {
          InvFlow.IsRefresh = false;

          // TODO: animate refresh.
        }

        if (InvFlow.IsReload || InvFlow.ReloadSectionList.Count > 0 || InvFlow.ReloadItemList.Count > 0)
        {
          InvFlow.IsReload = false;
          InvFlow.ReloadSectionList.Clear(); // not supported.
          InvFlow.ReloadItemList.Clear(); // not supported.

          WpfFlow.Reload();
        }

        if (InvFlow.ScrollSection != null || InvFlow.ScrollIndex != null)
        {
          if (InvFlow.ScrollSection != null && InvFlow.ScrollIndex != null)
            WpfFlow.ScrollTo(InvFlow.ScrollSection.Value, InvFlow.ScrollIndex.Value);

          InvFlow.ScrollSection = null;
          InvFlow.ScrollIndex = null;
        }
      });

      return WpfFlow;
    }
    private System.Windows.FrameworkElement TranslateGraphic(Inv.Panel InvPanel)
    {
      var InvGraphic = (Inv.Graphic)InvPanel;

      var WpfGraphic = AccessPanel(InvGraphic, P =>
      {
        var Result = new WpfGraphic();
        return Result;
      });

      RenderPanel(InvGraphic, WpfGraphic, () =>
      {
        TranslateLayout(InvGraphic, WpfGraphic);

        if (InvGraphic.ImageSingleton.Render())
          WpfGraphic.Source = TranslateImage(InvGraphic.Image);
      });

      return WpfGraphic;
    }
    private System.Windows.FrameworkElement TranslateLabel(Inv.Panel InvPanel)
    {
      var InvLabel = (Inv.Label)InvPanel;

      var WpfLabel = AccessPanel(InvLabel, P =>
      {
        var Result = new WpfLabel();
        return Result;
      });

      RenderPanel(InvLabel, WpfLabel, () =>
      {
        TranslateLayout(InvLabel, WpfLabel);

        TranslateFont(InvLabel.Font, WpfLabel.GetTextBlock());

        WpfLabel.TextWrapping = InvLabel.LineWrapping ? System.Windows.TextWrapping.Wrap : System.Windows.TextWrapping.NoWrap;
        WpfLabel.TextAlignment = InvLabel.Justification == Justification.Left ? System.Windows.TextAlignment.Left : InvLabel.Justification == Justification.Right ? System.Windows.TextAlignment.Right : System.Windows.TextAlignment.Center;
        WpfLabel.Text = InvLabel.Text;
      });

      return WpfLabel;
    }
    private System.Windows.FrameworkElement TranslateMemo(Inv.Panel InvPanel)
    {
      var InvMemo = (Inv.Memo)InvPanel;

      var WpfMemo = AccessPanel(InvMemo, P =>
      {
        var Result = new WpfMemo();
        Result.TextChanged += (Sender, Event) => P.ChangeText(Result.Text);
        return Result;
      });

      RenderPanel(InvMemo, WpfMemo, () =>
      {
        TranslateLayout(InvMemo, WpfMemo);

        TranslateFont(InvMemo.Font, WpfMemo.GetTextBox());

        WpfMemo.IsReadOnly = InvMemo.IsReadOnly;
        WpfMemo.Text = InvMemo.Text;
      });

      return WpfMemo;
    }
    private System.Windows.FrameworkElement TranslateOverlay(Inv.Panel InvPanel)
    {
      var InvOverlay = (Inv.Overlay)InvPanel;

      var WpfOverlay = AccessPanel(InvOverlay, P =>
      {
        var Result = new WpfGrid();
        return Result;
      });

      RenderPanel(InvOverlay, WpfOverlay, () =>
      {
        TranslateLayout(InvOverlay, WpfOverlay);

        if (InvOverlay.PanelCollection.Render())
        {
          WpfOverlay.Children.Clear();
          foreach (var InvElement in InvOverlay.GetPanels())
          {
            var WpfPanel = TranslatePanel(InvElement);
            WpfOverlay.Children.Add(WpfPanel);
          }
        }
      });

      return WpfOverlay;
    }
    private System.Windows.FrameworkElement TranslateScroll(Inv.Panel InvPanel)
    {
      var InvScroll = (Inv.Scroll)InvPanel;

      var WpfScroll = AccessPanel(InvScroll, P =>
      {
        var Result = new WpfScroll(P.Orientation == ScrollOrientation.Vertical, P.Orientation == ScrollOrientation.Horizontal);
        return Result;
      });

      RenderPanel(InvScroll, WpfScroll, () =>
      {
        TranslateLayout(InvScroll, WpfScroll);

        if (InvScroll.ContentSingleton.Render())
          WpfScroll.Content = TranslatePanel(InvScroll.ContentSingleton.Data);
      });

      return WpfScroll;
    }
    private System.Windows.FrameworkElement TranslateFrame(Inv.Panel InvPanel)
    {
      var InvFrame = (Inv.Frame)InvPanel;

      var WpfFrame = AccessPanel(InvFrame, P =>
      {
        var Result = new WpfFrame();
        return Result;
      });

      RenderPanel(InvFrame, WpfFrame, () =>
      {
        TranslateLayout(InvFrame, WpfFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          WpfFrame.SetContent(null); // TODO: there is a problem in Help > Items if this line is removed.
          WpfFrame.SetContent(TranslatePanel(InvFrame.ContentSingleton.Data));
        }
      });

      return WpfFrame;
    }
    private System.Windows.FrameworkElement TranslateStack(Inv.Panel InvPanel)
    {
      var InvStack = (Inv.Stack)InvPanel;

      var WpfStack = AccessPanel(InvStack, P =>
      {
        var Result = new WpfStack();
        Result.Orientation = P.Orientation == StackOrientation.Horizontal ? System.Windows.Controls.Orientation.Horizontal : System.Windows.Controls.Orientation.Vertical;
        return Result;
      });

      RenderPanel(InvStack, WpfStack, () =>
      {
        TranslateLayout(InvStack, WpfStack);

        if (InvStack.PanelCollection.Render())
          WpfStack.Compose(InvStack.GetPanels().Select(P => TranslatePanel(P)));
      });

      return WpfStack;
    }
    private System.Windows.FrameworkElement TranslateTable(Inv.Panel InvPanel)
    {
      var InvTable = (Inv.Table)InvPanel;

      var WpfTable = AccessPanel(InvTable, P =>
      {
        var Result = new WpfGrid();
        return Result;
      });

      RenderPanel(InvTable, WpfTable, () =>
      {
        TranslateLayout(InvTable, WpfTable);

        if (InvTable.CollectionRender())
        {
          WpfTable.Children.Clear();
          WpfTable.RowDefinitions.Clear();
          WpfTable.ColumnDefinitions.Clear();

          foreach (var TableColumn in InvTable.ColumnCollection)
          {
            WpfTable.ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = TranslateTableLength(TableColumn, true) });

            var WpfColumn = TranslatePanel(TableColumn.Content);

            if (WpfColumn != null)
            {
              WpfTable.Children.Add(WpfColumn);

              System.Windows.Controls.Grid.SetColumn(WpfColumn, TableColumn.Index);
              System.Windows.Controls.Grid.SetRow(WpfColumn, 0);
              System.Windows.Controls.Grid.SetRowSpan(WpfColumn, InvTable.ColumnCollection.Count);
            }
          }

          foreach (var TableRow in InvTable.RowCollection)
          {
            WpfTable.RowDefinitions.Add(new System.Windows.Controls.RowDefinition() { Height = TranslateTableLength(TableRow, false) });

            var WpfRow = TranslatePanel(TableRow.Content);

            if (WpfRow != null)
            {
              WpfTable.Children.Add(WpfRow);

              System.Windows.Controls.Grid.SetRow(WpfRow, TableRow.Index);
              System.Windows.Controls.Grid.SetColumn(WpfRow, 0);
              System.Windows.Controls.Grid.SetColumnSpan(WpfRow, InvTable.ColumnCollection.Count);
            }
          }

          foreach (var TableCell in InvTable.CellCollection)
          {
            var WpfCell = TranslatePanel(TableCell.Content);

            if (WpfCell != null)
            {
              WpfTable.Children.Add(WpfCell);

              System.Windows.Controls.Grid.SetColumn(WpfCell, TableCell.Column.Index);
              System.Windows.Controls.Grid.SetRow(WpfCell, TableCell.Row.Index);
            }
          }
        }
      });

      return WpfTable;
    }

    private System.Windows.FrameworkElement TranslatePanel(Inv.Panel InvPanel)
    {
      if (InvPanel == null)
        return null;
      else
        return RouteArray[InvPanel.PanelType](InvPanel);
    }
    private Inv.Key? TranslateKey(System.Windows.Input.Key WpfKey)
    {
      Inv.Key Result;
      if (KeyDictionary.TryGetValue(WpfKey, out Result))
        return Result;
      else
        return null;
    }
    private System.Windows.Media.Brush TranslateHoverBrush(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return null;
      else
        return TranslateBrush(InvColour.Lighten(0.25F));
    }
    private System.Windows.Media.Brush TranslatePressBrush(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return null;
      else
        return TranslateBrush(InvColour.Darken(0.25F));
    }
    private System.Windows.Media.Color TranslateColour(Inv.Colour Colour)
    {
      var ArgbArray = BitConverter.GetBytes(Colour.RawValue);

      return System.Windows.Media.Color.FromArgb(ArgbArray[3], ArgbArray[2], ArgbArray[1], ArgbArray[0]);
    }
    private Inv.Point TranslatePoint(System.Windows.Point WpfPoint)
    {
      return new Inv.Point((int)WpfPoint.X, (int)WpfPoint.Y);
    }
    private void TranslateLayout(Panel InvPanel, System.Windows.Controls.Border WpfPanel)
    {
      if (InvPanel.Background.Render())
        WpfPanel.Background = TranslateBrush(InvPanel.Background.Colour);

      if (InvPanel.Corner.Render())
        WpfPanel.CornerRadius = TranslateCorner(InvPanel.Corner);

      TranslateMargin(InvPanel.Margin, WpfPanel);
      TranslatePadding(InvPanel.Padding, WpfPanel);
      TranslateBorder(InvPanel.Border, WpfPanel);
      TranslateOpacity(InvPanel.Opacity, WpfPanel);
      TranslateAlignment(InvPanel.Alignment, WpfPanel);
      TranslateVisibility(InvPanel.Visibility, WpfPanel);
      TranslateSize(InvPanel.Size, WpfPanel);
      TranslateElevation(InvPanel.Elevation, WpfPanel);
    }
    private void TranslateBorder(Inv.Border InvBorder, System.Windows.Controls.Control WpfControl)
    {
      if (InvBorder.Render())
      {
        WpfControl.BorderBrush = TranslateBrush(InvBorder.Colour);
        WpfControl.BorderThickness = TranslateEdge(InvBorder);
      }
    }
    private void TranslateBorder(Inv.Border InvBorder, System.Windows.Controls.Border WpfBorder)
    {
      if (InvBorder.Render())
      {
        WpfBorder.BorderBrush = TranslateBrush(InvBorder.Colour);
        WpfBorder.BorderThickness = TranslateEdge(InvBorder);
      }
    }
    private void TranslateMargin(Inv.Edge InvMargin, System.Windows.FrameworkElement WpfElement)
    {
      if (InvMargin.Render())
        WpfElement.Margin = TranslateEdge(InvMargin);
    }
    private void TranslatePadding(Inv.Edge InvPadding, System.Windows.Controls.Control WpfControl)
    {
      if (InvPadding.Render())
        WpfControl.Padding = TranslateEdge(InvPadding);
    }
    private void TranslatePadding(Inv.Edge InvPadding, System.Windows.Controls.Border WpfBorder)
    {
      if (InvPadding.Render())
        WpfBorder.Padding = TranslateEdge(InvPadding);
    }
    private void TranslateSize(Inv.Size InvSize, System.Windows.FrameworkElement WpfElement)
    {
      if (InvSize.Render())
      {
        if (InvSize.Width != null)
          WpfElement.Width = InvSize.Width.Value;
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.WidthProperty);

        if (InvSize.Height != null)
          WpfElement.Height = InvSize.Height.Value;
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.HeightProperty);

        if (InvSize.MinimumWidth != null)
          WpfElement.MinWidth = InvSize.MinimumWidth.Value;
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.MinWidthProperty);

        if (InvSize.MinimumHeight != null)
          WpfElement.MinHeight = InvSize.MinimumHeight.Value;
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.MinHeightProperty);

        if (InvSize.MaximumWidth != null)
          WpfElement.MaxWidth = InvSize.MaximumWidth.Value;
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.MaxWidthProperty);

        if (InvSize.MaximumHeight != null)
          WpfElement.MaxHeight = InvSize.MaximumHeight.Value;
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.MaxHeightProperty);
      }
    }
    private void TranslateVisibility(Inv.Visibility InvVisibility, System.Windows.FrameworkElement WpfElement)
    {
      if (InvVisibility.Render())
        WpfElement.Visibility = InvVisibility.Get() ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
    }
    private void TranslateAlignment(Inv.Alignment InvAlignment, System.Windows.FrameworkElement WpfElement)
    {
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Inv.Placement.Stretch:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchLeft:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchCenter:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchRight:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.TopStretch:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopLeft:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopCenter:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopRight:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            break;

          case Inv.Placement.CenterStretch:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            break;

          case Inv.Placement.CenterLeft:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            break;

          case Inv.Placement.Center:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            break;

          case Inv.Placement.CenterRight:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            break;

          case Inv.Placement.BottomStretch:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomLeft:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomCenter:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomRight:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            break;

          default:
            throw new ApplicationException("Inv.Placement not handled: " + InvAlignment.Get());
        }
      }
    }
    private void TranslateElevation(Inv.Elevation InvElevation, System.Windows.FrameworkElement WpfElement)
    {
      if (InvElevation.Render())
      {
        var Depth = InvElevation.Get();

        if (Depth > 0)
        {
          WpfElement.Effect = new System.Windows.Media.Effects.DropShadowEffect()
          {
            Color = System.Windows.Media.Colors.Black, // WPF default anyway.
            Direction = 315,                           // WPF default anyway.
            ShadowDepth = Depth,
            Opacity = 0.50,
            RenderingBias = System.Windows.Media.Effects.RenderingBias.Performance
          };
        }
        else
        {
          WpfElement.Effect = null;
        }
      }
    }
    private void TranslateFont(Inv.Font InvFont, System.Windows.Controls.Control WpfElement, string DefaultFontName)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null || DefaultFontName != null)
          WpfElement.FontFamily = new System.Windows.Media.FontFamily(InvFont.Name ?? DefaultFontName);
        else
          WpfElement.ClearValue(System.Windows.Controls.Control.FontFamilyProperty);

        if (InvFont.Size != null)
          WpfElement.FontSize = InvFont.Size.Value;
        else
          WpfElement.ClearValue(System.Windows.Controls.Control.FontSizeProperty);

        if (InvFont.Colour != null)
          System.Windows.Documents.TextElement.SetForeground(WpfElement, TranslateBrush(InvFont.Colour));
        else
          WpfElement.ClearValue(System.Windows.Controls.Control.ForegroundProperty);

        WpfElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private void TranslateFont(Inv.Font InvFont, System.Windows.Controls.TextBlock WpfElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          WpfElement.FontFamily = new System.Windows.Media.FontFamily(InvFont.Name);
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBlock.FontFamilyProperty);

        if (InvFont.Size != null)
          WpfElement.FontSize = InvFont.Size.Value;
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBlock.FontSizeProperty);

        if (InvFont.Colour != null)
          System.Windows.Controls.TextBlock.SetForeground(WpfElement, TranslateBrush(InvFont.Colour));
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBlock.ForegroundProperty);

        WpfElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private void TranslateFont(Inv.Font InvFont, System.Windows.Controls.TextBox WpfElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          WpfElement.FontFamily = new System.Windows.Media.FontFamily(InvFont.Name);
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBox.FontFamilyProperty);

        if (InvFont.Size != null)
          WpfElement.FontSize = InvFont.Size.Value;
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBox.FontSizeProperty);

        var WpfBrush = TranslateBrush(InvFont.Colour);

        if (WpfBrush != null)
          System.Windows.Documents.TextElement.SetForeground(WpfElement, WpfBrush);
        else
          WpfElement.ClearValue(System.Windows.Documents.TextElement.ForegroundProperty);

        WpfElement.CaretBrush = WpfBrush;

        WpfElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private System.Windows.GridLength TranslateTableLength(TableAxis InvTableLength, bool Horizontal)
    {
      switch (InvTableLength.LengthType)
      {
        case TableAxisLength.Auto:
          return System.Windows.GridLength.Auto;

        case TableAxisLength.Fixed:
          return new System.Windows.GridLength(Horizontal ? InvTableLength.LengthValue : InvTableLength.LengthValue, System.Windows.GridUnitType.Pixel);

        case TableAxisLength.Star:
          return new System.Windows.GridLength(InvTableLength.LengthValue, System.Windows.GridUnitType.Star);

        default:
          throw new Exception("Inv.TableLength not handled: " + InvTableLength.LengthType);
      }
    }
    private System.Windows.Thickness TranslateEdge(Inv.Edge InvEdge)
    {
      return new System.Windows.Thickness(InvEdge.Left, InvEdge.Top, InvEdge.Right, InvEdge.Bottom);
    }
    private System.Windows.CornerRadius TranslateCorner(Inv.Corner InvCorner)
    {
      return new System.Windows.CornerRadius(InvCorner.TopLeft, InvCorner.TopRight, InvCorner.BottomRight, InvCorner.BottomLeft);
    }
    private void TranslateOpacity(Opacity Opacity, System.Windows.FrameworkElement WpfElement)
    {
      if (Opacity.Render())
        WpfElement.Opacity = Opacity.Get();
    }
    private System.Windows.Media.Imaging.BitmapSource LoadImage(Image Image)
    {
      // NOTE: input images are assumed to be 1pt=3px.
      using (var MemoryStream = new MemoryStream(Image.GetBuffer()))
      {
        var Result = new System.Windows.Media.Imaging.BitmapImage();

        Result.BeginInit();
        Result.CacheOption = System.Windows.Media.Imaging.BitmapCacheOption.OnLoad;
        Result.StreamSource = MemoryStream;
        Result.EndInit();
        Result.Freeze();

        return Result;
      }
    }

    private WpfSurface AccessSurface(Inv.Surface InvSurface, Func<Inv.Surface, WpfSurface> BuildFunction)
    {
      if (InvSurface.Node == null)
      {
        var Result = BuildFunction(InvSurface);

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (WpfSurface)InvSurface.Node;
      }
    }
    private System.Windows.Threading.DispatcherTimer AccessTimer(Inv.Timer InvTimer, Func<Inv.Timer, System.Windows.Threading.DispatcherTimer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (System.Windows.Threading.DispatcherTimer)InvTimer.Node;
      }
    }
    private void RenderPanel(Inv.Panel InvPanel, System.Windows.FrameworkElement WpfElement, Action Action)
    {
      if (InvPanel.Render())
        Action();
    }
    private TElement AccessPanel<TPanel, TElement>(TPanel InvPanel, Func<TPanel, TElement> BuildFunction)
      where TPanel : Inv.Panel
      where TElement : System.Windows.FrameworkElement
    {
      if (InvPanel.Node == null)
      {
        var Result = BuildFunction(InvPanel);
        InvPanel.Node = Result;

        return Result;
      }
      else
      {
        return (TElement)InvPanel.Node;
      }
    }
    private void Rendering(object Sender, EventArgs Event)
    {
      Process();
    }

    private System.Windows.Controls.ContentControl WpfContainer;
    private PenKey LookupPenKey;
    private Dictionary<System.Windows.Input.Key, Inv.Key> KeyDictionary;
    private Inv.EnumArray<Inv.PanelType, Func<Inv.Panel, System.Windows.FrameworkElement>> RouteArray;
    private Dictionary<PenKey, System.Windows.Media.Pen> PenDictionary;
    private WpfGrid WpfMaster;
    private WpfLabel WpfEmulationInstructionLabel;
    private WpfLabel WpfEmulationFrameLabel;
    private int SurfaceCount;
    private System.Windows.Controls.Border WpfBorder;
    private WpfSoundPlayer SoundPlayer;
    private Exception SoundException;
    private readonly WpfGraphic WpfEmulationDeviceGraphic;
    private string DeviceFrameLocation;
    private BitmapImage DeviceFrameImage;
    private WpfLabel WpfEmulationApplicationLabel;

    private struct PenKey
    {
      public Inv.Colour Colour;
      public int Thickness;

      public override int GetHashCode()
      {
        return Colour.GetHashCode() ^ Thickness.GetHashCode();
      }
      public override bool Equals(object obj)
      {
        var Key = (PenKey)obj;

        return Key.Thickness == Thickness && Key.Colour == Colour;
      }
    }
  }
}