﻿/*! 13 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Inv.Support;
using System.Windows.Media;
using System.Windows;
using System.Windows.Shell;

namespace Inv
{
  internal sealed class WpfChrome
  {
    public WpfChrome(WpfWindow Window)
    {
      this.Window = Window;
      this.ClipElementSet = new HashSet<FrameworkElement>();
      this.Chrome = new System.Windows.Shell.WindowChrome()
      {
        ResizeBorderThickness = new Thickness(5),
        CaptionHeight = 32,
        CornerRadius = new CornerRadius(0),
        GlassFrameThickness = new Thickness(0)
      };

      Toggle(true);
    }

    public bool IsMouseOver { get; internal set; }
    public event Action MouseEnterEvent;
    public event Action MouseLeaveEvent;

    public int CaptionHeight
    {
      get { return (int)Chrome.CaptionHeight; }
      set { Chrome.CaptionHeight = value; }
    }
    public void ClipCaption(FrameworkElement Element)
    {
      ClipElementSet.Add(Element);

      System.Windows.Shell.WindowChrome.SetIsHitTestVisibleInChrome(Element, true);
    }
    public void BindCaptionHeight(FrameworkElement Element)
    {
      Element.SizeChanged += (sender, e) =>
      {
        if (e.HeightChanged)
          Chrome.CaptionHeight = e.NewSize.Height;
      };
    }

    internal void Toggle(bool Value)
    {
      if (Value)
        System.Windows.Shell.WindowChrome.SetWindowChrome(Window, Chrome);
      else
        System.Windows.Shell.WindowChrome.SetWindowChrome(Window, null);
    }
    internal void SetMouseOver(bool Value)
    {
      if (IsMouseOver != Value)
      {
        if (Value || (!Value && !ClipElementSet.Any(E => E.IsMouseOver)))
        {
          this.IsMouseOver = Value;

          //Debug.WriteLine("Chrome mouse over: " + IsMouseOver);

          if (IsMouseOver)
          {
            if (MouseEnterEvent != null)
              MouseEnterEvent();
          }
          else
          {
            if (MouseLeaveEvent != null)
              MouseLeaveEvent();
          }
        }
      }
    }

    private WpfWindow Window;
    private WindowChrome Chrome;
    private HashSet<FrameworkElement> ClipElementSet;
  }

  internal sealed class WpfSoundPlayer
  {
    public WpfSoundPlayer()
    {
      this.SoundList = new DistinctList<Sound>(1024);

      this.SoundEngine = new IrrKlang.ISoundEngine();
    }

    public IrrKlang.ISound Play(Inv.Sound InvSound, float VolumeScale = 1.0F, float RateScale = 1.0F, bool Looped = false)
    {
      if (SoundEngine == null || InvSound == null)
        return null;

      var SoundSource = InvSound.Node as IrrKlang.ISoundSource;

      if (SoundSource == null)
      {
        SoundSource = SoundEngine.AddSoundSourceFromMemory(InvSound.GetBuffer(), SoundList.Count.ToString());

        if (InvSound.Node == null || !SoundList.Contains(InvSound))
          SoundList.Add(InvSound);

        InvSound.Node = SoundSource;
      }

      var Sound = SoundEngine.Play2D(SoundSource, false, false, true);
      if (Sound != null)
      {
        Sound.Volume = VolumeScale;
        Sound.PlaybackSpeed = RateScale;
        Sound.Looped = Looped;
      }

      return Sound;
    }
    public void Reclamation()
    {
      foreach (var Sound in SoundList)
      {
        var WpfSound = Sound.Node as IrrKlang.ISoundSource;

        if (WpfSound != null)
        {
          SoundEngine.RemoveSoundSource(WpfSound.Name);
          WpfSound.Dispose();
        }

        Sound.Node = null;
      }
      SoundList.Clear();
    }

    private IrrKlang.ISoundEngine SoundEngine;
    private Inv.DistinctList<Inv.Sound> SoundList;
  }

  internal interface WpfOverrideFocusContract
  {
    void OverrideFocus();
  }

  internal sealed class WpfSurface : System.Windows.Controls.Border
  {
    public WpfSurface()
    {
      this.ClipToBounds = true;
    }
  }

  internal sealed class WpfButton : System.Windows.Controls.Button
  {
    static WpfButton()
    {
      HoverBackgroundProperty = System.Windows.DependencyProperty.Register("HoverBackground", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Controls.Button.BackgroundProperty.DefaultMetadata.DefaultValue, System.Windows.FrameworkPropertyMetadataOptions.AffectsArrange | System.Windows.FrameworkPropertyMetadataOptions.AffectsMeasure));
      PressedBackgroundProperty = System.Windows.DependencyProperty.Register("PressedBackground", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Controls.Button.BackgroundProperty.DefaultMetadata.DefaultValue, System.Windows.FrameworkPropertyMetadataOptions.AffectsArrange | System.Windows.FrameworkPropertyMetadataOptions.AffectsMeasure));
      CornerRadiusProperty = System.Windows.DependencyProperty.Register("CornerRadius", typeof(System.Windows.CornerRadius), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(new System.Windows.CornerRadius(0), System.Windows.FrameworkPropertyMetadataOptions.AffectsArrange | System.Windows.FrameworkPropertyMetadataOptions.AffectsMeasure));
      HoverBorderBrushProperty = System.Windows.DependencyProperty.Register("HoverBorderBrush", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Controls.Button.BorderBrushProperty.DefaultMetadata.DefaultValue, System.Windows.FrameworkPropertyMetadataOptions.AffectsArrange | System.Windows.FrameworkPropertyMetadataOptions.AffectsMeasure));
      HoverBorderThicknessProperty = System.Windows.DependencyProperty.Register("HoverBorderThickness", typeof(System.Windows.Thickness), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(new System.Windows.Thickness(0), System.Windows.FrameworkPropertyMetadataOptions.AffectsArrange | System.Windows.FrameworkPropertyMetadataOptions.AffectsMeasure));
      InnerBorderThicknessProperty = System.Windows.DependencyProperty.Register("InnerBorderThickness", typeof(System.Windows.Thickness), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(new System.Windows.Thickness(0), System.Windows.FrameworkPropertyMetadataOptions.AffectsRender));
      InnerBorderBrushProperty = System.Windows.DependencyProperty.Register("InnerBorderBrush", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Media.Brushes.Transparent, System.Windows.FrameworkPropertyMetadataOptions.AffectsRender));
      InnerBorderPressedBrushProperty = System.Windows.DependencyProperty.Register("InnerBorderPressedBrush", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Media.Brushes.Transparent, System.Windows.FrameworkPropertyMetadataOptions.AffectsRender));
      InnerBorderHoverBrushProperty = System.Windows.DependencyProperty.Register("InnerBorderHoverBrush", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Media.Brushes.Transparent, System.Windows.FrameworkPropertyMetadataOptions.AffectsRender));
      InnerBorderFocusBrushProperty = System.Windows.DependencyProperty.Register("InnerBorderFocusBrush", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Media.Brushes.Transparent, System.Windows.FrameworkPropertyMetadataOptions.AffectsRender));
      PressedTransformProperty = System.Windows.DependencyProperty.Register("PressedTransform", typeof(System.Windows.Media.Transform), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(null, System.Windows.FrameworkPropertyMetadataOptions.AffectsRender));

      var StyleDictionary = new System.Windows.ResourceDictionary();
      StyleDictionary.Source = new Uri("/Inv.PlatformW;component/InvWpfStyles.xaml", UriKind.RelativeOrAbsolute);

      DefaultStyle = (System.Windows.Style)StyleDictionary["InvDefaultButton"];
      DefaultStyle.Seal();
    }

    public WpfButton()
    {
      this.Background = null;
      this.HoverBackgroundBrush = null;
      this.PressedBackgroundBrush = null;
      this.HoverBorderThickness = new System.Windows.Thickness(0);
      this.HoverBorderBrush = null;
      this.BorderThickness = new System.Windows.Thickness(0);
      this.InnerBorderThickness = new System.Windows.Thickness(0);
      this.HoverBorderThickness = new System.Windows.Thickness(0);
      this.Margin = new System.Windows.Thickness(0);
      this.Padding = new System.Windows.Thickness(0);
      this.Focusable = false;
      this.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.VerticalContentAlignment = System.Windows.VerticalAlignment.Stretch;

      this.MouseRightButtonDown += new System.Windows.Input.MouseButtonEventHandler(RightClickButton_MouseRightButtonDown);
      this.MouseRightButtonUp += new System.Windows.Input.MouseButtonEventHandler(RightClickButton_MouseRightButtonUp);

      this.Style = DefaultStyle;
    }

    public static readonly System.Windows.DependencyProperty HoverBackgroundProperty;
    public static readonly System.Windows.DependencyProperty PressedBackgroundProperty;
    public static readonly System.Windows.DependencyProperty CornerRadiusProperty;
    public static readonly System.Windows.DependencyProperty HoverBorderBrushProperty;
    public static readonly System.Windows.DependencyProperty HoverBorderThicknessProperty;
    public static readonly System.Windows.DependencyProperty InnerBorderThicknessProperty;
    public static readonly System.Windows.DependencyProperty InnerBorderBrushProperty;
    public static readonly System.Windows.DependencyProperty InnerBorderPressedBrushProperty;
    public static readonly System.Windows.DependencyProperty InnerBorderHoverBrushProperty;
    public static readonly System.Windows.DependencyProperty InnerBorderFocusBrushProperty;
    public static readonly System.Windows.DependencyProperty PressedTransformProperty;

    public System.Windows.Media.Brush HoverBackgroundBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(HoverBackgroundProperty); }
      set { SetValue(HoverBackgroundProperty, value); }
    }
    public System.Windows.Media.Brush PressedBackgroundBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(PressedBackgroundProperty); }
      set { SetValue(PressedBackgroundProperty, value); }
    }
    public System.Windows.CornerRadius CornerRadius
    {
      get { return (System.Windows.CornerRadius)GetValue(CornerRadiusProperty); }
      set { SetValue(CornerRadiusProperty, value); }
    }
    public System.Windows.Media.Brush HoverBorderBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(HoverBorderBrushProperty); }
      set { SetValue(HoverBorderBrushProperty, value); }
    }
    public System.Windows.Thickness HoverBorderThickness
    {
      get { return (System.Windows.Thickness)GetValue(HoverBorderThicknessProperty); }
      set { SetValue(HoverBorderThicknessProperty, value); }
    }
    public System.Windows.Thickness InnerBorderThickness
    {
      get { return (System.Windows.Thickness)GetValue(InnerBorderThicknessProperty); }
      set { SetValue(InnerBorderThicknessProperty, value); }
    }
    public System.Windows.Media.Brush InnerBorderBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(InnerBorderBrushProperty); }
      set { SetValue(InnerBorderBrushProperty, value); }
    }
    public System.Windows.Media.Brush InnerBorderPressedBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(InnerBorderPressedBrushProperty); }
      set { SetValue(InnerBorderPressedBrushProperty, value); }
    }
    public System.Windows.Media.Brush InnerBorderHoverBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(InnerBorderHoverBrushProperty); }
      set { SetValue(InnerBorderHoverBrushProperty, value); }
    }
    public System.Windows.Media.Brush InnerBorderFocusBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(InnerBorderFocusBrushProperty); }
      set { SetValue(InnerBorderFocusBrushProperty, value); }
    }
    public System.Windows.Media.Transform PressedTransform
    {
      get { return (System.Windows.Media.Transform)GetValue(PressedTransformProperty); }
      set { SetValue(PressedTransformProperty, value); }
    }
    public event System.Windows.RoutedEventHandler LeftClick
    {
      add { Click += value; }
      remove { Click -= value; }
    }
    public event System.Windows.RoutedEventHandler RightClick;
    public event System.Windows.RoutedEventHandler PressClick;
    public event System.Windows.RoutedEventHandler ReleaseClick;

    public void SetContent(FrameworkElement Element)
    {
      this.Content = Element;
    }

    protected override void OnClick()
    {
      base.OnClick();
    }

    protected override void OnMouseDown(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnMouseDown(e);

      if (PressClick != null)
        PressClick(this, e);
    }
    protected override void OnMouseUp(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnMouseDown(e);

      if (ReleaseClick != null)
        ReleaseClick(this, e);
    }
    protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
    {
      base.OnMouseMove(e);

      if (this.IsMouseCaptured)
      {
        var isInside = false;

        System.Windows.Media.VisualTreeHelper.HitTest(
            this,
            d =>
            {
              if (d == this)
                isInside = true;

              return System.Windows.Media.HitTestFilterBehavior.Stop;
            },
            ht => System.Windows.Media.HitTestResultBehavior.Stop, new System.Windows.Media.PointHitTestParameters(e.GetPosition(this)));

        this.IsPressed = isInside;
      }
    }

    void RightClickButton_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      this.IsPressed = true;
      CaptureMouse();
      this.IsClicked = true;
    }
    void RightClickButton_MouseRightButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      ReleaseMouseCapture();

      if (this.IsMouseOver && IsClicked)
      {
        if (RightClick != null)
          RightClick.Invoke(this, e);
      }

      this.IsClicked = false;
      this.IsPressed = false;
    }

    private bool IsClicked;
    private static System.Windows.Style DefaultStyle;
  }

  [System.Windows.Data.ValueConversion(typeof(System.Windows.CornerRadius), typeof(System.Windows.CornerRadius))]
  internal sealed class InnerCornerRadiusConverter : System.Windows.Data.IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var InputRadius = (System.Windows.CornerRadius)value;

      return new System.Windows.CornerRadius(Math.Max(0, InputRadius.TopLeft - 1), Math.Max(0, InputRadius.TopRight - 1), Math.Max(0, InputRadius.BottomRight - 1), Math.Max(0, InputRadius.BottomLeft - 1));
    }
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return null;
    }
  }

  internal sealed class WpfScroll : WpfClipper
  {
    internal WpfScroll(bool IsVertical, bool IsHorizontal)
    {
      this.Inner = new WpfScrollViewer();

      if (!IsVertical && IsHorizontal)
      {
        Inner.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Disabled;
        Inner.HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
        Inner.PanningMode = System.Windows.Controls.PanningMode.HorizontalOnly;
      }
      else if (IsVertical && !IsHorizontal)
      {
        Inner.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
        Inner.HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Disabled;
        Inner.PanningMode = System.Windows.Controls.PanningMode.VerticalOnly;
      }
      else
      {
        Inner.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
        Inner.HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
        Inner.PanningMode = System.Windows.Controls.PanningMode.Both;
      }

      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.FrameworkElement Content
    {
      get { return (System.Windows.FrameworkElement)Inner.Content; }
      set { Inner.Content = value; }
    }

    private WpfScrollViewer Inner;
  }

  internal sealed class WpfScrollViewer : System.Windows.Controls.ScrollViewer
  {
    public WpfScrollViewer()
    {
      this.PreviewMouseWheel += PreviewMouseWheelHandler;
      this.PanningMode = System.Windows.Controls.PanningMode.Both;
      this.ManipulationBoundaryFeedback += (Sender, Event) =>
      {
        // Prevent the truly nasty default window moving behaviour on reaching the scroll boundary.
        Event.Handled = true;
      };
      this.Loaded += (Sender, Event) =>
      {
        if (VerticalScrollBar != null && HorizontalScrollBar != null)
        {
          VerticalScrollBar.Opacity = 0.50;
          HorizontalScrollBar.Opacity = 0.50;
        }
      };

      this.CanContentScroll = false;
      this.Focusable = false;
    }

    static WpfScrollViewer()
    {
      KeyDownEventsArgsList = new HashSet<System.Windows.Input.KeyEventArgs>();
      MouseWheelEventArgsList = new HashSet<System.Windows.Input.MouseWheelEventArgs>();
    }

    public bool MousePanningEnabled { get; set; }

    internal double? ScrollBarSize
    {
      get { return ScrollBarSizeField; }
      set
      {
        if (ScrollBarSizeField != value)
        {
          ScrollBarSizeField = value;

          UpdateScrollBarSize();
        }
      }
    }
    internal System.Windows.Controls.Primitives.ScrollBar VerticalScrollBar { get; private set; }
    internal System.Windows.Controls.Primitives.ScrollBar HorizontalScrollBar { get; private set; }

    public override void OnApplyTemplate()
    {
      base.OnApplyTemplate();

      if (Template != null)
      {
        VerticalScrollBar = Template.FindName("PART_VerticalScrollBar", this) as System.Windows.Controls.Primitives.ScrollBar;
        HorizontalScrollBar = Template.FindName("PART_VerticalScrollBar", this) as System.Windows.Controls.Primitives.ScrollBar;

        UpdateScrollBarSize();
      }
    }

    protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
    {
      base.OnMouseMove(e);

      if (MousePanningEnabled && IsMouseCaptured)
      {
        var CurrentPosition = e.GetPosition(this);

        var Delta = new System.Windows.Point();
        Delta.X = (CurrentPosition.X > PanStartPosition.X) ? -(CurrentPosition.X - PanStartPosition.X) : (PanStartPosition.X - CurrentPosition.X);
        Delta.Y = (CurrentPosition.Y > PanStartPosition.Y) ? -(CurrentPosition.Y - PanStartPosition.Y) : (PanStartPosition.Y - CurrentPosition.Y);

        ScrollToHorizontalOffset(PanStartOffset.X + Delta.X);
        ScrollToVerticalOffset(PanStartOffset.Y + Delta.Y);
      }
    }
    protected override void OnPreviewMouseDown(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnPreviewMouseDown(e);

      if (MousePanningEnabled && IsMouseOver)
      {
        PanStartPosition = e.GetPosition(this);
        PanStartOffset.X = HorizontalOffset;
        PanStartOffset.Y = VerticalOffset;

        Cursor = System.Windows.Input.Cursors.ScrollAll;
        CaptureMouse();
      }
    }
    protected override void OnPreviewMouseUp(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnPreviewMouseUp(e);

      if (MousePanningEnabled && IsMouseCaptured)
      {
        Cursor = System.Windows.Input.Cursors.Arrow;
        ReleaseMouseCapture();
      }
    }

    private void UpdateScrollBarSize()
    {
      if (ScrollBarSizeField.HasValue)
      {
        var Value = ScrollBarSizeField.Value;

        if (VerticalScrollBar != null)
          VerticalScrollBar.Width = Value;

        if (HorizontalScrollBar != null)
          HorizontalScrollBar.Height = Value;
      }
    }

    private System.Windows.Point PanStartPosition;
    private System.Windows.Point PanStartOffset;
    private double? ScrollBarSizeField;

    private static void PreviewMouseWheelHandler(object Sender, System.Windows.Input.MouseWheelEventArgs e)
    {
      var Control = Sender as System.Windows.Controls.ScrollViewer;

      Debug.Assert(Control != null, "Sender is not assigned or is not a ScrollViewer");

      if (!e.Handled && !MouseWheelEventArgsList.Contains(e))
      {
        var PreviewEventArg = new System.Windows.Input.MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta)
        {
          RoutedEvent = System.Windows.UIElement.PreviewMouseWheelEvent,
          Source = Sender
        };

        var OriginalSource = e.OriginalSource as System.Windows.UIElement;

        if (OriginalSource != null)
        {
          MouseWheelEventArgsList.Add(PreviewEventArg);
          OriginalSource.RaiseEvent(PreviewEventArg);
          MouseWheelEventArgsList.Remove(PreviewEventArg);
        }

        e.Handled = PreviewEventArg.Handled;

        if (!e.Handled)
        {
          var Delta = e.Delta;

          if ((Delta > 0 && Control.VerticalOffset == 0) || (Delta <= 0 && Control.VerticalOffset >= Control.ExtentHeight - Control.ViewportHeight))
          {
            var LogicalParent = (System.Windows.UIElement)((System.Windows.FrameworkElement)Sender).Parent;
            if (LogicalParent != null)
            {
              var EventArgs = new System.Windows.Input.MouseWheelEventArgs(e.MouseDevice, e.Timestamp, Delta)
              {
                RoutedEvent = System.Windows.UIElement.MouseWheelEvent,
                Source = Sender
              };

              LogicalParent.RaiseEvent(EventArgs);

              e.Handled = EventArgs.Handled;
            }
          }
        }
      }
    }

    private static readonly HashSet<System.Windows.Input.KeyEventArgs> KeyDownEventsArgsList;
    private static readonly HashSet<System.Windows.Input.MouseWheelEventArgs> MouseWheelEventArgsList;
  }

  internal sealed class WpfCanvas : WpfClipper, Inv.DrawContract
  {
    internal WpfCanvas(WpfHost WpfHost)
    {
      this.WpfHost = WpfHost;

      this.ClipToBounds = true; // prevents drawing outside of the control.

      this.CacheMode = new System.Windows.Media.BitmapCache();

      this.WpfDrawingVisual = new System.Windows.Media.DrawingVisual();
      AddVisualChild(WpfDrawingVisual);

      this.WpfRectangleRect = new System.Windows.Rect();
      this.WpfImageRect = new System.Windows.Rect();
    }

    public static readonly System.Windows.RoutedEvent MouseDoubleClickEvent = System.Windows.Controls.Control.MouseDoubleClickEvent.AddOwner(typeof(WpfCanvas));

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public event System.Windows.Input.MouseButtonEventHandler MouseDoubleClick
    {
      add { base.AddHandler(WpfCanvas.MouseDoubleClickEvent, value); }
      remove { base.RemoveHandler(WpfCanvas.MouseDoubleClickEvent, value); }
    }

    public void StartDrawing()
    {
      this.WpfDrawingContext = WpfDrawingVisual.RenderOpen();
    }
    public void StopDrawing()
    {
      WpfDrawingContext.Close();
    }

    protected override void OnMouseDown(System.Windows.Input.MouseButtonEventArgs e)
    {
      if (e.ClickCount == 2)
      {
        RaiseEvent(new System.Windows.Input.MouseButtonEventArgs(System.Windows.Input.Mouse.PrimaryDevice, 0, e.ChangedButton)
        {
          RoutedEvent = WpfCanvas.MouseDoubleClickEvent,
          Source = this,
        });

        e.Handled = true;
      }
      else
      {
        base.OnMouseDown(e);
      }
    }
    protected override int VisualChildrenCount
    {
      get { return 1; }
    }
    protected override System.Windows.Media.Visual GetVisualChild(int index)
    {
      return WpfDrawingVisual;
    }

    int DrawContract.Width
    {
      get { return (int)ActualWidth; }
    }
    int DrawContract.Height
    {
      get { return (int)ActualHeight; }
    }
    void DrawContract.DrawText(string TextFragment, string TextFontName, int TextFontSize, FontWeight TextFontWeight, Colour TextFontColour, Point TextPoint, HorizontalPosition TextHorizontal, VerticalPosition TextVertical)
    {
      var WpfFormattedText = new System.Windows.Media.FormattedText
      (
        TextFragment,
        CultureInfo.CurrentCulture,
        System.Windows.FlowDirection.LeftToRight,
        new System.Windows.Media.Typeface(new System.Windows.Media.FontFamily(TextFontName.EmptyAsNull() ?? WpfShell.DefaultFontName), System.Windows.FontStyles.Normal, WpfHost.TranslateFontWeight(TextFontWeight), System.Windows.FontStretches.Normal),
        TextFontSize,
        WpfHost.TranslateBrush(TextFontColour)
      );

      var WpfTextPoint = WpfHost.TranslatePoint(TextPoint);

      if (TextHorizontal != HorizontalPosition.Left)
      {
        var TextWidth = (int)WpfFormattedText.Width;

        if (TextHorizontal == HorizontalPosition.Right)
          WpfTextPoint.X -= TextWidth;
        else if (TextHorizontal == HorizontalPosition.Center)
          WpfTextPoint.X -= TextWidth / 2;
      }

      if (TextVertical != VerticalPosition.Top)
      {
        var TextHeight = (int)WpfFormattedText.Height;

        if (TextVertical == VerticalPosition.Bottom)
          WpfTextPoint.Y -= TextHeight;
        else if (TextVertical == VerticalPosition.Center)
          WpfTextPoint.Y -= TextHeight / 2;
      }

      WpfDrawingContext.DrawText(WpfFormattedText, WpfTextPoint);
    }
    void DrawContract.DrawLine(Inv.Colour LineStrokeColour, int LineStrokeThickness, Inv.Point LineSourcePoint, Inv.Point LineTargetPoint, params Inv.Point[] LineExtraPointArray)
    {
      if (LineStrokeThickness <= 0 || LineStrokeColour == Inv.Colour.Transparent)
        return;

      var PointPen = WpfHost.TranslatePen(LineStrokeColour, LineStrokeThickness);

      var SourcePoint = WpfHost.TranslatePoint(LineSourcePoint);
      var CurrentPoint = WpfHost.TranslatePoint(LineTargetPoint);
      WpfDrawingContext.DrawLine(PointPen, SourcePoint, CurrentPoint);

      for (var Index = 0; Index < LineExtraPointArray.Length; Index++)
      {
        var TargetPoint = WpfHost.TranslatePoint(LineExtraPointArray[Index]);
        WpfDrawingContext.DrawLine(PointPen, CurrentPoint, TargetPoint);

        CurrentPoint = TargetPoint;
      }
    }
    void DrawContract.DrawRectangle(Colour RectangleFillColour, Colour RectangleStrokeColour, int RectangleStrokeThickness, Rect RectangleRect)
    {
      WpfHost.TranslateRect(RectangleRect, ref WpfRectangleRect);

      if (RectangleStrokeThickness > 0)
      {
        var StrokeWidth = RectangleStrokeThickness / 2.0;
        WpfRectangleRect.X += StrokeWidth;
        WpfRectangleRect.Y += StrokeWidth;

        if (WpfRectangleRect.Width >= RectangleStrokeThickness)
          WpfRectangleRect.Width -= RectangleStrokeThickness;

        if (WpfRectangleRect.Height >= RectangleStrokeThickness)
          WpfRectangleRect.Height -= RectangleStrokeThickness;
      }
      /*
      if (StrokeWidth > 0)
      {
        var Guidelines = new System.Windows.Media.GuidelineSet();
        Guidelines.GuidelinesX.Add(WpfRectangleRect.Left + StrokeWidth);
        Guidelines.GuidelinesX.Add(WpfRectangleRect.Right + StrokeWidth);
        Guidelines.GuidelinesY.Add(WpfRectangleRect.Top + StrokeWidth);
        Guidelines.GuidelinesY.Add(WpfRectangleRect.Bottom + StrokeWidth);
        WpfContext.PushGuidelineSet(Guidelines);
      }*/

      WpfDrawingContext.DrawRectangle(
        WpfHost.TranslateBrush(RectangleFillColour),
        WpfHost.TranslatePen(RectangleStrokeColour, RectangleStrokeThickness),
        WpfRectangleRect);

      //if (StrokeWidth > 0)
      //  WpfContext.Pop();
    }
    void DrawContract.DrawEllipse(Colour EllipseFillColour, Colour EllipseStrokeColour, int EllipseStrokeThickness, Point EllipseCenter, Point EllipseRadius)
    {
      WpfDrawingContext.DrawEllipse(
        WpfHost.TranslateBrush(EllipseFillColour),
        WpfHost.TranslatePen(EllipseStrokeColour,
        EllipseStrokeThickness),
        WpfHost.TranslatePoint(EllipseCenter), EllipseRadius.X, EllipseRadius.Y);
    }
    void DrawContract.DrawArc(Inv.Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Inv.Point ArcCenter, Inv.Point ArcRadius, float StartAngle, float SweepAngle)
    {
      var MaxWidth = Math.Max(0.0, (ArcRadius.X * 2) - ArcStrokeThickness);
      var MaxHeight = Math.Max(0.0, (ArcRadius.Y * 2) - ArcStrokeThickness);

      var StartX = ArcRadius.X * Math.Cos(StartAngle * Math.PI / 180.0);
      var StartY = ArcRadius.Y * Math.Sin(StartAngle * Math.PI / 180.0);

      var EndX = ArcRadius.X * Math.Cos(SweepAngle * Math.PI / 180.0);
      var EndY = ArcRadius.Y * Math.Sin(SweepAngle * Math.PI / 180.0);

      var WpfGeometry = new PathGeometry();

      var WpfFigure = new PathFigure 
      { 
        StartPoint = new System.Windows.Point(ArcCenter.X + StartX, ArcCenter.Y - StartY),
        IsFilled = true,
        IsClosed = true
      };
      WpfFigure.Segments.Add(new ArcSegment
      {
        Point = new System.Windows.Point(ArcCenter.X + EndX, ArcCenter.Y - EndY),
        Size = new System.Windows.Size(MaxWidth / 2.0, MaxHeight / 2),
        RotationAngle = 0.0,
        IsLargeArc = (SweepAngle - StartAngle) > 180,
        SweepDirection = System.Windows.Media.SweepDirection.Counterclockwise,
        IsStroked = true,
        IsSmoothJoin = false
      });
      WpfFigure.Segments.Add(new LineSegment
      {
        Point = new System.Windows.Point(ArcCenter.X, ArcCenter.Y),
        IsStroked = true,
        IsSmoothJoin = false
      });
      WpfFigure.Freeze();

      WpfGeometry.Figures.Add(WpfFigure);
      WpfGeometry.Freeze();

      WpfDrawingContext.DrawGeometry(WpfHost.TranslateBrush(ArcFillColour), WpfHost.TranslatePen(ArcStrokeColour, ArcStrokeThickness), WpfGeometry);
    }
    void DrawContract.DrawImage(Image ImageSource, Rect ImageRect, float ImageOpacity, Colour ImageTintColour, Mirror? ImageMirror)
    {
      WpfHost.TranslateRect(ImageRect, ref WpfImageRect);

      if (ImageOpacity != 1.0F)
      {
        // NOTE: WpfContext.PushOpacity performs terribly if you push/pop in quick succession (this is a workaround).
        var WpfTintBrush = WpfHost.TranslateBrush(Inv.Colour.White.Opacity(ImageOpacity));
        WpfDrawingContext.PushOpacityMask(WpfTintBrush);
      }

      if (ImageMirror != null)
      {
        WpfDrawingContext.PushTransform(WpfHost.MirrorTransformArray[ImageMirror.Value]);

        if (ImageMirror.Value == Mirror.Horizontal)
          WpfImageRect.X = -WpfImageRect.X - WpfImageRect.Width;

        if (ImageMirror.Value == Mirror.Vertical)
          WpfImageRect.Y = -WpfImageRect.Y - WpfImageRect.Height;
      }

      var WpfImage = WpfHost.TranslateImage(ImageSource);

      WpfDrawingContext.DrawImage(WpfImage, WpfImageRect);

      if (ImageTintColour != null)
      {
        var WpfTintBrush = WpfHost.TranslateBrush(ImageTintColour);

        var WpfImageBrush = new System.Windows.Media.ImageBrush(WpfImage);
        WpfImageBrush.Freeze();

        WpfDrawingContext.PushOpacityMask(WpfImageBrush);
        WpfDrawingContext.DrawRectangle(WpfTintBrush, null, WpfImageRect);
        WpfDrawingContext.Pop();
      }

      if (ImageMirror != null)
        WpfDrawingContext.Pop();

      if (ImageOpacity != 1.0F)
        WpfDrawingContext.Pop();
    }
    void DrawContract.DrawPolygon(Colour FillColour, Colour StrokeColour, int StrokeThickness, LineJoin LineJoin, Point StartPoint, params Point[] PointArray)
    {
      var PathGeometry = new PathGeometry();
      var PathFigure = new PathFigure()
      {
        StartPoint = WpfHost.TranslatePoint(StartPoint),
        IsClosed = true
      };
      PathFigure.Segments.Add(new PolyLineSegment(PointArray.Select(P => WpfHost.TranslatePoint(P)).ToList(), true));
      PathFigure.Freeze();
      PathGeometry.Figures.Add(PathFigure);
      PathGeometry.Freeze();

      PenLineJoin PenLineJoin;
      switch (LineJoin)
      {
        case Inv.LineJoin.Miter:
          PenLineJoin = PenLineJoin.Miter;
          break;

        case Inv.LineJoin.Bevel:
          PenLineJoin = PenLineJoin.Bevel;
          break;

        case Inv.LineJoin.Round:
          PenLineJoin = PenLineJoin.Round;
          break;

        default:
          throw new NotSupportedException("Line join type not handled: " + LineJoin.ToString());
      }

      var Pen = WpfHost.TranslatePen(StrokeColour, StrokeThickness, PenLineJoin);

      WpfDrawingContext.DrawGeometry(WpfHost.TranslateBrush(FillColour), Pen, PathGeometry);
    }

    private WpfHost WpfHost;
    private System.Windows.Media.DrawingVisual WpfDrawingVisual;
    private System.Windows.Media.DrawingContext WpfDrawingContext;
    private System.Windows.Rect WpfRectangleRect;
    private System.Windows.Rect WpfImageRect;
  }

  internal sealed class WpfWindow : System.Windows.Window
  {
  }

  internal abstract class WpfClipper : System.Windows.Controls.Border
  {
    protected override void OnRender(DrawingContext dc)
    {
      OnApplyChildClip();
      base.OnRender(dc);
    }

    public override System.Windows.UIElement Child
    {
      get { return base.Child; }
      set
      {
        if (this.Child != value)
        {
          if (this.Child != null)
            this.Child.SetValue(System.Windows.UIElement.ClipProperty, OldClip); // Restore original clipping

          if (value != null)
            OldClip = value.ReadLocalValue(System.Windows.UIElement.ClipProperty);
          else
            OldClip = null; // If we dont set it to null we could leak a Geometry object

          base.Child = value;
        }
      }
    }

    protected virtual void OnApplyChildClip()
    {
      var ApplyChild = this.Child;

      if (ApplyChild != null)
      {
        if (this.CornerRadius.TopLeft != 0 || this.CornerRadius.TopRight != 0 || this.CornerRadius.BottomLeft != 0 || this.CornerRadius.BottomRight != 0)
        {
          /*
          if (ClipPath == null)
            this.ClipPath = new PathGeometry();
          else
            ClipPath.Clear();

          var WpfFigure = new PathFigure
          {
            StartPoint = new System.Windows.Point(0, 0),
            IsFilled = true,
            IsClosed = true
          };
          WpfFigure.Segments.Add(new ArcSegment
          {
            Point = new System.Windows.Point(ArcCenter.X + EndX, ArcCenter.Y - EndY),
            Size = new System.Windows.Size(MaxWidth / 2.0, MaxHeight / 2),
            RotationAngle = 0.0,
            IsLargeArc = (SweepAngle - StartAngle) > 180,
            SweepDirection = System.Windows.Media.SweepDirection.Counterclockwise,
            IsStroked = true,
            IsSmoothJoin = false
          });
          WpfFigure.Segments.Add(new LineSegment
          {
            Point = new System.Windows.Point(ArcCenter.X, ArcCenter.Y),
            IsStroked = true,
            IsSmoothJoin = false
          });
          WpfFigure.Freeze();

          ClipPath.Figures.Add(WpfFigure);
          ClipPath.Freeze();
          */
          var Radius = Math.Max(0.0, this.CornerRadius.TopLeft - (this.BorderThickness.Left * 0.5));
          
          if (ClipRect == null)
            ClipRect = new RectangleGeometry();
          
          ClipRect.RadiusX = Radius;
          ClipRect.RadiusY = Radius;
          ClipRect.Rect = new System.Windows.Rect(Child.RenderSize);

          ApplyChild.Clip = ClipRect;
        }
        else
        {
          this.ClipRect  = null;
          //this.ClipPath = null;

          ApplyChild.Clip = null;
        }
      }
    }

    private RectangleGeometry ClipRect;
    //private PathGeometry ClipPath;
    private object OldClip;
  }

  internal sealed class WpfCanvasElement : System.Windows.Controls.Border
  {
  }

  internal sealed class WpfFrame : WpfClipper
  {
    public void SetContent(FrameworkElement Element)
    {
      this.Child = Element;
    }
  }

  internal sealed class WpfBrowser : WpfClipper
  {
    public WpfBrowser()
    {
      this.Inner = new System.Windows.Controls.WebBrowser();
      base.Child = Inner;
      Inner.Margin = new System.Windows.Thickness(0);
    }
    
    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }

    public void Navigate(Uri Uri, string Html)
    {
      if (Html != null)
        Inner.NavigateToString(Html);
      else if (Uri != null)
        Inner.Navigate(Uri);
      else
        Inner.Navigate("about:blank");
    }

    private System.Windows.Controls.WebBrowser Inner;
  }

  internal sealed class WpfStack : WpfClipper
  {
    public WpfStack()
    {
      this.Inner = new System.Windows.Controls.StackPanel();
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.Controls.Orientation Orientation
    {
      get { return Inner.Orientation; }
      set { Inner.Orientation = value; }
    }

    public void Compose(IEnumerable<System.Windows.FrameworkElement> WpfElements)
    {
      // TODO: delta algorithm?

      Inner.Children.Clear();
      foreach (var WpfElement in WpfElements)
        Inner.SafeAddChild(WpfElement);
    }

    private System.Windows.Controls.StackPanel Inner;
  }

  internal sealed class WpfFlow : WpfClipper
  {
    public WpfFlow()
    {
      this.FlowItemsControl = new WpfFlowItemsControl();
      base.Child = FlowItemsControl;

      System.Windows.Controls.ScrollViewer.SetVerticalScrollBarVisibility(FlowItemsControl, System.Windows.Controls.ScrollBarVisibility.Auto);
      System.Windows.Controls.ScrollViewer.SetHorizontalScrollBarVisibility(FlowItemsControl, System.Windows.Controls.ScrollBarVisibility.Disabled);
      System.Windows.Controls.ScrollViewer.SetPanningMode(FlowItemsControl, System.Windows.Controls.PanningMode.VerticalOnly);

      this.ScrollTargetIndex = -1;

      WpfFlowVirtualizingStackPanel.AddScrollChangeHandler(FlowItemsControl, (Sender, E) =>
      {
        this.Panel = E.Panel;

        //Debug.WriteLine("Name: {0} ViewportHeight: {1}", E.Name, E.ViewportHeight);

        QueryItemRange((int)E.VerticalOffset, (int)(E.VerticalOffset + E.ViewportHeight));

        if (Panel != null && ScrollTargetIndex >= 0)
        {
          Panel.BringIndexIntoViewPublic(ScrollTargetIndex);
          this.ScrollTargetIndex = -1;
        }
      });
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public Func<int, System.Windows.FrameworkElement> HeaderContentQuery;
    public Func<int, System.Windows.FrameworkElement> FooterContentQuery;
    public Func<int> SectionCountQuery;
    public Func<int, int> ItemCountQuery;
    public Func<int, int, System.Windows.FrameworkElement> ItemContentQuery;

    public void Reload()
    {      
      var SectionCount = SectionCountQuery();

      this.PlaceholderCollection = new System.Collections.ObjectModel.ObservableCollection<System.Windows.FrameworkElement>();
            
      for (var SectionIndex = 0; SectionIndex < SectionCount; SectionIndex++)
      {
        var HeaderContent = HeaderContentQuery(SectionIndex);
        if (HeaderContent != null)
          AddPlaceholder(null);

        var ItemCount = ItemCountQuery(SectionIndex);

        for (int ItemIndex = 0; ItemIndex < ItemCount; ItemIndex++)
          AddPlaceholder(null);

        var FooterContent = FooterContentQuery(SectionIndex);
        if (FooterContent != null)
          AddPlaceholder(null);
      }

      FlowItemsControl.ItemsSource = PlaceholderCollection;

      if (/*FlowItemsControl.IsLoaded && */Panel != null)
        QueryItemRange((int)Panel.VerticalOffset, (int)(Panel.VerticalOffset + Panel.ViewportHeight));
    }
    public void ScrollTo(int Section, int Index)
    {
      var TargetIndex = GetAbsoluteIndex(Section, Index);

      if (Panel != null)
        Panel.BringIndexIntoViewPublic(TargetIndex);
      else
        this.ScrollTargetIndex = TargetIndex;
    }

    private WpfFlowItemsControl FlowItemsControl;
    private System.Collections.ObjectModel.ObservableCollection<FrameworkElement> PlaceholderCollection;
    private WpfFlowVirtualizingStackPanel Panel;
    private int ScrollTargetIndex;

    private void QueryItemRange(int FromIndex, int UntilIndex)
    {
      var SectionCount = SectionCountQuery();
      var AbsoluteIndex = 0;

      for (var SectionIndex = 0; SectionIndex < SectionCount; SectionIndex++)
      {
        var ItemCount = ItemCountQuery(SectionIndex);

        var HeaderContent = HeaderContentQuery(SectionIndex);
        if (HeaderContent != null)
        {
          if (AbsoluteIndex.Between(FromIndex, UntilIndex))
            AddOrUpdatePlaceholder(AbsoluteIndex, HeaderContent);

          AbsoluteIndex += 1;
        }

        for (int ItemIndex = 0; ItemIndex < ItemCount; ItemIndex++)
        {
          if (AbsoluteIndex.Between(FromIndex, UntilIndex))
            AddOrUpdatePlaceholder(AbsoluteIndex, ItemContentQuery(SectionIndex, ItemIndex));

          AbsoluteIndex += 1;
        }

        var FooterContent = FooterContentQuery(SectionIndex);
        if (FooterContent != null)
        {
          if (AbsoluteIndex.Between(FromIndex, UntilIndex))
            AddOrUpdatePlaceholder(AbsoluteIndex, FooterContent);

          AbsoluteIndex += 1;
        }
      }
    }
    private void AddPlaceholder(FrameworkElement Element)
    {
      PlaceholderCollection.Add(new System.Windows.Controls.ContentPresenter() { Height = Element != null ? double.NaN : 50, Content = Element });
    }
    private void AddOrUpdatePlaceholder(int AbsoluteIndex, FrameworkElement Element)
    {
      if (PlaceholderCollection.Count < AbsoluteIndex)
      {
        AddPlaceholder(Element);
      }
      else
      {
        var ExistingContentPresenter = PlaceholderCollection.ElementAt(AbsoluteIndex) as System.Windows.Controls.ContentPresenter;
        if (ExistingContentPresenter != null)
        {
          ExistingContentPresenter.Content = Element;
          ExistingContentPresenter.Height = double.NaN;
        }
      }
    }
    private int GetAbsoluteIndex(int Section, int Index)
    {
      var Position = 0;

      for (var SectionIndex = 0; SectionIndex <= Section; SectionIndex++)
      {
        if (HeaderContentQuery(SectionIndex) != null)
          Position++;

        if (SectionIndex == Section)
        {
          Position += Index;
          break;
        }

        Position += ItemCountQuery(SectionIndex);

        if (FooterContentQuery(SectionIndex) != null)
          Position++;
      }

      return Position;
    }

    internal class WpfFlowItemContainerControl : System.Windows.Controls.ContentControl
    {
      public WpfFlowItemContainerControl()
      {
      }
    }

    internal class WpfFlowItemsControl : System.Windows.Controls.ItemsControl
    {
      public WpfFlowItemsControl()
      {
        var StyleDictionary = WpfShell.LoadResourceDictionary("InvWpfVirtualisedItemsControl.xaml");

        if (StyleDictionary != null)
        {
          var DefaultStyle = (System.Windows.Style)StyleDictionary["BaseStyle"];
          DefaultStyle.Seal();

          this.Style = DefaultStyle;
        }
      }

      protected override bool IsItemItsOwnContainerOverride(object Item)
      {
        return Item is WpfFlowItemContainerControl;
      }
      protected override System.Windows.DependencyObject GetContainerForItemOverride()
      {
        return new WpfFlowItemContainerControl();
      }
    }
  }

  internal class WpfFlowOffsetChangedEventArgs : RoutedEventArgs
  {
    public WpfFlowOffsetChangedEventArgs(RoutedEvent routedEvent, object source)
    {
      this.RoutedEvent = routedEvent;
      this.Source = source;
    }

    public WpfFlowVirtualizingStackPanel Panel { get; set; }
    public double ViewportHeight { get; set; }
    public double VerticalOffset { get; set; }

    public string Name { get; set; }
  }

  internal delegate void WpfFlowOffsetChangedEventHandler(object sender, WpfFlowOffsetChangedEventArgs e);

  internal class WpfFlowVirtualizingStackPanel : System.Windows.Controls.VirtualizingStackPanel
  {
    static WpfFlowVirtualizingStackPanel()
    {
      ScrollChangeEvent = System.Windows.EventManager.RegisterRoutedEvent("OffsetChangedEvent", System.Windows.RoutingStrategy.Bubble, typeof(WpfFlowOffsetChangedEventHandler), typeof(WpfFlowVirtualizingStackPanel));
    }
    
    public static readonly System.Windows.RoutedEvent ScrollChangeEvent;

    public static void AddScrollChangeHandler(DependencyObject o, WpfFlowOffsetChangedEventHandler Event)
    {
      ((UIElement)o).AddHandler(WpfFlowVirtualizingStackPanel.ScrollChangeEvent, Event);
    }
    public static void RemoveScrollChangeHandler(DependencyObject o, WpfFlowOffsetChangedEventHandler Event)
    {
      ((UIElement)o).RemoveHandler(WpfFlowVirtualizingStackPanel.ScrollChangeEvent, Event);
    }

    protected override void OnViewportOffsetChanged(System.Windows.Vector OldViewportOffset, System.Windows.Vector NewViewportOffset)
    {
      base.OnViewportOffsetChanged(OldViewportOffset, NewViewportOffset);

      OffsetChangedInvoke("OnViewportOffsetChanged");
    }
    protected override void OnViewportSizeChanged(System.Windows.Size OldViewportSize, System.Windows.Size NewViewportSize)
    {
      base.OnViewportSizeChanged(OldViewportSize, NewViewportSize);
            
      OffsetChangedInvoke("OnViewportSizeChanged");
    }

    private void OffsetChangedInvoke(string Name)
    {
      this.RaiseEvent(new WpfFlowOffsetChangedEventArgs(WpfFlowVirtualizingStackPanel.ScrollChangeEvent, this) { Name = Name, Panel = this, ViewportHeight = this.ViewportHeight, VerticalOffset = this.VerticalOffset });
    }
  }

  internal sealed class WpfGrid : WpfClipper
  {
    public WpfGrid()
    {
      this.Inner = new System.Windows.Controls.Grid();
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.Controls.Grid Inner { get; private set; }

    public System.Windows.Controls.ColumnDefinitionCollection ColumnDefinitions
    {
      get { return Inner.ColumnDefinitions; }
    }
    public System.Windows.Controls.RowDefinitionCollection RowDefinitions
    {
      get { return Inner.RowDefinitions; }
    }
    public System.Windows.Controls.UIElementCollection Children
    {
      get { return Inner.Children; }
    }

    public void ComposeDock(bool IsHorizontal, IEnumerable<System.Windows.FrameworkElement> WpfHeaders, IEnumerable<System.Windows.FrameworkElement> WpfClients, IEnumerable<System.Windows.FrameworkElement> WpfFooters)
    {
      // TODO: cache elements.

      Children.Clear();
      RowDefinitions.Clear();
      ColumnDefinitions.Clear();

      var Position = 0;

      foreach (var WpfHeader in WpfHeaders)
      {
        Inner.SafeAddChild(WpfHeader);

        if (IsHorizontal)
        {
          ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = System.Windows.GridLength.Auto });
          System.Windows.Controls.Grid.SetColumn(WpfHeader, Position);
        }
        else
        {
          RowDefinitions.Add(new System.Windows.Controls.RowDefinition() { Height = System.Windows.GridLength.Auto });
          System.Windows.Controls.Grid.SetRow(WpfHeader, Position);
        }

        Position++;
      }

      var WpfClientArray = WpfClients.ToArray();

      if (WpfClientArray.Length > 0)
      {
        foreach (var WpfClient in WpfClientArray)
        {
          Inner.SafeAddChild(WpfClient);

          if (IsHorizontal)
          {
            ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = new System.Windows.GridLength(1.0, System.Windows.GridUnitType.Star) });
            System.Windows.Controls.Grid.SetColumn(WpfClient, Position);
          }
          else
          {
            RowDefinitions.Add(new System.Windows.Controls.RowDefinition() { Height = new System.Windows.GridLength(1.0, System.Windows.GridUnitType.Star) });
            System.Windows.Controls.Grid.SetRow(WpfClient, Position);
          }

          Position++;
        }
      }
      else
      {
        if (IsHorizontal)
          ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = new System.Windows.GridLength(1.0, System.Windows.GridUnitType.Star) });
        else
          RowDefinitions.Add(new System.Windows.Controls.RowDefinition() { Height = new System.Windows.GridLength(1.0, System.Windows.GridUnitType.Star) });

        Position++;
      }

      foreach (var WpfFooter in WpfFooters)
      {
        Inner.SafeAddChild(WpfFooter);

        if (IsHorizontal)
        {
          ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = System.Windows.GridLength.Auto });
          System.Windows.Controls.Grid.SetColumn(WpfFooter, Position);
        }
        else
        {
          RowDefinitions.Add(new System.Windows.Controls.RowDefinition() { Height = System.Windows.GridLength.Auto });
          System.Windows.Controls.Grid.SetRow(WpfFooter, Position);
        }

        Position++;
      }
    }
  }

  internal sealed class WpfBoard : WpfClipper
  {
    public WpfBoard()
    {
      this.Inner = new System.Windows.Controls.Canvas();
      base.Child = Inner;

      this.UseLayoutRounding = true;
      this.SnapsToDevicePixels = true;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }

    public IEnumerable<WpfCanvasElement> GetElements()
    {
      return Inner.Children.Cast<WpfCanvasElement>();
    }
    public void RemoveElements()
    {
      Inner.Children.Clear();
    }
    public WpfCanvasElement AddElement()
    {
      var Result = new WpfCanvasElement();

      Inner.Children.Add(Result);

      return Result;
    }

    private System.Windows.Controls.Canvas Inner;
  }

  internal sealed class WpfGraphic : WpfClipper
  {
    public WpfGraphic()
    {
      this.Inner = new System.Windows.Controls.Image()
      {
        Stretch = System.Windows.Media.Stretch.Uniform
      };
      RenderOptions.SetBitmapScalingMode(Inner, BitmapScalingMode.Fant);
      
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.Media.ImageSource Source
    {
      get { return Inner.Source; }
      set { Inner.Source = value; }
    }
    public System.Windows.Media.Stretch Stretch
    {
      get { return Inner.Stretch; }
      set { Inner.Stretch = value; }
    }

    private System.Windows.Controls.Image Inner;
  }

  internal sealed class WpfLabel : WpfClipper
  {
    public WpfLabel()
    {
      this.Inner = new System.Windows.Controls.TextBlock()
      {
        VerticalAlignment = System.Windows.VerticalAlignment.Center,
        TextTrimming = System.Windows.TextTrimming.CharacterEllipsis
      };
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.TextAlignment TextAlignment
    {
      get { return Inner.TextAlignment; }
      set { Inner.TextAlignment = value; }
    }
    public System.Windows.TextWrapping TextWrapping
    {
      get { return Inner.TextWrapping; }
      set { Inner.TextWrapping = value; }
    }
    public System.Windows.TextTrimming TextTrimming
    {
      get { return Inner.TextTrimming; }
      set { Inner.TextTrimming = value; }
    }
    public System.Windows.Media.Brush Foreground
    {
      get { return Inner.Foreground; }
      set { Inner.Foreground = value; }
    }
    public double FontSize
    {
      get { return Inner.FontSize; }
      set { Inner.FontSize = value; }
    }
    public string Text
    {
      get { return Inner.Text; }
      set { Inner.Text = value; }
    }

    public System.Windows.Controls.TextBlock GetTextBlock()
    {
      return Inner;
    }

    private System.Windows.Controls.TextBlock Inner;
  }

  internal sealed class WpfSearch : WpfClipper, WpfOverrideFocusContract
  {
    public WpfSearch()
    {
      this.LayoutDock = new System.Windows.Controls.DockPanel();
      base.Child = LayoutDock;
      LayoutDock.LastChildFill = true;

      // TODO: left search image.

      // TODO: clear button X path geometry.
      this.ClearButton = new System.Windows.Controls.Button()
      {
        BorderThickness = new System.Windows.Thickness(0),
        Padding = new System.Windows.Thickness(6)
      };
      //LayoutDock.Children.Add(ClearButton);
      System.Windows.Controls.DockPanel.SetDock(ClearButton, System.Windows.Controls.Dock.Right);
      ClearButton.IsEnabled = false;
      ClearButton.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
      ClearButton.VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
      ClearButton.Visibility = System.Windows.Visibility.Hidden;
      ClearButton.Click += (Sender, Event) =>
      {
        TextBox.Text = ""; // fires change event.
      };

      this.TextBox = new System.Windows.Controls.TextBox()
      {
        IsReadOnlyCaretVisible = true,
        AcceptsReturn = false,
        AcceptsTab = false,
        TextWrapping = System.Windows.TextWrapping.Wrap,
        Background = System.Windows.Media.Brushes.Transparent,
        BorderBrush = System.Windows.Media.Brushes.Transparent,
        BorderThickness = new System.Windows.Thickness(0),
        Padding = new System.Windows.Thickness(-2, 0, 0, 0),
      };
      TextBox.TextChanged += (Sender, Event) =>
      {
        //ClearButton.Visibility = string.IsNullOrEmpty(TextBox.Text) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;

        Change();
      };
      TextBox.KeyDown += (Sender, Event) =>
      {
        if (Event.Key == System.Windows.Input.Key.Escape)
        {
          if (!string.IsNullOrEmpty(TextBox.Text))
          {
            TextBox.Text = ""; // fires change event.

            Event.Handled = true;
          }
        }
        else if (Event.Key == System.Windows.Input.Key.Return)
        {
          Return();

          Event.Handled = true;
        }
      };
      LayoutDock.Children.Add(TextBox);
    }

    public event Action ChangeEvent;
    public event Action ReturnEvent;
    public string Text
    {
      get { return TextBox.Text; }
      set { TextBox.Text = value; }
    }
    public System.Windows.Controls.TextBox TextBox { get; private set; }

    private void Change()
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }
    private void Return()
    {
      if (ReturnEvent != null)
        ReturnEvent();
    }

    void WpfOverrideFocusContract.OverrideFocus()
    {
      TextBox.Focus();
    }

    private System.Windows.Controls.DockPanel LayoutDock;
    private System.Windows.Controls.Button ClearButton;
  }

  internal sealed class WpfEdit : WpfClipper, WpfOverrideFocusContract
  {
    public WpfEdit(bool PasswordMask)
    {
      if (PasswordMask)
      {
        this.InnerPasswordBox = new System.Windows.Controls.PasswordBox()
        {
          Background = System.Windows.Media.Brushes.Transparent,
          BorderBrush = System.Windows.Media.Brushes.Transparent,
          BorderThickness = new System.Windows.Thickness(0),
          Padding = new System.Windows.Thickness(-2, 0, 0, 0),
        };
        InnerPasswordBox.PasswordChanged += (Sender, Event) => TextChangedInvoke(Sender, Event);
        base.Child = InnerPasswordBox;
      }
      else
      {
        this.InnerTextBox = new System.Windows.Controls.TextBox()
        {
          IsReadOnlyCaretVisible = true,
          AcceptsReturn = false,
          AcceptsTab = false,
          TextWrapping = System.Windows.TextWrapping.Wrap,
          Background = System.Windows.Media.Brushes.Transparent,
          BorderBrush = System.Windows.Media.Brushes.Transparent,
          Margin = new System.Windows.Thickness(0),
          Padding = new System.Windows.Thickness(-2, 0, 0, 0),
          BorderThickness = new System.Windows.Thickness(0),
        };
        InnerTextBox.TextChanged += (Sender, Event) => TextChangedInvoke(Sender, Event);
        base.Child = InnerTextBox;
      }
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public bool IsReadOnly
    {
      get 
      {
        if (InnerTextBox != null)
          return InnerTextBox.IsReadOnly;
        else
          return !InnerPasswordBox.IsEnabled;
      }
      set 
      {
        if (InnerTextBox != null)
          InnerTextBox.IsReadOnly = value; 
        else if (InnerPasswordBox.IsEnabled != !value)
          InnerPasswordBox.IsEnabled = !value;
      }
    }
    public string Text
    {
      get
      {
        if (InnerTextBox != null)
          return InnerTextBox.Text;
        else
          return InnerPasswordBox.Password;
      }
      set
      {
        if (InnerTextBox != null)
          InnerTextBox.Text = value;
        else if (InnerPasswordBox.Password != value)
          InnerPasswordBox.Password = value;
      }
    }
    public event System.Windows.RoutedEventHandler TextChanged;
    public Brush Foreground
    {
      get
      {
        if (InnerTextBox != null)
          return InnerTextBox.Foreground;
        else
          return InnerPasswordBox.Foreground;
      }
      set
      {
        if (InnerTextBox != null)
          InnerTextBox.Foreground = value;
        else
          InnerPasswordBox.Foreground = value;
      }
    }

    public System.Windows.Controls.TextBox GetTextBox()
    {
      return InnerTextBox;
    }
    public System.Windows.Controls.PasswordBox GetPasswordBox()
    {
      return InnerPasswordBox;
    }

    private void TextChangedInvoke(object Sender, System.Windows.RoutedEventArgs EventArgs)
    {
      if (TextChanged != null)
        TextChanged(Sender, EventArgs);
    }

    void WpfOverrideFocusContract.OverrideFocus()
    {
      if (InnerTextBox != null)
        InnerTextBox.Focus();
      else
        InnerPasswordBox.Focus();
    }

    private System.Windows.Controls.TextBox InnerTextBox;
    private System.Windows.Controls.PasswordBox InnerPasswordBox;
  }

  internal sealed class WpfMemo : WpfClipper, WpfOverrideFocusContract
  {
    public WpfMemo()
    {
      this.Inner = new System.Windows.Controls.TextBox()
      {
        AcceptsReturn = true,
        AcceptsTab = true,
        TextWrapping = System.Windows.TextWrapping.Wrap,
        IsReadOnlyCaretVisible = true,
        VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto,
        HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto,
        Background = System.Windows.Media.Brushes.Transparent,
        BorderBrush = System.Windows.Media.Brushes.Transparent,
        BorderThickness = new System.Windows.Thickness(0),
      };
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public bool IsReadOnly
    {
      get { return Inner.IsReadOnly; }
      set { Inner.IsReadOnly = value; }
    }
    public string Text
    {
      get { return Inner.Text; }
      set { Inner.Text = value; }
    }
    public event System.Windows.Controls.TextChangedEventHandler TextChanged
    {
      add { Inner.TextChanged += value; }
      remove { Inner.TextChanged -= value; }
    }

    public System.Windows.Controls.TextBox GetTextBox()
    {
      return Inner;
    }

    void WpfOverrideFocusContract.OverrideFocus()
    {
      Inner.Focus();
    }

    private System.Windows.Controls.TextBox Inner;
  }

  internal sealed class WpfCalendar : System.Windows.Controls.Calendar
  {
    public WpfCalendar()
    {
      this.DisplayDateStart = new System.DateTime(1800, 1, 1);
      this.DisplayDateEnd = new System.DateTime(2199, 12, 31);
      this.LayoutTransform = new ScaleTransform(1.25, 1.25);

      var StyleDictionary = WpfShell.LoadResourceDictionary("InvWpfMaterialCalendar.xaml");

      if (StyleDictionary != null)
      {
        var OverrideStyle = (System.Windows.Style)StyleDictionary["MaterialDesignCalendarPortrait"];
        OverrideStyle.Seal();

        this.Style = OverrideStyle;
      }
    }

    protected override void OnPreviewMouseUp(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnPreviewMouseUp(e);

      // NOTE: workaround for a strange focus issue, where you have to click twice to 'get out' of the calendar.
      if (System.Windows.Input.Mouse.Captured is System.Windows.Controls.Primitives.CalendarItem)
        System.Windows.Input.Mouse.Capture(null);
    }
  }

  public sealed class SystemMutex : IDisposable
  {
    public SystemMutex(string Name)
    {
      var MutexId = string.Format("Global\\{0}", Name);

      this.Handle = new Mutex(false, MutexId);
    }
    public void Dispose()
    {
      if (Handle != null)
      {
        try
        {
          Unlock();
        }
        catch (Exception Exception)
        {
          Debug.WriteLine(Exception.Message);

          if (Debugger.IsAttached)
            Debugger.Break();
        }

        Handle.Dispose();
        Handle = null;
      }
    }

    public bool Lock(TimeSpan Timeout)
    {
      if (!IsAcquired)
      {
        try
        {
          this.IsAcquired = Handle.WaitOne(Timeout);
        }
        catch (AbandonedMutexException)
        {
          this.IsAcquired = true;
        }
      }

      return IsAcquired;
    }
    public void Unlock()
    {
      if (IsAcquired)
      {
        Handle.ReleaseMutex();
        IsAcquired = false;
      }
    }

    private System.Threading.Mutex Handle;
    private bool IsAcquired;
  }

  internal static class WpfFoundation
  {
    public static void SafeAddChild(this System.Windows.Controls.Panel Parent, UIElement Child)
    {
      var Owner = VisualTreeHelper.GetParent(Child);

      if (Owner == null)
      {
        Parent.Children.Add(Child);
      }
      else if (Owner != Parent)
      {
        var OwnerAsPanel = Owner as System.Windows.Controls.Panel;
        if (OwnerAsPanel != null)
        {
          OwnerAsPanel.Children.Remove(Child);
        }
        else
        {
          var OwnerAsDecorator = Owner as System.Windows.Controls.Decorator;
          if (OwnerAsDecorator != null)
          {
            if (OwnerAsDecorator.Child == Child)
              OwnerAsDecorator.Child = null;
          }
          else
          {
            var OwnerAsContentControl = Owner as System.Windows.Controls.ContentControl;
            if (OwnerAsContentControl != null)
            {
              if (OwnerAsContentControl.Content == Child)
                OwnerAsContentControl.Content = null;
            }
            else
            {
              var OwnerAsContentPresenter = Owner as System.Windows.Controls.ContentPresenter;
              if (OwnerAsContentPresenter != null)
              {
                if (OwnerAsContentPresenter.Content == Child)
                  OwnerAsContentPresenter.Content = null;
              }
              else
              {
                Debug.Assert(false, "Unhandled owner.");
              }
            }
          }
        }

        Parent.Children.Add(Child);
      }
    }
  }
}