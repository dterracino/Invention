rem BUILD **INVENTION EXTENSION**

"C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe" C:\Development\Forge\Inv\Inv.sln /p:Configuration=Release;Platform="Any CPU" /t:Extension\InvExtension:Clean
"C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe" C:\Development\Forge\Inv\Inv.sln /p:Configuration=Release;Platform="Any CPU" /t:Extension\InvExtension

@echo off
Set YEAR=%DATE:~10,4%
Set MONTH=%DATE:~7,2%
Set DAY=%DATE:~4,2%
Set HOUR=%TIME:~0,2%
Set HOUR=%HOUR: =0%
Set MIN=%TIME:~3,2%
Set DTS=%YEAR%.%MONTH%%DAY%.%HOUR%%MIN%
@echo on

copy C:\Development\Forge\Inv\InvExtension\bin\Release\Invention.Extension.vsix c:\Deployment\Inv\InventionExtension.%DTS%.vsix