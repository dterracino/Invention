﻿/*! 12 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Foundation;
using UIKit;
using CoreGraphics;
using Inv.Support;

namespace Inv
{
  internal interface iOSLayoutElement
  {
    CGSize CalculateSize(CGSize MaxSize);
    UIView View { get; }
    iOSLayoutBorder Border { get; }
  }

  internal interface ITouchable
  {
  }

  internal sealed class iOSLayoutBorder
  {
    public iOSLayoutBorder(UIView View)
    {
      this.View = View;
    }

    public int TopLeftCornerRadius
    {
      get { return CurrentState.TopLeftCornerRadius; }
      set { CurrentState.TopLeftCornerRadius = value; }
    }
    public int TopRightCornerRadius
    {
      get { return CurrentState.TopRightCornerRadius; }
      set { CurrentState.TopRightCornerRadius = value; }
    }
    public int BottomRightCornerRadius
    {
      get { return CurrentState.BottomRightCornerRadius; }
      set { CurrentState.BottomRightCornerRadius = value; }
    }
    public int BottomLeftCornerRadius
    {
      get { return CurrentState.BottomLeftCornerRadius; }
      set { CurrentState.BottomLeftCornerRadius = value; }
    }

    public int LeftBorderThickness
    {
      get { return CurrentState.LeftBorderThickness; }
      set { CurrentState.LeftBorderThickness = value; }
    }
    public int TopBorderThickness
    {
      get { return CurrentState.TopBorderThickness; }
      set { CurrentState.TopBorderThickness = value; }
    }
    public int RightBorderThickness
    {
      get { return CurrentState.RightBorderThickness; }
      set { CurrentState.RightBorderThickness = value; }
    }
    public int BottomBorderThickness
    {
      get { return CurrentState.BottomBorderThickness; }
      set { CurrentState.BottomBorderThickness = value; }
    }

    public CGColor BorderColour
    {
      get { return CurrentState.BorderColour; }
      set { CurrentState.BorderColour = value; }
    }

    public void Layout()
    {
      CurrentState.Width = View.Frame.Width;
      CurrentState.Height = View.Frame.Height;

      if ((ClipLayer == null && BorderLayer == null && (!CurrentState.IsCornerRadiusUniform || !CurrentState.IsBorderThicknessUniform)) || !CurrentState.Equals(PreviousState))
      {
        if (ClipLayer != null)
        {
          View.Layer.Mask = null;
          ClipLayer = null;
        }

        if (BorderLayer != null)
        {
          BorderLayer.RemoveFromSuperLayer();
          BorderLayer = null;
        }

        if (!CurrentState.IsCornerRadiusUniform || !CurrentState.IsBorderThicknessUniform)
        {
          var Size = new iOSLayoutBezierPathBuilder.Size((float)View.Frame.Size.Width, (float)View.Frame.Size.Height);
          var CornerRadius = new iOSLayoutBezierPathBuilder.CornerRadius(CurrentState.TopLeftCornerRadius, CurrentState.TopRightCornerRadius, CurrentState.BottomRightCornerRadius, CurrentState.BottomLeftCornerRadius);
          var BorderThickness = new iOSLayoutBezierPathBuilder.BorderThickness(CurrentState.LeftBorderThickness, CurrentState.TopBorderThickness, CurrentState.RightBorderThickness, CurrentState.BottomBorderThickness);

          var RectBuilder = new iOSLayoutBezierPathBuilder();
          RectBuilder.AddRoundedRect(Size, CornerRadius, BorderThickness);
          var RectPath = RectBuilder.Render();

          // Clip the view to the shape of the outer border
          var ViewMaskShape = new CoreAnimation.CAShapeLayer()
          {
            Path = RectPath.CGPath
          };
          View.Layer.Mask = ViewMaskShape;
          ClipLayer = ViewMaskShape;

          if (CurrentState.IsBorderThicknessSet)
          {
            var BorderBuilder = new iOSLayoutBezierPathBuilder();
            BorderBuilder.AddRoundedRectBorder(Size, CornerRadius, BorderThickness);
            var BorderPath = BorderBuilder.Render();

            var BorderShape = new CoreAnimation.CAShapeLayer();
            BorderShape.Path = BorderPath.CGPath;
            BorderShape.FillColor = BorderColour;

            View.Layer.AddSublayer(BorderShape);
            BorderLayer = BorderShape;
          }
        }

        PreviousState = CurrentState;
      }
    }

    private UIView View;
    private CoreAnimation.CALayer BorderLayer;
    private CoreAnimation.CALayer ClipLayer;
    private State CurrentState;
    private State PreviousState;

    private struct State : IEquatable<State>
    {
      public int TopLeftCornerRadius { get; set; }
      public int TopRightCornerRadius { get; set; }
      public int BottomRightCornerRadius { get; set; }
      public int BottomLeftCornerRadius { get; set; }

      public int LeftBorderThickness { get; set; }
      public int TopBorderThickness { get; set; }
      public int RightBorderThickness { get; set; }
      public int BottomBorderThickness { get; set; }

      public CGColor BorderColour { get; set; }

      public nfloat Width { get; set; }
      public nfloat Height { get; set; }

      public bool IsCornerRadiusSet
      {
        get { return TopLeftCornerRadius != 0 || TopRightCornerRadius != 0 || BottomRightCornerRadius != 0 || BottomLeftCornerRadius != 0; }
      }
      public bool IsBorderThicknessSet
      {
        get { return LeftBorderThickness != 0 || TopBorderThickness != 0 || RightBorderThickness != 0 || BottomBorderThickness != 0; }
      }
      public bool IsCornerRadiusUniform
      {
        get { return TopLeftCornerRadius == TopRightCornerRadius && TopRightCornerRadius == BottomRightCornerRadius && BottomRightCornerRadius == BottomLeftCornerRadius; }
      }
      public bool IsBorderThicknessUniform
      {
        get { return LeftBorderThickness == TopBorderThickness && TopBorderThickness == RightBorderThickness && RightBorderThickness == BottomBorderThickness; }
      }

      public bool Equals(State Other)
      {
        return TopLeftCornerRadius == Other.TopLeftCornerRadius && TopRightCornerRadius == Other.TopRightCornerRadius
          && BottomRightCornerRadius == Other.BottomRightCornerRadius && BottomLeftCornerRadius == Other.BottomLeftCornerRadius
          && LeftBorderThickness == Other.LeftBorderThickness && TopBorderThickness == Other.TopBorderThickness
          && RightBorderThickness == Other.RightBorderThickness && BottomBorderThickness == Other.BottomBorderThickness
          && BorderColour == Other.BorderColour
          && Width == Other.Width && Height == Other.Height;
      }
      public override bool Equals(object Other)
      {
        if (Other is State)
          return Equals((State)Other);

        return false;
      }
      public override int GetHashCode()
      {
        return Tuple.Create(
          Tuple.Create(TopLeftCornerRadius, TopRightCornerRadius, BottomRightCornerRadius, BottomLeftCornerRadius),
          Tuple.Create(LeftBorderThickness, TopBorderThickness, RightBorderThickness, BottomBorderThickness),
          BorderColour,
          Width,
          Height).GetHashCode();
      }
    }
  }

  internal sealed class iOSLayoutBezierPathBuilder : Inv.BezierPathBuilder<UIBezierPath>
  {
    public iOSLayoutBezierPathBuilder()
    {
      this.Path = new UIBezierPath();
    }

    public override void MoveTo(Point Point)
    {
      Path.MoveTo(new CGPoint(Point.X, Point.Y));
    }
    public override void AddLineTo(Point Point)
    {
      Path.AddLineTo(new CGPoint(Point.X, Point.Y));
    }
    public override void AddCurveTo(Point EndPoint, Point ControlPoint1, Point ControlPoint2)
    {
      Path.AddCurveToPoint(new CGPoint(EndPoint.X, EndPoint.Y), new CGPoint(ControlPoint1.X, ControlPoint1.Y), new CGPoint(ControlPoint2.X, ControlPoint2.Y));
    }
    public override void Close()
    {
      Path.ClosePath();
    }
    public override UIBezierPath Render()
    {
      return Path;
    }

    private UIBezierPath Path;
  }

  internal abstract class iOSLayoutView : UIView
  {
    public iOSLayoutView()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
    }

    /*public override void Dispose()
    {
      // Brute force, remove everything
      foreach (var view in Subviews)
        view.RemoveFromSuperview();
      base.Dispose();
    }*/

    public override UIView HitTest(CGPoint Point, UIEvent Event)
    {
      var Result = base.HitTest(Point, Event);

      if (Result == this && (BackgroundColor ?? UIColor.Clear) == UIColor.Clear)
        return this.HitTestFindNothing(Result);
      else
        return this.HitTestFindTouchable(Result);
    }
  }

  internal sealed class iOSLayoutBrowser : WebKit.WKWebView, iOSLayoutElement
  {
    public iOSLayoutBrowser()
      : base(CGRect.Empty, new WebKit.WKWebViewConfiguration())
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.Border = new iOSLayoutBorder(this);

      // TODO: how to set the browser to have a transparent background?
      //this.BackgroundColor = UIColor.Clear;
      //this.ScrollView.BackgroundColor = UIColor.Clear;
    }

    public iOSLayoutBorder Border { get; private set; }

    public void Navigate(Uri Uri, string Html)
    {
      if (Html != null)
        LoadData(NSData.FromString(Html, NSStringEncoding.UTF8), "text/html", "UTF-8", new Foundation.NSUrl(""));
      else if (Uri != null)
        LoadRequest(new Foundation.NSUrlRequest(new Foundation.NSUrl(Uri.AbsoluteUri)));
      else
        LoadRequest(new Foundation.NSUrlRequest(new Foundation.NSUrl("about:blank")));
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      Border.Layout();
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }
  }

  internal sealed class iOSLayoutFlow : UITableView, iOSLayoutElement
  {
    internal iOSLayoutFlow(Configuration Configuration)
      : base(CGRect.Empty, UITableViewStyle.Grouped)
    {
      this.BackgroundColor = UIKit.UIColor.Clear;
      this.ConfigurationField = Configuration;
      this.KnownHeightDictionary = new Dictionary<NSIndexPath, nfloat>();
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.AllowsSelection = false;
      this.Border = new iOSLayoutBorder(this);
      this.DataSource = new iOSLayoutFlowDataSource(this);
      this.Delegate = new iOSLayoutFlowDelegate(this);
      this.SeparatorStyle = UITableViewCellSeparatorStyle.None;
      this.EstimatedRowHeight = UITableView.AutomaticDimension;
      this.EstimatedSectionHeaderHeight = UITableView.AutomaticDimension;
      this.EstimatedSectionFooterHeight = UITableView.AutomaticDimension;

      this.TableHeaderView = new UIView(new CGRect(0, 0, 0, 0.001));// HACK: Without a header view, there will be ~35 pixels of padding at the top of the table
      this.TableFooterView = new UIView(new CGRect(0, 0, 0, 0.001));// HACK: Without a footer view, there will be ~35 pixels of padding at the bottom of the table

      if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
        this.CellLayoutMarginsFollowReadableWidth = false;

      RegisterClassForCellReuse(typeof(iOSLayoutFlowTile), iOSLayoutFlowTile.CellReuseIdentifier);
    }

    public Action RefreshAction;
    public iOSLayoutBorder Border { get; private set; }
    public bool IsRefreshable
    {
      get { return RefreshControlField != null; }
      set
      {
        if (value != IsRefreshable)
        {
          var SupportsRefreshControl = UIDevice.CurrentDevice.CheckSystemVersion(10, 0);

          if (value)
          {
            this.RefreshControlField = new UIRefreshControl();
            RefreshControlField.ValueChanged += WeakHelpers.WeakEventHandlerWrapper(this, (layoutFlow, Sender, Event) => layoutFlow.RefreshAction());

            if (SupportsRefreshControl)
              RefreshControl = RefreshControlField;
            else
              AddSubview(RefreshControlField);

            ApplyShade();
          }
          else
          {
            if (SupportsRefreshControl)
              RefreshControl = null;
            else
              RefreshControlField.RemoveFromSuperview();

            RefreshControlField = null;
          }
        }
      }
    }

    public void NewRefresh()
    {
      if (RefreshControlField != null)
      {
        if (ContentOffset.Y <= 0f)
        {
          var X = ContentOffset.X;
          var StartY = ContentOffset.Y;
          var Y = -RefreshControlField.Frame.Size.Height;

          // HACK: This somehow fixes an issue with the refresh control not applying its TintColor on the first call of BeginRefreshing()
          ContentOffset = new CGPoint(X, Y);
          ContentOffset = new CGPoint(X, StartY);

          // Force the refresh control to be visible
          UIView.Animate(0.25, () => ContentOffset = new CGPoint(X, Y));
        }

        RefreshControlField.BeginRefreshing();
        RefreshAction();
      }
    }
    public void CompleteRefresh()
    {
      if (RefreshControlField != null)
        RefreshControlField.EndRefreshing();
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      Border.Layout();
    }
    public override void MovedToSuperview()
    {
      base.MovedToSuperview();

      ApplyShade();
    }

    internal Dictionary<NSIndexPath, nfloat> KnownHeightDictionary { get; private set; }

    internal void SetKnownHeight(NSIndexPath IndexPath, nfloat Height)
    {
      KnownHeightDictionary[IndexPath] = Height;
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = SizeThatFits(this.FitToSize(MaxSize));

      if (MaxSize.Height == UIView.NoIntrinsicMetric)
        Result.Height = Window.Bounds.Height;
      else
        Result.Height = MaxSize.Height;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      //Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private void ApplyShade()
    {
      var OptimalShade = this.GetPreferredContentLightness();

      if (OptimalShade == 1f)
      {
        this.IndicatorStyle = UIScrollViewIndicatorStyle.White;
        if (RefreshControlField != null)
          RefreshControlField.TintColor = UIColor.White;
      }
      else
      {
        if (OptimalShade == 0f)
          this.IndicatorStyle = UIScrollViewIndicatorStyle.Black;
        else
          this.IndicatorStyle = UIScrollViewIndicatorStyle.Default;

        if (RefreshControlField != null)
          RefreshControlField.TintColor = null;
      }
    }

    private UIRefreshControl RefreshControlField;
    private Configuration ConfigurationField;

    internal sealed class Configuration
    {
      public Func<NSIndexPath, iOSLayoutElement> GetCellContentQuery;
      public Func<int> NumberOfSectionsQuery;
      public Func<int, int> RowsInSectionQuery;
      public Func<int, iOSLayoutElement> GetViewForHeaderQuery;
      public Func<int, iOSLayoutElement> GetViewForFooterQuery;
    }

    private sealed class iOSLayoutFlowDataSource : UITableViewDataSource
    {
      public iOSLayoutFlowDataSource(iOSLayoutFlow Flow)
      {
        this.Flow = Flow;
      }

      public override UITableViewCell GetCell(UITableView TableView, NSIndexPath IndexPath)
      {
        // TODO: the DequeueReusableCell techniques leads to missing cells when scrolling rapidly.
        //var Tile = TableView.DequeueReusableCell(iOSLayoutFlowTile.CellReuseIdentifier, IndexPath) as iOSLayoutFlowTile;
        var Tile = new iOSLayoutFlowTile();

        var Element = Flow.ConfigurationField.GetCellContentQuery(IndexPath);
        Tile.SetContentElement(Element);

        var FittingSize = Element.CalculateSize(new CGSize(TableView.Frame.Width, UILayoutFittingExpandedSize.Height));
        Flow.SetKnownHeight(IndexPath, FittingSize.Height);

        return Tile;
      }
      public override nint NumberOfSections(UITableView TableView)
      {
        return Flow.ConfigurationField.NumberOfSectionsQuery();
      }
      public override nint RowsInSection(UITableView TableView, nint Section)
      {
        return Flow.ConfigurationField.RowsInSectionQuery((int)Section);
      }

      private iOSLayoutFlow Flow;
    }

    private sealed class iOSLayoutFlowDelegate : UITableViewDelegate
    {
      public iOSLayoutFlowDelegate(iOSLayoutFlow Flow)
      {
        this.Flow = Flow;
      }

      public override nfloat EstimatedHeight(UITableView TableView, NSIndexPath IndexPath)
      {
        if (Flow.KnownHeightDictionary.ContainsKey(IndexPath))
          return Flow.KnownHeightDictionary[IndexPath];

        return UITableView.AutomaticDimension;
      }
      public override nfloat GetHeightForRow(UITableView TableView, NSIndexPath IndexPath)
      {
        if (Flow.KnownHeightDictionary.ContainsKey(IndexPath))
          return Flow.KnownHeightDictionary[IndexPath];

        var Element = Flow.ConfigurationField.GetCellContentQuery(IndexPath);
        var FittingSize = Element.CalculateSize(new CGSize(TableView.Frame.Width, UILayoutFittingExpandedSize.Height));

        return FittingSize.Height;
      }
      public override UIView GetViewForHeader(UITableView TableView, nint Section)
      {
        var Element = Flow.ConfigurationField.GetViewForHeaderQuery((int)Section);
        return Element == null ? null : Element.View;
      }
      public override nfloat GetHeightForHeader(UITableView TableView, nint Section)
      {
        var Element = Flow.ConfigurationField.GetViewForHeaderQuery((int)Section);
        if (Element == null)
          return 0.001f; // HACK: to prevent leading header gaps.

        var FittingSize = Element.CalculateSize(new CGSize(TableView.Frame.Width, UILayoutFittingExpandedSize.Height));
        return FittingSize.Height;
      }
      public override UIView GetViewForFooter(UITableView TableView, nint Section)
      {
        var Element = Flow.ConfigurationField.GetViewForFooterQuery((int)Section);
        return Element == null ? null : Element.View;
      }
      public override nfloat GetHeightForFooter(UITableView TableView, nint Section)
      {
        var Element = Flow.ConfigurationField.GetViewForFooterQuery((int)Section);
        if (Element == null)
          return 0.001f; // HACK: to prevent trailing footer gaps.

        var FittingSize = Element.CalculateSize(new CGSize(TableView.Frame.Width, UILayoutFittingExpandedSize.Height));
        return FittingSize.Height;
      }

      private iOSLayoutFlow Flow;
    }
  }

  internal sealed class iOSLayoutFlowTile : UITableViewCell
  {
    public iOSLayoutFlowTile()
    {
      Prepare();
    }
    public iOSLayoutFlowTile(IntPtr Handle)
      : base(Handle)
    {
      Prepare();
    }

    public void SetContentElement(iOSLayoutElement Content)
    {
      if (Content != this.Content)
      {
        if (this.Content != null)
          ContentView.SafeRemoveView(this.Content.View);

        this.Content = Content;

        if (this.Content != null)
          ContentView.SafeAddView(this.Content.View);

        this.Arrange();
      }
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      if (Content != null)
        Content.View.Frame = Bounds;
    }

    internal iOSLayoutElement Content { get; private set; }

    private void Prepare()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.BackgroundColor = UIColor.Clear;
    }

    public const string CellReuseIdentifier = "tileGridCell";
  }

  internal sealed class iOSLayoutButton : UIButton, iOSLayoutElement, ITouchable
  {
    public iOSLayoutButton()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.Border = new iOSLayoutBorder(this);
      this.ExclusiveTouch = true;
    }

    public iOSLayoutBorder Border { get; private set; }
    public event Action PreviewTouchesBeganEvent;
    public event Action PreviewTouchesEndedEvent;
    public event Action PreviewTouchesCancelledEvent;

    public void SetContentElement(iOSLayoutElement Element)
    {
      if (Content != Element)
      {
        if (Content != null)
          this.SafeRemoveView(Content.View);

        this.Content = Element;

        if (Content != null)
          this.SafeAddView(Content.View);

        this.Arrange();
      }
    }

    public override void TouchesBegan(NSSet touches, UIEvent evt)
    {
      base.TouchesBegan(touches, evt);

      if (PreviewTouchesBeganEvent != null)
        PreviewTouchesBeganEvent();
    }
    public override void TouchesCancelled(NSSet touches, UIEvent evt)
    {
      base.TouchesCancelled(touches, evt);

      if (PreviewTouchesCancelledEvent != null)
        PreviewTouchesCancelledEvent();
    }
    public override void TouchesEnded(NSSet touches, UIEvent evt)
    {
      base.TouchesEnded(touches, evt);

      if (PreviewTouchesEndedEvent != null)
        PreviewTouchesEndedEvent();
    }
    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      if (Content != null)
        Content.View.Frame = LayoutMargins.InsetRect(Bounds);

      Border.Layout();
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = Content != null ? Content.CalculateSize(this.FitToSize(MaxSize)) : CGSize.Empty;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private iOSLayoutElement Content;
  }

  internal sealed class iOSLayoutLabel : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutLabel()
      : base()
    {
      // NOTE: using a nested label because you can't have both a CornerRadius and a Shadow (CornerRadius requires ClipsToBounds = true or the background is drawn as a rectangle; but it clips off the shadow).

      this.LabelView = new UILabel();
      AddSubview(LabelView);
      LabelView.TranslatesAutoresizingMaskIntoConstraints = false;
      LabelView.LayoutMargins = UIEdgeInsets.Zero;

      this.Border = new iOSLayoutBorder(this);
    }

    public iOSLayoutBorder Border { get; private set; }

    public string Text
    {
      get
      {
        return LabelView.Text;
      }
      set
      {
        if (LabelView.Text != value)
        {
          LabelView.Text = value;
          this.Arrange();
        }
      }
    }
    public UIFont Font
    {
      get { return LabelView.Font; }
      set { LabelView.Font = value; }
    }
    public UIColor TextColor
    {
      get { return LabelView.TextColor; }
      set { LabelView.TextColor = value; }
    }
    public UITextAlignment TextAlignment
    {
      get { return LabelView.TextAlignment; }
      set { LabelView.TextAlignment = value; }
    }
    public nint Lines
    {
      get { return LabelView.Lines; }
      set { LabelView.Lines = value; }
    }
    public UILineBreakMode LineBreakMode
    {
      get { return LabelView.LineBreakMode; }
      set { LabelView.LineBreakMode = value; }
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      LabelView.Frame = LayoutMargins.InsetRect(Bounds);

      Border.Layout();
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = LabelView.SizeThatFits(this.FitToSize(MaxSize));

      // HACK: ensure 'empty' labels have a height of one line of text.
      if (Result.Height == 0 && string.IsNullOrEmpty(LabelView.Text))
      {
        var SwapText = LabelView.Text;
        try
        {
          LabelView.Text = "Pp";
          Result.Height = LabelView.SizeThatFits(this.FitToSize(MaxSize)).Height;
        }
        finally
        {
          LabelView.Text = SwapText;
        }
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return new CGSize(Math.Ceiling(Result.Width), Math.Ceiling(Result.Height));
    }

    private UILabel LabelView;
  }

  internal sealed class iOSLayoutGraphic : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutGraphic()
      : base()
    {
      this.ImageView = new UIImageView();
      AddSubview(ImageView);
      ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
      //ImageView.ClipsToBounds = true; // TODO: clip the image to the owning border shape.

      this.Border = new iOSLayoutBorder(this);
    }

    public UIImage Image
    {
      get { return ImageView.Image; }
      set { ImageView.Image = value; }
    }
    public iOSLayoutBorder Border { get; private set; }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      ImageView.Frame = LayoutMargins.InsetRect(Bounds);

      Border.Layout();
    }

    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var MaxResult = this.FitToSize(MaxSize);
      var Result = ImageView.SizeThatFits(MaxResult);

      var Image = ImageView.Image;

      if (Image != null && Image.Size.Height > 0 && ((MaxResult.Width != -1 && MaxResult.Width < Result.Width) || (MaxResult.Height != -1 && MaxResult.Height < Result.Height)))
      {
        var ImageAspect = Image.Size.Width / Image.Size.Height;

        if (MaxResult.Width != -1 && MaxResult.Height != -1)
        {
          if (ImageAspect > MaxResult.Width / MaxResult.Height)
            Result = new CGSize(MaxResult.Width, MaxResult.Width / ImageAspect);
          else
            Result = new CGSize(MaxResult.Height * ImageAspect, MaxResult.Height);
        }
        else
        {
          if (MaxResult.Width != -1)
            Result = new CGSize(MaxResult.Width, MaxResult.Width / ImageAspect);
          else
            Result = new CGSize(MaxResult.Height * ImageAspect, MaxResult.Height);
        }
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }

    private UIImageView ImageView;
  }

  internal sealed class iOSLayoutSearch : UISearchBar, iOSLayoutElement
  {
    public iOSLayoutSearch()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.SearchButtonClicked += (Sender, Event) =>
      {
        var Result = Sender as UIResponder;
        if (Result != null)
          Result.ResignFirstResponder();
      };

      this.SearchBarStyle = UISearchBarStyle.Prominent;
      this.Translucent = false; // hides weird semi-transparent rounded gray background.
      this.ShowsCancelButton = false;
      this.TextColor = UIColor.Black;
      this.TextFont = UIKit.UIFont.SystemFontOfSize(14);

      this.BackgroundColor = UIColor.Clear;
      this.BackgroundImage = new UIImage(); // fixes top and bottom thin black lines.
      this.Border = new iOSLayoutBorder(this);

      this.InputAccessoryView = new iOSLayoutDoneToolbar(this);
    }

    public bool IsReadOnly { get; set; }
    public UIFont TextFont { get; set; }
    public UIColor TextColor { get; set; }
    public iOSLayoutBorder Border { get; private set; }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      var TextField = GetTextField();

      if (TextField != null)
      {
        TextField.Frame = new CGRect(LayoutMargins.Left, LayoutMargins.Top, Frame.Size.Width - (LayoutMargins.Left + LayoutMargins.Right), Frame.Size.Height - (LayoutMargins.Top + LayoutMargins.Bottom));

        var FontSize = TextFont.PointSize;
        var IconSize = Frame.Size.Height - (LayoutMargins.Top + LayoutMargins.Bottom);
        var IconOffset = (IconSize - TextFont.PointSize) / 2;

        var GlassView = TextField.LeftView;
        if (GlassView != null)
          GlassView.Frame = new CGRect(LayoutMargins.Left, LayoutMargins.Top + IconOffset, FontSize, FontSize);

        // Clear button: UISearchBar > UIView > UISearchBarTextField > UIButton > UIImageView

        // TODO: can't seem to find the clear button anywhere in the subviews.
        //       probably have to do a custom clear button - something like: http://www.craigotis.com/custom-clear-button-for-uitextfield/
        /*
        var ClearButton = TextField.RightView;
        if (ClearButton != null)
          ClearButton.Frame = new CGRect(Frame.Size.Width - LayoutMargins.Right - FontSize, LayoutMargins.Top + IconOffset, FontSize, FontSize);
        */

        Border.Layout();
      }
    }
    public override void Draw(CGRect rect)
    {
      this.TintColor = BackgroundColor;
      this.BarTintColor = BackgroundColor;

      var TextField = GetTextField();

      if (TextField != null)
      {
        TextField.Font = TextFont;
        TextField.TextColor = TextColor;
        TextField.BackgroundColor = BackgroundColor;
        TextField.TintColor = TextColor; // cursor colour.
      }

      base.Draw(rect);
    }

    private UITextField GetTextField()
    {
      return Subviews[0].Subviews.Find(V => V is UITextField) as UITextField;
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var TextField = GetTextField();

      var Result = TextField != null ? TextField.SizeThatFits(this.FitToSize(MaxSize)) : SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }
  }

  internal sealed class iOSLayoutEdit : UITextField, iOSLayoutElement
  {
    public iOSLayoutEdit()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ShouldBeginEditing = (t) => !IsReadOnly;
      this.ShouldReturn = (t) =>
      {
        t.EndEditing(true);

        if (ReturnEvent != null)
          ReturnEvent();

        return true;
      };
      this.ShouldChangeCharacters = (t, r, s) =>
      {
        var View = Superview;
        while (View.Superview != null && !(View is UIScrollView))
          View = View.Superview;

        if (View is UIScrollView)
          (View as UIScrollView).ScrollRectToVisible(ConvertRectToView(Bounds, View), true);

        return true;
      };
      this.Border = new iOSLayoutBorder(this);
      this.InputAccessoryView = new iOSLayoutDoneToolbar(this);
    }

    public bool IsReadOnly { get; set; }
    public iOSLayoutBorder Border { get; private set; }
    public event Action ReturnEvent;

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      Border.Layout();
    }
    // TextRect and EditingRect observe the left and right padding.
    public override CGRect TextRect(CGRect forBounds)
    {
      forBounds.X += LayoutMargins.Left;
      forBounds.Width -= LayoutMargins.Right;
      return forBounds;
    }
    public override CGRect EditingRect(CGRect forBounds)
    {
      forBounds.X += LayoutMargins.Left;
      forBounds.Width -= LayoutMargins.Right;
      return forBounds;
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }
  }

  internal sealed class iOSLayoutDoneToolbar : UIKit.UIToolbar
  {
    public iOSLayoutDoneToolbar(UIView View)
      : base(new CoreGraphics.CGRect(0, 0, 320, 50))
    {
      this.BarStyle = UIKit.UIBarStyle.Default;
      this.Items = new UIKit.UIBarButtonItem[]
      {
         new UIKit.UIBarButtonItem(UIKit.UIBarButtonSystemItem.FlexibleSpace),
         new UIKit.UIBarButtonItem("Done", UIKit.UIBarButtonItemStyle.Done, (Sender, Event) => View.ResignFirstResponder())
       };
      this.SizeToFit();
    }
  }

  internal sealed class iOSLayoutMemo : UITextView, iOSLayoutElement
  {
    public iOSLayoutMemo()
    {
      this.BackgroundColor = UIColor.Clear;
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ContentInset = UIEdgeInsets.Zero;
      this.TextContainer.LineFragmentPadding = 0;
      this.TextContainerInset = UIEdgeInsets.Zero;
      this.Changed += WeakHelpers.WeakEventHandlerWrapper(this, (layoutMemo, Sender, Event) => layoutMemo.Arrange());
      this.ShouldBeginEditing = (t) => !IsReadOnly;
      this.Border = new iOSLayoutBorder(this);
      this.InputAccessoryView = new iOSLayoutDoneToolbar(this);
      //this.AlwaysBounceVertical = false;
      //this.AlwaysBounceHorizontal = false;
      //this.Bounces = false;
    }

    public override string Text
    {
      get
      {
        return base.Text;
      }
      set
      {
        base.Text = value;
        this.Arrange();
      }
    }
    public bool IsReadOnly { get; set; }
    public override UIEdgeInsets LayoutMargins
    {
      get { return base.TextContainerInset; }
      set { base.TextContainerInset = value; }
    }
    public iOSLayoutBorder Border { get; private set; }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      Border.Layout();
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom + 0.5F; // HACK: without this +0.5F, the memo height is slightly short when the memo is stretched to automatic height.  When this happens, the memo will think it needs to scroll and will show the bounce animation.

      return Result;
    }
  }

  internal sealed class iOSLayoutCanvas : UIView, iOSLayoutElement, Inv.DrawContract
  {
    public iOSLayoutCanvas(iOSEngine iOSEngine)
    {
      this.iOSEngine = iOSEngine;
      this.BackgroundColor = UIKit.UIColor.Clear;
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.Border = new iOSLayoutBorder(this);

      //this.Layer.ShouldRasterize = true; // NOTE: does not seem to help achieve 60fps.
      this.Layer.DrawsAsynchronously = true; // NOTE: very much seems to help achieve 60fps!

      AddGestureRecognizer(new UIKit.UITapGestureRecognizer(R =>
      {
        if (R.State == UIKit.UIGestureRecognizerState.Ended)
        {
          var SinglePoint = R.LocationInView(this);

          if (SingleTapEvent != null)
            SingleTapEvent(SinglePoint);

          TouchUpInvoke(SinglePoint);
        }
      }));
      AddGestureRecognizer(new UIKit.UITapGestureRecognizer(R =>
      {
        if (R.State == UIKit.UIGestureRecognizerState.Ended)
        {
          var DoublePoint = R.LocationInView(this);

          if (DoubleTapEvent != null)
            DoubleTapEvent(DoublePoint);

          TouchUpInvoke(DoublePoint);
        }
      }) { NumberOfTapsRequired = 2 });
      AddGestureRecognizer(new UIKit.UILongPressGestureRecognizer(R =>
      {
        if (R.State == UIKit.UIGestureRecognizerState.Ended)
        {
          var LongPoint = R.LocationInView(this);

          if (LongPressEvent != null)
            LongPressEvent(LongPoint);

          TouchUpInvoke(LongPoint);
        }
      }));

      AddGestureRecognizer(new UIKit.UIPinchGestureRecognizer(R =>
      {
        // NOTE: R.Scale is percentage increase/decrease eg 120% (1.20) or 20% (0.20)
        if (R.NumberOfTouches < 2)
        {
          R.Enabled = false;
          R.Enabled = true;

          TouchUpInvoke(R.LocationInView(this));
        }
        else
        {
          if (ZoomEvent != null && R.State == UIKit.UIGestureRecognizerState.Changed)
            ZoomEvent(R.LocationInView(this), R.Scale > 1.0F ? +1 : -1);

          if (R.State == UIKit.UIGestureRecognizerState.Ended || R.State == UIKit.UIGestureRecognizerState.Cancelled)
            TouchUpInvoke(R.LocationInView(this));
        }
      }));

      this.iOSEllipseRect = new CoreGraphics.CGRect();
      this.iOSRectangleRect = new CoreGraphics.CGRect();
      this.iOSTextPoint = new CoreGraphics.CGPoint();
      this.iOSImageRect = new CoreGraphics.CGRect();
    }

    public iOSLayoutBorder Border { get; private set; }
    public Action DrawAction;
    public event Action<CoreGraphics.CGPoint> TouchDownEvent;
    public event Action<CoreGraphics.CGPoint> TouchUpEvent;
    public event Action<CoreGraphics.CGPoint> TouchMoveEvent;
    public event Action<CoreGraphics.CGPoint> SingleTapEvent;
    public event Action<CoreGraphics.CGPoint> DoubleTapEvent;
    public event Action<CoreGraphics.CGPoint> LongPressEvent;
    public event Action<CoreGraphics.CGPoint, int> ZoomEvent;

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      Border.Layout();
    }
    public override void Draw(CoreGraphics.CGRect rect)
    {
      base.Draw(rect);

      this.iOSCanvas = UIGraphics.GetCurrentContext();
      DrawAction();
    }

    public override void TouchesBegan(NSSet touches, UIEvent evt)
    {
      base.TouchesBegan(touches, evt);

      if (TouchDownEvent != null)
      {
        var Touch = touches.AnyObject as UITouch;

        if (Touch != null)
          TouchDownEvent(Touch.LocationInView(this));
      }
    }
    public override void TouchesMoved(NSSet touches, UIEvent evt)
    {
      base.TouchesMoved(touches, evt);

      if (TouchMoveEvent != null)
      {
        var Touch = touches.AnyObject as UITouch;

        if (Touch != null)
          TouchMoveEvent(Touch.LocationInView(this));
      }
    }
    public override void TouchesEnded(NSSet touches, UIEvent evt)
    {
      base.TouchesEnded(touches, evt);

      if (TouchUpEvent != null)
      {
        var Touch = touches.AnyObject as UITouch;

        if (Touch != null)
          TouchUpEvent(Touch.LocationInView(this));
      }
    }

    private void TouchUpInvoke(CGPoint Point)
    {
      if (TouchUpEvent != null)
        TouchUpEvent(Point);
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    int DrawContract.Width
    {
      get { return (int)Frame.Width; }
    }
    int DrawContract.Height
    {
      get { return (int)Frame.Height; }
    }
    void DrawContract.DrawText(string TextFragment, string TextFontName, int TextFontSize, FontWeight TextFontWeight, Colour TextFontColour, Point TextPoint, HorizontalPosition TextHorizontal, VerticalPosition TextVertical)
    {
      var AttributedDictionary = new Foundation.NSMutableDictionary();
      AttributedDictionary["NSColor"] = iOSEngine.TranslateUIColor(TextFontColour);
      AttributedDictionary["NSFont"] = iOSEngine.TranslateUIFont(TextFontName.EmptyAsNull(), TextFontSize, TextFontWeight);

      var AttributedString = new Foundation.NSAttributedString(TextFragment, new CoreText.CTStringAttributes(AttributedDictionary));

      var TextRect = AttributedString.GetBoundingRect(CoreGraphics.CGSize.Empty, Foundation.NSStringDrawingOptions.OneShot, null);
      var TextPointX = TextPoint.X;
      var TextPointY = TextPoint.Y;

      if (TextHorizontal != HorizontalPosition.Left)
      {
        var TextWidth = (int)TextRect.Width;

        if (TextHorizontal == HorizontalPosition.Right)
          TextPointX -= TextWidth;
        else if (TextHorizontal == HorizontalPosition.Center)
          TextPointX -= TextWidth / 2;
      }

      if (TextVertical != VerticalPosition.Top)
      {
        var TextHeight = (int)TextRect.Height;

        if (TextVertical == VerticalPosition.Bottom)
          TextPointY -= TextHeight;
        else if (TextVertical == VerticalPosition.Center)
          TextPointY -= TextHeight / 2;
      }

      iOSTextPoint.X = TextPointX;
      iOSTextPoint.Y = TextPointY;

      AttributedString.DrawString(iOSTextPoint);
    }
    void DrawContract.DrawLine(Inv.Colour LineStrokeColour, int LineStrokeThickness, Inv.Point LineSourcePoint, Inv.Point LineTargetPoint, params Inv.Point[] LineExtraPointArray)
    {
      if (LineStrokeThickness <= 0 || LineStrokeColour == Inv.Colour.Transparent)
        return;

      var iOSPointArray = new CGPoint[(LineExtraPointArray.Length * 2) + 2];
      iOSPointArray[0] = iOSEngine.TranslateCGPoint(LineSourcePoint);
      iOSPointArray[1] = iOSEngine.TranslateCGPoint(LineTargetPoint);

      var CurrentPoint = iOSPointArray[1];
      var iOSPointIndex = 2;
      for (var Index = 0; Index < LineExtraPointArray.Length; Index++)
      {
        var TargetPoint = iOSEngine.TranslateCGPoint(LineExtraPointArray[Index]);

        iOSPointArray[iOSPointIndex++] = CurrentPoint;
        iOSPointArray[iOSPointIndex++] = TargetPoint;

        CurrentPoint = TargetPoint;
      }

      iOSCanvas.SetStrokeColor(iOSEngine.TranslateCGColor(LineStrokeColour));
      iOSCanvas.SetLineWidth(LineStrokeThickness);
      iOSCanvas.SetLineCap(CGLineCap.Round);
      iOSCanvas.StrokeLineSegments(iOSPointArray);
    }
    void DrawContract.DrawRectangle(Colour RectangleFillColour, Colour RectangleStrokeColour, int RectangleStrokeThickness, Rect RectangleRect)
    {
      iOSRectangleRect.X = RectangleRect.Left;
      iOSRectangleRect.Y = RectangleRect.Top;
      iOSRectangleRect.Width = RectangleRect.Width;
      iOSRectangleRect.Height = RectangleRect.Height;

      if (RectangleFillColour != null)
      {
        iOSCanvas.SetFillColor(iOSEngine.TranslateCGColor(RectangleFillColour));
        iOSCanvas.FillRect(iOSRectangleRect);
      }

      if (RectangleStrokeColour != null && RectangleStrokeThickness > 0)
      {
        var HalfThickness = RectangleStrokeThickness / 2.0F;
        iOSRectangleRect.X += HalfThickness;
        iOSRectangleRect.Y += HalfThickness;

        if (iOSRectangleRect.Width > RectangleStrokeThickness)
          iOSRectangleRect.Width -= RectangleStrokeThickness;

        if (iOSRectangleRect.Height > RectangleStrokeThickness)
          iOSRectangleRect.Height -= RectangleStrokeThickness;

        iOSCanvas.SetStrokeColor(iOSEngine.TranslateCGColor(RectangleStrokeColour));
        iOSCanvas.SetLineWidth(RectangleStrokeThickness);
        iOSCanvas.StrokeRect(iOSRectangleRect);
      }
    }
    void DrawContract.DrawArc(Inv.Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Inv.Point ArcCenter, Inv.Point ArcRadius, float StartAngle, float SweepAngle)
    {
      var MaxWidth = (float)Math.Max(0.0, (ArcRadius.X * 2) - ArcStrokeThickness);
      var MaxHeight = (float)Math.Max(0.0, (ArcRadius.Y * 2) - ArcStrokeThickness);

      var StartAngleRad = SanitisedDegreesToRadians(360 - StartAngle);
      var SweepAngleRad = SanitisedDegreesToRadians(360 - SweepAngle);

      var Center = new CGPoint(ArcCenter.X, ArcCenter.Y);

      var BezierPath = new UIBezierPath();
      BezierPath.MoveTo(Center);
      BezierPath.AddArc
      (
        center: Center,
        radius: Math.Max(MaxWidth / 2, MaxHeight / 2),
        startAngle: SweepAngleRad,
        endAngle: StartAngleRad,
        clockWise: true
      );

      BezierPath.ClosePath();

      if (ArcFillColour != null)
      {
        iOSCanvas.SetFillColor(iOSEngine.TranslateCGColor(ArcFillColour));
        BezierPath.Fill();
      }

      if (ArcStrokeColour != null && ArcStrokeThickness > 0)
      {
        iOSCanvas.SetStrokeColor(iOSEngine.TranslateCGColor(ArcStrokeColour));
        BezierPath.LineWidth = ArcStrokeThickness;
        BezierPath.Stroke();
      }
    }
    void DrawContract.DrawEllipse(Colour EllipseFillColour, Colour EllipseStrokeColour, int EllipseStrokeThickness, Point EllipseCenter, Point EllipseRadius)
    {
      iOSEllipseRect.X = EllipseCenter.X - EllipseRadius.X;
      iOSEllipseRect.Y = EllipseCenter.Y - EllipseRadius.Y;
      iOSEllipseRect.Width = EllipseRadius.X * 2;
      iOSEllipseRect.Height = EllipseRadius.Y * 2;

      if (EllipseFillColour != null)
      {
        iOSCanvas.SetFillColor(iOSEngine.TranslateCGColor(EllipseFillColour));
        iOSCanvas.FillEllipseInRect(iOSEllipseRect);
      }

      if (EllipseStrokeColour != null && EllipseStrokeThickness > 0)
      {
        iOSCanvas.SetStrokeColor(iOSEngine.TranslateCGColor(EllipseStrokeColour));
        iOSCanvas.SetLineWidth(EllipseStrokeThickness);
        iOSCanvas.StrokeEllipseInRect(iOSEllipseRect);
      }
    }
    void DrawContract.DrawImage(Image ImageSource, Rect ImageRect, float ImageOpacity, Colour ImageTint, Mirror? ImageMirror)
    {
      var iOSImageSource = iOSEngine.TranslateCGImage(ImageSource, ImageRect.Width, ImageRect.Height);

      iOSCanvas.SaveState();

      if (ImageMirror == null)
      {
        var BoundsHeight = Bounds.Height;

        iOSImageRect.X = ImageRect.Left;
        iOSImageRect.Y = BoundsHeight - ImageRect.Top - ImageRect.Height;
        iOSImageRect.Width = ImageRect.Width;
        iOSImageRect.Height = ImageRect.Height;

        iOSCanvas.TranslateCTM(0, BoundsHeight);
        iOSCanvas.ScaleCTM(+1.0F, -1.0F);
      }
      else if (ImageMirror.Value == Mirror.Horizontal)
      {
        var BoundsWidth = Bounds.Width;
        var BoundsHeight = Bounds.Height;

        iOSImageRect.X = BoundsWidth - ImageRect.Left - ImageRect.Width;
        iOSImageRect.Y = BoundsHeight - ImageRect.Top - ImageRect.Height;
        iOSImageRect.Width = ImageRect.Width;
        iOSImageRect.Height = ImageRect.Height;

        iOSCanvas.TranslateCTM(BoundsWidth, BoundsHeight);
        iOSCanvas.ScaleCTM(-1.0F, -1.0F);
      }
      else if (ImageMirror.Value == Mirror.Vertical)
      {
        iOSImageRect.X = ImageRect.Left;
        iOSImageRect.Y = ImageRect.Top;
        iOSImageRect.Width = ImageRect.Width;
        iOSImageRect.Height = ImageRect.Height;
      }

      if (ImageOpacity != 1.0F)
        iOSCanvas.SetAlpha(ImageOpacity);

      iOSCanvas.DrawImage(iOSImageRect, iOSImageSource);

      // TODO: use of image tint severely impacts on FPS.
      if (ImageTint != null)
      {
        iOSCanvas.ClipToMask(iOSImageRect, iOSImageSource);
        iOSCanvas.SetFillColor(iOSEngine.TranslateCGColor(ImageTint));
        iOSCanvas.FillRect(iOSImageRect);
      }

      iOSCanvas.RestoreState();
    }
    void DrawContract.DrawPolygon(Colour FillColour, Colour StrokeColour, int StrokeThickness, LineJoin LineJoin, Point StartPoint, params Point[] PointArray)
    {
    }

    private float SanitisedDegreesToRadians(float StartAngle)
    {
      if (StartAngle < 0)
        StartAngle = ((StartAngle % 360) + 360) % 360;
      else if (StartAngle >= 360)
        StartAngle = StartAngle % 360;

      return (float)(StartAngle * Math.PI / 180.0);
    }

    private CGContext iOSCanvas;
    private CGRect iOSEllipseRect;
    private CGRect iOSRectangleRect;
    private CGPoint iOSTextPoint;
    private CGRect iOSImageRect;
    private iOSEngine iOSEngine;
  }

  internal enum iOSLayoutHorizontal
  {
    Stretch,
    Center,
    Left,
    Right
  }

  internal enum iOSLayoutVertical
  {
    Stretch,
    Center,
    Top,
    Bottom
  }

  internal sealed class iOSLayoutContainer : UIView, iOSLayoutElement
  {
    public iOSLayoutContainer()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ContentWidth = float.NaN;
      this.ContentHeight = float.NaN;
      this.ContentMinimumWidth = float.NaN;
      this.ContentMinimumHeight = float.NaN;
      this.ContentMaximumWidth = float.NaN;
      this.ContentMaximumHeight = float.NaN;
      this.ContentVisible = true;
      this.Border = new iOSLayoutBorder(this);

      //this.ClipsToBounds = true;
    }

    public iOSLayoutBorder Border { get; private set; }

    public void SetContentVisiblity(bool Visible)
    {
      if (Visible != this.ContentVisible)
      {
        this.ContentVisible = Visible;

        this.Arrange();
      }
    }
    public void SetContentAlignment(iOSLayoutVertical Vertical, iOSLayoutHorizontal Horizontal)
    {
      if (Vertical != this.ContentVertical || Horizontal != this.ContentHorizontal)
      {
        this.ContentVertical = Vertical;
        this.ContentHorizontal = Horizontal;

        this.Arrange();
      }
    }
    public void SetContentElement(iOSLayoutElement Content)
    {
      if (Content != this.ContentElement)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement.View);

        this.ContentElement = Content;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement.View);

        this.Arrange();
      }
    }
    public void SetContentWidth(float Width)
    {
      if (Width != this.ContentWidth)
      {
        this.ContentWidth = Width;

        this.Arrange();
      }
    }
    public void SetContentHeight(float Height)
    {
      if (Height != this.ContentHeight)
      {
        this.ContentHeight = Height;

        this.Arrange();
      }
    }
    public void SetContentMinimumWidth(float MinimumWidth)
    {
      if (MinimumWidth != this.ContentMinimumWidth)
      {
        this.ContentMinimumWidth = MinimumWidth;

        this.Arrange();
      }
    }
    public void SetContentMaximumWidth(float MaximumWidth)
    {
      if (MaximumWidth != this.ContentMaximumWidth)
      {
        this.ContentMaximumWidth = MaximumWidth;

        this.Arrange();
      }
    }
    public void SetContentMinimumHeight(float MinimumHeight)
    {
      if (MinimumHeight != this.ContentMinimumHeight)
      {
        this.ContentMinimumHeight = MinimumHeight;

        this.Arrange();
      }
    }
    public void SetContentMaximumHeight(float MaximumHeight)
    {
      if (MaximumHeight != this.ContentMaximumHeight)
      {
        this.ContentMaximumHeight = MaximumHeight;

        this.Arrange();
      }
    }

    public override UIView HitTest(CGPoint Point, UIEvent Event)
    {
      return this.HitTestFindNothing(base.HitTest(Point, Event));
    }
    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      if (ContentElement != null)
      {
        var Element = ContentElement.View;

        if (!ContentVisible)
        {
          Element.Hidden = true;
          Element.Frame = CGRect.Empty;
        }
        else
        {
          var BoundsSize = Bounds.Size;
          BoundsSize.Width -= LayoutMargins.Left + LayoutMargins.Right;
          BoundsSize.Height -= LayoutMargins.Top + LayoutMargins.Bottom;

          var ElementSize = MeasureSize(BoundsSize);

          nfloat ElementWidth;
          nfloat ElementX;

          // TODO: this check differs the behaviour from Android/Wpf, but otherwise it will draw over other views.
          if (ElementSize.Width > BoundsSize.Width)
          {
            ElementWidth = BoundsSize.Width;
            ElementX = 0;
          }
          else
          {
            switch (ContentHorizontal)
            {
              case iOSLayoutHorizontal.Stretch:
                if (!float.IsNaN(this.ContentWidth) && this.ContentWidth < BoundsSize.Width)
                {
                  ElementWidth = ContentWidth;
                  ElementX = (BoundsSize.Width - ContentWidth) / 2;
                }
                else if (!float.IsNaN(ContentMaximumWidth) && ContentMaximumWidth < BoundsSize.Width)
                {
                  ElementWidth = ContentMaximumWidth;
                  ElementX = (BoundsSize.Width - ContentMaximumWidth) / 2;
                }
                else
                {
                  ElementWidth = BoundsSize.Width;
                  ElementX = 0;
                }
                break;

              case iOSLayoutHorizontal.Center:
                ElementWidth = ElementSize.Width;
                ElementX = (BoundsSize.Width - ElementWidth) / 2;
                break;

              case iOSLayoutHorizontal.Left:
                ElementWidth = ElementSize.Width;
                ElementX = 0;
                break;

              case iOSLayoutHorizontal.Right:
                ElementWidth = ElementSize.Width;
                ElementX = BoundsSize.Width - ElementWidth;
                break;

              default:
                throw new Exception("LayoutVertical not handled.");
            }
          }

          nfloat ElementHeight;
          nfloat ElementY;

          // TODO: this check differs the behaviour from Android/Wpf, but otherwise it will draw over other views.
          if (ElementSize.Height > BoundsSize.Height)
          {
            ElementHeight = BoundsSize.Height;
            ElementY = 0;
          }
          else
          {
            switch (ContentVertical)
            {
              case iOSLayoutVertical.Stretch:
                if (!float.IsNaN(this.ContentHeight) && this.ContentHeight < BoundsSize.Height)
                {
                  ElementHeight = ContentHeight;
                  ElementY = (BoundsSize.Height - ContentHeight) / 2;
                }
                else if (!float.IsNaN(this.ContentMaximumHeight) && this.ContentMaximumHeight < BoundsSize.Height)
                {
                  ElementHeight = ContentMaximumHeight;
                  ElementY = (BoundsSize.Height - ContentMaximumHeight) / 2;
                }
                else
                {
                  ElementHeight = BoundsSize.Height;
                  ElementY = 0;
                }
                break;

              case iOSLayoutVertical.Center:
                ElementHeight = ElementSize.Height;
                ElementY = (BoundsSize.Height - ElementHeight) / 2;
                break;

              case iOSLayoutVertical.Top:
                ElementHeight = ElementSize.Height;
                ElementY = 0;
                break;

              case iOSLayoutVertical.Bottom:
                ElementHeight = ElementSize.Height;
                ElementY = BoundsSize.Height - ElementHeight;
                break;

              default:
                throw new Exception("LayoutVertical not handled.");
            }
          }

          //Element.Frame = LayoutMargins.InsetRect(new CGRect(ContentX, ContentY, ContentWidth, ContentHeight));
          Element.Hidden = false;
          Element.Frame = new CGRect(LayoutMargins.Left + ElementX, LayoutMargins.Top + ElementY, ElementWidth, ElementHeight);
        }
      }

      Border.Layout();
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = CGSize.Empty;

      if (ContentVisible)
      {
        Result = MeasureSize(this.FitToSize(MaxSize));

        Result.Width += LayoutMargins.Left + LayoutMargins.Right;
        Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;
      }

      return Result;
    }

    private CGSize MeasureSize(CGSize MaxSize)
    {
      if (!float.IsNaN(ContentWidth) && !float.IsNaN(ContentHeight))
      {
        return new CGSize(ContentWidth, ContentHeight);
      }
      else
      {
        var ContentSize = MaxSize;
        if (!float.IsNaN(ContentWidth))
          ContentSize.Width = ContentWidth;
        else if (!float.IsNaN(ContentMaximumWidth) && ContentMaximumWidth < ContentSize.Width)
          ContentSize.Width = ContentMaximumWidth;

        if (!float.IsNaN(ContentHeight))
          ContentSize.Height = ContentHeight;
        else if (!float.IsNaN(ContentMaximumHeight) && ContentMaximumHeight < ContentSize.Height)
          ContentSize.Height = ContentMaximumHeight;

        var Result = ContentElement != null ? ContentElement.CalculateSize(ContentSize) : CGSize.Empty;

        if (!float.IsNaN(ContentWidth))
        {
          Result.Width = ContentWidth;
        }
        else
        {
          if (!float.IsNaN(ContentMinimumWidth))
            Result.Width = Math.Max(ContentMinimumWidth, (float)Result.Width);

          if (!float.IsNaN(ContentMaximumWidth))
            Result.Width = Math.Min(ContentMaximumWidth, (float)Result.Width);
        }

        if (!float.IsNaN(ContentHeight))
        {
          Result.Height = ContentHeight;
        }
        else
        {
          if (!float.IsNaN(ContentMinimumHeight))
            Result.Height = Math.Max(ContentMinimumHeight, (float)Result.Height);

          if (!float.IsNaN(ContentMaximumHeight))
            Result.Height = Math.Min(ContentMaximumHeight, (float)Result.Height);
        }

        return Result;
      }
    }

    private iOSLayoutElement ContentElement;
    private iOSLayoutVertical ContentVertical;
    private iOSLayoutHorizontal ContentHorizontal;
    private bool ContentVisible;
    private float ContentWidth;
    private float ContentHeight;
    private float ContentMinimumWidth;
    private float ContentMinimumHeight;
    private float ContentMaximumWidth;
    private float ContentMaximumHeight;
  }

  internal enum iOSLayoutOrientation
  {
    Vertical,
    Horizontal
  }

  internal sealed class iOSLayoutFrame : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutFrame()
      : base()
    {
      this.Border = new iOSLayoutBorder(this);
    }

    public void SetContentElement(iOSLayoutElement Content)
    {
      if (Content != this.Content)
      {
        if (this.Content != null)
          this.SafeRemoveView(this.Content.View);

        this.Content = Content;

        if (this.Content != null)
          this.SafeAddView(this.Content.View);

        this.Arrange();
      }
    }

    public iOSLayoutBorder Border { get; private set; }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      if (Content != null)
        Content.View.Frame = LayoutMargins.InsetRect(Bounds);

      Border.Layout();
    }

    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = Content != null ? Content.CalculateSize(this.FitToSize(MaxSize)) : CGSize.Empty;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }

    private iOSLayoutElement Content;
  }

  internal sealed class iOSLayoutStack : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutStack()
      : base()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ElementArray = new iOSLayoutElement[] { };
      this.Border = new iOSLayoutBorder(this);
    }

    public iOSLayoutBorder Border { get; private set; }

    public void SetOrientation(iOSLayoutOrientation Orientation)
    {
      if (Orientation != this.Orientation)
      {
        this.Orientation = Orientation;

        this.Arrange();
      }
    }
    public void ComposeElements(IEnumerable<iOSLayoutElement> Elements)
    {
      var PreviousArray = ElementArray;
      this.ElementArray = Elements.ToArray();

      foreach (var Element in ElementArray.Except(PreviousArray))
        this.SafeAddView(Element.View);

      foreach (var Element in PreviousArray.Except(ElementArray))
        this.SafeRemoveView(Element.View);

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      switch (Orientation)
      {
        case iOSLayoutOrientation.Horizontal:
          var StackHeight = Bounds.Height - LayoutMargins.Top - LayoutMargins.Bottom;
          var ElementLeft = (nfloat)LayoutMargins.Left;

          var ColumnSize = new CGSize(UIView.NoIntrinsicMetric, StackHeight);

          foreach (var Element in ElementArray)
          {
            var ElementSize = Element.CalculateSize(ColumnSize);

            Element.View.Frame = new CGRect(ElementLeft, LayoutMargins.Top, ElementSize.Width, StackHeight);

            ElementLeft += ElementSize.Width;
          }
          break;

        case iOSLayoutOrientation.Vertical:
          var StackWidth = Bounds.Width - LayoutMargins.Left - LayoutMargins.Right;
          var ElementTop = (nfloat)LayoutMargins.Top;

          var RowSize = new CGSize(StackWidth, UIView.NoIntrinsicMetric);

          foreach (var Element in ElementArray)
          {
            var PanelSize = Element.CalculateSize(RowSize);

            Element.View.Frame = new CGRect(LayoutMargins.Left, ElementTop, StackWidth, PanelSize.Height);

            ElementTop += PanelSize.Height;
          }
          break;

        default:
          throw new Exception("LayoutOrientation is not handled.");
      }

      Border.Layout();
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      switch (Orientation)
      {
        case iOSLayoutOrientation.Vertical:
          foreach (var Element in ElementArray)
          {
            var ElementSize = Element.CalculateSize(FitSize);

            if (ElementSize.Width > Result.Width)
              Result.Width = ElementSize.Width;

            Result.Height += ElementSize.Height;
          }
          break;

        case iOSLayoutOrientation.Horizontal:
          foreach (var Element in ElementArray)
          {
            var ElementSize = Element.CalculateSize(FitSize);

            if (ElementSize.Height > Result.Height)
              Result.Height = ElementSize.Height;

            Result.Width += ElementSize.Width;
          }
          break;

        default:
          throw new Exception("LayoutOrientation is not handled.");
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private iOSLayoutElement[] ElementArray;
    private iOSLayoutOrientation Orientation;
  }

  internal sealed class iOSLayoutDock : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutDock()
      : base()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ActiveElementList = new DistinctList<iOSLayoutElement>();
      this.HeaderList = new DistinctList<iOSLayoutElement>();
      this.ClientList = new DistinctList<iOSLayoutElement>();
      this.FooterList = new DistinctList<iOSLayoutElement>();
      this.ElementListArray = new[] { HeaderList, FooterList, ClientList };
      this.Border = new iOSLayoutBorder(this);
    }

    public iOSLayoutBorder Border { get; private set; }

    public void SetOrientation(iOSLayoutOrientation Orientation)
    {
      if (Orientation != this.Orientation)
      {
        this.Orientation = Orientation;

        this.Arrange();
      }
    }
    public void ComposeElements(IEnumerable<iOSLayoutElement> Headers, IEnumerable<iOSLayoutElement> Clients, IEnumerable<iOSLayoutElement> Footers)
    {
      this.HeaderList.Clear();
      HeaderList.AddRange(Headers);

      this.ClientList.Clear();
      ClientList.AddRange(Clients);

      this.FooterList.Clear();
      FooterList.AddRange(Footers);

      var PreviousElementList = ActiveElementList;
      this.ActiveElementList = new DistinctList<iOSLayoutElement>(HeaderList.Count + ClientList.Count + FooterList.Count);
      ActiveElementList.AddRange(HeaderList);
      ActiveElementList.AddRange(ClientList);
      ActiveElementList.AddRange(FooterList);

      foreach (var Element in ActiveElementList.Except(PreviousElementList))
        this.SafeAddView(Element.View);

      foreach (var Header in PreviousElementList.Except(ActiveElementList))
        this.SafeRemoveView(Header.View);

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      var DockWidth = Bounds.Width - LayoutMargins.Left - LayoutMargins.Right;
      var DockHeight = Bounds.Height - LayoutMargins.Top - LayoutMargins.Bottom;

      switch (Orientation)
      {
        case iOSLayoutOrientation.Horizontal:
          var PanelLeft = (nfloat)LayoutMargins.Left;

          var ColumnSize = new CGSize(UIView.NoIntrinsicMetric, DockHeight);
          var HeaderWidth = (nfloat)0;

          foreach (var Header in HeaderList)
          {
            var HeaderSize = Header.CalculateSize(ColumnSize);
            Header.View.Frame = new CGRect(PanelLeft, LayoutMargins.Top, HeaderSize.Width, DockHeight);
            PanelLeft += HeaderSize.Width;
            HeaderWidth += HeaderSize.Width;
          }

          var FooterWidth = (nfloat)FooterList.Sum(F => F.CalculateSize(ColumnSize).Width);
          var RemainderWidth = DockWidth - HeaderWidth - FooterWidth;

          if (ClientList.Count > 0)
          {
            var SharedWidth = RemainderWidth < 0 ? 0 : RemainderWidth / ClientList.Count;

            var LastClient = ClientList.Last();

            foreach (var Client in ClientList)
            {
              var FrameWidth = SharedWidth;
              if (RemainderWidth > 0 && Client == LastClient)
                FrameWidth += RemainderWidth - (SharedWidth * ClientList.Count);

              Client.View.Frame = new CGRect(PanelLeft, LayoutMargins.Top, FrameWidth, DockHeight);
              PanelLeft += FrameWidth;
            }
          }

          var PanelRight = LayoutMargins.Left + (RemainderWidth < 0 ? DockWidth - RemainderWidth : DockWidth);

          foreach (var Footer in FooterList)
          {
            var FooterSize = Footer.CalculateSize(ColumnSize);
            PanelRight -= FooterSize.Width;
            Footer.View.Frame = new CGRect(PanelRight, LayoutMargins.Top, FooterSize.Width, DockHeight);
          }
          break;

        case iOSLayoutOrientation.Vertical:
          var PanelTop = (nfloat)LayoutMargins.Top;

          var RowSize = new CGSize(DockWidth, UIView.NoIntrinsicMetric);
          var HeaderHeight = (nfloat)0;

          foreach (var Header in HeaderList)
          {
            var HeaderSize = Header.CalculateSize(RowSize);
            Header.View.Frame = new CGRect(LayoutMargins.Left, PanelTop, DockWidth, HeaderSize.Height);
            PanelTop += HeaderSize.Height;
            HeaderHeight += HeaderSize.Height;
          }

          var FooterHeight = (nfloat)FooterList.Sum(F => F.CalculateSize(RowSize).Height);
          var RemainderHeight = DockHeight - HeaderHeight - FooterHeight;

          if (ClientList.Count > 0)
          {
            var SharedHeight = RemainderHeight < 0 ? 0 : RemainderHeight / ClientList.Count;
            var LastClient = ClientList.Last();

            foreach (var Client in ClientList)
            {
              var FrameHeight = SharedHeight;
              if (RemainderHeight > 0 && Client == LastClient)
                FrameHeight += RemainderHeight - (SharedHeight * ClientList.Count);

              Client.View.Frame = new CGRect(LayoutMargins.Left, PanelTop, DockWidth, FrameHeight);
              PanelTop += FrameHeight;
            }
          }

          var PanelBottom = LayoutMargins.Top + (RemainderHeight < 0 ? DockHeight - RemainderHeight : DockHeight);

          foreach (var Footer in FooterList)
          {
            var FooterSize = Footer.CalculateSize(RowSize);
            PanelBottom -= FooterSize.Height;
            Footer.View.Frame = new CGRect(LayoutMargins.Left, PanelBottom, DockWidth, FooterSize.Height);
          }
          break;

        default:
          throw new Exception("LayoutOrientation is not handled.");
      }

      Border.Layout();
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      switch (Orientation)
      {
        case iOSLayoutOrientation.Vertical:
          foreach (var ElementList in ElementListArray)
          {
            foreach (var Element in ElementList)
            {
              var ElementSize = Element.CalculateSize(FitSize);

              if (ElementSize.Width > Result.Width)
                Result.Width = ElementSize.Width;

              if (FitSize.Height != UIView.NoIntrinsicMetric)
                FitSize.Height -= ElementSize.Height;

              Result.Height += ElementSize.Height;
            }
          }
          break;

        case iOSLayoutOrientation.Horizontal:
          foreach (var ElementList in ElementListArray)
          {
            foreach (var Element in ElementList)
            {
              var ElementSize = Element.CalculateSize(FitSize);

              if (ElementSize.Height > Result.Height)
                Result.Height = ElementSize.Height;

              if (FitSize.Width != UIView.NoIntrinsicMetric)
                FitSize.Width -= ElementSize.Width;

              Result.Width += ElementSize.Width;
            }
          }
          break;

        default:
          throw new Exception("LayoutOrientation is not handled.");
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private Inv.DistinctList<iOSLayoutElement> ActiveElementList;
    private Inv.DistinctList<iOSLayoutElement> HeaderList;
    private Inv.DistinctList<iOSLayoutElement> ClientList;
    private Inv.DistinctList<iOSLayoutElement> FooterList;
    private Inv.DistinctList<iOSLayoutElement>[] ElementListArray;
    private iOSLayoutOrientation Orientation;
  }

  internal sealed class iOSLayoutOverlay : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutOverlay()
      : base()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ElementList = new DistinctList<iOSLayoutElement>();
      this.Border = new iOSLayoutBorder(this);
    }

    public iOSLayoutBorder Border { get; private set; }

    public void ComposeElements(IEnumerable<iOSLayoutElement> Elements)
    {
      var PreviousList = ElementList;
      this.ElementList = Elements.ToDistinctList();

      foreach (var Element in ElementList.Except(PreviousList))
        this.SafeAddView(Element.View);

      foreach (var Element in PreviousList.Except(ElementList))
        this.SafeRemoveView(Element.View);

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      var PanelFrame = LayoutMargins.InsetRect(Bounds);
      foreach (var Panel in ElementList)
        Panel.View.Frame = PanelFrame;

      Border.Layout();
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      foreach (var Panel in ElementList)
      {
        var PanelSize = Panel.CalculateSize(FitSize);

        if (PanelSize.Width > Result.Width)
          Result.Width = PanelSize.Width;

        if (PanelSize.Height > Result.Height)
          Result.Height = PanelSize.Height;
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private Inv.DistinctList<iOSLayoutElement> ElementList;
  }

  internal sealed class iOSLayoutBoard : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutBoard()
      : base()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.RecordList = new DistinctList<iOSLayoutRecord>();
      this.Border = new iOSLayoutBorder(this);
    }

    public iOSLayoutBorder Border { get; private set; }

    public void RemoveElements()
    {
      if (RecordList.Count > 0)
      {
        foreach (var Record in RecordList)
          this.SafeRemoveView(Record.Element.View);
        RecordList.Clear();

        this.Arrange();
      }
    }
    public void AddElement(iOSLayoutElement Element, CGRect Rect)
    {
      RecordList.Add(new iOSLayoutRecord()
      {
        Element = Element,
        Rect = Rect
      });
      this.SafeAddView(Element.View);

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      foreach (var Record in RecordList)
        Record.Element.View.Frame = LayoutMargins.InsetRect(Record.Rect);

      Border.Layout();
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = new CGSize();

      foreach (var Record in RecordList)
      {
        var RecordSize = Record.Rect.Size;

        if (RecordSize.Width > Result.Width)
          Result.Width = RecordSize.Width;

        if (RecordSize.Height > Result.Height)
          Result.Height = RecordSize.Height;
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private Inv.DistinctList<iOSLayoutRecord> RecordList;

    private sealed class iOSLayoutRecord
    {
      public iOSLayoutElement Element;
      public CGRect Rect;
    }
  }

  internal sealed class iOSLayoutTable : iOSLayoutView, iOSLayoutElement
  {
    public iOSLayoutTable()
      : base()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.RowList = new DistinctList<iOSLayoutTableRow>();
      this.ColumnList = new DistinctList<iOSLayoutTableColumn>();
      this.CellList = new DistinctList<iOSLayoutTableCell>();
      this.ElementList = new DistinctList<iOSLayoutElement>();
      this.Border = new iOSLayoutBorder(this);
    }

    public iOSLayoutBorder Border { get; private set; }

    public void ComposeElements(IEnumerable<iOSLayoutTableRow> Rows, IEnumerable<iOSLayoutTableColumn> Columns, IEnumerable<iOSLayoutTableCell> Cells)
    {
      this.RowList.Clear();
      RowList.AddRange(Rows);

      this.ColumnList.Clear();
      ColumnList.AddRange(Columns);

      this.CellList.Clear();
      CellList.AddRange(Cells);

      var PreviousElementList = ElementList;
      this.ElementList = new DistinctList<iOSLayoutElement>(RowList.Count + ColumnList.Count + CellList.Count);
      ElementList.AddRange(RowList.Select(R => R.Element).ExceptNull());
      ElementList.AddRange(ColumnList.Select(R => R.Element).ExceptNull());
      ElementList.AddRange(CellList.Select(R => R.Element).ExceptNull());

      foreach (var Element in ElementList.Except(PreviousElementList))
        this.SafeAddView(Element.View);

      foreach (var Header in PreviousElementList.Except(ElementList))
        this.SafeRemoveView(Header.View);

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      var AvailableWidth = Bounds.Width - LayoutMargins.Left - LayoutMargins.Right;
      var AvailableHeight = Bounds.Height - LayoutMargins.Top - LayoutMargins.Bottom;

      var RowLimit = new CGSize(AvailableWidth, UIView.NoIntrinsicMetric);
      var ColumnLimit = new CGSize(UIView.NoIntrinsicMetric, AvailableHeight);

      var StaticRowList = RowList.Where(R => !R.Length.IsStar).ToDistinctList();
      var DynamicRowList = RowList.Where(R => R.Length.IsStar).ToDistinctList();

      var StaticColumnList = ColumnList.Where(R => !R.Length.IsStar).ToDistinctList();
      var DynamicColumnList = ColumnList.Where(R => R.Length.IsStar).ToDistinctList();

      var LastStarRow = DynamicRowList.LastOrDefault();
      var LastStarColumn = DynamicColumnList.LastOrDefault();

      var DynamicRowWeight = (nfloat)DynamicRowList.Sum(R => R.Length.Size ?? 1);
      var DynamicColumnWeight = (nfloat)DynamicColumnList.Sum(C => C.Length.Size ?? 1);

      var StaticHeight = (nfloat)StaticRowList.Sum(R => R.CalculateSize(RowLimit).Height);
      var StaticWidth = (nfloat)StaticColumnList.Sum(C => C.CalculateSize(ColumnLimit).Width);

      var RemainderWidth = AvailableWidth - StaticWidth;
      var RemainderHeight = AvailableHeight - StaticHeight;

      var ActualSize = new CGSize(DynamicColumnList.Count > 0 ? AvailableWidth : StaticWidth, DynamicRowList.Count > 0 ? AvailableHeight : StaticHeight);

      var TableTop = LayoutMargins.Top;
      var TableLeft = LayoutMargins.Left;

      foreach (var Column in ColumnList)
      {
        var ColumnWidth = Column.CalculateSize(ActualSize).Width;

        if (Column.Length.IsStar)
        {
          ColumnWidth = (int)(((Column.Length.Size ?? 1) / DynamicColumnWeight) * RemainderWidth);
          // TODO: last star column bucket.
        }

        if (Column.Element != null)
          Column.Element.View.Frame = new CGRect(TableLeft, TableTop, ColumnWidth, ActualSize.Height);

        Column.TempStart = TableLeft;
        Column.TempLength = ColumnWidth;

        TableLeft += ColumnWidth;
      }

      TableTop = LayoutMargins.Top;
      TableLeft = LayoutMargins.Left;

      foreach (var Row in RowList)
      {
        var RowHeight = Row.CalculateSize(ActualSize).Height;

        if (Row.Length.IsStar)
        {
          RowHeight = (int)(((Row.Length.Size ?? 1) / DynamicRowWeight) * RemainderHeight);
          // TODO: last star row bucket.
        }

        if (Row.Element != null)
          Row.Element.View.Frame = new CGRect(TableLeft, TableTop, ActualSize.Width, RowHeight);

        Row.TempStart = TableTop;
        Row.TempLength = RowHeight;

        TableTop += RowHeight;
      }

      foreach (var Cell in CellList)
      {
        if (Cell.Element != null)
          Cell.Element.View.Frame = new CGRect(Cell.Column.TempStart, Cell.Row.TempStart, Cell.Column.TempLength, Cell.Row.TempLength);
      }

      Border.Layout();
    }

    internal Inv.DistinctList<iOSLayoutTableRow> RowList { get; private set; }
    internal Inv.DistinctList<iOSLayoutTableColumn> ColumnList { get; private set; }
    internal Inv.DistinctList<iOSLayoutTableCell> CellList { get; private set; }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      foreach (var Row in RowList)
      {
        var RowSize = Row.CalculateSize(FitSize);

        if (RowSize.Width > Result.Width)
          Result.Width = RowSize.Width;

        Result.Height += RowSize.Height;
      }

      foreach (var Column in ColumnList)
      {
        var ColumnSize = Column.CalculateSize(FitSize);

        if (ColumnSize.Height > Result.Height)
          Result.Height = ColumnSize.Height;

        Result.Width += ColumnSize.Width;
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private DistinctList<iOSLayoutElement> ElementList;
  }

  internal sealed class iOSLayoutTableLength
  {
    public iOSLayoutTableLength(bool Fill, int? Value)
    {
      this.Fill = Fill;
      this.Size = Value;
    }

    public bool Fill { get; private set; }
    public int? Size { get; private set; }
    public bool IsStar
    {
      get { return Fill; }
    }
    public bool IsAuto
    {
      get { return !Fill && Size == null; }
    }
    public bool IsFixed
    {
      get { return !Fill && Size != null; }
    }
  }

  internal sealed class iOSLayoutTableColumn
  {
    public iOSLayoutTableColumn(iOSLayoutTable Table, iOSLayoutElement Element, bool Fill, int? Value)
    {
      this.Table = Table;
      this.Element = Element;
      this.Length = new iOSLayoutTableLength(Fill, Value);
    }

    public iOSLayoutElement Element { get; private set; }
    public iOSLayoutTableLength Length { get; private set; }

    internal nfloat TempStart;
    internal nfloat TempLength;

    internal CGSize CalculateSize(CGSize MaxSize)
    {
      var Result = Element != null ? Element.CalculateSize(MaxSize) : CGSize.Empty;

      if (Length.IsFixed)
        Result.Width = Length.Size.Value;

      foreach (var Cell in Table.CellList.Where(C => C.Column == this))
      {
        if (Cell.Element != null)
        {
          var CellSize = Cell.Element.CalculateSize(MaxSize);

          if (!Length.IsFixed && CellSize.Width > Result.Width)
            Result.Width = CellSize.Width;

          if (Cell.Row.Length.IsFixed)
            Result.Height += Cell.Row.Length.Size.Value;
          else
            Result.Height += CellSize.Height;
        }
      }

      return Result;
    }

    private iOSLayoutTable Table;
  }

  internal sealed class iOSLayoutTableRow
  {
    public iOSLayoutTableRow(iOSLayoutTable Table, iOSLayoutElement Element, bool Fill, int? Value)
    {
      this.Table = Table;
      this.Element = Element;
      this.Length = new iOSLayoutTableLength(Fill, Value);
    }

    public iOSLayoutElement Element { get; private set; }
    public iOSLayoutTableLength Length { get; private set; }

    internal nfloat TempStart;
    internal nfloat TempLength;

    internal CGSize CalculateSize(CGSize MaxSize)
    {
      var Result = Element != null ? Element.CalculateSize(MaxSize) : CGSize.Empty;

      if (Length.IsFixed)
        Result.Height = Length.Size.Value;

      foreach (var Cell in Table.CellList.Where(C => C.Row == this))
      {
        if (Cell.Element != null)
        {
          var AvailableSize = new CGSize(Math.Max(MaxSize.Width - Result.Width, 0), MaxSize.Height);

          var CellSize = Cell.Element.CalculateSize(AvailableSize);

          if (!Length.IsFixed && CellSize.Height > Result.Height)
            Result.Height = CellSize.Height;

          if (Cell.Column.Length.IsFixed)
            Result.Width += Cell.Column.Length.Size.Value;
          else
            Result.Width += CellSize.Width;
        }
      }

      return Result;
    }

    private iOSLayoutTable Table;
  }

  internal sealed class iOSLayoutTableCell
  {
    public iOSLayoutTableCell(iOSLayoutTableColumn Column, iOSLayoutTableRow Row, iOSLayoutElement Element)
    {
      this.Column = Column;
      this.Row = Row;
      this.Element = Element;
    }

    public iOSLayoutTableColumn Column { get; private set; }
    public iOSLayoutTableRow Row { get; private set; }
    public iOSLayoutElement Element { get; private set; }
  }

  internal sealed class iOSLayoutScroll : UIScrollView, iOSLayoutElement
  {
    public iOSLayoutScroll()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.Border = new iOSLayoutBorder(this);

      this.KeyboardNotificationObserverList = new List<NSObject>();

      foreach (var Name in new[] { UIKeyboard.WillShowNotification, UIKeyboard.WillHideNotification, UIKeyboard.WillChangeFrameNotification, UIKeyboard.DidShowNotification, UIKeyboard.DidHideNotification, UIKeyboard.DidChangeFrameNotification })
      {
        KeyboardNotificationObserverList.Add(NSNotificationCenter.DefaultCenter.AddObserver(Name, (Notification) =>
        {
          var AnimationCurveValue = Notification.UserInfo[UIKeyboard.AnimationCurveUserInfoKey] as NSNumber;
          var AnimationDurationValue = Notification.UserInfo[UIKeyboard.AnimationDurationUserInfoKey] as NSNumber;
          var KeyboardEndValue = Notification.UserInfo[UIKeyboard.FrameEndUserInfoKey] as NSValue;
          var IsLocalValue = Notification.UserInfo[UIKeyboard.IsLocalUserInfoKey] as NSNumber;

          var IsLocal = IsLocalValue == null ? true : IsLocalValue.BoolValue;

          if (AnimationCurveValue != null && AnimationDurationValue != null && KeyboardEndValue != null && IsLocal)
          {
            var AnimationCurve = (UIViewAnimationCurve)(ulong)AnimationCurveValue.UnsignedLongValue;
            var AnimationDuration = AnimationDurationValue.DoubleValue;
            var KeyboardEndRect = ConvertRectFromView(KeyboardEndValue.CGRectValue, null);

            var MaxY = Bounds.GetMaxY();
            var NewInsets = UIEdgeInsets.Zero;

            // If keyboard overlaps, but doesn't completely obscure scroll view...
            if (KeyboardEndRect.Y < MaxY && MaxY <= KeyboardEndRect.GetMaxY() && MaxY - KeyboardEndRect.Y < Bounds.Height)
            {
              NewInsets = new UIEdgeInsets(0, 0, MaxY - KeyboardEndRect.Y, 0);
            }
            else if (KeyboardEndRect.Y <= Bounds.Y && Bounds.Y < KeyboardEndRect.GetMaxY() && KeyboardEndRect.GetMaxY() - Bounds.Y < Bounds.Height)
            {
              NewInsets = new UIEdgeInsets(KeyboardEndRect.GetMaxY() - Bounds.Y, 0, 0, 0);
            }

            UIView.BeginAnimations("au.com.kestral.ioslayoutscroll-keyboard");
            UIView.SetAnimationDuration(AnimationDuration);
            UIView.SetAnimationCurve(AnimationCurve);
            UIView.SetAnimationBeginsFromCurrentState(true);

            ContentInset = NewInsets;
            ScrollIndicatorInsets = NewInsets;

            UIView.CommitAnimations();
          }
        }));
      }
    }
    ~iOSLayoutScroll()
    {
      foreach (var Observer in KeyboardNotificationObserverList)
        NSNotificationCenter.DefaultCenter.RemoveObserver(Observer);
    }

    public iOSLayoutBorder Border { get; private set; }

    public void SetOrientation(iOSLayoutOrientation Orientation)
    {
      if (Orientation != this.Orientation)
      {
        this.Orientation = Orientation;

        this.Arrange();
      }
    }
    public void SetContentElement(iOSLayoutElement Content)
    {
      if (Content != this.Content)
      {
        if (this.Content != null)
          this.SafeRemoveView(this.Content.View);

        this.Content = Content;

        if (this.Content != null)
          this.SafeAddView(this.Content.View);

        this.Arrange();
      }
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      if (Content != null)
      {
        var LayoutSize = Content.CalculateSize(Bounds.Size);

        if (Orientation == iOSLayoutOrientation.Vertical)
        {
          LayoutSize.Width = Bounds.Width;

          var LayoutHeight = Bounds.Height;
          if (LayoutSize.Height < LayoutHeight)
            LayoutSize.Height = LayoutHeight;
        }
        else if (Orientation == iOSLayoutOrientation.Horizontal)
        {
          LayoutSize.Height = Bounds.Height;

          var LayoutWidth = Bounds.Width;
          if (LayoutSize.Width < LayoutWidth)
            LayoutSize.Width = LayoutWidth;
        }

        this.ContentSize = LayoutSize;

        Content.View.Frame = LayoutMargins.InsetRect(new CGRect(0, 0, LayoutSize.Width, LayoutSize.Height));
      }
      else
      {
        ContentSize = CGSize.Empty;
      }

      Border.Layout();
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = Content != null ? Content.CalculateSize(this.FitToSize(MaxSize)) : CGSize.Empty;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private iOSLayoutElement Content;
    private iOSLayoutOrientation Orientation;
    private List<NSObject> KeyboardNotificationObserverList;
  }

  internal sealed class iOSLayoutDatePicker : UIDatePicker, iOSLayoutElement
  {
    public iOSLayoutDatePicker()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.Border = new iOSLayoutBorder(this);
    }

    public iOSLayoutBorder Border { get; private set; }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      Border.Layout();
    }

    UIView iOSLayoutElement.View
    {
      get { return this; }
    }
    CGSize iOSLayoutElement.CalculateSize(CGSize MaxSize)
    {
      var Result = IntrinsicContentSize;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }
  }

  internal sealed class iOSPopoverViewController : UIViewController
  {
    public iOSPopoverViewController()
    {
      this.ModalPresentationStyle = UIKit.UIModalPresentationStyle.Popover;
    }

    public event Action LoadEvent;

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();

      if (LoadEvent != null)
        LoadEvent();
    }
    public override void ViewDidLayoutSubviews()
    {
      base.ViewDidLayoutSubviews();

      if (Content != null)
        Content.View.Frame = View.Bounds;
    }

    public void SetContentElement(iOSLayoutElement Content)
    {
      if (this.Content != Content)
      {
        if (this.Content != null)
          View.SafeRemoveView(this.Content.View);

        this.Content = Content;

        if (this.Content != null)
          View.SafeAddView(this.Content.View);
      }
    }

    private iOSLayoutElement Content;
  }

  internal sealed class iOSLayoutController : UIKit.UIViewController
  {
    public iOSLayoutController()
      : base()
    {
      this.LeftEdgeRecognizer = new UIKit.UIScreenEdgePanGestureRecognizer(() =>
      {
        if (LeftEdgeRecognizer.State == UIGestureRecognizerState.Recognized)
        {
          if (LeftEdgeSwipeEvent != null)
            LeftEdgeSwipeEvent();
        }
      }) { Edges = UIKit.UIRectEdge.Left, DelaysTouchesBegan = false };
      View.AddGestureRecognizer(LeftEdgeRecognizer);

      this.RightEdgeRecognizer = new UIKit.UIScreenEdgePanGestureRecognizer(() =>
      {
        if (RightEdgeRecognizer.State == UIGestureRecognizerState.Recognized)
        {
          if (RightEdgeSwipeEvent != null)
            RightEdgeSwipeEvent();
        }
      }) { Edges = UIKit.UIRectEdge.Right, DelaysTouchesBegan = false };
      View.AddGestureRecognizer(RightEdgeRecognizer);
    }

    public event Action LoadEvent;
    public event Action LeftEdgeSwipeEvent;
    public event Action RightEdgeSwipeEvent;
    public event Action MemoryWarningEvent;
    public event Action<CoreGraphics.CGSize> ResizeEvent;
    public event Func<UIKeyCommand[]> KeyQuery;
    public event Action<UIKit.UIKeyModifierFlags, string> KeyEvent;

    public override bool CanBecomeFirstResponder
    {
      get { return true; }
    }
    public override UIKeyCommand[] KeyCommands
    {
      get
      {
        if (FindFirstResponder(View) == null && KeyQuery != null)
          return KeyQuery();
        else
          return base.KeyCommands;
      }
    }
    public void SetContentElement(iOSLayoutElement Content)
    {
      if (this.Content != Content)
      {
        if (this.Content != null)
          View.SafeRemoveView(this.Content.View);

        this.Content = Content;

        if (this.Content != null)
          View.SafeAddView(this.Content.View);
      }
    }
    public void Reload()
    {
      if (IsLoaded)
        LoadInvoke();
    }

    public override void ViewDidLayoutSubviews()
    {
      base.ViewDidLayoutSubviews();

      if (Content != null)
        Content.View.Frame = View.Bounds;
    }
    public override void DidReceiveMemoryWarning()
    {
      // Release any cached data, images, etc that aren't in use.
      if (MemoryWarningEvent != null)
        MemoryWarningEvent();

      base.DidReceiveMemoryWarning();
    }
    public override void ViewDidLoad()
    {
      base.ViewDidLoad();

      //View.TranslatesAutoresizingMaskIntoConstraints = false; // NOTE: causes the background colour to be lost.
      View.LayoutMargins = UIEdgeInsets.Zero; // NOTE: this is a workaround to avoid the default leading/trailing 16pt margin.

      this.IsLoaded = true;

      LoadInvoke();
    }
    public override void ViewWillAppear(bool animated)
    {
      base.ViewWillAppear(animated);
    }
    public override void ViewDidAppear(bool animated)
    {
      base.ViewDidAppear(animated);

      BecomeFirstResponder();
    }
    public override void ViewWillDisappear(bool animated)
    {
      base.ViewWillDisappear(animated);
    }
    public override void ViewDidDisappear(bool animated)
    {
      base.ViewDidDisappear(animated);
    }
    public override void ViewWillTransitionToSize(CoreGraphics.CGSize toSize, IUIViewControllerTransitionCoordinator coordinator)
    {
      base.ViewWillTransitionToSize(toSize, coordinator);

      if (ResizeEvent != null)
        ResizeEvent(toSize);
    }
    public override bool ShouldAutorotateToInterfaceOrientation(UIInterfaceOrientation toInterfaceOrientation)
    {
      return true;
    }
    public override void WillAnimateRotation(UIInterfaceOrientation toInterfaceOrientation, double duration)
    {
      base.WillAnimateRotation(toInterfaceOrientation, duration);
    }

    private static bool UserInterfaceIdiomIsPhone
    {
      get { return UIKit.UIDevice.CurrentDevice.UserInterfaceIdiom == UIKit.UIUserInterfaceIdiom.Phone; }
    }

    [Foundation.Export("KeyDown:")]
    private void KeyDown(UIKeyCommand Command)
    {
      if (KeyEvent != null)
        KeyEvent(Command.ModifierFlags, Command.Input);
    }
    /* // TODO: track both key up/down.
    [Foundation.Export("KeyUp:")]
    private void KeyDown(UIKeyCommand Command)
    {
      if (KeyEvent != null)
        KeyEvent(Command.ModifierFlags, Command.Input);
    }
    */
    private UIView FindFirstResponder(UIView CurrentView)
    {
      if (CurrentView.IsFirstResponder)
        return CurrentView;

      foreach (var Subview in CurrentView.Subviews)
      {
        var ResultView = FindFirstResponder(Subview);

        if (ResultView != null)
          return ResultView;
      }

      return null;
    }

    private void LoadInvoke()
    {
      if (LoadEvent != null)
        LoadEvent();
    }

    private bool IsLoaded;
    private iOSLayoutElement Content;
    private UIScreenEdgePanGestureRecognizer LeftEdgeRecognizer;
    private UIScreenEdgePanGestureRecognizer RightEdgeRecognizer;
  }

  internal sealed class iOSNavigationControllerDelegate : UINavigationControllerDelegate
  {
    public override void DidShowViewController(UINavigationController navigationController, UIViewController viewController, bool animated)
    {
      if (animated)
      {
        foreach (var child in navigationController.ChildViewControllers)
        {
          if (child != viewController)
            child.RemoveFromParentViewController();
        }
      }
    }
  }

  [Foundation.Register("iOSAppDelegate")]
  internal sealed class iOSAppDelegate : UIKit.UIApplicationDelegate
  {
    public override UIKit.UIWindow Window { get; set; }

    public override void OnActivated(UIApplication application)
    {
    }
    public override void OnResignActivation(UIApplication application)
    {
      iOSEngine.Pausing();
    }
    public override void DidEnterBackground(UIApplication application)
    {
    }
    public override void WillEnterForeground(UIApplication application)
    {
      iOSEngine.Resuming();
    }
    public override void WillTerminate(UIApplication application)
    {
      iOSEngine.Terminating();
    }
    public override bool FinishedLaunching(UIKit.UIApplication app, Foundation.NSDictionary options)
    {
      this.Window = iOSEngine.Launching();

      return true;
    }
    public override void ReceiveMemoryWarning(UIApplication application)
    {
      // TODO: what to do?
    }

    internal static iOSEngine iOSEngine;
  }

  internal static class iOSLayoutFoundation
  {
    public static void SafeAddView(this UIView ParentView, UIView ChildView)
    {
      if (ChildView.Superview != ParentView)
        ParentView.AddSubview(ChildView);
    }
    public static void SafeRemoveView(this UIView ParentView, UIView ChildView)
    {
      if (ChildView.Superview == ParentView)
        ChildView.RemoveFromSuperview();
    }

    public static CGSize FitToSize(this UIView iOSView, CGSize MaxSize)
    {
      var Result = MaxSize;
      if (Result.Width != UIView.NoIntrinsicMetric)
        Result.Width -= iOSView.LayoutMargins.Left + iOSView.LayoutMargins.Right;

      if (Result.Height != UIView.NoIntrinsicMetric)
        Result.Height -= iOSView.LayoutMargins.Top + iOSView.LayoutMargins.Bottom;

      return Result;
    }
    public static void Arrange(this UIView iOSView)
    {
      // NOTE: setting needs layout on all parents is necessary when a panel is hidden/removed from the visual tree.

      var iOSCurrent = iOSView;
      while (iOSCurrent != null)
      {
        iOSCurrent.SetNeedsLayout();

        iOSCurrent = iOSCurrent.Superview;
      }
    }
    public static void FadeIn(this UIView iOSView, double Duration = 0.3, double Delay = 0.0, Action CompleteAction = null)
    {
      Fade(iOSView, MinAlpha, MaxAlpha, Duration, Delay, CompleteAction);
    }
    public static void FadeOut(this UIView iOSView, double Duration = 0.3, double Delay = 0.0, Action CompleteAction = null)
    {
      Fade(iOSView, MaxAlpha, MinAlpha, Duration, Delay, CompleteAction);
    }
    public static void SlideInLeft(this UIView iOSView, double Duration = 0.3, Action CompleteAction = null)
    {
      SlideHorizontal(iOSView, true, true, Duration, CompleteAction);
    }
    public static void SlideOutRight(this UIView iOSView, double Duration = 0.3, Action CompleteAction = null)
    {
      SlideHorizontal(iOSView, false, false, Duration, CompleteAction);
    }
    public static void Fade(this UIView iOSView, nfloat FromAlpha, nfloat ToAlpha, double Duration = 0.3, double Delay = 0.0, Action CompleteAction = null, bool UserInteractions = false)
    {
      iOSView.Alpha = FromAlpha;
      iOSView.Transform = CGAffineTransform.MakeIdentity();
      UIView.Animate(Duration, Delay, UserInteractions ? UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.AllowUserInteraction : UIViewAnimationOptions.CurveEaseInOut, () => iOSView.Alpha = ToAlpha, CompleteAction);
    }
    public static float GetPreferredContentLightness(this UIView iOSView)
    {
      var ShadeStack = new Stack<Tuple<float, float>>();

      var Parent = iOSView;

      while (Parent != null)
      {
        if (Parent.BackgroundColor != null)
        {
          nfloat White, Alpha;
          Parent.BackgroundColor.GetWhite(out White, out Alpha);

          ShadeStack.Push(new Tuple<float, float>((float)White, (float)Alpha));

          if (Alpha == 1f)
            break;
        }

        Parent = Parent.Superview;
      }

      var ResultShade = 0f;

      while (ShadeStack.Any())
      {
        var NextShade = ShadeStack.Pop();
        ResultShade = (ResultShade * (1 - NextShade.Item2)) + (NextShade.Item1 * NextShade.Item2);
      }

      if (ResultShade == 1f)
        return 0f;
      else if (ResultShade < 0.5f)
        return 1f;
      else
        return 0.5f;
    }
    public static string ChildHierarchyToString(this UIView RootView)
    {
      var Viewer = new HierarchyWriter<UIView>(V => V.Subviews.Length, V => V.Subviews);
      return Viewer.WriteToString(RootView);
    }
    public static UIView HitTestFindTouchable(this UIView View, UIView SuperHitTestResult)
    {
      if (SuperHitTestResult == View)
      {
        while (SuperHitTestResult != null)
        {
          SuperHitTestResult = SuperHitTestResult.Superview;

          if (SuperHitTestResult is ITouchable)
            return SuperHitTestResult;
        }

        return View;
      }

      return SuperHitTestResult;
    }
    public static UIView HitTestFindNothing(this UIView View, UIView SuperHitTestResult)
    {
      // ignore any user interaction on the layout view itself.
      if (SuperHitTestResult == View)
        return null;
      else
        return SuperHitTestResult;
    }
    private static void SlideHorizontal(this UIView iOSView, bool IsIn, bool FromLeft, double Duration = 0.3, Action CompleteAction = null)
    {
      var MinTransform = CGAffineTransform.MakeTranslation((FromLeft ? -1 : 1) * iOSView.Bounds.Width, 0);
      var MaxTransform = CGAffineTransform.MakeIdentity();

      iOSView.Transform = IsIn ? MinTransform : MaxTransform;
      UIView.Animate(Duration, 0, UIViewAnimationOptions.CurveEaseInOut, () => iOSView.Transform = IsIn ? MaxTransform : MinTransform, CompleteAction);
    }

    private static readonly nfloat MinAlpha = (nfloat)0.0f;
    private static readonly nfloat MaxAlpha = (nfloat)1.0f;
  }
}