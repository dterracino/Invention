﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using Security;
using Foundation;
using System.Diagnostics;

namespace Inv
{
  internal class iOSVault
  {
    public iOSVault()
    {
      this.ServiceId = Foundation.NSBundle.MainBundle.BundleIdentifier ?? "Invention";
    }

    public IEnumerable<Secret> LoadSecrets(Inv.Vault Vault)
    {
      var query = new SecRecord(SecKind.GenericPassword);
      query.Service = ServiceId;

      SecStatusCode result;
      var records = SecKeyChain.QueryAsRecord(query, 1000, out result);

      return records != null ? records.Select(R =>
      {
        var Result = new Secret(Vault, R.Account);

        TranslateSecRecord(Result, R);

        return Result;
      }).ToList() : new List<Secret>();
    }
    public void Load(Secret Secret)
    {
      var SecRecord = GetSecRecord(Secret.Name);

      if (SecRecord != null)
        TranslateSecRecord(Secret, SecRecord);
      else
        Secret.Properties.Clear();
    }
    public void Save(Secret Secret)
    {
      var statusCode = SecStatusCode.Success;
      var serializedSecret = Secret.Serialize();
      var data = NSData.FromString(serializedSecret, NSStringEncoding.UTF8);

      // Remove any existing record
      var existing = GetSecRecord(Secret.Name);

      if (existing != null)
      {
        var query = new SecRecord(SecKind.GenericPassword);
        query.Service = ServiceId;
        query.Account = Secret.Name;

        statusCode = SecKeyChain.Remove(query);
        if (statusCode != SecStatusCode.Success)
          throw new Exception("Could not replace secret in keychain: " + statusCode);
      }

      // Add this record
      var record = new SecRecord(SecKind.GenericPassword);
      record.Service = ServiceId;
      record.Account = Secret.Name;
      record.Generic = data;
      record.Accessible = SecAccessible.WhenUnlocked;

      statusCode = SecKeyChain.Add(record);

      if (statusCode != SecStatusCode.Success)
      {
        Debug.WriteLine("Could not save secret to keychain: " + statusCode);
      }
    }
    public void Delete(Secret Secret)
    {
      var query = new SecRecord(SecKind.GenericPassword);
      query.Service = ServiceId;
      query.Account = Secret.Name;

      var statusCode = SecKeyChain.Remove(query);

      if (statusCode != SecStatusCode.Success)
      {
        Debug.WriteLine("Could not delete secret from keychain: " + statusCode);
      }
    }

    private Security.SecRecord GetSecRecord(string Name)
    {
      var query = new SecRecord(SecKind.GenericPassword);
      query.Service = ServiceId;
      query.Account = Name;

      SecStatusCode result;
      var record = SecKeyChain.QueryAsRecord(query, out result);

      return record;
    }
    private void TranslateSecRecord(Secret Secret, SecRecord r)
    {
      var serializedData = NSString.FromData(r.Generic, NSStringEncoding.UTF8);

      Secret.Deserialize(serializedData);
    }

    private string ServiceId;
  }
}
