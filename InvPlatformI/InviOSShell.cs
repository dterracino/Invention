﻿/*! 16 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Inv.Support;
using MobileCoreServices;

namespace Inv
{
  public static class iOSShell
  {
    public static bool Update(iOSSelfUpdate SelfUpdate)
    {
      UpgradeSelfUpdate = SelfUpdate;

      if (SelfUpdate == null)
        return false;

#if DEBUG
      System.Net.ServicePointManager.ServerCertificateValidationCallback += (Sender, Certificate, Chain, PolicyErrors) => true;
#endif

      try
      {
        var WebBroker = new Inv.WebBroker(SelfUpdate.RemoteHostAddress);

        var LocalVersion = SelfUpdate.LocalVersionText;
        var RemoteVersion = WebBroker.GetTextJsonObject<string>(SelfUpdate.RemoteVersionResource);

        var Result = LocalVersion != RemoteVersion;

        if (Result)
          UIKit.UIApplication.Main(new string[] { }, null, typeof(UpdateAppDelegate));

        return Result;
      }
      catch (Exception Exception)
      {
        Debug.WriteLine(Exception.Message);
        // NOTE: could not reach upgrade server.

        return false;
      }
    }
    public static void Run(Action<Inv.Application> ApplicationAction)
    {
      RunningApplication = new Inv.Application();
      AppDomain.CurrentDomain.UnhandledException += UnhandledException;
      System.Threading.Tasks.TaskScheduler.UnobservedTaskException += UnobservedTaskException;
      try
      {
        var Engine = new iOSEngine(RunningApplication);
        ApplicationAction(RunningApplication);
        Engine.Run();
      }
      finally
      {
        AppDomain.CurrentDomain.UnhandledException -= UnhandledException;
        System.Threading.Tasks.TaskScheduler.UnobservedTaskException -= UnobservedTaskException;
        RunningApplication = null;
      }
    }

    internal static iOSSelfUpdate UpgradeSelfUpdate { get; private set; }

    private static void UnobservedTaskException(object Sender, System.Threading.Tasks.UnobservedTaskExceptionEventArgs Event)
    {
      var Application = RunningApplication;
      var Exception = Event.Exception;
      if (Application != null && Exception != null)
        Application.HandleExceptionInvoke(Exception);
    }
    private static void UnhandledException(object Sender, UnhandledExceptionEventArgs Event)
    {
      var Application = RunningApplication;
      var Exception = Event.ExceptionObject as Exception;
      if (Application != null && Exception != null)
        Application.HandleExceptionInvoke(Exception);
    }

    private static Application RunningApplication;
  }

  internal sealed class iOSPlatform : Inv.Platform
  {
    public iOSPlatform(iOSEngine Engine)
    {
      this.Engine = Engine;
      this.Vault = new iOSVault();
      this.CurrentProcess = System.Diagnostics.Process.GetCurrentProcess();

      // NOTE: required to workaround AVAudioPlayer problems.
      ObjCRuntime.Class.ThrowOnInitFailure = false;

      // This allows music from other apps to continue playing.
      AVFoundation.AVAudioSession.SharedInstance().SetCategory(AVFoundation.AVAudioSession.CategoryAmbient);
    }

    int Platform.ThreadAffinity()
    {
      return System.Threading.Thread.CurrentThread.ManagedThreadId;
    }
    string Platform.CalendarTimeZoneName()
    {
      return Foundation.NSTimeZone.LocalTimeZone.Name;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      Engine.ShowCalendarPicker(CalendarPicker);
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      var Result = MessageUI.MFMailComposeViewController.CanSendMail;

      if (Result)
      {
        var MailController = new MessageUI.MFMailComposeViewController();
        MailController.SetToRecipients(EmailMessage.GetTos().Select(T => T.Address).ToArray());
        MailController.SetSubject(EmailMessage.Subject ?? "");
        MailController.SetMessageBody(EmailMessage.Body ?? "", false);

        foreach (var Attachment in EmailMessage.GetAttachments())
        {
          var FileData = Foundation.NSData.FromFile(SelectFilePath(Attachment.File));

          // TODO: better mime type analysis.
          string MimeType;

          switch (Attachment.File.Extension.ToLower())
          {
            case ".log":
            case ".txt":
              MimeType = "text/plain";
              break;

            case ".png":
              MimeType = "image/png";
              break;

            case ".jpg":
            case ".jpeg":
              MimeType = "image/jpeg";
              break;

            default:
              MimeType = "application/octect-stream";
              break;
          }

          MailController.AddAttachmentData(FileData, MimeType, Attachment.File.Name);
        }

        MailController.Finished += (Sender, Event) =>
        {
          Console.WriteLine(Event.Result.ToString());
          Event.Controller.DismissViewController(true, null);
        };
        Engine.iOSWindow.RootViewController.PresentViewController(MailController, true, null);
      }

      return Result;
    }
    bool Platform.PhoneIsSupported
    {
      get { return UIKit.UIApplication.SharedApplication.CanOpenUrl(new Foundation.NSUrl("tel:")); }
    }
    void Platform.PhoneDial(string PhoneNumber)
    {
      if (!UIKit.UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl("tel:" + PhoneNumber.Strip(' '))))
        Engine.Toast(null, "Unable to dial this number " + PhoneNumber, "OK");
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
      if (!UIKit.UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl("sms:" + PhoneNumber.Strip(' '))))
        Engine.Toast(null, "Unable to SMS this number " + PhoneNumber, "OK");
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).LastWriteTimeUtc;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      System.IO.File.SetLastWriteTimeUtc(SelectFilePath(File), Timestamp);
    }
    Stream Platform.DirectoryCreateFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Create, FileAccess.Write, FileShare.Read, 65536);
    }
    Stream Platform.DirectoryAppendFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Append, FileAccess.Write, FileShare.Read, 65536);
    }
    Stream Platform.DirectoryOpenFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Open, FileAccess.Read, FileShare.Read, 65536);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return System.IO.File.Exists(SelectFilePath(File));
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      System.IO.File.Delete(SelectFilePath(File));
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Copy(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Move(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return new DirectoryInfo(SelectFolderPath(Folder)).GetFiles(FileMask).Select(F => Folder.NewFile(F.Name));
    }
    Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return new System.IO.FileStream(SelectAssetPath(Asset), System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read, 65536);
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      return System.IO.File.Exists(SelectAssetPath(Asset));
    }
    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      switch (FilePicker.Type)
      {
        case FileType.Image:
          var Permitted = Foundation.NSBundle.MainBundle.ObjectForInfoDictionary("NSPhotoLibraryUsageDescription");
          
          if (Permitted == null)
          {
            Engine.Toast(null, "The picture library cannot be accessed without the required permission. " +
#if DEBUG
              "Please install this key into your Info.plist: " +
              "<key>NSPhotoLibraryUsageDescription</key> " + 
              "<string>$(PRODUCT_NAME) needs access to use your photo library</string>"
#else
              "Please contact the developer for assistance."
#endif 
            , "OK");
            return;
          }

          var ImagePicker = new UIKit.UIImagePickerController();
          ImagePicker.SourceType = UIKit.UIImagePickerControllerSourceType.PhotoLibrary;
          //ImagePicker.MediaTypes = UIKit.UIImagePickerController.AvailableMediaTypes(UIKit.UIImagePickerControllerSourceType.PhotoLibrary); // NOTE: default is a 'still image' only.
          ImagePicker.FinishedPickingMedia += WeakHelpers.WeakEventHandlerWrapper<iOSEngine, DirectoryFilePicker, UIKit.UIImagePickerController, UIKit.UIImagePickerMediaPickedEventArgs>(Engine, FilePicker, ImagePicker, (engine, filePicker, imagePicker, Sender, Event) =>
          {
            try
            {
              var iOSImage = Event.Info[UIKit.UIImagePickerController.OriginalImage] as UIKit.UIImage;
              if (iOSImage != null)
                engine.Guard(() => filePicker.SelectInvoke(new Inv.Binary(iOSImage.AsPNG().ToArray(), ".png")));

              imagePicker.DismissModalViewController(true);
            }
            catch (Exception Exception)
            {
              engine.Guard(() =>
              {
                engine.InvApplication.HandleExceptionInvoke(Exception);

                filePicker.CancelInvoke(); // the select failed.
              });
            }
          });
          ImagePicker.Canceled += WeakHelpers.WeakEventHandlerWrapper<iOSEngine, DirectoryFilePicker, UIKit.UIImagePickerController>(Engine, FilePicker, ImagePicker, (engine, filePicker, imagePicker, Sender, Event) =>
          {
            try
            {
              Engine.Guard(() => FilePicker.CancelInvoke());

              ImagePicker.DismissModalViewController(true);
            }
            catch (Exception Exception)
            {
              Engine.Guard(() =>
              {
                Engine.InvApplication.HandleExceptionInvoke(Exception);
              });
            }
          });
          
          Engine.iOSWindow.RootViewController.PresentModalViewController(ImagePicker, true); 
          break;

        case FileType.Any:
          throw new NotImplementedException("File picking not implemented on iOS");
        //  var AllowedUTIs = new string[] { UTType.UTF8PlainText, UTType.PlainText, UTType.RTF, UTType.Text };

        //  var AnyFilePicker = new UIKit.UIDocumentPickerViewController(AllowedUTIs, UIKit.UIDocumentPickerMode.Import);
        //  AnyFilePicker.DidPickDocument += (DocumentSender, DocumentEvent) =>
        //  {
        //    try
        //    {
        //      var securityEnabled = DocumentEvent.Url.StartAccessingSecurityScopedResource();
        //      var Data = Foundation.NSData.FromUrl(DocumentEvent.Url);

        //      var Extension = DocumentEvent.Url.PathExtension;
        //      if (!Extension.StartsWith("."))
        //        Extension = "." + Extension;

        //      var Binary = new Inv.Binary(Data.ToArray(), Extension);

        //      Engine.Guard(() => FilePicker.SelectInvoke(Binary));
        //      //file = new ImportedFile(DocumentEvent.Url.LastPathComponent, data.ToArray());
        //    }
        //    catch (Exception Exception)
        //    {
        //      Engine.Guard(() =>
        //      {
        //        Engine.InvApplication.HandleExceptionInvoke(Exception);

        //        FilePicker.CancelInvoke(); // the select failed.
        //      });
        //    }
        //    finally
        //    {
        //      DocumentEvent.Url.StopAccessingSecurityScopedResource();
        //    }
        //  };
        //  AnyFilePicker.WasCancelled += (Sender, Event) =>
        //  {
        //    try
        //    {
        //      Engine.Guard(() => FilePicker.CancelInvoke());

        //      AnyFilePicker.DismissModalViewController(true);
        //    }
        //    catch (Exception Exception)
        //    {
        //      Engine.Guard(() =>
        //      {
        //        Engine.InvApplication.HandleExceptionInvoke(Exception);
        //      });
        //    }
        //  };

        //  Engine.iOSWindow.RootViewController.PresentModalViewController(AnyFilePicker, true);
          //break;

        default:
          throw new Exception("FileType not handled: " + FilePicker.Type);
      }
    }
    bool Platform.LocationIsSupported
    {
      get { return UIKit.UIDevice.CurrentDevice.CheckSystemVersion(6, 0) && CoreLocation.CLLocationManager.LocationServicesEnabled; }
    }
    void Platform.LocationLookup(LocationLookup LocationLookup)
    {
      var iOSGeocoder = new CoreLocation.CLGeocoder();
      iOSGeocoder.ReverseGeocodeLocation(new CoreLocation.CLLocation(LocationLookup.Coordinate.Latitude, LocationLookup.Coordinate.Longitude), (PlacemarkArray, Error) =>
      {
        Engine.Guard(() => LocationLookup.SetPlacemarks(PlacemarkArray.Select(P => new Inv.Placemark
        (
          Name: P.Name,
          Locality: P.Locality,
          SubLocality: P.SubLocality,
          PostalCode: P.PostalCode,
          AdministrativeArea: P.AdministrativeArea,
          SubAdministrativeArea: P.SubAdministrativeArea,
          CountryName: P.Country,
          CountryCode: P.IsoCountryCode,
          Latitude: P.Location.Coordinate.Latitude,
          Longitude: P.Location.Coordinate.Longitude
        ))));
      });
    }
    void Platform.AudioPlaySound(Inv.Sound Sound, float Volume, float Rate)
    {
      Engine.PlaySound(Sound, Volume, Rate, false);
    }
    void Platform.AudioPlayClip(AudioClip Clip)
    {
      Clip.Node = Engine.PlaySound(Clip.Sound, Clip.Volume, Clip.Rate, Clip.Loop);
    }
    void Platform.AudioStopClip(AudioClip Clip)
    {
      var iOSSound = Clip.Node as AVFoundation.AVAudioPlayer;
      if (iOSSound != null)
      {
        Clip.Node = null;
        iOSSound.Stop();
      }
    }
    void Platform.WindowBrowse(Inv.File File)
    {
      var Popup = new UIKit.UIDocumentInteractionController();
      Popup.Url = new Uri("file://" + SelectFilePath(File));
      Popup.DidDismissOptionsMenu += (s, a) => Popup = null;
      Popup.PresentOptionsMenu(new CoreGraphics.CGRect(0, Engine.iOSWindow.Bounds.Height - 300, Engine.iOSWindow.Bounds.Width, 300), Engine.iOSWindow.RootViewController.View, true);
    }
    void Platform.WindowPost(Action Action)
    {
      Engine.iOSWindow.BeginInvokeOnMainThread(Action);
    }
    void Platform.WindowCall(Action Action)
    {
      Engine.iOSWindow.InvokeOnMainThread(Action);
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      // NOTE: CurrentProcess.Refresh() doesn't seem to be required here on iOS.
      return CurrentProcess.PrivateMemorySize64;
    }
    void Platform.ProcessMemoryReclamation()
    {
      Engine.Reclamation();
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      var TcpClient = new Inv.Tcp.Client(WebClient.Host, WebClient.Port, WebClient.CertHash);
      TcpClient.Connect();

      WebClient.Node = TcpClient;
      WebClient.SetStreams(TcpClient.Stream, TcpClient.Stream);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
      var TcpClient = (Inv.Tcp.Client)WebClient.Node;
      if (TcpClient != null)
      {
        WebClient.Node = null;
        WebClient.SetStreams(null, null);

        TcpClient.Disconnect();
      }
    }
    void Platform.WebServerConnect(WebServer WebServer)
    {
      var TcpServer = new Inv.Tcp.Server(WebServer.Host, WebServer.Port, WebServer.CertHash);
      TcpServer.AcceptEvent += (TcpChannel) => WebServer.AcceptChannel(TcpChannel, TcpChannel.Stream, TcpChannel.Stream);
      TcpServer.RejectEvent += (TcpChannel) => WebServer.RejectChannel(TcpChannel);

      WebServer.Node = TcpServer;
      WebServer.DropDelegate = (Node) => ((Inv.Tcp.Channel)Node).Drop();

      TcpServer.Connect();
    }
    void Platform.WebServerDisconnect(WebServer WebServer)
    {
      var TcpServer = (Inv.Tcp.Server)WebServer.Node;
      if (TcpServer != null)
      {
        TcpServer.Disconnect();

        WebServer.Node = null;
        WebServer.DropDelegate = null;
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      UIKit.UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl(Uri.AbsoluteUri));
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      UIKit.UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl("itms-apps://itunes.apple.com/app/id" + AppleiTunesID));
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      Vault.Load(Secret);
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      Vault.Save(Secret);
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      Vault.Delete(Secret);
    }

    private string SelectAssetPath(Asset Asset)
    {
      return Foundation.NSBundle.MainBundle.PathForResource(System.IO.Path.GetFileNameWithoutExtension(Asset.Name), System.IO.Path.GetExtension(Asset.Name));
    }
    private string SelectFilePath(File File)
    {
      return Path.Combine(SelectFolderPath(File.Folder), File.Name);
    }
    private string SelectFolderPath(Folder Folder)
    {
      // /Library/ is the top-level directory for any files that are not user data files - ideal for application data files.
      var Result = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..", "Library");

      if (Folder.Name != null)
        Result = System.IO.Path.Combine(Result, Folder.Name);

      System.IO.Directory.CreateDirectory(Result);

      return Result;
    }

    private iOSEngine Engine;
    private System.Diagnostics.Process CurrentProcess;
    private iOSVault Vault;
  }

  internal static class iOSKeyboard
  {
    static iOSKeyboard()
    {
      var ForwardDictionary = new Dictionary<Inv.Key, string>()
      {
        { Inv.Key.n0, "0" },
        { Inv.Key.n1, "1" },
        { Inv.Key.n2, "2" },
        { Inv.Key.n3, "3" },
        { Inv.Key.n4, "4" },
        { Inv.Key.n5, "5" },
        { Inv.Key.n6, "6" },
        { Inv.Key.n7, "7" },
        { Inv.Key.n8, "8" },
        { Inv.Key.n9, "9" },

        { Inv.Key.A, "A" },
        { Inv.Key.B, "B" },
        { Inv.Key.C, "C" },
        { Inv.Key.D, "D" },
        { Inv.Key.E, "E" },
        { Inv.Key.F, "F" },
        { Inv.Key.G, "G" },
        { Inv.Key.H, "H" },
        { Inv.Key.I, "I" },
        { Inv.Key.J, "J" },
        { Inv.Key.K, "K" },
        { Inv.Key.L, "L" },
        { Inv.Key.M, "M" },
        { Inv.Key.N, "N" },
        { Inv.Key.O, "O" },
        { Inv.Key.P, "P" },
        { Inv.Key.Q, "Q" },
        { Inv.Key.R, "R" },
        { Inv.Key.S, "S" },
        { Inv.Key.T, "T" },
        { Inv.Key.U, "U" },
        { Inv.Key.V, "V" },
        { Inv.Key.W, "W" },
        { Inv.Key.X, "X" },
        { Inv.Key.Y, "Y" },
        { Inv.Key.Z, "Z" },

        { Inv.Key.F1, "F1" },
        { Inv.Key.F2, "F2" },
        { Inv.Key.F3, "F3" },
        { Inv.Key.F4, "F4" },
        { Inv.Key.F5, "F5" },
        { Inv.Key.F6, "F6" },
        { Inv.Key.F7, "F7" },
        { Inv.Key.F8, "F8" },
        { Inv.Key.F9, "F9" },
        { Inv.Key.F10, "F10" },
        { Inv.Key.F11, "F11" },
        { Inv.Key.F12, "F12" },

        { Inv.Key.Escape, UIKit.UIKeyCommand.Escape },
        { Inv.Key.Enter, "\r" },
        { Inv.Key.Tab, "\t" },
        { Inv.Key.Space, " " },
        { Inv.Key.Period, "." },
        { Inv.Key.Comma, "," },
        { Inv.Key.Tilde, "~" },
        { Inv.Key.BackQuote, "`" },
        { Inv.Key.Up, UIKit.UIKeyCommand.UpArrow },
        { Inv.Key.Down, UIKit.UIKeyCommand.DownArrow },
        { Inv.Key.Left, UIKit.UIKeyCommand.LeftArrow },
        { Inv.Key.Right, UIKit.UIKeyCommand.RightArrow },
        //{ Inv.Key.Backspace, "\b" },
        { Inv.Key.Delete, "\b" },
        { Inv.Key.Backslash, "\\" },
        { Inv.Key.Slash, "/" },
      };

      ReverseDictionary = ForwardDictionary.ToDictionary(D => D.Value, D => D.Key);

      // TODO: DiscoverabilityTitle in iOS 9?

      // NOTE: the "KeyDown:" selector is actually a private method on the iOSSurfaceController.
      var KeyList =
        ForwardDictionary.Select(K => UIKit.UIKeyCommand.Create(new Foundation.NSString(K.Value), (UIKit.UIKeyModifierFlags)0, new ObjCRuntime.Selector("KeyDown:"))).Union(
        ForwardDictionary.Select(K => UIKit.UIKeyCommand.Create(new Foundation.NSString(K.Value), UIKit.UIKeyModifierFlags.Shift, new ObjCRuntime.Selector("KeyDown:"))).Union(
        ForwardDictionary.Select(K => UIKit.UIKeyCommand.Create(new Foundation.NSString(K.Value), UIKit.UIKeyModifierFlags.Control, new ObjCRuntime.Selector("KeyDown:"))).Union(
        ForwardDictionary.Select(K => UIKit.UIKeyCommand.Create(new Foundation.NSString(K.Value), UIKit.UIKeyModifierFlags.Alternate, new ObjCRuntime.Selector("KeyDown:")))))).ToDistinctList();

      // TODO: page up/down, home/end keys - requires function modifier flag?
      //KeyList.Add(UIKit.UIKeyCommand.Create(new Foundation.NSString(UIKit.UIKeyCommand.UpArrow), UIKit.UIKeyModifierFlags.Fn, new ObjCRuntime.Selector("KeyDown:")));

      KeyCommandArray = KeyList.ToArray();
    }

    public static Inv.Key TranslateKey(string iOSInput)
    {
      return ReverseDictionary[iOSInput];
    }
    public static Inv.KeyModifier TranslateModifier(UIKit.UIKeyModifierFlags iOSModifierFlags)
    {
      var Result = new Inv.KeyModifier();

      if ((iOSModifierFlags & UIKit.UIKeyModifierFlags.Shift) != 0)
      {
        Result.IsLeftShift = true;
        Result.IsRightShift = true;
      }

      if ((iOSModifierFlags & UIKit.UIKeyModifierFlags.Control) != 0)
      {
        Result.IsLeftCtrl = true;
        Result.IsRightCtrl = true;
      }

      if ((iOSModifierFlags & UIKit.UIKeyModifierFlags.Alternate) != 0)
      {
        Result.IsLeftAlt = true;
        Result.IsRightAlt = true;
      }

      return Result;
    }

    public static readonly UIKit.UIKeyCommand[] KeyCommandArray;
    public static readonly UIKit.UIKeyCommand[] BlankCommandArray = new UIKit.UIKeyCommand[] { };

    private static Dictionary<string, Inv.Key> ReverseDictionary;
  }

  internal sealed class iOSEngine
  {
    public iOSEngine(Inv.Application InvApplication)
    {
      this.InvApplication = InvApplication;
      this.ImageList = new DistinctList<Inv.Image>(16384); // 64KB memory.
      this.SoundList = new DistinctList<Inv.Sound>(1024); // 1KB memory.
      //this.ColourList = new DistinctList<Inv.Colour>(16384); // 64KB memory.
      this.RouteArray = new Inv.EnumArray<Inv.PanelType, Func<Inv.Panel, iOSNode>>()
      {
        { Inv.PanelType.Board, TranslateBoard },
        { Inv.PanelType.Browser, TranslateBrowser },
        { Inv.PanelType.Button, TranslateButton },
        { Inv.PanelType.Canvas, TranslateCanvas },
        { Inv.PanelType.Dock, TranslateDock },
        { Inv.PanelType.Edit, TranslateEdit },
        { Inv.PanelType.Graphic, TranslateGraphic },
        { Inv.PanelType.Label, TranslateLabel },
        { Inv.PanelType.Memo, TranslateMemo },
        { Inv.PanelType.Overlay, TranslateOverlay },
        { Inv.PanelType.Scroll, TranslateScroll },
        { Inv.PanelType.Frame, TranslateFrame },
        { Inv.PanelType.Stack, TranslateStack },
        { Inv.PanelType.Table, TranslateTable },
        { Inv.PanelType.Flow, TranslateFlow },
      };

      this.FontFamilyArray = new Inv.EnumArray<FontWeight, string>()
      {
        { FontWeight.Thin, "-Thin" },
        { FontWeight.Light, "-Light" },
        { FontWeight.Regular, "" },
        { FontWeight.Medium, "-Medium" },
        { FontWeight.Bold, "-Bold" },
        { FontWeight.Heavy, "-Black" },
      };
      
      this.UIFontWeightArray = new Inv.EnumArray<FontWeight, UIKit.UIFontWeight>();
      UIFontWeightArray.Fill(W => Inv.Support.EnumHelper.Parse<UIKit.UIFontWeight>(W.ToString(), true));

      InvApplication.SetPlatform(new iOSPlatform(this));
    }

    public void Run()
    {
      iOSAppDelegate.iOSEngine = this;
      try
      {
        UIKit.UIApplication.Main(new string[] { }, null, typeof(iOSAppDelegate));
      }
      finally
      {
        iOSAppDelegate.iOSEngine = null;
      }
    }

    internal UIKit.UIWindow iOSWindow { get; private set; }
    internal Inv.Application InvApplication { get; private set; }

    // NOTE: called from the AppDelegate.
    internal UIKit.UIWindow Launching()
    {
      Guard(() =>
      {
        InvApplication.Directory.Installation = Foundation.NSBundle.MainBundle.BundleIdentifier;

        var CurrentDevice = UIKit.UIDevice.CurrentDevice;
        InvApplication.Device.Name = CurrentDevice.Name;
        InvApplication.Device.Model = iOSHardware.DeviceModel;
        InvApplication.Device.System = "iOS " + CurrentDevice.SystemVersion;
        InvApplication.Device.Keyboard = false; // TODO: doesn't seem to have a reliable way to ask if there is a hardware keyboard connected.
        InvApplication.Device.Mouse = false; // TODO: is this a thing?  you can use mouse in the simulator.
        InvApplication.Device.Touch = true;
        InvApplication.Device.ProportionalFontName = "HelveticaNeue"; // NOTE: this UIKit.UIFont.SystemFontOfSize(10).FamilyName; returns SF UI Text
        InvApplication.Device.MonospacedFontName = "Menlo";

        InvApplication.Process.Id = Foundation.NSProcessInfo.ProcessInfo.ProcessIdentifier;

        this.iOSWindow = new UIKit.UIWindow();

        Resize();

        iOSWindow.MakeKeyAndVisible();

        InvApplication.StartInvoke();

        this.iOSRootController = new UIKit.UINavigationController();
        iOSWindow.RootViewController = iOSRootController;
        iOSRootController.Delegate = new iOSNavigationControllerDelegate();
        iOSRootController.NavigationBarHidden = true;

        this.iOSDisplayLink = CoreAnimation.CADisplayLink.Create(Process);
        iOSDisplayLink.AddToRunLoop(Foundation.NSRunLoop.Current, Foundation.NSRunLoopMode.Common);

        if (InvApplication.Location.IsRequired && InvApplication.Location.IsSupported)
        {
          this.iOSLocationManager = new CoreLocation.CLLocationManager();

          // iOS 8 requires you to manually request authorisation now.
          if (UIKit.UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
          {
            iOSLocationManager.RequestAlwaysAuthorization(); // works in background and foreground
            //iOSLocationManager.RequestWhenInUseAuthorization(); // works only in foreground
          }

          iOSLocationManager.DesiredAccuracy = CoreLocation.CLLocation.AccuracyNearestTenMeters;
          iOSLocationManager.DistanceFilter = 10.0F;
          iOSLocationManager.LocationsUpdated += (Sender, Event) =>
          {
            if (Event.Locations.Length > 0)
            {
              var Location = Event.Locations[Event.Locations.Length - 1];

              Guard(() => InvApplication.Location.ChangeInvoke(new Inv.Coordinate(Location.Coordinate.Latitude, Location.Coordinate.Longitude, Location.Altitude)));
            }
          };

          iOSLocationManager.StartUpdatingLocation();
        }
      });

      Process();

      return iOSWindow;
    }

    internal void Terminating()
    {
      Guard(() =>
      {
        if (iOSLocationManager != null)
        {
          iOSLocationManager.StopUpdatingLocation();
          iOSLocationManager = null;
        }

        InvApplication.StopInvoke();
      });
    }
    internal void Pausing()
    {
      Guard(() => InvApplication.SuspendInvoke());
    }
    internal void Resuming()
    {
      Guard(() => InvApplication.ResumeInvoke());
    }
    internal AVFoundation.AVAudioPlayer PlaySound(Sound InvSound, float Volume, float Rate, bool Loop)
    {
      if (InvSound == null)
        return null;

      var iOSSound = InvSound.Node as AVFoundation.AVAudioPlayer;

      if (iOSSound == null)
      {
        var SoundBuffer = InvSound.GetBuffer();

        // NOTE: this is an attempted workaround when a sound cannot be played:
        //       "Could not initialize an instance of the type 'AVFoundation.AVAudioPlayer': the native 'initWithContentsOfURL:error:' method returned nil."

        Foundation.NSError Error;
        var Retry = 0;
        do
        {
          var Result = AVFoundation.AVAudioPlayer.FromData(Foundation.NSData.FromArray(SoundBuffer), out Error);
          if (Error == null)
          {
            iOSSound = Result;
            break;
          }
          else
          {
            System.Threading.Thread.Sleep(10);
          }

          Retry++;
        }
        while (Error != null && Retry < 100);

        if (iOSSound == null)
          return null;

        if (InvSound.Node == null || !SoundList.Contains(InvSound))
          SoundList.Add(InvSound);

        InvSound.Node = iOSSound;
      }

      // this is necessary to overlap the playing of the same SFX.
      if (iOSSound.Playing)
        iOSSound = AVFoundation.AVAudioPlayer.FromData(iOSSound.Data);

      // ensure volume is within a valid range.
      if (Volume > 1.0F)
        Volume = 1.0F;
      else if (Volume < 0.0F)
        Volume = 0.0F;

      // NOTE: iOS docs say that 0.5 to 2.0 is the support range for playback rate (https://developer.apple.com/reference/avfoundation/avaudioplayer/1386118-rate).
      if (Rate > 2.0F)
        Rate = 2.0F;
      else if (Rate < 0.5F)
        Rate = 0.5F;

      iOSSound.PrepareToPlay();
      iOSSound.Volume = Volume;
      iOSSound.EnableRate = true;
      iOSSound.NumberOfLoops = Loop ? -1 : 0;
      iOSSound.Rate = Rate;
      iOSSound.Play();

      return iOSSound;
    }
    internal void Guard(Action Action)
    {
      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception);
      }
    }
    internal void Reclamation()
    {
      foreach (var Image in ImageList)
      {
        var iOSImage = Image.Node as UIKit.UIImage;
        
        if (iOSImage != null)
          iOSImage.Dispose();

        Image.Node = null;
      }
      ImageList.Clear();

      //foreach (var Colour in ColourList)
      //{
      //  var iOSColour = (UIKit.UIColor)Colour.Node;
      //
      //  if (iOSColour != null)
      //    iOSColour.Dispose();
      //
      //  Colour.Node = null;
      //}
      //ColourList.Clear();

      foreach (var Sound in SoundList)
      {
        var iOSSound = Sound.Node as AVFoundation.AVAudioPlayer;

        if (iOSSound != null)
          iOSSound.Dispose();

        Sound.Node = null;
      }
      SoundList.Clear();
    }
    internal void ShowCalendarPicker(CalendarPicker CalendarPicker)
    {
      var InvSurface = InvApplication.Window.ActiveSurface;
      var iOSSurface = AccessSurface(InvSurface);

      var DatePickerViewController = new iOSDateTimePicker(iOSSurface, "Select");
      DatePickerViewController.PickDate = CalendarPicker.SetDate;
      DatePickerViewController.PickTime = CalendarPicker.SetTime;
      DatePickerViewController.Value = CalendarPicker.Value;
      DatePickerViewController.DoneEvent += WeakHelpers.WeakWrapper(CalendarPicker, (calendarPicker) =>
      {
        calendarPicker.Value = DatePickerViewController.Value;
        calendarPicker.SelectInvoke();
      });
      DatePickerViewController.CancelEvent += WeakHelpers.WeakWrapper(CalendarPicker, (calendarPicker) =>
      {
        calendarPicker.CancelInvoke();
      });

      iOSWindow.RootViewController.PresentViewController(DatePickerViewController, true, null);
    }
    internal UIKit.UIFont TranslateUIFont(string FontName, int? FontSize, Inv.FontWeight InvFontWeight)
    {
      // TODO: custom font name.

      var iOSFontWeight = UIFontWeightArray[InvFontWeight];

      if (FontName == null)
      {
        return UIKit.UIFont.SystemFontOfSize(FontSize ?? UIKit.UIFont.SystemFontSize, iOSFontWeight);
      }
      else
      {
        var Result = UIKit.UIFont.FromName(FontName + FontFamilyArray[InvFontWeight], FontSize ?? UIKit.UIFont.SystemFontSize);

        Debug.Assert(Result != null, FontName);

        return Result;
      }
    }

    internal CoreGraphics.CGImage TranslateCGImage(Inv.Image Image, int? PointWidth, int? PointHeight)
    {
      if (Image == null)
        return null;
      else
        return TranslateUIImage(Image, PointWidth, PointHeight).CGImage;
    }
    internal UIKit.UIColor TranslateUIColor(Inv.Colour InvColour)
    {
      if (InvColour == null)
      {
        return UIKit.UIColor.Clear;
      }
      else if (InvColour.Node == null)
      {
        var Record = InvColour.GetARGBRecord();
        var Result = new UIKit.UIColor((float)Record.R / 255F, (float)Record.G / 255F, (float)Record.B / 255F, (float)Record.A / 255F);

        InvColour.Node = Result;
        //ColourList.Add(InvColour);

        return Result;
      }
      else
      {
        return (UIKit.UIColor)InvColour.Node;
      }
    }
    internal CoreGraphics.CGColor TranslateCGColor(Inv.Colour InvColour)
    {
      return TranslateUIColor(InvColour).CGColor;
    }
    internal CoreGraphics.CGPoint TranslateCGPoint(Inv.Point InvPoint)
    {
      return new CoreGraphics.CGPoint(InvPoint.X, InvPoint.Y);
    }
    internal void Toast(string Title, string Message, string Button)
    {
      new UIKit.UIAlertView(Title, Message, null, Button, null).Show();
    }

    private void Resize()
    {
      var ScreenBounds = UIKit.UIScreen.MainScreen.Bounds;
      InvApplication.Window.Width = (int)ScreenBounds.Width;
      InvApplication.Window.Height = (int)ScreenBounds.Height;

      // NOTE: using iOSWindow.Bounds will work in 9.x but not work in 8.x (it will cause the view to be offset from the middle of the screen).
      iOSWindow.Frame = ScreenBounds;
    }
    private void Process()
    {
      if (InvApplication.IsExit)
      {
        UIKit.UIApplication.SharedApplication.PerformSelector(new ObjCRuntime.Selector("terminateWithSuccess"), null, 0f);
        // ALTERNATIVE? [DllImport("__Internal", EntryPoint = "exit")] static extern void exit(int status); exit(0);
      }
      else
      {
        Guard(() =>
        {
          var InvWindow = InvApplication.Window;

          InvWindow.ProcessInvoke();

          if (InvWindow.ActiveTimerSet.Count > 0)
          {
            foreach (var InvTimer in InvWindow.ActiveTimerSet)
            {
              var iOSTimer = AccessTimer(InvTimer, S =>
              {
                var Result = new System.Timers.Timer();
                Result.Elapsed += (Sender, Event) => InvWindow.Application.Platform.WindowPost(() => Guard(() => InvTimer.IntervalInvoke()));
                return Result;
              });

              if (InvTimer.IsRestarting)
              {
                InvTimer.IsRestarting = false;
                iOSTimer.Stop();
              }

              if (iOSTimer.Interval != InvTimer.IntervalTime.TotalMilliseconds)
                iOSTimer.Interval = InvTimer.IntervalTime.TotalMilliseconds;

              if (InvTimer.IsEnabled && !iOSTimer.Enabled)
                iOSTimer.Start();
              else if (!InvTimer.IsEnabled && iOSTimer.Enabled)
                iOSTimer.Stop();
            }

            InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
          }

          var InvSurface = InvWindow.ActiveSurface;

          if (InvSurface != null)
          {
            var iOSSurface = AccessSurface(InvSurface);

            iOSSurface.Reload();

            if (InvSurface.Focus != null)
            {
              var iOSFocus = InvSurface.Focus.Node as iOSNode;

              InvSurface.Focus = null;

              if (iOSFocus != null)
                iOSWindow.BeginInvokeOnMainThread(() => iOSFocus.PanelView.View.BecomeFirstResponder());
            }
          }

          if (InvWindow.Render())
          {
            // TODO: InvApplication.Title;
            iOSWindow.BackgroundColor = TranslateUIColor(InvWindow.Background.Colour);
          }

          if (InvSurface != null)
            ProcessAnimation(InvSurface);

          InvWindow.DisplayRate.Calculate();
        });
      }
    }

    private void UpdateSurface(Surface InvSurface, iOSLayoutController iOSSurface)
    {
      if (InvSurface.Render())
      {
        TranslateBackground(InvSurface.Background.Colour ?? InvSurface.Window.Background.Colour ?? Inv.Colour.Black, iOSSurface.View);

        iOSSurface.SetContentElement(TranslatePanel(InvSurface.Content));
      }

      InvSurface.ProcessChanges(P => TranslatePanel(P));
    }
    private void ProcessTransition(iOSLayoutController iOSSurface)
    {
      var InvWindow = InvApplication.Window;
      var InvTransition = InvWindow.ActiveTransition;

      if (InvTransition == null)
      {
        // no transition.
      }
      else if (!IsTransitioning)
      {
        var iOSFromController = iOSRootController.ChildViewControllers.Length == 0 ? null : iOSRootController.ChildViewControllers[0];

        if (iOSSurface == iOSFromController)
        {
          Debug.WriteLine("Transition to self");
        }
        else
        {
          // give the previous surface a chance to process before it is animated away.
          if (InvTransition.FromSurface != null && iOSFromController != null)
            UpdateSurface(InvTransition.FromSurface, (iOSLayoutController)iOSFromController);

          switch (InvTransition.Animation)
          {
            case TransitionAnimation.None:
              if (iOSFromController != null)
                iOSFromController.RemoveFromParentViewController();

              iOSRootController.PushViewController(iOSSurface, false);
              break;

            case TransitionAnimation.Fade:
              this.IsTransitioning = true;

              if (iOSFromController != null)
              {
                var HalfTime = InvTransition.Duration.TotalSeconds / 2.0;

                iOSFromController.View.FadeOut(HalfTime, 0.0, () =>
                {
                  iOSFromController.RemoveFromParentViewController();
                  iOSFromController.View.Alpha = 1.0F;

                  iOSRootController.PushViewController(iOSSurface, false);

                  iOSSurface.View.FadeIn(HalfTime, 0.0, () => IsTransitioning = false);
                });
              }
              else
              {
                iOSRootController.PushViewController(iOSSurface, false);
                iOSSurface.View.FadeIn(InvTransition.Duration.TotalSeconds, 0.0, () => IsTransitioning = false);
              }
              break;

            // TODO: Carousel animations need to be implemented properly - there is a timing hole where this code will fail (click fast enough).

            case TransitionAnimation.CarouselBack:
              if (iOSFromController != null)
                iOSFromController.RemoveFromParentViewController();

              iOSRootController.PushViewController(iOSSurface, false);

              if (iOSFromController != null)
              {
                iOSRootController.PushViewController(iOSFromController, false);
                iOSRootController.PopViewController(true);
              }
              break;

            case TransitionAnimation.CarouselNext:
              iOSRootController.PushViewController(iOSSurface, true);
              break;

            default:
              throw new Exception("TransitionAnimation not handled: " + InvTransition.Animation);
          }
        }

        InvWindow.ActiveTransition = null;
      }
    }
    private void ProcessAnimation(Inv.Surface InvSurfaceActive)
    {
      if (InvSurfaceActive.StopAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StopAnimationSet)
        {
          if (InvAnimation.Node != null)
          {
            var iOSAnimationSet = (iOSAnimationSet)InvAnimation.Node;
            InvAnimation.Node = null;

            foreach (var iOSAnimationItem in iOSAnimationSet.ItemList)
            {
              var InvPanel = iOSAnimationItem.InvPanel;
              var iOSPanel = iOSAnimationItem.iOSPanel;

              var PresentationOpacity = iOSPanel.Layer.PresentationLayer.Opacity;

              iOSPanel.Layer.Opacity = PresentationOpacity;
              InvPanel.Opacity.BypassSet(PresentationOpacity);

              iOSPanel.Layer.RemoveAnimation(iOSAnimationItem.iOSAnimationKey);
            }
          }
        }
        InvSurfaceActive.StopAnimationSet.Clear();
      }
      else if (InvSurfaceActive.StartAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StartAnimationSet)
        {
          var iOSAnimationSet = new iOSAnimationSet();
          InvAnimation.Node = iOSAnimationSet;

          //CoreAnimation.CATransaction.Begin();

          var InvLastTarget = InvAnimation.GetTargets().LastOrDefault();

          foreach (var InvTarget in InvAnimation.GetTargets())
          {
            var InvLastCommand = InvTarget.GetCommands().LastOrDefault();

            var InvPanel = InvTarget.Panel;
            var iOSPanel = TranslatePanel(InvPanel);

            var iOSAnimationList = new Inv.DistinctList<CoreAnimation.CAAnimation>();

            foreach (var InvCommand in InvTarget.GetCommands())
            {
              var InvOpacityCommand = (Inv.AnimationOpacityCommand)InvCommand;

              var FadeAnimation = CoreAnimation.CABasicAnimation.FromKeyPath("opacity");
              FadeAnimation.From = new Foundation.NSNumber(InvOpacityCommand.From);
              FadeAnimation.To = new Foundation.NSNumber(InvOpacityCommand.To);
              FadeAnimation.Duration = InvOpacityCommand.Duration.TotalSeconds;
              FadeAnimation.TimingFunction = CoreAnimation.CAMediaTimingFunction.FromName(CoreAnimation.CAMediaTimingFunction.EaseInEaseOut);
              FadeAnimation.FillMode = CoreAnimation.CAFillMode.Forwards;

              if (InvOpacityCommand.Offset != null)
                FadeAnimation.BeginTime = CoreAnimation.CAAnimation.CurrentMediaTime() + InvOpacityCommand.Offset.Value.TotalSeconds;

              iOSPanel.Layer.Opacity = InvOpacityCommand.From;
              InvPanel.Opacity.BypassSet(InvOpacityCommand.From);

              FadeAnimation.AnimationStarted += WeakHelpers.WeakEventHandlerWrapper(InvAnimation, iOSPanel, InvPanel, InvOpacityCommand, (invAnimation, iosPanel, invPanel, invOpacityCommand, Sender, Event) =>
              {
                if (invAnimation.IsActive)
                {
                  iosPanel.Layer.Opacity = invOpacityCommand.To;
                  invPanel.Opacity.BypassSet(invOpacityCommand.To);
                }
              });
              FadeAnimation.AnimationStopped += WeakHelpers.WeakEventHandlerWrapper<Animation, AnimationTarget, AnimationTarget, AnimationCommand, AnimationCommand, CoreAnimation.CAAnimationStateEventArgs>(InvAnimation, InvTarget, InvLastTarget, InvCommand, InvLastCommand, (invAnimation, invTarget, invLastTarget, invCommand, invLastCommand, Sender, Event) =>
              {
                if (invAnimation.IsActive)
                {
                  invCommand.Complete();

                  if (invTarget == invLastTarget && invCommand == invLastCommand)
                    invAnimation.Complete();
                }
              });

              iOSAnimationSet.Add(InvPanel, iOSPanel, FadeAnimation);
            }
          }

          //CoreAnimation.CATransaction.Commit();
        }
        InvSurfaceActive.StartAnimationSet.Clear();
      }
    }

    private iOSLayoutContainer TranslatePanel(Inv.Panel InvPanel)
    {
      if (InvPanel == null)
        return null;
      else
        return RouteArray[InvPanel.PanelType](InvPanel).LayoutView;
    }
    private iOSNode TranslateBoard(Inv.Panel InvPanel)
    {
      var InvBoard = (Inv.Board)InvPanel;

      var iOSNode = AccessPanel(InvBoard, () =>
      {
        var Result = new iOSLayoutBoard();
        return Result;
      });

      RenderPanel(InvBoard, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSBoard = iOSNode.PanelView;

        TranslateLayout(InvBoard, iOSLayout, iOSBoard);

        if (InvBoard.PinCollection.Render())
        {
          iOSBoard.RemoveElements();
          foreach (var InvPin in InvBoard.PinCollection)
          {
            var InvRect = InvPin.Rect;

            var iOSElement = TranslatePanel(InvPin.Panel);
            iOSBoard.AddElement(iOSElement, new CoreGraphics.CGRect(InvRect.Left, InvRect.Top, InvRect.Width, InvRect.Height));
          }
        }
      });

      return iOSNode;
    }
    private iOSNode TranslateBrowser(Inv.Panel InvPanel)
    {
      var InvBrowser = (Inv.Browser)InvPanel;

      var iOSNode = AccessPanel(InvBrowser, () =>
      {
        var Result = new iOSLayoutBrowser();

        return Result;
      });

      RenderPanel(InvBrowser, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSBrowser = iOSNode.PanelView;

        TranslateLayout(InvBrowser, iOSLayout, iOSBrowser);

        var Navigate = InvBrowser.UriSingleton.Render();
        if (InvBrowser.HtmlSingleton.Render())
          Navigate = true;

        if (Navigate)
          iOSBrowser.Navigate(InvBrowser.UriSingleton.Data, InvBrowser.HtmlSingleton.Data);
      });

      return iOSNode;
    }
    private iOSNode TranslateButton(Inv.Panel InvPanel)
    {
      var InvButton = (Inv.Button)InvPanel;

      var iOSNode = AccessPanel(InvButton, () =>
      {
        var Result = new iOSLayoutButton();

        Result.PreviewTouchesBeganEvent += WeakHelpers.WeakWrapper(this, InvButton, Result, (engine, button, iosButton) =>
        {
          engine.Guard(() =>
          {
            if (engine.InvApplication.Window.IsActiveSurface(button.Surface))
            {
              engine.TranslateBackground(button.Background.Colour != null ? button.Background.Colour.Darken(0.25F) : (Inv.Colour)null, iosButton);

              button.PressInvoke();
            }
          });
        });
        Result.PreviewTouchesEndedEvent += WeakHelpers.WeakWrapper(this, InvButton, Result, (engine, button, iosButton) =>
        {
          engine.Guard(() =>
          {
            if (engine.InvApplication.Window.IsActiveSurface(button.Surface))
              engine.TranslateBackground(button.Background.Colour, iosButton);
          });
        });
        Result.PreviewTouchesCancelledEvent += WeakHelpers.WeakWrapper(this, InvButton, Result, (engine, button, iosButton) =>
        {
          engine.Guard(() =>
          {
            if (engine.InvApplication.Window.IsActiveSurface(button.Surface))
            {
              engine.TranslateBackground(button.Background.Colour, iosButton);

              button.ReleaseInvoke();
            }
          });
        });

        Result.AddGestureRecognizer(new UIKit.UITapGestureRecognizer(WeakHelpers.WeakWrapper<iOSEngine, Inv.Button, iOSLayoutButton, UIKit.UITapGestureRecognizer>(this, InvButton, Result, (engine, button, iosbutton, R) =>
        {
          if (R.State == UIKit.UIGestureRecognizerState.Ended)
          {
            if (engine.InvApplication.Window.IsActiveSurface(button.Surface))
              engine.Guard(() => button.SingleTapInvoke());
          }
        })));
        Result.AddGestureRecognizer(new UIKit.UILongPressGestureRecognizer(WeakHelpers.WeakWrapper<iOSEngine, Inv.Button, iOSLayoutButton, UIKit.UILongPressGestureRecognizer>(this, InvButton, Result, (engine, button, iosbutton, R) =>
        {
          if (R.State == UIKit.UIGestureRecognizerState.Began)
          {
            if (engine.InvApplication.Window.IsActiveSurface(button.Surface))
              engine.Guard(() => button.ContextTapInvoke());
          }
        })));

        return Result;
      });

      RenderPanel(InvButton, iOSNode, WeakHelpers.WeakWrapper(this, InvButton, iOSNode, (engine, invButton, iosNode) =>
      {
        var iOSLayout = iosNode.LayoutView;
        var iOSButton = iosNode.PanelView;

        engine.TranslateLayout(invButton, iOSLayout, iOSButton);

        iOSLayout.Alpha = invButton.IsEnabled ? 1.0F : 0.5F;
        iOSButton.Enabled = invButton.IsEnabled;
        // TODO: InvButton.IsFocused

        if (invButton.ContentSingleton.Render())
        {
          iOSButton.SetContentElement(null); // detach previous content in case it has moved.
          var iOSPanel = engine.TranslatePanel(invButton.ContentSingleton.Data);
          iOSButton.SetContentElement(iOSPanel);
        }
      }));

      return iOSNode;
    }
    private iOSNode TranslateCanvas(Inv.Panel InvPanel)
    {
      var InvCanvas = (Inv.Canvas)InvPanel;

      var iOSNode = AccessPanel(InvCanvas, () =>
      {
        var Result = new iOSLayoutCanvas(this);
        Result.TouchDownEvent += WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint>(this, InvCanvas, (engine, invCanvas, Point) => engine.Guard(() => invCanvas.PressInvoke(engine.TranslatePoint(Point))));
        Result.TouchMoveEvent += WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint>(this, InvCanvas, (engine, invCanvas, Point) => engine.Guard(() => invCanvas.MoveInvoke(engine.TranslatePoint(Point))));
        Result.TouchUpEvent += WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint>(this, InvCanvas, (engine, invCanvas, Point) => engine.Guard(() => invCanvas.ReleaseInvoke(engine.TranslatePoint(Point))));
        Result.SingleTapEvent += WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint>(this, InvCanvas, (engine, invCanvas, Point) => engine.Guard(() => invCanvas.SingleTapInvoke(engine.TranslatePoint(Point))));
        Result.DoubleTapEvent += WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint>(this, InvCanvas, (engine, invCanvas, Point) => engine.Guard(() => invCanvas.DoubleTapInvoke(engine.TranslatePoint(Point))));
        Result.LongPressEvent += WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint>(this, InvCanvas, (engine, invCanvas, Point) => engine.Guard(() => invCanvas.ContextTapInvoke(engine.TranslatePoint(Point))));
        Result.ZoomEvent += WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint, int>(this, InvCanvas, (engine, invCanvas, Point, Delta) => engine.Guard(() => invCanvas.ZoomInvoke(engine.TranslatePoint(Point), Delta)));
        Result.DrawAction = WeakHelpers.WeakWrapper(this, InvCanvas, (engine, invCanvas) => engine.Guard(() => invCanvas.DrawInvoke(Result)));
        return Result;
      });

      RenderPanel(InvCanvas, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSCanvas = iOSNode.PanelView;

        TranslateLayout(InvCanvas, iOSLayout, iOSCanvas);

        if (InvCanvas.Redrawing)
          iOSCanvas.SetNeedsDisplay();
      });

      return iOSNode;
    }
    private iOSNode TranslateDock(Inv.Panel InvPanel)
    {
      var InvDock = (Inv.Dock)InvPanel;

      var iOSNode = AccessPanel(InvDock, () =>
      {
        var Result = new iOSLayoutDock();
        Result.SetOrientation(InvDock.Orientation == DockOrientation.Vertical ? iOSLayoutOrientation.Vertical : iOSLayoutOrientation.Horizontal);
        return Result;
      });

      RenderPanel(InvDock, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSDock = iOSNode.PanelView;

        TranslateLayout(InvDock, iOSLayout, iOSDock);

        if (InvDock.CollectionRender())
          iOSDock.ComposeElements(InvDock.HeaderCollection.Select(H => TranslatePanel(H)), InvDock.ClientCollection.Select(C => TranslatePanel(C)), InvDock.FooterCollection.Reverse().Select(F => TranslatePanel(F)));
      });

      return iOSNode;
    }
    private iOSNode TranslateEdit(Inv.Panel InvPanel)
    {
      var InvEdit = (Inv.Edit)InvPanel;

      var IsSearch = InvEdit.Input == EditInput.Search;

      var iOSNode = AccessPanel(InvEdit, () =>
      {
        if (IsSearch)
        {
          var Result = new iOSLayoutSearch();

          Result.AutocapitalizationType = UIKit.UITextAutocapitalizationType.None;
          Result.AutocorrectionType = UIKit.UITextAutocorrectionType.No;
          Result.TextChanged += WeakHelpers.WeakEventHandlerWrapper<iOSEngine, Inv.Edit, iOSLayoutSearch, UIKit.UISearchBarTextChangedEventArgs>(this, InvEdit, Result, (engine, invEdit, iosNode, Sender, Event) => engine.Guard(() => invEdit.ChangeText(iosNode.Text)));
          Result.SearchButtonClicked += WeakHelpers.WeakEventHandlerWrapper(this, InvEdit, (engine, invEdit, Sender, Event) => engine.Guard(() => invEdit.Return()));

          return (iOSLayoutElement)Result;
        }
        else
        {
          var Result = new iOSLayoutEdit();

          switch (InvEdit.Input)
          {
            case EditInput.Decimal:
              Result.KeyboardType = UIKit.UIKeyboardType.Default;
              break;

            case EditInput.Email:
              Result.KeyboardType = UIKit.UIKeyboardType.EmailAddress;
              break;

            case EditInput.Integer:
              Result.KeyboardType = UIKit.UIKeyboardType.Default;
              break;

            case EditInput.Name:
              Result.KeyboardType = UIKit.UIKeyboardType.Default;
              Result.AutocapitalizationType = UIKit.UITextAutocapitalizationType.Words;
              Result.AutocorrectionType = UIKit.UITextAutocorrectionType.No;
              break;

            case EditInput.Number:
              Result.KeyboardType = UIKit.UIKeyboardType.Default;
              break;

            case EditInput.Password:
              Result.KeyboardType = UIKit.UIKeyboardType.Default;
              Result.SecureTextEntry = true;
              break;

            case EditInput.Phone:
              Result.KeyboardType = UIKit.UIKeyboardType.PhonePad;
              break;

            case EditInput.Text:
              Result.KeyboardType = UIKit.UIKeyboardType.Default;
              Result.AutocapitalizationType = UIKit.UITextAutocapitalizationType.None;
              break;

            case EditInput.Uri:
              Result.KeyboardType = UIKit.UIKeyboardType.Url;
              break;

            default:
              throw new Exception("EditInput not handled: " + InvEdit.Input);
          }

          Result.EditingChanged += WeakHelpers.WeakEventHandlerWrapper(this, InvEdit, Result, (engine, invEdit, iosNode, Sender, Event) => engine.Guard(() => invEdit.ChangeText(iosNode.Text)));
          Result.EditingDidEnd += WeakHelpers.WeakEventHandlerWrapper(this, InvEdit, Result, (engine, invEdit, iosNode, Sender, Event) => engine.Guard(() => invEdit.ChangeText(iosNode.Text)));
          Result.ReturnEvent += WeakHelpers.WeakWrapper(this, InvEdit, (engine, invEdit) => engine.Guard(() => invEdit.Return()));

          return (iOSLayoutElement)Result;
        }
      });

      RenderPanel(InvEdit, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;

        if (IsSearch)
        {
          var iOSSearch = (iOSLayoutSearch)iOSNode.PanelView;

          TranslateLayout(InvEdit, iOSLayout, iOSSearch);
          TranslateFont(InvEdit.Font, (iOSFont, iOSColor) =>
          {
            iOSSearch.TextFont = iOSFont;
            iOSSearch.TextColor = iOSColor;
          });

          iOSSearch.IsReadOnly = InvEdit.IsReadOnly;
          iOSSearch.Text = InvEdit.Text;
        }
        else
        {
          var iOSEdit = (iOSLayoutEdit)iOSNode.PanelView;

          TranslateLayout(InvEdit, iOSLayout, iOSEdit);
          TranslateFont(InvEdit.Font, (iOSFont, iOSColor) =>
          {
            iOSEdit.Font = iOSFont;
            iOSEdit.TextColor = iOSColor;
          });

          iOSEdit.IsReadOnly = InvEdit.IsReadOnly;
          iOSEdit.Text = InvEdit.Text;
        }
      });

      return iOSNode;
    }
    private iOSNode TranslateFlow(Inv.Panel InvPanel)
    {
      var InvFlow = (Inv.Flow)InvPanel;

      var iOSNode = AccessPanel(InvFlow, () =>
      {
        var Result = new iOSLayoutFlow(new iOSLayoutFlow.Configuration
        {
          NumberOfSectionsQuery = () => InvFlow.SectionList.Count,
          RowsInSectionQuery = (Section) => InvFlow.SectionList[Section].ItemCount,
          GetViewForHeaderQuery = (Section) => TranslatePanel(InvFlow.SectionList[Section].Header),
          GetViewForFooterQuery = (Section) => TranslatePanel(InvFlow.SectionList[Section].Footer),
          GetCellContentQuery = (IndexPath) =>
          {
            var TileSection = InvFlow.SectionList[IndexPath.Section];
            var TileContent = TileSection.ItemInvoke(IndexPath.Row);

            return TranslatePanel(TileContent);
          }
        });
        Result.RefreshAction = () => InvFlow.RefreshInvoke(new FlowRefresh(InvFlow, Result.CompleteRefresh));
        return Result;
      });

      RenderPanel(InvFlow, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSFlow = iOSNode.PanelView;

        TranslateLayout(InvFlow, iOSLayout, iOSFlow);

        iOSFlow.IsRefreshable = InvFlow.IsRefreshable;

        if (InvFlow.IsRefresh)
        {
          InvFlow.IsRefresh = false;

          iOSFlow.NewRefresh();
        }

        if (InvFlow.IsReload)
        {
          InvFlow.IsReload = false;
          InvFlow.ReloadSectionList.Clear();
          InvFlow.ReloadItemList.Clear();

          iOSFlow.ReloadData();
        }
        
        if (InvFlow.ReloadSectionList.Count > 0)
        {
          iOSFlow.BeginUpdates();
          foreach (var Section in InvFlow.ReloadSectionList)
          {
            iOSFlow.ReloadSections(Foundation.NSIndexSet.FromIndex(Section), UIKit.UITableViewRowAnimation.Fade);
            InvFlow.ReloadItemList.RemoveAll(Item => Item.Section == Section);
          }
          iOSFlow.EndUpdates();
          
          InvFlow.ReloadSectionList.Clear();
        }

        if (InvFlow.ReloadItemList.Count > 0)
        {
          iOSFlow.BeginUpdates();
          iOSFlow.ReloadRows(InvFlow.ReloadItemList.Select(Item => Foundation.NSIndexPath.FromItemSection(Item.Index, Item.Section)).ToArray(), UIKit.UITableViewRowAnimation.Fade);
          iOSFlow.EndUpdates();

          InvFlow.ReloadItemList.Clear();
        }

        if (InvFlow.ScrollSection != null || InvFlow.ScrollIndex != null)
        {
          if (InvFlow.ScrollSection != null && InvFlow.ScrollIndex != null)
            iOSFlow.ScrollToRow(Foundation.NSIndexPath.FromItemSection(InvFlow.ScrollIndex.Value, InvFlow.ScrollSection.Value), UIKit.UITableViewScrollPosition.Top, true);

          InvFlow.ScrollSection = null;
          InvFlow.ScrollIndex = null;
        }
      });

      return iOSNode;
    }
    private iOSNode TranslateGraphic(Inv.Panel InvPanel)
    {
      var InvGraphic = (Inv.Graphic)InvPanel;

      var iOSNode = AccessPanel(InvGraphic, () =>
      {
        var Result = new iOSLayoutGraphic();
        return Result;
      });

      RenderPanel(InvGraphic, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSGraphic = iOSNode.PanelView;

        TranslateLayout(InvGraphic, iOSLayout, iOSGraphic);

        if (InvGraphic.ImageSingleton.Render())
        {
          var iOSImage = TranslateUIImage(InvGraphic.Image, InvGraphic.Size.Width, InvGraphic.Size.Height);

          if (iOSGraphic.Image != iOSImage)
            iOSGraphic.Image = iOSImage;
        }
      });

      return iOSNode;
    }
    private iOSNode TranslateLabel(Inv.Panel InvPanel)
    {
      var InvLabel = (Inv.Label)InvPanel;

      var iOSNode = AccessPanel(InvLabel, () =>
      {
        var Result = new iOSLayoutLabel();
        return Result;
      });

      RenderPanel(InvLabel, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSLabel = iOSNode.PanelView;

        TranslateLayout(InvLabel, iOSLayout, iOSLabel);
        TranslateFont(InvLabel.Font, (iOSFont, iOSColor) =>
        {
          iOSLabel.Font = iOSFont;
          iOSLabel.TextColor = iOSColor;
        });

        switch (InvLabel.Justification)
        {
          case Inv.Justification.Left:
            iOSLabel.TextAlignment = UIKit.UITextAlignment.Left;
            break;

          case Inv.Justification.Center:
            iOSLabel.TextAlignment = UIKit.UITextAlignment.Center;
            break;

          case Inv.Justification.Right:
            iOSLabel.TextAlignment = UIKit.UITextAlignment.Right;
            break;

          default:
            throw new ApplicationException("Inv.Justification not handled: " + InvLabel.Justification);
        }

        iOSLabel.Lines = InvLabel.LineWrapping ? 0 : 1;
        iOSLabel.LineBreakMode = InvLabel.LineWrapping ? UIKit.UILineBreakMode.WordWrap : UIKit.UILineBreakMode.TailTruncation;

        iOSLabel.Text = InvLabel.Text ?? "";
      });

      return iOSNode;
    }
    private iOSNode TranslateMemo(Inv.Panel InvPanel)
    {
      var InvMemo = (Inv.Memo)InvPanel;

      var iOSNode = AccessPanel(InvMemo, () =>
      {
        var Result = new iOSLayoutMemo();
        Result.Changed += WeakHelpers.WeakEventHandlerWrapper(this, InvMemo, Result, (engine, invMemo, iosNode, Sender, Event) => engine.Guard(() => invMemo.ChangeText(iosNode.Text)));
        return Result;
      });

      RenderPanel(InvMemo, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSMemo = iOSNode.PanelView;

        TranslateLayout(InvMemo, iOSLayout, iOSMemo);
        TranslateFont(InvMemo.Font, (iOSFont, iOSColor) =>
        {
          iOSMemo.Font = iOSFont;
          iOSMemo.TextColor = iOSColor;
        });

        iOSMemo.Editable = !InvMemo.IsReadOnly;
        iOSMemo.Text = InvMemo.Text;
      });

      return iOSNode;
    }
    private iOSNode TranslateOverlay(Inv.Panel InvPanel)
    {
      var InvOverlay = (Inv.Overlay)InvPanel;

      var iOSNode = AccessPanel(InvOverlay, () =>
      {
        var Result = new iOSLayoutOverlay();
        return Result;
      });

      RenderPanel(InvOverlay, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSOverlay = iOSNode.PanelView;

        TranslateLayout(InvOverlay, iOSLayout, iOSOverlay);

        if (InvOverlay.PanelCollection.Render())
          iOSOverlay.ComposeElements(InvOverlay.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return iOSNode;
    }
    private iOSNode TranslateScroll(Inv.Panel InvPanel)
    {
      var InvScroll = (Inv.Scroll)InvPanel;

      var iOSNode = AccessPanel(InvScroll, () =>
      {
        var Result = new iOSLayoutScroll();
        Result.SetOrientation(InvScroll.Orientation == ScrollOrientation.Vertical ? iOSLayoutOrientation.Vertical : iOSLayoutOrientation.Horizontal);
        return Result;
      });

      RenderPanel(InvScroll, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSScroll = iOSNode.PanelView;

        TranslateLayout(InvScroll, iOSLayout, iOSScroll);

        if (InvScroll.ContentSingleton.Render())
          iOSScroll.SetContentElement(TranslatePanel(InvScroll.Content));
      });

      return iOSNode;
    }
    private iOSNode TranslateFrame(Inv.Panel InvPanel)
    {
      var InvFrame = (Inv.Frame)InvPanel;

      var iOSNode = AccessPanel(InvFrame, () =>
      {
        var Result = new iOSLayoutFrame();
        return Result;
      });

      RenderPanel(InvFrame, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSFrame = iOSNode.PanelView;

        TranslateLayout(InvPanel, iOSLayout, iOSFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          iOSFrame.SetContentElement(null); // detach previous content in case it has moved.
          var iOSPanel = TranslatePanel(InvFrame.ContentSingleton.Data);
          iOSFrame.SetContentElement(iOSPanel);
        }
      });

      return iOSNode;
    }
    private iOSNode TranslateStack(Inv.Panel InvPanel)
    {
      var InvStack = (Inv.Stack)InvPanel;

      var iOSNode = AccessPanel(InvStack, () =>
      {
        var Result = new iOSLayoutStack();
        Result.SetOrientation(InvStack.Orientation == StackOrientation.Vertical ? iOSLayoutOrientation.Vertical : iOSLayoutOrientation.Horizontal);
        return Result;
      });

      RenderPanel(InvStack, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSStack = iOSNode.PanelView;

        TranslateLayout(InvStack, iOSLayout, iOSStack);

        if (InvStack.PanelCollection.Render())
          iOSStack.ComposeElements(InvStack.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return iOSNode;
    }
    private iOSNode TranslateTable(Inv.Panel InvPanel)
    {
      var InvTable = (Inv.Table)InvPanel;

      var iOSNode = AccessPanel(InvTable, () =>
      {
        var Result = new iOSLayoutTable();
        return Result;
      });

      RenderPanel(InvTable, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSTable = iOSNode.PanelView;

        TranslateLayout(InvTable, iOSLayout, iOSTable);

        if (InvTable.CollectionRender())
        {
          var iOSRowList = new Inv.DistinctList<iOSLayoutTableRow>(InvTable.RowCollection.Count);

          foreach (var InvRow in InvTable.RowCollection)
          {
            var iOSRow = new iOSLayoutTableRow(iOSTable, TranslatePanel(InvRow.Content), InvRow.LengthType == TableAxisLength.Star, InvRow.LengthType == TableAxisLength.Fixed || InvRow.LengthType == TableAxisLength.Star ? InvRow.LengthValue : (int?)null);
            iOSRowList.Add(iOSRow);
          }

          var iOSColumnList = new Inv.DistinctList<iOSLayoutTableColumn>(InvTable.ColumnCollection.Count);

          foreach (var InvColumn in InvTable.ColumnCollection)
          {
            var iOSColumn = new iOSLayoutTableColumn(iOSTable, TranslatePanel(InvColumn.Content), InvColumn.LengthType == TableAxisLength.Star, InvColumn.LengthType == TableAxisLength.Fixed || InvColumn.LengthType == TableAxisLength.Star ? InvColumn.LengthValue : (int?)null);
            iOSColumnList.Add(iOSColumn);
          }

          var iOSCellList = new Inv.DistinctList<iOSLayoutTableCell>(InvTable.CellCollection.Count);

          foreach (var InvCell in InvTable.CellCollection)
          {
            var iOSCell = new iOSLayoutTableCell(iOSColumnList[InvCell.Column.Index], iOSRowList[InvCell.Row.Index], TranslatePanel(InvCell.Content));
            iOSCellList.Add(iOSCell);
          }

          iOSTable.ComposeElements(iOSRowList, iOSColumnList, iOSCellList);
        }
      });

      return iOSNode;
    }

    private void TranslateLayout(Panel InvPanel, iOSLayoutContainer iOSLayout, iOSLayoutElement iOSElement)
    {
      var iOSView = iOSElement.View;

      if (InvPanel.Opacity.Render())
        iOSView.Alpha = InvPanel.Opacity.Get();

      var InvBackground = InvPanel.Background;
      if (InvBackground.Render())
        TranslateBackground(InvBackground.Colour, iOSView);

      var InvCorner = InvPanel.Corner;

      var InvBorder = InvPanel.Border;
      var RenderBorder = InvBorder.IsChanged;

      var RenderBorderOnCornerRadius = InvCorner.Render();
      if (InvBorder.Render())
        RenderBorderOnCornerRadius = true;

      if (RenderBorderOnCornerRadius)
      {
        var TranslatedColour = TranslateCGColor(InvBorder.Colour);

        if (InvBorder.IsUniform && InvCorner.IsUniform)
        {
          iOSView.Layer.BorderColor = TranslatedColour;
          iOSView.Layer.BorderWidth = InvBorder.Left;
          iOSView.Layer.CornerRadius = InvCorner.TopLeft;
          //iOSView.Layer.MasksToBounds = true;

          iOSElement.Border.TopLeftCornerRadius = 0;
          iOSElement.Border.TopRightCornerRadius = 0;
          iOSElement.Border.BottomRightCornerRadius = 0;
          iOSElement.Border.BottomLeftCornerRadius = 0;

          iOSElement.Border.LeftBorderThickness = 0;
          iOSElement.Border.TopBorderThickness = 0;
          iOSElement.Border.RightBorderThickness = 0;
          iOSElement.Border.BottomBorderThickness = 0;
        }
        else
        {
          iOSView.Layer.BorderWidth = 0;
          iOSView.Layer.CornerRadius = 0;
          //iOSView.Layer.MasksToBounds = false;

          iOSElement.Border.TopLeftCornerRadius = InvCorner.TopLeft;
          iOSElement.Border.TopRightCornerRadius = InvCorner.TopRight;
          iOSElement.Border.BottomRightCornerRadius = InvCorner.BottomRight;
          iOSElement.Border.BottomLeftCornerRadius = InvCorner.BottomLeft;

          iOSElement.Border.LeftBorderThickness = InvBorder.Left;
          iOSElement.Border.TopBorderThickness = InvBorder.Top;
          iOSElement.Border.RightBorderThickness = InvBorder.Right;
          iOSElement.Border.BottomBorderThickness = InvBorder.Bottom;

          iOSElement.Border.BorderColour = TranslatedColour;
        }

        iOSView.SetNeedsLayout();
      }

      var InvElevation = InvPanel.Elevation;
      if (InvElevation.Render())
      {
        var Depth = InvElevation.Get();

        if (Depth > 0)
        {
          iOSView.Layer.ShadowColor = TranslateCGColor(Inv.Colour.DimGray);
          iOSView.Layer.ShadowOffset = new CoreGraphics.CGSize(0, Depth);
          iOSView.Layer.ShadowRadius = Depth;
          iOSView.Layer.ShadowOpacity = 0.7F;
        }
        else
        {
          //iOSView.Layer.ShadowColor = null;
          iOSView.Layer.ShadowOffset = CoreGraphics.CGSize.Empty;
          iOSView.Layer.ShadowRadius = 0;
          iOSView.Layer.ShadowOpacity = 0.0F;
        }
      }

      var InvMargin = InvPanel.Margin;

      if (InvMargin.Render())
      {
        var iOSMargins = InvPanel.Visibility.Get() ? new UIKit.UIEdgeInsets(InvMargin.Top, InvMargin.Left, InvMargin.Bottom, InvMargin.Right) : UIKit.UIEdgeInsets.Zero;

        //if (iOSLayout.LayoutMargins != iOSMargins)
        {
          iOSLayout.LayoutMargins = iOSMargins;
          iOSLayout.Arrange();
        }
      }

      var InvPadding = InvPanel.Padding;
      if (InvPadding.Render() || RenderBorder)
      {
        iOSView.LayoutMargins = new UIKit.UIEdgeInsets(InvPadding.Top + InvPanel.Border.Top, InvPadding.Left + InvPanel.Border.Left, InvPadding.Bottom + InvPanel.Border.Bottom, InvPadding.Right + InvPanel.Border.Right);
        iOSView.Arrange();
      }

      TranslateVisibilitySizeAlignment(InvPanel, iOSLayout, iOSElement);
    }
    private void TranslateVisibilitySizeAlignment(Inv.Panel InvPanel, iOSLayoutContainer iOSLayout, iOSLayoutElement iOSElement)
    {
      var InvVisibility = InvPanel.Visibility;
      if (InvVisibility.Render())
        iOSLayout.SetContentVisiblity(InvVisibility.Get());

      var InvSize = InvPanel.Size;
      if (InvSize.Render())
      {
        iOSLayout.SetContentWidth(InvSize.Width ?? float.NaN);
        iOSLayout.SetContentHeight(InvSize.Height ?? float.NaN);
        iOSLayout.SetContentMinimumWidth(InvSize.MinimumWidth ?? float.NaN);
        iOSLayout.SetContentMinimumHeight(InvSize.MinimumHeight ?? float.NaN);
        iOSLayout.SetContentMaximumWidth(InvSize.MaximumWidth ?? float.NaN);
        iOSLayout.SetContentMaximumHeight(InvSize.MaximumHeight ?? float.NaN);
      }

      var InvAlignment = InvPanel.Alignment;
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Placement.BottomStretch:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Bottom, iOSLayoutHorizontal.Stretch);
            break;

          case Placement.BottomLeft:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Bottom, iOSLayoutHorizontal.Left);
            break;

          case Placement.BottomCenter:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Bottom, iOSLayoutHorizontal.Center);
            break;

          case Placement.BottomRight:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Bottom, iOSLayoutHorizontal.Right);
            break;

          case Placement.TopStretch:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Top, iOSLayoutHorizontal.Stretch);
            break;

          case Placement.TopLeft:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Top, iOSLayoutHorizontal.Left);
            break;

          case Placement.TopCenter:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Top, iOSLayoutHorizontal.Center);
            break;

          case Placement.TopRight:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Top, iOSLayoutHorizontal.Right);
            break;

          case Placement.CenterStretch:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Center, iOSLayoutHorizontal.Stretch);
            break;

          case Placement.CenterLeft:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Center, iOSLayoutHorizontal.Left);
            break;

          case Placement.Center:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Center, iOSLayoutHorizontal.Center);
            break;

          case Placement.CenterRight:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Center, iOSLayoutHorizontal.Right);
            break;

          case Placement.Stretch:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Stretch, iOSLayoutHorizontal.Stretch);
            break;

          case Placement.StretchLeft:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Stretch, iOSLayoutHorizontal.Left);
            break;

          case Placement.StretchCenter:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Stretch, iOSLayoutHorizontal.Center);
            break;

          case Placement.StretchRight:
            iOSLayout.SetContentAlignment(iOSLayoutVertical.Stretch, iOSLayoutHorizontal.Right);
            break;

          default:
            throw new ApplicationException("Placement not handled: " + InvPanel.Alignment.Get());
        }
      }
    }
    private void TranslateBackground(Inv.Colour InvColour, UIKit.UIView iOSView)
    {
      iOSView.BackgroundColor = TranslateUIColor(InvColour);

      // TODO: this appears to have no effect?
      //iOSView.Opaque = true;// InvColour.IsOpaque;
    }
    private void TranslateFont(Inv.Font InvFont, Action<UIKit.UIFont, UIKit.UIColor> Action)
    {
      if (InvFont.Render())
      {
        var FontTypeface = TranslateUIFont(InvFont.Name, InvFont.Size ?? 14, InvFont.Weight);
        var FontColor = TranslateUIColor(InvFont.Colour ?? Inv.Colour.Black);

        Action(FontTypeface, FontColor);
      }
    }
    private Inv.Point TranslatePoint(CoreGraphics.CGPoint iOSPoint)
    {
      return new Inv.Point((int)iOSPoint.X, (int)iOSPoint.Y);
    }
    private UIKit.UIFont TranslateUIFont(Inv.Font InvFont)
    {
      return TranslateUIFont(InvFont.Name, InvFont.Size, InvFont.Weight);
    }
    private UIKit.UIImage TranslateUIImage(Inv.Image InvImage, int? PointWidth, int? PointHeight)
    {
      if (InvImage == null)
      {
        return null;
      }
      else
      {
        var Result = InvImage.Node as UIKit.UIImage;

        if (Result == null)
        {
          var Buffer = InvImage.GetBuffer();

          Result = UIKit.UIImage.LoadFromData(Foundation.NSData.FromArray(Buffer), 3.0F);

          if (InvImage.Node == null || !ImageList.Contains(InvImage))
            ImageList.Add(InvImage);

          InvImage.Node = Result;
        }

        return Result;
      }
    }
    private iOSLayoutController AccessSurface(Inv.Surface InvSurface)
    {
      if (InvSurface.Node == null)
      {
        var Result = new iOSLayoutController();
        Result.KeyQuery += () =>
        {
          if (IsTransitioning)
            return iOSKeyboard.BlankCommandArray;
          else
            return iOSKeyboard.KeyCommandArray;
        };
        Result.KeyEvent += (Modifier, Key) =>
        {
          // TODO: track key up/down so we can track ctrl/alt/shift state with Window.KeyModifier.

          InvSurface.KeystrokeInvoke(new Keystroke() 
          { 
            Key = iOSKeyboard.TranslateKey(Key), 
            Modifier = iOSKeyboard.TranslateModifier(Modifier) 
          });
        };
        Result.MemoryWarningEvent += () =>
        {
#if DEBUG
          Reclamation();
#endif
        };
        Result.ResizeEvent += (Size) =>
        {
          InvSurface.Window.Application.Platform.WindowPost(() =>
          {
            Guard(() =>
            {
              var ScreenBounds = UIKit.UIScreen.MainScreen.Bounds;
              InvApplication.Window.Width = (int)Size.Width;
              InvApplication.Window.Height = (int)Size.Height;
              InvSurface.ArrangeInvoke();
            });
          });
        };
        Result.LeftEdgeSwipeEvent += () =>
        {
          if (InvSurface.Window.ActiveSurface == InvSurface)
            Guard(() => InvSurface.GestureBackwardInvoke());
        };
        Result.RightEdgeSwipeEvent += () =>
        {
          if (InvSurface.Window.ActiveSurface == InvSurface)
            Guard(() => InvSurface.GestureForwardInvoke());
        };
        Result.LoadEvent += () =>
        {
          Guard(() =>
          {
            if (!iOSRootController.ChildViewControllers.Contains(Result) && InvSurface.Window.ActiveTransition != null)
              InvSurface.ArrangeInvoke();

            ProcessTransition(Result);

            InvSurface.ComposeInvoke();

            UpdateSurface(InvSurface, Result);
          });
        };

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (iOSLayoutController)InvSurface.Node;
      }
    }

    private System.Timers.Timer AccessTimer(Inv.Timer InvTimer, Func<Inv.Timer, System.Timers.Timer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (System.Timers.Timer)InvTimer.Node;
      }
    }
    private iOSNode<TView> AccessPanel<TView>(Panel InvPanel, Func<TView> BuildFunction)
      where TView : iOSLayoutElement
    {
      if (InvPanel.Node == null)
      {
        var Result = new iOSNode<TView>();
        Result.LayoutView = new iOSLayoutContainer();
        Result.PanelView = BuildFunction();
        Result.LayoutView.SetContentElement(Result.PanelView);

        InvPanel.Node = Result;

        return Result;
      }
      else
      {
        return (iOSNode<TView>)InvPanel.Node;
      }
    }
    private void RenderPanel(Inv.Panel InvPanel, iOSNode iOSNode, Action Action)
    {
      if (InvPanel.Render())
        Action();
    }

    private Inv.EnumArray<Inv.PanelType, Func<Inv.Panel, iOSNode>> RouteArray;
    private Inv.EnumArray<Inv.FontWeight, UIKit.UIFontWeight> UIFontWeightArray;
    //private Inv.DistinctList<Inv.Colour> ColourList;
    private Inv.DistinctList<Inv.Image> ImageList;
    private Inv.DistinctList<Inv.Sound> SoundList;
    private CoreLocation.CLLocationManager iOSLocationManager;
    private CoreAnimation.CADisplayLink iOSDisplayLink;
    private UIKit.UINavigationController iOSRootController;
    private bool IsTransitioning;
    private EnumArray<FontWeight, string> FontFamilyArray;

    private sealed class iOSAnimationSet
    {
      public iOSAnimationSet()
      {
        this.ItemList = new DistinctList<iOSAnimationItem>();
      }

      public void Add(Inv.Panel InvPanel, UIKit.UIView iOSPanel, CoreAnimation.CAAnimation iOSAnimation)
      {
        var iOSAnimationKey = "a_" + AnimationCount++;
        iOSPanel.Layer.AddAnimation(iOSAnimation, iOSAnimationKey);

        ItemList.Add(new iOSAnimationItem(InvPanel, iOSPanel, iOSAnimationKey));
      }

      public readonly Inv.DistinctList<iOSAnimationItem> ItemList;

      private static int AnimationCount;
    }

    private sealed class iOSAnimationItem
    {
      public iOSAnimationItem(Inv.Panel InvPanel, UIKit.UIView iOSPanel, string iOSAnimationKey)
      {
        this.InvPanel = InvPanel;
        this.iOSPanel = iOSPanel;
        this.iOSAnimationKey = iOSAnimationKey;
      }

      public readonly Inv.Panel InvPanel;
      public readonly UIKit.UIView iOSPanel;
      public readonly string iOSAnimationKey;
    }
  }

  internal abstract class iOSNode
  {
    public iOSLayoutContainer LayoutView { get; set; }
    public iOSLayoutElement PanelView { get; set; }
  }

  internal sealed class iOSNode<T> : iOSNode
    where T : iOSLayoutElement
  {
    public new T PanelView
    {
      get { return (T)base.PanelView; }
      set { base.PanelView = value; }
    }
  }

  internal static class DateTimeExtensions
  {
    public static DateTime NSDateToDateTime(this Foundation.NSDate Source)
    {
      if (Source == null) 
        return DateTime.MinValue;

      var Calendar = Foundation.NSCalendar.CurrentCalendar;
      var Year = (int)Calendar.GetComponentFromDate(Foundation.NSCalendarUnit.Year, Source);
      var Month = (int)Calendar.GetComponentFromDate(Foundation.NSCalendarUnit.Month, Source);
      var Day = (int)Calendar.GetComponentFromDate(Foundation.NSCalendarUnit.Day, Source);
      var Hour = (int)Calendar.GetComponentFromDate(Foundation.NSCalendarUnit.Hour, Source);
      var Minute = (int)Calendar.GetComponentFromDate(Foundation.NSCalendarUnit.Minute, Source);
      var Second = (int)Calendar.GetComponentFromDate(Foundation.NSCalendarUnit.Second, Source);
      var Nanosecond = (int)Calendar.GetComponentFromDate(Foundation.NSCalendarUnit.Nanosecond, Source);
      var Millisecond = (Nanosecond / 1000000);

      return new DateTime(Year, Month, Day, Hour, Minute, Second, Millisecond, DateTimeKind.Local);
    }

    public static Foundation.NSDate DateTimeToNSDate(this DateTime Source)
    {
      if (Source == DateTime.MinValue) 
        return null;

      var LocalTime = Source.ToLocalTime();

      var Components = new Foundation.NSDateComponents()
      {
        Year = LocalTime.Year,
        Month = LocalTime.Month,
        Day = LocalTime.Day,
        Hour = LocalTime.Hour,
        Minute = LocalTime.Minute,
        Second = LocalTime.Second,
        Nanosecond = (LocalTime.Millisecond * 1000000)
      };

      return Foundation.NSCalendar.CurrentCalendar.DateFromComponents(Components);
    }
  }
}