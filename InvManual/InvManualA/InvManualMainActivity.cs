﻿/*! 4 !*/
using Android.App;
using Android.Widget;
using Android.OS;

namespace Inv.Manual
{
  [Activity(Label = "Invention Manual", Theme = "@android:style/Theme.NoTitleBar", MainLauncher = true, NoHistory = true)]
  public class SplashActivity : Inv.AndroidSplashActivity
  {
    protected override void OnCreate(Android.OS.Bundle bundle)
    {
      this.ImageResourceId = Inv.Manual.Resource.Drawable.splash;
      this.LaunchActivity = typeof(MainActivity);

      base.OnCreate(bundle);
    }
  }
  [Activity
    (
    Label = "Invention Manual", 
    MainLauncher = false, 
    //Theme = "@style/Theme.AppCompat.Light",
    Icon = "@drawable/icon", 
    ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize
    )
  ]
  public sealed class MainActivity : Inv.AndroidActivity
  {
    public override void Install(Inv.Application Application)
    {
      Inv.Manual.Shell.Main(Application);
    }
  }
}