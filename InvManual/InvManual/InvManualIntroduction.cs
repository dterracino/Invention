﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv.Manual
{
  internal sealed class IntroductionSurface : Inv.Mimic<Inv.Surface>
  {
    public IntroductionSurface(Application Application)
    {
      this.Base = Application.NewSurface();
      Base.Background.Colour = Theme.SheetColour;

      var OuterDock = Base.NewVerticalDock();
      Base.Content = OuterDock;

      var NavigatePanel = new NavigatePanel(Base);
      OuterDock.AddFooter(NavigatePanel);
      NavigatePanel.BackText = "< ABOUT";
      NavigatePanel.BackEvent += () => Application.BackAbout();
      NavigatePanel.NextEvent += () => Application.NextBook(Application.GetBooks().First());
      NavigatePanel.TitleIsEnabled = false;
      NavigatePanel.TitleText = "";

      var Scroll = Base.NewVerticalScroll();
      OuterDock.AddClient(Scroll);

      var Dock = Base.NewVerticalDock();
      Scroll.Content = Dock;

      var SubjectPanel = new SubjectPanel(Base);
      Dock.AddHeader(SubjectPanel);
      SubjectPanel.Text = Application.Title;

      this.Document = new DocumentPanel(Base);
      Dock.AddHeader(Document);

      var Stack = Base.NewVerticalStack();
      Dock.AddClient(Stack);
      Stack.Alignment.Center();

      Base.Background.Colour = Theme.SheetColour;
      Base.ArrangeEvent += () =>
      {
        NavigatePanel.NextText = Application.GetBooks().First().Title.ToUpper() + " >";

        Stack.RemovePanels();

        foreach (var Book in Application.GetBooks())
        {
          var BookButton = new BookButton(Base);
          Stack.AddPanel(BookButton);
          BookButton.Text = Book.Title;
          BookButton.Progress = Application.GetBookProgress(Book);
          BookButton.SelectEvent += () => Application.NextBook(Book);
        }

        var SearchPanel = new SearchPanel(Base);
        Stack.AddPanel(SearchPanel);
        SearchPanel.Alignment.Center();
        SearchPanel.SingleTapEvent += () => Application.JumpSearch();
      };
    }

    public DocumentPanel Document { get; private set; }
  }
}
