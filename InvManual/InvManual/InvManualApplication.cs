﻿/*! 22 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using Inv.Support;

namespace Inv.Manual
{
  internal sealed class Application : Inv.Mimic<Inv.Application>
  {
    public Application(Inv.Application Base)
    {
      this.Base = Base;
      this.Bookmark = new Bookmark();
      this.BookList = new Inv.DistinctList<Book>();

      Base.StartEvent += () =>
      {
        this.BookmarkFile = Base.Directory.RootFolder.NewFile("Bookmark.txt");

        this.Introduction = new IntroductionSurface(this);
        this.Conclusion = new ConclusionSurface(this);
        this.About = new AboutSurface(this);
        this.Search = new SearchSurface(this);

        if (BuildEvent != null)
          BuildEvent();

        // ensure the topics lists are populated.
        foreach (var Book in BookList)
          Book.Build();

#if DEBUG
        UniqueCheck("Book titles", BookList, B => B.Title);
        UniqueCheck("Topic subjects", BookList.SelectMany(B => B.GetTopics()), T => T.Subject);
#endif

        LoadBookmark();
      };
      Base.StopEvent += () =>
      {
        SaveBookmark();
      };
      Base.SuspendEvent += () =>
      {
        SaveBookmark();
      };
    }

    public string Title
    {
      get { return Base.Title; }
      set { Base.Title = value; }
    }
    public Bookmark Bookmark { get; private set; }
    public IntroductionSurface Introduction { get; private set; }
    public ConclusionSurface Conclusion { get; private set; }
    public AboutSurface About { get; private set; }

    public event Action BuildEvent;

    public Book AddBook(string Title, Action<Book> BuildEvent)
    {
      var Result = new Book(this, Title);
      Result.BuildEvent += () => BuildEvent(Result);

      BookList.Add(Result);

      return Result;
    }

    internal Inv.Surface NewSurface()
    {
      return Base.Window.NewSurface();
    }
    internal void JumpIntroduction()
    {
      TransitionIntroduction().Fade();
    }
    internal void NextIntroduction()
    {
      TransitionIntroduction().CarouselNext();
    }
    internal void BackIntroduction()
    {
      TransitionIntroduction().CarouselBack();
    }
    internal void NextConclusion()
    {
      TransitionConclusion().CarouselNext();
    }
    internal void BackAbout()
    {
      TransitionAbout().CarouselBack();
    }
    internal void JumpBook(Book Book)
    {
      TransitionBook(Book).Fade();
    }
    internal void BackBook(Book Book)
    {
      TransitionBook(Book).CarouselBack();
    }
    internal void NextBook(Book Book)
    {
      TransitionBook(Book).CarouselNext();
    }
    internal bool HasNextBook(Book Book)
    {
      return BookList.IndexOf(Book) < BookList.Count - 1;
    }
    internal Book GetNextBook(Book Book)
    {
      var Index = BookList.IndexOf(Book) + 1;

      return Index < BookList.Count ? BookList[Index] : null;
    }
    internal bool HasBackBook(Book Book)
    {
      return BookList.IndexOf(Book) > 0;
    }
    internal Book GetBackBook(Book Book)
    {
      var Index = BookList.IndexOf(Book) - 1;

      return Index >= 0 ? BookList[Index] : null;
    }
    internal IEnumerable<Book> GetBooks()
    {
      return BookList;
    }
    internal void BackTopic(Topic Topic)
    {
      Topic.Build();

      TransitionTopic(Topic).CarouselBack();
    }
    internal void NextTopic(Topic Topic)
    {
      Topic.Build();

      TransitionTopic(Topic).CarouselNext();
    }
    internal void JumpTopic(Topic Topic)
    {
      Topic.Build();

      TransitionTopic(Topic).Fade();
    }
    internal void JumpSearch()
    {
      TransitionSearch().Fade();
    }
    internal void ReadTopic(Topic Topic)
    {
      var Read = Bookmark.TopicReadDictionary.GetValueOrDefault(Topic.Subject);

     if (Read == null || Read.Version != Topic.Version)
        Bookmark.TopicReadDictionary[Topic.Subject] = new Read(Topic.Version, DateTimeOffset.Now);
    }
    internal void UnreadTopic(Topic Topic)
    {
      Bookmark.TopicReadDictionary.Remove(Topic.Subject);
    }
    internal Badge GetTopicBadge(Topic Topic)
    {
      return new Badge(Topic, Bookmark.TopicReadDictionary.GetValueOrDefault(Topic.Subject));
    }
    internal Progress GetBookProgress(Book Book)
    {
      var Read = 0;
      var Count = 0;

      foreach (var Topic in Book.GetTopics())
      {
        if (GetTopicBadge(Topic).IsRead)
          Read++;

        Count++;
      }

      return new Progress(Read, Count);
    }

    private Inv.Transition TransitionBook(Book Book)
    {
      Bookmark.LastLocation = Location.Book;
      Bookmark.LastBookTitle = Book.Title;
      Bookmark.LastTopicSubject = null;

      return Base.Window.Transition(Book);
    }
    private Inv.Transition TransitionTopic(Topic Topic)
    {
      Topic.Build();

      Bookmark.LastLocation = Location.Topic;
      Bookmark.LastBookTitle = Topic.Book.Title;
      Bookmark.LastTopicSubject = Topic.Subject;

      return Base.Window.Transition(Topic);
    }
    private Inv.Transition TransitionIntroduction()
    {
      Bookmark.LastLocation = Location.Introduction;
      Bookmark.LastBookTitle = null;
      Bookmark.LastTopicSubject = null;

      return Base.Window.Transition(Introduction);
    }
    private Inv.Transition TransitionConclusion()
    {
      Bookmark.LastLocation = Location.Conclusion;
      Bookmark.LastBookTitle = null;
      Bookmark.LastTopicSubject = null;

      return Base.Window.Transition(Conclusion);
    }
    private Inv.Transition TransitionSearch()
    {
      Bookmark.LastLocation = Location.Search;
      Bookmark.LastBookTitle = null;
      Bookmark.LastTopicSubject = null;

      return Base.Window.Transition(Search);
    }
    private Inv.Transition TransitionAbout()
    {
      Bookmark.LastLocation = Location.About;
      Bookmark.LastBookTitle = null;
      Bookmark.LastTopicSubject = null;

      return Base.Window.Transition(About);
    }
    private void SaveBookmark()
    {
      if (BookmarkFile != null)
      {
        try
        {
          BookmarkFile.AsText().AsSyntax(SyntaxGrammar.Reference).Write(Formatter =>
          {
            Formatter.WriteKeyword(SyntaxGrammar.MarkKeyword);
            Formatter.WriteSpace();
            Formatter.WriteKeyword(Bookmark.LastLocation.ToString().ToLower());

            switch (Bookmark.LastLocation)
            {
              case Location.Book:
                Formatter.WriteSpace();
                Formatter.WriteIdentifier(Bookmark.LastBookTitle);
                break;

              case Location.Topic:
                Formatter.WriteSpace();
                Formatter.WriteIdentifier(Bookmark.LastBookTitle);
                Formatter.WriteSpace();
                Formatter.WriteCloseAngle();
                Formatter.WriteSpace();
                Formatter.WriteIdentifier(Bookmark.LastTopicSubject);
                break;

              case Location.Introduction:
              case Location.Conclusion:
              case Location.Search:
              case Location.About:
                break;

              default:
                throw new Exception("Location not handled: " + Bookmark.LastLocation);
            }

            Formatter.WriteSemicolon();

            if (Bookmark.TopicReadDictionary.Count > 0)
            {
              Formatter.WriteLine();

              foreach (var Read in Bookmark.TopicReadDictionary)
              {
                Formatter.WriteLine();
                Formatter.WriteKeyword(SyntaxGrammar.ReadKeyword);
                Formatter.WriteSpace();
                Formatter.WriteIdentifier(Read.Key);
                Formatter.WriteColon();
                Formatter.WriteInteger(Read.Value.Version);
                Formatter.WriteAt();
                Formatter.WriteDateTimeOffset(Read.Value.Timestamp);
                Formatter.WriteSemicolon();
              }
            }
          });
        }
        catch
        {
          // TODO: exception logs.
        }
      }
    }
    private void LoadBookmark()
    {
      this.Bookmark = new Bookmark();

      try
      {
        if (BookmarkFile.Exists())
        {
          BookmarkFile.AsText().AsSyntax(SyntaxGrammar.Reference).Read(Extractor =>
          {
            Extractor.ReadKeywordValue(SyntaxGrammar.MarkKeyword);
            
            var LocationKeyword = Extractor.ReadKeyword();
            Bookmark.LastLocation = Inv.Support.EnumHelper.ParseOrDefault<Location>(LocationKeyword, null, true) ?? Location.Introduction;

            switch (Bookmark.LastLocation)
            {
              case Location.Book:
                Bookmark.LastBookTitle = Extractor.ReadIdentifier();

                if (Extractor.ReadOptionalKeywordValue(Location.Topic.ToString().ToLower()))
                  Bookmark.LastTopicSubject = Extractor.ReadIdentifier(); // NOTE: this is just for backwards compatibility.
                else
                  Bookmark.LastTopicSubject = null;
                break;

              case Location.Topic:
                Bookmark.LastBookTitle = Extractor.ReadIdentifier();
                Extractor.ReadCloseAngle();
                Bookmark.LastTopicSubject = Extractor.ReadIdentifier();
                break;

              case Location.Introduction:
              case Location.Conclusion:
              case Location.Search:
              case Location.About:
                Bookmark.LastBookTitle = null;
                Bookmark.LastTopicSubject = null;
                break;

              default:
                throw new Exception("Location not handled: " + Bookmark.LastLocation);          
            }

            Extractor.ReadSemicolon();

            while (Extractor.ReadOptionalKeywordValue(SyntaxGrammar.ReadKeyword))
            {
              var TopicSubject = Extractor.ReadIdentifier();
              Extractor.ReadColon();
              var TopicVersion = Extractor.ReadInteger32();
              Extractor.ReadAt();
              var TopicTimestamp = Extractor.ReadDateTimeOffset();
              Extractor.ReadSemicolon();

              Bookmark.TopicReadDictionary.Add(TopicSubject, new Read(TopicVersion, TopicTimestamp));
            }
          });
        }
      }
      catch
      {
        // TODO: log exception.
      }

      switch (Bookmark.LastLocation)
      {
        case Location.Introduction:
          Base.Window.Transition(Introduction);
          break;

        case Location.Conclusion:
          Base.Window.Transition(Conclusion);
          break;

        case Location.About:
          Base.Window.Transition(About);
          break;

        case Location.Search:
          Base.Window.Transition(Search);
          break;

        case Location.Book:
        case Location.Topic:
          var StartBook = BookList.Find(B => B.Title == Bookmark.LastBookTitle);
          var StartTopic = StartBook != null ? StartBook.GetTopics().Find(T => T.Subject == Bookmark.LastTopicSubject) : null;

          if (StartTopic != null)
            TransitionTopic(StartTopic);
          else if (StartBook != null)
            TransitionBook(StartBook);
          else
            Base.Window.Transition(Introduction);
          break;

        default:
          throw new Exception("Location not handled: " + Bookmark.LastLocation);
      }
    }
    private void UniqueCheck<TItem, TField>(string Title, IEnumerable<TItem> Items, Func<TItem, TField> FieldFunction)
    {
      var UniqueSet = new HashSet<TField>();
      var DuplicateSet = new HashSet<TField>();

      foreach (var Item in Items)
      {
        var Field = FieldFunction(Item);

        if (!UniqueSet.Add(Field))
          DuplicateSet.Add(Field);
      }

      if (DuplicateSet.Count > 0)
        throw new Exception(Title + " duplicates were found: " + DuplicateSet.OrderBy().Select(D => D.ToString()).AsSeparatedText(", "));
    }

    private Inv.File BookmarkFile;
    private Inv.DistinctList<Book> BookList;
    private SearchSurface Search;
  }

  internal sealed class Progress
  {
    internal Progress(int Index, int Count)
    {
      this.Index = Index;
      this.Count = Count;
      this.Percent = Count != 0 ? (float)Index / (float)Count : 0.0F;
    }

    public int Index { get; private set; }
    public int Count { get; private set; }
    public float Percent { get; private set; }

    public override string ToString()
    {
      return Index + " / " + Count;
    }

    public bool IsComplete()
    {
      return Index >= Count;
    }
  }

  internal sealed class Badge
  {
    internal Badge(Topic Topic, Read Read)
    {
      this.Topic = Topic;
      this.Read = Read;
    }

    public override string ToString()
    {
      if (IsRead)
      {
        return "read " + (DateTimeOffset.Now - Read.Timestamp).FormatTimeSpanShort() + " ago";
      }
      else if (IsUpdated)
      {
        return "updated!";
      }
      else
      {
        return "";
      }
    }

    public bool IsUnread
    {
      get { return Read == null; }
    }
    public bool IsRead
    {
      get { return Read != null && Read.Version == Topic.Version; }
    }
    public bool IsUpdated
    {
      get { return Read != null && Read.Version != Topic.Version; }
    }

    private Topic Topic;
    private Read Read;
  }

  internal enum Location
  {
    Introduction,
    Conclusion,
    Search,
    About,
    Book,
    Topic
  }

  internal sealed class Bookmark
  {
    public Bookmark()
    {
      this.TopicReadDictionary = new Dictionary<string, Read>();
    }

    public Location LastLocation { get; set; }
    public string LastBookTitle { get; set; }
    public string LastTopicSubject { get; set; }

    public Dictionary<string, Read> TopicReadDictionary { get; private set; }
  }

  internal sealed class Read
  {
    internal Read(int Version, DateTimeOffset Timestamp)
    {
      this.Version = Version;
      this.Timestamp = Timestamp;
    }

    public int Version { get; private set; }
    public DateTimeOffset Timestamp { get; private set; }
  }

  internal sealed class Book : Inv.Mimic<Inv.Surface>
  {
    public Book(Application Application, string Title)
    {
      this.Application = Application;
      this.Title = Title;
      this.TopicList = new Inv.DistinctList<Topic>();
    }

    public Application Application { get; private set; }
    public string Title { get; private set; }
    public DocumentPanel Document { get; private set; }

    public Topic AddTopic(int Version, string Subject, Action<Topic> BuildEvent)
    {
      var Result = new Topic(this, Version, Subject);
      Result.BuildEvent += () => BuildEvent(Result);

      TopicList.Add(Result);

      return Result;
    }

    internal event Action BuildEvent;

    internal void Build()
    {
      this.Base = Application.NewSurface();

      var OuterDock = Base.NewVerticalDock();
      Base.Content = OuterDock;

      var Scroll = Base.NewVerticalScroll();
      OuterDock.AddClient(Scroll);

      var Dock = Base.NewVerticalDock();
      Scroll.Content = Dock;

      var SubjectPanel = new SubjectPanel(Base);
      Dock.AddHeader(SubjectPanel);
      SubjectPanel.Text = Title;

      this.Document = new DocumentPanel(Base);
      Dock.AddHeader(Document);

      var Stack = Base.NewVerticalStack();
      Dock.AddClient(Stack);
      Stack.Alignment.Center();

      var NavigatePanel = new NavigatePanel(Base);
      OuterDock.AddFooter(NavigatePanel);
      NavigatePanel.TitleEvent += () => Application.JumpIntroduction();
      NavigatePanel.BackEvent += () =>
      {
        if (HasBackBook())
          Application.BackTopic(GetBackBook().GetTopics().Last());
        else
          Application.BackIntroduction();
      };
      NavigatePanel.NextEvent += () => Application.NextTopic(GetTopics().First());

      Base.Background.Colour = Theme.SheetColour;
      Base.GestureBackwardEvent += () => NavigatePanel.Back();
      Base.GestureForwardEvent += () => NavigatePanel.Next();
      Base.ArrangeEvent += () =>
      {
        if (Base.Window.Width < 538)
          NavigatePanel.TitleText = "▲";
        else
          NavigatePanel.TitleText = Application.Title.ToUpper();

        Stack.RemovePanels();

        if (HasBackBook())
          NavigatePanel.BackText = "< " + GetBackBook().GetTopics().Last().Subject.ToUpper();
        else
          NavigatePanel.BackText = "< MANUAL";
        NavigatePanel.NextText = GetTopics().First().Subject.ToUpper() + " >";

        foreach (var Topic in GetTopics())
        {
          var TopicButton = new TopicButton(Base);
          Stack.AddPanel(TopicButton);
          TopicButton.Badge = Application.GetTopicBadge(Topic);
          TopicButton.Text = Topic.Subject;
          TopicButton.SelectEvent += () => Application.NextTopic(Topic);
        }
      };

      if (BuildEvent != null)
        BuildEvent();
    }
    internal bool HasBackTopic(Topic Topic)
    {
      return TopicList.IndexOf(Topic) > 0;
    }
    internal Topic GetBackTopic(Topic Topic)
    {
      var TopicIndex = TopicList.IndexOf(Topic) - 1;

      return TopicIndex >= 0 ? TopicList[TopicIndex] : null;
    }
    internal bool HasNextTopic(Topic Topic)
    {
      return TopicList.IndexOf(Topic) < TopicList.Count - 1;
    }
    internal Topic GetNextTopic(Topic Topic)
    {
      var TopicIndex = TopicList.IndexOf(Topic) + 1;

      return TopicIndex < TopicList.Count ? TopicList[TopicIndex] : null;
    }
    internal IEnumerable<Topic> GetTopics()
    {
      return TopicList;
    }
    internal bool HasNextBook()
    {
      return Application.HasNextBook(this);
    }
    internal Book GetNextBook()
    {
      return Application.GetNextBook(this);
    }
    internal bool HasBackBook()
    {
      return Application.HasBackBook(this);
    }
    internal Book GetBackBook()
    {
      return Application.GetBackBook(this);
    }

    private Inv.DistinctList<Topic> TopicList;
  }

  internal sealed class Topic : Inv.Mimic<Inv.Surface>
  {
    public Topic(Book Book, int Version, string Subject)
    {
      this.Book = Book;
      this.Version = Version;
      this.Subject = Subject;
      this.ControlPanelList = new Inv.DistinctList<ControlPanel>();
      this.PalettePanelList = new Inv.DistinctList<PalettePanel>();
    }

    public Book Book { get; private set; }
    public int Version { get; private set; }
    public string Subject { get; private set; }
    public DocumentPanel Document { get; private set; }
    public DialogPanel Dialog { get; private set; }

    internal event Action BuildEvent;

    internal void Build()
    {
      if (!IsBuilt)
      {
        this.IsBuilt = true;

        this.Base = Book.Application.NewSurface();
        Base.Background.Colour = Theme.SheetColour;

        this.Dialog = new DialogPanel(Base);
        Base.Content = Dialog;

        var OuterDock = Base.NewVerticalDock();
        Dialog.AddLayer(OuterDock);

        var NavigatePanel = new NavigatePanel(Base);
        OuterDock.AddFooter(NavigatePanel);
        NavigatePanel.BackEvent += () =>
        {
          if (Book.HasBackTopic(this))
            Book.Application.BackTopic(Book.GetBackTopic(this));
          else
            Book.Application.BackBook(Book);
        };
        NavigatePanel.NextEvent += () =>
        {
          if (Book.HasNextTopic(this))
            Book.Application.NextTopic(Book.GetNextTopic(this));
          else if (Book.HasNextBook())
            Book.Application.NextBook(Book.GetNextBook());
          else
            Book.Application.NextConclusion();            
        };
        NavigatePanel.TitleEvent += () => Book.Application.JumpBook(Book);

        var Scroll = Base.NewVerticalScroll();
        OuterDock.AddClient(Scroll);

        this.TopicDock = Base.NewVerticalDock();
        Scroll.Content = TopicDock;

        this.SubjectPanel = new SubjectPanel(Base);
        TopicDock.AddHeader(SubjectPanel);

        this.Document = new DocumentPanel(Base);
        TopicDock.AddHeader(Document);

        this.SampleStack = Base.NewVerticalStack();
        TopicDock.AddClient(SampleStack);
        SampleStack.Alignment.Center();

        if (BuildEvent != null)
          BuildEvent();

        SubjectPanel.Text = Subject;

        if (Book.HasBackTopic(this))
          NavigatePanel.BackText = Book.HasBackTopic(this) ? "< " + Book.GetBackTopic(this).Subject.ToUpper() : "";
        else 
          NavigatePanel.BackText = "< " + Book.Title.ToUpper();

        if (Book.HasNextTopic(this))
          NavigatePanel.NextText = Book.GetNextTopic(this).Subject.ToUpper() + " >";
        else if (Book.HasNextBook())
          NavigatePanel.NextText = Book.GetNextBook().Title.ToUpper() + " >";
        else
          NavigatePanel.NextText = "THE END >";

        // TODO: autoplay?
        if (ControlPanelList.Count > 0)
        {
          //ControlPanelList[0].Autoplay();
        }

        Base.GestureBackwardEvent += () => NavigatePanel.Back();
        Base.GestureForwardEvent += () => NavigatePanel.Next();
        Base.ArrangeEvent += () =>
        {
          SubjectPanel.Badge = Book.Application.GetTopicBadge(this);

          Book.Application.ReadTopic(this);

          if (Base.Window.Width < 538)
            NavigatePanel.TitleText = "▲";
          else
            NavigatePanel.TitleText = Book.Title.ToUpper();

          var ChildWidth = Base.Window.Width - (Theme.DocumentGap * 2);

          foreach (var ControlPanel in ControlPanelList)
            ControlPanel.Arrange(ChildWidth);

          foreach (var PalettePanel in PalettePanelList)
            PalettePanel.Arrange(ChildWidth);

          Refresh();
        };
      }
    }

    public ControlPanel AddControlPanel(string Title)
    {
      var Result = new ControlPanel(Base, Title);
      TopicDock.AddFooter(Result);
      Result.Margin.Set(Theme.DocumentGap, 0, Theme.DocumentGap, Theme.DocumentGap);
      Result.SelectEvent += () =>
      {
        Refresh();
      };

      ControlPanelList.Add(Result);

      return Result;
    }
    public PalettePanel AddPalettePanel()
    {
      var Result = new PalettePanel(Base);
      SampleStack.AddPanel(Result);
      Result.Margin.Set(Theme.DocumentGap, 0, Theme.DocumentGap, Theme.DocumentGap);

      PalettePanelList.Add(Result);

      return Result;
    }
    public void AddPanel(Inv.Panel Panel)
    {
      SampleStack.AddPanel(Panel);
    }
    public PreviewPanel SetPreviewPanel()
    {
      var Result = new PreviewPanel(Base);
      SampleStack.AddPanel(Result);

      this.PreviewPanel = Result;

      return Result;
    }
    public CodePanel SetCodePanel()
    {
      var Result = new CodePanel(Base);
      SampleStack.AddPanel(Result);
      Result.Margin.Set(0, Theme.DocumentGap, 0, Theme.DocumentGap);

      this.CodePanel = Result;

      return Result;
    }
    public void Refresh()
    {
      foreach (var ControlPanel in ControlPanelList)
        ControlPanel.Refresh();

      if (PreviewPanel != null && CodePanel != null)
      {
        var X = PreviewPanel.GetExamplePanel();

        var LineList = new Inv.DistinctList<string>();

        // NOTE: this should be in the same order as the topics.

        if (X.Background.Colour != null)
          LineList.Add("Background.Set(Colour." + X.Background.Colour.Name + ");");

        var Placement = X.Alignment.Get();
        if (Placement != Inv.Placement.Stretch)
          LineList.Add("Alignment." + Placement.ToString() + "();");

        if (X.Size.Width != null || X.Size.Height != null)
        {
          if (X.Size.Width != null && X.Size.Height != null)
            LineList.Add("Size.Set(" + X.Size.Width.Value + ", " + X.Size.Height.Value + ");");
          else if (X.Size.Width != null)
            LineList.Add("Size.SetWidth(" + X.Size.Width.Value + ");");
          else if (X.Size.Height != null)
            LineList.Add("Size.SetHeight(" + X.Size.Height.Value + ");");
        }

        if (X.Margin.IsSet)
        {
          if (X.Margin.IsUniform)
            LineList.Add("Margin.Set(" + X.Margin.Left + ");");
          else if (X.Margin.IsVertical && X.Margin.IsHorizontal)
            LineList.Add("Margin.Set(" + X.Margin.Left + ", " + X.Margin.Top + ");");
          else
            LineList.Add("Margin.Set(" + X.Margin.Left + ", " + X.Margin.Top + ", " + X.Margin.Right + ", " + X.Margin.Bottom + ");");
        }

        if (X.Padding.IsSet)
        {
          if (X.Padding.IsUniform)
            LineList.Add("Padding.Set(" + X.Padding.Left + ");");
          else if (X.Padding.IsVertical && X.Padding.IsHorizontal)
            LineList.Add("Padding.Set(" + X.Padding.Left + ", " + X.Padding.Top + ");");
          else
            LineList.Add("Padding.Set(" + X.Padding.Left + ", " + X.Padding.Top + ", " + X.Padding.Right + ", " + X.Padding.Bottom + ");");
        }

        if (X.Corner.IsSet)
        {
          if (X.Corner.IsUniform)
            LineList.Add("Corner.Set(" + X.Corner.TopLeft + ");");
          else
            LineList.Add("Corner.Set(" + X.Corner.TopLeft + ", " + X.Corner.TopRight + ", " + X.Corner.BottomRight + ", " + X.Corner.BottomLeft + ");");
        }

        if (X.Border.IsSet || X.Border.Colour != null)
        {
          if (X.Border.Colour != null)
            LineList.Add("Border.Colour = Colour." + X.Border.Colour.Name + ";");

          if (X.Border.IsUniform)
            LineList.Add("Border.Set(" + X.Border.Left + ");");
          else if (X.Border.IsVertical && X.Border.IsHorizontal)
            LineList.Add("Border.Set(" + X.Border.Left + ", " + X.Border.Top + ");");
          else
            LineList.Add("Border.Set(" + X.Border.Left + ", " + X.Border.Top + ", " + X.Border.Right + ", " + X.Border.Bottom + ");");
        }

        var Depth = X.Elevation.Get();
        if (Depth != 0)
          LineList.Add("Elevation.Set(" + Depth.ToString() + ");");

        var Percent = X.Opacity.Get();
        if (Percent != 1.0F)
          LineList.Add("Opacity.Set(" + Percent.ToString("F") + "F);");

        if (!X.Visibility.Get())
          LineList.Add("Visibility.Collapse();");

        var Label = X as Inv.Label;

        if (Label != null)
        {
          if (!Label.LineWrapping)
            LineList.Add("LineWrapping = false;");

          if (Label.Justification != Inv.Justification.Left)
            LineList.Add("Justify" + Label.Justification + "();");

          if (Label.Font.Name != null)
            LineList.Add("Font.Name = " + Label.Font.Name.ConvertToCSharpString() + ";");

          if (Label.Font.Size != null)
            LineList.Add("Font.Size = " + Label.Font.Size.Value + ";");

          if (Label.Font.Colour != null)
            LineList.Add("Font.Colour = Colour." + Label.Font.Colour.Name + ";");

          if (Label.Font.Weight != Inv.FontWeight.Regular)
            LineList.Add("Font." + Label.Font.Weight + "();");

          if (Label.Text != null)
            LineList.Add("Text = " + Label.Text.ConvertToCSharpString() + ";");
        }

        var XName = X.GetType().Name;

        CodePanel.Text = LineList.Select(L => XName + "." + L).AsSeparatedText(Environment.NewLine);
      }
    }

    private SubjectPanel SubjectPanel;
    private Inv.Dock TopicDock;
    private Inv.Stack SampleStack;
    private PreviewPanel PreviewPanel;
    private CodePanel CodePanel;
    private Inv.DistinctList<ControlPanel> ControlPanelList;
    private Inv.DistinctList<PalettePanel> PalettePanelList;
    private bool IsBuilt;
  }
}