﻿/*! 4 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Inv.Manual
{
  public static class ServerProgram
  {
    [STAThread]
    static void Main(string[] args)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.FullScreenMode = false;
        Inv.WpfShell.DefaultWindowWidth = 400;
        Inv.WpfShell.DefaultWindowHeight = 400;
        Inv.WpfShell.Run(A =>
        {
          A.Title = "Invention Manual Server";

          Action RefreshTenants = null;

          var Engine = Inv.ServerShell.NewEngine(A, (Identity, Application) => Inv.Manual.Shell.Main(Application));
          Engine.AcceptEvent += (Tenant) =>
          {
            RefreshTenants();
          };
          Engine.RejectEvent += (Tenant) =>
          {
            RefreshTenants();
          };

          A.StartEvent += () =>
          {
            A.Window.Background.Colour = Inv.Colour.WhiteSmoke;

            var HostSurface = A.Window.NewSurface();
            A.Window.Transition(HostSurface);

            var TenantStack = HostSurface.NewVerticalStack();
            HostSurface.Content = TenantStack;

            RefreshTenants = () =>
            {
              var TenantArray = Engine.GetTenants().Select(S => new
              {
                Title = S.InvApplication.Title,
              }).ToArray();

              A.Window.Post(() =>
              {
                TenantStack.RemovePanels();

                foreach (var Tenant in TenantArray)
                {
                  var HostButton = HostSurface.NewButton();
                  TenantStack.AddPanel(HostButton);
                  HostButton.Background.Colour = Inv.Colour.DimGray;
                  HostButton.Padding.Set(5);
                  HostButton.Margin.Set(0, 0, 0, 2);

                  var HostLabel = HostSurface.NewLabel();
                  HostButton.Content = HostLabel;
                  HostLabel.Font.Size = 20;
                  HostLabel.Font.Colour = Inv.Colour.White;
                  HostLabel.Text = Tenant.Title;
                }
              });
            };

            Engine.Start();

            StartClient(Guid.NewGuid(), true);
            Thread.Sleep(1000);
            StartClient(Guid.NewGuid(), false);
          };
          A.StopEvent += () =>
          {
            Engine.Stop();
          };
        });
      });
    }

    private static void StartClient(Guid Identity, bool First)
    {
      var Thread = new Thread(() =>
      {
        var ScreenWidth = Inv.WpfShell.PrimaryScreenWidth; 
        var ScreenHeight = Inv.WpfShell.PrimaryScreenHeight; 

        var ClientWidth = Math.Min(1024, ScreenWidth / 2);
        var ClientHeight = Math.Min(1600, ScreenHeight - 40);

        Inv.WpfShell.FullScreenMode = false;
        Inv.WpfShell.DefaultWindowX = First ? 0 : ScreenWidth - ClientWidth;
        Inv.WpfShell.DefaultWindowY = 0;
        Inv.WpfShell.DefaultWindowWidth = ClientWidth;
        Inv.WpfShell.DefaultWindowHeight = ClientHeight;

        Inv.WpfShell.Run(A => new Inv.ServerApplication(A, Identity));
      });
      Thread.IsBackground = true;
      Thread.SetApartmentState(ApartmentState.STA);
      Thread.Start();
    }
  }
}
