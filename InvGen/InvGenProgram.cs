﻿/*! 13 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;
using System.IO;

namespace Inv.Gen
{
  public static class Program
  {
    static void Main(string[] args)
    {
      if (args.Length != 1)
      {
        Console.WriteLine("Exactly one argument must be provided.");
        return;
      }

      var Argument = args[0];

      if (Path.GetExtension(Argument).ToLower() == ".csproj")
      {
        var Project = new Inv.CSharpProject();
        Project.LoadFromFile(Argument);

        foreach (var Reference in Project.IncludeReference)
        {
          if (Path.GetExtension(Reference.File.Path).ToLower() == ".invresourcepackage")
          {
            Argument = Path.Combine(Path.GetDirectoryName(Argument), Reference.File.Path);
            break;
          }
        }

        if (Argument == args[0])
        {
          //Console.WriteLine("Install the Invention VSIX if you want to add a resource package to project " + Argument);
          return;
        }        
      }
      else if (Path.GetExtension(Argument).ToLower() != ".invresourcepackage")
      {
        Console.WriteLine("Argument must be a .InvResourcePackage or a .csproj that contains a .InvResourcePackage: " + Argument);
        return;
      }
      
      if (!System.IO.File.Exists(Argument))
      {
        Console.WriteLine("Argument file path does not exist: " + Argument);
        Environment.ExitCode = 1;
        return;
      }

      var ArgumentFilePath = GetExactPathName(Argument); // NOTE: VisualStudio right click 'Shell (Open)' makes the argument lower case for some reason.
      var SourceFolderPath = Path.GetDirectoryName(ArgumentFilePath);
      var RootFolderName = Path.GetFileName(SourceFolderPath);
      var DataNamespace = Path.GetFileNameWithoutExtension(ArgumentFilePath);
      var CodeNamespace = System.IO.File.ReadAllText(ArgumentFilePath).Trim().EmptyAsNull() ?? DataNamespace;
      var RsFilePath = Argument + ".rs";
      var CsFilePath = Argument + ".cs";

      var ResourcePackage = new Inv.Resource.Package()
      {
        DirectoryList = new Inv.DistinctList<Inv.Resource.Directory>()
      };

      foreach (var DirectoryInfo in new DirectoryInfo(SourceFolderPath).GetDirectories())
      {
        var ResourceDirectory = new Inv.Resource.Directory();
        ResourceDirectory.Name = DirectoryInfo.Name.ConvertToCSharpIdentifier();
        ResourceDirectory.FileList = new Inv.DistinctList<Inv.Resource.File>();

        foreach (var FileInfo in DirectoryInfo.GetFiles())
        {
          var Name = Path.GetFileNameWithoutExtension(FileInfo.Name).Replace('.', ' ').Replace('-', ' ').PascalCaseToTitleCase().ToTitleCase().ConvertToCSharpIdentifier();
          var Content = new Inv.Binary(System.IO.File.ReadAllBytes(FileInfo.FullName), FileInfo.Extension);
          var Format = Inv.Resource.Format.Binary;

          switch (FileInfo.Extension.ToLower())
          {
            case ".mp3":
            case ".wav":
              Format = Inv.Resource.Format.Sound;
              break;

            case ".png":
            case ".jpg":
            case ".jpeg":
            case ".bmp":
            case ".tif":
            case ".tiff":
            case ".gif":
              Format = Inv.Resource.Format.Image;
              break;

            case ".txt":
            case ".csv":
              Format = Inv.Resource.Format.Text;
              break;

            default:
              System.Diagnostics.Debug.WriteLine("File not handled" + FileInfo.Extension);
              break;
          }

          ResourceDirectory.FileList.Add(new Inv.Resource.File()
          {
            Name = Name,
            Format = Format,
            Content = Content
          });
        }

        if (ResourceDirectory.FileList.Count > 0)
          ResourcePackage.DirectoryList.Add(ResourceDirectory);
      }

      var ResourceGovernor = new Inv.Resource.Governor();

      using (var MemoryStream = new MemoryStream())
      {
        ResourceGovernor.Save(ResourcePackage, MemoryStream);

        MemoryStream.Flush();

        var RsFileInfo = new FileInfo(RsFilePath);

        var OldData = RsFileInfo.Exists ? System.IO.File.ReadAllBytes(RsFilePath) : null;
        var NewData = MemoryStream.ToArray();

        if (OldData == null || !OldData.ShallowEqualTo(NewData))
        {
          if (RsFileInfo.Exists && RsFileInfo.IsReadOnly)
            RsFileInfo.IsReadOnly = false;

          System.IO.File.WriteAllBytes(RsFilePath, NewData);
        }
      }

      var FormatTypeArray = new Inv.EnumArray<Inv.Resource.Format, string>
      {
        { Inv.Resource.Format.Text, "string" },
        { Inv.Resource.Format.Sound, "global::Inv.Sound" },
        { Inv.Resource.Format.Image, "global::Inv.Image" },
        { Inv.Resource.Format.Binary, "global::Inv.Binary" }
      };

      var CsFileInfo = new FileInfo(CsFilePath);

      var OldText = (string)null;
      var Version = "";

      if (CsFileInfo.Exists)
      {
        OldText = System.IO.File.ReadAllText(CsFileInfo.FullName);

        var StartIndex = OldText.IndexOf("/*");
        if (StartIndex >= 0)
        {
          var EndIndex = OldText.IndexOf("*/");

          if (StartIndex < EndIndex)
            Version = OldText.Substring(StartIndex + 3, EndIndex - StartIndex - 4);
        }
      }

      string NewText;

      using (var StringWriter = new StringWriter())
      {
        if (Version != "")
        {
          StringWriter.WriteLine("/*!" + Version + "!*/");
          StringWriter.WriteLine("");
        }
        StringWriter.WriteLine("namespace " + CodeNamespace);
        StringWriter.WriteLine("{");
        StringWriter.WriteLine("  public static class {0}", RootFolderName);
        StringWriter.WriteLine("  {");
        StringWriter.WriteLine("    static {0}()", RootFolderName);
        StringWriter.WriteLine("    {");
        StringWriter.WriteLine("      global::Inv.Resource.Foundation.Import(typeof({0}), \"{0}.{1}.InvResourcePackage.rs\");", RootFolderName, DataNamespace);
        StringWriter.WriteLine("    }");
        StringWriter.WriteLine("");
        foreach (var Directory in ResourcePackage.DirectoryList)
          StringWriter.WriteLine("    public static readonly {0}{1} {1};", RootFolderName, Directory.Name);
        StringWriter.WriteLine("  }");

        foreach (var ResourceDirectory in ResourcePackage.DirectoryList)
        {
          StringWriter.WriteLine("");
          StringWriter.WriteLine("  public sealed class {0}{1}", RootFolderName, ResourceDirectory.Name);
          StringWriter.WriteLine("  {");
          StringWriter.WriteLine("    public {0}{1}() {{ }}", RootFolderName, ResourceDirectory.Name);
          StringWriter.WriteLine("");
          foreach (var ResourceFile in ResourceDirectory.FileList)
          {
            StringWriter.WriteLine("    ///<Summary>{0:F1} KB</Summary>", ResourceFile.Content.GetLength() / 1024.0);
            StringWriter.WriteLine("    public readonly {0} {1};", FormatTypeArray[ResourceFile.Format], ResourceFile.Name);
          }
          StringWriter.WriteLine("  }");
        }
        StringWriter.Write("}");

        NewText = StringWriter.ToString();
      }

      if (OldText == null || OldText != NewText)
      {
        if (CsFileInfo.Exists && CsFileInfo.IsReadOnly)
          CsFileInfo.IsReadOnly = false;

        System.IO.File.WriteAllText(CsFileInfo.FullName, NewText);
      }
    }

    static string GetExactPathName(string pathName)
    {
      if (!(System.IO.File.Exists(pathName) || System.IO.Directory.Exists(pathName)))
        return pathName;

      var di = new DirectoryInfo(pathName);

      if (di.Parent != null)
        return Path.Combine(GetExactPathName(di.Parent.FullName), di.Parent.GetFileSystemInfos(di.Name)[0].Name);
      else
        return di.Name.ToUpper();
    }
  }
}
