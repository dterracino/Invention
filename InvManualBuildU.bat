rem BUILD **INVENTION MANUAL** UNIVERSAL WINDOWS

@echo off
attrib -r "C:\Development\Forge\Inv\InvManual\InvManual\Resources\Texts\Version.txt"
Set YEAR=%DATE:~10,4%
Set MONTH=%DATE:~7,2%
Set DAY=%DATE:~4,2%
Set HOUR=%TIME:~0,2%
Set HOUR=%HOUR: =0%
Set MIN=%TIME:~3,2%
Set DTS=%YEAR%.%MONTH%%DAY%.%HOUR%%MIN%
echo echo|set/p="u%DTS%">"C:\Development\Forge\Inv\InvManual\InvManual\Resources\Texts\Version.txt"
@echo on

"C:\Development\Forge\Inv\InvGen\bin\Debug\InvGen.exe" "C:\Development\Forge\Inv\InvManual\InvManual\Resources\InvManual.InvResourcePackage"

@echo off
echo ******************************************************************
@echo Now you can right click on InvManualU, Store, Create App Packages
echo ******************************************************************
@echo on

pause