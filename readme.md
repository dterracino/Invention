**Invention**

Develop native apps for iOS, Android and Windows with 100% code sharing using Visual Studio and C#.NET. Download the Invention Manual app for your device:

* [iOS](mailto:hodgskin.callan@gmail) (send email request to join External Beta with TestFlight).
* [Android](https://play.google.com/store/apps/details?id=com.x10host.invention.manual) (Open Beta on Google Play)
* [Windows](https://www.microsoft.com/en-us/store/p/invention-manual/9nj614c9m60v) (Published to Windows Store)

Supported platforms

	iOS			iTunes App Store
	ANDROID		Google Play* (or .apk file)
	WINRT		Windows Store
	WPF			Windows Desktop
	SERVER      Remote client-server over TCP sockets

Write your app code in portable class libraries (PCLs) using the Invention API.  Your app code runs consistently on each platform without additional effort.  Build your app once and deliver to the three major app stores.
User interface can be thought as an arrangement of rectangular panels. There are panels for layout such as stacks, docks and overlays.  There are panels for function such as buttons, labels, graphics, edits and memos.  There is even a panel for custom rendering at 60 fps. All panels have background, borders, corners, margins, padding, size, alignment, opacity and visibility.  

Invention maps these panels to the appropriate native control on each platform.  Default styling is stripped from each platform and you have full artistic control of your app.  Invention also consolidates common services across the platforms.

	AUDIO		mp3 playback and sound effects.
	EMAIL		start email messages and attach files
	LOCATION	global positioning and reverse geocoding
	PHONE		dial phone numbers and start sending text messages
	DIRECTORY	local storage of folders and files
	WEB			download from the web, use REST APIs and Json

Rapidly develop your app using the Windows Desktop platform.  This platform is based on the Windows Presentation Foundation (WPF) and is fast to compile and run in Visual Studio.  There are modes to emulate devices so you can easily refine the experience across all devices. Android and iOS applications require Xamarin (commercial license). Windows Store applications are built on the Windows Runtime (WinRT).

**Invention is open source under the MIT license.**

The flagship app is a free game called Pathos (http://pathos.x10host.com).  Pathos is a turn-based dungeon adventure game based on the rules of Nethack.  Click the above link or search for it in Apple iTunes, Google Play and the Windows Store.




	
