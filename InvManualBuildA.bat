rem DID YOU REMEMBER TO INCREMENT THE BUILD NUMBER?

pause

rem BUILD **INVENTION MANUAL** ANDROID

@echo off
attrib -r "C:\Development\Forge\Inv\InvManual\InvManual\Resources\Texts\Version.txt"
Set YEAR=%DATE:~10,4%
Set MONTH=%DATE:~7,2%
Set DAY=%DATE:~4,2%
Set HOUR=%TIME:~0,2%
Set HOUR=%HOUR: =0%
Set MIN=%TIME:~3,2%
Set DTS=%YEAR%.%MONTH%%DAY%.%HOUR%%MIN%
echo echo|set/p="a%DTS%">"C:\Development\Forge\Inv\InvManual\InvManual\Resources\Texts\Version.txt"
@echo on

"C:\Development\Forge\Inv\InvGen\bin\Debug\InvGen.exe" "C:\Development\Forge\Inv\InvManual\InvManual\Resources\InvManual.InvResourcePackage"

"C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe" C:\Development\Forge\Inv\Inv.sln /p:Configuration=Release;Platform="Any CPU" /t:Manual\InvManualA:Clean
"C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe" C:\Development\Forge\Inv\Inv.sln /p:Configuration=Release;Platform="Any CPU" /t:Manual\InvManualA
"C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe" C:\Development\Forge\Inv\InvManual\InvManualA\InvManualA.csproj /p:Configuration=Release;Platform="AnyCPU" /t:SignAndroidPackage

"C:\Program Files (x86)\Android\android-sdk\build-tools\23.0.2\zipalign.exe" -f -v 4 "C:\Development\Forge\Inv\InvManual\InvManualA\bin\Release\com.x10host.invention.manual-Signed.apk" C:\Deployment\Inv\InventionManual.%dts%.apk