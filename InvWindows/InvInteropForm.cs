﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using Inv.Support;

namespace Inv
{
  public class InteropFormHost : HwndHost
  {
    public InteropFormHost(IntPtr FormHandle)
    {
      this.FormHandle = FormHandle;
      MessageHook += MessageHandler;
    }

    public IntPtr FormHandle { get; private set; }

    protected override HandleRef BuildWindowCore(HandleRef ParentHandleRef)
    {
      var HostHandle =
        Win32.User32.CreateWindowExW
        (
          0, CustomWndClassName, string.Empty, Win32.User32.WindowStyles.WS_CHILD, 0, 0, 0, 0, ParentHandleRef.Handle, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero
        );
      if (HostHandle == IntPtr.Zero)
        throw new Win32Exception();

      var SetHandle = Win32.User32.SetWindowLong(FormHandle, (int)Win32.User32.GetWindowLongFields.GWL_STYLE, (IntPtr)Win32.User32.WindowStyles.WS_CHILD);
      if (SetHandle == IntPtr.Zero)
        throw new Win32Exception();

      if (!Win32.User32.SetWindowPos
      (
        FormHandle,
        IntPtr.Zero,
        0, 0,
        0, 0,
        Win32.User32.SetWindowPosFlags.SWP_NOMOVE | Win32.User32.SetWindowPosFlags.SWP_NOSIZE | Win32.User32.SetWindowPosFlags.SWP_NOZORDER | Win32.User32.SetWindowPosFlags.SWP_FRAMECHANGED
      ))
        throw new Win32Exception();

      var ParentHandle = Win32.User32.SetParent(FormHandle, HostHandle);
      if (ParentHandle == IntPtr.Zero)
        throw new Win32Exception();

      return new HandleRef(this, HostHandle);
    }
    protected override void DestroyWindowCore(HandleRef hwndParent)
    {
      Win32.User32.DestroyWindow(hwndParent.Handle);
    }
    protected override void OnWindowPositionChanged(Rect rcBoundingBox)
    {
      if (Handle != IntPtr.Zero && FormHandle != IntPtr.Zero)
      {
        Win32.User32.SetWindowPos
        (
          Handle,
          IntPtr.Zero,
          (int)rcBoundingBox.X, (int)rcBoundingBox.Y,
          (int)rcBoundingBox.Width, (int)rcBoundingBox.Height,
          Win32.User32.SetWindowPosFlags.SWP_ASYNCWINDOWPOS | Win32.User32.SetWindowPosFlags.SWP_NOZORDER | Win32.User32.SetWindowPosFlags.SWP_NOACTIVATE
        );

        Win32.User32.SetWindowPos
        (
          FormHandle,
          IntPtr.Zero,
          0, 0,
          (int)rcBoundingBox.Width, (int)rcBoundingBox.Height,
          Win32.User32.SetWindowPosFlags.SWP_ASYNCWINDOWPOS | Win32.User32.SetWindowPosFlags.SWP_NOZORDER | Win32.User32.SetWindowPosFlags.SWP_NOACTIVATE
        );
      }
    }
    protected override IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
    {
      handled = false;
      return IntPtr.Zero;
    }

    private IntPtr MessageHandler(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
    {
      handled = false;
      return IntPtr.Zero;
    }

    private const string CustomWndClassName = "Custom";

    static InteropFormHost()
    {
      WndClass = new Win32.User32.WNDCLASS();
      WndClass.lpszClassName = CustomWndClassName;
      WndClass.lpfnWndProc = Win32.User32.DefWindowProcW;
      WndClass.hbrBackground = IntPtr.Zero;
      var WndResult = Win32.User32.RegisterClassW(ref WndClass);
      if (WndResult == 0)
        throw new Win32Exception();
    }

    private static Win32.User32.WNDCLASS WndClass; // Must prevent garbage collection on WndClass.lpfnWndProc.
  }
}
