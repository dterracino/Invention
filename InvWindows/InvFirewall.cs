﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetFwTypeLib;
using Inv.Support;

namespace Inv
{
  public sealed class FirewallRuleManager
  {
    internal const int NET_FW_IP_PROTOCOL_TCP = 6;
    internal const int NET_FW_IP_PROTOCOL_UDP = 17;
    internal const int NET_FW_IP_PROTOCOL_ANY = 256;

    public void AddAllowRule(string Name, string Description, string ApplicationPath)
    {
      AddRule(Name, Description, ApplicationPath, NET_FW_ACTION_.NET_FW_ACTION_ALLOW);
    }
    public void AddRule(string Name, string Description, string ApplicationPath, NET_FW_ACTION_ Action)
    {
      var Rule = ProduceRule(Name, Description, ApplicationPath, Action);
      ProducePolicy().Rules.Add(Rule);
    }
    public void AddRule(string Name, string Description, string ApplicationPath, NET_FW_ACTION_ Action, string Port)
    {
      var Rule = ProduceRule(Name, Description, ApplicationPath, Action, Port);
      ProducePolicy().Rules.Add(Rule);
    }
    public void RemoveRule(string Name)
    {
      ProducePolicy().Rules.Remove(Name);
    }
    public bool RuleExistsByApplicationPath(string Path)
    {
      bool Result = false;

      var RuleEnumerator = ProducePolicy().Rules.GetEnumerator();
      {
        while (!Result && RuleEnumerator.MoveNext())
        {
          var Rule = RuleEnumerator.Current as INetFwRule;

          Result = Rule.ApplicationName == Path;
        }
      }
      
      return Result;
    }

    public void AllowApplication(string Name, string ApplicationPath) // For XP and Server 2003
    {
      var AuthorisedApplication = ProduceAuthorisedApplication();
      AuthorisedApplication.Enabled = true;
      AuthorisedApplication.Name = Name;
      AuthorisedApplication.ProcessImageFileName = ApplicationPath;
      AuthorisedApplication.Scope = NET_FW_SCOPE_.NET_FW_SCOPE_ALL;
      AuthorisedApplication.IpVersion = NET_FW_IP_VERSION_.NET_FW_IP_VERSION_ANY;

      ProduceFirewallManager().LocalPolicy.CurrentProfile.AuthorizedApplications.Add(AuthorisedApplication);
    }
    public void RemoveApplication(string ApplicationPath) // For XP and Server 2003
    {
      ProduceFirewallManager().LocalPolicy.CurrentProfile.AuthorizedApplications.Remove(ApplicationPath);
    }
    
    private INetFwRule ProduceRule(string Name, string Description, string ApplicationPath, NET_FW_ACTION_ Action, string Port = null)
    {
      var Rule = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
      Rule.Name = Name;
      Rule.Description = Description;
      Rule.Enabled = true;
      Rule.InterfaceTypes = "All";
      Rule.Profiles = ProducePolicy().CurrentProfileTypes;
      Rule.Action = Action;
      Rule.Protocol = NET_FW_IP_PROTOCOL_ANY;

      if (ApplicationPath != null)
        Rule.ApplicationName = ApplicationPath;

      if (Port.NullAsEmpty() != "")
        Rule.LocalPorts = Port;

      return Rule;
    }

    private INetFwMgr ProduceFirewallManager()
    {
      return (INetFwMgr)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwMgr"));
    }
    private INetFwAuthorizedApplication ProduceAuthorisedApplication()
    {
      return (INetFwAuthorizedApplication)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwAuthorizedApplication"));
    }
    private INetFwPolicy2 ProducePolicy()
    {
      return (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
    }
  }
}
