﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Diagnostics;
using Inv.Support;

namespace Inv
{
  public sealed class TaskThrottle : IDisposable
  {
    public TaskThrottle()
    {
      this.FireCriticalSection = new Inv.ExclusiveCriticalSection("Inv.Throttle-Fire");
      this.FireTimer = new Inv.TaskTimer();
      FireTimer.IntervalEvent += () =>
      {
        Action FireAction;

        using (FireCriticalSection.Lock())
        {
          FireTimer.Stop();

          if (IsFiring)
          {
            IsFiring = false;
            FireAction = FireEvent;

            FireTimer.IntervalTime = ThresholdTime;
            FireTimer.Start();
          }
          else
          {
            FireAction = null;
          }
        }

        if (FireAction != null)
          FireAction();
      };
    }
    public void Dispose()
    {
      if (!IsDisposed)
      {
        IsDisposed = true;
        FireTimer.Dispose();
      }
    }

    public TimeSpan ThresholdTime { get; set; }
    public event System.Action FireEvent;

    public void Fire()
    {
      CheckDisposed();

      using (FireCriticalSection.Lock())
      {
        if (!IsFiring)
        {
          IsFiring = true;

          if (!FireTimer.IsActive)
          {
            FireTimer.IntervalTime = TimeSpan.Zero;
            FireTimer.Start();
          }
        }
      }
    }

    [Conditional("DEBUG")]
    private void CheckDisposed()
    {
      if (IsDisposed)
        throw new ObjectDisposedException(GetType().FullName);
    }

    private bool IsDisposed;
    private Inv.ExclusiveCriticalSection FireCriticalSection;
    private Inv.TaskTimer FireTimer;
    private bool IsFiring;
  }

  public sealed class TaskThrottle<TContext> : IDisposable
  {
    public TaskThrottle()
    {
      this.ItemCriticalSection = new Inv.ExclusiveCriticalSection("Inv.Throttle-Item");
      this.ItemList = new Inv.DistinctList<TContext>();

      this.BaseThrottle = new TaskThrottle();
      BaseThrottle.FireEvent += () =>
      {
        Inv.DistinctList<TContext> CurrentList;
        using (ItemCriticalSection.Lock())
        {
          CurrentList = ItemList.ToDistinctList();
          ItemList.Clear();
        }

        if (FireEvent != null)
          FireEvent(CurrentList);
      };
    }
    public void Dispose()
    {
      BaseThrottle.Dispose();
    }

    public event System.Action<Inv.DistinctList<TContext>> FireEvent;
    public TimeSpan ThresholdTime 
    {
      get { return BaseThrottle.ThresholdTime; }
      set { BaseThrottle.ThresholdTime = value; }
    }

    public void Fire(TContext Item)
    {
      using (ItemCriticalSection.Lock())
        ItemList.Add(Item);

      BaseThrottle.Fire();
    }
    public void Fire(IEnumerable<TContext> FireList)
    {
      using (ItemCriticalSection.Lock())
        ItemList.AddRange(FireList);

      BaseThrottle.Fire();
    }
    public void Fire()
    {
      BaseThrottle.Fire();
    }

    private TaskThrottle BaseThrottle;
    private Inv.DistinctList<TContext> ItemList;
    private ExclusiveCriticalSection ItemCriticalSection;
  }

  public sealed class TaskTimer : IDisposable
  {
    public TaskTimer()
    {
    }
    public void Dispose()
    {
      if (!IsDisposed)
      {
        IsDisposed = true;

        if (TimerHandle != null)
          TimerHandle.Dispose();
      }
    }

    public TimeSpan? InitialTime { get; set; }
    public TimeSpan IntervalTime { get; set; }
    public bool IsActive
    {
      get
      {
        CheckDisposed();
        return IsActiveField;
      }
    }
    public event Action IntervalEvent;

    public void Start()
    {
      CheckDisposed();

      Debug.Assert(!IsActiveField, "Timer must not already be active.");

      IsActiveField = true;

      TimerHandle = new System.Threading.Timer(ExecuteMethod, null, InitialTime.HasValue ? InitialTime.Value : IntervalTime, IntervalTime);
    }
    public void Stop()
    {
      CheckDisposed();

      Debug.Assert(IsActiveField, "Timer is not started.");

      IsActiveField = false;

      if (TimerHandle != null)
      {
        TimerHandle.Dispose();
        TimerHandle = null;
      }
    }
    public void Restart()
    {
      CheckDisposed();

      Stop();
      Start();
    }

    private void ExecuteMethod(object StateInfo)
    {
      try
      {
        if (IsActiveField && !IsDisposed)
        {
          var Event = IntervalEvent;
          if (Event != null)
            Event();
        }
      }
      catch (Exception Exception)
      {
        // NOTE: unhandled exceptions in a timer event will now cause the timer to stop.
        IsActiveField = false;

        if (TimerHandle != null)
          TimerHandle.Dispose();
        
        TimerHandle = null;

        // TODO: is there a better way to handle exceptions here?
        if (Debugger.IsAttached)
          Debugger.Break();
        else
          Debug.Assert(false, Exception.Message);
      }
    }
    [Conditional("DEBUG")]
    private void CheckDisposed()
    {
      if (IsDisposed)
        throw new ObjectDisposedException(GetType().FullName);
    }

    private volatile bool IsActiveField;
    private volatile bool IsDisposed;
    private System.Threading.Timer TimerHandle;
  }
}
