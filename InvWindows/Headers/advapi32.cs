﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Win32
{
  public static class advapi32
  {
    // Registr.
    [Flags]
    public enum RegOption
    {
      NonVolatile = 0x0,
      Volatile = 0x1,
      CreateLink = 0x2,
      BackupRestore = 0x4,
      OpenLink = 0x8
    }

    [Flags]
    public enum RegSam
    {
      QueryValue = 0x0001,
      SetValue = 0x0002,
      CreateSubKey = 0x0004,
      EnumerateSubKeys = 0x0008,
      Notify = 0x0010,
      CreateLink = 0x0020,
      WOW64_32Key = 0x0200,
      WOW64_64Key = 0x0100,
      WOW64_Res = 0x0300,
      Read = 0x00020019,
      Write = 0x00020006,
      Execute = 0x00020019,
      AllAccess = 0x000f003f
    }

    public enum RegResult
    {
      CreatedNewKey = 0x00000001,
      OpenedExistingKey = 0x00000002
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SECURITY_ATTRIBUTES
    {
      int nLength;
      IntPtr lpSecurityDescriptor;
      int bInheritHandle;
    }

    public static uint HKEY_CLASSES_ROOT = 0x80000000;
    public static uint HKEY_CURRENT_USER = 0x80000001;
    public static uint HKEY_LOCAL_MACHINE = 0x80000002;
    public static uint HKEY_USERS = 0x80000003;
    public static uint HKEY_PERFORMANCE_DATA = 0x80000004;
    public static uint HKEY_CURRENT_CONFIG = 0x80000005;
    public static uint HKEY_DYN_DATA = 0x80000006;

    [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    public static extern long RegDeleteKeyTransacted(uint hkey, [MarshalAs(UnmanagedType.LPWStr)]string subkey, RegSam sam, uint reserved, IntPtr transaction, IntPtr reserved2);

    [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    public static extern int RegCreateKeyTransacted(int hKey, [MarshalAs(UnmanagedType.LPWStr)]string lpSubKey, int Reserved, [MarshalAs(UnmanagedType.LPWStr)]string lpClass, RegOption dwOptions, RegSam samDesired, SECURITY_ATTRIBUTES lpSecurityAttributes, out int phkResult, out RegResult lpdwDisposition, IntPtr transaction, IntPtr pExtendedParemeter);

    [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    public static extern int RegOpenKeyTransacted(IntPtr hKey, [MarshalAs(UnmanagedType.LPWStr)]string subKey, uint options, int sam, out IntPtr phkResult, IntPtr transaction, IntPtr pExtendedParemeter);

    public const int LOGON32_PROVIDER_DEFAULT = 0;
    public const int LOGON32_LOGON_INTERACTIVE = 2;
    public const int LOGON32_LOGON_NETWORK = 3;
    public const int LOGON32_LOGON_BATCH = 4;
    public const int LOGON32_LOGON_SERVICE = 5;

    [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword, int dwLogonType, int dwLogonProvider, out Win32.Kernel32.SafeTokenHandle phToken);

    // Service control.
    [Flags]
    public enum SC_MANAGER_ACCESS : uint
    {
      CONNECT = 0x0001,
      CREATE_SERVICE = 0x0002,
      ENUMERATE_SERVICE = 0x0004,
      LOCK = 0x0008,
      QUERY_LOCK_STATUS = 0x0010,
      MODIFY_BOOT_CONFIG = 0x0020,

      STANDARD_RIGHTS_REQUIRED = 0x000F0000,

      GENERIC_READ = 0x80000000,
      GENERIC_WRITE = 0x40000000,
      GENERIC_EXECUTE = 0x20000000,
      GENERIC_ALL = 0x10000000,

      ALL_ACCESS = STANDARD_RIGHTS_REQUIRED |
                  CONNECT |
                  CREATE_SERVICE |
                  ENUMERATE_SERVICE |
                  LOCK |
                  QUERY_LOCK_STATUS |
                  MODIFY_BOOT_CONFIG
    }

    [Flags]
    public enum SC_SERVICE_TYPE : uint
    {
      KERNEL_DRIVER = 0x00000001,
      FILE_SYSTEM_DRIVER = 0x00000002,
      ADAPTER = 0x00000004,
      RECOGNIZER_DRIVER = 0x00000008,

      DRIVER = (KERNEL_DRIVER |
        FILE_SYSTEM_DRIVER |
        RECOGNIZER_DRIVER),

      WIN32_OWN_PROCESS = 0x00000010,
      WIN32_SHARE_PROCESS = 0x00000020,
      WIN32 = (WIN32_OWN_PROCESS |
        WIN32_SHARE_PROCESS),

      INTERACTIVE_PROCESS = 0x00000100,

      TYPE_ALL = (WIN32 |
        ADAPTER |
        DRIVER |
        INTERACTIVE_PROCESS)
    }

    public enum SC_START_TYPE : int
    {
      SERVICE_BOOT_START = 0x00000000,
      SERVICE_SYSTEM_START = 0x00000001,
      SERVICE_AUTO_START = 0x00000002,
      SERVICE_DEMAND_START = 0x00000003,
      SERVICE_DISABLED = 0x00000004
    }

    public enum SC_ERROR_CONTROL : int
    {
      SERVICE_ERROR_IGNORE = 0x00000000,
      SERVICE_ERROR_NORMAL = 0x00000001,
      SERVICE_ERROR_SEVERE = 0x00000002,
      SERVICE_ERROR_CRITICAL = 0x00000003
    }

    [DllImport("advapi32")]
    public static extern IntPtr CreateService(IntPtr hSCManager,
      string lpServiceName,
      string lpDisplayName,
      SC_MANAGER_ACCESS dwDesiredAccess,
      SC_SERVICE_TYPE dwServiceType,
      SC_START_TYPE dwStartType,
      SC_ERROR_CONTROL dwErrorControl,
      string lpBinaryPathName,	// Make sure to double quote, and double escape
      string lpLoadOrderGroup,	// Use null for no group
      IntPtr lpdwTagId,			// Use null for no tag
      string lpDependencies,		// Use null for no dependencies
      string lpServiceStartName,	// Use DOMAIN\USER
      string lpPassword
      );

    [DllImport("advapi32")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool StartService(IntPtr hService,
                        int dwNumServiceArgs,
                        IntPtr lpServiceArgVectors
      );


    [DllImport("advapi32")]
    public static extern IntPtr OpenSCManager(string lpMachineName,	// Null for current machine
      string lpDatabaseName,	// Null for active database
      SC_MANAGER_ACCESS dwDesiredAccess
      );

    [DllImport("advapi32")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool CloseServiceHandle(IntPtr hSCObject);

    [DllImport("advapi32")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool DeleteService(IntPtr hService);

    [DllImport("advapi32")]
    public static extern IntPtr OpenService(IntPtr hSCManager,
                          string lpServiceName,
                          SC_MANAGER_ACCESS dwDesiredAccess
                          );

    [Flags]
    public enum SERVICE_CONTROL : uint
    {
      STOP = 0x00000001,
      PAUSE = 0x00000002,
      CONTINUE = 0x00000003,
      INTERROGATE = 0x00000004,
      SHUTDOWN = 0x00000005,
      PARAMCHANGE = 0x00000006,
      NETBINDADD = 0x00000007,
      NETBINDREMOVE = 0x00000008,
      NETBINDENABLE = 0x00000009,
      NETBINDDISABLE = 0x0000000A,
      DEVICEEVENT = 0x0000000B,
      HARDWAREPROFILECHANGE = 0x0000000C,
      POWEREVENT = 0x0000000D,
      SESSIONCHANGE = 0x0000000E
    }

    public enum SERVICE_STATE : uint
    {
      SERVICE_STOPPED = 0x00000001,
      SERVICE_START_PENDING = 0x00000002,
      SERVICE_STOP_PENDING = 0x00000003,
      SERVICE_RUNNING = 0x00000004,
      SERVICE_CONTINUE_PENDING = 0x00000005,
      SERVICE_PAUSE_PENDING = 0x00000006,
      SERVICE_PAUSED = 0x00000007
    }

    [Flags]
    public enum SERVICE_ACCEPT : uint
    {
      STOP = 0x00000001,
      PAUSE_CONTINUE = 0x00000002,
      SHUTDOWN = 0x00000004,
      PARAMCHANGE = 0x00000008,
      NETBINDCHANGE = 0x00000010,
      HARDWAREPROFILECHANGE = 0x00000020,
      POWEREVENT = 0x00000040,
      SESSIONCHANGE = 0x00000080,
    }

    [DllImport("advapi32")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool ControlService(IntPtr hService,
                          SERVICE_CONTROL dwControl,
                          ref SERVICE_STATUS lpServiceStatus
                          );

    [StructLayout(LayoutKind.Sequential)]
    public struct SERVICE_STATUS
    {
      SC_SERVICE_TYPE dwServiceType;
      SERVICE_STATE dwCurrentState;
      SERVICE_ACCEPT dwControlsAccepted;
      int dwWin32ExitCode;
      int dwServiceSpecificExitCode;
      uint dwCheckPoint;
      uint dwWaitHint;
    }

    // Wcrypt2
    public static readonly uint PROV_RSA_FULL = 1;

    public static readonly uint CRYPT_VERIFYCONTEXT = 0xF0000000;
    public static readonly uint CRYPT_NEWKEYSET = 0x8;
    public static readonly uint CRYPT_DELETEKEYSET = 0x10;
    public static readonly uint CRYPT_MACHINE_KEYSET = 0x20;

    public static readonly uint CALG_SHA1 = 0x8004;
    public static readonly uint CALG_RC4 = 0x6801;

    public static readonly uint AT_SIGNATURE = 0x2;

    [DllImport("advapi32.dll", SetLastError = true)]
    public static extern bool CryptAcquireContext(out IntPtr phProv, string pszContainer, string pszProvider, uint dwProvType, uint dwFlags);
    [DllImport("advapi32.dll", SetLastError = true)]
    public static extern bool CryptCreateHash(IntPtr hProv, uint Algid, IntPtr hKey, uint dwFlags, out IntPtr phHash);
    [DllImport("advapi32.dll", SetLastError = true)]
    public static extern bool CryptDestroyHash(IntPtr hHash);
    [DllImport("advapi32.dll", SetLastError = true)]
    public static extern bool CryptHashData(IntPtr hHash, byte[] pbData, int dwDataLen, uint dwFlags);
    [DllImport("advapi32.dll", SetLastError = true)]
    public static extern bool CryptDeriveKey(IntPtr hProv, uint Algid, IntPtr hBaseData, uint dwFlags, out IntPtr phKey);
    [DllImport("advapi32.dll", SetLastError = true)]
    public static extern bool CryptImportKey(IntPtr hProv, byte[] pbData, int dwDataLen, IntPtr hPubKey, uint dwFlags, out IntPtr phKey);
    [DllImport("advapi32.dll", SetLastError = true)]
    public static extern bool CryptSignHash(IntPtr hHash, uint dwKeySpec, string sDescription, uint dwFlags, [Out] byte[] pbSignature, [In, Out] ref int pdwSignLen);

    // Query.
    [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    public static extern bool QueryServiceStatusEx(IntPtr serviceHandle, int infoLevel, IntPtr buffer, int bufferSize, out int bytesNeeded);

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct SERVICE_STATUS_PROCESS
    {
      public int serviceType;
      public int currentState;
      public int controlsAccepted;
      public int win32ExitCode;
      public int serviceSpecificExitCode;
      public int checkPoint;
      public int waitHint;
      public int processID;
      public int serviceFlags;
    }
  }
}
