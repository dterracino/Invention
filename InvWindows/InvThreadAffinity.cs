﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Globalization;

namespace Inv
{
  /// <summary>
  /// Tracks and checks thread affinity.
  /// </summary>
  /// <remarks>
  /// <para>Use instances of <see cref="ThreadAffinity"/> to ensure that classes are only being accessed 
  /// from the thread they were started on.</para>
  /// <example>
  /// <para>This example uses <see cref="ThreadAffinity"/> to protect the Data property of MyClass from being
  /// accessed by threads other than the thread that instantiated the MyClass object.</para>
  /// <code>
  /// public sealed class MyClass
  /// {
  ///   private Inv.ThreadAffinity affinity = new Inv.ThreadAffinity();
  ///   private int data = 0;
  ///   
  ///   public Data
  ///   {
  ///     get
  ///     {
  ///       // Will throw an InvalidOperationException if the thread accessing Data is not
  ///       // the thread that created the instance of MyClass.
  ///       affinity.Check();
  ///       
  ///       return data;
  ///     }
  ///     
  ///     set
  ///     {
  ///       // Will throw an InvalidOperationException if the thread accessing Data is not
  ///       // the thread that created the instance of MyClass.
  ///       affinity.Check();
  ///       
  ///       data = value;
  ///     }
  ///   }
  /// }
  /// </code>
  /// </example>
  /// </remarks>
  public sealed class ThreadAffinity
  {
    private readonly int ThreadID;

    /// <summary>
    /// Initializes a new instance of the <see cref="ThreadAffinity"/> class with affinity to the currently executing thread.
    /// </summary>
    public ThreadAffinity()
    {
      ThreadID = Thread.CurrentThread.ManagedThreadId;
    }
    
    /// <summary>
    /// Check to see if this <see cref="ThreadAffinity"/> object is being executed on a different thread to the thread
    /// it has affinity to.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the current thread is not the thread to which the
    /// <see cref="ThreadAffinity"/> object was created on.</exception>
    public void Check()
    {
      if (Thread.CurrentThread.ManagedThreadId != ThreadID)
        throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Detected access to class with affinity to thread [{0}] from thread [{1}].", ThreadID, Thread.CurrentThread.ManagedThreadId));
    }
  }
}