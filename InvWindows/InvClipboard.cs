﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Represents the Windows system clipboard.
  /// </summary>
  /// <remarks>
  /// <para>Provides an interface to allow manipulation of the Windows clipboard.</para>
  /// <example>
  /// <code>
  /// string fragment1 = "&lt;p&gt;Example fragment&lt;/p&gt;";
  /// 
  /// Inv.Clipboard.CopyHtmlFragment(fragment1);
  /// 
  /// string fragment2 = Inv.Clipboard.PasteHtmlFragment();
  /// 
  /// Debug.Assert(fragment1 == fragment2, "Clipboard copy/paste failed!");
  /// </code>
  /// </example>
  /// </remarks>
  public static class Clipboard
  {
    public static void Clear()
    {
      ExecuteAction(() => System.Windows.Clipboard.Clear());
    }
    public static bool HasText()
    {
      return System.Windows.Clipboard.ContainsText();
    }
    public static string GetText()
    {
      return System.Windows.Clipboard.GetText();
    }
    public static void SetText(string Text)
    {
      ExecuteAction(System.Windows.Clipboard.SetText, Text);
    }
    public static bool HasAnsiText()
    {
      return System.Windows.Clipboard.ContainsText(System.Windows.TextDataFormat.Text);
    }
    public static string GetAnsiText()
    {
      return System.Windows.Clipboard.GetText(System.Windows.TextDataFormat.Text);
    }
    public static bool HasUnicodeText()
    {
      return System.Windows.Clipboard.ContainsText(System.Windows.TextDataFormat.UnicodeText);
    }
    public static string GetUnicodeText()
    {
      return System.Windows.Clipboard.GetText(System.Windows.TextDataFormat.UnicodeText);
    }
    public static bool HasOemText()
    {
      return System.Windows.Clipboard.ContainsData(System.Windows.DataFormats.OemText);
    }
    public static string GetOemText()
    {
      return (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.OemText);
    }
    public static bool HasHtmlText()
    {
      return System.Windows.Clipboard.ContainsText(System.Windows.TextDataFormat.Html);
    }
    public static string GetHtmlText()
    {
      return System.Windows.Clipboard.GetText(System.Windows.TextDataFormat.Html);
    }
    public static void SetHtmlText(string Text)
    {
      ExecuteAction(System.Windows.Clipboard.SetText, Text, System.Windows.TextDataFormat.Html);
    }
    public static bool HasRtfText()
    {
      return System.Windows.Clipboard.ContainsText(System.Windows.TextDataFormat.Rtf);
    }
    public static string GetRtfText()
    {
      return System.Windows.Clipboard.GetText(System.Windows.TextDataFormat.Rtf);
    }
    public static void SetRtfText(string Text)
    {
      ExecuteAction(System.Windows.Clipboard.SetText, Text, System.Windows.TextDataFormat.Rtf);
    }
    public static bool HasImage()
    {
      return System.Windows.Clipboard.ContainsImage();
    }
    public static System.Windows.Media.Imaging.BitmapSource GetImage()
    {
      return System.Windows.Clipboard.GetImage();
    }
    public static bool HasData(string Format)
    {
      return System.Windows.Clipboard.ContainsData(Format);
    }
    public static object GetData(string Format)
    {
      return System.Windows.Clipboard.GetData(Format);
    }
    public static object GetObject()
    {
      return System.Windows.Clipboard.GetDataObject();
    }
    
    public static void SetObject(object Object, bool KeepOnExit)
    {/*
      try
      {
        System.Windows.Clipboard.SetDataObject(Object, KeepOnExit);
      }
      catch (System.Runtime.InteropServices.ExternalException)
      {
      }*/
      
      ExecuteAction(System.Windows.Clipboard.SetDataObject, Object, KeepOnExit);
    }
    public static string[] GetFileDropArray()
    {
      var Collection = System.Windows.Clipboard.GetFileDropList();

      var Result = new string[Collection.Count];
      Collection.CopyTo(Result, 0);

      return Result;
    }
    /// <summary>
    /// 'Paste' the current contents of the Windows clipboard into a string.
    /// </summary>
    /// <returns>The current contents of the clipboard in HTML format.</returns>
    public static string GetHtmlFragment()
    {
      // CF_HTML format specification:
      // http://msdn.microsoft.com/library/default.asp?url=/workshop/networking/clipboard/htmlclipboard.asp

      var ClipboardText = GetHtmlText();

      var StartHtml = 0;
      //var EndHtml = 0;
      var StartFragment = 0;
      //string Context = null;
      string Fragment = null;
      //Uri SourceUri = null;

      var Expression = new Regex("([a-zA-Z]+):(.+?)[\r\n]", RegexOptions.IgnoreCase | RegexOptions.Compiled);

      for (var Match = Expression.Match(ClipboardText); Match.Success; Match = Match.NextMatch())
      {
        var Key = Match.Groups[1].Value.ToLower();
        var Value = Match.Groups[2].Value;

        switch (Key)
        {
          case "version":
            // Version number of the clipboard. Starting version is 0.9. 
            //Version = Value;
            break;

          case "starthtml":
            // Byte count from the beginning of the clipboard to the start of the context, or -1 if no context
            if (StartHtml != 0)
              throw new FormatException("StartHtml is already declared");

            StartHtml = int.Parse(Value);
            break;

          case "endhtml":
            // Byte count from the beginning of the clipboard to the end of the context, or -1 if no context.
            if (StartHtml == 0)
              throw new FormatException("StartHTML must be declared before endHTML");

            //EndHtml = int.Parse(Value);

            //Context = ClipboardText.Substring(StartHtml, EndHtml - StartHtml);
            break;

          case "startfragment":
            //  Byte count from the beginning of the clipboard to the start of the fragment.
            if (StartFragment != 0)
              throw new FormatException("StartFragment is already declared");

            StartFragment = int.Parse(Value);
            break;

          case "endfragment":
            // Byte count from the beginning of the clipboard to the end of the fragment.
            if (StartFragment == 0)
              throw new FormatException("StartFragment must be declared before EndFragment");

            var EndFragment = int.Parse(Value);
            Fragment = ClipboardText.Substring(StartFragment, EndFragment - StartFragment);
            break;

          case "sourceurl":
            // Optional Source URL, used for resolving relative links.
            //SourceUri = new System.Uri(Value);
            break;
        }
      }

      return Fragment;
    }
    /// <summary>
    /// 'Copy' a HTML fragment to the Windows clipboard.
    /// </summary>
    /// <param name="Fragment">The HTML fragment to place onto the Windows clipboard.</param>
    public static void SetHtmlFragment(string Fragment)
    {
      SetHtmlText(FormatHtmlFragment(Fragment));
    }
    public static string FormatHtmlFragment(string Fragment)
    {
      // CF_HTML format specification:
      // http://msdn.microsoft.com/library/default.asp?url=/workshop/networking/clipboard/htmlclipboard.asp

      var StringBuilder = new System.Text.StringBuilder();

      var Header =
        "Format:HTML Format\r\n" +
        "Version:1.0\r\n" +
        "StartHTML:<<<<<<<1\r\n" +
        "EndHTML:<<<<<<<2\r\n" +
        "StartFragment:<<<<<<<3\r\n" +
        "EndFragment:<<<<<<<4\r\n" +
        "StartSelection:<<<<<<<3\r\n" +
        "EndSelection:<<<<<<<3\r\n" +
        "\r\n";

      var Doctype =
        "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n" +
        "<HTML><HEAD><TITLE>Inv.Clipboard</TITLE></HEAD><BODY><!--StartFragment-->";

      var Footer = "<!--EndFragment--></BODY></HTML>";

      StringBuilder.Append(Header);

      var startHTML = StringBuilder.Length;

      StringBuilder.Append(Doctype);
      var fragmentStart = StringBuilder.Length;

      StringBuilder.Append(Fragment);
      var fragmentEnd = StringBuilder.Length;

      StringBuilder.Append(Footer);
      var endHTML = StringBuilder.Length;

      StringBuilder.Replace("<<<<<<<1", string.Format("{0,8}", startHTML));
      StringBuilder.Replace("<<<<<<<2", string.Format("{0,8}", endHTML));
      StringBuilder.Replace("<<<<<<<3", string.Format("{0,8}", fragmentStart));
      StringBuilder.Replace("<<<<<<<4", string.Format("{0,8}", fragmentEnd));

      return StringBuilder.ToString();
    }

    // NOTE: 
    // This strategy was found in System.Windows.Documents.TextEditorCopyPaste.Copy().
    // For some reason, the content will actually be copied to the clipboard, despite the exception.
    // There will be a one second delay because the System.Windows.Clipboard implementation retries 10 times
    // with an interval of 100ms.
    [DebuggerNonUserCode]
    private static void ExecuteAction<T1, T2>(Action<T1, T2> Action, T1 A1, T2 A2)
    {
      try
      {
        Action(A1, A2);
      }
      catch (System.Runtime.InteropServices.ExternalException)
      {
      }
    }
    [DebuggerNonUserCode]
    private static void ExecuteAction<T1>(Action<T1> Action, T1 A1)
    {
      try
      {
        Action(A1);
      }
      catch (System.Runtime.InteropServices.ExternalException)
      {
      }
    }
    [DebuggerNonUserCode]
    private static void ExecuteAction(Action Action)
    {
      try
      {
        Action();
      }
      catch (System.Runtime.InteropServices.ExternalException)
      {
      }
    }
  }
}