﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Threading;
using Inv.Support;
using System.Threading;

namespace Inv
{
  /// <summary>
  /// Represents a series of tasks to be executed on a separate thread.
  /// </summary>
  /// <remarks>
  /// For tasks that do not need to be executed on a separate thread, consider <see cref="TaskSequence"/>.
  /// <example>
  /// <para>This example sets up a new <see cref="TaskProcedure"/>, adds three tasks to it, and executes the procedure.</para>
  /// <code>
  /// TaskProcedure tp = new TaskProcedure("Test Task Procedure");
  /// TaskStep step1 = tp.AddStep("Step 1");
  /// step1.Execute = (Context) => { Console.Out.WriteLine("Step 1"); };
  /// 
  /// TaskStep step2 = tp.AddStep("Step 2");
  /// step2.Query = () => { return false; };
  /// step2.Execute = (Context) => { Console.Out.WriteLine("Step 2"); };
  /// 
  /// TaskStep step3 = tp.AddStep("Step 3");
  /// step3.Execute = (Context) => { Console.Out.WriteLine("Step 3"); };
  /// 
  /// tp.Start();
  /// </code>
  /// <para>Note that due to the <see cref="TaskStep.Query"/> delegate returning False on step 2, the main execution delegate for step 2 will not be executed,
  /// resulting in the following output:</para>
  /// <code language="none">
  /// Step 1
  /// Step 3
  /// </code>
  /// </example>
  /// </remarks>
  public sealed class TaskProcedure
  {
    /// <summary>
    /// Creates a new <see cref="TaskProcedure"/> with no steps.
    /// </summary>
    /// <param name="Name">The name of the new <see cref="TaskProcedure"/>.</param>
    public TaskProcedure(string Name)
    {
      this.Name = Name;
      this.StepList = new Inv.DistinctList<TaskStep>();
      this.MinimumElapsedTime = TimeSpan.Zero;
      this.ExecuteCriticalSection = new ExclusiveCriticalSection("Inv.TaskProcedure-Execute");
      this.ExecuteState = new TaskState();
    }

    /// <summary>
    /// The name of the <see cref="TaskProcedure"/>.
    /// </summary>
    public string Name { get; private set; }
    /// <summary>
    /// The minimum amount of time this <see cref="TaskProcedure"/> may execute in.
    /// </summary>
    /// <remarks>
    /// If the actual execution of the <see cref="TaskProcedure"/> takes less than <see cref="MinimumElapsedTime"/> time,
    /// a delay will occur to pad execution time to the specified value.
    /// </remarks>
    public TimeSpan MinimumElapsedTime { get; set; }
    /// <summary>
    /// Occurs when each step of the <see cref="TaskProcedure"/> is about to be executed, and when the TaskProcedure completes.
    /// </summary>
    /// <remarks>
    /// <para>The <see cref="NotifyEvent"/> delegate will be called immediately before each step of the <see cref="TaskProcedure"/> is executed
    /// and additionally once after all tasks have been executed.</para>
    /// <para>It will not be executed in the event that the <see cref="TaskProcedure"/> is explicitly terminated with a call to
    /// <see cref="Stop"/>.</para>
    /// </remarks>
    public event Action<TaskState> NotifyEvent;

    /// <summary>
    /// Add a new step to the TaskProcedure.
    /// </summary>
    /// <remarks>Adds a new step to the <see cref="TaskProcedure"/>, provided that the <see cref="TaskProcedure"/>
    /// has not already started to execute.</remarks>
    /// <param name="Description">The name of the new step.</param>
    /// <returns>A <see cref="TaskStep"/> object representing the new step.</returns>
    public TaskStep AddStep(string Description)
    {
      Debug.Assert(ExecuteThread == null, "Cannot add a step as the procedure is already started.");

      var Result = new TaskStep(this, Description);

      StepList.Add(Result);

      return Result;
    }
    public IEnumerable<TaskStep> Steps()
    {
      return StepList;
    }
    /// <summary>
    /// Count the number of steps in the <see cref="TaskProcedure"/>.
    /// </summary>
    /// <returns>The number of steps in the <see cref="TaskProcedure"/>.</returns>
    public int StepCount()
    {
      return StepList.Count;
    }
    /// <summary>
    /// Begin executing the steps of the <see cref="TaskProcedure"/> in a new thread.
    /// </summary>
    public void Start()
    {
      ThreadInterrupted = false;
      InterruptDelegate = null;

      using (ExecuteCriticalSection.Lock())
      {
        ExecuteState.Open();
        ExecuteState.Count = StepList.Count;
      }

      ExecuteThread = Inv.ThreadGovernor.NewContext("Procedure-" + Name, () =>
      {
        try
        {
          foreach (var Stage in StepList)
          {
            var RunStage = Stage.QueryDelegate == null || Stage.QueryDelegate();

            using (ExecuteCriticalSection.Lock())
            {
              ExecuteState.CurrentStep = Stage;
              ExecuteState.CurrentProgress = null;
              ExecuteState.Index++;

              if (RunStage && NotifyEvent != null)
                NotifyEvent(ExecuteState);
            }

            if (RunStage)
            {
              if (Stage.ExecuteDelegate != null)
              {
                HookInterrupt(Stage.InterruptDelegate);
                try
                {
                  ThreadExecuting = true;
                  try
                  {
                    Stage.ExecuteDelegate();
                  }
                  finally
                  {
                    ThreadExecuting = false;
                  }
                }
                finally
                {
                  HookInterrupt(null);
                }
              }
            }

            if (ThreadInterrupted)
              break;
          }
        }
        catch (Exception Exception)
        {
          using (ExecuteCriticalSection.Lock())
          {
            ExecuteState.Exception = Exception;
          }
        }

        using (ExecuteCriticalSection.Lock())
        {
          ExecuteState.Close();

          if (!ThreadInterrupted)
          {
            var DelayTime = MinimumElapsedTime - ExecuteState.ElapsedTime;

            if (DelayTime >= TimeSpan.Zero)
              Thread.Sleep((int)DelayTime.TotalMilliseconds);

            if (NotifyEvent != null)
              NotifyEvent(ExecuteState);
          }
        }
      });
      ExecuteThread.SetSingleThreadedApartmentState();
      ExecuteThread.Start();
    }
    /// <summary>
    /// Interrupt execution of the <see cref="TaskProcedure"/>.
    /// </summary>
    /// <remarks>
    /// <para>Interrupt execution of the <see cref="TaskProcedure"/> after the current task step is executed.</para>
    /// <para>Prevents any further tasks from occurring, and will execute the interruption delegate for the
    /// current task step if one was specified.</para>
    /// </remarks>
    public void Stop()
    {
      Interrupt();

      if (ExecuteThread != null)
      {
        ExecuteThread.Stop();
        ExecuteThread = null;
      }
    }
    public void Interrupt()
    {
      using (ExecuteCriticalSection.Lock())
      {
        if (!ThreadInterrupted)
        {
          ThreadInterrupted = true;

          if (InterruptDelegate != null)
          {
            InterruptDelegate();
            InterruptDelegate = null;
          }
        }
      }
    }
    public bool IsInterrupted
    {
      get
      {
        using (ExecuteCriticalSection.Lock())
        {
          return ThreadInterrupted;
        }
      }
    }

    internal void HookInterrupt(Action Action)
    {
      using (ExecuteCriticalSection.Lock())
      {
        if (!ThreadInterrupted)
          InterruptDelegate = Action;
      }
    }
    internal void SetProgress(TaskStep Step, double? Value)
    {
      using (ExecuteCriticalSection.Lock())
      {
        Debug.Assert(ThreadExecuting && ExecuteState.CurrentStep == Step, "Step must be executing.");

        if (!ThreadInterrupted)
        {
          ExecuteState.CurrentProgress = Value;

          if (NotifyEvent != null)
            NotifyEvent(ExecuteState);
        }
      }
    }

    private Inv.DistinctList<TaskStep> StepList;
    private Inv.ExclusiveCriticalSection ExecuteCriticalSection;
    private TaskState ExecuteState;
    private Inv.ThreadContext ExecuteThread;
    private Action InterruptDelegate;
    private bool ThreadInterrupted;
    private bool ThreadExecuting;
  }

  /// <summary>
  /// Represents a step within a <see cref="TaskProcedure"/>.
  /// </summary>
  /// <remarks>
  /// <para><see cref="TaskStep"/> objects are not directly instantiated. Instead, use <see cref="TaskProcedure.AddStep"/> to obtain a new <see cref="TaskStep"/> object.</para>
  /// <example>
  /// <para>To set up a basic <see cref="TaskStep"/>:</para>
  /// <code>
  /// TaskProcedure tp = new TaskProcedure("TaskProcedureName");
  /// 
  /// TaskStep step = tp.AddStep("StepName");
  /// 
  /// step.Execute = () => 
  /// {
  ///   // Task step execution code goes here.
  /// }
  /// 
  /// step.Query = () =>
  /// {
  ///   // Optional code to determine if the step needs to be run. 
  ///   return true;
  /// }
  /// </code>
  /// </example>
  /// </remarks>
  public sealed class TaskStep
  {
    internal TaskStep(TaskProcedure Procedure, string Description)
    {
      this.Procedure = Procedure;
      this.Description = Description;
    }

    /// <summary>
    /// The description of this <see cref="TaskStep"/>.
    /// </summary>
    public string Description { get; private set; }
    /// <summary>
    /// The progress of this <see cref="TaskStep"/>.
    /// </summary>
    /// <summary>
    /// A query delegate to determine whether or not to run the execution delegate for this <see cref="TaskStep"/>.
    /// </summary>
    public Func<bool> Query
    {
      set
      {
        Debug.Assert(QueryDelegate == null, "Query must not already be set.");

        QueryDelegate = value;
      }
    }
    /// <summary>
    /// The execution delegate for this <see cref="TaskStep"/> to perform any required work.
    /// </summary>
    public Action Execute
    {
      set
      {
        Debug.Assert(ExecuteDelegate == null, "Execute must not already be set.");

        ExecuteDelegate = value;
      }
    }
    /// <summary>
    /// The interruption delegate for this <see cref="TaskStep"/>.
    /// </summary>
    /// <remarks>
    /// The interruption delegate will be called if the task is interrupted by calling <see cref="TaskProcedure.Stop"/>
    /// on the <see cref="TaskProcedure"/> containing this <see cref="TaskStep"/>.
    /// </remarks>
    public Action Interrupt
    {
      set
      {
        Debug.Assert(InterruptDelegate == null, "Interrupt must not already be set.");

        InterruptDelegate = value;
      }
    }

    public void SetProgress(double? Value)
    {
      Procedure.SetProgress(this, Value);
    }

    internal TaskProcedure Procedure { get; private set; }
    internal Action ExecuteDelegate { get; private set; }
    internal Action InterruptDelegate { get; private set; }
    internal Func<bool> QueryDelegate { get; private set; }
  }

  /// <summary>
  /// Represents the state of execution for a <see cref="TaskProcedure"/>.
  /// </summary>
  /// <remarks>
  /// <para>A <see cref="TaskState"/> object is passed to the <see cref="TaskProcedure.NotifyEvent"/> delegate of
  /// the <see cref="TaskProcedure"/> immediately before execution of each step, and after all steps have been executed.</para> 
  /// <para>It contains information about the execution state of the <see cref="TaskProcedure"/>, including the elapsed time and
  /// task number/count.</para>
  /// </remarks>
  public sealed class TaskState
  {
    /// <summary>
    /// The number of task steps in the <see cref="TaskProcedure"/>.
    /// </summary>
    public int Count { get; internal set; }
    /// <summary>
    /// The current task number.
    /// </summary>
    public int Index { get; internal set; }
    /// <summary>
    /// Whether or not the <see cref="TaskProcedure"/> has completed successfully.
    /// </summary>
    public bool IsCompleted { get; internal set; }
    /// <summary>
    /// The <see cref="Exception"/> (if any) that caused the <see cref="TaskProcedure"/> to abort.
    /// </summary>
    public Exception Exception { get; internal set; }
    /// <summary>
    /// The <see cref="TaskStep"/> object representing the currently executing task step.
    /// </summary>
    public TaskStep CurrentStep { get; internal set; }
    /// <summary>
    /// The optional progress of the currently executing task step.
    /// </summary>
    public double? CurrentProgress { get; internal set; }
    /// <summary>
    /// The time elapsed so far during execution of the <see cref="TaskProcedure"/>.
    /// </summary>
    /// <returns>The time elapsed so far during execution of the <see cref="TaskProcedure"/>.</returns>
    public TimeSpan ElapsedTime
    {
      get { return Stopwatch.Elapsed; }
    }

    internal void Open()
    {
      IsCompleted = false;
      Index = 0;
      Count = 0;
      Exception = null;
      CurrentStep = null;
      CurrentProgress = null;
      Stopwatch = Stopwatch.StartNew();
    }
    internal void Close()
    {
      IsCompleted = true;
      Stopwatch.Stop();
      CurrentStep = null;
      CurrentProgress = null;
    }

    internal Stopwatch Stopwatch { get; set; }
  }
}
