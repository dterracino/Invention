﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Inv.Support;
using System.Net.NetworkInformation;
using System.Net;

namespace Inv
{
  public sealed class PingStatisticsUpdatedEventArgs
  {
    public long MinimumRoundTripTime { get; set; }
    public long MaximumRoundTripTime { get; set; }
    public long AverageRoundTripTime { get; set; }
  }

  public sealed class PingControl
  {    
    public PingControl()
    {
      Ping = new Ping();
      Buffer = Encoding.ASCII.GetBytes("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
      Timeout = 1000;
      Options = new PingOptions();
      Options.DontFragment = true;
    }

    public PingOptions Options { get; private set; }
    public long PacketsSent { get; private set; }
    public long PacketsReceived { get; private set; }
    public long MinimumRoundTripTime { get; private set; }
    public long MaximumRoundTripTime { get; private set; }
    public long AverageRoundTripTime { get; private set; }
    public bool Active { get; private set; }

    public bool KeepRunningAverageRoundTripTime { get; set; }
    public bool IncludeLineNumbersInLog { get; set; }
    public event Action<PingStatisticsUpdatedEventArgs> StatisticsUpdatedEvent;
    public event Action<PingReply, string> ReturnedEvent;
    public event Action<IPStatus, string> FailedEvent;
    public event Action<string> StoppedEvent;

    public void Start(IPAddress Target)
    {
      if (Active)
        throw new ApplicationException("Pinger is already pinging");

      PacketsSent = 0;
      PacketsReceived = 0;
      MinimumRoundTripTime = long.MaxValue;
      MaximumRoundTripTime = 0;

      Thread = new Thread(DoPing);
      Active = true;
      Thread.Start(Target);
    }
    public void Stop()
    {
      Active = false;
    }

    private void DoPing(object Target)
    {
      var RoundTripList = new List<long>();
      var LineNumber = 0;
      var Address = (IPAddress)Target;
      
      while (Active)
      {
        LineNumber++;
        PacketsSent++;

        var PingReply = Ping.Send(Address, Timeout, Buffer, Options);
        var LogLine = "";

        switch (PingReply.Status)
        {
          case IPStatus.Success:
            PacketsReceived++;

            RoundTripList.Add(PingReply.RoundtripTime);
            MinimumRoundTripTime = Math.Min(PingReply.RoundtripTime, MinimumRoundTripTime);
            MaximumRoundTripTime = Math.Max(PingReply.RoundtripTime, MaximumRoundTripTime);

            if (KeepRunningAverageRoundTripTime)
              AverageRoundTripTime = (long)Math.Round(RoundTripList.Average(), 0);

            var Builder = new StringBuilder();
            Builder.Append(string.Format("Reply From {0}: ", Target));
            Builder.Append("bytes=" + PingReply.Buffer.Length.ToString());

            if (PingReply.RoundtripTime < 1)
              Builder.Append(" time<1ms");
            else
              Builder.Append(string.Format(" time={0}ms", PingReply.RoundtripTime));

            Builder.Append(" TTL=" + PingReply.Options.Ttl.ToString());
            Builder.Append("\n");

            LogLine = Builder.ToString();

            if (ReturnedEvent != null)
              ReturnedEvent(PingReply, LogLine);
            break;

          default:
            LogLine = string.Format("Ping did not succeed: {0}\n", PingReply.Status.ToString().ToSentenceCase());
            if (FailedEvent != null)
              FailedEvent(PingReply.Status, LogLine);
            break;
        }

        if (IncludeLineNumbersInLog)
          LogLine = string.Format("Ping {0}: {1}", LineNumber, LogLine);

        if (StatisticsUpdatedEvent != null)
          StatisticsUpdatedEvent(new PingStatisticsUpdatedEventArgs
          {
            MinimumRoundTripTime = MinimumRoundTripTime,
            MaximumRoundTripTime = MaximumRoundTripTime,
            AverageRoundTripTime = AverageRoundTripTime
          });

        Thread.Sleep(1000);
      }

      if (!KeepRunningAverageRoundTripTime && RoundTripList.Any())
        AverageRoundTripTime = (long)Math.Round(RoundTripList.Average(), 0);

      if (!RoundTripList.Any())
      {
        MinimumRoundTripTime = 0;
        MaximumRoundTripTime = 0;
      }

      if (StatisticsUpdatedEvent != null)
        StatisticsUpdatedEvent(new PingStatisticsUpdatedEventArgs 
        { 
          MinimumRoundTripTime = MinimumRoundTripTime, 
          MaximumRoundTripTime = MaximumRoundTripTime, 
          AverageRoundTripTime = AverageRoundTripTime
        });

      if (StoppedEvent != null)
      {
        double PacketsLost = PacketsSent != PacketsReceived ? PacketsSent - PacketsReceived : 0;
        var LossPercentage = ((PacketsLost / PacketsSent) * 100).RoundToSignificantDigits(1);

        var Builder = new StringBuilder();
        Builder.Append("\n");
        Builder.Append(string.Format("Ping statistics for {0}\n", Target));
        Builder.Append(string.Format("    Packets: Sent = {0}, Received = {1}, Lost = {2} ({3}% loss),\n", PacketsSent, PacketsReceived, PacketsLost, LossPercentage));
        Builder.Append(string.Format("Approximate round trip times in milli-seconds:\n"));
        Builder.Append(string.Format("    Minimum = {0}ms, Maximum = {1}ms, Average = {2}ms", MinimumRoundTripTime, MaximumRoundTripTime, AverageRoundTripTime));

        StoppedEvent(Builder.ToString());
      }
    }

    private Thread Thread;
    private Ping Ping;
    private Byte[] Buffer;
    private int Timeout;
  }
}