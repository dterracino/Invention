﻿/*! 5 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows;
using Inv.Support;

namespace Inv
{
  public static class CSharpFoundation
  {
    internal const string SolutionFolderGuid = "2150E333-8FDC-42A3-9474-1A3956D46DE8";
    internal const string SolutionCPlusPlusProjectGuid = "8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942";
    internal const string SolutionCSharpProjectGuid = "FAE04EC0-301F-11D3-BF4B-00C04F79EFBC";
    internal const string BeginProjectPrefix = "Project(\"{";
    internal const string BeginProjectSuffix = "}\")";
    internal const string MSBuildNamespaceUri = "http://schemas.microsoft.com/developer/msbuild/2003";

    internal static readonly Inv.Bidictionary<CSharpProjectType, string> ProjectTypeDictionary = new Inv.Bidictionary<CSharpProjectType, string>()
    {
      { CSharpProjectType.Library, "Library" },
      { CSharpProjectType.ConsoleApplication, "Exe" },
      { CSharpProjectType.WindowsApplication, "WinExe" },
      { CSharpProjectType.AppContainer, "AppContainerExe" }
    };
    internal static readonly Inv.Bidictionary<CSharpPlatform, string> SolutionPlatformDictionary = new Inv.Bidictionary<CSharpPlatform, string>()
    {
      { CSharpPlatform.AnyCPU, "Any CPU" },
      { CSharpPlatform.x64, "x64" },
      { CSharpPlatform.x86, "x86" },
    };
    internal static readonly Inv.Bidictionary<CSharpPlatform, string> ProjectPlatformDictionary = new Inv.Bidictionary<CSharpPlatform, string>()
    {
      { CSharpPlatform.AnyCPU, "AnyCPU" },
      { CSharpPlatform.x64, "x64" },
      { CSharpPlatform.x86, "x86" },
    };
    internal static readonly Inv.Bidictionary<CSharpConfiguration, string> ConfigurationDictionary = new Inv.Bidictionary<CSharpConfiguration, string>()
    {
      { CSharpConfiguration.Debug, "Debug" },
      { CSharpConfiguration.Release, "Release" },
    };
    internal static readonly Inv.Bidictionary<CSharpFramework, string> FrameworkDictionary = new Inv.Bidictionary<CSharpFramework, string>()
    {
      { CSharpFramework.v20, "v2.0" },
      { CSharpFramework.v30, "v3.0" },
      { CSharpFramework.v35, "v3.5" },
      { CSharpFramework.v40, "v4.0" },
      { CSharpFramework.v45, "v4.5" },
    };

    public const string CompileType = "Compile";
    public const string NoneType = "None";

    public const string MicrosoftCSharpBuildTargets = @"$(MSBuildBinPath)\Microsoft.CSharp.targets";
  }

  public static class CSharpSolutionTextFileExtractEngine
  {
    public static void Execute
    (
      CSharpSolution CSharpSolution,
      string FilePath
    )
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
      using (var StreamReader = new StreamReader(FileStream))
      {
        var CurrentLine = StreamReader.ReadLine();
        if (CurrentLine == "")
          CurrentLine = StreamReader.ReadLine(); // First line may be blank.

        if (CurrentLine != "Microsoft Visual Studio Solution File, Format Version 12.00" && CurrentLine != "Microsoft Visual Studio Solution File, Format Version 11.00")
          throw new Exception("Second line must be the expected file format version.");

        CurrentLine = StreamReader.ReadLine();
        if (CurrentLine != "# Visual Studio 14" && CurrentLine != "# Visual Studio 2013" && CurrentLine != "# Visual Studio 2012" && CurrentLine != "# Visual Studio 2010")
          throw new Exception("Third line must be the expected visual studio version.");

        CurrentLine = StreamReader.ReadLine();

        // Discard any non-project lines - additional metadata introduced with VS2013, VisualStudioVersion and MinimumVisualStudioVersion
        while (!CurrentLine.StartsWith(CSharpFoundation.BeginProjectPrefix))
          CurrentLine = StreamReader.ReadLine();

        while (CurrentLine.StartsWith(CSharpFoundation.BeginProjectPrefix))
        {
          var ProjectPartArray = CurrentLine.Split(new char[] { ',', '=' });

          var ProjectType = ProjectPartArray[0].Substring(CSharpFoundation.BeginProjectPrefix.Length, ProjectPartArray[0].IndexOf(CSharpFoundation.BeginProjectSuffix) - CSharpFoundation.BeginProjectPrefix.Length);
          var ProjectName = ProjectPartArray[1].Trim().Trim('"');
          var ProjectPath = ProjectPartArray[2].Trim().Trim('"');
          var ProjectGuid = ProjectPartArray[3].Trim().Trim('"', '{', '}');

          switch (ProjectType)
          {
            case CSharpFoundation.SolutionFolderGuid:
              var CSharpFolder = new CSharpFolder();
              CSharpSolution.Folder.Add(CSharpFolder);
              CSharpFolder.Name = ProjectName;
              CSharpFolder.Guid = ProjectGuid;

              CurrentLine = StreamReader.ReadLine();

              if (CurrentLine.Trim() == "ProjectSection(SolutionItems) = preProject")
              {
                CurrentLine = StreamReader.ReadLine();

                while (CurrentLine.Trim() != "EndProjectSection")
                {
                  var FilePartArray = CurrentLine.Split('=');

                  var CSharpFile = new CSharpFile();
                  CSharpFolder.File.Add(CSharpFile);
                  CSharpFile.Path = FilePartArray[0].Trim();

                  CurrentLine = StreamReader.ReadLine();
                }

                CurrentLine = StreamReader.ReadLine();
              }
              break;

            case CSharpFoundation.SolutionCSharpProjectGuid:
              var CSharpProject = new CSharpProject();
              CSharpSolution.Project.Add(CSharpProject);
              CSharpProject.Name = ProjectName;
              CSharpProject.Path = ProjectPath;
              CSharpProject.Guid = ProjectGuid;

              CurrentLine = StreamReader.ReadLine();
              break;

            case CSharpFoundation.SolutionCPlusPlusProjectGuid:
              CurrentLine = StreamReader.ReadLine();
              break;

            default:
              throw new Exception("Project type not handled: " + ProjectType + " in project guid " + ProjectGuid);
          }

          if (CurrentLine.Trim() != "EndProject")
            throw new Exception("EndProject was expected.");

          CurrentLine = StreamReader.ReadLine();
        }

        var ProjectGuidDictionary = CSharpSolution.Project.ToDictionary(Key => Key.Guid);
        var FolderGuidDictionary = CSharpSolution.Folder.ToDictionary(Key => Key.Guid);

        if (CurrentLine == "Global")
        {
          CurrentLine = StreamReader.ReadLine();

          // Configuration Platforms Pre solution.
          if (CurrentLine.Trim() == "GlobalSection(SolutionConfigurationPlatforms) = preSolution")
          {
            CurrentLine = StreamReader.ReadLine();

            while (CurrentLine.Trim() != "EndGlobalSection")
            {
              CurrentLine = StreamReader.ReadLine();
            }

            CurrentLine = StreamReader.ReadLine();
          }

          // Post solution.
          if (CurrentLine.Trim() == "GlobalSection(ProjectConfigurationPlatforms) = postSolution")
          {
            CurrentLine = StreamReader.ReadLine();

            while (CurrentLine.Trim() != "EndGlobalSection")
            {
              CurrentLine = StreamReader.ReadLine();
            }

            CurrentLine = StreamReader.ReadLine();
          }

          // Solution properties.
          if (CurrentLine.Trim() == "GlobalSection(SolutionProperties) = preSolution")
          {
            CurrentLine = StreamReader.ReadLine();

            while (CurrentLine.Trim() != "EndGlobalSection")
            {
              CurrentLine = StreamReader.ReadLine();
            }

            CurrentLine = StreamReader.ReadLine();
          }

          // Nested projects.
          if (CurrentLine.Trim() == "GlobalSection(NestedProjects) = preSolution")
          {
            CurrentLine = StreamReader.ReadLine();
            while (CurrentLine.Trim() != "EndGlobalSection")
            {
              var NestedPartArray = CurrentLine.Split('=');

              var ChildGuid = NestedPartArray[0].Trim().Trim('"', '{', '}');
              var ParentGuid = NestedPartArray[1].Trim().Trim('"', '{', '}');

              var ParentFolder = FolderGuidDictionary.GetValueOrDefault(ParentGuid);
              if (ParentFolder == null)
                throw new Exception("Parent folder was not found: " + ParentGuid);

              var ChildFolder = FolderGuidDictionary.GetValueOrDefault(ChildGuid);
              var ChildProject = ChildFolder == null ? ProjectGuidDictionary.GetValueOrDefault(ChildGuid) : null;

              if (ChildFolder != null)
                ParentFolder.Subfolder.Add(ChildFolder);
              else if (ChildProject != null)
                ParentFolder.Project.Add(ChildProject);
              //else
              //  throw new Exception("Child folder or project was not found: " + ChildGuid);

              CurrentLine = StreamReader.ReadLine();
            }

            CurrentLine = StreamReader.ReadLine();
          }

          // Performance Pre solution.
          if (CurrentLine.Trim() == "GlobalSection(Performance) = preSolution")
          {
            CurrentLine = StreamReader.ReadLine();

            while (CurrentLine.Trim() != "EndGlobalSection")
            {
              CurrentLine = StreamReader.ReadLine();
            }

            CurrentLine = StreamReader.ReadLine();
          }

          if (CurrentLine.Trim() != "EndGlobal")
            throw new Exception("EndGlobal was expected.");
        }
      }

      var ProjectRootPath = Path.GetDirectoryName(FilePath);

      foreach (var CSharpProject in CSharpSolution.Project)
      {
        var ProjectFilePath = Path.Combine(ProjectRootPath, CSharpProject.Path);

        if (System.IO.File.Exists(ProjectFilePath))
        {
          CSharpProjectXmlFileExtractEngine.Execute
          (
            CSharpProject: CSharpProject,
            FilePath: ProjectFilePath
          );
        }
      }
    }
  }

  internal static class CSharpProjectXmlFileExtractEngine
  {
    public static void Execute
    (
      CSharpProject CSharpProject,
      string FilePath
    )
    {
      if (CSharpProject.Name == null)
        CSharpProject.Name = Path.GetFileNameWithoutExtension(FilePath);

      if (CSharpProject.Path == null)
        CSharpProject.Path = FilePath;

      var XmlDocument = new Inv.XmlDocument();
      XmlDocument.ReadFromFile(FilePath);

      var ProjectElement = XmlDocument.AccessRoot("Project", CSharpFoundation.MSBuildNamespaceUri);

      var FirstPropertyGroup = true;

      foreach (var PropertyGroupElement in ProjectElement.AccessChildElement("PropertyGroup"))
      {
        if (FirstPropertyGroup)
        {
          if (PropertyGroupElement.HasAttribute("Condition"))
            throw new Exception("First property group must not have a condition.");

          FirstPropertyGroup = false;

          var SafeVersionElement = PropertyGroupElement.AccessChildElement("SafeVersion").SingleOrDefault();

          var SafeVersionContent = SafeVersionElement != null ? SafeVersionElement.Content.Trim('!').Trim() : null;

          if (SafeVersionContent != null)
            CSharpProject.SafeVersionMark = int.Parse(SafeVersionContent);
          else
            CSharpProject.SafeVersionMark = null;

          var OutputTypeElement = PropertyGroupElement.AccessChildElement("OutputType").SingleOrDefault();
          if (OutputTypeElement != null)
            CSharpProject.Type = CSharpFoundation.ProjectTypeDictionary.GetByRight(OutputTypeElement.Content);
          else
            CSharpProject.Type = null;

          var ConfigurationElement = PropertyGroupElement.AccessChildElement("Configuration").SingleOrDefault();
          if (ConfigurationElement != null)
            CSharpProject.Configuration = CSharpFoundation.ConfigurationDictionary.GetByRight(ConfigurationElement.Content);
          else
            CSharpProject.Configuration = null;

          var PlatformElement = PropertyGroupElement.AccessChildElement("Platform").SingleOrDefault();
          if (PlatformElement != null)
            CSharpProject.Platform = CSharpFoundation.ProjectPlatformDictionary.GetByRightOrDefault(PlatformElement.Content);
          else
            CSharpProject.Platform = null;

          var FrameworkElement = PropertyGroupElement.AccessChildElement("TargetFrameworkVersion").SingleOrDefault();
          if (FrameworkElement != null)
            CSharpProject.Framework = CSharpFoundation.FrameworkDictionary.GetByRightOrDefault(FrameworkElement.Content);
          else
            CSharpProject.Framework = null;

          var FrameworkVersionElement = PropertyGroupElement.AccessChildElement("TargetFrameworkProfile").SingleOrDefault();
          if (FrameworkVersionElement != null && FrameworkVersionElement.HasContent)
            CSharpProject.ClientProfile = FrameworkVersionElement.Content == "Client";
          else
            CSharpProject.ClientProfile = null;

          var GuidElement = PropertyGroupElement.AccessChildElement("ProjectGuid").SingleOrDefault();
          if (GuidElement != null)
            CSharpProject.Guid = GuidElement.Content.Trim('}', '{');
          else
            CSharpProject.Guid = null;

          var ProjectTypeElement = PropertyGroupElement.AccessChildElement("ProjectTypeGuids").SingleOrDefault();
          if (ProjectTypeElement != null && ProjectTypeElement.HasContent)
            CSharpProject.TypeGuid.AddRange(ProjectTypeElement.Content.Split(';').Select(Guid => Guid.Trim('{', '}')));

          var RootNamespaceElement = PropertyGroupElement.AccessChildElement("RootNamespace").SingleOrDefault();
          if (RootNamespaceElement == null)
            CSharpProject.DefaultNamespace = null;
          else if (RootNamespaceElement.HasContent)
            CSharpProject.DefaultNamespace = RootNamespaceElement.Content;
          else
            CSharpProject.DefaultNamespace = "";

          var AssemblyNameElement = PropertyGroupElement.AccessChildElement("AssemblyName").SingleOrDefault();
          if (AssemblyNameElement != null)
            CSharpProject.AssemblyName = AssemblyNameElement.Content;
          else
            CSharpProject.AssemblyName = null;

          var MvcBuildViewsElement = PropertyGroupElement.AccessChildElement("MvcBuildViews").SingleOrDefault();
          if (MvcBuildViewsElement != null && MvcBuildViewsElement.HasContent)
            CSharpProject.MvcBuildViews = bool.Parse(MvcBuildViewsElement.Content);
          else
            CSharpProject.MvcBuildViews = null;

          var UseIISExpressElement = PropertyGroupElement.AccessChildElement("UseIISExpress").SingleOrDefault();
          if (UseIISExpressElement != null && UseIISExpressElement.HasContent)
            CSharpProject.UseIISExpress = bool.Parse(UseIISExpressElement.Content);
          else
            CSharpProject.UseIISExpress = null;

          CSharpProject.IconName = null;
          CSharpProject.AllowUnsafeBlocks = null;
        }
        else if (PropertyGroupElement.HasAttribute("Condition"))
        {
          var AllowUnsafeBlocksElement = PropertyGroupElement.AccessChildElement("AllowUnsafeBlocks").SingleOrDefault();

          if (AllowUnsafeBlocksElement != null && AllowUnsafeBlocksElement.HasContent)
            CSharpProject.AllowUnsafeBlocks = (CSharpProject.AllowUnsafeBlocks ?? false) || bool.Parse(AllowUnsafeBlocksElement.Content);

          // TODO: we are not loading property conditions?
        }
        else
        {
          var ApplicationIconElement = PropertyGroupElement.AccessChildElement("ApplicationIcon").SingleOrDefault();
          if (ApplicationIconElement != null && ApplicationIconElement.HasContent)
            CSharpProject.IconName = ApplicationIconElement.Content;
        }
      }

      //if (FirstPropertyGroup)
      //  throw new Exception("First property group was not found.");

      foreach (var ItemElement in ProjectElement.AccessChildElement("ItemGroup").SelectMany(Group => Group.GetChildElements()))
      {
        switch (ItemElement.LocalName)
        {
          case "Reference":
            var CSharpAssemblyReference = new CSharpAssemblyReference();
            CSharpProject.AssemblyReference.Add(CSharpAssemblyReference);
            CSharpAssemblyReference.Name = ItemElement.AccessAttribute("Include");

            var HintPathElement = ItemElement.AccessChildElement("HintPath").SingleOrDefault();

            if (HintPathElement != null && HintPathElement.HasContent)
              CSharpAssemblyReference.HintPath = HintPathElement.Content;
            else
              CSharpAssemblyReference.HintPath = null;

            var SpecificVersionElement = ItemElement.AccessChildElement("SpecificVersion").SingleOrDefault();
            if (SpecificVersionElement != null && SpecificVersionElement.HasContent)
              CSharpAssemblyReference.SpecificVersion = bool.Parse(SpecificVersionElement.Content);
            else
              CSharpAssemblyReference.SpecificVersion = null;

            var AssemblyEmbedElement = ItemElement.AccessChildElement("EmbedInteropTypes").SingleOrDefault();
            if (AssemblyEmbedElement != null && AssemblyEmbedElement.HasContent)
              CSharpAssemblyReference.EmbedInteropTypes = bool.Parse(AssemblyEmbedElement.Content);
            else
              CSharpAssemblyReference.EmbedInteropTypes = null;
            break;

          case "ProjectReference":
            var CSharpProjectReference = new CSharpProjectReference();
            CSharpProject.ProjectReference.Add(CSharpProjectReference);
            CSharpProjectReference.Path = ItemElement.AccessAttribute("Include");
            CSharpProjectReference.Name = ItemElement.AccessChildElement("Name").Single().Content;
            CSharpProjectReference.Guid = ItemElement.AccessChildElement("Project").Single().Content.Trim('{', '}');
            break;

          case "COMReference":
            var CSharpCOMReference = new CSharpCOMReference();
            CSharpProject.COMReference.Add(CSharpCOMReference);
            CSharpCOMReference.Name = ItemElement.AccessAttribute("Include");
            CSharpCOMReference.Guid = ItemElement.AccessChildElement("Guid").Single().Content.Trim('{', '}');
            CSharpCOMReference.MajorVersion = ItemElement.AccessChildElement("VersionMajor").Single().Content;
            CSharpCOMReference.MinorVersion = ItemElement.AccessChildElement("VersionMinor").Single().Content;
            CSharpCOMReference.Lcid = ItemElement.AccessChildElement("Lcid").Single().Content;
            CSharpCOMReference.WrapperTool = ItemElement.AccessChildElement("WrapperTool").Single().Content;
            CSharpCOMReference.Isolated = bool.Parse(ItemElement.AccessChildElement("Isolated").Single().Content);

            var COMEmbedElement = ItemElement.AccessChildElement("EmbedInteropTypes").SingleOrDefault();
            if (COMEmbedElement != null && COMEmbedElement.HasContent)
              CSharpCOMReference.EmbedInteropTypes = bool.Parse(COMEmbedElement.Content);
            else
              CSharpCOMReference.EmbedInteropTypes = null;
            break;

          default:
            var CSharpIncludeReference = new CSharpIncludeReference()
            {
              File = new CSharpFile()
              {
                Path = ItemElement.AccessAttribute("Include")
              },
              Type = ItemElement.LocalName,
              DependentUpon = null
            };
            CSharpProject.IncludeReference.Add(CSharpIncludeReference);

            foreach (var ChildElement in ItemElement.GetChildElements().Where(Element => Element.HasContent))
            {
              if (ChildElement.LocalName == "DependentUpon")
              {
                CSharpIncludeReference.DependentUpon = ChildElement.Content;
              }
              else
              {
                CSharpIncludeReference.Option.Add(new CSharpIncludeOption()
                {
                  Name = ChildElement.LocalName,
                  Value = ChildElement.Content
                });
              }
            }
            break;
        }
      }

      foreach (var ImportElement in ProjectElement.AccessChildElement("Import"))
      {
        var CSharpImport = new CSharpImport();
        CSharpProject.Import.Add(CSharpImport);

        if (ImportElement.HasAttribute("Condition"))
          CSharpImport.Condition = ImportElement.AccessAttribute("Condition");
        else
          CSharpImport.Condition = null;

        CSharpImport.Project = ImportElement.AccessAttribute("Project");
      }

      foreach (var TargetElement in ProjectElement.AccessChildElement("Target"))
      {
        var CSharpTarget = new CSharpTarget();
        CSharpProject.Target.Add(CSharpTarget);

        if (TargetElement.HasAttribute("Condition"))
          CSharpTarget.Condition = TargetElement.AccessAttribute("Condition");
        else
          CSharpTarget.Condition = null;

        CSharpTarget.Name = TargetElement.AccessAttribute("Name");

        if (TargetElement.HasAttribute("BeforeTargets"))
          CSharpTarget.BeforeTargets = TargetElement.AccessAttribute("BeforeTargets");
        else
          CSharpTarget.BeforeTargets = null;

        if (TargetElement.HasAttribute("AfterTargets"))
          CSharpTarget.AfterTargets = TargetElement.AccessAttribute("AfterTargets");
        else
          CSharpTarget.AfterTargets = null;

        if (TargetElement.HasAttribute("DependsOnTargets"))
          CSharpTarget.DependsOnTargets = TargetElement.AccessAttribute("DependsOnTargets");
        else
          CSharpTarget.DependsOnTargets = null;
      }
    }
  }

  internal static class CSharpSolutionTextStringFormatEngine
  {
    public static void Execute
    (
      CSharpSolution CSharpSolution,
      out string Text
    )
    {
      using (var StringWriter = new StringWriter())
      {
        CSharpSolutionTextStreamFormatEngine.Execute
        (
          CSharpSolution: CSharpSolution,
          StreamWriter: StringWriter
        );

        Text = StringWriter.ToString();
      }
    }
  }

  internal static class CSharpSolutionTextStreamFormatEngine
  {
    public static void Execute
    (
      CSharpSolution CSharpSolution,
      TextWriter StreamWriter
    )
    {
      //StreamWriter.WriteLine(""); // NOTE: blank line is important.
      StreamWriter.WriteLine("Microsoft Visual Studio Solution File, Format Version 12.00");
      StreamWriter.WriteLine("# Visual Studio 2012");

      foreach (var CSharpFolder in CSharpSolution.Folder)
      {
        StreamWriter.WriteLine("Project(\"{{{0}}}\") = \"{1}\", \"{1}\", \"{{{2}}}\"", CSharpFoundation.SolutionFolderGuid, CSharpFolder.Name, CSharpFolder.Guid);

        if (CSharpFolder.File.Count > 0)
        {
          StreamWriter.WriteLine("\tProjectSection(SolutionItems) = preProject");

          foreach (var CSharpFile in CSharpFolder.File)
            StreamWriter.WriteLine("\t\t{0} = {0}", CSharpFile.Path);

          StreamWriter.WriteLine("\tEndProjectSection");
        }

        StreamWriter.WriteLine("EndProject");
      }

      foreach (var CSharpProject in CSharpSolution.Project)
      {
        StreamWriter.WriteLine("Project(\"{{{0}}}\") = \"{1}\", \"{2}\", \"{{{3}}}\"", CSharpFoundation.SolutionCSharpProjectGuid, CSharpProject.Name, CSharpProject.Path, CSharpProject.Guid);
        StreamWriter.WriteLine("EndProject");
      }

      StreamWriter.WriteLine("Global");

      StreamWriter.WriteLine("\tGlobalSection(SolutionConfigurationPlatforms) = preSolution");
      StreamWriter.WriteLine("\t\tDebug|Any CPU = Debug|Any CPU");
      StreamWriter.WriteLine("\t\tRelease|Any CPU = Release|Any CPU");
      StreamWriter.WriteLine("\tEndGlobalSection");

      StreamWriter.WriteLine("\tGlobalSection(ProjectConfigurationPlatforms) = postSolution");

      foreach (var CSharpProject in CSharpSolution.Project)
      {
        if (CSharpProject.Platform != null)
        {
          var PlatformString = CSharpFoundation.SolutionPlatformDictionary.GetByLeft(CSharpProject.Platform.Value);

          StreamWriter.WriteLine("\t\t{{{0}}}.Debug|Any CPU.ActiveCfg = Debug|{1}", CSharpProject.Guid, PlatformString);
          StreamWriter.WriteLine("\t\t{{{0}}}.Debug|Any CPU.Build.0 = Debug|{1}", CSharpProject.Guid, PlatformString);
          StreamWriter.WriteLine("\t\t{{{0}}}.Release|Any CPU.ActiveCfg = Release|{1}", CSharpProject.Guid, PlatformString);
          StreamWriter.WriteLine("\t\t{{{0}}}.Release|Any CPU.Build.0 = Release|{1}", CSharpProject.Guid, PlatformString);
        }
      }

      StreamWriter.WriteLine("\tEndGlobalSection");

      StreamWriter.WriteLine("\tGlobalSection(SolutionProperties) = preSolution");
      StreamWriter.WriteLine("\t\tHideSolutionNode = FALSE");
      StreamWriter.WriteLine("\tEndGlobalSection");

      StreamWriter.WriteLine("\tGlobalSection(NestedProjects) = preSolution");

      foreach (var CSharpFolder in CSharpSolution.Folder)
      {
        foreach (var CSharpSubfolder in CSharpFolder.Subfolder)
          StreamWriter.WriteLine("\t\t{{{0}}} = {{{1}}}", CSharpSubfolder.Guid, CSharpFolder.Guid);

        foreach (var CSharpProject in CSharpFolder.Project)
          StreamWriter.WriteLine("\t\t{{{0}}} = {{{1}}}", CSharpProject.Guid, CSharpFolder.Guid);
      }

      StreamWriter.WriteLine("\tEndGlobalSection");

      StreamWriter.WriteLine("EndGlobal");
    }
  }

  internal static class CSharpSolutionTextFileFormatEngine
  {
    public static void Execute
    (
      CSharpSolution CSharpSolution,
      string FilePath
    )
    {
      using (var FileSystemTransaction = Inv.FileSystemTransaction.BeginKTM("CSharpSolutionFileFormatEngine"))
      {
        using (var StreamWriter = FileSystemTransaction.OpenWriter(FilePath))
        {
          CSharpSolutionTextStreamFormatEngine.Execute
          (
            CSharpSolution: CSharpSolution,
            StreamWriter: StreamWriter
          );
        }

        var ProjectRootPath = Path.GetDirectoryName(FilePath);

        foreach (var CSharpProject in CSharpSolution.Project)
        {
          var XmlDocument = new Inv.XmlDocument();

          CSharpProjectXmlDocumentFormatEngine.Execute
          (
            CSharpProject: CSharpProject,
            XmlDocument: XmlDocument
          );

          using (var FileStream = FileSystemTransaction.OpenFile(Path.Combine(ProjectRootPath, CSharpProject.Path), FileMode.Create, FileAccess.Write, FileShare.None))
            XmlDocument.WriteToStream(FileStream);
        }

        FileSystemTransaction.Commit();
      }
    }
  }

  internal static class CSharpProjectXmlFileFormatEngine
  {
    public static void Execute
    (
      CSharpProject CSharpProject,
      string FilePath
    )
    {
      using (var FileSystemTransaction = Inv.FileSystemTransaction.BeginKTM("CSharpProjectXmlFileFormatEngine"))
      {
        var XmlDocument = new Inv.XmlDocument();

        CSharpProjectXmlDocumentFormatEngine.Execute
        (
          CSharpProject: CSharpProject,
          XmlDocument: XmlDocument
        );

        using (var FileStream = FileSystemTransaction.OpenFile(FilePath, FileMode.Create, FileAccess.Write, FileShare.None))
          XmlDocument.WriteToStream(FileStream);

        FileSystemTransaction.Commit();
      }
    }
  }
  internal static class CSharpProjectXmlDocumentFormatEngine
  {
    public static void Execute
    (
      CSharpProject CSharpProject,
      Inv.XmlDocument XmlDocument
    )
    {
      var ProjectElement = XmlDocument.InstallRoot("Project", CSharpFoundation.MSBuildNamespaceUri);
      ProjectElement.AddAttribute("ToolsVersion", "4.0");
      ProjectElement.AddAttribute("DefaultTargets", "Build");

      var PropertyGroupElement = ProjectElement.AddChildElement("PropertyGroup");

      if (CSharpProject.SafeVersionMark != null)
        PropertyGroupElement.AddChildElement("SafeVersion").Content = "! " + CSharpProject.SafeVersionMark.ToString() + " !";

      if (CSharpProject.Configuration != null)
      {
        var ConfigurationElement = PropertyGroupElement.AddChildElement("Configuration");
        ConfigurationElement.Content = CSharpFoundation.ConfigurationDictionary.GetByLeft(CSharpProject.Configuration.Value);
        ConfigurationElement.AddAttribute("Condition", " '$(Configuration)' == '' ");
      }

      string PlatformString;
      if (CSharpProject.Platform != null)
      {
        PlatformString = CSharpFoundation.ProjectPlatformDictionary.GetByLeft(CSharpProject.Platform.Value);

        var PlatformElement = PropertyGroupElement.AddChildElement("Platform");
        PlatformElement.Content = PlatformString;
        PlatformElement.AddAttribute("Condition", " '$(Platform)' == '' ");
      }
      else
      {
        PlatformString = null;
      }

      if (CSharpProject.Guid != null)
        PropertyGroupElement.AddChildElement("ProjectGuid").Content = "{" + CSharpProject.Guid + "}";

      if (CSharpProject.TypeGuid.Count > 0)
        PropertyGroupElement.AddChildElement("ProjectTypeGuids").Content = CSharpProject.TypeGuid.Select(Guid => "{" + Guid + "}").AsSeparatedText(";");

      if (CSharpProject.Type != null)
        PropertyGroupElement.AddChildElement("OutputType").Content = CSharpFoundation.ProjectTypeDictionary.GetByLeft(CSharpProject.Type.Value);

      PropertyGroupElement.AddChildElement("AppDesignerFolder").Content = "Properties";

      if (CSharpProject.DefaultNamespace != null)
        PropertyGroupElement.AddChildElement("RootNamespace").Content = CSharpProject.DefaultNamespace;

      if (CSharpProject.AssemblyName != null)
        PropertyGroupElement.AddChildElement("AssemblyName").Content = CSharpProject.AssemblyName;

      if (CSharpProject.Framework != null)
        PropertyGroupElement.AddChildElement("TargetFrameworkVersion").Content = CSharpFoundation.FrameworkDictionary.GetByLeft(CSharpProject.Framework.Value);

      if (CSharpProject.ClientProfile != null)
        PropertyGroupElement.AddChildElement("TargetFrameworkProfile").Content = CSharpProject.ClientProfile.Value ? "Client" : "Full";

      PropertyGroupElement.AddChildElement("FileAlignment").Content = "512";

      if (CSharpProject.MvcBuildViews != null)
        PropertyGroupElement.AddChildElement("MvcBuildViews").Content = CSharpProject.MvcBuildViews.Value.ToString();

      if (CSharpProject.UseIISExpress != null)
        PropertyGroupElement.AddChildElement("UseIISExpress").Content = CSharpProject.UseIISExpress.Value.ToString();

      if (PlatformString != null)
      {
        var DebugElement = ProjectElement.AddChildElement("PropertyGroup");
        DebugElement.AddAttribute("Condition", string.Format(" '$(Configuration)|$(Platform)' == 'Debug|{0}' ", PlatformString));
        DebugElement.AddChildElement("DebugSymbols").Content = "true";
        DebugElement.AddChildElement("DebugType").Content = "full";
        DebugElement.AddChildElement("Optimize").Content = "false";
        DebugElement.AddChildElement("OutputPath").Content = @"bin\Debug\";
        DebugElement.AddChildElement("DefineConstants").Content = "DEBUG;TRACE";
        DebugElement.AddChildElement("ErrorReport").Content = "prompt";
        DebugElement.AddChildElement("WarningLevel").Content = "4";
        if (CSharpProject.AllowUnsafeBlocks != null)
          DebugElement.AddChildElement("AllowUnsafeBlocks").Content = CSharpProject.AllowUnsafeBlocks.ToString();

        var ReleaseElement = ProjectElement.AddChildElement("PropertyGroup");
        ReleaseElement.AddAttribute("Condition", string.Format(" '$(Configuration)|$(Platform)' == 'Release|{0}' ", PlatformString));
        ReleaseElement.AddChildElement("DebugType").Content = "pdbonly";
        ReleaseElement.AddChildElement("Optimize").Content = "true";
        ReleaseElement.AddChildElement("OutputPath").Content = @"bin\Release\";
        ReleaseElement.AddChildElement("DefineConstants").Content = "TRACE";
        ReleaseElement.AddChildElement("ErrorReport").Content = "prompt";
        ReleaseElement.AddChildElement("WarningLevel").Content = "4";
        if (CSharpProject.AllowUnsafeBlocks != null)
          ReleaseElement.AddChildElement("AllowUnsafeBlocks").Content = CSharpProject.AllowUnsafeBlocks.ToString();
      }

      if (CSharpProject.AssemblyReference.Count > 0)
      {
        var ItemGroupElement = ProjectElement.AddChildElement("ItemGroup");

        foreach (var AssemblyReference in CSharpProject.AssemblyReference)
        {
          var ReferenceElement = ItemGroupElement.AddChildElement("Reference");
          ReferenceElement.AddAttribute("Include", AssemblyReference.Name);

          if (AssemblyReference.HintPath != null)
            ReferenceElement.AddChildElement("HintPath").Content = AssemblyReference.HintPath;

          if (AssemblyReference.SpecificVersion != null)
            ReferenceElement.AddChildElement("SpecificVersion").Content = AssemblyReference.SpecificVersion.ToString();

          if (AssemblyReference.EmbedInteropTypes != null)
            ReferenceElement.AddChildElement("EmbedInteropTypes").Content = AssemblyReference.EmbedInteropTypes.ToString();
        }
      }

      if (CSharpProject.ProjectReference.Count > 0)
      {
        var ItemGroupElement = ProjectElement.AddChildElement("ItemGroup");

        foreach (var ProjectReference in CSharpProject.ProjectReference)
        {
          var ReferenceElement = ItemGroupElement.AddChildElement("ProjectReference");
          ReferenceElement.AddAttribute("Include", ProjectReference.Path);
          ReferenceElement.AddChildElement("Project").Content = "{" + ProjectReference.Guid + "}";
          ReferenceElement.AddChildElement("Name").Content = ProjectReference.Name;
        }
      }

      if (CSharpProject.COMReference.Count > 0)
      {
        var ItemGroupElement = ProjectElement.AddChildElement("ItemGroup");

        foreach (var COMReference in CSharpProject.COMReference)
        {
          var ReferenceElement = ItemGroupElement.AddChildElement("COMReference");
          ReferenceElement.AddAttribute("Include", COMReference.Name);
          ReferenceElement.AddChildElement("Guid").Content = "{" + COMReference.Guid + "}";
          ReferenceElement.AddChildElement("VersionMajor").Content = COMReference.MajorVersion;
          ReferenceElement.AddChildElement("VersionMinor").Content = COMReference.MinorVersion;
          ReferenceElement.AddChildElement("Lcid").Content = COMReference.Lcid;
          ReferenceElement.AddChildElement("WrapperTool").Content = COMReference.WrapperTool;
          ReferenceElement.AddChildElement("Isolated").Content = COMReference.Isolated.ToString();

          if (COMReference.EmbedInteropTypes != null)
            ReferenceElement.AddChildElement("EmbedInteropTypes").Content = COMReference.EmbedInteropTypes.ToString();
        }
      }

      foreach (var IncludeGroup in CSharpProject.IncludeReference.GroupBy(Reference => Reference.Type))
      {
        var ItemGroupElement = ProjectElement.AddChildElement("ItemGroup");

        foreach (var IncludeReference in IncludeGroup)
        {
          var IncludeElement = ItemGroupElement.AddChildElement(IncludeGroup.Key);
          IncludeElement.AddAttribute("Include", IncludeReference.File.Path);

          if (IncludeReference.DependentUpon != null)
            IncludeElement.AddChildElement("DependentUpon").Content = IncludeReference.DependentUpon;

          foreach (var IncludeOption in IncludeReference.Option)
            IncludeElement.AddChildElement(IncludeOption.Name).Content = IncludeOption.Value;
        }
      }

      foreach (var Import in CSharpProject.Import)
      {
        var ImportElement = ProjectElement.AddChildElement("Import");

        if (Import.Condition != null)
          ImportElement.AddAttribute("Condition", Import.Condition);

        ImportElement.AddAttribute("Project", Import.Project);
      }

      foreach (var Target in CSharpProject.Target)
      {
        var TargetElement = ProjectElement.AddChildElement("Target");

        if (Target.Condition != null)
          TargetElement.AddAttribute("Condition", Target.Condition);

        TargetElement.AddAttribute("Name", Target.Name);

        if (Target.BeforeTargets != null)
          TargetElement.AddAttribute("BeforeTargets", Target.BeforeTargets);

        if (Target.AfterTargets != null)
          TargetElement.AddAttribute("AfterTargets", Target.AfterTargets);

        if (Target.DependsOnTargets != null)
          TargetElement.AddAttribute("DependsOnTargets", Target.DependsOnTargets);

        if (Target.Content != null)
          TargetElement.ImportAsChildElement(Target.Content.Value);
      }
    }
  }

  public sealed class CSharpSolution
  {
    public CSharpSolution()
    {
      this.Folder = new Inv.DistinctList<CSharpFolder>();
      this.Project = new Inv.DistinctList<CSharpProject>();
    }

    public void LoadFromDisk(string SolutionFilePath)
    {
      Inv.CSharpSolutionTextFileExtractEngine.Execute
      (
        FilePath: SolutionFilePath,
        CSharpSolution: this
      );
    }

    public Inv.DistinctList<CSharpFolder> Folder { get; private set; }
    public Inv.DistinctList<CSharpProject> Project { get; private set; }
  }

  public sealed class CSharpFolder
  {
    public CSharpFolder()
    {
      this.File = new Inv.DistinctList<CSharpFile>();
      this.Subfolder = new Inv.DistinctList<CSharpFolder>();
      this.Project = new Inv.DistinctList<CSharpProject>();
    }

    public string Guid { get; set; }
    public string Name { get; set; }
    public Inv.DistinctList<CSharpFile> File { get; private set; }
    public Inv.DistinctList<CSharpFolder> Subfolder { get; private set; }
    public Inv.DistinctList<CSharpProject> Project { get; private set; }
  }

  public sealed class CSharpFile
  {
    public string Path { get; set; }
  }

  public enum CSharpProjectType
  {
    Library,
    ConsoleApplication,
    WindowsApplication,
    AppContainer
  }

  public sealed class CSharpProject
  {
    public CSharpProject()
    {
      this.TypeGuid = new Inv.DistinctList<string>();
      this.AssemblyReference = new Inv.DistinctList<CSharpAssemblyReference>();
      this.ProjectReference = new Inv.DistinctList<CSharpProjectReference>();
      this.IncludeReference = new Inv.DistinctList<CSharpIncludeReference>();
      this.COMReference = new Inv.DistinctList<CSharpCOMReference>();
      this.Import = new Inv.DistinctList<CSharpImport>();
      this.Target = new Inv.DistinctList<CSharpTarget>();
    }

    public string Name { get; set; }
    public string Path { get; set; }
    public long? SafeVersionMark { get; set; }
    public string AssemblyName { get; set; }
    public string DefaultNamespace { get; set; }
    public string IconName { get; set; }
    public CSharpPlatform? Platform { get; set; }
    public CSharpConfiguration? Configuration { get; set; }
    public CSharpFramework? Framework { get; set; }
    public bool? ClientProfile { get; set; }
    public string Guid { get; set; }
    public Inv.DistinctList<string> TypeGuid { get; set; }
    public bool? MvcBuildViews { get; set; }
    public bool? UseIISExpress { get; set; }
    public bool? AllowUnsafeBlocks { get; set; }
    public CSharpProjectType? Type { get; set; }
    public Inv.DistinctList<CSharpAssemblyReference> AssemblyReference { get; private set; }
    public Inv.DistinctList<CSharpProjectReference> ProjectReference { get; private set; }
    public Inv.DistinctList<CSharpIncludeReference> IncludeReference { get; private set; }
    public Inv.DistinctList<CSharpCOMReference> COMReference { get; private set; }
    public Inv.DistinctList<CSharpImport> Import { get; private set; }
    public Inv.DistinctList<CSharpTarget> Target { get; private set; }

    public void LoadFromFile(string FilePath)
    {
      Inv.CSharpProjectXmlFileExtractEngine.Execute
      (
        FilePath: FilePath,
        CSharpProject: this
      );
    }
    public void SaveToFile(string FilePath)
    {
      Inv.CSharpProjectXmlFileFormatEngine.Execute
      (
        FilePath: FilePath,
        CSharpProject: this
      );
    }
    public bool IsPortable()
    {
      return TypeGuid.Contains("786C830F-07A1-408B-BD7F-6EE04809D6DB") && TypeGuid.Contains("FAE04EC0-301F-11D3-BF4B-00C04F79EFBC");
    }
  }

  public enum CSharpConfiguration
  {
    Debug,
    Release
  }

  public enum CSharpFramework
  {
    v20,
    v30,
    v35,
    v40,
    v45
  }

  public enum CSharpPlatform
  {
    x86,
    x64,
    AnyCPU
  }

  public sealed class CSharpAssemblyReference
  {
    public string Name { get; set; }
    public string HintPath { get; set; }
    public bool? EmbedInteropTypes { get; set; }
    public bool? SpecificVersion { get; set; }
  }

  public sealed class CSharpProjectReference
  {
    public string Name { get; set; }
    public string Path { get; set; }
    public string Guid { get; set; }
  }

  public sealed class CSharpIncludeReference
  {
    public CSharpIncludeReference()
    {
      this.Option = new Inv.DistinctList<CSharpIncludeOption>();
    }

    public CSharpFile File { get; set; }
    public string Type { get; set; }
    public string DependentUpon { get; set; }
    public Inv.DistinctList<CSharpIncludeOption> Option { get; private set; }
  }

  public sealed class CSharpIncludeOption
  {
    public string Name { get; set; }
    public string Value { get; set; }
  }

  public sealed class CSharpCOMReference
  {
    public string Name { get; set; }
    public string Guid { get; set; }
    public string MajorVersion { get; set; }
    public string MinorVersion { get; set; }
    public string Lcid { get; set; }
    public string WrapperTool { get; set; }
    public bool Isolated { get; set; }
    public bool? EmbedInteropTypes { get; set; }
  }

  public sealed class CSharpImport
  {
    public string Condition { get; set; }
    public string Project { get; set; }
  }

  public sealed class CSharpTarget
  {
    public string Condition { get; set; }
    public string Name { get; set; }
    public string BeforeTargets { get; set; }
    public string AfterTargets { get; set; }
    public string DependsOnTargets { get; set; }
    public Inv.Xml? Content { get; set; }
  }

  public sealed class CSharpCodepoint
  {
    public CSharpCodepoint()
    {
    }

    public bool HasLabel
    {
      get { return LabelFilePath != null; }
    }

    [Conditional("DEBUG")]
    public void Anchor(int Depth = 0)
    {
      if (!HasLabel)
      {
        var StackFrame = new StackFrame(Depth + 1, true);

        this.LabelFilePath = StackFrame.GetFileName();
        this.LabelLineNumber = StackFrame.GetFileLineNumber();
      }
    }
    [Conditional("DEBUG")]
    public void Reanchor(int Depth = 0)
    {
      this.LabelFilePath = null;
      Anchor(Depth);
    }
    [Conditional("DEBUG")]
    public void Goto()
    {
      if (HasLabel)
      {
        var VisualStudio = new Inv.VisualStudioAutomation();
        VisualStudio.OpenFile(LabelFilePath, LabelLineNumber);
      }
    }
    [Conditional("DEBUG")]
    public void Raise()
    {
      if (GoToCodeRaising)
      {
        Goto();
      }
      else
      {
        GoToCodeRaising = true;
        try
        {
          var Control = Mouse.DirectlyOver;
          var Visual = Control as Visual;

          if (Visual != null)
            Control.RaiseEvent(new KeyEventArgs(Keyboard.PrimaryDevice, PresentationSource.FromVisual(Visual), 0, Key.F12) { RoutedEvent = Keyboard.KeyDownEvent });
          else
            Goto();
        }
        finally
        {
          GoToCodeRaising = false;
        }
      }
    }

    private string LabelFilePath;
    private int LabelLineNumber;
    private bool GoToCodeRaising;
  }
}