﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Security.Principal;
using System.Security.AccessControl;
using System.Threading;
using Inv.Support;

namespace Inv
{
  public sealed class GlobalMutex : IDisposable
  {
    public GlobalMutex(string Name)
    {
      var MutexId = string.Format("Global\\{0}", Name);
      
      this.Handle = new Mutex(false, MutexId);

      var AllowEveryoneRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl, AccessControlType.Allow);
      var SecuritySettings = new MutexSecurity();
      SecuritySettings.AddAccessRule(AllowEveryoneRule);
      Handle.SetAccessControl(SecuritySettings);
    }
    public void Dispose()
    {
      if (Handle != null)
      {
        try
        {
          Unlock();
        }
        catch (Exception Exception)
        {
          Debug.WriteLine(Exception.Message);

          if (Debugger.IsAttached)
            Debugger.Break();
        }

        Handle.Dispose();
        Handle = null;
      }
    }

    public bool Lock(TimeSpan Timeout)
    {
      if (!IsAcquired)
      {
        try
        {
          this.IsAcquired = Handle.WaitOne(Timeout, false);
        }
        catch (AbandonedMutexException)
        {
          this.IsAcquired = true;
        }
      }

      return IsAcquired;
    }
    public void Unlock()
    {
      if (IsAcquired)
      {
        Handle.ReleaseMutex();
        IsAcquired = false;
      }
    }

    private System.Threading.Mutex Handle;
    private bool IsAcquired;
  }

}
